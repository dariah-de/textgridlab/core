package info.textgrid.lab.core.model;

public class ProjectDoesNotExistException extends Exception {

	public ProjectDoesNotExistException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProjectDoesNotExistException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ProjectDoesNotExistException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ProjectDoesNotExistException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
