/**
 * The TextGridLab's model layer, represents objects' metadata.
 */
package info.textgrid.lab.core.model;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static info.textgrid.lab.core.model.Activator.PLUGIN_ID;
import static info.textgrid.lab.core.model.Activator.handleError;
import static java.text.MessageFormat.format;
import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event;
import info.textgrid.lab.core.model.util.ModelUtil;
import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.core.swtutils.CoreExceptionStatus;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.lab.log.logsession;
import info.textgrid.namespaces.metadata.agent._2010.AgentRoleType;
import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;
import info.textgrid.namespaces.metadata.core._2010.AuthorityType;
import info.textgrid.namespaces.metadata.core._2010.CollectionType;
import info.textgrid.namespaces.metadata.core._2010.EditionType;
import info.textgrid.namespaces.metadata.core._2010.EditionType.License;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.TextgridUri;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.ItemType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectFactory;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.metadata.core._2010.SourceType;
import info.textgrid.namespaces.metadata.core._2010.WorkType;
import info.textgrid.namespaces.metadata.script._2010.FormOfNotationType;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerFactory;
import javax.xml.ws.Holder;

import org.apache.axiom.om.OMElement;
import org.apache.xerces.jaxp.JAXPConstants;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.internal.resources.ResourceException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.AssertionFailedException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * This class represents one TextGridObject, respectively its metadata.
 * 
 * You may get a new TextGridObject in the following ways, depending on what you
 * have:
 * 
 * <ul>
 * <li>From an {@link IFile}, call it <var>file</var>
 * 
 * <pre>
 *  	TextGridObject tgo = file.{@linkplain IAdaptable#getAdapter(Class) getAdapter}(TextGridObject.class);
 * </pre>
 * 
 * </li>
 * <li>From an {@link URI} representing a TextGridObject:
 * 
 * <pre>
 *  	{@link TextGridObject} tgo = TextGridObject.{@linkplain #getInstance(URI, boolean) getInstance(uri, true)};
 * </pre>
 * 
 * (or <code>false</code> to always avoid network operations)
 * 
 * </li>
 * <li>From an {@link OMElement} containing existing metadata:
 * 
 * <pre>
 *  	{@link TextGridObject} tgo = TextGridObject.{@linkplain #getInstance(OMElement, boolean) getInstance(element, false)};
 * </pre>
 * 
 * (or <code>true</code> as last argument if <code>element</code> definitely
 * contains the complete metadata)
 * 
 * </ul>
 * 
 * <p>
 * You <em>cannot</em> call a constructor directly. The create methods make sure
 * that every object in the TextGridRep is represented by only one
 * {@link TextGridObject}.
 * </p>
 * 
 * <p>
 * TextGridObjects may be <em>incomplete</em>, i.e. they may contain less
 * metadata than the object has, if this avoids network access. It will always
 * have an {@linkplain #getURI() URI}, however. Check for completeness using
 * {@link #isComplete()}, retrieve potentially incomplete objects via
 * {@link #getInstance(OMElement, boolean)} or
 * {@link #getInstance(URI, boolean)} with the second argument set to
 * <code>false</code>.
 * </p>
 * 
 * <p>
 * To read or write the actual contents of an object, you should convert the
 * TextGridObject to an {@link IFile}:
 * </p>
 * 
 * <pre>
 * IFile file = (IFile) textGridObject.getAdapter(IFile.class);
 * </pre>
 * <p>
 * You can then call IFile's {@link IFile#getContents()} and
 * {@link IFile#setContents(InputStream, int, IProgressMonitor)} methods.
 * </p>
 * 
 * 
 * Please see the <a href="../../../../../../guide/model/index.html">Overview
 * document</a> for more information about the general architecture.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) and Christoph
 *         Ludwig (cl) for TextGrid
 * @TODO extract interface
 */
@SuppressWarnings("restriction")
public class TextGridObject extends AbstractResource implements
		ITextGridPermission, ITextGridModelConstants {

	/**
	 * Runtime exception that is thrown when creating a deep copy of a metadata
	 * record fails.
	 */
	protected static class DeepCopyException extends RuntimeException {
		private static final long serialVersionUID = 2755746487109125338L;

		public DeepCopyException(ObjectType objectType, Throwable cause) {
			super(formatMessage(objectType, cause), cause);
		}

		private static String formatMessage(ObjectType objectType,
				Throwable cause) {
			return format(
					"An internal error ({2}) occurred creating a deep copy of the metadata record {0}: {1}", //$NON-NLS-1$
					objectType, cause.getLocalizedMessage(), cause.getClass()
							.getSimpleName());
		}

	}

	/**
	 * The actual metadata record
	 */
	private ObjectType metadata;

	// private static final String XPATH_URI = ".//tg:uri";
	//
	// public static final String XPATH_TITLE = ".//tg:title";
	//
	// public static final String XPATH_SIZE = ".//tg:size";
	//
	// public static final String XPATH_PROJECT_ID = ".//tg:project/@id";
	//
	// public static final String XPATH_PERMISSIONS = ".//tg:permissions";
	//
	// public static final String XPATH_LAST_MODIFIED = ".//tg:lastModified";
	//
	// public static final String XPATH_CREATED = ".//tg:created";
	//
	// public static final String XPATH_FORMAT = ".//tg:format";
	//
	// public static final String XPATH_AUTHORS = ".//tg:agent[@role='author']";
	//
	// public static final String XPATH_ISVERSIONOF = ".//tg:isVersionOf";
	//
	// public static final String XPATH_OWNER_ID = ".//tg:owner/@id";
	//
	// public static final String XPATH_CRUDWARNING =
	// ".//tg:administrative/tg:middleware/tg:warning";

	public static final String MOVE_TGOBJECT = "MOVE_TGOBJECT"; //$NON-NLS-1$

	private static final ISIDChangedListener sidChangedListener = new ISIDChangedListener() {

		public void sidChanged(String newSID, String newEPPN) {
			for (TextGridObject object : registry.values()) {
				object.resetPermissionCache();
			}
		}

	};

	static {
		AuthBrowser.addSIDChangedListener(sidChangedListener);
	}

	/**
	 * Returns a string representation of this object.
	 * 
	 * <p>
	 * The result of this method is not intended for end users, it should be
	 * used in logging and debugging contexts. The following alternative methods
	 * also return some kind of string representation of this object:
	 * </p>
	 * <ul>
	 * <li>{@link #getURI()} returns the object's uri.</li>
	 * <li>{@link #serialize()} returns a serialized XML representation of the
	 * object's full metadata.</li>
	 * <li>{@link #getNameCandidate()} returns a candidate for representing this
	 * object to the user when a name is required.
	 * </ul>
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (title != null && getURI() != null)
			return title + " (" + getURI().toString() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
		if (getURI() != null)
			return getURI().toString();
		return super.toString();
	}

	public static final String TEXTGRID_METADATA_NAMESPACE = "http://textgrid.info/namespaces/metadata/core/2010"; //$NON-NLS-1$

	// FIXME: The following namespaces are WRONG. It should be possible to get
	// this all from somewhere else ...
	@Deprecated
	public static final QName CLIENT_NAME = new QName(
			TEXTGRID_METADATA_NAMESPACE, "client"); //$NON-NLS-1$
	@Deprecated
	public static final String TEXTGRID_DATERANGE_NAMESPACE = "http://textgrid.info/namespaces/metadata/dateRange/2008-02-18"; //$NON-NLS-1$

	public static String CUSTOM_NAMESPACE = "http://textgrid.info/custom-namespace/2009-01-01"; //$NON-NLS-1$

	/**
	 * This {@link QName} is passed into the stub's getOMElement method for
	 * tgObjectMetadata as parent qname since this may not be null (or it would
	 * result in an {@link NullPointerException}). It is used to create the
	 * tgObjectMetadata element, because the response types' getTgObjectMetadata
	 * seem to return only the <em>contents</em> of the tgObjectMetadata
	 * element, not the actual element.
	 * 
	 * Is it possible to retrieve the actual root element?
	 * 
	 */

	public static final QName METADATA_ROOT_NAME = new QName("objectType"); //$NON-NLS-1$

	private static final Calendar LAST_MODIFIED_NOT_AVAILABLE;

	public static final String NO_TITLE = Messages.TextGridObject_noTitle;

	static {
		LAST_MODIFIED_NOT_AVAILABLE = Calendar.getInstance();
		LAST_MODIFIED_NOT_AVAILABLE.setTimeInMillis(0L);
	}

	private static Map<URI, TextGridObject> registry = new ConcurrentHashMap<URI, TextGridObject>();
	private static Map<URI, URI> latestRegistry = new ConcurrentHashMap<URI, URI>();
	private static Map<URI, TextGridObject> handleRegistry = new ConcurrentHashMap<URI, TextGridObject>();
	private URI uri = null;

	/**
	 * cache,
	 * 
	 * @see #getLastModified()
	 */
	private Calendar lastModified;

	/**
	 * cache,
	 * 
	 * @see #getCreated()
	 */
	private Calendar created;

	/**
	 * cache,
	 * 
	 * @see #getSize()
	 */
	private long size = -1;

	/**
	 * cache -- except in new objects, where it contains the actual value.
	 * 
	 * @see #getProject()
	 */
	private String project = null;
	private String title = null;
	private volatile int permissions = UNKNOWN; // TG-423
	private boolean complete = false;
	private boolean submittable = false;
	private URI efsUri;
	private boolean deleted = false;
	private boolean metadataDirty = false;
	private String contentTypeID;
	private byte[] initialContent = null;
	
	// flag is used to remember if this object should
	// be opened in read only mode in an editor
	private boolean openAsReadOnly = false;

	/**
	 * title and idNo->type must not be empty in the Metadata-editor.
	 */
	private boolean isTitleEmpty = false;
	private boolean isIdnoTypeEmpty = false;

	private TextGridObject preparedNewVersion;

	private TextGridObject preparedNewRevision;
	private String revisionURI = ""; //$NON-NLS-1$

	private boolean accessible = true;

	private URI preparedURI;

	// private boolean preventNotifyingMetadataChanged = false;

	public void setTitleEmpty(boolean empty) {
		isTitleEmpty = empty;
	}

	private boolean isTitleEmpty() {
		return isTitleEmpty;
	}

	public void setIdnoTypeEmpty(boolean empty) {
		isIdnoTypeEmpty = empty;
	}

	private boolean isIdnoTypeEmpty() {
		return isIdnoTypeEmpty;
	}

	/**
	 * If true, this object has been deleted. The inverse is not neccessarily
	 * true.
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * True if this object's metadata has been
	 * {@linkplain #internalSetMetadataXML(OMElement, boolean) modified} but not
	 * yet {@linkplain #makeMetadataPersistent() made persistent}.
	 */
	public boolean isMetadataDirty() {
		return metadataDirty;
	}

	/**
	 * DO NOT CALL THIS AT HOME. IT IS NOT API. IT IS AUTOMAGICALLY CALLED FROM
	 * THE RESPONSIBLE METHODS. TRUST US.
	 */
	public void setMetadataDirty(boolean dirty) {
		this.metadataDirty = dirty;
		if (dirty && Activator.isDebugging(Activator.DEBUG_METADATA)
				&& getURI().toString().equals(Platform.getDebugOption(Activator.DEBUG_METADATA_URI).trim())) {
			StatusManager.getManager()
					.handle(new Status(IStatus.INFO, Activator.PLUGIN_ID,
							"Someone considers the metadata dirty, it is now:\n" + serialize(),
							new Exception("And here's the suspect: ")), StatusManager.LOG);
		}
	}

	/**
	 * Invalidate this object's data cache, this MUST be called every time the
	 * backing XML data is modified.
	 * 
	 * <p>
	 * Instances of this class store the TextGrid object's metadata in an XML
	 * representation, like it is read and written from and to the repository.
	 * Frequently accessed fields may be accessed by getter methods like
	 * {@link #getProject()}, {@link #getTitle()}, {@link #getURI()} etc. These
	 * getters walk through (and potentially parse) the XML data the first time
	 * they are called. For further calls the results are cached.
	 * </p>
	 * <p>
	 * <code>invalidateCache</code> is automatically called by
	 * {@link #internalSetMetadataXML(OMElement, boolean)} and
	 * {@link #move(OMElement)}.
	 * </p>
	 * 
	 */
	protected synchronized void invalidateCache() {
		lastModified = null;
		size = -1;
		if (!isNew())
			project = null;
		title = null;
		permissions = -1;
		submittable = checkMetadataSubmittable(); // TG-157
		contentTypeID = null;
	}

	/**
	 * A very crude (and inefficient) method to check if the metadata stored in
	 * rootElement is complete.
	 * 
	 * @return true if and only if the XML fragment stored in rootElement
	 *         satisfies the TextGrid Metadata Schema and, in case the object is
	 *         already known by the middleware (i.e., the scheme of the objects
	 *         URI is "textgrid"), if the middleware-element is present in the
	 *         administrative metadata.
	 */
	@Deprecated
	private boolean checkMetadataSubmittable() {
		// TODO: adjust the code with the requirements of the new metadata
		// schema
		if (isTitleEmpty() || isIdnoTypeEmpty())
			return false;

		return true;
	}

	/**
	 * Creates a new TextGridObject from the URI of an existing object, reading
	 * the metadata via TGcrud.
	 * 
	 * Clients may not call this method, but must use {@link #getInstance(URI)}.
	 * 
	 * @param uri
	 *            the object's URI
	 * @param readMetadata
	 *            if true, ask CRUD politely for the complete metadata (this may
	 *            be a long-running operation), otherwise, set
	 *            {@link #isComplete()} to <code>false</code>.
	 * @throws CrudServiceException
	 *             when a call to the service fails.
	 */
	protected TextGridObject(URI uri, boolean readMetadata)
			throws CrudServiceException {
		assertURIValidForTGO(uri);
		this.setURI(uri);
		this.efsUri = computeEFSURI(uri);

		if (readMetadata)
			doReadMetadata();
		else
			setComplete(false);
	}

	/**
	 * Performs the Act of reading the object's metadata from TGcrud. The
	 * {@link #uri} field must already be set.
	 * 
	 * <p>
	 * Clients call {@link #getInstance(URI, boolean)} with the second argument
	 * set to <code>true</code> for or {@link #reloadMetadata(boolean)} instead
	 * of this method.
	 * </p>
	 * 
	 * This is a long-running operation with network access.
	 * 
	 * @throws CrudServiceException
	 *             when something goes wrong.
	 */
	private void doReadMetadata() throws CrudServiceException {
		if (isNew()) {
			setComplete(true);
			return;
		}

		final String sid = RBACSession.getInstance().getSID(false);
		final String loggingParam = logsession.getInstance().getloginfo();

		ModelUtil.checkNonUIThread(
				"Tried to read metadata for {0} from an UI thread.", this); //$NON-NLS-1$

		TGCrudService stub = CrudClientUtilities.getCrudServiceStub();

		MetadataContainerType metadataContainer;
		try {
			try {
				if (Activator.isDebugging(Activator.DEBUG_READS)) {
					StatusManager.getManager().handle(
							new Status(IStatus.INFO, Activator.PLUGIN_ID, NLS
									.bind("Called readMetadata on {0}", this), //$NON-NLS-1$
									new Exception()));
				}
				metadataContainer = stub.readMetadata(sid, loggingParam,
						getURI().toString());
			} catch (AuthFault e) {
				String sessionId = RBACSession.getInstance().getSID(true);
				if ("".equals(sessionId)) { //$NON-NLS-1$
					throw e; // user doesn't want to login -> just throw the
								// error
				} else {
					metadataContainer = stub.readMetadata(sessionId,
							loggingParam, getURI().toString());
				}
			}
		} catch (AuthFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (IoFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (MetadataParseFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (ObjectNotFoundFault e) {
			throw new CrudServiceException(e, getURI());
		}

		ObjectType metadata = metadataContainer.getObject();

		internalSetMetadata(metadata, false);
		setComplete(true);
		setAccessible(true);
		// setComplete must be called before any workspace callout
		// to avoid recursion on empty metadata fields via stuff like
		// getTitle() (TG-630)
		register(); // TG-354 -- need to be registered before
		// the refresh to avoid recursion
		refreshWorkspaceIfNeccessary(); // TG-301 -- causes
		// scheduling
		// conflicts by now

	}

	/**
	 * Returns true when this object has not yet been created in the
	 * TextGridRep.
	 */
	public boolean isNew() {
		String scheme = getURI().getScheme();
		return ITextGridModelConstants.SCHEMA_NEWFILE.equals(scheme);
	}

	/**
	 * Creates a new TextGridObject instance from an existing XML representation
	 * of a metadata object.
	 * 
	 * Clients MUST NOT call this method directly, but instead call the factory
	 * method {@link #getInstance(OMElement)}.
	 * 
	 * <p>
	 * This is a long-running operation with network access.
	 * </p>
	 * 
	 * @param metadata
	 * @throws CoreException
	 *             when something goes wrong
	 */
	protected TextGridObject(final ObjectType metadata) throws CoreException {
		String uriString = ""; //$NON-NLS-1$
		try {
			try {
				setURI(new URI(metadata.getGeneric().getGenerated()
						.getTextgridUri().getValue()));
				assertURIValidForTGO(getURI());
			} catch (AssertionFailedException e) {
				throw new IllegalArgumentException(
						"Could not extract an URI from the given metadata object " //$NON-NLS-1$
								+ metadata.toString(), e);
			}
			efsUri = computeEFSURI(getURI());
			internalSetMetadata(metadata, false);
		} catch (URISyntaxException e) {
			throw new CoreException(Activator.handleError(e,
					Messages.TextGridObject_InvalidURIInMD,
					uriString));
		}
		register();
	}

	/**
	 * Creates a new TextGridObject instance representing a new TextGridRep
	 * object. Clients call {@link #getNewObjectInstance(String, String)}
	 * instead.
	 * 
	 * 
	 * @param project
	 *            The ID of the project the object lives in.
	 * @param format
	 *            The object's content type.
	 * @see #getNewObjectInstance(String, String)
	 */
	protected TextGridObject(final String project, final String format) {

		Assert.isLegal(project != null && project.length() > 0,
				"The project ID must be a non-empty string"); //$NON-NLS-1$

		final ObjectFactory objectFactory = new ObjectFactory();
		metadata = objectFactory.createObjectType();
		GenericType generic = objectFactory.createGenericType();
		metadata.setGeneric(generic);
		ProvidedType provided = objectFactory.createProvidedType();
		generic.setProvided(provided);
		provided.setFormat(format);

		this.project = project;

		// FIXME do we need to create other metadata records already? (custom,
		// relations, ...)

		setComplete(true); // new files are always complete

		try {
			IPath path = new Path("/").append(project).append( //$NON-NLS-1$
					Messages.TextGridObject_NewObjectPrefix.concat(UUID.randomUUID().toString()));
			this.setURI(new URI(ITextGridModelConstants.SCHEMA_NEWFILE, path
					.toPortableString(), null));
			efsUri = computeEFSURI(getURI());
		} catch (URISyntaxException e) {
			handleError(e,
					"Subtle URI creation error for new object. Please file a bug report."); //$NON-NLS-1$
		}
	}

	/**
	 * Sets this object's metadata.
	 * 
	 * <p>
	 * This method <em>does not make anything persistent</em>, i.e. nothing is
	 * written to the repository until one of the following events occur:
	 * </p>
	 * <ol>
	 * <li>Some client explicitly calls {@link #makeMetadataPersistent()}</li>
	 * <li>Some client saves the contents corresponding to this object, e.g. by
	 * calling
	 * {@linkplain IFile#setContents(java.io.InputStream, int, IProgressMonitor)
	 * IFile.setContents(...)}.</li>
	 * </ol>
	 * <p>
	 * The metadata modified using this method is
	 * {@linkplain #isMetadataDirty() marked as dirty} until someone saves it.
	 * </p>
	 * 
	 * <h4>Warning: Call-by-reference semantics</h4> Clients <strong>must
	 * abandon the object passed in and may not modify it</strong> after calling
	 * this method. Otherwise {@link TextGridObject}'s behaviour is undefined.
	 * 
	 * <h4>Metadata handling requirements</h4>
	 * 
	 * Clients who want to modify metadata should usually follow the following
	 * workflow:
	 * <ol>
	 * <li>Load the object's metadata by calling {@link #getMetadata()}</li>
	 * <li>Modify those parts of the result of the first step that the client
	 * cares for, but leave everything else intact</li>
	 * <li>call {@link #setMetadata(ObjectType)} to set the
	 * {@link TextGridObject} to the modified metadata object</li>
	 * <li>throw away all references to the object passed in</li>
	 * <li>If desired, call {@link #makeMetadataPersistent()} to write the
	 * metadata to the grid.</li>
	 * </ol> {@link #getMetadata()} may be and {@link #setMetadata(ObjectType)}
	 * is a long running operation, thus you should perform these steps in a
	 * background job. You should also perform steps 1–3 immediately after one
	 * another in the background job to avoid corrupting other clients' changes.
	 * 
	 * 
	 * <p>
	 * Clients that use this method to modify metadata MUST ensure that:
	 * <ul>
	 * <li>the given metadata is valid according to the current metadata scheme,
	 * at least as far as possible with the data given by the user,</li>
	 * <li>the {@linkplain #getURI() URI} is not changed,</li>
	 * <li>the section <code>administrative/middleware</code> is left unchanged,
	 * </li>
	 * <li>modifications in sections not handled by the client are left
	 * unchanged.</li>
	 * </ul>
	 * 
	 * 
	 * @param metadata
	 *            root element containing the new metadata. This must be in the
	 *            {@link #TEXTGRID_METADATA_NAMESPACE} and may not be null.
	 *            <strong>Clients MUST give up on this object after calling this
	 *            method.</strong>
	 * @throws IllegalArgumentException
	 *             if the given metadata element is not in the correct namespace
	 *             (i.e. {@link #TEXTGRID_METADATA_NAMESPACE}) or some other
	 *             condition required above has been detected as not been met
	 */
	public synchronized void setMetadata(final ObjectType metadata) {
		internalSetMetadata(metadata, true);
	}

	/**
	 * Compatiblity method that unmarshals the given XML fragment to an
	 * {@link ObjectType} and then call {@link #setMetadata(ObjectType)}
	 * 
	 * @param metadataXML
	 *            an XML fragment starting at &lt;tgm:object&gt;
	 * 
	 * @deprecated use {@link #setMetadata(ObjectType)} directly
	 */
	public synchronized void setMetadataXML(final OMElement metadataXML) {
		setMetadata(toObjectType(metadataXML));
	}

	/**
	 * Internal method that sets the XML fragment that is the basis for this
	 * element.
	 * 
	 * <strong>This is an internal method only, clients must call
	 * {@link #setMetadataXML(OMElement)}.</strong>
	 * 
	 * @param element
	 *            root element containing the new metadata. This must be in the
	 *            {@link #TEXTGRID_METADATA_NAMESPACE} and may not be null.
	 * @param markDirty
	 *            the new value of the object's {@link #isMetadataDirty()} flag.
	 *            Only internal methods may set this to <code>false</code>, and
	 *            only if the metadata in <var>element</var> is coming directly
	 *            from the TextGridRep (ie TG-search or TG-crud). In all other
	 *            events, {@link #setMetadataXML(OMElement)} must be called
	 *            (which sets the flag to <code>true</code>).
	 * @throws IllegalArgumentException
	 *             if the given metadata element is not in the correct namespace
	 *             (i.e. {@link #TEXTGRID_METADATA_NAMESPACE}) or some other
	 *             condition required above has been detected as not been met
	 * @noreference This method is not intended to be referenced by clients.
	 */
	public synchronized void internalSetMetadata(ObjectType metadata,
			boolean markDirty) {
		Assert.isNotNull(metadata,
				"setMetadateXML does not accept a null argument."); //$NON-NLS-1$

		String newURI;
		newURI = metadata.getGeneric().getGenerated() == null ? null : metadata
				.getGeneric().getGenerated().getTextgridUri().getValue();

		if (newURI != null && !"".equals(newURI) && getURI() != null //$NON-NLS-1$
				&& !getURI().toString().equals(newURI.trim()))
			this.setURI(URI.create(newURI));

		this.metadata = metadata;
		if (Activator.isDebugging(Activator.DEBUG_METADATA)
				&& this.getURI().toString().equals(Platform.getDebugOption(Activator.DEBUG_METADATA_URI).trim())) {
			StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID,
					"Changed metadata to\n" + serialize(), new Exception("Metadata changed from here: ")),
					StatusManager.LOG);
		}

		if (getHandle() != null)
			synchronized (handleRegistry) {
				handleRegistry.put(getHandle(), this);
			}

		invalidateCache();
		setMetadataDirty(markDirty);

		TextGridObject.notifyListeners(Event.METADATA_CHANGED, this);
	}

	/**
	 * Returns the PID (handle) of the object or <code>null</code> if none
	 * found.
	 */
	private URI getHandle() {
		if (metadata.getGeneric().getGenerated() != null) {
			List<Pid> pids = metadata.getGeneric().getGenerated().getPid();
			if (pids != null)
				for (Pid pid : pids)
					if (pid.getPidType().equals("handle")) //$NON-NLS-1$
						return URI.create(pid.getValue());
		}
		return null;
	}

	/**
	 * Eclipse's resource framework caches some information about the
	 * {@link IFile}s it contains, even if these are links (like our
	 * TextGridObject counterparts). If the remote copy has changed, but the
	 * local cache hasn't, our users are likely to get a
	 * <em>Resource out of Sync</em> error message (TG-301).
	 * 
	 * This method tries to check first if a refresh is neccessary and if true,
	 * tries to perform a refresh of the local file's information.
	 * 
	 * Note that this method may silently fail (well, almost silently: It logs
	 * an error to eclipse's error log) if a refresh is forbidden by the current
	 * scheduling rules.
	 * 
	 * Also note that refreshing may happen deferredly in a separate workspace
	 * job.
	 * 
	 */
	public void refreshWorkspaceIfNeccessary() {
		try {
			final IFile file = ModelAdaptorFactory.getFileFor(this, false);

			if (file != null && file.exists()
					&& !file.isSynchronized(IFile.DEPTH_ZERO)) {
				ISchedulingRule refreshRule = file.getWorkspace()
						.getRuleFactory().refreshRule(file);
				WorkspaceJob refreshJob = new WorkspaceJob(NLS.bind(
						Messages.TextGridObject_Refreshing_X, file)) {

					@Override
					public IStatus runInWorkspace(IProgressMonitor monitor)
							throws CoreException {
						file.refreshLocal(IFile.DEPTH_ZERO, monitor);
						Activator
								.handleProblem(
										IStatus.INFO,
										null,
										"Refreshed workspace link {0} due to modifications of the remote copy.", //$NON-NLS-1$
										file);
						return Status.OK_STATUS;
					}
				};
				Job currentJob = Job.getJobManager().currentJob();
				ISchedulingRule rule = currentJob == null ? null : currentJob
						.getRule();
				if (rule != null && refreshRule.isConflicting(rule)) {
					refreshJob.setRule(refreshRule);
					refreshJob.schedule();
				} else
					try {
						refreshJob.runInWorkspace(null);
					} catch (CoreException e) {
						Activator.handleError(e, Messages.TextGridObject_FailedToRefreshX, file);
					}
			}
		} catch (SchedulingException e) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							e,
							"Could not try to refresh the workspace representation of {0} due to scheduling issues.", //$NON-NLS-1$
							this);
		}
	}

	/**
	 * Pushes the TextGridObject's metadata to the Grid.
	 * 
	 * <p>
	 * This is a <strong>long-running operation</strong> with network access.
	 * Clients should not call this from the UI thread.
	 * </p>
	 * 
	 * @throws CoreException
	 */
	public synchronized void makeMetadataPersistent(
			final IProgressMonitor monitor) throws CoreException {
		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridObject_SavingMDForX, this), 100);
		if (!isSubmittable() || !isComplete() || isTitleEmpty()
				|| isIdnoTypeEmpty()) {
			TextGridObject.notifyListeners(Event.METADATA_INCOMPLETE, this);
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					Messages.TextGridObject_IncompleteMD, null);
			throw new IncompleteMetadataException(status);
		}

		ObjectType metadataToSubmit = this.getMetadata();

		try {
			if (isNew()) {
				IFile file = (IFile) getAdapter(IFile.class);
				// passing null to setContents instead of an InputStream
				// means 0 Byte "file length".
				file.setContents((InputStream) null, IFile.FORCE, null);
			} else {
				ModelUtil
						.checkNonUIThread(
								"Tried to make {0}'s metadata persistent from an UI thread.", //$NON-NLS-1$
								this);
				assertPermission(
						ITextGridPermission.UPDATE,
						Messages.TextGridObject_UpdateForbidden,
						this);

				Activator.logServiceCall(
						"Calling TGcrud#updateMetadata({0}, {1}, {2}, ...)", //$NON-NLS-1$
						RBACSession.getInstance().getSID(false), logsession
								.getInstance().getLogEndpoint(), this);
				Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
				metadataHolder.value = new ObjectFactory()
						.createMetadataContainerType();
				metadataHolder.value.setObject(metadataToSubmit);

				TGCrudService crudServiceStub = CrudClientUtilities
						.getCrudServiceStub();
				crudServiceStub.updateMetadata(RBACSession.getInstance()
						.getSID(false), logsession.getInstance()
						.getLogEndpoint(), metadataHolder);

				internalSetMetadata(metadataHolder.value.getObject(), false);
				setComplete(true);
				IFile file = (IFile) getAdapter(IFile.class);
				if (file != null)
					file.refreshLocal(IResource.DEPTH_ZERO,
							progress.newChild(5)); // TG-296
				TextGridObject.notifyListeners(Event.METADATA_SAVED, this);
			}
		} catch (OfflineException e) {
			throw new CrudServiceException(e, getURI());
		} catch (AuthFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (IoFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (MetadataParseFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (ObjectNotFoundFault e) {
			throw new CrudServiceException(e, getURI());
		} catch (UpdateConflictFault e) {
			throw new CrudServiceException(e, getURI());
		}
	}

	/**
	 * @deprecated this is a long-running operation, use
	 *             {@link #makeMetadataPersistent(IProgressMonitor)} instead.
	 */
	@Deprecated
	public synchronized void makeMetadataPersistent() throws CoreException {
		makeMetadataPersistent(null);
	}

	/**
	 * Creates a deep copy of the TextGridObject's metadata and give is back as
	 * function value. Notice that this function is very expensive, so it should
	 * only be called if really necessary. If the metadata is only needed for
	 * reading, {@link #getMetadataForReading()} should be used.
	 * 
	 * @return copy of metadata
	 */
	public synchronized ObjectType getMetadata() throws CrudServiceException {
		reloadMetadata(false);
		return deepCopy(metadata);
	}

	/**
	 * Returns a reference to the TextGridObject's metadata. This function
	 * should only be used, if the metadata is needed for informational
	 * purposes. If you intend to manipulate the metadata {@link #getMetadata()}
	 * should be used.
	 * 
	 * @return metadata as reference
	 */
	public synchronized ObjectType getMetadataForReading()
			throws CrudServiceException {
		reloadMetadata(false);
		return metadata;
	}

	@Deprecated
	public synchronized OMElement getMetadataXML() throws CrudServiceException {
		try {
			return StringToOM.getOMElement(serialize());
		} catch (XMLStreamException e) {
			throw new IllegalStateException(
					"Internal error: Can't parse our own metadata.", e); //$NON-NLS-1$
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException(
					"Internal error: Can't parse our own metadata.", e); //$NON-NLS-1$
		} catch (FactoryConfigurationError e) {
			throw new IllegalStateException(
					"Internal error: Can't parse our own metadata.", e); //$NON-NLS-1$
		}
	}

	/**
	 * Creates a deep copy of the given ObjectType.
	 * 
	 * @param in
	 * @return
	 */
	public static ObjectType deepCopy(final ObjectType in) {
		ObjectType result;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream objectStream = new ObjectOutputStream(baos);
			objectStream.writeObject(in);
			objectStream.close();
			baos.close();
			ObjectInputStream objectInputStream = new ObjectInputStream(
					new ByteArrayInputStream(baos.toByteArray()));
			result = (ObjectType) objectInputStream.readObject();
		} catch (IOException e) {
			throw new DeepCopyException(in, e);
		} catch (ClassNotFoundException e) {
			throw new DeepCopyException(in, e);
		}
		return result;
	}

	/**
	 * Loads this object's metadata from an external source. Warning: This
	 * replaces the existing metadata, it does not perform a merge!
	 * 
	 * <p>
	 * This method does not replace all metadata. Generally, it cannot be used
	 * to change the project or the URI (it is not possible to change these
	 * values), and the middleware generated fields (if present) will not be
	 * modified, as well.
	 * </p>
	 * 
	 * @param source
	 *            the stream to load the metadata from
	 */
	public synchronized void loadMetadata(InputStream source)
			throws CoreException { // TODO Rewrite for new metadata
									// implementation
		ObjectType newMetadata = JAXB.unmarshal(source, ObjectType.class);
		ObjectType oldMetadata = getMetadata();

		// Replacements in the loaded metadata
		newMetadata.getGeneric().setGenerated(
				oldMetadata.getGeneric().getGenerated());

		// now documentElement contains the prepared new metadata.
		setMetadata(newMetadata);
	}

	/**
	 * Returns whether the TextGridObject contains a copy of the metadata that
	 * is considered complete, i.e. it has either {@linkplain #doReadMetadata()
	 * been read from TGcrud} or {@linkplain #getInstance(OMElement, boolean)
	 * been created by a caller that trusts the given metadata to be complete}.
	 * 
	 * <strong>Attention:</strong> to check whether the user has entered enough
	 * metadata for this object to be made persistent, please use
	 * {@link #isSubmittable()}.
	 * 
	 * @return true if the metadata has been completely loaded (from TGcrud).
	 * @see reloadMetadata(boolean)
	 * @see #isSubmittable()
	 */
	public boolean isComplete() {
		return complete;
	}

	protected void setComplete(boolean complete) {
		this.complete = complete;
	}

	/**
	 * This flag determines whether this object is generally considered
	 * <em>accessible</em>, i.e. read or readMetadata operations are likely to
	 * succeed.
	 * <p>
	 * The value defaults to <code>true</code> and might be set to
	 * <code>false</code> when a read operation on the object fails with an
	 * authentication or object not found fault.
	 * </p>
	 * <p>
	 * Clients may use this to avoid repeated authentication errors on the same
	 * object, cf., e.g., TG-897.
	 * </p>
	 * 
	 * @return <code>false</code> if a previous attempt to load this object's
	 *         metadata during this session has failed, <code>true</code>
	 *         otherwise.
	 * @category EXPERIMENTAL
	 */
	public boolean isAccessible() {
		return accessible;
	}

	protected void setAccessible(boolean accessible) {
		this.accessible = accessible;
	}

	/**
	 * Returns whether the user has supplied enough metadata for this object to
	 * be submitted to the repository.
	 * 
	 * @see #isComplete()
	 */
	public boolean isSubmittable() {
		return submittable;
	}

	/**
	 * @see #isSubmittable()
	 */
	protected void setSubmittable(boolean submittable) {
		this.submittable = submittable;
	}

	/**
	 * Reloads or completes the object's metadata.
	 * 
	 * <p>
	 * Clients should call this whenever they need a consistent and {linkplain
	 * {@link #isComplete() complete} set of metadata, e.g. before editing.
	 * 
	 * <p>
	 * This is a <strong>long-running operation</strong> with network access.
	 * Clients should not call this from the UI thread.
	 * </p>
	 * 
	 * @param force
	 *            If false, call TGcrud only if the object does not consider the
	 *            metadata to be {@linkplain #isComplete() complete}. If true,
	 *            TGcrud is called in any case.
	 * @throws CrudServiceException
	 *             If the call to crud fails.
	 */
	public void reloadMetadata(boolean force) throws CrudServiceException {
		if (force || !complete) {
			if (isAccessible()) {
				if (Activator.isDebugging(Activator.DEBUG_INCOMPLETE)) {
					System.out
							.println(MessageFormat
									.format("reloadMetadata will reload metadata: force = {0}, complete = {1} ({2})", //$NON-NLS-1$
											force, complete, this));

					if (Activator
							.isDebugging(Activator.DEBUG_INCOMPLETE_VERBOSE)) {
						StackTraceElement[] stackTrace = new Exception()
								.getStackTrace();
						System.out.println("  " + stackTrace[1]); //$NON-NLS-1$
						System.out.println("  " + stackTrace[2]); //$NON-NLS-1$
					}
				}
				doReadMetadata();
			} else {
				if (Activator.isDebugging(Activator.DEBUG_INCOMPLETE)) {
					System.out
							.println(MessageFormat
									.format("prevented reloadMetadata since resource is not accessible: force = {0}, complete = {1} ({2})", //$NON-NLS-1$
											force, complete, this));
				}
			}
		}
	}

	@Deprecated
	public static TextGridObject getInstance(final OMElement element,
			final boolean isComplete) throws CoreException {

		// JAXB.unmarshal(String,Class) treats a String argument as a
		// filename!!!
		// so we better give an InputStream, creating one from the String
		InputStream xml = null;
		try {
			xml = new ByteArrayInputStream(element.toString().getBytes("UTF-8")); //$NON-NLS-1$
		} catch (UnsupportedEncodingException e) {
			// can neglect this, UTF-8 is essential for running the whole Lab
		}

		ObjectType objectType = JAXB.unmarshal(xml, ObjectType.class);

		return getInstance(objectType, isComplete, false);
	}

	/**
	 * Returns the TextGridObject instance representing the existing TextGridRep
	 * object represented by the given metadata. The metadata must contain the
	 * {@linkplain TextgridUri TextGrid URI} or the method will fail.
	 * 
	 * <p>
	 * In every TextGridLab instance, there is at most one instance of
	 * TextGridObject for every TextGridRep object (identified by a unique URI
	 * without fragment identifier).
	 * </p>
	 * 
	 * <p>
	 * This method does never call TGcrud, i.e. no direct net interaction. As
	 * well, it currently does not update the metadata of TextGridObjects
	 * already existing in the registry, if you want this, call
	 * {@link #internalSetMetadataXML(OMElement, boolean)} on the result.
	 * </p>
	 * 
	 * FIXME maybe: if (complete && !existingObject.isComplete())
	 * setMetadata(metadata), setComplete(true); ?
	 * 
	 * @param metadata
	 *            the object's metadata as stored in the repository.
	 *            <strong>Clients must not modify this object (or anything
	 *            referenced from therein)</strong> after they have passed it to
	 *            this factory method.
	 * 
	 * @param complete
	 *            clients may only set this to true if they consider the
	 *            metadata passed in the <code>metadata</code> argument as
	 *            complete (i.e. TextGridRep does not have 'more' metadata for
	 *            the object) and if the said metadata conforms to TextGrid's
	 *            metadata schema. Otherwise, clients should set complete to
	 *            false.
	 * 
	 * @param setMetadata
	 *            used to force a replacing of existent metadata with the
	 *            delivered ObjectType. Should only be used if it is sure, that
	 *            the delivered metadata is newer than the cached one (e.g.
	 *            search result)
	 * 
	 * 
	 * @return the existing instance for the URI in the given metadata
	 * @throws CoreException
	 *             if no {@link URI} has been found or the URI is invalid. This
	 *             exception will have a {@linkplain Throwable#getCause() cause
	 *             field}.
	 * 
	 *             FIXME create specialized exception
	 * @see #isComplete()
	 * @see #internalSetMetadataXML(OMElement, boolean)
	 * @see #reloadMetadata(boolean)
	 */
	public static TextGridObject getInstance(ObjectType metadata,
			boolean complete, boolean setMetadata) throws CoreException {
		Object rawResult = null;
		try {
			URI givenURI = new URI(metadata.getGeneric().getGenerated()
					.getTextgridUri().getValue());

			TextGridObject object = getRegisteredTGO(givenURI);

			if (object == null) {
				object = new TextGridObject(metadata);
				object.setComplete(complete);
				// registry.put(givenURI, object); // object registers itself,
				// see TG-354
			} else
			/* synchronized (object) */{ // causes blocking, TG-409 related
				if (complete && !object.isComplete()
						&& !object.isMetadataDirty()) {
					object.internalSetMetadata(metadata, false); // complete
					// metadata,
					// cf. TG-286
					object.setComplete(complete);
				}
				// TODO
				 else if (setMetadata) {
					 object.internalSetMetadata(metadata, false);
				 }
			}
			if (Activator.isDebugging(Activator.DEBUG_INCOMPLETE)) {
				System.out.println(MessageFormat.format(
						"TGO.getInstance(OMElement, complete={0}) => {1}", //$NON-NLS-1$
						complete, object));
				if (Activator.isDebugging(Activator.DEBUG_INCOMPLETE_VERBOSE))
					System.out.println("  " + metadata.toString()); //$NON-NLS-1$
			}
			return object;

		} catch (URISyntaxException e) {
			throw new CoreException(new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, NLS.bind(
							Messages.TextGridObject_ErrorUriFromMD, rawResult), e));
		}
	}

	/**
	 * Returns the TextGridObject representing the TextGridRep object identified
	 * by the given URI. This may also objects that have not been written yet.
	 * 
	 * This method may interact with the repository and thus be long-running.
	 * 
	 * @param uri
	 *            the object's URI (from the <code>textgrid</code> scheme for
	 *            TextGridRep objects or from the
	 *            <code>textgrid-temporary</code> scheme for objects yet to be
	 *            created). May not be <code>null</code>.
	 * @param readMetadata
	 *            if true, ask TGcrud politely for the complete metadata (this
	 *            may be a long-running operation), otherwise, set
	 *            {@link #isComplete()} to <code>false</code>.
	 * @return the TextGridObject instance.
	 * @throws CrudServiceException
	 *             on errors reading metadata, but only if
	 *             <var>readMetadata</var> is true.
	 * @throws AssertionFailedException
	 *             if the URI is invalid
	 * @see #getInstance(OMElement, boolean)
	 * @see #getInstanceOffline(URI)
	 * @see #reloadMetadata(boolean)
	 */
	public synchronized static TextGridObject getInstance(URI uri,
			boolean readMetadata) throws CrudServiceException {

		// Fix if someone passes in an EFS uri
		if (uri != null) {
			if (ITextGridModelConstants.SCHEMA_EFS.equals(uri.getScheme()))
				uri = computeTextGridURI(uri);
			else if (ITextGridModelConstants.SCHEMA_HANDLE.equals(uri
					.getScheme())) {
				try {
					return getInstanceByHandle(uri, readMetadata);
				} catch (FileNotFoundException e) {
					throw new CrudServiceException(e, uri);
				} catch (CrudServiceException e) {
					throw e;
				} catch (CoreException e) {
					throw new CrudServiceException(e, uri);
				}
			}
		}

		assertURIValidForTGO(uri);

		TextGridObject object = getRegisteredTGO(uri);

		if (object == null) {
			object = new TextGridObject(uri, readMetadata);
			registry.put(uri, object);
		}

		return object;
	}

	/**
	 * Returns a published TextGrid object by the given handle.
	 * 
	 * @param handle
	 *            an URI containing the handle URL of the object
	 * @param readMetadata
	 *            if true, read the metadata if we don't know the object yet
	 * @return the object
	 * @throws FileNotFoundException
	 *             if the object has not been found in the repository.
	 * @throws CoreException
	 *             if something else goes wrong.
	 */
	protected synchronized static TextGridObject getInstanceByHandle(
			final URI handle, boolean readMetadata)
			throws FileNotFoundException, CoreException {
		//Feature #10872
		
		TextGridObject object = handleRegistry.get(handle);
		if (object != null)
			return object;
		

		final String sid = RBACSession.getInstance().getSID(false);
		final String errorString = "Error in TextGridObject.getInstanceByHandle()";

		TGCrudService stub = CrudClientUtilities.getCrudServiceStub();

		MetadataContainerType metadataContainer = null;
		try {
			metadataContainer = stub.readMetadata(sid, logsession.getInstance().getloginfo(),
							handle.toString());
		} catch (ObjectNotFoundFault e) {
			Activator.handleError(e,errorString); //$NON-NLS-1$
		} catch (MetadataParseFault e) {
			Activator.handleError(e,errorString); //$NON-NLS-1$
		} catch (IoFault e) {
			Activator.handleError(e,errorString); //$NON-NLS-1$
		} catch (AuthFault e) {
			Activator.handleError(e,errorString); //$NON-NLS-1$
		}
		
		ObjectType metadata = metadataContainer.getObject();
		object = TextGridObject.getInstance(metadata,
				true, true);

		return object;
	}

	/**
	 * Retrieves the TextGridObject for the given URI without any grid access.
	 * This is essentially the same as
	 * <code>{@linkplain #getInstance(URI, boolean) <i>getInstance</i>(uri, <b>false</b>)}</code>
	 * , but it doesn't declare any exceptions.
	 * 
	 * @param uri
	 *            The TextGridObject's URI
	 * @throws AssertionFailedException
	 *             if the URI {@linkplain #assertURIValidForTGO(URI) is not a
	 *             valid TextGrid URI}.
	 * @return the (probably {@linkplain #isComplete() incomplete})
	 *         TextGridObject for the given URI.
	 */
	public synchronized static TextGridObject getInstanceOffline(URI uri) {
		try {
			return getInstance(uri, false);
		} catch (CrudServiceException e) {
			// this should not happen.
			throw new RuntimeException(
					"Internal Error: TextGridObject.getInstance(URI, false) threw a CrudServiceException, although it is not allowed to. Please file a bug report.", //$NON-NLS-1$
					e);
		}
	}

	/**
	 * Asserts that the given URI might be valid for a TextGridObject. Currently
	 * this checks just that it is not null, of the correct scheme and has a
	 * SSP.
	 * 
	 * @param uri
	 *            the URI to check
	 * @throws AssertionFailedException
	 *             if the preconditions are not met.
	 */
	private static void assertURIValidForTGO(URI uri) {
		Assert.isNotNull(uri, "A TextGridObject's URI may not be null."); //$NON-NLS-1$
		Assert.isTrue(
				// Expanded for #10872
				ITextGridModelConstants.SCHEMA.equals(uri.getScheme())
						|| ITextGridModelConstants.SCHEMA_NEWFILE.equals(uri
								.getScheme()) || ITextGridModelConstants.SCHEMA_HANDLE.equals(uri
										.getScheme()),
				MessageFormat
						.format("A TextGridObject''s URI ({4}) must have the scheme {0} or {1} or {2}, not {3}", //$NON-NLS-1$
								ITextGridModelConstants.SCHEMA,
								ITextGridModelConstants.SCHEMA_NEWFILE,
								ITextGridModelConstants.SCHEMA_HANDLE,
								uri.getScheme(), uri));
		Assert.isTrue(uri.getSchemeSpecificPart() != null
				&& uri.getSchemeSpecificPart().length() > 0,
				"A TextGridObject's URI must contain a scheme specific part."); //$NON-NLS-1$
	}

	/**
	 * Creates and registers a new TextGridObject instance representing a new
	 * TextGridRep object. <strong>Please consider using
	 * {@link #getNewObjectInstance(TextGridProject, TGContentType)}
	 * instead.</strong>
	 * 
	 * A call to this method does not yet write something to the grid. it just
	 * represents this object. You will have to set at least the mandatory
	 * metadata fields before storing this object to TextGridRep.
	 * 
	 * If you need to create contents for this object, please use code like
	 * 
	 * <pre>
	 * &lt;code&gt;
	 * TextGridObject object = TextGridObject.getNewObjectInstance(project,
	 * 		&quot;text/plain&quot;);
	 * IFile file = object.getAdapter(IFile.class);
	 * &lt;/code&gt;
	 * </pre>
	 * 
	 * You may now pass this {@link IFile} to {@link IEditorPart}s etc. Calling
	 * one of the file's <code>setContents</code> methods saves contents
	 * <em>and metadata</em> to the repository, but fails if you have not
	 * provided the mandatory metadata fields.
	 * 
	 * 
	 * @param project
	 *            The id of the project the object lives in.
	 * @param format
	 *            The object's content type.
	 * @return new incomplete and unwritten TextGridObject
	 * @deprecated use
	 *             {@link #getNewObjectInstance(TextGridProject, TGContentType)}
	 *             instead to ensure that the values for project and object are
	 *             valid
	 */
	@Deprecated
	public synchronized static TextGridObject getNewObjectInstance(
			String project, String format) {

		TextGridObject newObject = new TextGridObject(project, format);

		TextGridProjectFile.setBasicMetadataToNewObject(project,
				TextGridProjectFile.getTypeFromFormat(format), newObject);

		registry.put(newObject.getURI(), newObject);

		notifyListeners(Event.CREATED_TEMPORARY, newObject);
		return newObject;
	}

	/**
	 * Creates and registers a new TextGridObject instance representing a new
	 * TextGridRep object.
	 * 
	 * A call to this method does not yet write something to the grid. it just
	 * represents this object. You will have to set at least the mandatory
	 * metadata fields before storing this object to TextGridRep.
	 * 
	 * If you need to create contents for this object, please use code like
	 * 
	 * <pre>
	 * &lt;code&gt;
	 * TextGridObject object = TextGridObject.getNewObjectInstance(project,
	 * 		&quot;text/plain&quot;);
	 * IFile file = object.getAdapter(IFile.class);
	 * &lt;/code&gt;
	 * </pre>
	 * 
	 * You may now pass this {@link IFile} to {@link IEditorPart}s etc. Calling
	 * one of the file's <code>setContents</code> methods saves contents
	 * <em>and metadata</em> to the repository, but fails if you have not
	 * provided the mandatory metadata fields.
	 * 
	 * 
	 * @param project
	 *            The project the object lives in.
	 * @param format
	 *            The object's content type.
	 * @return new incomplete and unwritten TextGridObject
	 */
	public static TextGridObject getNewObjectInstance(TextGridProject project,
			TGContentType contentType) {
		Assert.isNotNull(project, "Each object must live in a project."); //$NON-NLS-1$
		Assert.isNotNull(contentType, "Each object must have a content type."); //$NON-NLS-1$
		return getNewObjectInstance(project.getId(), contentType.getId());
	}

	// // FIXME clean up the following two methods
	// private static OMElement getElementWithName(String elementName,
	// OMElement root) throws JaxenException {
	//
	// AXIOMXPath xpath = new AXIOMXPath(elementName);
	// xpath.addNamespace("tg", TextGridObject.TEXTGRID_METADATA_NAMESPACE);
	// OMElement elem = (OMElement) xpath.selectSingleNode(root);
	//
	// return elem;
	// }

	/**
	 * 
	 * Add a new metadata xml to the object
	 * 
	 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
	 * 
	 * @param object
	 * @param title
	 * @category HACK
	 */
	public static void addDefaultMetaDataXML(TextGridObject object, String title) {
		/*
		 * Autor = Name oder User-ID des aktuellen Benutzers Title =
		 * getProjectInfo().getName(); Datum = jetzt (new Date()); typ =
		 * "internal"
		 */
		try {
			ObjectFactory factory = new ObjectFactory();
			ObjectType objectType = object.getMetadata();
			if (objectType.getCustom() == null)
				objectType.setCustom(factory.createObjectTypeCustom());
			if (objectType.getRelations() == null)
				objectType.setRelations(factory.createRelationType());
			objectType.getGeneric().getProvided().getTitle().add(title);
			object.metadata = objectType;

		} catch (CrudServiceException e) {
			Activator.handleError(e,
					"Error in TextGridObject.addDefaultMetaDataXML()"); //$NON-NLS-1$
		} catch (javax.xml.parsers.FactoryConfigurationError e) {
			Activator.handleError(e,
					"Error in TextGridObject.addDefaultMetaDataXML()"); //$NON-NLS-1$
		}
	}

	private static ObjectType toObjectType(final OMElement element) {
		try {
			// try to work around #16060
			System.setProperty(TransformerFactory.class.getName(), "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); //$NON-NLS-1$
			return JAXB.unmarshal(new ByteArrayInputStream(element.toString()
					.getBytes("UTF-8")), ObjectType.class); //$NON-NLS-1$
		} catch (UnsupportedEncodingException e) {
			Activator.handleError(e, "Error in TextGridObject.toObjectType()"); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Builds a new TextGrid object with metadata that has been initialized to
	 * the given metadata values.
	 * 
	 * @param project
	 *            Target project. Must not be <code>null</code>.
	 * @param contentType
	 *            Target content type. Must not be <code>null</code>.
	 * @param title
	 *            Title. Must not be <code>null</code>
	 * @return a new TextGridObject
	 * 
	 * @category HACK
	 */
	public static TextGridObject getNewObjectInstance(TextGridProject project,
			TGContentType contentType, String title) {

		TextGridObject object = TextGridObject.getNewObjectInstance(project,
				contentType);

		addDefaultMetaDataXML(object, title);

		return object;
	}

	/**
	 * Returns the last modified date from the object's administrative metadata.
	 * 
	 * @return A calendar with the date
	 * @throws CoreException
	 *             on an internal error or invalid metadata.
	 */
	public Calendar getLastModified() throws CoreException {
		if (lastModified == null) {
			GeneratedType generated = metadata.getGeneric().getGenerated();
			if (generated != null) {
				try {
					lastModified = generated.getLastModified()
							.toGregorianCalendar();
				} catch (Exception e) {
					lastModified = LAST_MODIFIED_NOT_AVAILABLE;
					e.printStackTrace();
				}
			}
		}
		if (lastModified == null)
			lastModified = LAST_MODIFIED_NOT_AVAILABLE;
		return lastModified;
	}

	/**
	 * Returns the create date from the object's administrative metadata.
	 * 
	 * @return A calendar with the date
	 * @throws CoreException
	 *             on an internal error or invalid metadata.
	 */
	public Calendar getCreated() throws CoreException {
		if (created == null) {
			GeneratedType generated = metadata.getGeneric().getGenerated();
			if (generated != null)
				created = generated.getCreated().toGregorianCalendar();
		}
		return created;
	}

	// /**
	// * Returns the string result for the first node selected by the given
	// XPath
	// * expression. The prefix <code>tg</code> is bound to the metadata
	// * namespace.
	// *
	// * This is a convenience method for
	// * {@linkplain #getStringValue(String, boolean)
	// * getStringValue(<var>xpathString</xpathString>, true)} that must not be
	// * called with root expressions and might cause network access: Please see
	// * the notes at {@link #getStringValue(String, boolean)}.
	// *
	// * @param xpathString
	// * an XPath expression to be evaluated in the context of the
	// * current metadata element
	// * @return the string value
	// * @throws CoreException
	// * , potentially with an embedded JaxenException
	// * @see #getStringValue(String, boolean)
	// * @deprecated use {@link #getMetadata()} instead
	// */
	// public String getStringValue(String xpathString) throws CoreException {
	// return getStringValue(xpathString, true);
	// }

	// /**
	// * Returns the string result for the first node selected by the given
	// XPath
	// * expression. The prefix <code>tg</code> is bound to the metadata
	// * namespace.
	// *
	// * <p>
	// * Clients <strong>SHOULD</strong> use one of the more specific methods
	// like
	// * {@link #getTitle()} if available. TextGrid implementors should consider
	// * adding methods like {@link #getTitle()} for their properties as needed
	// * instead of directly calling {@link #getStringValue(String, boolean)}.
	// * </p>
	// *
	// * <p>
	// * Clients <strong>MUST NOT</strong> absolute path expressions, or queries
	// * that contain absolute path expressions. In some situations the
	// * </p>
	// *
	// * <p>
	// * Note that this method <strong>might cause network access</strong>: It
	// * will {@linkplain #reloadMetadata(boolean) reload} the object's metadata
	// * from TG-crud if (and only if) all of the following conditions hold:
	// * <ul>
	// * <li>the <var>reload</var> parameter is <code>true</code>,</li>
	// * <li>the object {@linkplain #isComplete() is not yet complete}, i.e. it
	// * may have stored only partial metadata, and</li>
	// * <li>the XPath expression given evaluates to an empty sequence.</li>
	// * </ul>
	// *
	// * </p>
	// *
	// * @param xpathString
	// * a relative(!) XPath expression to be evaluated in the context
	// * of the current metadata element
	// * @param reload
	// * whether to reload the metadata if the test fails on an
	// * incomplete object.
	// * @return the string value
	// * @throws CoreException
	// * , potentially with an embedded JaxenException
	// * @deprecated use {@link #getMetadata()} instead
	// */
	// public String getStringValue(String xpathString, boolean reload)
	// throws CoreException {
	//
	// String result;
	// synchronized (this) {
	// result = getStringValue(xpathString, rootElement);
	// }
	//
	// if (reload && (result == null || "".equals(result)) && !isComplete()) {
	// reloadMetadata(false);
	// synchronized (this) {
	// result = getStringValue(xpathString, rootElement);
	// }
	// }
	//
	// return result;
	// }

	// /**
	// * Helper function returning the string value of the first node matching
	// the
	// * given xpath query (with <code>tg</code> bound to the metadata
	// namespace)
	// * in the XML fragment rooted at the given rootElement.
	// *
	// * @see #getStringValue(String, boolean)
	// * @return the string value of the first matching node, or
	// <code>null</code>
	// * if nothing matches.
	// *
	// */
	// protected static String getStringValue(String xpathString,
	// OMElement rootElement) throws CoreException {
	// String resultString = "";
	// try {
	// AXIOMXPath xpath = new AXIOMXPath("string(" + xpathString + ")");
	// xpath.addNamespace("tg", TEXTGRID_METADATA_NAMESPACE);
	// xpath.addNamespace("tgd", TEXTGRID_DATERANGE_NAMESPACE);
	// Object rawResult = xpath.selectSingleNode(rootElement);
	// if (rawResult == null)
	// return null;
	// if (rawResult instanceof String)
	// resultString = (String) rawResult;
	// else if (rawResult instanceof OMText) {
	// resultString = ((OMText) rawResult).getText();
	// } else
	// throw new CoreException(
	// new Status(
	// IStatus.ERROR,
	// Activator.PLUGIN_ID,
	// NLS
	// .bind(
	// "AXIOM's XPath implementation delivered a {0} instead of a String!?",
	// rawResult.getClass()
	// .getCanonicalName())));
	// } catch (JaxenException e) {
	// throw new CoreException(new Status(IStatus.ERROR,
	// Activator.PLUGIN_ID, NLS.bind(
	// "Error retrieving {2} from {0}: {1}", new Object[] {
	// rootElement, e, xpathString }), e));
	// }
	// return resultString;
	// }

	/**
	 * Extract the object's size (in bytes) from the metadata.
	 * 
	 * @return the object's size from the metadata, if known.
	 * @throws CoreException
	 */
	public synchronized long getSize() throws CoreException {
		if (size < 0) {
			GeneratedType generated = getMetadataForReading().getGeneric()
					.getGenerated();
			if (generated != null)
				size = generated.getExtent().longValue();
		}
		return size;
	}

	/**
	 * Serializes the object's metadata to an XML string.
	 * 
	 * @return a string containing an XML representation of the object's
	 *         metadata.
	 * @throws DataBindingException
	 *             if something goes wrong while serializing (which shouldn't).
	 */
	public synchronized String serialize() throws DataBindingException {
		StringWriter writer = new StringWriter();
		System.setProperty(TransformerFactory.class.getName(), "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); //$NON-NLS-1$
		JAXB.marshal(metadata, writer);

		// ObjectFactory factory = new ObjectFactory();
		// JAXBElement<ObjectType> object = factory.createObject(metadata);
		// JAXBContext context;
		// try {
		// context = JAXBContext.newInstance(ObjectType.class);
		// Marshaller marshaller = context.createMarshaller();
		// marshaller.marshal(object, writer);
		return writer.toString();
		// } catch (JAXBException e) {
		// final String message =
		// NLS.bind("Could not serialize metadata for {0}", this);
		// StatusManager.getManager().handle(new Status(IStatus.ERROR,
		// Activator.PLUGIN_ID, message, e));
		// throw new DataBindingException(message, e);
		// }
	}

	/**
	 * Returns the object's project from the metadata.
	 * 
	 * @throws CoreException
	 */
	public synchronized String getProject() throws CoreException {
		if (project == null || "".equals(project)) { //$NON-NLS-1$
			project = getMetadataForReading().getGeneric().getGenerated()
					.getProject().getId();
		}
		return project;
	}

	/**
	 * Returns the object's owner from the metadata.
	 * 
	 * @throws CoreException
	 */
	public synchronized String getDataContributor() throws CoreException {
		return getMetadata().getGeneric().getGenerated().getDataContributor();
	}

	/**
	 * Returns the object's CRUD-Warning from the metadata.
	 * 
	 * @throws CoreException
	 *             FIXME there can be several warnings. We just join them here,
	 *             but that's probably not optimal.
	 */
	public synchronized String getCrudWarning() throws CoreException {
		String warnings = ""; //$NON-NLS-1$
		try {
			for (Warning warning : getMetadataForReading().getGeneric()
					.getGenerated().getWarning()) {
				if (!warnings.equals("")) //$NON-NLS-1$
					warnings = warnings + "\n"; //$NON-NLS-1$
				warnings = warnings + warning.getValue();
			}
		} catch (Exception e) {
			// no warning existent
		}
		return warnings;
	}

	/**
	 * @return this object's project.
	 * @throws CoreException
	 *             // FIXME subject to refactoring (Exceptions, Name!?)
	 */
	public TextGridProject getProjectInstance() throws CoreException {
		try {
			return TextGridProject.getProjectInstance(getProject());
		} catch (RemoteException e) {
			// TODO Improve error handling.
			throw new CoreException(
					new Status(
							IStatus.ERROR,
							PLUGIN_ID,
							MessageFormat
									.format(Messages.TextGridObject_ErrorProjectID,
											getProject(), this, e.getMessage())));
		} catch (ProjectDoesNotExistException e) {
			throw new CoreException(
					new Status(
							IStatus.ERROR,
							PLUGIN_ID,
							MessageFormat
									.format(Messages.TextGridObject_ReferencedProjectMissing,
											this, getProject())));
		}
	}

	public String getContentTypeID() throws CoreException {
		synchronized (this) {
			if (contentTypeID == null || "".equals(contentTypeID)) { //$NON-NLS-1$
				contentTypeID = getMetadataForReading().getGeneric()
						.getProvided().getFormat();
			}
			return contentTypeID;
		}
	}

	/**
	 * Returns the name under which the file is represented in the local file
	 * system, without potential numerical suffix.
	 * 
	 * This name candidate is usually created from the object's title, and if
	 * there are multiple objects with the same title and content type, it will
	 * not be unique. However, it's rather presentable to the users.
	 * 
	 * @throws CoreException
	 *             if one of the underlying getters fails
	 */
	public String getNameCandidate() throws CoreException {
		if (ITextGridModelConstants.SCHEMA_NEWFILE.equalsIgnoreCase(getURI()
				.getScheme())) {
			return new Path(getURI().getPath()).lastSegment().toString();
			// return "Unnamed-" + uri.getSchemeSpecificPart();
		}
		String title = getTitle();
		if (Integer.valueOf(getRevisionNumber()) > 0)
			title = MessageFormat.format("{0} r{1}", title, getRevisionNumber());
		return title.replace('/', '-');
	}

	/**
	 * Creates a local file name based on the object's URI. Does not contain any
	 * path component.
	 * 
	 * This name candidate does not look very good, but there's a good chance
	 * that it is unique for the object without further processing. The name
	 * will have a file name extension if
	 * {@linkplain TGContentType#getExtension() specified} by the objects
	 * {@linkplain #getContentType(boolean) content type}.
	 * 
	 * @return a file name, probably with extension, without directory
	 *         component.
	 * @throws CoreException
	 *             if determining the content type failed.
	 */
	public String getURIBasedName() throws CoreException {
		// care for windows paths and % translation
		String localFileName = getURI().toASCIIString();
		localFileName = getNameCandidate() + "." //$NON-NLS-1$
				+ localFileName.substring(getURI().getScheme().length() + 1);

		localFileName = localFileName.replaceAll("[%:/\\*?\"<>|]", "_"); //$NON-NLS-1$ //$NON-NLS-2$
		TGContentType contentType = getContentType(false);
		if (contentType != null)
			localFileName = localFileName + "." + contentType.getExtension(); //$NON-NLS-1$

		return localFileName;
	}

	/**
	 * Returns the object's first title field.
	 * 
	 * FIXME returns {@link #NO_TITLE} when no title present -- throwing core
	 * exceptions causes too much trouble currently.
	 * 
	 * @throws CoreException
	 *             FIXME e.g. if the field does not exist
	 */
	public synchronized String getTitle() throws CoreException {
		if ((title == null || "".equals(title)) && metadata != null //$NON-NLS-1$
				&& metadata.getGeneric() != null
				&& metadata.getGeneric().getProvided() != null) {
			List<String> titles = metadata.getGeneric().getProvided()
					.getTitle();
			if (titles != null && titles.size() > 0)
				title = titles.get(0);
			else
				// FIXME when I throw a CoreException as documented I get a
				// large number of error messages in the Navigator.
				// throw new CoreException(new Status(IStatus.ERROR,
				// Activator.PLUGIN_ID, NLS.bind(
				// "All objects should have at least one title, but {0} has none.",
				// toString())));
				return NO_TITLE;
		}
		return title;
	}

	/*
	 * @MH 2017-12-13
	 * #22826 returns true if title doesn't exist or is empty
	 * this makes it easier and more consistens elsewhere
	 */
	public boolean hasEmptyTitle() throws CoreException{
		
		String tmpTitle = this.getTitle();

		if (tmpTitle != null
				&& tmpTitle != NO_TITLE
				&& !("".equals(tmpTitle))
			){
			return false;
		}

		return true;
	}

	/**
	 * Directly sets this object's (first) title field. Doesn't make anything
	 * persistent, and works directly on the metadata record.
	 * 
	 * @param value
	 *            the new title.
	 */
	public synchronized void setTitle(final String value) {
		List<String> titles = metadata.getGeneric().getProvided().getTitle();
		if (titles.size() == 0)
			titles.add(value);
		else
			titles.set(0, value);

		title = value;

		TextGridObject.notifyListeners(Event.METADATA_CHANGED, this);
	}

	/**
	 * Returns a list of authors of this object for display purposes.
	 * 
	 * <em>Note that this doesn't reload the metadata on incomplete objects!</em>
	 * 
	 * The list that is returned will be Immutable.
	 * 
	 * @throws CoreException
	 *             with an embedded {@link JaxenException} if extracting the
	 *             authors failed
	 */
	public synchronized List<String> getAuthors() throws CoreException {

		final Function<AgentType, String> agentToString = new Function<AgentType, String>() {

			public String apply(AgentType agent) {
				return agent.getValue();
			}

		};

		// filters authors from agents
		final Predicate<AgentType> authorsOnly = new Predicate<AgentType>() {

			public boolean apply(AgentType input) {
				return input.getRole().equals(AgentRoleType.AUTHOR);
			}
		};

		Iterable<String> authors = null;
		if (metadata.getItem() != null)
			authors = ImmutableList.of(MessageFormat.format(
					Messages.TextGridObject_dataContributorAsAuthor, isNew() ? RBACSession
							.getInstance().getPerson().getValue()
							: getDataContributor()));
		// // or use the rightsHolder?
		// authors = transform(metadata.getItem().getRightsHolder(), new
		// Function<PersonType, String>() {
		//
		// public String apply(PersonType person) {
		// return NLS.bind("{0} (rights holder)", person.getValue());
		// }
		//
		// });
		else if (metadata.getEdition() != null)
			authors = transform(
					filter(metadata.getEdition().getAgent(), authorsOnly),
					agentToString);
		else if (metadata.getWork() != null)
			authors = transform(
					filter(metadata.getWork().getAgent(), authorsOnly),
					agentToString);

		if (authors != null)
			return ImmutableList.copyOf(authors);
		else if (metadata.getGeneric().getGenerated() != null)
			return ImmutableList.of(metadata.getGeneric().getGenerated()
					.getDataContributor());
		else
			return ImmutableList.of(Messages.TextGridObject_you);
	}

	/**
	 * Returns the URI identifying this object.
	 */
	public synchronized URI getURI() {
		return uri;
	}

	private synchronized void setURI(final URI uri) {
		if (this.uri != null
				&& this.uri.getScheme().equals(ITextGridModelConstants.SCHEMA)
				&& !uri.getSchemeSpecificPart().startsWith(
						uri.getSchemeSpecificPart()))
			throw new IllegalArgumentException(NLS.bind(
					"Tried to change {0}''s URI to {1} -- this is forbidden.", //$NON-NLS-1$
					this.uri, uri));

		this.uri = uri;
	}

	/**
	 * Returns the latest URI identifying this object as string.
	 */
	public String getLatestURI() {
		try {
			return getURI().toString().split("\\.")[0]; //$NON-NLS-1$
		} catch (ArrayIndexOutOfBoundsException e) {
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * Returns the revision number of this object as string. The number is
	 * identified out of the objects uri.
	 */
	public String getRevisionNumber() {
		try {
			return getURI().toString().split("\\.")[1]; //$NON-NLS-1$
		} catch (ArrayIndexOutOfBoundsException e) {
			return ""; //$NON-NLS-1$
		}
	}

	public URI getEFSURI() {
		return efsUri;
	}

	public URI getPreparedURI() {
		if (isNew())
			return preparedURI;
		else
			return getURI();
	}

	/**
	 * Fetches new URIs for all objects passed in that don't already have URIs.
	 * 
	 * @param newObjects
	 *            the new objects for which to fetch URIs.
	 * @param monitor
	 *            progress reporting
	 * @throws CoreException
	 *             if something goes wrong (typically CRUD access problems)
	 * @see #getPreparedURI()
	 */
	public static void fetchURIs(final Iterable<TextGridObject> newObjects,
			final IProgressMonitor monitor) throws CoreException {
		SubMonitor progress = SubMonitor.convert(monitor, Messages.TextGridObject_RetrievingURIs, 9);

		// only fetch URIs for objects that really need one
		final LinkedList<TextGridObject> objects = Lists.newLinkedList();
		int number = 0;
		for (TextGridObject object : newObjects) {
			if (object.isNew() && object.getPreparedURI() == null) {
				objects.add(object);
				number++;
			}
		}
		progress.worked(1);

		if (number == 0) {
			progress.done();
			return; // nothing to do
		}

		TGCrudService crudServiceStub = CrudClientUtilities
				.getCrudServiceStub();
		progress.worked(2);

		List<String> uris;
		try {
			// we can just pass in any project, URIs are valid for all projects.
			uris = crudServiceStub.getUri(
					RBACSession.getInstance().getSID(false), logsession
							.getInstance().getloginfo(), objects.get(0)
							.getProject(), number);
		} catch (AuthFault e) {
			throw new CrudServiceException(e, null);
		} catch (IoFault e) {
			throw new CrudServiceException(e, null);
		} catch (ObjectNotFoundFault e) {
			throw new CrudServiceException(e, null);
		}
		progress.worked(5);

		synchronized (TextGridObject.class) {
			Iterator<String> iterator = uris.iterator();
			for (TextGridObject object : objects) {
				object.preparedURI = URI.create(iterator.next());
				registry.put(object.preparedURI, object);
			}
		}

		progress.worked(1);
		progress.done();
	}

	// /**
	// * Creates an ADB Metadata object by allocating the corresponding
	// * {@link TextGridObject}, serializing it and letting the ADB stub
	// re-parse
	// * the result.
	// *
	// * The result <em>does</em> contain the
	// * <code>administrative/middleware</code> part.
	// *
	// * @return the ADB {@link TgObjectMetadata}
	// * @throws CoreException
	// * if something fails (see the cause)
	// */
	// @SuppressWarnings("unchecked")
	// public TgObjectMetadata getADBMetadata() throws CoreException {
	//
	// if (isTitleEmpty() || isIdnoTypeEmpty()) {
	// throw new CoreException(
	// new Status(
	// IStatus.ERROR,
	// Activator.PLUGIN_ID,
	// "An error occured while trying to prepare the metadata object. The Title or idNo->Type field is empty!"));
	// }
	// // FIXME getAdapter()
	// XMLStreamReader reader;
	// try {
	//
	// OMElement outputElement = rootElement.cloneOMElement();
	// // Iterator children = outputElement.getFirstChildWithName(
	// // ADMINISTRATIVE_NAME).getChildren();
	// // while (children.hasNext()) {
	// // OMNode child = (OMNode) children.next();
	// // if (child instanceof OMElement
	// // && ((OMElement) child).getQName().equals(
	// // MIDDLEWARE_NAME))
	// // children.remove();
	// // }
	//
	// String metadataObjectNS = TgObjectMetadata.MY_QNAME
	// .getNamespaceURI();
	//
	// // the root element may either be in the CRUD or in the metadata
	// // namespace ;-(
	// if (!outputElement.getNamespace().getNamespaceURI().equals(
	// metadataObjectNS)) {
	// OMFactory factory = OMAbstractFactory.getOMFactory();
	// OMElement mdElement = factory
	// .createOMElement(TgObjectMetadata.MY_QNAME);
	// Iterator children = outputElement.getChildren();
	// while (children.hasNext()) {
	// mdElement.addChild((OMNode) children.next());
	// }
	//
	// outputElement = mdElement;
	//
	// }
	//
	// String serializedOutput = serialize(outputElement);
	// reader = XMLInputFactory.newInstance().createXMLStreamReader(
	// new StringReader(serializedOutput));
	//
	// return TgObjectMetadata.Factory.parse(reader);
	// } catch (Exception e) {
	// Throwable rootCause = e;
	// while (rootCause.getCause() != null)
	// rootCause = rootCause.getCause();
	// throw new CoreException(
	// new Status(
	// IStatus.ERROR,
	// Activator.PLUGIN_ID,
	// MessageFormat
	// .format(
	// "An {0} occured while trying to prepare the metadata object for {1}.\nRoot cause was an {2}: {3}",
	// e.getClass().getName(), uri,
	// rootCause.getClass()
	// .getSimpleName(), rootCause
	// .getMessage()), e));
	// }
	// }

	/**
	 * This method is used in two cases: (1) when a new object is created with
	 * TGcrud, (2) when a new version of an object is created.
	 * 
	 * This method IS NOT API. It may only be called from inside the TGcrud EFS
	 * implementation, or unpredictable things may happen.
	 * 
	 * The workspace part of moving stuff is done by a separate workspace job
	 * and is <em>not</em> neccessarily complete when this method returns.
	 * 
	 * @param newMetadata
	 * @throws CoreException
	 */
	public synchronized void move(final ObjectType newMetadata)
			throws CoreException {
		Assert.isNotNull(newMetadata, "move does not accept a null argument."); //$NON-NLS-1$

		final IFile file = (IFile) TextGridObject.this.getAdapter(IFile.class);
		if (file == null) {
			throw new CoreException(
					new Status(
							IStatus.ERROR,
							Activator.PLUGIN_ID,
							NLS.bind(
									"Could not find IFile for {0}, Please file a bug report.", //$NON-NLS-1$
									TextGridObject.this)));
		}

		final URI oldURI = getURI();
		try {
			URI newURI = new URI(newMetadata.getGeneric().getGenerated()
					.getTextgridUri().getValue());

			assertURIValidForTGO(newURI);
			setURI(newURI);
			efsUri = computeEFSURI(newURI);
			// register the new object
			registry.put(getURI(), TextGridObject.this);

		} catch (URISyntaxException e) {
			throw new CoreException(
					new Status(
							IStatus.ERROR,
							Activator.PLUGIN_ID,
							NLS.bind(
									"Could not extract URI from the new metadata block: \n{0}\n ... for the object with the temporary uri {1}", //$NON-NLS-1$
									newMetadata, getURI()), e));
		}
		final IFile destination = ModelAdaptorFactory.getFileFor(
				TextGridObject.this, true); // we don't want a link there
		// (target will never exist)

		internalSetMetadata(newMetadata, false);
		setComplete(true);

		IResourceRuleFactory ruleFactory = ResourcesPlugin.getWorkspace()
				.getRuleFactory();
		ISchedulingRule moveJobRule = MultiRule.combine(
				ruleFactory.moveRule(file, destination),
				ruleFactory.createRule(destination));

		IndexedWorkspaceJob moveJob = new IndexedWorkspaceJob(NLS.bind(
				Messages.TextGridObject_MovingOnCreate, this)) {

			@Override
			public boolean belongsTo(Object family) {
				return super.belongsTo(family) || this.equals(family);
			}

			@Override
			public IStatus runInWorkspace(IProgressMonitor monitor)
					throws CoreException {

				try {
					SubMonitor progress = SubMonitor.convert(monitor,
							getName(), 100);
					progress.worked(10);

					invalidateCache();
					progress.worked(10);

					// someone confused us with #setMetadata(OMElement).
					// Robuschdness Brinzibbl
					if (getURI().equals(oldURI))
						return Status.OK_STATUS; // nothing more to do.

					if (Activator.getDefault().isDebugging())
						System.out.println("TextGridObject#move from " + oldURI //$NON-NLS-1$
								+ " to " + getURI()); //$NON-NLS-1$

					registry.remove(oldURI);

					progress.worked(10);

					file.createLink(efsUri, IResource.REPLACE,
							progress.newChild(35));

					if (destination.exists())
						destination.delete(IResource.FORCE,
								progress.newChild(5));
					else
						progress.worked(5);
					file.move(destination.getFullPath(), IResource.FORCE
							| IResource.SHALLOW, progress.newChild(30));

				} catch (CoreException e) {
					return new CoreExceptionStatus(Activator.PLUGIN_ID, e);
				} finally {
					if (monitor != null)
						monitor.done();
				}

				return Status.OK_STATUS;
			}

		};

		moveJob.setFamilyName(MOVE_TGOBJECT);
		moveJob.setRule(moveJobRule);
		moveJob.schedule();
	}

	/**
	 * Convert the passed in TextGrid URI into an hierarchical URI for EFS
	 * internal use.
	 * 
	 * Note that
	 * <code>computeTextGridURI(computeEFSURI(uri)).equals(uri) == true<code>
	 * for all valid URIs uri.
	 * 
	 * @param textGridURI
	 * @return a hierarchical URI.
	 */
	public static URI computeEFSURI(URI textGridURI) {
		URI efs_uri = null;
		if (ITextGridModelConstants.SCHEMA.equals(textGridURI.getScheme())) {
			String efs_uri_string = ITextGridModelConstants.SCHEMA_EFS + "://" //$NON-NLS-1$
					+ ITextGridModelConstants.AUTHORITY_EFS + "/" //$NON-NLS-1$
					+ textGridURI.toString();
			try {
				efs_uri = new URI(efs_uri_string);
			} catch (URISyntaxException e) {
				handleError(e,
						"Subtle EFS URI creation error. Please file a bug report."); //$NON-NLS-1$
			}
		} else {
			efs_uri = textGridURI;
		}
		return efs_uri;
	}

	/**
	 * retrieve the TextGrid URI from the passed in EFS URI.
	 * 
	 * @param efsURI
	 * @return The non-hierarchical textGrid URI stored in efsURI.
	 */
	public static URI computeTextGridURI(URI efsURI) {
		URI textgrid_uri = null;
		if (efsURI == null) // happens on local files. These can't be textgrid
			// objects.
			return null;
		if (ITextGridModelConstants.SCHEMA_EFS.equals(efsURI.getScheme())) {
			String uri_string = efsURI.getRawPath();
			String query = efsURI.getRawQuery();
			if (query != null) {
				uri_string = uri_string + "?" + query; //$NON-NLS-1$
			}
			String fragment = efsURI.getRawFragment();
			if (fragment != null) {
				uri_string = uri_string + "#" + fragment; //$NON-NLS-1$
			}
			uri_string = uri_string.replaceFirst("^/*", ""); //$NON-NLS-1$ //$NON-NLS-2$
			try {
				textgrid_uri = new URI(uri_string);
			} catch (URISyntaxException e) {
				handleError(e,
						"Subtle TextGrid URI creation error. Please file a bug report."); //$NON-NLS-1$
			}
		} else {
			textgrid_uri = efsURI;
		}
		return textgrid_uri;

	}

	/**
	 * Wait for all workspace jobs that have been created for this object.
	 * 
	 * <strong>Warning: This may cause deadlocks.</strong>
	 * 
	 * @param monitor
	 *            Progress monitor for reporting progress on how the wait is
	 *            progressing, or null if no progress monitoring is required.
	 * @throws OperationCanceledException
	 *             if the progress monitor is canceled while waiting
	 * @throws InterruptedException
	 *             if this thread is interrupted while waiting
	 * @see IJobManager#join(Object, IProgressMonitor)
	 */
	public void joinWorkspaceJobs(IProgressMonitor monitor)
			throws OperationCanceledException, InterruptedException {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		IJobManager workspaceJobManager = WorkspaceJob.getJobManager();
		workspaceJobManager.join(this, monitor);
	}

	/**
	 * Deletes this object (and the corresponding resource, if applicable)
	 * 
	 * @param monitor
	 *            the progress monitor to use for reporting progress to the
	 *            user. It is the caller's responsibility to call done() on the
	 *            given monitor. Accepts null, indicating that no progress
	 *            should be reported and that the operation cannot be cancelled.
	 * 
	 * @throws CoreException
	 *             if something goes wrong
	 */
	public void delete(IProgressMonitor monitor) throws CoreException {
		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridObject_DeletingX, this), 100);

		IFile file = (IFile) getAdapter(IFile.class);
		progress.worked(25);
		if (progress.isCanceled())
			return;
		if (file != null) {
			file.delete(true, progress.newChild(25));
		} else
			progress.worked(25); // that was easy :)
		/*
		 * Deleting a lined file only deletes the link -> we need to delete the
		 * file itself manually
		 */
		IFileStore fileStore = EFS.getStore(getEFSURI());
		progress.worked(10);
		if (progress.isCanceled())
			return;
		if (fileStore != null)
			fileStore.delete(EFS.NONE, progress.newChild(40));
		else
			progress.worked(40);

		deleted = true;

		// Unregister the Projectfile if the deleted object is a project file
		TextGridProject p = this.getProjectInstance();
		String pfUri = p.getProjectfile();
		if (pfUri != null && !pfUri.equals("") //$NON-NLS-1$
				&& pfUri.equals(this.getURI().toString())) {
			try {
				p.unsetProjectFile();
				System.err.println("unsetProjectFile() called"); //$NON-NLS-1$
			} catch (OfflineException e) {
				Activator.handleError(e, "Error TextGridObject.delete()"); //$NON-NLS-1$
			} catch (RemoteException e) {
				Activator.handleError(e, "Error TextGridObject.delete()"); //$NON-NLS-1$
			} catch (AuthenticationFault e) {
				Activator.handleError(e, "Error TextGridObject.delete()"); //$NON-NLS-1$
			}
		}

		// FIXME do we need to tell someone that we ceased to ripple this
		// universe's time-space continuum?
		notifyListeners(Event.DELETED, this);

	}

	/**
	 * A listener that is called when something happens with an object. You may
	 * add your listeners with
	 * {@link TextGridObject#addListener(ITextGridObjectListener)} and remove
	 * them using {@link TextGridObject#removeListener(ITextGridObjectListener)}
	 * .
	 */
	public interface ITextGridObjectListener {

		public enum Event {
			/**
			 * Deleted events are fired when the object has been deleted
			 */
			DELETED,
			/**
			 * When {@link TextGridObject#getNewObjectInstance(String, String)}
			 * has been called:
			 */
			CREATED_TEMPORARY,
			/**
			 * When a file created by
			 * {@link TextGridObject#getNewObjectInstance(String, String)} has
			 * been written to the TextGridRep for the first time (i.e. made
			 * persistent)
			 */
			CREATED,
			/**
			 * When an object's metadata have been changed (from inside or
			 * outside the metadata editor)
			 */
			METADATA_CHANGED,
			/**
			 * When an object's metadata have been changed from outside the
			 * metadata editor (HACK)
			 */
			METADATA_CHANGED_FROM_OUTSIDE,
			/**
			 * When an object's content has been made persistent
			 */
			SAVED,
			/**
			 * When an object's metadata has been made persistent
			 */
			METADATA_SAVED,
			/**
			 * metadata incomplete
			 */
			METADATA_INCOMPLETE,
		};

		/**
		 * The given <var>event</var> has happend to the given
		 * <var>object</var>.
		 */
		public void textGridObjectChanged(Event event, TextGridObject object);

	}

	private static ListenerList listeners = new ListenerList();

	/**
	 * Adds a listener for object change events.
	 * 
	 * @param listener
	 */
	public static void addListener(ITextGridObjectListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes a listener for object change events.
	 * 
	 * @param listener
	 */
	public static void removeListener(ITextGridObjectListener listener) {
		listeners.remove(listener);
	}

	/**
	 * <strong>THIS IS NOT API</strong>, it may only be called from the model or
	 * EFS implementation.
	 * 
	 * @param event
	 * @param textGridObject
	 */
	public static void notifyListeners(
			final ITextGridObjectListener.Event event,
			final TextGridObject textGridObject) {

		UIJob job = new UIJob(Messages.TextGridObject_notifying) {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				for (Object listener : listeners.getListeners())
					((ITextGridObjectListener) listener).textGridObjectChanged(
							event, textGridObject);
				return Status.OK_STATUS;
			}
		};
		job.setSystem(true);
		job.schedule();
	}

	/**
	 * Returns the content type of this object.
	 * 
	 * @param exact
	 *            if true, return only content types exactly matching the
	 *            content type string. Otherwise, if the Lab has not registered
	 *            a content type with the object's content type ID, tries to
	 *            match a content type object.
	 * @return The content type object. May return null on unknown content
	 *         types.
	 * @throws CoreException
	 *             When reading the object's metadata fails.
	 */
	public TGContentType getContentType(boolean exact) throws CoreException {
		if (exact)
			return TGContentType.getContentType(getContentTypeID());
		else
			return TGContentType.findMatchingContentType(getContentTypeID());
	}

	/**
	 * Returns the exactly matching content type object.
	 * 
	 * @deprecated use {@link #getContentType(boolean)} instead.
	 * @throws CoreException
	 */
	@Deprecated
	public TGContentType getContentType() throws CoreException {
		return getContentType(true);
	}

	/**
	 * Sets the initial content of a {@linkplain #isNew() new} TextGridObject.
	 * 
	 * <p>
	 * The <em>initial contents</em> feature is intended for situations where
	 * you want a new TextGridObject to have some content available, but do not
	 * want to save the object to the Grid. If you'd use the regular way of
	 * setting content, i.e.
	 * </p>
	 * 
	 * <pre>
	 *  IFile file = (IFile) textGridObject.getAdapter(IFile.&lt;b&gt;class&lt;/b&gt;);
	 *  &lt;b&gt;if&lt;/b&gt; (file != &lt;b&gt;null&lt;/b&gt;)
	 *    file.setContent(inputStream);
	 * </pre>
	 * <p>
	 * you would cause an attempt to save the file to the grid (which will fail
	 * if, e.g., the metadata isn't valid yet). OTOH, you might want to pass
	 * some content to arbtrary editors, anyway.
	 * </p>
	 * <p>
	 * The contents of this file will be returned by code like
	 * </p>
	 * 
	 * <pre>
	 *  IFile file = (IFile) textGridObject.getAdapter(IFile.&lt;b&gt;class&lt;/b&gt;);
	 *  &lt;b&gt;if&lt;/b&gt; (file != &lt;b&gt;null&lt;/b&gt;)
	 *    outputStream = file.getContent();
	 * </pre>
	 * <p>
	 * so it will be available in editors etc.
	 * </p>
	 * 
	 * @param initialContents
	 *            The initial contents.
	 * @throws IllegalStateException
	 *             When someone tries to set intial contents of a non-
	 *             {@linkplain #isNew() new} object.
	 */
	public void setInitialContent(byte[] initialContents)
			throws IllegalStateException {

		if (isNew() || initialContents == null)
			this.initialContent = initialContents;
		else
			throw new IllegalStateException(
					"Cannot set initial contents for objects that have been materalized."); //$NON-NLS-1$
	}

	/**
	 * Read the initial contents for a TextGridObject from a stream.
	 * 
	 * @param contentStream
	 *            The stream holding the initial contents.
	 * @param monitor
	 *            the progress monitor to use for reporting progress to the
	 *            user. It is the caller's responsibility to call done() on the
	 *            given monitor. Accepts null, indicating that no progress
	 *            should be reported and that the operation cannot be cancelled.
	 * @throws IOException
	 *             on errors while reading from the <var>contentStream</var>
	 * @throws IllegalStateException
	 *             When someone tries to set intial contents of a non-
	 *             {@linkplain #isNew() new} object.
	 * @see #setInitialContent(byte[])
	 */
	public void setInitialContent(InputStream contentStream,
			IProgressMonitor monitor) throws IOException {
		SubMonitor progress = SubMonitor.convert(monitor,
				IProgressMonitor.UNKNOWN);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];

		int bytesRead;
		while ((bytesRead = contentStream.read(buffer)) > -1
				&& !progress.isCanceled()) {
			baos.write(buffer, 0, bytesRead);
		}
		setInitialContent(baos.toByteArray());
	}

	/**
	 * Returns the initial content of new files, if available, or null. The
	 * initial contents will be reset to null when the object is saved to the
	 * grid.
	 */
	public byte[] getInitialContent() {
		return initialContent;
	}

	/**
	 * Sets the <var>hasSchema</var> relation of this object to <var>uri</var>,
	 * i.e. <var>uri</var> is the uri to a schema for this object.
	 */
	public synchronized void setSchema(URI uri) {
		getRelations().setHasSchema(uri.toString());
		setMetadataDirty(true);
	}

	private RelationType getRelations() {
		RelationType relations = metadata.getRelations();
		// TODO handle incomplete objects:
		// if (relations == null && !isComplete())
		// reloadMetadata(false);
		if (relations == null) {
			relations = new ObjectFactory().createRelationType();
			metadata.setRelations(relations);
		}
		return relations;
	}

	/**
	 * Sets the <var>hasAdaptor</var> relation to this object to <var>uri</var>.
	 * An Adaptor is an XSLT stylesheet that transforms this object's contents
	 * to the TextGrid Baseline Encoding.
	 * 
	 * @param uri
	 *            the URI to the Adaptor of this object.
	 */
	public synchronized void setAdaptor(URI uri) {
		getRelations().setHasAdaptor(uri.toString());
	}

	/**
	 * Sets / adds the <em>isVersionOf</em> relation to this object.
	 * 
	 * @param uri
	 *            the URI of the object of which this object is a version.
	 */
	@Deprecated
	public synchronized void setPreviousVersion(final URI uri) {
		getRelations().setIsDerivedFrom(uri.toString());
	}

	@Deprecated
	public synchronized void clearPreviousVersion() {
		getRelations().setIsDerivedFrom(null);
	}

	/**
	 * If this object has a previous version set, this method returns that
	 * version, otherwise it simply returns <code>null</code>.
	 * 
	 * This method doesn't access the repository, if the respective metadatum
	 * isn't there, it returns <code>null</code>. Call
	 * {@link #reloadMetadata(boolean)} first if you really need to be sure.
	 * 
	 * @return the TextGridObject linked to by <em>isVersionOf</em>
	 */
	@Deprecated
	public synchronized TextGridObject getPreviousVersion() {
		final String isVersionOf = getRelations().getIsDerivedFrom();
		if (isVersionOf != null && isVersionOf != "") //$NON-NLS-1$
			return TextGridObject.getInstanceOffline(URI.create(isVersionOf));
		else
			return null;
	}

	/**
	 * Returns the adaptor associated with this object or <code>null</code> if
	 * none has been associated
	 * 
	 * @throws CrudServiceException
	 */
	public synchronized URI getAdaptor() throws CrudServiceException {
		String adaptorProperty = getRelations().getHasAdaptor();
		if (adaptorProperty == null)
			return null;
		else
			return URI.create(getRelations().getHasAdaptor());
	}

	/**
	 * If an adaptor is associated, clears it.
	 * 
	 * @throws CrudServiceException
	 *             if reading the object's metadata fails
	 */
	public synchronized void clearAdaptor() throws CrudServiceException {
		getRelations().setHasAdaptor(null);
	}

	/**
	 * Returns the URI of this object's URI relation or null if none present.
	 * This does only handle the element from the metadata, not some inferred or
	 * 
	 * @throws CrudServiceException
	 */
	public URI getSchemaURI() throws CrudServiceException {
		String schema = getRelations().getHasSchema();
		if (schema != null)
			return URI.create(schema);
		return null;
		// TODO we probably need two implementations for getRelations(), one
		// that is isComplete-aware and one that doesn't throw exceptions
		// OMElement relationsElement = getRelationsElement(false);
		// if (relationsElement != null) {
		// OMElement hasSchema = getFirstChildElement(relationsElement,
		// HAS_SCHEMA);
		// if (hasSchema == null)
		// return null;
		// try {
		// return new URI(hasSchema.getText());
		// } catch (URISyntaxException e) {
		// Activator
		// .handleError(
		// e,
		// "The object {0} contains an invalid URI ({1}) in its metadata: {2}",
		// this, e.getInput(), e.getLocalizedMessage());
		// }
		// }
		// if (!isComplete()) {
		// reloadMetadata(false);
		// return getSchemaURI();
		// }
		// return null;
	}

	/**
	 * Removes the schema association from this element, if it has one. Does
	 * nothing otherwise ...
	 */
	public void deleteSchema() {
		getRelations().setHasSchema(null);
	}

	// protected static OMElement getFirstChildElement(OMElement root, QName
	// child) {
	// if (root == null)
	// return null; // but this would be strange!?
	// Iterator<?> iter = root.getChildrenWithName(child);
	// if (iter.hasNext())
	// return (OMElement) iter.next();
	// else
	// return null;
	// }

	// /**
	// * Returns the <var>relations</var> element.
	// *
	// * @param create
	// * if the element does not exist yet, then: if <code>true</code>,
	// * create and return a new relationsElement, else return
	// * <code>null</code>
	// * @return the relations element or <code>null</code>, if none exists and
	// * none has been created
	// */
	// protected synchronized OMElement getRelationsElement(boolean create) {
	// OMElement relationsElement = getFirstChildElement(rootElement,
	// RELATIONS_NAME);
	// if (relationsElement == null && create) {
	// OMElement administrativeElement = getFirstChildElement(rootElement,
	// ADMINISTRATIVE_NAME);
	// OMFactory factory = OMAbstractFactory.getOMFactory();
	// relationsElement = factory.createOMElement(RELATIONS_NAME);
	// administrativeElement.insertSiblingAfter(relationsElement);
	// }
	// return relationsElement;
	// }

	protected static IPath makeLocalPath(IPath path) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IPath workspaceLocation = root.getLocation();
		if (workspaceLocation.isPrefixOf(path)) {
			path = path.removeFirstSegments(workspaceLocation.segmentCount())
					.makeAbsolute();
			path = path.setDevice(null);
		}
		return path;
	}

	/**
	 * Creates a local (read-only) copy of the document. The result is
	 * guaranteed to be a local file. This might be cleaned on shutdown, but
	 * that is currently not implemented.
	 * 
	 * @category HACK
	 * @param monitor
	 *            the progress monitor to use for reporting progress to the
	 *            user. It is the caller's responsibility to call done() on the
	 *            given monitor. Accepts null, indicating that no progress
	 *            should be reported and that the operation cannot be cancelled.
	 * @param reload
	 *            FIXME currently not honoured <del>if the local copy already
	 *            exists, reload determines whether the current local copy will
	 *            be replaced and the object reloaded or whether simply the
	 *            existing local copy will be returned.</del>
	 * @return the IFile of the local copy, or null
	 * @throws CoreException
	 *             when some access function fails
	 */
	public File toLocalFile(IProgressMonitor monitor, boolean reload)
			throws CoreException {
		IFileStore store = EFS.getStore(getEFSURI());
		return store.toLocalFile(EFS.CACHE, monitor);
	}

	protected static void createRecursively(IContainer folder)
			throws CoreException {
		if (folder.exists())
			return;

		IContainer parent = folder.getParent();
		if (!parent.exists())
			createRecursively(parent);

		try {

			switch (folder.getType()) {
			case IResource.FOLDER:
				((IFolder) folder).create(true, true, null);
				break;
			case IResource.PROJECT:
				((IProject) folder).create(null);
				((IProject) folder).open(null);
				break;
			}
		} catch (ResourceException e) {
			// FIXME strangely, exists is false although the stuff exists ...
		}

	}

	public synchronized int getPermissions() throws CoreException {

		if (permissions >= 0)
			return permissions;

		// TODO for new files, infer the permissions from the project. But this
		// requires TG-auth* calls in TextGridProject ...

		if (permissions != RESET) {
			GeneratedType generated = metadata.getGeneric().getGenerated();
			if (generated != null) {
				String permissionString = generated.getPermissions();

				if (permissionString != null) {
					Scanner scanner = new Scanner(permissionString);

					permissions = 0;

					while (scanner.hasNext()) {
						String permission = scanner.next();
						permissions |= getNumericPermission(permission);
					}
				} else {
					permissions = UNKNOWN;
				}
			} else {
				permissions = UNKNOWN;
			}
		}

		// TG-244
		if (permissions <= 0)
			if (isNew()) { // can't retrieve permissions for non-existing
				// objects
				permissions = READ;
				if (getProjectInstance().hasPermissions(CREATE))
					permissions |= UPDATE;
			} else
				permissions = retrievePermissions(getURI().toString());

		return permissions;
	}

	protected void resetPermissionCache() {
		permissions = RESET;
		// TG-423. See also getPermissions().
		// try {
		// @SuppressWarnings("unchecked")
		// List<OMNode> nodeList =
		// select("tg:administrative/tg:middleware/tg:permissions");
		// if (nodeList.size() > 0) {
		// OMElement permissionsElement = (OMElement) nodeList.get(0);
		// permissionsElement.setText("");
		// }
		//
		// } catch (JaxenException e) {
		// Activator
		// .handleProblem(
		// IStatus.WARNING,
		// e,
		// "An metadata processing error ({0}) occured while resetting the permission cache for {1}",
		// e.getMessage(), this);
		// }
	}

	/**
	 * Checks if the object is public.
	 * 
	 * @return true if the object is public, otherwise false.
	 */
	public boolean isPublic() throws CoreException {
		synchronized (this) {
			try {
				if (getMetadataForReading().getGeneric().getGenerated()
						.getAvailability().equals("public")) //$NON-NLS-1$
					return true;
				else
					return false;
			} catch (NullPointerException e) {
				// Availability is null
				return false;
			}
		}
	}

	/**
	 * Sets this object's format / content type metadatum to the argument.
	 * 
	 * @param contentType
	 *            the new content type. May not be null.
	 */
	public synchronized void setContentType(final TGContentType contentType) {
		this.contentTypeID = contentType.getId();
		metadata.getGeneric().getProvided().setFormat(contentTypeID);

		notifyListeners(Event.METADATA_CHANGED, this);
	}

	/**
	 * Change the project. This only works for {@linkplain #isNew() new}
	 * TextGridObjects
	 * 
	 * @param target
	 *            the target project
	 * @throws IllegalStateException
	 *             if the object has already been written to the repository
	 */
	public void setProject(TextGridProject target) throws IllegalStateException {
		if (!isNew())
			throw new IllegalStateException(
					NLS.bind(
							Messages.TextGridObject_CannotChangeProjectOfSavedObject,
							this, target));
		this.project = target.getId();
	}

	/**
	 * Registers this object with the object registry.
	 */
	protected void register() {
		synchronized (TextGridObject.class) {
			registry.put(getURI(), TextGridObject.this);
		}
	}

	/**
	 * Prepares a new version of the object.
	 * 
	 * <p>
	 * If you would like to save an object as a new version (i.e. "Save as"),
	 * you need to perform a two-step process:
	 * <ol>
	 * <li>Prepare a new version of the object</li>
	 * <li>Save the old object</li>
	 * </ol>
	 * 
	 * Calling {@link #prepareNewVersion(IProgressMonitor)} creates a
	 * {@linkplain #isNew() new} TextGridObject that represents the new version
	 * and puts this object in new-version-mode. When saving this object's
	 * associated file's contents the next time, the save operation will go to
	 * the new version instead and this object will be reset.
	 * 
	 * <p>
	 * While there is a prepared new version for this object, the new version
	 * can be requested by {@link #getPreparedNewVersion()}. To cancel the
	 * creation process, call {@link #cancelNewVersion()}.
	 * </p>
	 * 
	 * @param monitor
	 *            TODO
	 * 
	 */
	public synchronized TextGridObject prepareNewVersion(
			IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, 10);

		try {

			progress.worked(1);
			ObjectType objectType = getMetadata();
			objectType.getGeneric().setGenerated(null);
			TextGridObject newVersion = TextGridObject.getNewObjectInstance(
					getProject(), getContentTypeID());
			progress.worked(1);
			newVersion.setMetadata(objectType);
			progress.worked(2);
			newVersion.setPreviousVersion(getURI());
			this.preparedNewVersion = newVersion;
			progress.worked(1);
			newVersion.getAdapter(IFile.class); // force create the new link in
			// to avoid
			// deadlocks when the save process calls move()
			progress.worked(5);
			return this.preparedNewVersion;

		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
		return null;
	}

	public synchronized void cancelNewVersion() {
		preparedNewVersion = null;
	}

	public synchronized TextGridObject getPreparedNewVersion() {
		return preparedNewVersion;
	}

	/**
	 * Prepares a new revision of the object.
	 * 
	 * <p>
	 * If you would like to save an object as a new revision (i.e.
	 * "Save as new revision"), you need to perform a two-step process:
	 * <ol>
	 * <li>Prepare a new revision of the object</li>
	 * <li>Save the old object</li>
	 * </ol>
	 * 
	 * Calling {@link #prepareNewRevision(IProgressMonitor)} creates a
	 * {@linkplain #isNew() new} TextGridObject that represents the new revision
	 * and puts this object in new-revision-mode. When saving this object's
	 * associated file's contents the next time, the save operation will go to
	 * the new revision instead and this object will be reset.
	 * 
	 * <p>
	 * While there is a prepared new revision for this object, the new revision
	 * can be requested by {@link #getPreparedNewRevision()}. To cancel the
	 * creation process, call {@link #cancelNewRevision()}.
	 * </p>
	 * 
	 * @param monitor
	 *            TODO
	 * 
	 */
	public synchronized TextGridObject prepareNewRevision(
			IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, 10);

		try {

			progress.worked(1);
			ObjectType objectType = getMetadata();
			objectType.getGeneric().setGenerated(null);
			TextGridObject newRevision = TextGridObject.getNewObjectInstance(
					getProject(), getContentTypeID());
			progress.worked(1);
			newRevision.setMetadata(objectType);
			progress.worked(2);
			newRevision.setRevisionURI(getURI().toString());
			this.preparedNewRevision = newRevision;
			progress.worked(1);
			newRevision.getAdapter(IFile.class); // force create the new link in
			// to avoid
			// deadlocks when the save process calls move()
			progress.worked(5);
			return this.preparedNewRevision;

		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
		return null;
	}

	public synchronized void cancelNewRevision() {
		preparedNewRevision = null;
	}

	public synchronized TextGridObject getPreparedNewRevision() {
		return preparedNewRevision;
	}

	public synchronized void setRevisionURI(String revisionURI) {
		this.revisionURI = revisionURI;
	}

	public synchronized String getRevisionURI() {
		return this.revisionURI;
	}

	/**
	 * Prepares the metadata set of the TextGrid object for an edition.
	 * 
	 * @param isEditonOf
	 * @param agent
	 *            should not be null
	 * @param source
	 * @param formOfNotiation
	 * @param license
	 *            should not be null
	 */
	public synchronized void setEditionMetadata(String isEditionOf,
			AgentType agent, SourceType source,
			FormOfNotationType formOfNotation, License license) {
		ObjectType objectType;
		try {
			objectType = getMetadata();
			objectType.setItem(null);
			objectType.setWork(null);
			objectType.setCollection(null);

			ObjectFactory objectFactory = new ObjectFactory();
			EditionType editionType = objectFactory.createEditionType();
			objectType.setEdition(editionType);

			if (isEditionOf != null)
				objectType.getEdition().setIsEditionOf(isEditionOf);

			if (agent != null)
				objectType.getEdition().getAgent().add(agent);

			if (source != null)
				objectType.getEdition().getSource().add(source);

			if (formOfNotation != null)
				objectType.getEdition().getFormOfNotation().add(formOfNotation);

			// TODO: class LanguageType is actually not available

			if (license != null)
				objectType.getEdition().setLicense(license);

			metadata = objectType;

			notifyListeners(Event.METADATA_CHANGED, this);

		} catch (CrudServiceException e) {
			new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					NLS.bind(
							Messages.TextGridObject_ErrorPreparingMDEdition,
							e));
		}
	}

	/**
	 * Prepares the metadata set of the TextGrid object for a collection.
	 * 
	 * @param collector
	 *            should not be null
	 * @param abstract_var
	 * @param collectionDescription
	 * @param spartial
	 * @param temporal
	 * @param subject
	 */
	public synchronized void setCollectionMetadata(PersonType collector,
			String abstract_var, String collectionDescription,
			AuthorityType spartial, AuthorityType temporal,
			AuthorityType subject) {
		ObjectType objectType;
		try {
			objectType = getMetadata();
			objectType.setItem(null);
			objectType.setWork(null);
			objectType.setEdition(null);

			ObjectFactory objectFactory = new ObjectFactory();
			CollectionType collectionType = objectFactory
					.createCollectionType();
			objectType.setCollection(collectionType);

			if (collector != null)
				objectType.getCollection().getCollector().add(collector);

			if (abstract_var != null)
				objectType.getCollection().setAbstract(abstract_var);

			if (collectionDescription != null)
				objectType.getCollection().getCollectionDescription()
						.add(collectionDescription);

			if (spartial != null)
				objectType.getCollection().getSpatial().add(spartial);

			if (temporal != null)
				objectType.getCollection().getTemporal().add(temporal);

			if (subject != null)
				objectType.getCollection().getTemporal().add(subject);

			metadata = objectType;

			notifyListeners(Event.METADATA_CHANGED, this);

		} catch (CrudServiceException e) {
			new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
					Messages.TextGridObject_ErrorPreparingMDCollection,
					e));
		}

	}

	/**
	 * Prepares the metadata set of the TextGrid object for an item.
	 * 
	 * @param rightsHolder
	 *            should not be null
	 */
	public synchronized void setItemMetadata(final PersonType rightsHolder) {
		ObjectType objectType;
		try {
			objectType = getMetadata();
			objectType.setCollection(null);
			objectType.setWork(null);
			objectType.setEdition(null);

			ObjectFactory objectFactory = new ObjectFactory();
			ItemType itemType = objectFactory.createItemType();
			objectType.setItem(itemType);

			if (rightsHolder != null)
				objectType.getItem().getRightsHolder().add(rightsHolder);

			metadata = objectType;

			notifyListeners(Event.METADATA_CHANGED, this);

		} catch (CrudServiceException e) {
			new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
					Messages.TextGridObject_ErrorPreparingMDItem, e));
		}
	}

	/**
	 * Prepares the metadata set of the TextGrid object for a work.
	 * 
	 * @param agent
	 *            should not be null
	 */
	public synchronized void setWorkMetadata(AgentType agent) {
		ObjectType objectType;
		try {
			objectType = getMetadata();
			objectType.setItem(null);
			objectType.setCollection(null);
			objectType.setEdition(null);

			ObjectFactory objectFactory = new ObjectFactory();
			WorkType workType = objectFactory.createWorkType();
			objectType.setWork(workType);

			if (agent != null)
				objectType.getWork().getAgent().add(agent);

			metadata = objectType;

			notifyListeners(Event.METADATA_CHANGED, this);

		} catch (CrudServiceException e) {
			new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
					Messages.TextGridObject_ErrorPreparingMDWork, e));
		}
	}

	/**
	 * Adds an item to the latest URI registry. This registry is used to
	 * remember all TextGrid objects of which we know the latest - concret uri
	 * relation.
	 * 
	 * @param latest
	 *            the latest uri
	 * @param uri
	 *            the concret uri
	 */
	public static void addLatestRegistryItem(URI latest, URI uri) {
		latestRegistry.put(latest, uri);
	}

	/**
	 * Removes an item to the latest URI registry. This registry is used to
	 * remember all TextGrid objects of which we know the latest - concret uri
	 * relation.
	 * 
	 * @param latest
	 *            the latest uri
	 */
	public static void removeLatestRegistryItem(URI latest) {
		latestRegistry.remove(latest);
	}

	/**
	 * The function tries to find a registered TextGrid object to the given uri.
	 * 
	 * @param uri
	 * @return
	 */
	private synchronized static TextGridObject getRegisteredTGO(URI uri) {
		URI concretUri = null;
		if (!uri.toString().contains(".")) { //$NON-NLS-1$
			// latest uri
			concretUri = latestRegistry.get(uri);
			if (concretUri != null)
				return registry.get(concretUri);
			else
				return registry.get(uri);
		} else
			// concret uri
			return registry.get(uri);
	}

	// public void setPreventNotifyingMetadataChanged(
	// boolean preventNotifyingMetadataChanged) {
	// //this.preventNotifyingMetadataChanged = preventNotifyingMetadataChanged;
	// }

	// public boolean isPreventNotifyingMetadataChanged() {
	// return preventNotifyingMetadataChanged;
	// }

	/**
	 * Notify the metadata editor, that the metadata of this object has been
	 * changed
	 * 
	 * @category HACK (TG-1281)
	 */
	public void notifyMetadataEditor() {
		TextGridObject.notifyListeners(Event.METADATA_CHANGED_FROM_OUTSIDE,
				this);
	}
	
	/**
	 * Set the "open as read only" flag.
	 * @param value
	 */
	public void setOpenAsReadOnly(boolean value) {
		this.openAsReadOnly = value;
	}
	
	/**
	 * Gives the "open as read only" flag.
	 * @return
	 */
	public boolean isToOpenAsReadOnly() {
		return this.openAsReadOnly;
	}
}
