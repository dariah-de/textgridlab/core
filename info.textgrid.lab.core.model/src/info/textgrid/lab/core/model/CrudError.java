package info.textgrid.lab.core.model;

import java.net.URI;
import java.text.MessageFormat;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;

/**
 * A central class to cope with (CRUD) errors.
 * 
 * The idea is that this class is able to collect the various information regarding
 * errors and create both a human-readable and localizable error message and detailed
 * technical information that may be communicated back to a developer. Information 
 * sources are the actual faults from which the information should be extracted
 * as well as context information that must be given by the caller.
 * 
 * The class should log the extracted information and be able to display a dialog to the user.
 * 
 * <h4>TODO</h4>
 * We should extract a super class from this that generally handles errors.
 * We need some 
 * 
 * @author tv
 */
public class CrudError {
	
	public static final String CRUD_SERVICE_NAMESPACE = "http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService"; //$NON-NLS-1$
	public static final QName FAULT_MESSAGE_NAME = new QName(CRUD_SERVICE_NAMESPACE, "faultMessage"); //$NON-NLS-1$
	public static final QName FAULT_CAUSE_NAME = new QName(CRUD_SERVICE_NAMESPACE, "cause"); //$NON-NLS-1$
	
	private Exception fault;
	private URI uri;
	private String message;
	private String faultMessage;
	private String faultCause;

	/**
	 * Parses the detail element of a TGcrud fault.
	 * @param detail an OMElement that should contain a faultMessage and a faultCause
	 */
	protected void parseDetail(OMElement detail) {
		OMElement faultMessageElement = detail.getFirstChildWithName(FAULT_MESSAGE_NAME);
		if (faultMessageElement != null)
			setFaultMessage(faultMessageElement.getText());
		OMElement faultCauseElement = detail.getFirstChildWithName(FAULT_CAUSE_NAME);
		if (faultCauseElement != null)
			setFaultCause(faultCause);
	}

	public void setException(Exception fault) {
		this.fault = fault;
	}

	public Exception getFault() {
		return fault;
	}
	
	public String getDetailMessage() {
		
		
		return null;		
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public URI getUri() {
		return uri;
	}

	public void setMessage(String message, Object... arguments) {
		this.message = MessageFormat.format(message, arguments);
	}

	public String getMessage() {
		return message;
	}

	public void setFaultMessage(String faultMessage) {
		this.faultMessage = faultMessage;
	}

	public String getFaultMessage() {
		return faultMessage;
	}

	public void setFaultCause(String faultCause) {
		this.faultCause = faultCause;
	}

	public String getFaultCause() {
		return faultCause;
	}
	
	
}
