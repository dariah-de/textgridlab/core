package info.textgrid.lab.core.model;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.Bundle;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;

/**
 * A (read-only) implementation for TextGrid content types.
 * 
 * <p>
 * New content types are contributed using the extension point
 * <tt>info.textgrid.lab.core.contentTypes</tt>
 * </p>
 * 
 * <h3>How to register a new content type</h3>
 * 
 * <p>
 * To register a new content type, you need to define an extension to the
 * extension point <tt>info.textgrid.lab.core.contentTypes</tt>. Open your
 * plugin manifest, go to the <em>Extensions</em> tab, click <em>add</em> and
 * select <em>info.textgrid.lab.core.contentTypes</em>. You can now right-click
 * the corresponding entry and add a new <em>contentType</em> element. Select
 * the element and hover the fields for a description of the individual values.
 * </p>
 * <p>
 * Some notes for specific fields:
 * </p>
 * <dl>
 * <dt>Eclipse Content Type</dt>
 * <dd>The eclipse content type is used for eclipse specific handling like
 * editor selecion etc. You should assign a type here. It may be useful to
 * create a new eclipse content type (extension point
 * org.eclipse.core.runtime.contentType) that is <em>based on</em> an existing
 * content type for some applications (e.g., if you define a new XML format).</dd>
 * 
 * <dt>Extension</dt>
 * <dd>You need to define exactly one extension here. It will be used in some
 * use cases, e.g., (1) as extension of the pseudo file that is used to
 * represent the {@link TextGridObject}s of your type to Eclipse, (2) as file
 * extension when exporting objects of your type, or (3) in the content type
 * detection heuristics of the import wizard. The latter is a reason to not
 * reuse the extension of another content type
 * <em>(don't use <tt>xml</tt> or I will change it)</em>, otherwise the
 * heuristics just takes the first available content type.
 * 
 * </dd>
 * </dl>
 */
public class TGContentType implements Comparable<TGContentType> {

	private static Map<String, TGContentType> registry;

	private static List<TGContentType> publicTypes;
	
	/**
	 * A default type identifying unknown content types
	 */
	public static final String UNKNOWN_ID = "unknown/unknown"; //$NON-NLS-1$
	
	private String id;
	private String description;
	private Pattern additionalTypes;
	private String extension;
	private String eclipseContentType;
	private boolean internal = false;
	private IContributor contributor;

	private ImageDescriptor imageDescriptor;

	private String imagePath;

	private boolean creatable;
	private boolean adHoc = false;

	/**
	 * Returns whether the TextGridLab can create new objects of this content type.
	 */
	public boolean isCreatable() {
		return creatable;
	}

	/**
	 * A content type is an <em>ad hoc</em> content type if it has been
	 * automatically created from a file extension or from a content type id,
	 * without proper configuration from an extension point etc.
	 */
	public boolean isAdHoc() {
		return adHoc;
	}

	/**
	 * Returns the file extension to append to link names of this content type
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Returns a pattern that matches additional content type strings that might
	 * describe this content type. Used for legacy content types in the
	 * repository.
	 */
	public Pattern getAdditionalTypes() {
		return additionalTypes;
	}

	
	/**
	 * Returns the content type's MIME id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns a human-readable description for this content type.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns Eclipse's content type ID for this content type, if applicable
	 */
	public String getEclipseContentTypeID() {
		return eclipseContentType;
	}
	
	public IContentType getEclipseContentType() {
		return eclipseContentType == null ? null : Platform
				.getContentTypeManager().getContentType(eclipseContentType);
	}

	/**
	 * If true, files of this type are created internally by the lab and should
	 * not show up in the New dialog etc.
	 */
	public boolean isInternal() {
		return internal;
	}

	/**
	 * @return the bundle that contributed this content type.
	 */
	public IContributor getContributor() {
		return contributor;
	}

	/**
	 * Returns an image descriptor representing an icon for objects of this
	 * content type.
	 * 
	 * <p>
	 * If the contributor specifies an <var>image</var> attribute in its
	 * extension, this image is used. Otherwise we try to find this object's
	 * default editor and use its icon.
	 * </p>
	 * 
	 * <p>
	 * Clients might want to use {@link #getImage(boolean)}, which delivers an
	 * image managed by this plugin's {@link ImageRegistry}.
	 * </p>
	 * 
	 * @return an ImageDescriptor or <code>null</code>, if no appropriate icon
	 *         could be found.
	 * 
	 */
	public ImageDescriptor getImageDescriptor() {
		
		// Case 0: Already computed
		if (imageDescriptor != null)
			return imageDescriptor;
		
		// The following cases are tried in order, the first that resolves to a
		// non-null descriptor is used.
		
		// Case 1: Image explicitely specified in extension.
		if (imageDescriptor == null && imagePath != null) {
			ImageDescriptor descriptor = imageDescriptorFromPlugin(contributor
					.getName(), imagePath);
			setImageDescriptor(descriptor);
			if (Activator.getDefault().isDebugging())
				System.out.println(MessageFormat.format(
						"Set image descriptor {0} for {1} from imagePath {2}", //$NON-NLS-1$
						descriptor, this, imagePath));
		}
		
		// The alternatives involve looking at the editor registry.
		String filename = extension == null ? "TextGridObject" //$NON-NLS-1$
				: "TextGridObject." + extension; //$NON-NLS-1$
		IEditorRegistry editorRegistry = PlatformUI.getWorkbench()
		.getEditorRegistry();
		
		// Case 2: Eclipse content type specified.
		if (imageDescriptor == null && eclipseContentType != null) {
			ImageDescriptor descriptor = editorRegistry
					.getImageDescriptor(filename, getEclipseContentType());
			setImageDescriptor(descriptor);
			if (Activator.getDefault().isDebugging())
				System.out
					.println(MessageFormat
							.format(
									"Image descriptor {0} from eclipse type {1} for content type {2}", //$NON-NLS-1$
									descriptor, eclipseContentType, this));
		}
		
		// Case 3: Extension specified
		if (imageDescriptor == null && extension != null) {
			ImageDescriptor descriptor = editorRegistry
					.getImageDescriptor(filename);
			setImageDescriptor(descriptor);
			if (Activator.getDefault().isDebugging())
				System.out.println(MessageFormat.format(
					"ImageDescriptor {0} for filename {1} for contenttype {2}", //$NON-NLS-1$
					descriptor, filename, this));
		}
		return imageDescriptor;
	}

	/**
	 * Loads a new image for this content type and registers it with the
	 * plug-in's image registry. Clients must use {@link #getImage(boolean)}.
	 * 
	 * @return A new image (which must be disposed by the client), or null if
	 *         none but the default has been found.
	 */
	private Image loadImage() {
		ImageDescriptor descriptor = getImageDescriptor();
		if (descriptor == null)
			return null;
		
		Image image = descriptor.createImage(false);
		if (image == null) {
			Activator
					.getDefault()
					.handleProblem(
							IStatus.WARNING,
							MessageFormat
									.format(
											"Could not load the icon for content type {0} (image descriptor {1}).", //$NON-NLS-1$
											this, imageDescriptor), null);
			return null;
		}
		
		Activator.getDefault().getImageRegistry().put(getId(), image);
		return image;

	}

	/**
	 * Tries to return an {@link ImageDescriptor} for the image at the given
	 * <var>path</var> in the given <var>plugin</var>. This is a replacement for
	 * {@link AbstractUIPlugin#imageDescriptorFromPlugin(String, String)} that
	 * is intended to work.
	 * 
	 * @param plugin
	 *            the ID of the plugin in which to search
	 * @param path
	 *            the plugin relative path
	 * @return an image descriptor or <code>null</code>.
	 */
	private ImageDescriptor imageDescriptorFromPlugin(String plugin, String path) {
		if (plugin == null || path == null) {
			throw new IllegalArgumentException();
		}

		Bundle bundle = Platform.getBundle(plugin);
		URL rawURL = FileLocator.find(bundle, new Path(path), null);
		if (rawURL == null)
			return null;
		try {
			URL resolvedURL = FileLocator.resolve(rawURL);
			ImageDescriptor descriptor = ImageDescriptor
					.createFromURL(resolvedURL);
			if (descriptor != null
					&& !descriptor.equals(ImageDescriptor
							.getMissingImageDescriptor()))
				return descriptor;

		} catch (IOException e) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							e,
							"Could not resolve {0}, which is said to be the image {1} in {2}.", //$NON-NLS-1$
							rawURL, path, plugin);
		}
		
		if (rawURL != null) {
			ImageDescriptor descriptor = ImageDescriptor.createFromURL(rawURL);
			if (descriptor != null
					&& !descriptor.equals(ImageDescriptor
							.getMissingImageDescriptor()))
				return descriptor;
		}

		return null;
	}

	/**
	 * Returns an image representing the object. The image will be managed by
	 * this plugin's or the workbench's
	 * {@linkplain AbstractUIPlugin#getImageRegistry() image resistry}.
	 * 
	 * @see #getImageDescriptor()
	 * @param fallback
	 *            if <code>true</code>, return a default image if no image could
	 *            be found.
	 * @return an {@link Image} or <code>null</code>, if none found and
	 *         <var>fallback</var> is <code>false</code>.
	 */
	public Image getImage(boolean fallback) {
		Image image = null;
		ImageRegistry registry = Activator.getDefault().getImageRegistry();
			image = registry.get(getId());
			if (image == null) {
				image = loadImage();
			}
		if (image == null && fallback)
			image = PlatformUI.getWorkbench().getSharedImages().getImage(
					ISharedImages.IMG_OBJ_FILE);
		return image;
	}

	/**
	 * Registers the given image descriptor for use with this content type.
	 * 
	 * @param imageDescriptor
	 */
	protected void setImageDescriptor(ImageDescriptor imageDescriptor) {
		if (imageDescriptor == null)
			return;
		this.imageDescriptor = imageDescriptor;
	}

	/**
	 * @param id
	 *            the MIME type, see {@link #getId()}
	 * @param description
	 *            a human readable description, see {@link #getDescription()}
	 * @param extension
	 *            TODO
	 * @param additionalPattern
	 *            TODO
	 * @throws CoreException
	 *             when the content type could not be created.
	 */
	public TGContentType(String id, String description, String extension,
			String additionalTypes, boolean internal, String eclipseContentType)
			throws CoreException {
		super();
		if (id == null || "".equals(id)) //$NON-NLS-1$
			throw new CoreException(
					Activator
							.handleProblem(
									IStatus.WARNING,
									null,
									"Could not create content type for invalid id {0} (name would be {1}, Eclipse content type {2}).", //$NON-NLS-1$
									id, description));
		this.id = id;
		this.description = description;
		this.extension = extension;
		if (null != additionalTypes && !"".equals(additionalTypes)) //$NON-NLS-1$
			this.additionalTypes = Pattern.compile(additionalTypes);
		this.internal = internal;
		this.eclipseContentType = eclipseContentType;
	}


	/**
	 * Creates a content type from the extension point
	 * 
	 * @param element
	 * @throws CoreException
	 *             When the content type could not be created.
	 * @throws InvalidRegistryObjectException
	 */
	public TGContentType(IConfigurationElement element)
			throws InvalidRegistryObjectException, CoreException {
		this(element.getAttribute("typeID"), element.getAttribute("name"), //$NON-NLS-1$ //$NON-NLS-2$
				element.getAttribute("extension"), element //$NON-NLS-1$
						.getAttribute("additionalContentTypePattern"), "true" //$NON-NLS-1$ //$NON-NLS-2$
						.equals(element.getAttribute("internal")), element //$NON-NLS-1$
						.getAttribute("eclipseContentType")); //$NON-NLS-1$
		contributor = element.getContributor();
		imagePath = element.getAttribute("image"); //$NON-NLS-1$
		creatable = !"false".equals(element.getAttribute("creatable")); //$NON-NLS-1$ //$NON-NLS-2$
		
	}

	protected static void loadContentTypes() {
		registry = new LinkedHashMap<String, TGContentType>();
		publicTypes = new LinkedList<TGContentType>();
		
		IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();
		IConfigurationElement[] elements = extensionRegistry
				.getConfigurationElementsFor("info.textgrid.lab.core.model.contentTypes"); //$NON-NLS-1$
		for (IConfigurationElement element : elements) {
			
			TGContentType newType;
			try {
				newType = new TGContentType(element);
				TGContentType replaced = registry.put(newType.getId(), newType);
				if (!newType.isInternal()) {
					publicTypes.add(newType);
				}
				if (replaced != null) {
					Activator
							.handleProblem(
									IStatus.WARNING,
									null,
									Messages.TGContentType_ConflictingContentTypes,
									replaced, replaced.getContributor(),
									newType, newType.getContributor());
				}
			} catch (CoreException e) {
				Activator.handleProblem(IStatus.ERROR, e, 
						"Could not add a content type due to an invalid specification in the extension registry." //$NON-NLS-1$
										+ "The offending element is {0} in {1}. Please file a bug report.", //$NON-NLS-1$
								element, element.getContributor().getName());
			}
		}
		Activator
				.getDefault()
				.getLog()
				.log(
						new Status(
								IStatus.INFO,
								Activator.PLUGIN_ID,
 getDebugInfo()));
	}

	public static String getDebugInfo() {
		String table = Joiner.on("\n|-\n| ").join(Iterables.transform(registry.values(), new Function<TGContentType, String>() { //$NON-NLS-1$

			public String apply(TGContentType arg0) {
				return arg0.toDebugString(" || "); //$NON-NLS-1$
			}
		}));
		return MessageFormat.format("Loaded the following content types from the plug-in registry: \n{0}", //$NON-NLS-1$
 table);
	}

	/**
	 * Returns the content type with the given id.
	 * 
	 * @param id
	 * @return The content type object or null, if none found.
	 * @see #findMatchingContentType(String)
	 */
	public static TGContentType getContentType(String id) {
		if (registry == null)
			loadContentTypes();
		return registry.get(id);
	}

	/**
	 * Tries to find a content type for the given ID. First it looks for content
	 * types with the given id in their ID field, if this is fruitless it looks
	 * for a pattern whose {@linkplain #getAdditionalTypes() additional types
	 * pattern} maches the given string.
	 * 
	 * @param id
	 *            A content type identifier.
	 * @return A matching content type or the {@linkplain #isUnknown() unknown}
	 *         content type, if none found.
	 */
	public static TGContentType findMatchingContentType(String id) {
		TGContentType contentType = getContentType(id);
		if (contentType != null)
			return contentType;
		
			for (Iterator<TGContentType> i = registry.values().iterator(); i
					.hasNext();) {
				TGContentType candidate = i.next();
				if (candidate.getAdditionalTypes() != null) {
					Matcher matcher = candidate.getAdditionalTypes()
							.matcher(id);
					if (matcher.matches())
						return candidate;
				}
			}
		return getContentType(UNKNOWN_ID);
	}

	/**
	 * Returns a content type for the given MIME type.
	 * 
	 * If a {@link TGContentType} with the given ID could be found in the
	 * registry, it is returned. Otherwise, we try to create an
	 * {@linkplain #isAdHoc() ad-hoc} content type for the given and return
	 * that.
	 * 
	 * If we create a new content type object, it is also added to the registry
	 * as non-creatable and ad-hoc.
	 * 
	 * @param id
	 *            a MIME type for which to return a TGContentType object
	 * @return a {@link TGContentType} for the given id, either as returned by
	 *         {@link #findMatchingContentType(String)} or created ad hoc.
	 */
	public static TGContentType of(final String id) throws IllegalArgumentException {
		TGContentType contentType = findMatchingContentType(id);
		if (contentType.isUnknown() && !UNKNOWN_ID.equals(id)) {
			try {
				MimeType mimeType = new MimeType(id);
				String subType = mimeType.getSubType();
				int plusPos = subType.indexOf('+');
				String extension;
				if (plusPos >= 1)
					extension = subType.substring(0, plusPos - 1);
				else
					extension = subType;
				contentType = new TGContentType(id, id, extension, null, false, null);
				contentType.adHoc = true;
				contentType.creatable = false;
				registry.put(id, contentType);
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			} catch (MimeTypeParseException e) {
				throw new IllegalArgumentException(e);
			}
		}
		return contentType;
	}


	/**
	 * @deprecated Use {@link #getContentTypes(boolean)} instead
	 */
	@Deprecated
	public static TGContentType[] getContentTypes() {
		return getContentTypes(false);
	}

	/**
	 * Returns a copy of the list of content types.
	 * 
	 * @param onlyPublic
	 *            If true, {@linkplain #isInternal() internal} content types are
	 *            not included.
	 */
	public static TGContentType[] getContentTypes(boolean onlyPublic) {
		if (registry == null)
			loadContentTypes();
		Collection<TGContentType> values = onlyPublic ? publicTypes : registry
				.values();
		return values.toArray(new TGContentType[0]);
	}
	
	/**
	 * Returns a subset of content types.
	 * 
	 * @param filter a {@link Predicate} that returns true for each content type that should be returned
	 * @return those content types for which the <var>filter</var> returned true.
	 */
	public static TGContentType[] getContentTypes(final Predicate<TGContentType> filter) {
		return Collections2.filter(registry.values(), filter).toArray(new TGContentType[0]);
		
	}
	
	/**
	 * Returns a human-readable string describing the content type.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (internal)
			return MessageFormat
					.format("{0} ({1}, internal)", description, id); //$NON-NLS-1$
		else
			return MessageFormat.format("{0} ({1})", description, id); //$NON-NLS-1$
	}
	
	/**
	 * If true, this is the special content type representing unknown mime
	 * types. You MUST NOT create objects with this type!
	 */
	public boolean isUnknown() {
		return UNKNOWN_ID.equals(id);
	}

	/**
	 * Tries to find a matching content type the given file extension.
	 * 
	 * <h4>Alternative</h4> If you have a file name and accept
	 * {@linkplain #isAdHoc() ad-hoc} content types, use
	 * {@link #getByFilename(String)} instead.
	 * 
	 * @param extension
	 *            the file extension, case insensitive without the leading dot.
	 *            May not be null.
	 * @return A {@linkplain TGContentType content type} whose
	 *         {@linkplain #getExtension() extension}
	 *         {@linkplain String#equalsIgnoreCase(String) matches} the given
	 *         extension, or null if none found.
	 */
	public static TGContentType findByExtension(String extension) {
		for (TGContentType contentType : registry.values()) {
			if (extension.equalsIgnoreCase(contentType.getExtension()))
				return contentType;
		}
		return null;
	}

	/**
	 * Returns or creates a content type for the given file name.
	 * 
	 * We're trying to find an existing content type from the content type
	 * registry:
	 * <ol>
	 * <li>by calling {@link #findByExtension(String)} on the filename's
	 * extension</li>
	 * <li>by asking the system's MIME type map (cf.
	 * {@link MimetypesFileTypeMap}) for the MIME type and searchiong for that
	 * in our registry.</li>
	 * </ol>
	 * If we're unsuccessful, a new {@link ContentType} is created. We try to
	 * guess the new type's attributes. New types will be
	 * {@linkplain #isAdHoc() ad-hoc} and {@linkplain #isCreatable() not
	 * creatable}. The new content type is added to the registry, it will be
	 * returned by future calls of {@linkplain #getContentTypes(boolean)
	 * <code>getContentTypes(false)</code>} and considered in future calls of
	 * the content type retrieval methods.
	 * 
	 * @param filename
	 *            The name of the file for which we're looking for a content
	 *            type.
	 * @return the content type object, possibly an {@linkplain #isAdHoc()
	 *         ad-hoc} one.
	 */
	public static TGContentType getByFilename(final String filename) {
		String extension = FilenameUtils.getExtension(filename);
		TGContentType contentType = findByExtension(extension);
		if (contentType == null) {
			String mimeType = MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(filename);
			contentType = getContentType(mimeType);

			IContentType eclipseContentType = Platform.getContentTypeManager().findContentTypeFor(filename);
			if (contentType == null || "application/octet-stream".equals(mimeType)) { //$NON-NLS-1$
				TGContentType byEclipseType = findByEclipseType(eclipseContentType);
				if (byEclipseType != null)
					contentType = byEclipseType;
			}

			if (contentType == null) {
				String eclipseType;
				String description;
				if (eclipseContentType != null) {
					eclipseType = eclipseContentType.getId();
					description = eclipseContentType.getName();
				} else {
					eclipseType = null;
					description = NLS.bind("{0} ({1})", mimeType, extension); //$NON-NLS-1$
				}

				try {
					contentType = new TGContentType(mimeType, description, extension, null, false, eclipseType);
					contentType.adHoc = true;
					contentType.creatable = false;
					registry.put(mimeType, contentType);
					StatusManager.getManager().handle(
							new Status(IStatus.INFO, Activator.PLUGIN_ID, MessageFormat.format(
											"The new ad-hoc content type {0} has been created and registered for {1}. Eclipse content type: {2}.", //$NON-NLS-1$
											contentType, filename, eclipseType)));
				} catch (CoreException e) {
					// should never happen
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				}
			}
		}
		return contentType;
	}

	/**
	 * Tries to find a suitable TextGrid content type for the given Eclipse
	 * content type
	 * 
	 * @param An
	 *            eclipse content type to look for
	 * @return a {@link TGContentType} that is associated with the given Eclipse
	 *         content type or some parent type of that, or <code>null</code>.
	 */
	public static TGContentType findByEclipseType(final IContentType type) {
		if (type == null)
			return null;
		TGContentType[] candidates = getContentTypes(new Predicate<TGContentType>() {

			public boolean apply(TGContentType input) {
				return type.equals(input.getEclipseContentType());
			}
		});
		if (candidates.length == 0)
			return findByEclipseType(type.getBaseType());
		else
			return candidates[0]; // better selection algorithm?
	}

	/**
	 * Returns a debug string for the current object, containing of the
	 * following fields in order, delimited by the given delimiter:
	 * <ul>
	 * <li> {@link #getDescription()}
	 * <li> {@link #getId()}
	 * <li>flags, if present: {@linkplain #isInternal() internal},
	 * {@linkplain #isCreatable() creatable}, {@linkplain #isAdHoc() ad-hoc},
	 * {@linkplain #isUnknown() unknown}
	 * <li> {@link #getExtension()}
	 * <li> {@link #getEclipseContentTypeID()}
	 * <li> {@linkplain #getContributor() the contributing plugin's ID}.
	 * </ul>
	 * 
	 * @param delimiter
	 *            string used to separate the fields.
	 */
	public String toDebugString(final String delimiter) {
		StringBuilder s = new StringBuilder();
		s.append(description);
		s.append(delimiter);
		s.append(id);
		s.append(delimiter);

		if (isInternal())
			s.append("internal "); //$NON-NLS-1$
		if (isCreatable())
			s.append("creatable "); //$NON-NLS-1$
		if (isAdHoc())
			s.append("ad-hoc "); //$NON-NLS-1$
		if (isUnknown())
			s.append("unknown "); //$NON-NLS-1$

		s.append(delimiter);

		s.append(getExtension());
		s.append(delimiter);
		s.append(getEclipseContentTypeID());
		s.append(delimiter);

		if (getContributor() != null)
			s.append(getContributor().getName());

		return s.toString();
	}

	/**
	 * Compares this {@link TGContentType} with the specified {@link TGContentType} for order. 
	 * @param contentType
	 * 				the content type to be compared
	 * @return a negative integer, zero, or a positive integer as this object is less than, 
	 * 		   equal to, or greater than the specified object.
	 */
	public int compareTo(TGContentType contentType) {
		if (contentType.getDescription() == null && this.getDescription() == null) {
		      return 0;
		    }
		    if (this.getDescription() == null) {
		      return 1;
		    }
		    if (contentType.getDescription() == null) {
		      return -1;
		    }
		    return this.getDescription().compareTo(contentType.getDescription());
	}

}
