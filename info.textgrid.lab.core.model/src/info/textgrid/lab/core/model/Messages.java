package info.textgrid.lab.core.model;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.model.messages"; //$NON-NLS-1$
	public static String AbstractResource_MetaNoPermission;
	public static String AggregationReader_CloseFailed;
	public static String AggregationReader_InvalidContent;
	public static String AggregationReader_InvalidURI;
	public static String CrudServiceException_ErrorDuringOperation;
	public static String CrudServiceException_ServiceCallFailed;
	public static String ModelAdaptorFactory_CachingError;
	public static String ModelAdaptorFactory_CouldNotDetectFormat;
	public static String ProjectFileException_3;
	public static String ProjectFileException_CreateError;
	public static String ProjectFileException_ReadError;
	public static String ProjectFileException_SaveError;
	public static String RestrictedTextGridObject_Title;
	public static String TextGridErrorHandler_AlreadyDeletedErrorXonY;
	public static String TextGridErrorHandler_AuthFault;
	public static String TextGridErrorHandler_DetailMessage;
	public static String TextGridErrorHandler_ErrorWhileErrorHandling;
	public static String TextGridErrorHandler_InternalServiceError;
	public static String TextGridErrorHandler_IoFault;
	public static String TextGridErrorHandler_MetadataParseFault;
	public static String TextGridErrorHandler_NoConfigurationFault;
	public static String TextGridErrorHandler_ObjectNotFoundFault;
	public static String TextGridErrorHandler_ProtocolNotImplementedFault;
	public static String TextGridErrorHandler_RelationExistsFault;
	public static String TextGridErrorHandler_RepetativeErrorXonY;
	public static String TextGridErrorHandler_UnknownFault;
	public static String TextGridErrorHandler_UpdateConflictFault;
	public static String TextGridObject_CannotChangeProjectOfSavedObject;
	public static String TextGridObject_dataContributorAsAuthor;
	public static String TextGridObject_DeletingX;
	public static String TextGridObject_ErrorPreparingMDCollection;
	public static String TextGridObject_ErrorPreparingMDEdition;
	public static String TextGridObject_ErrorPreparingMDItem;
	public static String TextGridObject_ErrorPreparingMDWork;
	public static String TextGridObject_ErrorProjectID;
	public static String TextGridObject_ErrorUriFromMD;
	public static String TextGridObject_FailedToRefreshX;
	public static String TextGridObject_HandleMultipleResults;
	public static String TextGridObject_HandleNotFound;
	public static String TextGridObject_IncompleteMD;
	public static String TextGridObject_InvalidURIInMD;
	public static String TextGridObject_MovingOnCreate;
	public static String TextGridObject_NewObjectPrefix;
	public static String TextGridObject_notifying;
	public static String TextGridObject_noTitle;
	public static String TextGridObject_ReferencedProjectMissing;
	public static String TextGridObject_Refreshing_X;
	public static String TextGridObject_RetrievingURIs;
	public static String TextGridObject_SavingMDForX;
	public static String TextGridObject_UpdateForbidden;
	public static String TextGridObject_you;
	public static String TextGridProjectFile_CouldNotSet;
	public static String TextGridProjectFile_EncodingError;
	public static String TextGridProjectFile_FindingPF;
	public static String TextGridProjectFile_Initializing;
	public static String TextGridProjectFile_Loading;
	public static String TextGridProjectFile_OfflinePF;
	public static String TextGridProjectFile_ParseError;
	public static String TextGridProjectFile_ParseError2;
	public static String TextGridProjectFile_Saving;
	public static String TextGridProjectRoot_CouldNotFetchProjects;
	public static String TextGridProjectRoot_EverythingReadable;
	public static String TextGridProjectRoot_Loading;
	public static String TextGridProjectRoot_MyEditorProjects;
	public static String TextGridProjectRoot_MyManagerProjects;
	public static String TextGridProjectRoot_MyObserverProjects;
	public static String TextGridProjectRoot_MyProjects;
	public static String TextGridProjectRoot_TGProjects;
	public static String TGContentType_ConflictingContentTypes;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
