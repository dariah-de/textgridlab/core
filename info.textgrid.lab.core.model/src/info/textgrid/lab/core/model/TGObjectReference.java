package info.textgrid.lab.core.model;

import java.net.URI;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.progress.UIJob;


/**
 * Proxy class to keep in mind how the delivered TextGridObject
 * was addressed.
 * @author Frank Queens for TextGrid
 *
 */

public class TGObjectReference extends PlatformObject {
	private final String refURI;
	private final TextGridObject tgo;
	private static ListenerList listeners = new ListenerList();
	private TGObjectReference aggregation = null; //parent aggregation, if existent

	public TGObjectReference(String refURI, TextGridObject tgo) {
		this.refURI = refURI;
		this.tgo = tgo;
	}

	public TGObjectReference(final URI uri) {
		this.refURI = uri.toString();
		this.tgo = TextGridObject.getInstanceOffline(uri);
	}

	public String getRefUri() {
		return this.refURI;
	}

	public TextGridObject getTgo() {
		return this.tgo;
	}

	public boolean isLatestRef() {
		return !refURI.contains("."); //$NON-NLS-1$
	}

	public String getRevisionNumber() {
		try {
			return tgo.getURI().toString().split("\\.")[1]; //$NON-NLS-1$
		} catch (final ArrayIndexOutOfBoundsException e) {
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * A listener that is called when something happens with an TGObjectReference object. You may
	 * add your listeners with
	 * {@link TGObjectReference#addListener(ITGObjectReferenceListener)} and remove
	 * them using {@link TGObjectReference#removeListener(ITGObjectReferenceListener)}
	 * .
	 */
	public interface ITGObjectReferenceListener {

		public enum Event {
			/**
			 * When a TGObjectReference object for the navigator is created.
			 */
			NAVIGATOR_OBJECT_CREATED,
		};

		/**
		 * The given <var>event</var> has happend to the given
		 * <var>object</var>.
		 */
		public void tgObjectReferenceChanged(Event event, TGObjectReference object);

	}

	/**
	 * Adds a listener for project change events.
	 *
	 * @param listener
	 */
	public static void addListener(ITGObjectReferenceListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes a listener for project change events.
	 *
	 * @param listener
	 */
	public static void removeListener(ITGObjectReferenceListener listener) {
		listeners.remove(listener);
	}

	/**
	 * <strong>THIS IS NOT API</strong>, it may only be called from the model or
	 * EFS implementation.
	 *
	 * @param event
	 * @param textGridProject
	 */
	public static void notifyListeners(
			final ITGObjectReferenceListener.Event event,
			final TGObjectReference tgObjectReference) {

		final UIJob job = new UIJob("Notifying TextGridProject listeners") { //$NON-NLS-1$

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				for (final Object listener : listeners.getListeners())
					((ITGObjectReferenceListener) listener).tgObjectReferenceChanged(
							event, tgObjectReference);
				return Status.OK_STATUS;
			}
		};
		job.setSystem(true);
		job.schedule();
	}

	@Override
	public Object getAdapter(Class adapter) {
		if(adapter == TextGridObject.class)
			return tgo;

		return super.getAdapter(adapter);
	}

	public TGObjectReference getAggregation() {
		return aggregation;
	}

	public void setAggregation(TGObjectReference aggregation) {
		this.aggregation = aggregation;
	}

	public boolean isPartOfAggregation() {
		return (this.aggregation != null) ? true : false;
	}

	private static final Pattern FULL_TGO_REPR = Pattern.compile("(.*) \\((textgrid:[a-z0-9.]*)\\)$"); //$NON-NLS-1$
	@Override
	public String toString() {
		final Matcher objectRepr = FULL_TGO_REPR.matcher(tgo.toString());
		if (objectRepr.matches()) {
			return MessageFormat.format("{0} [{1}]", objectRepr.group(1), getRefUri()); //$NON-NLS-1$
		} else
			return getRefUri();
	}

}
