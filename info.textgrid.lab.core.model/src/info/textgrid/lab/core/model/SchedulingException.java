package info.textgrid.lab.core.model;

import java.text.MessageFormat;
import java.util.ConcurrentModificationException;

import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;

/**
 * A SchedulingException is thrown when an operation cannot be performed because
 * its {@linkplain ISchedulingRule scheduling rule} conflicts with the current
 * job and spawning a new job is not applicable in this situation.
 * 
 * <p>Note that this is a {@link RuntimeException}.</p>
 * 
 * <p>Usually, this method indicates a programming error.</p>
 * 
 */
public class SchedulingException extends ConcurrentModificationException {
	private static final long serialVersionUID = -4181948447691679120L;
	private final TextGridObject object;

	public SchedulingException(final TextGridObject object,
			final String message, final Object... args) {
		super(MessageFormat.format(message, args));
		this.object = object;
	}

	public TextGridObject getObject() {
		return object;
	}

	/**
	 * Checks whether the given scheduling rule conflicts with the current job's. 
	 * 
	 * @param rule The scheduling rule to check
	 * @param object Just for reference: The TextGridObject on which we operate
	 * @param message A message for the user
	 * @param args arguments to the message, see {@link MessageFormat#format(String, Object...)}
	 * @throws SchedulingException the rule conflicts with the current job's 
	 */
	public static void check(final ISchedulingRule rule,
			final TextGridObject object, final String message,
			final Object... args) {

		final Job currentJob = Job.getJobManager().currentJob();
		if (currentJob == null)
			return;
		final ISchedulingRule currentRule = currentJob.getRule();
		if (currentRule == null)
			return;
		if (rule.isConflicting(currentRule))
			throw new SchedulingException(object, message, args);
	}

}
