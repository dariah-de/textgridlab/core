/**
 * The model representing TextGridRep objects and related classes.
 * 
 * 
 * 
 * <h3>TextGridObjects</h3>
 * <p>
 * The data models core class is
 * {@link info.textgrid.lab.core.model.TextGridObject}. Each
 * {@link info.textgrid.lab.core.model.TextGridObject} represents an object in
 * the TextGridRep. Only objects that are or have been used in the current lab
 * session are available, so you may have to ask
 * {@linkplain info.textgrid.lab.search TG-search} for available stuff meeting
 * individual criteria.
 * <p>
 * More information can be found in the documentation to
 * {@link info.textgrid.lab.core.model.TextGridObject} and in the model <a
 * href="../../../../../../guide/model/index.html">overview</a> in the
 * Developers' Guide.
 * 
 * <h3>TextGridProjects</h3>
 * <p>
 * A {@link info.textgrid.lab.core.model.TextGridProject} is (to be) the general
 * accessor to projects in TextGrid.
 * </p>
 * 
 * 
 * @see info.textgrid.lab.core.model.TextGridObject
 * @see info.textgrid.lab.core.model.util
 */
package info.textgrid.lab.core.model;