package info.textgrid.lab.core.model;

import java.util.HashSet;
import java.util.Set;

/**
 * The model for the user / role Assignment, used e.g. in the UserManagement and
 * in TextGridProject. This version 2 is not a tree/recursive anymore!
 * 
 * @author martin
 */
public class UserRole2 {
	protected String ePPN;
	// We'd better not supply a cleartext name as the RBACSession namecache is more current.
//	protected String name; 
	protected Set<String> roles;

	public UserRole2(String ePPN) {
		this.ePPN = ePPN;
		roles = new HashSet<String>();
	}

	public String getePPN() {
		return ePPN;
	}

	public void setePPN(String ePPN) {
		this.ePPN = ePPN;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setAllRoles(Set<String> roles) {
		this.roles = roles;
	}

	public void addRole(String role) {
		roles.add(role);
	}

	public void deleteRole(String role) {
		roles.remove(role);
	}

}
