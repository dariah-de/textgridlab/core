package info.textgrid.lab.core.model;

import org.eclipse.core.resources.WorkspaceJob;


/**
 * The class provides the opportunity to group and locate workspace job instances 
 * by a job family.
 * 
 * @author queens
 */
public abstract class IndexedWorkspaceJob extends WorkspaceJob {
	
	private String  familyName;
	
	public IndexedWorkspaceJob(String name) {
		super(name);
	}
	
	public boolean belongsTo(Object family) {
        return familyName.equals(family);
     }

	public String getFamilyNameName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

}
