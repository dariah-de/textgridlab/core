package info.textgrid.lab.core.model;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * FIXME move somewhere useful
 * 
 */
public class AggregationReader {

	private static final QName RESOURCE = new QName(
			"http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource"); //$NON-NLS-1$ //$NON-NLS-2$
	private static QName AGGREGATES = new QName(
			"http://www.openarchives.org/ore/terms/", "aggregates"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * Reads the given aggregation to a list of (aggregated) URIs
	 * 
	 * @param aggregation
	 *            the aggregation to throw
	 * @param strict
	 *            if true errors during processing will always be thrown, if
	 *            false we will ignore errors inside 'ore:aggregates' elements.
	 * @return a list of URIs.
	 * @throws XMLStreamException
	 *             if something goes wrong parsing the file
	 * @throws URISyntaxException
	 *             if we find a non-URI where we expect an URI
	 */
	public static List<URI> read(final Source aggregation, final boolean strict)
			throws XMLStreamException, URISyntaxException {
		final List<URI> result = Lists.newLinkedList();
		final XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_COALESCING, true);
		final XMLEventReader reader = factory.createXMLEventReader(aggregation);

		while (reader.hasNext()) {
			final XMLEvent event = reader.nextEvent();
			if (event.isStartElement()) {
				final StartElement startElement = event.asStartElement();
				if (AGGREGATES.equals(startElement.getName()))
					try {
						// TG-1439: Try new form first, if the attribute is not
						// present, we try the old form:
						// <ore:aggregates>textgrid:4711</ore:aggregates>
						final Attribute resourceAttribute = startElement
								.getAttributeByName(RESOURCE);
						if (resourceAttribute != null) {
							final URI uri = new URI(resourceAttribute
									.getValue().trim());
							result.add(uri);
						} else {
							final String elementText = reader.getElementText();
							final URI uri = new URI(elementText.trim());
							result.add(uri);
						}
					} catch (final XMLStreamException e) {
						if (strict)
							throw e;
					} catch (final URISyntaxException e) {
						if (strict)
							throw e;
					}
			}
		}
		return result;
	}

	public static List<URI> read(final TextGridObject object,
			final boolean strict) throws CoreException {
		final IFile file = AdapterUtils.getAdapter(object, IFile.class);
		if (file != null) {
			final InputStream stream = file.getContents();
			try {
				return read(new StreamSource(stream), false);
			} catch (final XMLStreamException e) {
				throw new AggregationException(NLS.bind(
						Messages.AggregationReader_InvalidContent, object), e);
			} catch (final URISyntaxException e) {
				throw new AggregationException(NLS.bind(
						Messages.AggregationReader_InvalidURI, object), e);
			} finally {
				try {
					stream.close();
				} catch (final IOException e) {
					throw new AggregationException(NLS.bind(
							Messages.AggregationReader_CloseFailed, object), e);
				}
			}
		}
		return ImmutableList.of();
	}

	/**
	 * Lists an aggregation (via TG-search).
	 * 
	 * @param aggregation
	 *            The aggregation to list.
	 * @param onlyAccessible
	 *            If <code>true</code>, objects that are not accessible with the
	 *            current session ID are silently skipped. Otherwise, there are
	 *            entries for those objects, but those entries will not have
	 *            metadata, and an exception will probably be thrown when trying
	 *            to access these objects or their metadata.
	 * @return a list of references to all those objects that are directly
	 *         aggregated by the aggregation and that the user is allowed to
	 *         access. All accessible objects referenced in this list have been
	 *         initialized with metadata.
	 * @throws OfflineException
	 *             when the configuration server cannot be accessed.
	 * @throws CoreException
	 *             when something goes wrong with reading or initializing a
	 *             {@link TextGridObject}.
	 * @category EXPERIMENTAL
	 */
	public static List<TGObjectReference> list(
			final TextGridObject aggregation, final boolean onlyAccessible)
			throws OfflineException, CoreException {
		SearchClient searchClient;
		List<TGObjectReference> objects;

		if (aggregation.isPublic())
			searchClient = new SearchClient(ConfClient.getInstance().getValue(
					ConfservClientConstants.TG_SEARCH_PUBLIC));
		else
			searchClient = new SearchClient(ConfClient.getInstance().getValue(
					ConfservClientConstants.TG_SEARCH));

		searchClient.setSid(RBACSession.getInstance().getSID(false));
		final Response response = searchClient.listAggregation(aggregation
				.getURI().toASCIIString());
			objects = Lists.newArrayListWithCapacity(response.getResult().size());
		final List<ResultType> results = response.getResult();
		for (final ResultType result : results)
			if (result.getObject() != null) {
				TGObjectReference reference = new TGObjectReference(result.getTextgridUri(),
						TextGridObject.getInstance(result.getObject(), true,
								false));
				objects.add(reference);
			} else if (!onlyAccessible)
				objects.add(new TGObjectReference(URI.create(result
						.getTextgridUri())));

		return objects;
	}

	public static class AggregationException extends CoreException {
		private static final long serialVersionUID = 5457750241923815265L;

		public AggregationException(final String message, final Throwable cause) {
			super(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					message == null && cause != null ? cause.getMessage()
							: message, cause));
		}
	}

}
