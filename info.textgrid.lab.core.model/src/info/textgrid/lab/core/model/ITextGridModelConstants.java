package info.textgrid.lab.core.model;

/**
 * Holds some common constants for the Model and related implementations.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for <a
 *         href="http://www.textgrid.info/">TextGrid</a> (tv)
 */
public interface ITextGridModelConstants {
	/**
	 * The URI schema for TextGridObjects.
	 */
	public static final String SCHEMA = "textgrid"; //$NON-NLS-1$

	/**
	 * The URI schema for handle URIs by which published objects can also be
	 * identified
	 */
	public static final String SCHEMA_HANDLE = "hdl"; //$NON-NLS-1$

	/**
	 * The URI schema for TextGridObjects that have not yet been committed to
	 * TGcrud.
	 */
	public static final String SCHEMA_NEWFILE = "textgrid-newfile"; //$NON-NLS-1$
	
	/**
	 * The URI schema for the EFS internal references to TextGridObjects.
	 */
	public static final String SCHEMA_EFS = "textgrid-efs"; //$NON-NLS-1$
	
	/**
	 * The URI authority for EFS internal references to textGridObjects.
	 */
	public static final String AUTHORITY_EFS = "localhost"; //$NON-NLS-1$
}
