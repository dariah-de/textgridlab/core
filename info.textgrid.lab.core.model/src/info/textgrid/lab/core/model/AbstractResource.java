package info.textgrid.lab.core.model;

import static info.textgrid.lab.core.model.Activator.PLUGIN_ID;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.util.ModelUtil;
import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.lab.log.logsession;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.GetRightsRequest;
import info.textgrid.namespaces.middleware.tgauth.OperationsetResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;

/**
 * An abstract superclass for all TextGrid resources (currently, projects and
 * objects)
 * 
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 */
public abstract class AbstractResource extends PlatformObject implements
		ITextGridPermission {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.lab.core.model.ITextGridPermission#assertPermission(int,
	 * java.lang.String, java.lang.Object[])
	 */
	public void assertPermission(int permission, String errorMessage,
			Object... arguments) throws CoreException {
		if (!hasPermissions(permission))
			throw new CoreException(new Status(IStatus.ERROR, PLUGIN_ID,
					MessageFormat.format(errorMessage, arguments)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see info.textgrid.lab.core.model.ITextGridPermission#hasPermissions(int)
	 */
	public boolean hasPermissions(int rights) throws CoreException {
		return (getPermissions() & rights) == rights;
	}

	/**
	 * Helper method that retrieves the permissions for the ressource identified
	 * by the given ID from TG-auth.
	 * 
	 * @param resourceID
	 *            Identifier for the resource, this is a TextGridObject's URI or
	 *            a project ID.
	 * @return the permissions for the resource.
	 * @throws CoreException
	 */
	public int retrievePermissions(String resourceID) throws CoreException {
		try {
			ModelUtil.checkNonUIThread("retrieving permissions for {0}", resourceID);

			PortTgextra tgAuth = TgAuthClientUtilities.getTgAuthServiceStub();
			GetRightsRequest request = new GetRightsRequest();
			request.setAuth(RBACSession.getInstance().getSID(false));
			request.setLog(logsession.getInstance().getloginfo());
			request.setResource(resourceID);
			OperationsetResponse response = tgAuth.getRights(request);

			int permissions = 0;

			if (response != null && response.getOperation() != null)
				for (String permission : response.getOperation())
					permissions |= getNumericPermission(permission);

			return permissions;

		} catch (AuthenticationFault e) {
			throw new CoreException(
					new Status(
							IStatus.ERROR,
							PLUGIN_ID,
							MessageFormat
									.format(Messages.AbstractResource_MetaNoPermission,
											this, e.getMessage(),
											e.getFaultInfo())));
		}
	}

	/**
	 * Translates a single permission string to a numeric value as defined in
	 * {@link ITextGridPermission}.
	 * 
	 * @param symbolicPermission
	 * @return a numeric value from the constants in {@link ITextGridPermission}
	 *         or 0 if you pass in an unrecognized string.
	 */
	protected static int getNumericPermission(final String symbolicPermission) {
		if ("create".equals(symbolicPermission)) //$NON-NLS-1$
			return CREATE;
		else if ("read".equals(symbolicPermission)) //$NON-NLS-1$
			return READ;
		else if ("update".equals(symbolicPermission)) //$NON-NLS-1$
			return UPDATE;
		else if ("write".equals(symbolicPermission)) //$NON-NLS-1$
			return UPDATE;
		else if ("publish".equals(symbolicPermission)) //$NON-NLS-1$
			return PUBLISH;
		else if ("delegate".equals(symbolicPermission)) //$NON-NLS-1$
			return DELEGATE;
		else if ("delete".equals(symbolicPermission)) //$NON-NLS-1$
			return DELETE;

		return 0;
	}

}
