package info.textgrid.lab.core.model;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TextGridFaultType;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Hashtable;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;


/**
 * The TextGridErrorHandler class handles actually all thrown exceptions 
 * which arise when accessing TG-crud. 
 * Implemented Rules:
 * 1) Exceptions concerning TG-objects which are already deleted, aren't considered.
 * 2) Exceptions with the same URI and TextGridFaultType, which are thrown one after 
 * 	another aren't considered.
 * 
 * @author Frank Queens
 */
public class TextGridErrorHandler {

	private static TextGridErrorHandler _instance = new TextGridErrorHandler();
	private static Hashtable<String, TextGridError> errorList = new Hashtable<String,TextGridError>();
	private final int SPANOFTIME = 2000; // 2 seconds
	private final String INFO_DELETED = Messages.TextGridErrorHandler_RepetativeErrorXonY;
	private final String INFO_REPETITIVE = Messages.TextGridErrorHandler_AlreadyDeletedErrorXonY;
	
	
	private static final String[] TG_FAULT_CODES = {
			Messages.TextGridErrorHandler_UnknownFault,	// 0 
			Messages.TextGridErrorHandler_AuthFault, // 1
			Messages.TextGridErrorHandler_IoFault, // 2
			Messages.TextGridErrorHandler_ObjectNotFoundFault, // 3
			Messages.TextGridErrorHandler_MetadataParseFault, // 4
			Messages.TextGridErrorHandler_RelationExistsFault, // 5
			Messages.TextGridErrorHandler_UpdateConflictFault, // 6
			Messages.TextGridErrorHandler_ProtocolNotImplementedFault, // 7
			Messages.TextGridErrorHandler_NoConfigurationFault, // 8
			Messages.TextGridErrorHandler_InternalServiceError // 9
	};
	
	// TODO move this somewhere more useful, and introduce wider use.
	public static String formatErrorMessage(final TextGridFaultType faultType, final URI uri, final boolean addDetail) {
		int faultNo = faultType.getFaultNo();
		if (faultNo >= TG_FAULT_CODES.length)
			faultNo = 0;
		final TextGridObject object = TextGridObject.getInstanceOffline(uri);
		String result = MessageFormat.format(TG_FAULT_CODES[faultNo], object);
		if (addDetail)
			result = result + " \n" +  //$NON-NLS-1$
					MessageFormat.format(Messages.TextGridErrorHandler_DetailMessage, faultType.getFaultMessage(), faultType.getCause());
		return result;
	}
	
	class ErrorKey {
		private URI uri;
		private String localCause;
		
		public ErrorKey(URI uri, String localCause) {
			this.uri = uri;
			this.localCause = localCause;
		}
		
		public String toString() {
			return this.uri + this.localCause;
		}
	}
	
	class TextGridError {
		private Throwable error;
		private TextGridFaultType faultType;
		private long time;
		
		public TextGridError(Throwable error, TextGridFaultType faultType) {
			this.error = error;
			this.faultType = faultType;
			this.time = System.currentTimeMillis(); 
		}
		
	}
	
	public static TextGridErrorHandler getInstance() {
		return _instance;
	}
	
	
	public IStatus handleError(Throwable tgError, TextGridFaultType errorFaultType, URI uri) {
		boolean showError = false;	// show an error message to the user, i.e. first time message
		
		try {
			if (uri != null && !TextGridObject.getInstance(uri, false).isDeleted()) {
				ErrorKey errorKey = new ErrorKey(uri, errorFaultType.getCause());
				TextGridError listError = errorList.get(errorKey.toString());
				if (listError == null) {				
					showError = true;
				} else {
					// a repetitive error?
					if ((listError.time + SPANOFTIME) < System.currentTimeMillis()) {
						showError = true;
					}
				}	
				errorList.put(errorKey.toString(), new TextGridError(tgError, errorFaultType));
				
				if (showError) {
					Status status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
							formatErrorMessage(errorFaultType, uri,
									true), tgError);
					StatusManager.getManager().handle(status, StatusManager.SHOW | StatusManager.LOG);
					return status;
				}
				else 
					return Activator.handleProblem(IStatus.INFO, tgError, INFO_REPETITIVE, 
							tgError.getClass().getSimpleName(), uri); 
			} else {
				return Activator.handleProblem(IStatus.INFO, tgError, INFO_DELETED, 
												tgError.getClass().getSimpleName(), uri); 
			}
		} catch (CrudServiceException e) {
			return Activator.handleError(tgError, Messages.TextGridErrorHandler_ErrorWhileErrorHandling,
					e.getClass().getSimpleName(), uri, 
					CrudServiceException.getFaultType(e, TextGridFaultType.class).getFaultMessage(), 
					CrudServiceException.getFaultType(e, TextGridFaultType.class).getCause());
		}
	}	

}
