/**
 * 
 */
package info.textgrid.lab.core.model;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TextGridFaultType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

/**
 * Exception thrown by classes accessing TGcrud when the service call fails.
 * These classes have a {@linkplain #getCause() cause} that contains the info
 * returned by the service call. They are Eclipse's {@link CoreException}, so
 * they are allowed throughout some parts of the API. You may call
 * {@link #getStatus()} to retrieve a status object.
 * 
 * <p>
 * <strong>FIXME</strong> These exceptions automagically call
 * {@link Activator#handleError(Throwable, String, Object...)} on creation
 * because the actually thrown exceptions are sometimes silently caught by
 * Eclipse code. This is not a good idea and should be fixed.
 * </p>
 * 
 * @author tv
 */
public class CrudServiceException extends CoreException {

	private static final long serialVersionUID = 5608618934688718145L;

	public CrudServiceException(Throwable cause, URI uri) {
		/*
		 * super(new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind("An {0}
		 * occured during an operation on {1}.", cause.getClass().getName(),
		 * uri), cause));
		 */
		super(compileStatus(cause, uri));
		Activator.logServiceCall(Messages.CrudServiceException_ServiceCallFailed, uri, cause, getStatus());
	}

	protected static IStatus compileStatus(Throwable cause, URI uri) {
		TextGridFaultType faultType = getFaultType(cause, TextGridFaultType.class);
		IStatus status = null;
		if (faultType != null) {
			status = TextGridErrorHandler.getInstance().handleError(cause, faultType, uri);
		} else {
			status = Activator.handleError(cause, Messages.CrudServiceException_ErrorDuringOperation, cause.getClass().getName(), uri,
					cause.getLocalizedMessage());
		}
		return status;

	}

	/**
	 * Extracts the fault type for one of the Axis-generated exceptions.
	 * 
	 * @param <T>
	 *            the fault type we expect. E.g., {@link TextGridFaultType}.
	 * @param cause
	 *            the exception from which to extract the fault type. This must
	 *            be one of the Axis2-generated exceptions from the stubs, e.g.,
	 *            {@link IoFaultException}.
	 * @param faultTypeClass
	 *            the class of the fault type we expect. E.g.,
	 * 
	 *            <code>{@linkplain TextGridFaultType TextGridFaultType}.<strong>class</strong></code>
	 *            .
	 * @return an instance of <var>T</var> as extracted from the given
	 *         exception, or <code>null</code> if none such instance could be
	 *         found.
	 * @TODO This class method should be refactored to some more sensible place.
	 */
	public static <T> T getFaultType(final Throwable cause, final Class<T> faultTypeClass) {

		if (cause != null) {
			try {
				final Method getFaultInfo = cause.getClass().getMethod("getFaultInfo"); //$NON-NLS-1$
				final Object faultInfo = getFaultInfo.invoke(cause);
				if (faultInfo != null && faultTypeClass.isAssignableFrom(faultInfo.getClass())) {
					@SuppressWarnings("unchecked")	// checked by isAssignableFrom above
					T faultType = (T) faultInfo;
					return faultType;
				}

			} catch (final SecurityException e) {
				Activator.handleProblem(IStatus.WARNING, e,
						"Could not extract a fault type from {0} due to security restrictions: {1}", cause, e.getMessage()); //$NON-NLS-1$
			} catch (final NoSuchMethodException e) {
				// Expected: Someone passed in something inappropriate.
			} catch (final IllegalArgumentException e) {
				// Expected: Someone passed in something inappropriate.
			} catch (final IllegalAccessException e) {
				Activator.handleProblem(IStatus.WARNING, e,
						"Could not extract a fault type from {0} due to security restrictions: {1}", cause, e.getMessage()); //$NON-NLS-1$
			} catch (final InvocationTargetException e) {
				// Expected: Someone passed in something inappropriate.
			}
		}

		return null;
	}

}
