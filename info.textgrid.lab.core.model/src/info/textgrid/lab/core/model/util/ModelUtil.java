package info.textgrid.lab.core.model.util;

import info.textgrid.lab.core.model.Activator;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.statushandlers.StatusManager;

public final class ModelUtil {

	public static class InUIThreadException extends IllegalStateException {
		private static final long serialVersionUID = 1976037644136226877L;

		public InUIThreadException(final String message,
				final Object... arguments) {
			super(MessageFormat.format(message, arguments));
		}
	}

	private ModelUtil() {
	};

	/**
	 * Returns true if called from a user interface thread.
	 */
	public static boolean inUIThread() {
		return Display.getCurrent() != null;
	}

	/**
	 * Logs and returns an warning if we are in a non-UI thread.
	 * 
	 * @param message
	 *            the message for the status returned and logged. See
	 *            {@link MessageFormat#format(String, Object...)}
	 * @param arguments
	 *            arguments to the message.
	 * @return {@link Status#OK_STATUS} if we are in a non-UI thread, or a
	 *         warning status (that has also been logged) called from an UI
	 *         thread.
	 */
	public static IStatus checkNonUIThread(final String message,
			final Object... arguments) {
		if (inUIThread()) {
			InUIThreadException exception = new InUIThreadException(message,
					arguments);
			IStatus warning = new Status(IStatus.WARNING, Activator.PLUGIN_ID, "UI thread misuse: " + exception.getMessage(), exception);
			if (Activator.isDebugging(Activator.DEBUG_UITHREAD))
				StatusManager.getManager().handle(warning);
			return warning;
		} else
			return Status.OK_STATUS;
	}

	/**
	 * @param message
	 * @param arguments
	 * @throws InUIThreadException when called from a UI thread.
	 */
	public static void assertNonUIThread(final String message,
			final Object... arguments) {
		if (inUIThread())
			throw new InUIThreadException(message, arguments);
	}

}
