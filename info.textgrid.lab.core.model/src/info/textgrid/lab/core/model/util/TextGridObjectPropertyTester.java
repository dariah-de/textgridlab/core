package info.textgrid.lab.core.model.util;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;

import java.text.MessageFormat;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * This is a generic property tester for TextGridObjects. Currently the
 * following properties are supported:
 * <dl>
 * <dt>stringValue</dt>
 * <dd>An XPath expression to evaluate on the {@link TextGridObject}'s current
 * metadata state, see {@link TextGridObject#getStringValue(String, boolean)}. The given
 * argument must be an XPath expression to evaluate on the current expression,
 * where the namespace prefix 'tg:' is already bound to the metadata namespace.
 * If an expectedValue != null is given, it must be a string which is compared
 * against the string result.</dd>
 * <dt>stringValueFast</dt>
 * <dd>Like <var>stringValue</var>, but accuracy is given up for speed. I.e., if
 * the TextGridObject hasn't loaded the {@linkplain TextGridObject#isComplete()
 * complete} metadata set yet, it will not be loaded automatically. Please note
 * that incomplete TextGridObjects should be queried using
 * <code>.//<var>property</var></code> since correct nesting of the metadata
 * values isn't guaranteed.
 * </dl>
 * 
 * @author vitt
 */
public class TextGridObjectPropertyTester extends PropertyTester {

	public TextGridObjectPropertyTester() {
	}

	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		if (receiver instanceof TextGridObject) {
			TextGridObject textGridObject = (TextGridObject) receiver;

			if ("stringValue".equals(property) || "stringValueFast".equals(property)) { //$NON-NLS-1$ //$NON-NLS-2$
				boolean forceComplete = !"stringValueFast".equals(property); //$NON-NLS-1$

				if (args.length == 0)
					warn(receiver, property, args, expectedValue, "No args given."); //$NON-NLS-1$
			}
		}

		return false;
	}


	private void warn(Object receiver, String property, Object[] args, Object expectedValue, String message) {
		Status status = new Status(IStatus.WARNING, Activator.PLUGIN_ID, MessageFormat.format(
				"{0}\n  Testing {1} for property {2} with arguments {3} and expected value {4}.", message, receiver, property, //$NON-NLS-1$
				args,
				expectedValue));
		StatusManager.getManager().handle(status);
	}

}
