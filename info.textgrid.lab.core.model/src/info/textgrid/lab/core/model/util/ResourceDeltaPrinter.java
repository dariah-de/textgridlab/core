package info.textgrid.lab.core.model.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResourceDelta;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class ResourceDeltaPrinter {
	
	private IResourceDelta delta;
	private int level = 0;
	private static final ImmutableMap<Integer, String> FLAG_LEGEND = new ImmutableMap.Builder<Integer, String>().put(
			IResourceDelta.ADDED, "ADDED").put(IResourceDelta.REMOVED, "REMOVED").put(IResourceDelta.CHANGED, "CHANGED").put( //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			IResourceDelta.ADDED_PHANTOM, "ADDED_PHANTOM").put(IResourceDelta.REMOVED_PHANTOM, "REMOVED_PHANTOM").put( //$NON-NLS-1$ //$NON-NLS-2$
			IResourceDelta.CONTENT, "CONTENT").put(IResourceDelta.MOVED_FROM, "MOVED_FROM") //$NON-NLS-1$ //$NON-NLS-2$
			.put(IResourceDelta.MOVED_TO, "MOVED_TO").put(IResourceDelta.COPIED_FROM, "COPIED_FROM").put(IResourceDelta.OPEN, //$NON-NLS-1$ //$NON-NLS-2$
					"OPEN").put(IResourceDelta.TYPE, "TYPE").put(IResourceDelta.SYNC, "SYNC") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			.put(IResourceDelta.MARKERS, "MARKERS").put(IResourceDelta.REPLACED, "REPLACED").put(IResourceDelta.DESCRIPTION, //$NON-NLS-1$ //$NON-NLS-2$
					"DESCRIPTION").put(IResourceDelta.ENCODING, "ENCODING").put(IResourceDelta.LOCAL_CHANGED, "LOCAL_CHANGED") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			.build();
	
	
	public static ResourceDeltaPrinter build(IResourceDelta delta) {
		return new ResourceDeltaPrinter(delta, 0);
	}
	
	protected static ResourceDeltaPrinter build(IResourceDelta delta, int level) {
		return new ResourceDeltaPrinter(delta, level);
	}

	protected static List<ResourceDeltaPrinter> build(IResourceDelta[] deltas,
			int level) {
		List<ResourceDeltaPrinter> result = new ArrayList<ResourceDeltaPrinter>(
				deltas.length);

		for (IResourceDelta delta : deltas)
			result.add(build(delta, level));

		return result;
	}

	private ResourceDeltaPrinter(IResourceDelta delta, int level) {
		this.level = level;
		this.delta = delta;
	}
		
	
	private String getKindStr() {
		switch (delta.getKind()) {
		case IResourceDelta.ADDED: return "added"; //$NON-NLS-1$
		case IResourceDelta.REMOVED: return "removed"; //$NON-NLS-1$
		case IResourceDelta.CHANGED:
			return "changed"; //$NON-NLS-1$
		case IResourceDelta.ADDED_PHANTOM:
			return "added (phantom)"; //$NON-NLS-1$
		case IResourceDelta.REMOVED_PHANTOM:
			return "removed (phantom)"; //$NON-NLS-1$
		}
		return "unknown change (" + delta.getKind() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public String flagsToString(int flags) {
		List<String> flagNames = Lists.newArrayListWithCapacity(FLAG_LEGEND.size());
		for (Integer flag : FLAG_LEGEND.keySet())
			if ((flags & flag) != 0)
				flagNames.add(FLAG_LEGEND.get(flag));
		return Joiner.on(" | ").join(flagNames); //$NON-NLS-1$
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i <= level; i++)
			result.append(' ');

		result.append(getKindStr()).append(' ').append(delta.getResource());
		result.append(", flags: ").append(flagsToString(delta.getFlags())); //$NON-NLS-1$
		result.append(" (").append(Integer.toHexString(delta.getFlags())) //$NON-NLS-1$
				.append(")"); //$NON-NLS-1$
		IResourceDelta[] deltas = delta.getAffectedChildren();
		if (deltas != null && deltas.length > 0)
			for (ResourceDeltaPrinter printer : build(deltas, level + 1))
				result.append('\n').append(printer);

		return result.toString();
	}
	
	
	

}
