package info.textgrid.lab.core.model.util;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;

/**
 * A help class to convert strings to OMelements because The standard solution
 * <b>AXIOMUtil.stringToOM</b> is buggy with UTF-8 strings!
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class StringToOM {

	public static OMElement getOMElement(String theString)
			throws XMLStreamException, FactoryConfigurationError,
			UnsupportedEncodingException {
		// create the parser
		XMLStreamReader parser = XMLInputFactory.newInstance()
				.createXMLStreamReader(new StringReader(theString));
		// create the builder
		StAXOMBuilder builder = new StAXOMBuilder(parser);

		// get the root element
		OMElement elem = builder.getDocumentElement();

		return elem;
	}
}
