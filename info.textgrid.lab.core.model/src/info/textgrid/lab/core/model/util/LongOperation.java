package info.textgrid.lab.core.model.util;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

/**
 * A wrapper for long-running operations (like grid access) that should never be
 * run from the UI thread.
 * 
 * This is a convenience class that mainly takes care of result and exception
 * handling, freeing the user from writing long try-catch-blocks unwrapping an
 * rethrowing exceptions, but it has been designed for use for common use cases
 * within the model.
 * 
 * <p>
 * Implementors instantiate a subclass of this and implement its run method with
 * their functionality. Then, they call {@link #runInJob(IProgressMonitor, String)} on it.
 * </p>
 * <p>
 * Here is an example on how to use it:
 * </p>
 * 
 * <pre>
 * public InputStream openInputStream(final int options,
 * 		final IProgressMonitor monitor) throws CoreException {
 * 	return new LongOperation&lt;InputStream&gt;() {
 * 
 * 		&#064;Override
 * 		public InputStream run(IProgressMonitor monitor) throws CoreException {
 * 			return doOpenInputStream(options, monitor);
 * 		}
 * 
 * 	}.runInJob(monitor);
 * }
 * </pre>
 * 
 * 
 * @author tv
 * 
 * @param <T>
 *            the return value of the run method (that gets passed through
 *            {@link #busyCursor(IProgressMonitor)} etc.)
 */
public abstract class LongOperation<T> {

	protected class JoiningRunnable implements IRunnableWithProgress {

		private Job job;
		
		public JoiningRunnable(final Job wrappingJob) {
			this.job = wrappingJob;
		}

		public void run(IProgressMonitor monitor)
				throws InvocationTargetException, InterruptedException {
			try {
				job.join();
			} catch (Exception e) {
				throw new InvocationTargetException(e);
			}
		}

	}

	/**
	 * A long-running operation that should in no case be executed in the UI thread.
	 * 
	 * @param monitor A progress monitor to report progress to. May be <code>null</code>.
	 * @return The result.
	 * @throws CoreException
	 */
	public abstract T run(IProgressMonitor monitor) throws CoreException;

	/**
	 * Runs the {@link #run(IProgressMonitor)} method as appropriate:
	 * <ul>
	 * <li>When called from a non-UI thread, directly calls the
	 * {@link #run(IProgressMonitor)} method.</li>
	 * <li>When called from the UI thread, aquires the workbench's
	 * {@linkplain IProgressService progress service} and runs the method
	 * through its
	 * {@link IProgressService#busyCursorWhile(IRunnableWithProgress)} method.</li>
	 * </ul>
	 * In both cases, the GUI will stay reactive. 
	 * 
	 * @param monitor a progress monitor that will be used when the method is <em>not</em> called from the UI thread.
	 * 
	 * @return the result of the {@link #run(IProgressMonitor)} method.
	 * @throws LongOperationException when {@link #run(IProgressMonitor)} has thrown an unknown exception (which it shouldn't)
	 * @throws CoreException when the {@link #run(IProgressMonitor)} has thrown a CoreException
	 * @throws RuntimeException when {@link #run(IProgressMonitor)} has thrown a {@link RuntimeException}
	 * @deprecated in some cases, might trigger TG-360 on Macs. Also displays a large enervating popup dialog. Use {@link #runInJob(IProgressMonitor, String)} instead.
	 */
	public T busyCursor(IProgressMonitor monitor) throws CoreException {
	
		if (!ModelUtil.inUIThread())
			setResult(run(monitor));
		else
			try {
				PlatformUI.getWorkbench().getProgressService().busyCursorWhile(
						createRunnable());
			} catch (InvocationTargetException e) {
				LongOperationException.fail(e);
			} catch (InterruptedException e) {
				LongOperationException.fail(e);
			}
			return getResult();
	}
	
	/**
	 * Run the operation in a background job and wait for the background job to finish, when called from an UI thread. Otherwise, just run the operation.
	 * 
	 * Implemented for TG-360
	 * 
	 * FIXME this needs to be improved, esp. the progress reporting stuff should be merged.
	 * See also TG-328!
	 * 
	 * @param monitor a monitor to report progress to
	 * @param message the job title if this operation is forced to the background
	 * @return the result of the wrapped operation (see {@link #run(IProgressMonitor)})
	 * @throws CoreException in case of an error, see {@link #run(IProgressMonitor)} and {@link LongOperationException}
	 */
	public T runInJob(IProgressMonitor monitor, String message) throws CoreException {
		
		setResult(run(monitor));
		
		// FIXME this is a temporary work-around for TG-591. Please see the comments there for a discussion of the problem …
		
//		if (!ModelUtil.inUIThread())
//			setResult(run(monitor));
//		else {
//			if (message == null)
//				message = "Processing ...";
//			RunnableWrappingJob wrappingJob = new RunnableWrappingJob(createRunnable(), message);
//			wrappingJob.schedule();
//			if (monitor == null)
//				monitor = new NullProgressMonitor();
//			try {
//				ModalContext.run(new JoiningRunnable(wrappingJob), true, monitor, Display.getCurrent());
//			} catch (InvocationTargetException e) {
//				LongOperationException.fail(e.getCause());
//			} catch (InterruptedException e) {
//				LongOperationException.fail(e);
//			}
//		}
		
		return getResult();
	}
	

	/**
	 * A job that wraps a {@link IRunnableWithProgress} and adapts the API for error reporting and cancellation. 
	 * 
	 * @author vitt
	 *
	 */
	protected class RunnableWrappingJob extends Job {

		private IRunnableWithProgress runnable;

		public RunnableWrappingJob(IRunnableWithProgress runnable, String title) {
			super(title);
			this.runnable = runnable;
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			try {
				try {
					runnable.run(monitor);
				} catch (InvocationTargetException e) {
					LongOperation.LongOperationException.fail(e.getCause());	// TODO improve exception handling
				} catch (InterruptedException e) {
					return Status.CANCEL_STATUS;
				}
			} catch (CoreException e) {
				return e.getStatus();
			}
			return Status.OK_STATUS;
		}
	}

	/**
	 * @return the result of the {@link #run(IProgressMonitor)} method after that has run or <code>null</code>.
	 */
	protected T getResult() {
		return result;
	}

	/**
	 * stores the result of the {@link #run(IProgressMonitor)} method.
	 * @param result
	 */
	private void setResult(final T result) {
		this.result = result;
	}

	/**
	 * An exception that is thrown when an error occurs while running the {@link LongOperation}.
	 * @author vitt
	 *
	 */
	public static class LongOperationException extends CoreException {

		private static final long serialVersionUID = -4799958323881890920L;

		public LongOperationException(Throwable e) {
			super(Status.CANCEL_STATUS);
		}

		public static void fail(Throwable throwable) throws CoreException {
			if (throwable.getCause() instanceof CoreException)
				throw (CoreException) throwable.getCause();
			else if (throwable.getCause() instanceof RuntimeException)
				throw (RuntimeException) throwable.getCause();
			else
				throw new LongOperationException(throwable.getCause());
		}

	}

	private class OperationRunnable implements IRunnableWithProgress {

		public void run(IProgressMonitor monitor)
				throws InvocationTargetException, InterruptedException {
			try {
				// TODO setup monitor etc.
				setResult(LongOperation.this.run(monitor));
			} catch (CoreException e) {
				setResult(null);
				throw new InvocationTargetException(e);
			}
		}

	}

	private T result;

	private IRunnableWithProgress createRunnable() {
		return new OperationRunnable();
	}

}
