package info.textgrid.lab.core.model;

import java.rmi.RemoteException;

/**
 * A service exception that occurred when calling the RBAC.
 */
public class RBACServiceException extends RemoteException {

	private static final long serialVersionUID = -8594429440544976379L;

	public RBACServiceException() {
		super();
	}

	public RBACServiceException(String s, Throwable cause) {
		super(s, cause);
	}

	public RBACServiceException(String s) {
		super(s);
	}

}
