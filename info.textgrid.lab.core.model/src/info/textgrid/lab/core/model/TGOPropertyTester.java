package info.textgrid.lab.core.model;

import org.eclipse.core.expressions.PropertyTester;

public class TGOPropertyTester extends PropertyTester {

	public TGOPropertyTester() {
		// TODO Auto-generated constructor stub
	}

	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (receiver instanceof TextGridObject)
			if ("hasPreviousVersion".equals(property))
				if (((TextGridObject) receiver).getPreviousVersion() != null)
					return true;
		return false;
	}

}
