package info.textgrid.lab.core.model;

public interface IChildListChangedListener {
	public void childListChanged(IChildListParent parent);

}
