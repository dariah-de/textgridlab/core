/**
 * 
 */
package info.textgrid.lab.core.model;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;


/**
 * An operation on the project file failed. It's up to the caller to interact with the fellow users.
 */
public class ProjectFileException extends CoreException {
	
	private static final long serialVersionUID = 7584176592676754625L;

	public static enum Operation {
		CREATING, READING, SAVING
	}
	
	public static String[] MESSAGES = {
		Messages.ProjectFileException_CreateError,
		Messages.ProjectFileException_ReadError,
		Messages.ProjectFileException_SaveError
	};
	
	private Operation operation;
	private TextGridProject project;
	
	/**
	 * Returns the operation type on which this exception occurred.
	 */
	public Operation getOperation() {
		return operation;
	}
	
	/**
	 * Returns the project on which this exception occurred.
	 */
	public TextGridProject getProject() {
		return project;
	}

	/**
	 * Creates a new project file exception.
	 * @param operation	The kind of operation during that the problem occurred.
	 * @param project	The {@link TextGridProject} for which the project file operation occurred.
	 * @param cause		The exception that caused the trouble (or null).
	 * @param message	The first part of the message to display to the user (or null).
	 * @param arguments	Arguments to format into the message, see {@link NLS#bind(String, Object[])}.
	 */
	public ProjectFileException(Operation operation, TextGridProject project, Throwable cause,
			String message, Object... arguments) {
		super(createStatus(operation, project, cause, message, arguments));
		this.operation = operation;
		this.project = project;
	}
	
	/**
	 * Creates a new project file exception.
	 * 
	 * @param operation
	 *            The kind of operation during that the problem occurred.
	 * @param project
	 *            The {@link TextGridProject} for which the project file
	 *            operation occurred.
	 * @param cause
	 *            The exception that caused the trouble (or null).
	 */
	public ProjectFileException(Operation operation, TextGridProject project, Throwable cause) {
		super(createStatus(operation, project, cause, null));
		this.operation = operation;
		this.project = project;
	}
	
	protected static IStatus createStatus(Operation operation, TextGridProject project, Throwable cause, String message, Object... arguments) {
		StringBuilder statusMessage = new StringBuilder();
		
		if (message != null)
			statusMessage.append(NLS.bind(message, arguments));
		
		statusMessage.append(NLS.bind(Messages.ProjectFileException_3 + MESSAGES[operation.ordinal()], project));
		
		Status status;
		if (cause instanceof CoreException) {
			status = new MultiStatus(Activator.PLUGIN_ID, operation.ordinal(),
					new IStatus[] { ((CoreException) cause).getStatus() },
					statusMessage.toString(), cause);
		} else {
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, operation
					.ordinal(),
				statusMessage.toString(), cause);
		}
		return status;
	}

}
