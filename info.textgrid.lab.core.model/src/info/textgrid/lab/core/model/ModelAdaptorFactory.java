/**
 * 
 */
package info.textgrid.lab.core.model;

import static info.textgrid.lab.core.model.Activator.handleError;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.internal.resources.ResourceException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.osgi.util.NLS;

/**
 * AdaptorFactory for converting between the various model layers.
 * 
 * <p>
 * Clients do not use this file directly, but call methods like
 * {@link TextGridObject#getAdapter(Object, Class)} instead.
 * 
 * @see TextGridObject
 * @see IAdaptable#getAdapter(Class)
 * @author tv
 */
@SuppressWarnings("restriction")
public class ModelAdaptorFactory implements IAdapterFactory {
	
	@SuppressWarnings("unchecked")
	private Class[] adapterList = { IFile.class, TextGridObject.class,
			java.io.File.class };

	/**
	 * Not API, please use getAdapter().
	 * 
	 * This helper method tries to get the IFile object for a given
	 * TextGridObject. If there already is a link in the workspace, it is
	 * returned. Otherwise the method may return an IFile that does <em>not</em>
	 * exist yet, or null.
	 * 
	 * @param object
	 *            the TextGridObject to get the file for. May not be null.
	 * @param createProject
	 *            if <code>true</code> and the project does not exist yet, the project is
	 *            created. Beware that this may be prone to scheduling issues.
	 * @return an IFile object representing this object, possibly
	 *         <em>without</em> the link to the object.
	 * @throws SchedulingException
	 *             if we can't create the project due to scheduling issues. Will never be thrown when createProject is <code>false</code>
	 */
	protected static IFile getFileFor(TextGridObject object,
			boolean createProject) {
		try {
			IProject project = getOpenProject(object, createProject);
			if (project == null)
				return null;

			String nameCandidate = object.getNameCandidate();
			
			//windows specific: substitute invalid filename characters
			if (System.getProperties().getProperty("os.name").toUpperCase().contains("WINDOWS")) { //$NON-NLS-1$ //$NON-NLS-2$
				nameCandidate = nameCandidate.replaceAll("[:/\\\\*?\"<>|]", "_"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			
			String fileExtension = computeFileExtension(object);
			long nameSuffix = 0l;
			Pattern namePattern = Pattern.compile(Pattern.quote(nameCandidate)
					+ "(?: \\([1-9][0-9]*\\))?" + Pattern.quote(fileExtension), Pattern.CASE_INSENSITIVE); //$NON-NLS-1$

			for (IResource member : project.members()) {
				if (member instanceof IFile) {
					if (object.getEFSURI().equals(member.getLocationURI()))
						return (IFile) member;
					Matcher matcher = namePattern.matcher(member.getName());
					if (matcher.matches()) {
						if (matcher.groupCount() >= 1
								&& matcher.group(1) != null) {
							int matchedSuffix = Integer.parseInt(matcher
									.group(1));
							nameSuffix = Math
									.max(matchedSuffix + 1, nameSuffix);
						} else {
							nameSuffix++;
						}
					}
				}
			}

			// we have not found an existing link to the object (in this
			// project) → create name

			String name;
			if (nameSuffix == 0l)
				name = nameCandidate + fileExtension;
			else
				name = nameCandidate + " (" + nameSuffix + ")" + fileExtension; //$NON-NLS-1$ //$NON-NLS-2$

			IFile file = project.getFile(name);
			
			return file;
			
		} catch (CoreException e) {
			handleError(
					e,
					"Internal Error: Cannot extract {0}'s project. Please file a bug report.", //$NON-NLS-1$
					object);
		}
		return null;
	}

	/**
	 * <p>
	 * Returns an IFile for the given TextGridObject. If the IFile does not
	 * exist in the workspace yet, a link is created, use
	 * {@link #getFileFor(TextGridObject, boolean)} if you don't want this.
	 * </p>
	 * 
	 * <p>
	 * This method is not intended for end-users: Use
	 * {@linkplain TextGridObject#getAdapter(Class)
	 * TextGridObject.getAdapter(IFile.<b>class</b>)} instead.
	 * </p>
	 * 
	 */
	protected static IFile createFileFor(final TextGridObject object) {
		final IFile file = getFileFor(object, true);
		if ((file != null) && !file.exists())
			try {
				SchedulingException
						.check(
								file.getWorkspace().getRuleFactory()
										.createRule(file),
								object,
								"Could not create the link {0} for {1} due to scheduling issues.", //$NON-NLS-1$
								file, object);
				if (Activator.getDefault().isDebugging())
					System.out
					.println(NLS
							.bind(
									"ModelAdaptorFactory: IFile({0}).createLink({1})", //$NON-NLS-1$
									file, object));
				if (!file.exists())
					file.createLink(object.getEFSURI(), IFile.ALLOW_MISSING_LOCAL | IFile.REPLACE /* TG-573 */
					, null);
				
	
	
			} catch (ResourceException e) {
				// Catch arising ResourceException after creating link and treat it like a problem. 
				// There exists a race condition, which sometimes occur. 
				Activator.handleProblem(IStatus.WARNING, e, "Problem creating link to {0} as {1}",  //$NON-NLS-1$
						object, file);
			} catch (Exception e) {
				handleError(e, "Error creating link to {0} as {1}", //$NON-NLS-1$
						object, file);
				if (!(e instanceof CoreException))
					throw (RuntimeException) e;
			}
	
		return file;
	}

	/**
	 * Determines and opens the {@link IProject} in the current workspace where
	 * the resource for <var>object</var> will be placed in.
	 * 
	 * @param object
	 *            The {@link TextGridObject} for which a {@link IProject} should
	 *            be returned
	 * @param createProject
	 *            if the project does not exist yet, <var>createProject</var>
	 *            determines what to do: If <code>true</code>, the project will
	 *            be created and returned, if <code>false</code>,
	 *            <code>null</code> will be returned.
	 * @return the {@link IProject} where the link for {@link TextGridObject}'s
	 *         {@link IFile} can be placed into, or <code>null</code> if the
	 *         project doesn't exist and isn't created.
	 * @throws CoreException
	 *             if something goes wrong, especially with accessing metadata.
	 * @throws SchedulingException
	 *             if we aren't allowed to perform neccessary workspace
	 *             operations due to scheduling conflicts.
	 */
	protected static IProject getOpenProject(final TextGridObject object, final boolean createProject) throws CoreException {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();

		String projectName = object.getProject();
		IProject project = root.getProject(projectName);
		if (!project.exists()) {
			if (createProject) {
				ISchedulingRule createRule = project.getWorkspace()
						.getRuleFactory().createRule(project);
				SchedulingException
						.check(
								createRule,
								object,
								"Failed to get the file for {0}: We need to materialize the project, {1}, but we aren't allowed to due to the current scheduling rules.\n" //$NON-NLS-1$
										+ "This is probably a programming error within TextGridLab, please file a detailed bug report."); //$NON-NLS-1$
				try {
					project.create(null);
				} catch(ResourceException e) {
					if (!project.exists())
						throw e;
					// else silently drop exception (-> race condition), cf #12778
				}
			} else {
				return null;
			}
		}
		if (!project.isOpen())
			project.open(null);
		return project;
	}

	private TextGridObject getTextGridObjectFor(IFile file)
			throws CrudServiceException {
		URI uri = TextGridObject.computeTextGridURI((file).getLocationURI());
		if (uri == null) // no TextGrid URI
			return null;
		if (ITextGridModelConstants.SCHEMA.equalsIgnoreCase(uri.getScheme())
				|| ITextGridModelConstants.SCHEMA_NEWFILE.equalsIgnoreCase(uri
						.getScheme())) {
			return TextGridObject.getInstance(uri, false);
		}
	
		if (!file.exists()) {
			try {
				uri = new URI(ITextGridModelConstants.SCHEMA, file
						.getFullPath().toPortableString(), null);
			} catch (URISyntaxException e) {
				handleError(
						e,
						"Subtle temporary URI creation problem for {0}. This should not happen, please file a bug report.", //$NON-NLS-1$
						file);
			}
		}
		return null;
	}

	private static String computeFileExtension(TextGridObject object) {

		String format = ""; //$NON-NLS-1$
		try {
			format = object.getContentTypeID();
		} catch (CoreException e) {
			handleError(e, Messages.ModelAdaptorFactory_CouldNotDetectFormat);
		}

		String result = ".tgo"; //$NON-NLS-1$
		TGContentType type = TGContentType.findMatchingContentType(format);
		if (type != null && null != type.getExtension() && !"".equals(type.getExtension())) //$NON-NLS-1$
			result = "." + type.getExtension(); //$NON-NLS-1$

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object,
	 * java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (!(adaptableObject instanceof TextGridObject)
				&& !adapterType.isAssignableFrom(TextGridObject.class)) {
			Object tgo = Platform.getAdapterManager().getAdapter(
					adaptableObject, TextGridObject.class);
			if (tgo != null)
				adaptableObject = tgo;
		}

		if (adaptableObject instanceof TextGridObject) {
			final TextGridObject object = (TextGridObject) adaptableObject;
			if (adapterType.equals(IFile.class)) {
				return createFileFor(object);

			} else if (adapterType.equals(File.class)) {
				try {
					IFileStore store = EFS.getStore(object.getEFSURI());
					return store.toLocalFile(EFS.CACHE, null);
				} catch (CoreException e) {
					handleError(e, Messages.ModelAdaptorFactory_CachingError,
							object);
				}
			}
		} else if (adaptableObject instanceof IFile) {
			if (adapterType.isAssignableFrom(TextGridObject.class)) {
				try {
					return getTextGridObjectFor((IFile) adaptableObject);
				} catch (CrudServiceException e) {
					handleError(e, "Could not convert {0} to {1}: {2}", //$NON-NLS-1$
							adaptableObject, adapterType, e
									.getLocalizedMessage());
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@SuppressWarnings("unchecked")
	public Class[] getAdapterList() {
		// TODO Auto-generated method stub
		return adapterList;
	}

}
