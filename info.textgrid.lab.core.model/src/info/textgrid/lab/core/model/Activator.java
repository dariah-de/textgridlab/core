package info.textgrid.lab.core.model;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle and contains some helper
 * methods (e.g. for error logging).
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.core.model"; //$NON-NLS-1$
	
	// Debug option name for service calls, see .options file
	public static final String DEBUG_SERVICE = PLUGIN_ID
			+ "/debug/servicecalls"; //$NON-NLS-1$
	
	public static final String DEBUG_INCOMPLETE = PLUGIN_ID + "/debug/incomplete"; //$NON-NLS-1$
	public static final String DEBUG_INCOMPLETE_VERBOSE = PLUGIN_ID + "/debug/incomplete/verbose"; //$NON-NLS-1$
	public static final String DEBUG_READS = "info.textgrid.lab.core.efs.tgcrud/debug/reads"; //$NON-NLS-1$
	public static final String DEBUG_UITHREAD = PLUGIN_ID + "/debug/uithread"; 
	public static final String DEBUG_METADATA = PLUGIN_ID + "/debug/metadata"; 
	public static final String DEBUG_METADATA_URI = PLUGIN_ID + "/debug/metadata_uri"; 
	


	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/**
	 * If the debugging option for services is set, write arguments to standard
	 * output.
	 * 
	 * @param msg
	 * @param args
	 */
	public static void logServiceCall(String msg, Object... args) {
		if (getDefault().isDebugging()
				&& "true".equalsIgnoreCase(Platform //$NON-NLS-1$
						.getDebugOption(DEBUG_SERVICE))) {
			System.out.println("TGcrud/EFS: " + NLS.bind(msg, args)); //$NON-NLS-1$
		}

	}
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}
	
	public static boolean isDebugging(final String option) {
		return getDefault().isDebugging() &&
			"true".equalsIgnoreCase(Platform.getDebugOption(option)); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public IStatus handleProblem(Throwable e) {
		return handleProblem(IStatus.ERROR, null, e);
	}

	public IStatus handleProblem(int severity, String message, Throwable cause) {
		if (message == null || message.equals("")) { //$NON-NLS-1$
			if (cause != null)
				message = cause.getLocalizedMessage();
		}
		IStatus status = new Status(severity, PLUGIN_ID, message, cause);
		StatusManager.getManager().handle(
				status,
				status.getSeverity() == IStatus.ERROR ? StatusManager.SHOW
						| StatusManager.LOG : StatusManager.LOG);
		return status;
	}

	/**
	 * Cares for the given error. The <var>message</var> and the <var>args</var>
	 * are passed to {@link NLS#bind(String, Object[])} and the complete
	 * argument list to {@link #handleProblem(int, String, Throwable)}.
	 * 
	 * @param cause
	 *            The exception that caused the problem (or null)
	 * @param message
	 *            The message, use {0}, {1} ... for arguments
	 * @param args
	 *            Exactly as many arguments as referenced in the message
	 * @return the {@link IStatus} object created while handling the error
	 */
	public static IStatus handleError(Throwable cause, String message,
			Object... args) {
		return handleProblem(IStatus.ERROR, cause, message, args); 
	}

	
	/**
	 * Cares for the given problem. The <var>message</var> and the
	 * <var>args</var> are passed to {@link NLS#bind(String, Object[])} and the
	 * complete argument list to {@link #handleProblem(int, String, Throwable)}.
	 * 
	 * @param severity
	 *            One of the severity constants defined in {@link IStatus}.
	 *            Currently, IStatus.ERROR will pop up an error dialogue.
	 * @param cause
	 *            The exception that caused the problem (or null)
	 * @param message
	 *            The message, use {0}, {1} ... for arguments
	 * @param args
	 *            Exactly as many arguments as referenced in the message
	 * @return the {@link IStatus} object created while handling the error
	 */	
	public static IStatus handleProblem(int severity, Throwable cause,
			String message, Object... args) {
		return Activator.getDefault().handleProblem(severity,
				NLS.bind(message, args), cause);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

}
