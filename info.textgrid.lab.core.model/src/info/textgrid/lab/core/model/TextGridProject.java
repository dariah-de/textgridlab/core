package info.textgrid.lab.core.model;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.core.model.ITextGridProjectListener.Event;
import info.textgrid.lab.core.model.util.ModelUtil;
import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.lab.log.logsession;
import info.textgrid.namespaces.middleware.tgauth.AddMemberRequest;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectResponse;
import info.textgrid.namespaces.middleware.tgauth.DeleteMemberRequest;
import info.textgrid.namespaces.middleware.tgauth.GetLeaderRequest;
import info.textgrid.namespaces.middleware.tgauth.GetProjectDescriptionRequest;
import info.textgrid.namespaces.middleware.tgauth.GetProjectDescriptionResponse;
import info.textgrid.namespaces.middleware.tgauth.GetUserRoleRequest;
import info.textgrid.namespaces.middleware.tgauth.GetUserRoleResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;
import info.textgrid.namespaces.middleware.tgauth.RbacFault;
import info.textgrid.namespaces.middleware.tgauth.RolesetResponse;
import info.textgrid.namespaces.middleware.tgauth.SetProjectFileRequest;
import info.textgrid.namespaces.middleware.tgauth.TgAddActiveRoleRequest;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedRolesRequest;
import info.textgrid.namespaces.middleware.tgauth.TgDropActiveRoleRequest;
import info.textgrid.namespaces.middleware.tgauth.UserRole;
import info.textgrid.namespaces.middleware.tgauth.UsersetResponse;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.ui.progress.UIJob;

import com.google.common.collect.Maps;

// FIXME this class needs major cleanup & documentation

/**
 * @author martin
 * 
 */
public class TextGridProject extends AbstractResource implements IAdaptable,
		ITextGridPermission {
	public static final String TG_STANDARD_ROLE_PROJECTLEADER = "Projektleiter";
	public static final String TG_STANDARD_ROLE_ADMINISTRATOR = "Administrator";
	public static final String TG_STANDARD_ROLE_EDITOR = "Bearbeiter";
	public static final String TG_STANDARD_ROLE_WATCHER = "Beobachter";

	private int permissions = -1; // TODO implement cache invalidation
	private static PortTgextra stub = null; // we need the stub just ONCE
	private ProjectInfo projectInfo;
	private ArrayList<UserRole2> userRole2Root = null;
	private Boolean iAmLeader = null;
	private final ArrayList<String> leaders = new ArrayList<String>();
	private static ListenerList listeners = new ListenerList();

	private static Map<String, TextGridProject> registry = Maps.newHashMap();

	public void reset() {
		permissions = -1;
		userRole2Root = null;
		iAmLeader = null;
		leaders.clear();
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TextGridProject other = (TextGridProject) obj;

		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;

	}

	/**
	 * 
	 */
	@Deprecated
	public static PortTgextra ensureStubIsLoaded() {
		return getStub();
	}

	public static PortTgextra getStub() {
        ModelUtil.checkNonUIThread("preparing TG-extra call");
		if (stub == null)
			stub = TgAuthClientUtilities.getTgAuthServiceStub();
		return stub;
	}

	public static TextGridProject createNewProject(final String name,
			final String description) throws RemoteException {

		// call RBAC → ProjectInfo

		final String sid = RBACSession.getInstance().getSID(true);
		final String loggingParam = logsession.getInstance().getloginfo();

		final CreateProjectRequest cprequest = new CreateProjectRequest();

		cprequest.setAuth(sid);
		cprequest.setLog(loggingParam);
		cprequest.setName(name);
		cprequest.setDescription(description);

		CreateProjectResponse cpresponse = null;

		cpresponse = getStub().createProject(cprequest);

		final ProjectInfo resultFromRbac = new ProjectInfo();
		resultFromRbac.setName(name);
		resultFromRbac.setDescription(description);
		resultFromRbac.setId(cpresponse.getProjectId());

		final TextGridProject project = new TextGridProject(resultFromRbac);

		try {
			project.addMultiRolesToMember(RBACSession.getInstance().getEPPN(),
					new String[] { TG_STANDARD_ROLE_EDITOR,
							TG_STANDARD_ROLE_ADMINISTRATOR });
			project.activateRoleInSession(RBACSession.getInstance().getEPPN(),
					TG_STANDARD_ROLE_PROJECTLEADER);
		} catch (RbacFault e) {
			final IStatus status = new Status(IStatus.INFO,
					Activator.PLUGIN_ID, "Did not fully add roles.");
			Activator.getDefault().getLog().log(status);
		}
		new UIJob("Resetting project list") {

			public IStatus runInUIThread(IProgressMonitor monitor) {
				TextGridProjectRoot.getInstance().resetProjects();
				return Status.OK_STATUS;
			}
		}.schedule();
		TextGridProject.notifyListeners(Event.CREATED, project);
		return project;
	}

	public static TextGridProject getProjectInstance(final String id)
			throws RemoteException, ProjectDoesNotExistException,
			CrudServiceException {

		TextGridProject project = registry.get(id);
		if (project != null)
			return project;
		else {
			project = new TextGridProject(id); // reads from RBAC
			registry.put(id, project);
			return project;
		}
	}

	/**
	 * Constructor for TextGridProjcts that know their ProjectInfo. Can be used
	 * only if it is made sure that these ProjectInfo is valid (e.g. just
	 * returned by getAllProjects).
	 * 
	 * @param pi
	 *            a valid project info record. Must not be <code>null</code>.
	 * @return A valid {@link TextGridProject}, either an existing one from the
	 *         registry or a new one retrieved from the repo.
	 * @throws RemoteException
	 */
	public static TextGridProject getProjectInstance(final ProjectInfo pi)
			throws RemoteException, NullPointerException {
		if (pi == null)
			throw new NullPointerException(
					"Cannot create a project from a null project info record.");
		TextGridProject project = registry.get(pi.getId());
		if (project == null) {
			project = new TextGridProject(pi);
			registry.put(project.getId(), project);
		}
		// TODO do we need to update the project's info here?
		return project;
	}

	/**
	 * Protected constructor that reads project with the given ID from RBAC.
	 * Clients may not call this constructor, but must use
	 * {@link #getProjectInstance(String)}.
	 * 
	 * @param id
	 *            the ID of an (hopefully) existing project.
	 * @throws ProjectDoesNotExistException
	 *             if the project does not exist.
	 * @throws RBACServiceException
	 *             if some error occured while communicating with the backend
	 * 
	 */
	protected TextGridProject(final String id)
			throws ProjectDoesNotExistException, RBACServiceException {
		super();

		final String sid = RBACSession.getInstance().getSID(false);
		final String loggingParam = logsession.getInstance().getloginfo();

		ensureStubIsLoaded();
		// ProjectInfo abfragen
		final GetProjectDescriptionRequest prequest = new GetProjectDescriptionRequest();
		prequest.setAuth(sid);
		prequest.setLog(loggingParam);
		prequest.setProject(id);

		GetProjectDescriptionResponse presponse = getStub()
				.getProjectDescription(prequest);

		projectInfo = presponse.getProject();

		if (projectInfo == null) {
			throw new ProjectDoesNotExistException();
		}

	}

	/**
	 * called by {@link #createNewProject(String, String)} after asking RBAC to
	 * create the project
	 * <p>
	 * clients MUST NOT call this constructor, but use
	 * {@link #createNewProject(String, String)} instead.
	 * 
	 * @param pi
	 */
	protected TextGridProject(final ProjectInfo pi) throws NullPointerException {
		if (pi == null)
			throw new NullPointerException(
					"Cannot create a project from an empty project info.");
		projectInfo = pi;
	}

	public ProjectInfo getProjectInfo() {
		return projectInfo;
	}

	public String getId() {
		return projectInfo.getId();
	}

	public String getName() {
		return projectInfo.getName();
	}

	public String getDescription() {
		return projectInfo.getDescription();
	}

	/**
	 * Returns the ProjectFile URI (as String) for this Project
	 * 
	 * @return
	 */
	public String getProjectfile() {
		// XXX if you need this debugging output please guard with Eclipse's
		// tracing options
		// System.out.println("Returning PF of project " + info.getId());
		// System.out.println(info.getFile());
		// if (info.getFile() == null) {
		// Activator.handleProblem(IStatus.INFO, null, "Project "
		// + info.getName() + " has no ProjectFile set.");
		// }
		return projectInfo.getFile();
	}

	/**
	 * Accesses the RBAC to register a Projectfile URI as string with this
	 * project. Send an empty String to unregister. If not-empty, RBAC will
	 * register the URI at the project and modify access rights for the URI.
	 * This means the URI must be an URI of a TextGridObject saved to the Grid
	 * already.
	 * 
	 * @param PfURI
	 * @return
	 * @throws OfflineException
	 * @throws RemoteException
	 * @throws AuthenticationFault
	 */
	public boolean setProjectfile(final String PfURI) throws OfflineException,
			RemoteException, AuthenticationFault {
		if (PfURI.matches("^" + ITextGridModelConstants.SCHEMA_NEWFILE)) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							null,
							PfURI
									+ " is not saved to the Grid yet, cannot register it as Projectfile.");
			return false;
		}
		final String sid = RBACSession.getInstance().getSID(true);
		final String loggingParam = logsession.getInstance().getloginfo();
		final SetProjectFileRequest spfr = new SetProjectFileRequest();
		spfr.setAuth(sid);
		spfr.setLog(loggingParam);
		spfr.setProject(projectInfo.getId());
		spfr.setFile(PfURI);
		projectInfo.setFile(PfURI);
		final BooleanResponse bo = getStub().setProjectFile(spfr);
		if (bo.isResult()) {
			projectInfo.setFile(PfURI);
		}
		return bo.isResult();
	}

	/**
	 * Unregister the projectFile.
	 * 
	 * @throws OfflineException
	 * @throws RemoteException
	 * @throws AuthenticationFault
	 */
	public void unsetProjectFile() throws OfflineException, RemoteException,
			AuthenticationFault {

		setProjectfile("");
	}

	public synchronized int getPermissions() throws CoreException {

		if (permissions >= 0)
			return permissions; // cache.

		permissions = retrievePermissions(getId());

		return permissions;
	}

	@Override
	public Object getAdapter(final Class adapter) {
		return Platform.getAdapterManager().getAdapter(this, adapter);
	}

	public Boolean addMultiRolesToMember(final String ePPN, final String[] Roles)
			throws RemoteException, RbacFault {
		Boolean success = true;
		for (final String Role : Roles) {
			final boolean singleSuccess = addRoleMember(ePPN, Role);
			if (!singleSuccess) {
				success = false;
			}
		}
		return success;
	}

	/**
	 * Enters a Role for the User identified by ePPN into RBAC. Function does
	 * not check for plausibility, i.e. whether these roles exist in the project
	 * or whether that user ever logged in. If the calling user does not have
	 * the Projektleiter role, RBAC will complain and no user will be entered.
	 * 
	 * The role will be active in the user's session whenever the user logs in
	 * again, with one exception: if the caller adds a role for herself, the
	 * session gets updated immediately.
	 * 
	 * @param ePPN
	 * @param Role
	 * @param projectID
	 * @return
	 * @throws RemoteException
	 * @throws RbacFault
	 */
	public Boolean addRoleMember(final String ePPN, final String Role)
			throws RemoteException, RbacFault {
		final String rbacsid = RBACSession.getInstance().getSID(false);
		final AddMemberRequest addmemreq = new AddMemberRequest();
		addmemreq.setAuth(rbacsid);
		addmemreq.setLog("");
		addmemreq.setUsername(ePPN);
		final String FQRoleName = Role + "," + projectInfo.getId() + ","
				+ "Projekt-Teilnehmer";
		addmemreq.setRole(FQRoleName);
		final BooleanResponse added = getStub().addMember(addmemreq);

		if (!added.isResult()) {
			final IStatus status = new Status(IStatus.ERROR,
					Activator.PLUGIN_ID,
					"There was no exception, but I could not add member "
							+ ePPN + " in project " + projectInfo.getId());
			Activator.getDefault().getLog().log(status);
			return false;
		}

		return true;
	}

	public boolean activateRoleInSession(final String ePPN, final String Role)
			throws RemoteException {
		// dynamically activate the new role in the session if user added it for
		// herself
		final String rbacsid = RBACSession.getInstance().getSID(false);
		final String FQRoleName = Role + "," + projectInfo.getId() + ","
				+ "Projekt-Teilnehmer";
		if (ePPN.equalsIgnoreCase(RBACSession.getInstance().getEPPN())) {
			final TgAddActiveRoleRequest addSessionreq = new TgAddActiveRoleRequest();
			addSessionreq.setAuth(rbacsid);
			addSessionreq.setLog("");
			addSessionreq.setRole(FQRoleName);
			BooleanResponse addedToSession = getStub().tgAddActiveRole(
					addSessionreq);
			if (!addedToSession.isResult()) {
				final IStatus status = new Status(IStatus.INFO,
						Activator.PLUGIN_ID, "did not add role " + FQRoleName
								+ " to session for user " + ePPN);
				Activator.getDefault().getLog().log(status);
			}
		}
		return true;
	}

	/**
	 * Deletes a Role from RBAC. If the calling user does not have the
	 * Projektleiter role, RBAC will complain and no user will be deleted.
	 * 
	 * The role will invalidated in the user's session whenever the user logs in
	 * again, with one exception: if the caller deletes a role for herself, the
	 * session gets updated immediately.
	 * 
	 * @param user
	 * @param role
	 * @return
	 */
	public Boolean deleteRole(final String eppn, final String role) {
		final String rbacsid = RBACSession.getInstance().getSID(false);
		final DeleteMemberRequest delmemreq = new DeleteMemberRequest();
		delmemreq.setAuth(rbacsid);
		delmemreq.setLog("");
		final String FQRoleName = role + "," + projectInfo.getId() + ","
				+ "Projekt-Teilnehmer";
		delmemreq.setRole(FQRoleName);
		delmemreq.setUsername(eppn);
		try {
			final BooleanResponse deleted = getStub().deleteMember(delmemreq);

			// dynamically delete role if authenticated user deletes for herself
			if (delmemreq.getUsername().equalsIgnoreCase(
					RBACSession.getInstance().getEPPN())) {
				final TgDropActiveRoleRequest dropreq = new TgDropActiveRoleRequest();
				dropreq.setAuth(rbacsid);
				dropreq.setLog("");
				dropreq.setRole(FQRoleName);
				final BooleanResponse dropped = getStub().tgDropActiveRole(
						dropreq);
				if (!dropped.isResult()) {
					final IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID, "could not drop role "
									+ FQRoleName + " from session for user "
									+ eppn);
					Activator.getDefault().getLog().log(status);
				}
			}

			if (!deleted.isResult()) {
				final IStatus status = new Status(IStatus.ERROR,
						Activator.PLUGIN_ID,
						"There was no exception, but I could not delete role "
								+ FQRoleName + " from user " + eppn);
				Activator.getDefault().getLog().log(status);
				return false;
			}
		} catch (final Exception e2) {
			final IStatus status = new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, "Could not delete role " + FQRoleName
							+ " from user " + eppn, e2);
			Activator.getDefault().getLog().log(status);
			return false;
		}
		return true;

	}

	/**
	 * returns available Roles for this project, currently a fixed hard-wired
	 * set for any project.
	 * 
	 * @param projectId
	 * @return String[]
	 */
	public String[] getAvailableRoles() {
		return new String[] { TG_STANDARD_ROLE_PROJECTLEADER,
				TG_STANDARD_ROLE_ADMINISTRATOR, TG_STANDARD_ROLE_EDITOR,
				TG_STANDARD_ROLE_WATCHER, };
	}

	/**
	 * Returns cached User-Role Assigment without net access.
	 * 
	 * @see getUserRolesFromRBAC
	 * 
	 * @return
	 */
	public ArrayList<UserRole2> getUserRoles2Offline() {
		return userRole2Root;
	}

	/**
	 * returns a UserRole list. Used in the content provider of the
	 * UserManagement. Accesses RBAC server.
	 * 
	 * @return
	 */
	public ArrayList<UserRole2> getUserRoles2FromRBAC(
			final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor,
				"Fetching roles", 3);

		userRole2Root = new ArrayList<UserRole2>();

		final GetUserRoleRequest gur = new GetUserRoleRequest();
		gur.setAuth(RBACSession.getInstance().getSID(false));
		gur.setLog("");
		gur.setProject(projectInfo.getId());
		GetUserRoleResponse uRresponse;
		try {
			if (!progress.isCanceled()) {
				uRresponse = getStub().getUserRole(gur);
				if (uRresponse.getUserRole() == null) {
					return userRole2Root;
				}
				progress.worked(1);
				if (!progress.isCanceled()) {
					final List<UserRole> uRoles = uRresponse.getUserRole();
					final String me = RBACSession.getInstance().getEPPN();
					boolean foundIAmLeader = false;
					for (final UserRole ur : uRoles) {
						final UserRole2 userNode = new UserRole2(
								ur.getUsername());
						for (final String r : ur.getRoles()) {
							userNode.addRole(r);
							// update iAmLeader Cache in case another user added
							// me
							if (ur.getUsername().equalsIgnoreCase(me)
									&& r.equalsIgnoreCase(TG_STANDARD_ROLE_PROJECTLEADER)) {
								foundIAmLeader = true;
							}
						}
						userRole2Root.add(userNode);
					}
					if (foundIAmLeader) {
						iAmLeader = Boolean.TRUE;
					} else {
						iAmLeader = Boolean.FALSE;
					}
					progress.worked(2);
					if (!progress.isCanceled()) {
						// complete the Names Cache
						final ArrayList<String> users = new ArrayList<String>();
						for (final UserRole uR : uRresponse
								.getUserRole()) {
							users.add(uR.getUsername());
						}
						RBACSession.getInstance()
								.retrieveDetailsFromRBAC(users);
						progress.worked(3);
					}
				}
			}
		} catch (final Exception e3) {
			// TODO should separate different AxisFaults here...
			final IStatus status = new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					"Could not retrieve users for project "
							+ projectInfo.getName()
							+ ", perhaps authenticated user is not Projektleiter?",
					e3);
			Activator.getDefault().getLog().log(status);
			return userRole2Root;
		}

		return userRole2Root;
	}

	/**
	 * Takes a rolesetResponse (from tgAssignedRoles, which returns ALL Roles in
	 * any project the authenticated user is Projektleiter in) and splits the
	 * rolesetResponse (Array of Strings like
	 * "Administrator,TGPR3,Projekt-Teilnehmer") and returns only the roles a
	 * user has in the specified project. This function can be reused also
	 * outside of this class.
	 * 
	 * @param roleset
	 * @param projectID
	 * @return Array of filtered Roles, e.g. [Projektleiter,Bearbeiter]
	 * @author martin
	 */
	public static String[] filterRolesByProject(final RolesetResponse roleset,
			final String projectID) {
		final ArrayList<String> rolesInProject = new ArrayList<String>();
		if (roleset != null && roleset.getRole() != null) {
			for (final String fqRole : roleset.getRole()) {
				// System.out.println("checking..."+fqRole);
				final String[] parts = fqRole.split(",");
				if (projectID.equals(parts[1])) {
					rolesInProject.add(parts[0]);
					// System.out.println("Found role "+parts[0]);
				}
			}
		}
		return rolesInProject.toArray(new String[0]);
	}

	/**
	 * Returns the ePPNs of all users who are Projektleiter in this project.
	 * 
	 * @return
	 */
	private List<String> getLeadersFromRBAC() {
		final GetLeaderRequest gl = new GetLeaderRequest();
		gl.setProject(projectInfo.getId());
		gl.setAuth(RBACSession.getInstance().getSID(false));
		gl.setLog("");
		UsersetResponse pls = new UsersetResponse();
		try {
			pls = getStub().getLeader(gl);
		} catch (final Exception e4) {
			final IStatus status = new Status(IStatus.ERROR,
					Activator.PLUGIN_ID,
					"Could not retrieve Project Leaders for project "
							+ projectInfo.getId() + projectInfo.getName()
							+ " for some reason...", e4);
			Activator.getDefault().getLog().log(status);
			return new ArrayList<String>();
		}
		return pls.getUsername();
	}

	/**
	 * Checks whether the logged-in user has the Projektleiter role.
	 * 
	 * @return
	 */
	public boolean iAmLeader() {
		if ((iAmLeader != null) && (!leaders.isEmpty())) {
			return iAmLeader; // use from cache instead of next
			// service call
		}
		boolean iAml = false;
		final String me = RBACSession.getInstance().getEPPN();
		for (final String l : getLeadersFromRBAC()) {
			leaders.add(l);
			if (me.equalsIgnoreCase(l)) {
				iAml = true;
			}
		}
		iAmLeader = iAml; // cache this field for later
		return iAml;
	}

	public ArrayList<String> getLeaders() {
		iAmLeader();
		return leaders;
	}

	/**
	 * Checks whether the logged-in user has this role in this project, e.g.
	 * call checkForRole("TGPR123","Bearbeiter"), which returns true if the user
	 * has this role in her session activated.
	 * 
	 * @param projectID
	 * @param roleToCheck
	 * @return
	 */
	public static boolean checkForRole(final String projectID,
			final String roleToCheck) {
		try {
			ensureStubIsLoaded();
		} catch (final Exception e4) {
			final IStatus status = new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, "Could not contact tgAuth", e4);
			Activator.getDefault().getLog().log(status);
		}
		final TgAssignedRolesRequest tgar = new TgAssignedRolesRequest();
		tgar.setAuth(RBACSession.getInstance().getSID(false));
		tgar.setLog("");
		tgar.setUsername("");
		RolesetResponse allPossibleRolesOfUser = new RolesetResponse();
		try {
			allPossibleRolesOfUser = getStub().tgAssignedRoles(tgar);
		} catch (final Exception e4) {
			final IStatus status = new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					"Could not retrieve roles for this user for some reason...",
					e4);
			Activator.getDefault().getLog().log(status);
		}
		boolean result = false;
		for (final String role : filterRolesByProject(allPossibleRolesOfUser,
				projectID)) {
			if (roleToCheck.equalsIgnoreCase(role)) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * Returns a Language-specific Label of the Role String as used by RBAC.
	 * Knows the Role Constants from TextGridProject.TG_STANDARD_ROLE_*, for all
	 * other (unknown) roles will the returned String will be the input String.
	 * 
	 * @param role
	 * @return
	 */
	public static String findLabelForRBACRole(final String role) {
		String label;
		if (role.equalsIgnoreCase(TG_STANDARD_ROLE_PROJECTLEADER))
			label = "Project Manager";
		else if (role.equalsIgnoreCase(TG_STANDARD_ROLE_ADMINISTRATOR))
			label = "Authority to delete";
		else if (role.equalsIgnoreCase(TG_STANDARD_ROLE_EDITOR))
			label = "Editor";
		else if (role.equalsIgnoreCase(TG_STANDARD_ROLE_WATCHER))
			label = "Observer";
		else
			label = role;
		return label;
	}

	/**
	 * Adds a listener for project change events.
	 * 
	 * @param listener
	 */
	public static void addListener(final ITextGridProjectListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes a listener for project change events.
	 * 
	 * @param listener
	 */
	public static void removeListener(final ITextGridProjectListener listener) {
		listeners.remove(listener);
	}

	/**
	 * <strong>THIS IS NOT API</strong>, it may only be called from the model or
	 * EFS implementation.
	 * 
	 * @param event
	 * @param textGridProject
	 */
	public static void notifyListeners(
			final ITextGridProjectListener.Event event,
			final TextGridProject textGridProject) {

		final UIJob job = new UIJob("Notifying TextGridProject listeners") {

			@Override
			public IStatus runInUIThread(final IProgressMonitor monitor) {
				for (final Object listener : listeners.getListeners())
					((ITextGridProjectListener) listener)
							.textGridProjectChanged(event, textGridProject);
				return Status.OK_STATUS;
			}
		};
		job.setSystem(true);
		job.schedule();
	}

}
