package info.textgrid.lab.core.model;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;

/**
 * An interface for all resources that support permissions.
 * 
 * @author Thorsten Vitt (tv) and Christoph Ludwig (cl) for <a
 *         href="http://www.textgrid.de/">TextGrid</a>
 */
/**
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 * 
 */
public interface ITextGridPermission {

	/**
	 * Indicates the current permissions are unknown and have to be retrieved
	 * yet.
	 */
	static final int UNKNOWN = -1;
	/**
	 * Indicates the current permissions are unknown and have been reset, e.g.,
	 * due to a SID change. This means that no cached value may be used.
	 */
	static final int RESET = -2;
	
	public static final int READ = 1;
	public static final int CREATE = 2;
	public static final int UPDATE = 4;
	public static final int DELETE = 8;
	public static final int PUBLISH = 16;
	public static final int DELEGATE = 32;

	/**
	 * Return the user's permissions on the current resource.
	 * 
	 * Implementors <em>should not</em> popup the login dialog for the current
	 * resource, but return appropriate permissions for anonymous users if
	 * nobody is logged in.
	 * 
	 * @return some of the constant values as defined in this interface bitwise
	 *         or'ed
	 * @throws CoreException
	 *             with further details when something goes wrong determining
	 *             the permissions. FIXME: This is subject to TG-23 refactoring.
	 */
	public int getPermissions() throws CoreException;

	// TODO public void setPermissions(int newRights);

	/**
	 * Convenience function that returns true when the user has at least the
	 * given rights on the given resource.
	 * 
	 * @param rights
	 *            the requested rights, bitwise or'ed
	 * @throws CoreException
	 *             with further details when something goes wrong determining
	 *             the permissions. FIXME: This is subject to TG-23 refactoring.
	 */
	public boolean hasPermissions(int rights) throws CoreException;

	/**
	 * Asserts that the given permissions are available.
	 * 
	 * @param permission
	 *            The permission requested
	 * @param errorMessage
	 *            Error message for the status object constructed when the
	 *            permission is not available
	 * @param arguments
	 *            Arguments for the <var>errorMessage</var>, see
	 *            {@link MessageFormat#format(String, Object...)}
	 * @throws CoreException
	 *             when the requested permissions are not available, or when
	 *             something goes wrong determining them.
	 * 
	 *             FIXME: This is subject to TG-23 refactoring.
	 */
	public void assertPermission(int permission, String errorMessage,
			Object... arguments)
			throws CoreException; // TODO refactor to ~AuthPermission

}
