package info.textgrid.lab.core.model;

import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;

/**
 * An {@link IDeferredWorkbenchAdapter} that can notify a
 * {@link IChildListChangedListener} when its content has changed.
 */
public interface IChildListParent extends IDeferredWorkbenchAdapter {
	public void addChildListChangedListener(IChildListChangedListener listener);
	public void removeChildListChangedListener(IChildListChangedListener listener);
	
}
