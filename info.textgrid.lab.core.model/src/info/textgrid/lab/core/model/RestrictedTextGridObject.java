package info.textgrid.lab.core.model;

/**
 * Represents TextGridObjects on which the actual user
 * doesn't have access on. This presents the user the
 * information that an object exists but says nothing
 * about the objects shape.
 * @author Frank Queens
 *
 */
public class RestrictedTextGridObject {
	
	public static final String TITLE = Messages.RestrictedTextGridObject_Title;
	
	private String uri = null; 

	
	public RestrictedTextGridObject(String uri) {
		this.uri = uri;
	}
	
	public String getURI() {
		return this.uri;
	}
}
