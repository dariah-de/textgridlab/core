package info.textgrid.lab.core.model;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.core.model.ProjectFileException.Operation;
import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.AppData;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.TgProjectFile;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.text.MessageFormat;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerFactory;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import com.google.common.collect.Iterators;

/**
 * This class is responsible to create and edit the ProjectFiles of the
 * TextGridProjects.QName
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class TextGridProjectFile {

	public static final String PROJECT_FILE_TYPE = "text/tg.projectfile+xml"; //$NON-NLS-1$

	private TextGridProject project;
	private TextGridObject projectFileObject;
	private TgProjectFile projectFileData;
	private boolean isNewObject = false;

	public static final String projectFileNS = "http://textgrid.info/namespaces/metadata/projectfile/2008-11-27"; //$NON-NLS-1$
	public static final javax.xml.namespace.QName TG_PROJECT_FILE_QNAME = new QName(
			projectFileNS, "tgProjectFile"); // TgProjectFile.MY_QNAME; //$NON-NLS-1$

	public static final QName appData = new javax.xml.namespace.QName(
			projectFileNS, "appData"); //$NON-NLS-1$

	// The element for the template editor in- and output.
	public static final javax.xml.namespace.QName metadataSectionQName = new javax.xml.namespace.QName(
			projectFileNS, "metadataSection", "ns1"); //$NON-NLS-1$ //$NON-NLS-2$

	public static String basicMetadataSectionNS = "http://textgrid.info/namespaces/metadata/basicMetadata/config/2011-08-10"; //$NON-NLS-1$

	// The element for the basic metadata wizard in- and output.
	public static final javax.xml.namespace.QName basicMetadataSectionQName = new javax.xml.namespace.QName(
			projectFileNS, "basicMetadataSection", "ns1"); //$NON-NLS-1$ //$NON-NLS-2$

	private static OMFactory omFactory = OMAbstractFactory.getOMFactory();

	public static final OMAttribute descriptionChanged = omFactory
			.createOMAttribute("changed", //$NON-NLS-1$
					omFactory.createOMNamespace(projectFileNS, "ns1"), "true"); //$NON-NLS-1$ //$NON-NLS-2$

	public static final javax.xml.namespace.QName descriptionChangedQName = new javax.xml.namespace.QName(
			projectFileNS, "changed", "ns1"); //$NON-NLS-1$ //$NON-NLS-2$

	public TextGridProjectFile(TextGridProject project) {

		this.project = project;
	}

	/**
	 * Returns the {@link TextGridObject} for a project file for this project.
	 * This may be a long-running operation.
	 * 
	 * @param create
	 *            if there does not yet exist a project file for this project
	 *            and <var>create</var> is true, a new one is created and saved.
	 * @param monitor
	 * @return the {@link TextGridObject} for the project file, or
	 *         <code>null</code> if none exists and none has been created.
	 * @throws ProjectFileException
	 *             when something went wrong while reading or creating the
	 *             project file.
	 */
	public TextGridObject getProjectFileObject(boolean create,
			IProgressMonitor monitor) throws ProjectFileException {

		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridProjectFile_FindingPF, this), 100);

		String pfUri = this.project.getProjectfile();
		if (pfUri == null || pfUri.equals("")) { //$NON-NLS-1$
			if (create) {
				pfUri = createNewProjectFile(progress.newChild(50)).getURI()
						.toString();
				isNewObject = true;
			} else {
				return null;
			}
		} else {
			isNewObject = false;
		}

		try {
			try {
				return TextGridObject.getInstance(new URI(pfUri), true);
			} catch (CrudServiceException e) {
				Activator.handleProblem(IStatus.WARNING, e,
						Messages.TextGridProjectFile_OfflinePF);
				return TextGridObject.getInstanceOffline(new URI(pfUri));
			}
		} catch (URISyntaxException e) {
			throw new ProjectFileException(Operation.READING, project, e);
		}
	}

	/**
	 * Creates a new TextGridObject pre-initialized with contents and metadata
	 * appropriate for (empty) project files. This does write the stuff to the
	 * grid, but it does not yet set it as the active project file.
	 * 
	 * This is a long running operation.
	 * 
	 * @param monitor
	 *            the progress monitor to use for reporting progress to the
	 *            user. It is the caller's responsibility to call done() on the
	 *            given monitor. Accepts null, indicating that no progress
	 *            should be reported and that the operation cannot be cancelled.
	 * @return the {@link TextGridObject} created for the given file. This is no
	 *         longer {@linkplain TextGridObject#isNew() new} and has been
	 *         written to the grid, but all fields are empty.
	 * @throws ProjectFileException
	 *             when something goes wrong.
	 */
	protected TextGridObject createNewProjectFile(IProgressMonitor monitor)
			throws ProjectFileException {
		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridProjectFile_Initializing, this), 100);

		projectFileObject = createNewProjectFileObject();

		progress.worked(10);

		TgProjectFile t = createProjectFileSkeleton();

		saveProjectFile(t, projectFileObject, progress);

		try {
			if (!this.project.setProjectfile(projectFileObject.getURI()
					.toString())) {
				throw new RuntimeException(Messages.TextGridProjectFile_CouldNotSet);
			}
			System.err.println("Set ProjectFile with PFUri"); //$NON-NLS-1$
		} catch (OfflineException e) {
			Activator.handleError(e,
					"Error TextGridProjectFile.createNewProjectFile()"); //$NON-NLS-1$
		} catch (RemoteException e) {
			Activator.handleError(e,
					"Error TextGridProjectFile.createNewProjectFile()"); //$NON-NLS-1$
		} catch (AuthenticationFault e) {
			Activator.handleError(e,
					"Error TextGridProjectFile.createNewProjectFile()"); //$NON-NLS-1$
		} catch (RuntimeException e) {
			Activator.handleError(e,
					"Error TextGridProjectFile.createNewProjectFile()"); //$NON-NLS-1$
		}

		return projectFileObject;
	}

	/**
	 * Creates and returns a {@linkplain TextGridObject#isNew() new}
	 * TextGridObject suitable for a project file.
	 */
	protected TextGridObject createNewProjectFileObject() {
		TextGridObject object = TextGridObject.getNewObjectInstance(project,
				TGContentType.getContentType(PROJECT_FILE_TYPE), project
						.getProjectInfo().getName());

		final PersonType rightsHolder = RBACSession.getInstance().getPerson();
		object.setItemMetadata(rightsHolder);

		return object;
	}

	/**
	 * Saves the given project file record to the given file. This is a
	 * long-running operation.
	 * 
	 * @param projectFile
	 *            The project file record to save.
	 * @param file
	 *            The {@link IFile} to save to.
	 * @param monitor
	 *            Provides progress monitoring.
	 * @throws ProjectFileException
	 *             when either serializing fails (= bug) or writing fails
	 */
	protected void saveProjectFile(TgProjectFile projectFile,
			TextGridObject object, IProgressMonitor monitor)
			throws ProjectFileException {
		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridProjectFile_Saving, project), 90);
		try {
			System.setProperty(TransformerFactory.class.getName(), "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); //$NON-NLS-1$
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JAXB.marshal(projectFile, baos);
			progress.worked(5);

			IFile file = (IFile) object.getAdapter(IFile.class);
			if (file == null) {
				throw new ProjectFileException(
						Operation.SAVING,
						project,
						null,
						"Could not convert the TextGridObject for the project file, {0}, to a file. " //$NON-NLS-1$
								+ "If no previous error message explains this strange incident, please file a bug report.", //$NON-NLS-1$
						object);
			}
			file.setContents(new ByteArrayInputStream(baos.toByteArray()),
					IFile.FORCE, progress.newChild(85));
		} catch (CoreException e) {
			throw new ProjectFileException(Operation.SAVING, project, e);
		}
	}

	protected TgProjectFile createProjectFileSkeleton() {
		// --------------------------------------------------------------------------------
		// Create a new minimal project file.
		// --------------------------------------------------------------------------------

		TgProjectFile projectFile = new TgProjectFile();
		projectFile.setDescription(project.getProjectInfo().getDescription());
		// ObjectTypes objectTypes = new ObjectTypes();

		// Type_type0 type = new Type_type0();
		//
		// HasSchema_type0 hasSchema = new HasSchema_type0();
		// try {
		// hasSchema.setAnyURI(new org.apache.axis2.databinding.types.URI(
		// "textgrid:uri"));
		// } catch (MalformedURIException e) {
		// Activator.handleError(e,
		// "Error TextGridProjectFile.createProjectFileSkeleton()");
		// }
		//
		// type.addHasSchema(hasSchema);
		//
		// objectTypes.addType(type);
		//
		// projectFile.setObjectTypes(objectTypes);
		//
		// XmlSchemas_type0 xmlSchemas = new XmlSchemas_type0();
		//
		// Schema_type0 schema = new Schema_type0();
		//
		// HasAdaptor_type0 adaptor = new HasAdaptor_type0();
		// try {
		// adaptor.setAnyURI(new org.apache.axis2.databinding.types.URI(
		// "textgrid:uri"));
		// } catch (MalformedURIException e) {
		// Activator.handleError(e,
		// "Error TextGridProjectFile.createProjectFileSkeleton()");
		// }
		//
		// schema.addHasAdaptor(adaptor);
		// // schema.setMetadataTemplate(new
		// // org.apache.axis2.databinding.types.URI());
		// // schema.setUri(new org.apache.axis2.databinding.types.URI());
		//
		// xmlSchemas.addSchema(schema);
		//
		// projectFile.setXmlSchemas(xmlSchemas);
		//
		// AppData_type0 app = new AppData_type0();
		// projectFile.setAppData(app);

		return projectFile;
	}

	public boolean setProjectFile(URI uri) {

		TextGridObject object = null;
		try {
			object = TextGridObject.getInstance(uri, true);
			try {
				if (object != null
						&& object.getContentTypeID().equals(PROJECT_FILE_TYPE)) {
					this.projectFileObject = object;
					try {
						IFile file = (IFile) this.projectFileObject
								.getAdapter(IFile.class);
						InputStream inputStream = file.getContents(true);
						System.setProperty(TransformerFactory.class.getName(), "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); //$NON-NLS-1$
						this.projectFileData = JAXB.unmarshal(inputStream,
								TgProjectFile.class);
					} catch (Exception e) {
						throw new ProjectFileException(Operation.READING,
								project, e);
					}

					return true;
				}
			} catch (CoreException e) {
				Activator.handleError(e,
						"Error TextGridProjectFile.setProjectFile()"); //$NON-NLS-1$
			}
		} catch (CrudServiceException e) {
			Activator.handleError(e,
					"Error TextGridProjectFile.setProjectFile()"); //$NON-NLS-1$
		}

		return false;
	}

	public boolean setProjectFile(TextGridObject object) {

		return setProjectFile(object.getURI());
	}

	public TgProjectFile getProjectFileData(boolean create, boolean reload,
			IProgressMonitor monitor) throws ProjectFileException {
		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridProjectFile_Loading, this), 100);

		if (projectFileObject == null || reload)
			projectFileObject = getProjectFileObject(create,
					progress.newChild(50));
		else
			progress.worked(50);

		if (projectFileObject != null && !isNewObject) {
			IFile file = null;
			try {
				file = (IFile) projectFileObject.getAdapter(IFile.class);
				System.setProperty(TransformerFactory.class.getName(), "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); //$NON-NLS-1$
				projectFileData = JAXB.unmarshal(file.getContents(),
						TgProjectFile.class);
			} catch (Exception e) {
				//
				// // try it again with ISO-8859-1 encoding. for old project
				// files
				// // created on windows
				// try {
				// InputStream inputStream = file.getContents(true);
				// XMLStreamReader reader = XMLInputFactory.newInstance()
				//							.createXMLStreamReader(inputStream, "ISO-8859-1"); //$NON-NLS-1$
				// projectFileData = TgProjectFile.Factory.parse(reader);
				// } catch (Exception e1) {
				if (create)
					throw new ProjectFileException(Operation.CREATING, project,
							e);
				else
					throw new ProjectFileException(Operation.READING, project,
							e);
				// }
			}
		} else {
			projectFileData = createProjectFileSkeleton();
		}

		return projectFileData;
	}

	/**
	 * maybe Saves the given data as the associated project's project file. If
	 * the project already has a project file, it's content is replaced.
	 * Otherwise, a new ProjectFile object is created and associated with this
	 * project.
	 * 
	 * @param data
	 * @param monitor
	 * @return
	 * @throws ProjectFileException
	 * @throws CrudServiceException
	 */
	public TextGridObject saveProjectFileData(TgProjectFile data,
			IProgressMonitor monitor) throws ProjectFileException,
			CrudServiceException {
		SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TextGridProjectFile_Saving, project), 100);
		TextGridObject object = getProjectFileObject(false,
				progress.newChild(30));

		boolean needsSet = false; // do we need to call setProjectFileURI?
		if (object == null) {
			object = createNewProjectFileObject();
			needsSet = true;
		} else {
			object.reloadMetadata(true);
		}

		saveProjectFile(data, object, progress.newChild(60));
		if (needsSet) {
			setProjectFile(object);
		}

		progress.worked(10);

		return object;
	}

	/**
	 * Returns the type of the object (item, edition,...) from its format.
	 * 
	 * @param format
	 * @return
	 */
	public static String getTypeFromFormat(final String format) {
		String objectType = ""; //$NON-NLS-1$

		if (format != null && !"".equals(format)) { //$NON-NLS-1$
			if (format.toLowerCase().contains("tg.work+")) { //$NON-NLS-1$
				objectType = "work"; //$NON-NLS-1$
			} else if (format.toLowerCase().contains("tg.edition+")) { //$NON-NLS-1$
				objectType = "edition"; //$NON-NLS-1$
			} else if (format.toLowerCase().contains("tg.collection+")) { //$NON-NLS-1$
				objectType = "collection"; //$NON-NLS-1$
			} else {
				objectType = "item"; //$NON-NLS-1$
			}
		}

		return objectType;
	}

	/**
	 * Set the basic metadata to the new created object.
	 * 
	 * @param project
	 * @param type
	 * @param newObj
	 */
	public static void setBasicMetadataToNewObject(final String project,
			final String type, final TextGridObject newObj) {

		try {
			final TextGridProjectFile projectFile = new TextGridProjectFile(
					TextGridProject.getProjectInstance(project));

			TgProjectFile projectFileData = projectFile.getProjectFileData(
					false, true, null);

			OMElement[] appDataChildren = null;
			appDataChildren = extractAppDataChildren(projectFileData);

			OMElement basicMetadataSection = null;
			if (appDataChildren != null && appDataChildren.length > 0) {
				for (OMElement child : appDataChildren) {
					if (child.getQName().equals(
							TextGridProjectFile.basicMetadataSectionQName)) {
						basicMetadataSection = child;
						break;
					}
				}
			}

			if (basicMetadataSection != null) {
				OMElement type_elem = basicMetadataSection
						.getFirstChildWithName(new QName(
								TextGridProjectFile.basicMetadataSectionNS,
								type));

				if (type_elem != null) {
					OMElement basicMetadata = type_elem.getFirstElement();
					TGContentType contentType = newObj.getContentType();
					newObj.setMetadataXML(basicMetadata);
					newObj.setContentType(contentType);
				}
			}

		} catch (RemoteException e1) {
			Activator.handleError(e1, "error..."); //$NON-NLS-1$
		} catch (CrudServiceException e1) {
			Activator.handleError(e1, "error..."); //$NON-NLS-1$
		} catch (ProjectDoesNotExistException e1) {
			Activator.handleError(e1, "error..."); //$NON-NLS-1$
		} catch (ProjectFileException e) {
			Activator.handleError(e, "error..."); //$NON-NLS-1$
		} catch (CoreException e) {
			Activator.handleError(e, "error..."); //$NON-NLS-1$
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e, Messages.TextGridProjectFile_ParseError,
					e.getMessage());
		}
	}

	/**
	 * Extracts the application specific part of the given argument and converts
	 * it to an array of {@link OMElement}s.
	 * 
	 * TODO this could probably be refactored to be a proper method.
	 * 
	 * @param projectFile
	 *            project file record
	 * @return the appData OMElement, or <code>null</code> if either
	 *         <var>projectFile</var> was <code>null</code> or no application
	 *         data has been found.
	 *
	 */
	public static OMElement extractAppDataXML(final TgProjectFile projectFile) {
		if (projectFile == null)
			return null;

		final AppData appData = projectFile.getAppData();
		if (appData != null) {
			// XXX Hack:
			StringWriter writer = new StringWriter();
			System.setProperty(TransformerFactory.class.getName(), "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); //$NON-NLS-1$
			JAXB.marshal(appData, writer);
			OMElement omElement;
			try {
				omElement = StringToOM.getOMElement(writer.toString());
				return omElement;
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(
						Messages.TextGridProjectFile_EncodingError, e);
			} catch (XMLStreamException e) {
				throw new IllegalArgumentException(
						Messages.TextGridProjectFile_ParseError2, e);
			} catch (FactoryConfigurationError e) {
				throw new IllegalStateException(
						"Couldn't configure OM factory!?", e); //$NON-NLS-1$
			}
		} else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public static OMElement[] extractAppDataChildren(final TgProjectFile projectFile) {
		OMElement element = extractAppDataXML(projectFile);
		if (element == null)
			return null;
		else
			return Iterators.toArray(element.getChildElements(), OMElement.class);
	}
	
}