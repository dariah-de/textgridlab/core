package info.textgrid.lab.core.model;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

/**
 * Thrown from operations that require {@linkplain TextGridObject#isComplete()
 * complete} metadata.
 * 
 * @see TextGridObject#makeMetadataPersistent()
 * @see TextGridObject#isComplete()
 */
@SuppressWarnings("serial")
public class IncompleteMetadataException extends CoreException {

	public IncompleteMetadataException(IStatus status) {
		super(status);
	}

}
