package info.textgrid.lab.core.model;


/**
 * A listener that is called when something happens with a project. You may
 * add your listeners with
 * {@link TextGridProject#addListener(ITextGridProjectListener)} and remove
 * them using {@link TextGridProject#removeListener(ITextGridProjectListener)}
 * .
 */
public interface ITextGridProjectListener {

	public enum Event {
		/**
		 * Deleted events are fired when the project has been deleted
		 */
		DELETED,
		/**
		 * When a project has been created.
		 */
		CREATED,
		/**
		 * When the projects content has been changed.
		 */
		CONTENT_CHANGED,
		/**
		 * The project has no visible TG-Objects.
		 */
		NO_CHILDREN_CHECK,
	};

	/**
	 * The given <var>event</var> has happend to the given
	 * <var>project</var>.
	 */
	public void textGridProjectChanged(Event event, TextGridProject object);

}
