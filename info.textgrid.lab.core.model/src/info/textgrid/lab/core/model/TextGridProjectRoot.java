package info.textgrid.lab.core.model;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.core.swtutils.ChunkingElementCollector;
import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.lab.log.logsession;
import info.textgrid.namespaces.middleware.tgauth.GetAllProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetAllProjectsResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;
import info.textgrid.namespaces.middleware.tgauth.RolesetResponse;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedProjectsRequest;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;

/**
 * @author hausner
 * 
 *         The NavigatorRoot class represents the root-object in the input-model
 *         of the Navigator.
 * 
 */

public class TextGridProjectRoot extends PlatformObject implements
		IDeferredWorkbenchAdapter, IChildListParent, ISchedulingRule {

	private static TextGridProjectRoot root_level_none;

	private LinkedList<TextGridProject> projects = null;
	private ListenerList projectChangedListeners = new ListenerList();

	private LEVELS level = LEVELS.NONE;

	public static enum LEVELS {
		NONE, OBSERVER_OR_PUBLIC_RESOURCES, OBSERVER, EDITOR, PROJECT_LEADER
	};

	private TextGridProjectRoot() {

	}

	/**
	 * deletes all TextGridProjects and TextGridObjects from the model
	 */
	public void reset() {

		if (this.projects != null) {
			for (TextGridProject project : projects) {
				project.reset();
			}
		}
		resetProjects();
	}

	public void resetProjects() {
		// synchronized(this){
		this.projects = null;
		notifyChildListChangedListeners();
		// }
	}

	private void setLevel(LEVELS level) {

		this.level = level;
	}

	private LEVELS getLevel() {

		return this.level;
	}

	/**
	 * @return the singleton-instance of NavigatorRoot
	 */
	public static TextGridProjectRoot getInstance() {
		// TODO implemented the class as singleton concerning Level NONE to fix TG-498.
		// It should be verified whether the singleton implementation is profitable for the other levels too. 
		if (root_level_none == null) {
			root_level_none = new TextGridProjectRoot();
			root_level_none.level = LEVELS.NONE;
		}		
		return root_level_none;
	}

	public static TextGridProjectRoot getInstance(LEVELS level) {

		TextGridProjectRoot root = new TextGridProjectRoot();
		root.setLevel(level);

		return root;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.progress.IDeferredWorkbenchAdapter#fetchDeferredChildren
	 * (java.lang.Object, org.eclipse.ui.progress.IElementCollector,
	 * org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void fetchDeferredChildren(Object object,
			IElementCollector collector, IProgressMonitor monitor) {
		monitor.beginTask(Messages.TextGridProjectRoot_Loading, IProgressMonitor.UNKNOWN);
		synchronized (this) {
			
			final ChunkingElementCollector groupingCollector = new ChunkingElementCollector(collector, 16);

			// System.out.println(MessageFormat.format("begin fetchdeferredchildren({0}, {1}, {2})",
			// object, collector, monitor));
			// new Exception().printStackTrace();

			if (object instanceof TextGridProjectRoot) {

				// if (projects != null) {
				//
				// for (TextGridProject project : projects) {
				// project.reset();
				// }
				// this.projects = null;
				// System.out.println(MessageFormat.format("end fetchdeferredchildren({0}, {1}, {2})",
				// object, collector, monitor));
				// return;
				// }

				projects = new LinkedList<TextGridProject>();

				try {
					String rbacID = RBACSession.getInstance().getSID(level != LEVELS.OBSERVER_OR_PUBLIC_RESOURCES);
					String loggingParam = logsession.getInstance().getloginfo();
					PortTgextra stub = null;

					stub = TgAuthClientUtilities.getTgAuthServiceStub();

					RolesetResponse response = null;
					List<String> projectstrings = null;
					TgAssignedProjectsRequest request = new TgAssignedProjectsRequest();

					request.setAuth(rbacID);

					request.setLog(loggingParam);

					request.setLevel(this.getLevel().ordinal());

					response = stub.tgAssignedProjects(request);

					OnlineStatus.setOnline();

					if (response != null) {
						projectstrings = response.getRole();
					}

					// Reducing Web Service Calls caused by
					// TextGridProject.getProjectInstance(projectstrings[i]);
					// from n to 1, assuming one call returning much data is
					// cheaper than many small calls returning some bits each
					GetAllProjectsRequest gap = new GetAllProjectsRequest();
					gap.setAuth(rbacID);
					gap.setLog(loggingParam);
					GetAllProjectsResponse allProjects = new GetAllProjectsResponse();
					allProjects = stub.getAllProjects(gap);

					if (allProjects != null && projects != null
							&& projectstrings != null) {
						List<ProjectInfo> allPI = allProjects.getProject();
						for (ProjectInfo pi : allPI) {
							for (String myProject : projectstrings) {
								if (pi.getId().equals(myProject)) {
									TextGridProject project = TextGridProject
											.getProjectInstance(pi);
									projects.add(project);
									groupingCollector.add(project, monitor);
									break;
								}
							}
						}
					}

				} catch (RemoteException e) {
					OnlineStatus.netAccessFailed(
							Messages.TextGridProjectRoot_CouldNotFetchProjects, e);
				}

				groupingCollector.done();
				collector.done();
				monitor.done();

			} else {
				throw new IllegalArgumentException(
						MessageFormat
								.format(
										"NavigatorRoot''s fetchDeferredChildren does not know the children of {0}s like {1}", //$NON-NLS-1$
										object.getClass(), object));
			}

			// System.out.println(MessageFormat.format("end fetchdeferredchildren({0}, {1}, {2})",
			// object, collector, monitor));
		}
	}

	public ISchedulingRule getRule(Object object) {
		// TODO Auto-generated method stub
		return this;
	}

	public boolean isContainer() {
		return true;
	}

	public Object[] getChildren(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public ImageDescriptor getImageDescriptor(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLabel(Object o) {
		if (o == this) {
			switch (level) {
			case OBSERVER_OR_PUBLIC_RESOURCES:
				return Messages.TextGridProjectRoot_EverythingReadable;
			case OBSERVER:
				return Messages.TextGridProjectRoot_MyObserverProjects;
			case EDITOR:
				return Messages.TextGridProjectRoot_MyEditorProjects;
			case PROJECT_LEADER:
				return Messages.TextGridProjectRoot_MyManagerProjects;
			case NONE:
				return Messages.TextGridProjectRoot_MyProjects;
			}
		}
		return Messages.TextGridProjectRoot_TGProjects;
	}

	public Object getParent(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public void addChildListChangedListener(IChildListChangedListener listener) {
		projectChangedListeners.add(listener);
	}

	public void removeChildListChangedListener(
			IChildListChangedListener listener) {
		projectChangedListeners.remove(listener);

	}

	protected void notifyChildListChangedListeners() {
		Object[] listeners = projectChangedListeners.getListeners();
		for (int i = 0; i < listeners.length; ++i) {
			((IChildListChangedListener) listeners[i]).childListChanged(this);
		}
	}

	public boolean contains(ISchedulingRule rule) {
		if (rule instanceof TextGridProjectRoot && rule.equals(this)) {
			return true;
		}
		return false;
	}

	public boolean isConflicting(ISchedulingRule rule) {
		if (rule instanceof TextGridProjectRoot) {
			return true;
		}
		return false;
	}

}
