/**
 * 
 */
package info.textgrid.lab.protocol.textgrid;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.osgi.service.url.AbstractURLStreamHandlerService;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;

/**
 * @author tv
 *
 */
public class TextGridURLHandler extends AbstractURLStreamHandlerService {

	public static final String PROTOCOL = "textgrid"; //$NON-NLS-1$

	/* (non-Javadoc)
	 * @see org.osgi.service.url.AbstractURLStreamHandlerService#openConnection(java.net.URL)
	 */
	@Override
	public URLConnection openConnection(URL u) throws IOException {
//		return new TextGridLabURLConnection(u);
		return openCrudConnection(u);		
	}

	private URLConnection openCrudConnection(URL u) throws OfflineException, MalformedURLException, IOException {
		final String crudSOAP = ConfClient.getInstance().getValue(ConfservClientConstants.TG_CRUD);
		String restURL = crudSOAP.substring(0, crudSOAP.lastIndexOf('/') + 1)
				.concat("rest/")
				.concat(u.toExternalForm())
				.concat("/data");
		String sid = RBACSession.getInstance().getSID(false);
		if (!sid.isEmpty())
			restURL = restURL.concat("?sessionId=").concat(sid);
		URL crudURL = new URL(restURL);
		return crudURL.openConnection();
	}

}
