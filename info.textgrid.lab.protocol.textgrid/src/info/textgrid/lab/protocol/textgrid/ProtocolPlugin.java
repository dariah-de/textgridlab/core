package info.textgrid.lab.protocol.textgrid;

import java.util.Hashtable;

import org.eclipse.ui.IStartup;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.url.URLConstants;
import org.osgi.service.url.URLStreamHandlerService;

public class ProtocolPlugin implements BundleActivator, IStartup {

//	private static final String PROTOCOL_PROPERTY = "java.protocol.handler.pkgs"; //$NON-NLS-1$
//	private static final String PROTOCOL_PREFIX = "info.textgrid.lab.protocol"; //$NON-NLS-1$
	public static final String PLUGIN_ID = "info.textgrid.lab.protocol.textgrid";//$NON-NLS-1$
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	private static boolean registered;

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		ProtocolPlugin.context = bundleContext;
		earlyStartup();
	}

	public void earlyStartup() {
		registerHandlerPackage();
	}

	public static void registerHandlerPackage() {
		if (registered)
			return;
//		String property = System.getProperty(PROTOCOL_PROPERTY);
//		if (property == null || property.isEmpty())
//			property = PROTOCOL_PREFIX;
//		else
//			property = PROTOCOL_PREFIX + "|" + property ; //$NON-NLS-1$
//		System.setProperty(PROTOCOL_PROPERTY, property);
//		System.out.println(/*new Status(IStatus.INFO, ProtocolPlugin.PLUGIN_ID,*/ MessageFormat.format("Protocol handlers are now searched in {0}", System.getProperty(PROTOCOL_PROPERTY))); //$NON-NLS-1$
		
	    Hashtable<String, String[]> properties = new Hashtable<String, String[]>(1);
	    properties.put(URLConstants.URL_HANDLER_PROTOCOL, new String[] {TextGridURLHandler.PROTOCOL});
	    String serviceClass = URLStreamHandlerService.class.getName();
	    context.registerService(serviceClass, new TextGridURLHandler(), properties);
		
		registered = true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		ProtocolPlugin.context = null;
	}

}
