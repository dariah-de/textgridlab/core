package info.textgrid.lab.protocol.textgrid;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.protocol.textgrid.messages"; //$NON-NLS-1$
	public static String TextGridLabURLConnection_FailedToAccessUri;
	public static String TextGridLabURLConnection_InvalidTGURI;
	public static String TextGridLabURLConnection_xDoesNotExist;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
