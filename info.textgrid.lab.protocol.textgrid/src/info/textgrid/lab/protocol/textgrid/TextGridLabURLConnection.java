package info.textgrid.lab.protocol.textgrid;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.statushandlers.StatusManager;

public class TextGridLabURLConnection extends URLConnection {

	private TextGridObject textGridObject;
	private IFile file;

	public TextGridLabURLConnection(final URL url) {
		super(url);
	}

	@Override
	public void connect() throws IOException {
		try {
			textGridObject = TextGridObject.getInstance(getURL().toURI(), true);
			file = AdapterUtils.getAdapter(textGridObject, IFile.class);
			if (file == null)
				throw new IOException(
						MessageFormat
								.format("There was an unknown error trying to create the Eclipse representation for {0} ({1}). Check previous errors for details.",
										getURL(), textGridObject));
		} catch (final CrudServiceException e) {
			if (e.getCause() != null && e.getCause() instanceof ObjectNotFoundFault) {
				FileNotFoundException fileNotFoundException = new FileNotFoundException(MessageFormat.format(Messages.TextGridLabURLConnection_xDoesNotExist, getURL()));
				fileNotFoundException.initCause(e);
				throw fileNotFoundException;
			}  else {
				throw new IOException(MessageFormat.format(Messages.TextGridLabURLConnection_FailedToAccessUri, getURL(), e.getLocalizedMessage()));
			}
			// TODO Specialized exception(s)
		} catch (final URISyntaxException e) {
			throw new IOException(MessageFormat.format(
					Messages.TextGridLabURLConnection_InvalidTGURI, getURL(),
					e.getLocalizedMessage()), e);
		}
	}

	@Override
	public String getHeaderField(final String name) {
		if (textGridObject == null)
			throw new IllegalStateException("Need to connect first."); //$NON-NLS-1$

		try {
			if (name.equalsIgnoreCase("content-type")) //$NON-NLS-1$
				return textGridObject.getContentTypeID();
			else if (name.equalsIgnoreCase("last-modified")) //$NON-NLS-1$
				return textGridObject.getLastModified().getTime().toString();
			else if (name.equalsIgnoreCase("date")) //$NON-NLS-1$
				return textGridObject.getMetadataForReading().getGeneric()
						.getGenerated().getCreated().toString();
		} catch (final CoreException e) {
			StatusManager.getManager().handle(e, ProtocolPlugin.PLUGIN_ID);
		}
		return super.getHeaderField(name);
	}

	@Override
	public InputStream getInputStream() throws IOException {
		if (file == null)
			connect();
		try {
			return file.getContents();
		} catch (final CoreException e) {
			throw new IOException(e);
		}
	}
}
