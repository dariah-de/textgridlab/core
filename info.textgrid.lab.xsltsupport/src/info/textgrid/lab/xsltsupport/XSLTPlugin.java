package info.textgrid.lab.xsltsupport;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class XSLTPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.xsltsupport";

	// The shared instance
	private static XSLTPlugin plugin;
	
	/**
	 * The constructor
	 */
	public XSLTPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static XSLTPlugin getDefault() {
		return plugin;
	}

	public static IStatus handleError(Throwable e, String message,
			Object... arguments) {
		IStatus status;
		String formattedMessage;
		if (message != null)
			formattedMessage = MessageFormat.format(message, arguments);
		else if (e != null) {
			formattedMessage = e.getLocalizedMessage();
		} else {
			formattedMessage = "An error occurred and someone forgot to specify the message.";
		}
		if (e instanceof CoreException) {
			status = new MultiStatus(PLUGIN_ID, 0,
					new IStatus[] { ((CoreException) e).getStatus() },
					formattedMessage,
					e); 
		} else {
			status = new Status(IStatus.ERROR, PLUGIN_ID, formattedMessage, e);
		}
		StatusManager.getManager().handle(status);
		return status;
	}
}
