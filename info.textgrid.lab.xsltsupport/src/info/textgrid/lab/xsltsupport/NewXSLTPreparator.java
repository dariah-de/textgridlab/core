package info.textgrid.lab.xsltsupport;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;

public class NewXSLTPreparator implements INewObjectPreparator {

	private static final String XSLT_NAMESPACE = "http://www.w3.org/1999/XSL/Transform"; //$NON-NLS-0$
	private ITextGridWizard wizard;

	public void initializeObject(TextGridObject textGridObject) {

	}

	public boolean performFinish(final TextGridObject textGridObject) {
		
		try {
			InputStream stylesheet = FileLocator.openStream(XSLTPlugin
					.getDefault().getBundle(), new Path(
					"stylesheet-template.xsl"), true);
			textGridObject.setInitialContent(stylesheet, null);
		} catch (IOException e) {
			XSLTPlugin
					.handleError(
							e,
							"Could not read template file for new XSLT stylesheet {0}: {1}",
							textGridObject, e.getLocalizedMessage());
		}
		
		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard).defaultPerformFinish();
		else
			return false;
		
	}

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;
	}

}
