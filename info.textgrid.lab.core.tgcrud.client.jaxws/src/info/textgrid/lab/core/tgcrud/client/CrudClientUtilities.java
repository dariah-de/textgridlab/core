package info.textgrid.lab.core.tgcrud.client;

import info.textgrid.lab.conf.ConfPlugin;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService_Service;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.MTOMFeature;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.osgi.util.NLS;

/**
 * Some utility methods for TGcrud clients.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for <a
 *         href="http://www.textgrid.info/">TextGrid</a> (tv)
 * 
 */
public class CrudClientUtilities {

	private static final String TGCRUD_TIMEOUT_PROP = "info.textgrid.lab.core.tgcrud.timeout";
	private static final int DEFAULT_TIMEOUT = 360000;
	private static TGCrudService_Service service;
	private static TGCrudService stub;

	/**
	 * Please use {@link #getCrudServiceStub()} instead.
	 * 
	 * @return The end point reference to use for TG-crud
	 * @see #getCrudServiceStub()
	 */
	private static String getCrudEPR() {

		try {
			return ConfClient.getInstance().getValue(
					ConfservClientConstants.TG_CRUD);
		} catch (OfflineException e) {
			OnlineStatus.netAccessFailed(
					"Could not retrieve TG-Crud EPR from configuration server",
					e);
		}
		return null;
	}

	/**
	 * Always use this method to get a configured serviceStub ...
	 * 
	 * The service stub will be configured with a default timeout of
	 * {@value #DEFAULT_TIMEOUT} ms. Alternatively, you can set the system
	 * property {@value #TGCRUD_TIMEOUT_PROP} to set the timeout value in ms
	 * (well, if you supply a value < 1000 it will be interpreted as seconds.)
	 * 
	 * @return a service stub with current options, e.g. MTOM disabled
	 */
	public static synchronized TGCrudService getCrudServiceStub() {
		if (stub != null)
			return stub;
		try {
			stub = TGCrudClientUtilities.getTgcrud(getCrudEPR(), true);
		} catch (MalformedURLException e1) {
			throw new IllegalStateException(MessageFormat.format("The configured endpoint {0} is invalid.", getCrudEPR()), e1);
		}
		// http://cxf.apache.org/docs/client-http-transport-including-ssl-support.html#ClientHTTPTransport%28includingSSLsupport%29-Usingjavacode
		Client client = ClientProxy.getClient(stub);
		
        IPreferenceStore preferenceStore = ConfPlugin.getDefault().getPreferenceStore();
        if(preferenceStore.getBoolean(ConfPlugin.COMPRESSED_TRANSFER)) {
        	client.getInInterceptors().add(new GZIPInInterceptor());
        	// TODO: reactivate GZIPOutInterceptor() after enabling gzip on production server
        	//Feature #12770
        	client.getOutInterceptors().add(new GZIPOutInterceptor());
        }
		
		HTTPConduit http = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();

		int timeout = DEFAULT_TIMEOUT;
		try {
			String timeoutProp = System.getProperty(TGCRUD_TIMEOUT_PROP);
			if (timeoutProp != null) {
				int parsedTimeout = Integer.parseInt(timeoutProp);
				if (parsedTimeout < 1000)
					parsedTimeout *= 1000;
				timeout = parsedTimeout;
				TGCrudClientPlugin
						.getDefault()
						.getLog()
						.log(new Status(
								IStatus.INFO,
								TGCrudClientPlugin.PLUGIN_ID,
								NLS.bind(
										"Manually set TG-crud timeout to {0} ms",
										timeout)));
			}
		} catch (RuntimeException e) {
			TGCrudClientPlugin
					.getDefault()
					.getLog()
					.log(new Status(IStatus.WARNING,
							TGCrudClientPlugin.PLUGIN_ID,
							"An error occurred while reading the timeout property"
									+ TGCRUD_TIMEOUT_PROP, e));
		}

		httpClientPolicy.setConnectionTimeout(timeout);
		// httpClientPolicy.setAllowChunking(false);
		httpClientPolicy.setReceiveTimeout(timeout);
		http.setClient(httpClientPolicy);

		// TODO see if we need to configure something else

		return stub;
	}

}
