package info.textgrid.lab.core.tgcrud.client;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class TGCrudClientPlugin extends Plugin implements BundleActivator {
	
	public static final String PLUGIN_ID = "info.textgrid.lab.core.tgcrud.client.jaxws"; //$NON-NLS-1$
	private static TGCrudClientPlugin instance; 

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;
	}
	
	
	public static TGCrudClientPlugin getDefault() {
		return instance;
	}
	

	
	
}
