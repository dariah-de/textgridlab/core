package info.textgrid.lab.conf;

import java.net.NoRouteToHostException;
import java.text.MessageFormat;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.State;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.menus.IMenuStateIds;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.progress.UIJob;

/**
 * Maintains an online status.
 * 
 * Clients should never instantiate this class, but they can call the following
 * methods:
 * <ul>
 * <li>{@link #isOnline()} to determine whether we are online (and perform
 * client-side error handling)</li>
 * <li>{@link #isOnlineWithNotification(String)} if we should display a
 * notification message if offline</li>
 * <li>{@link #netAccessFailed(String, Throwable)} after a network operation to
 * some TextGrid utility failed (e.g. on an {@link OfflineException} or after a
 * request to Crud, Search, Auth, Logging etc.)</li>
 * </ul>
 * 
 * This object's methods can be called from any thread, even if they perform 
 * UI interaction.
 * 
 */
public class OnlineStatus {
	
	/**
	 * An {@link IStatusChangeListener}'s statusChanged method is called whenever
	 * the online status changed. There is no initial notification for the <var>true</var>
	 * status.
	 */
	public interface IStatusChangeListener {
		/**
		 * Called when the online status has changed.
		 * @param newStatus the new online state.
		 */
		public void statusChanged(boolean newStatus);
	}
	

	private static boolean online = true;
	private static ListenerList onlineStatusChangeListeners = new ListenerList();
	private static IStatusLineManager statusLineManager = new StatusLineManager();
	private static String OFFLINE_COMMAND;
	
	/**
	 * Adds the given listener as a status change listener. Clients are
	 * responsible for removing themselves again. 
	 * Duplicates are cared for automatically.
	 */
	public static void addOnlineStatusChangeListener(IStatusChangeListener listener) {
		onlineStatusChangeListeners.add(listener);
	}
	/**
	 * Removes the given class from the list of status change listeners.
	 */
	public static void removeOnlineStatusChangeListener(IStatusChangeListener listener) {
		onlineStatusChangeListeners.remove(listener);
	}
	
	/**
	 * Notifies the listeners of the new status.
	 */
	protected static void notifyOnlineStatusChangeListeners(boolean newStatus) {
		for (Object listener : onlineStatusChangeListeners.getListeners()) {
			((IStatusChangeListener) listener).statusChanged(newStatus);
		}
	}
	
	
	/**
	 * @return true if we are online.
	 */
	public static boolean isOnline() {
		return online;
	}

	
	/**
	 * Determines whether we are online. If not, show the unobstrusive
	 * notification given as argument.
	 * 
	 * @param message
	 *            the message to display
	 * @return the online status
	 */
	public static boolean isOnlineWithNotification(String message) {
		if (online)
			return true;
		showUnobstrusiveOnlineErrorMessage(message, null);
		return false;

	}

	/**
	 * Make us online.
	 */
	public static void setOnline() {
		setOnline(true);
	}

	/**
	 * Make us <var>newOnlineState</var>.
	 */
	public static void setOnline(boolean newOnlineState) {
		boolean oldOnline = online;
		online = newOnlineState;

		if (oldOnline != newOnlineState) {

			// notify the GUI item
			final ICommandService commandService = (ICommandService) PlatformUI
					.getWorkbench().getService(ICommandService.class);
			OFFLINE_COMMAND = "info.textgrid.lab.conf.toggleoffline"; //$NON-NLS-1$
			Command offlineCommand = commandService.getCommand(OFFLINE_COMMAND);
			State state = offlineCommand.getState(IMenuStateIds.STYLE);
			state.setValue(!online);
			
			PlatformUI.getWorkbench().getDisplay().syncExec(
					new Runnable() {
						public void run(){
							commandService.refreshElements(OFFLINE_COMMAND, null);
						}
					}
			);

			notifyOnlineStatusChangeListeners(online);
		}
	}

	/**
	 * Make us offline.
	 */
	public static void setOffline() {
		setOnline(false);
	}
	
	/**
	 * Check if the given exception is known to
	 * occur on network-connection failures
	 * 
	 * @param e exception to check
	 * @return
	 */
	public static boolean isOfflineException(Exception e) {
		
		if(e.getMessage().toLowerCase().startsWith("no route to host")) return true; //$NON-NLS-1$
		if (e instanceof NoRouteToHostException || e.getCause() != null
				&& e.getCause() instanceof NoRouteToHostException)
			return true; // in the latter case e may be e.g. an AxisFault
		
		return false;
	}

	/**
	 * Sets the current status to offline and notifies clients that network
	 * access failed and how they can re-enable their onlineness.
	 * 
	 * An error dialog is only shown if they were online before, otherwise, the
	 * notification is more
	 * {@linkplain #showUnobstrusiveOnlineErrorMessage(String, Throwable)
	 * subtle}.
	 * 
	 * @param message
	 *            a human-readable and localizeable message describing which
	 *            operation failed.
	 * @param cause
	 *            the exception that caused the problem
	 */
	public static void netAccessFailed(String message, Throwable cause) {

		ConfPlugin.log(IStatus.ERROR, cause, "network access failed ({0})", //$NON-NLS-1$
				message);

		if (isOnline()) {
			setOffline();
			showOfflineErrorDialog(message, cause);
		} else {
			showUnobstrusiveOnlineErrorMessage(message, cause);
		}
	}

	/**
	 * Shows an unobstrusive error message due to unwanted offlineness. Usually,
	 * an error notification will be shown in the StatusBar. This logs the
	 * notification, as well.
	 * 
	 * @param message
	 *            The message to display.
	 * @param cause
	 *            An (optional) exception to log. (may be null)
	 */
	protected static void showUnobstrusiveOnlineErrorMessage(final String message,
			final Throwable cause) {
		
		new UIJob("Showing unobtrusive online error message") { //$NON-NLS-1$

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				// TODO: write to statusbar
				// IStatusLineManager statusLineManager = PlatformUI.getWorkbench().
				// .getEditorSite().getActionBars().getStatusLineManager();
				// statusLineManager.setErrorMessage(message);
				// ConfPlugin.log(IStatus.ERROR, cause,
				// "This message should be on statusbar ({0})", message);
				statusLineManager.setErrorMessage(MessageFormat.format(
						Messages.OnlineStatus_x_SystemIsOffline, message));
				ConfPlugin.log(IStatus.INFO, cause,
						Messages.OnlineStatus_OfflineNotification, message);
				
				return Status.OK_STATUS;
			}			
		}.schedule();
		
		
	}

	protected static void showOfflineErrorDialog(final String message, final Throwable cause) {
		
		new UIJob("Display offline error dialog") { //$NON-NLS-1$

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				if (monitor == null) {
					monitor = new NullProgressMonitor();
				}
				IStatus status = new Status(IStatus.ERROR, ConfPlugin.PLUGIN_ID,
						MessageFormat.format(
								Messages.OnlineStatus_NetworkAccessFailedReason,
								message, cause));
				
				String dialogMessage = MessageFormat
				.format(
						Messages.OnlineStatus_NetworkAccessPossible,
						message);
				
				ErrorDialog errorDialog = new ErrorDialog(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(),
						Messages.OnlineStatus_AccessFailed, dialogMessage, status,
						0xFFFFFFF);

				errorDialog.setBlockOnOpen(false);
				if (!monitor.isCanceled())
					errorDialog.open();
				return Status.OK_STATUS;
			}
		}.schedule();
		
	}
	public static void setStatusLineManager(IStatusLineManager statusLineManager) {
		OnlineStatus.statusLineManager = statusLineManager;
	}

}
