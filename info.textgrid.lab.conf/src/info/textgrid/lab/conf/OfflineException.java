package info.textgrid.lab.conf;

import java.io.IOException;

public class OfflineException extends IOException {

	private static final long serialVersionUID = -2529297222958986947L;

	public OfflineException(String message, Throwable cause) {
		super(message);
		if (cause != null) initCause(cause);
	}

}
