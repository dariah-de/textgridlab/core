/**
 * Together with {@link info.textgrid.lab.conf.client.ConfClient}, this package contains
 * the Eclipse side of the configuration stuff.
 * 
 * <p>
 * {@link ConfPlugin} is this plugin's Activator. Use 
 * <code>ConfPlugin.getDefault().getPreferenceStore()</code> 
 * to retrieve the preference store containing the configured
 * Configuration service endpoint. Call 
 * {@link org.eclipse.jface.preference.IPreferenceStore#getString(String)}
 * with the preference name {@link info.textgrid.lab.conf.client.ConfClient#PREF_ENDPOINT}
 * on this store to retrieve the value. If you need to react to changes in this
 * value, you may 
 * {@linkplain org.eclipse.jface.preference.IPreferenceStore#addPropertyChangeListener(org.eclipse.jface.util.IPropertyChangeListener) 
 * register a property change listener} with the preference store.
 * </p>
 * 
 * <p>
 * {@link ConfservPrefPage} contains the page in the preferences dialogue
 * to edit the value above. Add further 
 * {@linkplain org.eclipse.jface.preference.FieldEditor fields} if you 
 * extend the conf plugin's behaviour. Completions are currently hard coded here.
 * </p>
 * 
 *  
 */
package info.textgrid.lab.conf;