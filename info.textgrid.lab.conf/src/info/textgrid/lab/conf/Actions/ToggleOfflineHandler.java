package info.textgrid.lab.conf.Actions;

import info.textgrid.lab.conf.OnlineStatus;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

public class ToggleOfflineHandler extends AbstractHandler implements IHandler,
		IElementUpdater {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		OnlineStatus.setOnline(!OnlineStatus.isOnline());
		return !OnlineStatus.isOnline();
	}
	
	@SuppressWarnings("unchecked")
	public void updateElement (UIElement element, Map parameters) {
		element.setChecked(!OnlineStatus.isOnline());
	}

}
