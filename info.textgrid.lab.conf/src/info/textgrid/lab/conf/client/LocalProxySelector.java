package info.textgrid.lab.conf.client;

import info.textgrid.lab.conf.ConfPlugin;

import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.IOException;

/**
 * Manages the use of proxy servers in TextGrid lab. 
 * Currently only the use of an HTTP proxy server is
 * regarded. The server information come from the
 * preference settings. 
 * 
 * @author Frank Queens for TextGrid
 *
 */

public class LocalProxySelector extends ProxySelector {
	ProxySelector defsel = null;
        
    /*
     * Inner class representing a Proxy and a few extra data
     */
    class InnerProxy {
    Proxy proxy;
            SocketAddress addr;
            // How many times did we fail to reach this proxy?
            int failedCount = 0;
            
            InnerProxy(InetSocketAddress a) {
                    addr = a;
                    proxy = new Proxy(Proxy.Type.HTTP, a);
            }
            
            SocketAddress address() {
                    return addr;
            }
            
            Proxy toProxy() {
                    return proxy;
            }
            
            int failed() {
                    return ++failedCount;
            }
    }
        
    HashMap<SocketAddress, InnerProxy> proxies = new HashMap<SocketAddress, InnerProxy>();

    LocalProxySelector(ProxySelector def) {
    	defsel = def;
	  
		// Populate the HashMap (List of proxies), at the moment only one HTTP proxy is set
		String host = ConfPlugin.getDefault().getPreferenceStore().getString(ConfPlugin.PROXY_CONNECTION_HTTP);
		int port = ConfPlugin.getDefault().getPreferenceStore().getInt(ConfPlugin.PROXY_CONNECTION_PORT);
		  
		if (!host.equals("") && (port != 0)) { //$NON-NLS-1$
		      InnerProxy i = new InnerProxy(new InetSocketAddress(host, port));
		      proxies.put(i.address(), i);
		}    
    }
          
    public java.util.List<Proxy> select(URI uri) {
        // Let's stick to the specs. 
        if (uri == null) {
        	throw new IllegalArgumentException("URI can't be null."); //$NON-NLS-1$
        }
        
        /*
         * If it's a http (or https) URL, then we use our own
         * list.
         */
        String protocol = uri.getScheme();
        if ("http".equalsIgnoreCase(protocol)  || "https".equalsIgnoreCase(protocol)) { //$NON-NLS-1$ //$NON-NLS-2$
                ArrayList<Proxy> l = new ArrayList<Proxy>();
                for (InnerProxy p : proxies.values()) {
                  l.add(p.toProxy());
                }
                return l;
        } else {
                ArrayList<Proxy> l = new ArrayList<Proxy>();
                l.add(Proxy.NO_PROXY);
                return l;
        }
    }
        
    public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        if (uri == null || sa == null || ioe == null) {
        	throw new IllegalArgumentException("Arguments can't be null."); //$NON-NLS-1$
        }
        
//        InnerProxy p = proxies.get(sa); 
//        if (p != null) {
//            /*
//             * It's one of ours, if it failed more than 3 times
//             * let's remove it from the list.
//             */
//            if (p.failed() >= 3)
//                    proxies.remove(sa);
//        } else {
//            /*
//             * Not one of ours, let's delegate to the default.
//             */
//            if (defsel != null)
//              defsel.connectFailed(uri, sa, ioe);
//        }
     }
}
