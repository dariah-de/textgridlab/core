package info.textgrid.lab.conf.client;

import static org.eclipse.core.runtime.IProgressMonitor.UNKNOWN;
import info.textgrid.lab.conf.ConfPlugin;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.middleware.confclient.ConfservClient;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.ProxySelector;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.xml.stream.XMLStreamException;

import org.codehaus.jettison.json.JSONException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;

/**
 * Client for the configuration service. In order to
 */
public class ConfClient {

	public static String EXPECTED_API_VERSION = "2011-02-15"; //$NON-NLS-1$

	/** The preference string for the configuration endpoint */
	public static final String PREF_ENDPOINT = "endpoint"; //$NON-NLS-1$
	
	/** The default value for the endpoint preference. Users can override this (also persistently) via the preferences dialog. */
	public static final String DEFAULT_ENDPOINT = "https://textgridlab.org/1.0/confserv"; //$NON-NLS-1$

	/**
	 * The default value for the endpoint preference that is actually used.
	 * 
	 * Normally, this is equal to DEFAULT_ENDPOINT, but for testing purposes and
	 * experimental versions this can be changed. There will be a warning
	 * message in the welcome screen.
	 */
	public static final String REAL_DEFAULT_ENDPOINT = DEFAULT_ENDPOINT;
	// public static final String REAL_DEFAULT_ENDPOINT =
	// "http://textgrid-ws3.sub.uni-goettingen.de/axis2/services/confserv";
	// "http://134.76.20.85/axis2/services/confserv";

	private static ConfClient confClient;
	
	/**
	 * Environment variable to override the endpoint (e.g, for automatted
	 * tests). If this environment variable is set, its value will be used as an
	 * endpoint and the value of the preference as well as the
	 * {@link #DEFAULT_ENDPOINT} will be ignored.
	 */
	protected static final String ENV_ENDPOINT_VAR = "TEXTGRIDLAB_CONFSERV"; //$NON-NLS-1$
	
	private String endpoint = ""; //$NON-NLS-1$

	private HashMap<String, String> confVars;
	private ConfservClient confservClient;
	private static ListenerList apiChangedListeners = new ListenerList();

	private ConfClient() throws OfflineException {
		IPreferenceStore preferenceStore = ConfPlugin.getDefault().getPreferenceStore();
		preferenceStore.setDefault(PREF_ENDPOINT, REAL_DEFAULT_ENDPOINT);
		setProxySelector();
		setEndpoint(computePreferredEndpoint());
	}

	/**
	 * Returns the preferred EPR for the configuration service. Tries
	 * <ol>
	 * <li>the environment variable {@value #ENV_ENDPOINT_VAR}</li>
	 * <li>the preference setting {@value #PREF_ENDPOINT} in
	 * {@value #PREF_QUALIFIER}</li>
	 * <li>A default value.</li>
	 * </ol>
	 * until it finds something not "" or null.
	 * 
	 * This does not invalidate something or need network access.
	 */
	public static String computePreferredEndpoint() {
		String endpoint;
		String env_endpoint = java.lang.System.getenv().get(ENV_ENDPOINT_VAR);
		if (env_endpoint != null) {
			endpoint = env_endpoint;
		} else {

			endpoint = ConfPlugin.getDefault().getPreferenceStore().getString(PREF_ENDPOINT);
			if (endpoint == null || "".equals(endpoint)) //$NON-NLS-1$
				endpoint = REAL_DEFAULT_ENDPOINT;
		}
		return endpoint;
	}

	/**
	 * Sets a new config client endpoint, creates a new client and invalidates
	 * the cache.
	 * 
	 * @param endpoint
	 * @throws OfflineException
	 */
	private void setEndpoint(String endpoint) throws OfflineException {
		if (this.endpoint == null || !this.endpoint.equals(endpoint)) {
			this.endpoint = endpoint;
			this.confservClient = new ConfservClient(endpoint);
			this.refresh();
		}
	}

	/**
	 * The EndPoint currently in use. Might be <code>""</code> if not yet
	 * determined.
	 */
	public String getEndpoint() {
		return endpoint;
	}

	public static ConfClient getInstance() throws OfflineException {
		if (confClient == null) {
			confClient = new ConfClient();
		}
		return confClient;
	}

	/**
	 * Returns the value for the given configuration variable
	 * 
	 * @param key
	 *            the variable name, see
	 *            {@link info.textgrid.middleware.confclient.ConfservClientConstants}
	 * @return the value
	 */

	public String getValue(String key) throws OfflineException {
		if (confVars == null) {
			if (OnlineStatus.isOnline()) {
				refresh();
			} else {
				throw new OfflineException(Messages.ConfClient_NoConfigWhileOffline, null);
			}
		}

		return confVars.get(key);

	}

	/**
	 * Returns the value for the given configuration variable
	 * 
	 * @param key
	 *            the variable name, see
	 *            {@link info.textgrid.middleware.confclient.ConfservClientConstants}
	 * @param force
	 *            if true, the value is retrieved from the server, otherwise we
	 *            may use a cached value
	 * @return the value
	 * @throws OfflineException
	 * @deprecated
	 */
	@Deprecated
	public String getValue(String key, Boolean force) throws OfflineException {
		return getValue(key);
		/*
		 * if (!force && confVars.containsKey(key)) { //
		 * ConfPlugin.log(IStatus.INFO, null, "{0}={1}", key, confVars //
		 * .get(key)); return confVars.get(key); } else { String value = ""; try
		 * { value = confservClient.getValue(key); } catch (Exception e) {
		 * e.printStackTrace(); } confVars.put(key, value); //
		 * ConfPlugin.log(IStatus.INFO, null, "{0}={1}", key, confVars //
		 * .get(key)); return value; }
		 */
	}

	public void refresh() throws OfflineException {

		IRunnableWithProgress fetchJob = new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					if (monitor == null)
						monitor = new NullProgressMonitor();
					monitor.beginTask(Messages.ConfClient_FetchingConfig, UNKNOWN);
					ConfClient.this.confVars = confservClient.getAll();
					monitor.done();
				} catch (IOException e) {
					throw new InvocationTargetException(new OfflineException(Messages.ConfClient_FailedToGetValues, e));
				} catch (JSONException e) {
					/*
					 * ConfPlugin.log(IStatus.ERROR, e,
					 * "Could not parse Confserv-Response");
					 */
					throw new InvocationTargetException(new OfflineException(Messages.ConfClient_PsrseError, e));
				} catch (XMLStreamException e) {
					/*
					 * ConfPlugin.log(IStatus.ERROR, e,
					 * "Could not parse Confserv-Response");
					 */
					throw new InvocationTargetException(new OfflineException(Messages.ConfClient_ParseError, e));
				}
				ConfPlugin.log(IStatus.INFO, null, "TextGrid Configuration: {0}", confVars); //$NON-NLS-1$
			}

		};

		try {
			if (Display.getCurrent() == null)
				fetchJob.run(null); // ProgressService may only be called from
			// an UI thread
			else
				PlatformUI.getWorkbench().getProgressService().run(false, false, fetchJob);

			checkApiChanged();

		} catch (InvocationTargetException e) {
			if (e.getCause() != null && e.getCause() instanceof OfflineException)
				throw (OfflineException) e.getCause();
		} catch (InterruptedException e) {
			ConfPlugin.log(IStatus.CANCEL, e, Messages.ConfClient_FetchJobInterrupted);
		}

	}

	/**
	 * Check if EXPECTED_API_VERSION is older than "last-api-change" from
	 * confserv if this is the case, notify ApiChangedListeners.
	 */
	private void checkApiChanged() {

		if (confVars.get("last-api-change") == null) //$NON-NLS-1$
			return;

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
		Date confServApiDate = new Date();
		Date labApiDate = new Date();
		try {
			confServApiDate = formatter.parse(confVars.get("last-api-change")); //$NON-NLS-1$
		} catch (ParseException e) {
			ConfPlugin.log(IStatus.ERROR, e, Messages.ConfClient_ErrorParsingApiChange
					+ confVars.get("last-api-change")); //$NON-NLS-1$
		}
		try {
			labApiDate = formatter.parse(EXPECTED_API_VERSION);
		} catch (ParseException e) {
			ConfPlugin.log(IStatus.ERROR, e, Messages.ConfClient_ErrorParsingApiVersionX + EXPECTED_API_VERSION);
		}

		if (labApiDate.compareTo(confServApiDate) < 0) {
			notifyApiChangedListeners(new Event());
		}
	}

	public static void addApiChangedListener(Listener l) {
		apiChangedListeners.add(l);
	}

	public static void removeApiChangedListener(Listener l) {
		// TODO check if that really works...
		apiChangedListeners.remove(l);
	}

	protected synchronized void notifyApiChangedListeners(Event event) {
		Object[] listeners = apiChangedListeners.getListeners();
		for (int i = 0; i < listeners.length; ++i) {
			((Listener) listeners[i]).handleEvent(event);
		}
	}
	
	public static void setProxySelector() {
		if (info.textgrid.lab.conf.ConfPlugin.getDefault().getPreferenceStore().getBoolean(ConfPlugin.PROXY_CONNECTION)) {
			LocalProxySelector ps = new LocalProxySelector(ProxySelector.getDefault());
		    ProxySelector.setDefault(ps);
		} else {
			ProxySelector.setDefault(null);
		}
	}

}
