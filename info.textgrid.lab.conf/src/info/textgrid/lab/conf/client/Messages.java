package info.textgrid.lab.conf.client;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.conf.client.messages"; //$NON-NLS-1$
	public static String ConfClient_ErrorParsingApiChange;
	public static String ConfClient_ErrorParsingApiVersionX;
	public static String ConfClient_FailedToGetValues;
	public static String ConfClient_FetchingConfig;
	public static String ConfClient_FetchJobInterrupted;
	public static String ConfClient_NoConfigWhileOffline;
	public static String ConfClient_ParseError;
	public static String ConfClient_PsrseError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
