package info.textgrid.lab.conf;

import info.textgrid.lab.conf.client.ConfClient;

import java.net.URI;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;


public class ConfservPrefPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	StringFieldEditor endpointEditor;
	BooleanFieldEditor proxyRadio;
	StringFieldEditor proxyString;
	IntegerFieldEditor portString;
	Composite compositeProxy;
	boolean shouldRestart = false;
	
	public ConfservPrefPage() {
		// TODO Auto-generated constructor stub
	}

	public ConfservPrefPage(int style) {
		super(style);
		// TODO Auto-generated constructor stub
	}

	public ConfservPrefPage(String title, int style) {
		super(title, style);
		// TODO Auto-generated constructor stub
	}

	public ConfservPrefPage(String title, ImageDescriptor image, int style) {
		super(title, image, style);
		// TODO Auto-generated constructor stub
	}

	private final class ControllableContentProposalAdapter extends ContentProposalAdapter {
		private ControllableContentProposalAdapter(Control control, IControlContentAdapter controlContentAdapter,
				IContentProposalProvider proposalProvider, KeyStroke keyStroke, char[] autoActivationCharacters) {
			super(control, controlContentAdapter, proposalProvider, keyStroke, autoActivationCharacters);
		}

		public void openProposalPopup() {
			super.openProposalPopup();
		}
	}


	private static class EndpointProposal implements IContentProposal {

		public EndpointProposal(final URI uri, final String description) {
			this.uri = uri;
			this.description = description;
		}

		private final URI uri;
		private final String description;

		public String getContent() {
			return uri.toString();
		}

		public int getCursorPosition() {
			return uri.getScheme().length() + uri.getHost().length() + 3;
		}

		public String getDescription() {
			return description;
		}

		public String getLabel() {
			return description + " (" + uri.toString() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
		}

	}

	private static final IContentProposal[] ENDPOINTS = {
			new EndpointProposal(URI.create(ConfClient.DEFAULT_ENDPOINT), Messages.ConfservPrefPage_DefaultEndpoint),
			new EndpointProposal(URI.create("https://dev.textgridlab.org/1.0/confserv"), Messages.ConfservPrefPage_DevSystem), //$NON-NLS-1$
	// ConfClient.DEFAULT_ENDPOINT,
	};

	@Override
	protected void createFieldEditors() {
		createEndpointFieldEditor();
		createCompressedTransferFieldEditor();
		createProxyFieldEditor();
	}

	private void createCompressedTransferFieldEditor() {
		// compressed data transfer
		Composite transferComposite = getFieldEditorParent();
		GridLayout transferGridlayout = new GridLayout();
		transferGridlayout.numColumns = 3;
		transferComposite.setLayout(transferGridlayout);
		BooleanFieldEditor comTrans = new BooleanFieldEditor("compressedTransfer", Messages.ConfservPrefPage_UseCompressedTransfer, transferComposite); //$NON-NLS-1$
		addField(comTrans);
	}

	private void createEndpointFieldEditor() {
		Composite endpointFieldParent = getFieldEditorParent();
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		endpointFieldParent.setLayout(gridLayout);
		endpointEditor = new StringFieldEditor("endpoint", Messages.ConfservPrefPage_Endpoint, endpointFieldParent); //$NON-NLS-1$
		endpointEditor.setEmptyStringAllowed(false);
		addField(endpointEditor);

		IContentProposalProvider proposalProvider = new IContentProposalProvider() {

			public IContentProposal[] getProposals(String contents, int position) {
				return ENDPOINTS;
			}
		};
		Text textControl = endpointEditor.getTextControl(endpointFieldParent);
		final ControllableContentProposalAdapter proposals = new ControllableContentProposalAdapter(textControl, new TextContentAdapter(), proposalProvider, null, null);
		proposals.setFilterStyle(ContentProposalAdapter.FILTER_CHARACTER);
		proposals.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);

		Label restartInfo = new Label(endpointFieldParent, SWT.WRAP);
		GridData layoutData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		layoutData.horizontalSpan = 2;
		restartInfo.setLayoutData(layoutData);
		restartInfo.setText(Messages.ConfservPrefPage_RestartRequired);

		Label modifiedInfo = new Label(endpointFieldParent, SWT.WRAP);
		GridData layoutData2 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		layoutData2.horizontalSpan = 2;
		layoutData2.widthHint = 400;
		modifiedInfo.setLayoutData(layoutData2);
		String usedEndpoint = ConfClient.computePreferredEndpoint(); // #11970 don't instantiate it
		String configuredEndpoint = getPreferenceStore().getString("endpoint"); //$NON-NLS-1$
		if (!usedEndpoint.equals(configuredEndpoint)) {
			modifiedInfo
					.setText(NLS
							.bind(
									Messages.ConfservPrefPage_RestartExplanation,
									usedEndpoint, configuredEndpoint));
			ControlDecoration modifiedWarning = new ControlDecoration(modifiedInfo, SWT.RIGHT);
			modifiedWarning.setImage(JFaceResources.getImage(Dialog.DLG_IMG_MESSAGE_WARNING));
		}
	}
	
	private void createProxyFieldEditor() {
		Composite proxyComposite = getFieldEditorParent();
		proxyComposite.setLayout(new GridLayout());
		GridData proxyLayout = new GridData(SWT.FILL, SWT.CENTER, true, true);
		Group proxyGroup = new Group(proxyComposite, SWT.NULL);
		proxyGroup.setText(Messages.ConfservPrefPage_ProxySettings);
		proxyGroup.setLayoutData(proxyLayout);

		proxyRadio = new BooleanFieldEditor("proxy_connection", Messages.ConfservPrefPage_UseProxy, proxyGroup); //$NON-NLS-1$
		addField(proxyRadio);
		
		GridData proxyFieldLayout = new GridData(SWT.FILL, SWT.CENTER, true, false);
		proxyFieldLayout.horizontalIndent = 25;
		compositeProxy = new Composite(proxyGroup, SWT.FILL);
		compositeProxy.setLayoutData(proxyFieldLayout);
		
		proxyString = new StringFieldEditor("proxy_connection_http", Messages.ConfservPrefPage_Proxy,compositeProxy); //$NON-NLS-1$
		portString = new IntegerFieldEditor("proxy_connection_port", Messages.ConfservPrefPage_ProxyPort,compositeProxy); //$NON-NLS-1$
		addField(proxyString);
		addField(portString);
		
		if (!getPreferenceStore().getBoolean("proxy_connection")) { //$NON-NLS-1$
			proxyString.setEnabled(false, compositeProxy);
    		portString.setEnabled(false, compositeProxy);
		}
	}
	
	public void propertyChange(PropertyChangeEvent event) {
        super.propertyChange(event);
		if (event.getProperty().equals(FieldEditor.VALUE) && proxyRadio != null && proxyString != null) {
        	if (proxyRadio.getBooleanValue() == false) {
        		proxyString.setEnabled(false, compositeProxy);
        		portString.setEnabled(false, compositeProxy);
        	} else {
        		proxyString.setEnabled(true, compositeProxy);
        		portString.setEnabled(true, compositeProxy);
        	}        	
        }        
    }

	public void init(IWorkbench workbench) {
		setDescription(Messages.ConfservPrefPage_PageDescription);
		setPreferenceStore(ConfPlugin.getDefault().getPreferenceStore());
		initialize();
	}
	
	protected void performApply() {
		shouldRestart = restartCheck();
		super.performApply();
		if (shouldRestart) {
			restartDialog();
		}	
	}
	
	public boolean performOk() {
		if (!shouldRestart)
			shouldRestart = restartCheck();
		boolean ok = super.performOk();
        if (ok && shouldRestart) {
        	restartDialog();
        }
        ConfClient.setProxySelector();
		return ok;
	}
	
	private boolean restartCheck() {
		if (!endpointEditor.getStringValue().equals(endpointEditor.getPreferenceStore().getString(ConfClient.PREF_ENDPOINT))) { 	
				return true;
			}
		return false;
	}
	
	private void restartDialog() {
		// Changing the endpoint with a running lab currently doesn't work (cf.
		// TG-351, TG-201). Thus, we require a restart instead of performing the
		// change. OTOH, a restart with an open editor is evil, so there.
		if (MessageDialog
				.openConfirm(
						null,
						Messages.ConfservPrefPage_LabRestartRequired,
						Messages.ConfservPrefPage_RestartQuestion)) {
			PlatformUI.getWorkbench().restart();
		}
	}
}
