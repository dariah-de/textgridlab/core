package info.textgrid.lab.conf;

import info.textgrid.lab.conf.client.ConfClient;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ConfPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.conf"; //$NON-NLS-1$

	// The shared instance
	private static ConfPlugin plugin;

	// The identifiers for the preferences
	public static final String COMPRESSED_TRANSFER = "compressedTransfer"; //$NON-NLS-1$
	public static final String CRUD_WARNING_PREFERENCE = "crudwarning"; //$NON-NLS-1$
	public static final String PROXY_CONNECTION = "proxy_connection"; //$NON-NLS-1$
	public static final String PROXY_CONNECTION_HTTP = "proxy_connection_http"; //$NON-NLS-1$
	public static final String PROXY_CONNECTION_PORT = "proxy_connection_port"; //$NON-NLS-1$
	public static final String MOVE_WARNING_PREFERENCE = "movewarning"; //$NON-NLS-1$

	/**
	 * The constructor
	 */
	public ConfPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		getPreferenceStore().setDefault(COMPRESSED_TRANSFER, true);
		getPreferenceStore().setDefault(ConfClient.PREF_ENDPOINT,
				ConfClient.REAL_DEFAULT_ENDPOINT);
		getPreferenceStore().setDefault(CRUD_WARNING_PREFERENCE, true);
		getPreferenceStore().setDefault(PROXY_CONNECTION, false);
		getPreferenceStore().setDefault(PROXY_CONNECTION_HTTP, ""); //$NON-NLS-1$
		getPreferenceStore().setDefault(PROXY_CONNECTION_PORT, 0);
		getPreferenceStore().setDefault(MOVE_WARNING_PREFERENCE, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static ConfPlugin getDefault() {
		return plugin;
	}

	/**
	 * Logs the problem to the system log.
	 * 
	 * @param cause
	 *            the exception causing the problem or <code><b>null</b></code>,
	 *            if not applicable
	 * @param message
	 *            an error message, optionally containing placeholders in the
	 *            {@link MessageFormat} syntax ({0}, ...}
	 * @param args
	 *            arguments to fill in the placeholders in the
	 *            <var>message</var>, see {@link MessageFormat}
	 */
	public void handleError(Throwable cause, String message, Object... args) {
		getLog().log(
				new Status(IStatus.ERROR, PLUGIN_ID, NLS.bind(message, args),
						cause));
	}

	public static IStatus log(int severity, Throwable cause, String message,
			Object... arguments) {
		Status status = new Status(severity, PLUGIN_ID, NLS.bind(message,
				arguments), cause);
		getDefault().getLog().log(status);
		return status;
	}
}
