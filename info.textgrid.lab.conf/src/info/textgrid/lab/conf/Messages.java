package info.textgrid.lab.conf;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.conf.messages"; //$NON-NLS-1$
	public static String ConfservPrefPage_DefaultEndpoint;
	public static String ConfservPrefPage_DevSystem;
	public static String ConfservPrefPage_Endpoint;
	public static String ConfservPrefPage_LabRestartRequired;
	public static String ConfservPrefPage_PageDescription;
	public static String ConfservPrefPage_Proxy;
	public static String ConfservPrefPage_ProxyPort;
	public static String ConfservPrefPage_ProxySettings;
	public static String ConfservPrefPage_RestartExplanation;
	public static String ConfservPrefPage_RestartQuestion;
	public static String ConfservPrefPage_RestartRequired;
	public static String ConfservPrefPage_UseCompressedTransfer;
	public static String ConfservPrefPage_UseProxy;
	public static String DialogPrefPage_ShowCrudWarnings;
	public static String DialogPrefPage_ShowMoveProcessWarnings;
	public static String OnlineStatus_AccessFailed;
	public static String OnlineStatus_NetworkAccessFailedReason;
	public static String OnlineStatus_NetworkAccessPossible;
	public static String OnlineStatus_OfflineNotification;
	public static String OnlineStatus_x_SystemIsOffline;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
