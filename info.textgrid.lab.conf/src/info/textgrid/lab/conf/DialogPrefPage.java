package info.textgrid.lab.conf;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class DialogPrefPage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public DialogPrefPage() {
		// TODO Auto-generated constructor stub
	}

	public DialogPrefPage(int style) {
		super(style);
		// TODO Auto-generated constructor stub
	}

	public DialogPrefPage(String title, int style) {
		super(title, style);
		// TODO Auto-generated constructor stub
	}

	public DialogPrefPage(String title, ImageDescriptor image, int style) {
		super(title, image, style);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createFieldEditors() {
		Composite dialogFieldParent = getFieldEditorParent();
		BooleanFieldEditor CrudWarning = new BooleanFieldEditor("crudwarning", //$NON-NLS-1$
				Messages.DialogPrefPage_ShowCrudWarnings, dialogFieldParent);
		addField(CrudWarning);
		BooleanFieldEditor CopyWarning = new BooleanFieldEditor("movewarning", //$NON-NLS-1$
				Messages.DialogPrefPage_ShowMoveProcessWarnings,
				dialogFieldParent);
		addField(CopyWarning);
	}

	public void init(IWorkbench workbench) {
		setPreferenceStore(ConfPlugin.getDefault().getPreferenceStore());
		initialize();
	}
}
