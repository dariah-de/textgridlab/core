package info.textgrid.lab.search;

import org.eclipse.core.runtime.PlatformObject;

/**
 * The class NoMatchEntry represents a search entry for 
 * a search with no matches
 * 
 * @author Frank Queens
 * 
 */
public class NoMatchEntry extends PlatformObject {
	
	private String caption = "no matches found";
	
	public String toString() {
		return caption;
	}

}
