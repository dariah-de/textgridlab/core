package info.textgrid.lab.search;

import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;

import java.text.MessageFormat;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.progress.DeferredTreeContentManager;

/**
 * Generic {@link ITreeContentProvider} suitable for use with deferred content.
 * 
 * This content provider must be
 * {@linkplain AbstractTreeViewer#setContentProvider(org.eclipse.jface.viewers.IContentProvider)
 * associated} with an {@link AbstractTreeViewer}. It will instantiate its own
 * {@link DeferredTreeContentManager} to deferredly fetch its content. The
 * viewer's {@linkplain AbstractTreeViewer#setInput(Object) input} should be a
 * parent node to the root level elements of the tree.
 * 
 * TODO refactor to some generic plugin
 * 
 * @author roman, -tv
 * @see DeferredTreeContentManager
 * 
 */
public class DeferredTreeContentProvider implements ITreeContentProvider,
		IChildListChangedListener {

	private DeferredTreeContentManager manager;
	private Viewer viewer;
	private IChildListParent input;

	private void assertValidManager() throws IllegalStateException {
		if (manager == null)
			throw new IllegalStateException(
					"This DeferredTreeContentManager does not have a valid manager. Please associate it with a valid AbstractTreeViewer first and call setInput() on the TreeViewer before calling any methods on the content provider.");
	}

	public Object[] getChildren(Object parentElement) {

		return manager == null ? new Object[0] : manager
				.getChildren(parentElement);

	}
	
	public DeferredTreeContentManager getManager() {
		return manager;
	}

	public Object getParent(Object element) throws IllegalStateException {
		assertValidManager();
		return manager.getChildren(element);
	}

	public boolean hasChildren(Object element) throws IllegalStateException {
		assertValidManager();
		return manager.mayHaveChildren(element);

	}

	public Object[] getElements(Object inputElement) {
		return this.getChildren(inputElement);
	}

	public void dispose() {
		if (input != null)
			input.removeChildListChangedListener(this);
	}

	public void cancel(Object input) {
		if (manager != null) {
			manager.cancel(input);
		}
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (this.input != newInput && newInput instanceof IChildListParent) {
			if (this.input != null) {
				this.input.removeChildListChangedListener(this);
			}
			this.input = (IChildListParent) newInput;
			this.input.addChildListChangedListener(this);
		}
		// System.out.println("input changed. instance " + this.toString()
		// + " manager == null : " + (manager == null));

		if (viewer instanceof AbstractTreeViewer) {
			if (viewer != this.viewer) {
				this.viewer = viewer;
				manager = new DeferredTreeContentManager((AbstractTreeViewer) viewer);
			}
		} else {
			throw new IllegalArgumentException(MessageFormat.format(
					"DeferredTreeContentManager must be associated with an AbstractTreeViewer, not an {0} like {1}", viewer
							.getClass(), viewer));
		}
	}

	public void childListChanged(IChildListParent parent) {
		if (viewer != null) {
			viewer.setInput(parent);
		}
	}
}
