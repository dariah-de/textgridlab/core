package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdapterFactory;

public class SSRAdaptorFactory implements IAdapterFactory {

	private static final Class[] ADAPTERLIST = new Class[]{TextGridObject.class,IFile.class};

	public Object getAdapter(Object adaptableObject, Class adapterType) {
		// TODO Auto-generated method stub
		if (adaptableObject instanceof SingleSearchResult && 
				TextGridObject.class.isAssignableFrom(adapterType)) {
			return ((SingleSearchResult ) adaptableObject).getTGO();
		}
		if (adaptableObject instanceof SingleSearchResult && 
				IFile.class.isAssignableFrom(adapterType)) {
			return ((SingleSearchResult) adaptableObject).getTGO().getAdapter(IFile.class);
		}
		
		return null;
	}

	public Class[] getAdapterList() {
		// TODO Auto-generated method stub
		return ADAPTERLIST;
	}

}
