package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TGObjectReference;

import org.eclipse.core.runtime.PlatformObject;

public class FullTextEntry extends PlatformObject {
	
	private TGObjectReference tgoRef;
	private String left;
	private String match;
	private String right;
	private String xpath;
	
	FullTextEntry(TGObjectReference tgoRef, String left, String match, String right, String xpath) {
		this.tgoRef = tgoRef;
		this.left = left.replace("\n", "");
		this.match = match.replace("\n", "");
		this.right = right.replace("\n", "");
		this.xpath = xpath;
	}
	
	public void setTgoRef(TGObjectReference tgoRef) {
		this.tgoRef = tgoRef;
	}
	
	public TGObjectReference getTgoRef() {
		return tgoRef;
	}

	public String getLeft() {
		return left;
	}

	public void setLeft(String left) {
		this.left = left.replace("\n", "");
	}

	public String getMatch() {
		return match;
	}

	public void setMatch(String match) {
		this.match = match.replace("\n", "");
	}

	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right.replace("\n", "");
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}
	
	

}
