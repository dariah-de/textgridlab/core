package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TextGridProject;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;

public class ProjectListAdapterFactory implements IAdapterFactory {

	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if(adapterType == IDeferredWorkbenchAdapter.class && adaptableObject instanceof TextGridProject){
			return new ProjectListAdapter((TextGridProject) adaptableObject);
		}
		
		return null;
	}

	public Class[] getAdapterList() {
		return new Class[]{IDeferredWorkbenchAdapter.class};
	}

}
