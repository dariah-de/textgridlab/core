package info.textgrid.lab.search;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.osgi.util.NLS;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;


/**
 * A single request to TG-search.
 * 
 * You should derive from this abstract class and then initialize the request 
 * with a field and a query string and then {@link #Request.schedule()} it.
 *  At any time, you can retrieve the current results filtered for a given 
 *  substring with {@link #getResultsFor(String)}. You should {@link #cancel()}
 *  a job that you need no longer.
 * 
 * @author Thorsten Vitt, Yahya Al-Hajj
 * 
 */
public abstract class AbstractRequest extends Job {

	
	protected final String query;
	protected final String field;

	private final List<String> currentResults = Collections.synchronizedList(new LinkedList<String>());
	
	
	/**
	 * Create a new request. You need to {@link #schedule()} it to let it
	 * run.
	 * 
	 * @param field
	 *            the field to query
	 * @param query
	 *            the substring to search for
	 */
	public AbstractRequest(final String field, final String query) {
		super(NLS.bind("Searching completions for {0} in {1} ...", query, field));
		this.field = field;
		this.query = query;

		Assert.isNotNull(field);
		Assert.isNotNull(query);
		Assert.isLegal(!"".equals(field), "Cannot search in an empty search field");
		Assert.isLegal(!"".equals(query), "Cannot search completions for the empty string");
	}

	/**
	 * returns <code>true</code> if this job is applicable for the given
	 * <var>query</var>, i.e. if {@linkplain #getResultsFor(String)
	 * <code>getResultsFor(query)</code>} will return meaningful results.
	 */
	public boolean isApplicableFor(final String query) {
		return query.contains(this.query);
	}

	/**
	 * returns the currently available results of this job filtered for the
	 * new query given as an argument.
	 * 
	 * @see #isApplicableFor(String)
	 */
	public Iterable<String> getResultsFor(final String query) {
		Iterable<String> result = Iterables.filter(currentResults, new Predicate<String>() {

			public boolean apply(final String result) {
				if(query != null)
					return result.toLowerCase().contains(query.toLowerCase());
				
				return result.contains(query);
			}

		});
		return result;
	}
	
	protected void addProposal(String proposal) {
		currentResults.add(proposal);
	}
	
	
	protected void addAllProposals(Collection<String> proposals) {
		currentResults.addAll(proposals);
	}
	
	
	abstract protected IStatus run(IProgressMonitor monitor);

}
