package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TextGridObject;
import org.eclipse.core.runtime.IAdapterFactory;

public class FulltextEntryAdapterFactory implements IAdapterFactory {

	@SuppressWarnings("rawtypes")
	private static final Class[] ADAPTERLIST = new Class[] {TextGridObject.class};

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof FullTextEntry && 
				TextGridObject.class.isAssignableFrom(adapterType)) {
			return ((FullTextEntry) adaptableObject).getTgoRef().getTgo();
		} else if (adaptableObject instanceof FullTextEntry)
			return ((FullTextEntry) adaptableObject).getTgoRef().getTgo().getAdapter(adapterType);

		return null;
	}

	@SuppressWarnings("rawtypes")
	public Class[] getAdapterList() {
		return ADAPTERLIST;
	}

}

