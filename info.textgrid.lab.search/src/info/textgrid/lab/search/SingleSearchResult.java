package info.textgrid.lab.search;

import static info.textgrid.lab.search.Activator.debugLog;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.ChunkingElementCollector;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Joiner;

/**
 * Just a container to hold a TextGridObject TOGETHER with its match count
 * Children of it are: Project, Author, and all KWICEntries which will be
 * retrieved deferredly.
 * 
 * @author martin
 * 
 */
public class SingleSearchResult extends PlatformObject implements IDeferredWorkbenchAdapter {
	@Deprecated
	private static final String XPATH_AUTHORS = ".//tg:agent[@role='author']";

	private TextGridObject mytgo = null;
	private Integer mycount = null;
	// private KWICEntry[] mykwics = null;
	private ArrayList<Object> allChildren;
	// the amount of extra lines needed besides Kwic Entries
	// at the moment it is 2: project and authors
	private final int EXTRA_CHILDREN = 2;
	private String TEXTGRID_METADATA_NAMESPACE = TextGridObject.TEXTGRID_METADATA_NAMESPACE;
	private KWICEntry[] kwicEntryList = new KWICEntry[0];

	/**
	 * The Constructor. Hand over an OMElement as returned by the restclient
	 * representing one result document
	 * 
	 * 
	 * @param resultDocument
	 *            an {@link OMElement} as returned by the
	 *            {@link TGSearchRestClient} representing one result document.
	 * @param isTextQuery
	 *            if true, matches will be shown in front of the title
	 * @param wantKwic
	 *            if true, the result XPath will be evaluated for the KWIC
	 *            entries.
	 * @param isComplete
	 *            whether the objects' metadata in the <var>resultDocument</var>
	 *            represent a {@linkplain TextGridObject#isComplete() complete}
	 *            metadata record.
	 */
	public SingleSearchResult(OMElement resultDocument, boolean isTextQuery,
			boolean wantKwic, boolean isComplete) {
		OMElement tgObjectMD = resultDocument.getFirstChildWithName(new QName(
				TEXTGRID_METADATA_NAMESPACE, "tgObjectMetadata"));

		String matches = "";
		if (isTextQuery) {
			OMElement matchesOM = resultDocument
					.getFirstChildWithName(new QName(
					"http://textgrid.info/namespaces/metadata", "matches"));
			matches = matchesOM.getText();
			if (wantKwic) {
				OMElement kwicEntries = resultDocument
						.getFirstChildWithName(new QName(
						"http://textgrid.info/namespaces/metadata", "result"));
				kwicEntryList = parse_kwicResult(kwicEntries);
			}
		}

		// Default: hand the full Metadata to build the TGO...
		// if (wantFullMetadata) {
		mytgo = null;
		try {
			mytgo = TextGridObject.getInstance(tgObjectMD, isComplete);
			debugLog("instantiated TGO "+mytgo.getTitle());
		} catch (CoreException ce) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Eclipse Core Exception!", ce);
			Activator.getDefault().getLog().log(status);
		}
		if (mytgo == null) {
			// ??? return false;
		} else {
			if (isTextQuery && !matches.equals("")) {
				mycount = new Integer(matches);
			} else {
				mycount = new Integer(0);
			}
		}
	}

	/**
	 * @deprecated use the other constructor
	 * @param tgo
	 * @param count
	 * @param kwicEntries
	 */
	public SingleSearchResult(TextGridObject tgo, Integer count,
			KWICEntry[] kwicEntries) {
		mytgo = tgo;
		mycount = count;
		// mykwics = kwicEntries;
		if (mytgo.isComplete()) {	
			// System.out.println("I'm Complete!");
		} else {
			// System.out.println("I'm not Complete! Trying to reload
			// Metadata...");
			try {
				mytgo.reloadMetadata(false);
			} catch (CrudServiceException cse) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"Could not reload Metadata from CRUD!", cse);
				Activator.getDefault().getLog().log(status);
			}
		}
		allChildren = new ArrayList<Object>(kwicEntries.length + EXTRA_CHILDREN);
		allChildren.add(new String("Project: " + getProjectName()));
		allChildren.add(new String("Authors: " + getAuthors()));
		// only for debug: allChildren.add(new String("URI: "+getURI()));
		for (KWICEntry e : kwicEntries) {
			e.setParent(this);
			allChildren.add(e);
		}
	}

	public TextGridObject getTGO() {
		return mytgo;
	}

	/**
	 * @return how many matches of the search term were in the document
	 * 
	 */
	public Integer getCount() {
		return mycount;
	}

	// public KWICEntry[] getKwics () {
	// return mykwics;
	// }
	public Object[] getChildren() {
		return allChildren.toArray(new Object[0]);
	}

	public String getURI() {
		return getTGO().getURI().toString();
	}

	public String getTGMD() {
		try {
			String result = getTGO().serialize();
			return result;
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Exception serializing TextGridObject", e);
			Activator.getDefault().getLog().log(status);
		}
		return "not resolved";
	}

	public String getProjectName() {
		try {
			return getTGO().getProjectInstance().getName();
		} catch (CoreException e1) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Exception getting Project Name for Object", e1);
			Activator.getDefault().getLog().log(status);
			return "<unknown project>";
		}
	}

	public String getAuthors() {
		try {
			List<String> authors = getTGO().getAuthors();
			if (authors.isEmpty())
				return "(not specified)";
			else
				return Joiner.on("; ").join(authors);
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
		return "";
	}

	/**
	 * @return the document's title, optionally prepended by the number of
	 *         matches of the search term
	 */
	@Override
	public String toString() {
		String result = "";
		if (!getCount().equals(new Integer(0))) {
			result = getCount().toString() + " ";
		}
		// try {
		// result += getTGO().getProject();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		try {
			result += getTGO().getTitle();
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Exception getting title for Object", e);
			Activator.getDefault().getLog().log(status);
		}
		return result;
	}

	public static KWICEntry[] parse_kwicResult(OMElement result) {
		ArrayList<KWICEntry> kes = new ArrayList<KWICEntry>();
		Iterator<?> iter = result.getChildElements();
		while (iter.hasNext()) {
			OMElement nextEntry = (OMElement) iter.next();
			KWICEntry ke = new KWICEntry(nextEntry,
					"http://textgrid.info/namespaces/metadata");
			kes.add(ke);
		}
		return kes.toArray(new KWICEntry[0]);
	}

	/**
	 * contains the logic of how to populate, on demand, the children list
	 */
	public void fetchDeferredChildren(Object object,
			IElementCollector collector, IProgressMonitor monitor) {

		monitor.beginTask("Retrieving children of " + toString(),
				IProgressMonitor.UNKNOWN);
		synchronized (this) {
			if (object instanceof SingleSearchResult) {

				final ChunkingElementCollector groupingCollector = new ChunkingElementCollector(collector, 16);

				allChildren = new ArrayList<Object>(kwicEntryList.length
						+ EXTRA_CHILDREN);
				allChildren.add(new String("Project: " + getProjectName()));
				collector.add(new String("Project: " + getProjectName()),
						monitor);
				allChildren.add(new String("Authors: " + getAuthors()));
				collector.add(new String("Authors: " + getAuthors()), monitor);

				// only for debug: allChildren.add(new String("URI:
				// "+getURI()));
				for (KWICEntry e : kwicEntryList) {
					e.setParent(this);
					allChildren.add(e);
					groupingCollector.add(e, monitor);
				}
				collector.done();
				monitor.done();
				groupingCollector.done();
			} else {
				throw new IllegalArgumentException(
						MessageFormat
								.format(
										"SingleSearchResult''s fetchDeferredChildren does not know the children of {0}s like {1}",
										object.getClass(), object));
			}
		}
	}

	public ISchedulingRule getRule(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isContainer() {
		// TODO Auto-generated method stub
		return true;
	}

	public Object[] getChildren(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public ImageDescriptor getImageDescriptor(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLabel(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getParent(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

}
