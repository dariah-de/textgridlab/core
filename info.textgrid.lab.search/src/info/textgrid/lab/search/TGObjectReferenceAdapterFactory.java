package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdapterFactory;

public class TGObjectReferenceAdapterFactory implements IAdapterFactory {

	@SuppressWarnings("rawtypes")
	private static final Class[] ADAPTERLIST = new Class[] { TextGridObject.class, IFile.class };

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof TGObjectReference && 
				TextGridObject.class.isAssignableFrom(adapterType)) {
			return ((TGObjectReference) adaptableObject).getTgo();
		} else if (adaptableObject instanceof TGObjectReference)
			return ((TGObjectReference) adaptableObject).getTgo().getAdapter(adapterType);

		return null;
	}

	@SuppressWarnings("rawtypes")
	public Class[] getAdapterList() {
		return ADAPTERLIST;
	}

}
