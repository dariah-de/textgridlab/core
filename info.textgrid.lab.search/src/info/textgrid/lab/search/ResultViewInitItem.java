package info.textgrid.lab.search;

import org.eclipse.jface.resource.ImageDescriptor;

public class ResultViewInitItem {
	private String text = "Empty Search Result";
	private ImageDescriptor imageDescriptor =ImageDescriptor.createFromFile(this.getClass(), 
											"/icons/sample.gif");
	
	public String toString() {
		return text;
	}
	
	public ImageDescriptor getImage() {
		return imageDescriptor;
	}

}
