package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdapterFactory;

public class ContextEntryAdaptorFactory implements IAdapterFactory {

	private static final Class[] ADAPTERLIST = new Class[]{KWICEntry.class,SingleSearchResult.class,TextGridObject.class};

	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof ContextEntry && 
				KWICEntry.class.isAssignableFrom(adapterType)) {
			return ((ContextEntry) adaptableObject).getParent(); //returns a KWICEntry
		}
		if (adaptableObject instanceof ContextEntry && 
				SingleSearchResult.class.isAssignableFrom(adapterType)) {
			KWICEntry kw = ((ContextEntry) adaptableObject).getParent();
			return kw.getParent(); //returns a SingleSearchResult
		}
		if (adaptableObject instanceof ContextEntry && 
				TextGridObject.class.isAssignableFrom(adapterType)) {
			KWICEntry kw = ((ContextEntry) adaptableObject).getParent();
			return kw.getParent().getTGO();
		}

		return null;
	}

	public Class[] getAdapterList() {
		// TODO Auto-generated method stub
		return ADAPTERLIST;
	}

}
