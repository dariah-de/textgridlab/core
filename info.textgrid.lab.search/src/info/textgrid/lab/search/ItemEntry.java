package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;
import info.textgrid.namespaces.middleware.tgsearch.PathType;

import org.eclipse.core.runtime.PlatformObject;

/**
 * Represents an search result entry in the item list. 
 * @author Frank Queens
 *
 */

public class ItemEntry extends PlatformObject 
{
	private TGObjectReference tgoRef;
	private PathType pathType;
	private String path = null;
	
	ItemEntry(TGObjectReference tgoRef, PathType pathType) {
		this.tgoRef = tgoRef;
		this.pathType = pathType;
		joinPath();
	}
	
	public void setTgoRef(TGObjectReference tgoRef) {
		this.tgoRef = tgoRef;
	}
	
	public TGObjectReference getTgoRef() {
		return tgoRef;
	}
	
	public String getPath() {
		return path;
	}
	
	private void joinPath() {
		path = "";
		if (pathType == null) 
			return;
		
		for (EntryType entry : pathType.getEntry()) {
			if (path.equals(""))
				path = entry.getTitle();
			else 
				path = path + "/" + entry.getTitle();
		}
	}
	
	@Override
	public Object getAdapter(Class adapter) {
		if(adapter == TextGridObject.class)
			return tgoRef.getTgo();
		
		return super.getAdapter(adapter);
	}
}
