package info.textgrid.lab.search;

import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;
import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TGObjectReference.ITGObjectReferenceListener.Event;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.ChunkingElementCollector;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.IElementCollector;

public class TextGridRepositoryItem_SandBox extends PlatformObject implements IChildListParent, IAdaptable {
	private String title = "TextGrid Repository SandBox";
	
	@Override
	public String toString() {
		return title;
	}
	
	@Override
	public Object getAdapter(final Class adapter) {
		return Platform.getAdapterManager().getAdapter(this, adapter);
	}

	@Override
	public void fetchDeferredChildren(Object object,
			IElementCollector collector, IProgressMonitor monitor) {
		final ChunkingElementCollector groupingCollector = new ChunkingElementCollector(collector, 16);

		monitor.beginTask("Fetching repository sandbox objects", IProgressMonitor.UNKNOWN);
		
		SearchRequest searchRequest = new SearchRequest(SearchRequest.EndPoint.PUBLIC);
		//TODO only TopLevelAggregations from the repository!
		Response response = searchRequest.listTopLevelAggregations();

		if (response != null) {
			Iterator<?> iter = response.getResult().iterator();
			while (iter.hasNext()) {
				Object obj = iter.next();
				if (obj instanceof ResultType) {
					ObjectType oType = ((ResultType) obj).getObject();
					try {
						if (((ResultType) obj).isSandbox() == true) {
							
							try {
								if (oType.getGeneric().getProvided() != null) {
									TextGridObject tgo = TextGridObject
											.getInstance(oType, true, true);
									TGObjectReference tgoRef = new TGObjectReference(
											tgo.getLatestURI(), tgo);
									TextGridObject.addLatestRegistryItem(new URI(
											tgo.getLatestURI()), tgo.getURI());
									TGObjectReference.notifyListeners(
											Event.NAVIGATOR_OBJECT_CREATED, tgoRef);
									groupingCollector.add(tgoRef, monitor);
								} else {
									RestrictedTextGridObject rtgo = new RestrictedTextGridObject(
											oType.getGeneric().getGenerated()
													.getTextgridUri().getValue());
									groupingCollector.add(rtgo, monitor);
								}
							} catch (CoreException e) {
								IStatus status = new Status(
										IStatus.ERROR,
										Activator.PLUGIN_ID,
										"Error while retrieving the repository sandbox objects",
										e);
								Activator.getDefault().getLog().log(status);

							} catch (URISyntaxException e) {
								IStatus status = new Status(
										IStatus.ERROR,
										Activator.PLUGIN_ID,
										NLS.bind(
												"Error on creating URI of the latest revision of {0}",
												oType), e);
								Activator.getDefault().getLog().log(status);
							}
						}
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
				}
			}
		}	
		
		groupingCollector.done();
		monitor.done();
		
	}

	@Override
	public boolean isContainer() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ISchedulingRule getRule(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] getChildren(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImageDescriptor getImageDescriptor(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabel(Object o) {
		return "TextGrid Repository";
	}

	@Override
	public Object getParent(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addChildListChangedListener(IChildListChangedListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeChildListChangedListener(
			IChildListChangedListener listener) {
		// TODO Auto-generated method stub
		
	}

}
