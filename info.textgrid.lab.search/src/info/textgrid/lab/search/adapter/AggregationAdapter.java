/**
 * 
 */
package info.textgrid.lab.search.adapter;

import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TGObjectReference.ITGObjectReferenceListener.Event;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.ChunkingElementCollector;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.search.Activator;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * This is a provisional class that should probably be factored into {@link Aggregation} somehow?
 * 
 * @author vitt
 */
public class AggregationAdapter extends PlatformObject implements
		IDeferredWorkbenchAdapter {

	private TextGridObject object;

	public AggregationAdapter(TextGridObject textGridObject) {
		this.object = textGridObject;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.progress.IDeferredWorkbenchAdapter#fetchDeferredChildren(java.lang.Object, org.eclipse.ui.progress.IElementCollector, org.eclipse.core.runtime.IProgressMonitor)
	 */
	// TODO the core functionality should be factored out of here.
	public void fetchDeferredChildren(Object object,
			IElementCollector collector, IProgressMonitor monitor) {
		final ChunkingElementCollector groupingCollector = new ChunkingElementCollector(collector, 16);
		SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(Messages.AggregationAdapter_IM_FetchingAggregation, object), 100);
		
		TextGridObject tgoAggregation = AdapterUtils.getAdapter(object, TextGridObject.class);
		SearchRequest searchRequest = null;
		try {
			if (tgoAggregation.isPublic()) 
				searchRequest = new SearchRequest(SearchRequest.EndPoint.PUBLIC);
			else 	
				searchRequest = new SearchRequest();
		} catch (CoreException e1) {
			Activator.handleError(e1, Messages.AggregationAdapter_EM_RetrieveAggragation, StatusManager.SHOW | StatusManager.LOG, tgoAggregation);
		}
		Response response = searchRequest.listAggregation(tgoAggregation.getURI().toString());
		
		Iterator<?> iter = response.getResult().iterator();
		while (iter.hasNext()) {
			Object obj = iter.next();
			if (obj instanceof ResultType) {
				ObjectType oType = ((ResultType) obj).getObject();
				try {
					if (oType.getGeneric().getProvided() != null) {
						TextGridObject tgo = TextGridObject.getInstance(oType, true, true);
						TGObjectReference tgoRef = new TGObjectReference(((ResultType) obj).getTextgridUri(), tgo);
						tgoRef.setAggregation((TGObjectReference)object);
						if (tgoRef.isLatestRef())
							TextGridObject.addLatestRegistryItem(new URI(tgo.getLatestURI()), tgo.getURI());
						TGObjectReference.notifyListeners(Event.NAVIGATOR_OBJECT_CREATED, tgoRef);
						groupingCollector.add(tgoRef, monitor);
					} else {
						RestrictedTextGridObject rtgo = new RestrictedTextGridObject(oType.getGeneric().getGenerated().getTextgridUri().getValue());
						groupingCollector.add(rtgo, monitor);
					}			
				} catch (CoreException e) {
					 IStatus status = new Status(
							 IStatus.ERROR, 
							 Activator.PLUGIN_ID, 
							 NLS.bind(Messages.AggregationAdapter_EM_RetrieveAggragation, object.toString()),
							 e);
					 Activator.getDefault().getLog().log(status);

				} catch (URISyntaxException e) {
					IStatus status = new Status(
							 IStatus.ERROR, 
							 Activator.PLUGIN_ID, 
							 NLS.bind(Messages.AggregationAdapter_EM_URISyntaxException, oType),
							 e);
					 Activator.getDefault().getLog().log(status);
				}
			}
		}	
		groupingCollector.done();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.progress.IDeferredWorkbenchAdapter#getRule(java.lang.Object)
	 */
	public ISchedulingRule getRule(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.progress.IDeferredWorkbenchAdapter#isContainer()
	 */
	public boolean isContainer() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.model.IWorkbenchAdapter#getChildren(java.lang.Object)
	 */
	public Object[] getChildren(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.model.IWorkbenchAdapter#getImageDescriptor(java.lang.Object)
	 */
	public ImageDescriptor getImageDescriptor(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.model.IWorkbenchAdapter#getLabel(java.lang.Object)
	 */
	public String getLabel(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.model.IWorkbenchAdapter#getParent(java.lang.Object)
	 */
	public Object getParent(Object o) {
		return null;
	}

}
