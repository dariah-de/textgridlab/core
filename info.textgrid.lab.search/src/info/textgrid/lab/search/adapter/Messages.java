package info.textgrid.lab.search.adapter;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.search.adapter.messages"; //$NON-NLS-1$
	public static String AggregationAdapter_EM_RetrieveAggragation;
	public static String AggregationAdapter_EM_URISyntaxException;
	public static String AggregationAdapter_IM_FetchingAggregation;
	public static String AggregationAdapterFactory_EM_NoContentType;
	public static String AggregationAdapterFactory_EM_Null;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
