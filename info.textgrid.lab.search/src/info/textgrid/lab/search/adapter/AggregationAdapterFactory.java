package info.textgrid.lab.search.adapter;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.search.Activator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Joiner;

public class AggregationAdapterFactory implements IAdapterFactory {

	private static final TGContentType AGGREGATION_CONTENT_TYPE = TGContentType.getContentType("text/tg.aggregation+xml"); //$NON-NLS-1$
	private static final TGContentType EDITION_CONTENT_TYPE = TGContentType.getContentType("text/tg.edition+tg.aggregation+xml"); //$NON-NLS-1$
	private static final TGContentType COLLECTION_CONTENT_TYPE = TGContentType.getContentType("text/tg.collection+tg.aggregation+xml"); //$NON-NLS-1$
	private static final Class<?>[] ADAPTER_LIST = new Class[] { AggregationAdapter.class, IDeferredWorkbenchAdapter.class };

	@SuppressWarnings("unchecked")
	// due to pre-generics interface
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType.isAssignableFrom(AggregationAdapter.class)) {
			if (adaptableObject instanceof TGObjectReference) {
				Assert.isNotNull(AGGREGATION_CONTENT_TYPE,
						Messages.AggregationAdapterFactory_EM_NoContentType
								+ Joiner.on(", ").join(TGContentType.getContentTypes(false)));
				Assert.isLegal(adaptableObject != null, Messages.AggregationAdapterFactory_EM_Null);
				try {
					TextGridObject tgo = AdapterUtils.getAdapter(adaptableObject, TextGridObject.class);
					if (AGGREGATION_CONTENT_TYPE.equals(tgo.getContentType(true))) {
						return new AggregationAdapter(tgo);
					} else if (EDITION_CONTENT_TYPE.equals(tgo.getContentType(true))) {
						return new AggregationAdapter(tgo);
					} else if (COLLECTION_CONTENT_TYPE.equals(tgo.getContentType(true))) {
						return new AggregationAdapter(tgo);
					}
					
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				}
			}
		}
		return null;
	}

	public Class<?>[] getAdapterList() {
		return ADAPTER_LIST;
	}

}
