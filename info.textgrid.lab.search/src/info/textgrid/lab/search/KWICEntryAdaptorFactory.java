package info.textgrid.lab.search;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdapterFactory;

public class KWICEntryAdaptorFactory implements IAdapterFactory {

	private static final Class[] ADAPTERLIST = new Class[]{SingleSearchResult.class,TextGridObject.class};

	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof KWICEntry && 
				SingleSearchResult.class.isAssignableFrom(adapterType)) {
			return ((KWICEntry) adaptableObject).getParent(); //returns a SingleSearchResult
		}
		if (adaptableObject instanceof KWICEntry && 
				TextGridObject.class.isAssignableFrom(adapterType)) {
			SingleSearchResult ssr = ((KWICEntry) adaptableObject).getParent();
			return ssr.getTGO();
		}

		return null;
	}

	public Class[] getAdapterList() {
		// TODO Auto-generated method stub
		return ADAPTERLIST;
	}

}
