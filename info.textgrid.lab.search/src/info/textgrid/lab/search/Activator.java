package info.textgrid.lab.search;

import java.util.Date;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.search";
	public static final String DEBUG_TIMING = PLUGIN_ID + "/debug/request-timing";
	public static final String DEBUG_LAZY = PLUGIN_ID + "/debug/lazy-query";
	
	private static Date start = new Date();

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static boolean isDebugging(final String option) {
		return getDefault().isDebugging() &&
			Boolean.valueOf(Platform.getDebugOption(option));
	}
	
	public static void resetLogTimer() {
		start = new Date();
	}
	
	public static void debugLog(String msg) {
		if (!isDebugging(DEBUG_TIMING))
			return;
		Date now = new Date();
		long ms = now.getTime() - start.getTime();
		System.out.printf("%1$tT.%1$tL (%2$tT.%2$tL) %3$s\n", ms, now, msg);
	}
	
	public static IStatus handleError(Throwable e, String message, int style, Object... args) {
		Status status = null;
		if (style==StatusManager.BLOCK) {
			status = new Status(IStatus.ERROR, PLUGIN_ID, NLS.bind(message, args), new Exception(""));
		} else {
			status = new Status(IStatus.ERROR, PLUGIN_ID, NLS.bind(message, args), e);
		}
		StatusManager.getManager().handle(status, style);
		return status;
	}
}
