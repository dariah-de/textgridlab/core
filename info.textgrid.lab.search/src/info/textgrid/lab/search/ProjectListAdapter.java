package info.textgrid.lab.search;

import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TGObjectReference; 
import info.textgrid.lab.core.model.TGObjectReference.ITGObjectReferenceListener.Event;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.ChunkingElementCollector;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;

public class ProjectListAdapter implements IDeferredWorkbenchAdapter {
	
	private TextGridProject project;
	
	public ProjectListAdapter(TextGridProject adaptableObject){
		this.project = adaptableObject;
	}

	public void fetchDeferredChildren(Object object,
			IElementCollector collector, IProgressMonitor monitor) {
		
		final ChunkingElementCollector groupingCollector = new ChunkingElementCollector(collector, 16);

		monitor.beginTask("Fetching objects in " + project, IProgressMonitor.UNKNOWN);
		
		SearchRequest searchRequest = new SearchRequest();
		Response response = searchRequest.listProject(project.getId());
		
		if (response != null) {
			Iterator<?> iter = response.getResult().iterator();
			while (iter.hasNext()) {
				Object obj = iter.next();
				if (obj instanceof ResultType) {
					ObjectType oType = ((ResultType) obj).getObject();
					try {
						if (oType.getGeneric().getProvided() != null) {
							TextGridObject tgo = TextGridObject.getInstance(oType, true, true);
							TGObjectReference tgoRef = new TGObjectReference(tgo.getLatestURI(), tgo);
							TextGridObject.addLatestRegistryItem(new URI(tgo.getLatestURI()), tgo.getURI());
							TGObjectReference.notifyListeners(Event.NAVIGATOR_OBJECT_CREATED, tgoRef);
							groupingCollector.add(tgoRef, monitor);
						} else {
							RestrictedTextGridObject rtgo = new RestrictedTextGridObject(oType.getGeneric().getGenerated().getTextgridUri().getValue());
							groupingCollector.add(rtgo, monitor);
						}	
					} catch (CoreException e) {
						 IStatus status = new Status(
								 IStatus.ERROR, 
								 Activator.PLUGIN_ID, 
								 NLS.bind("Error while retrieving the objects of project {0}", project.getName()),
								 e);
						 Activator.getDefault().getLog().log(status);
	
					} catch (URISyntaxException e) {
						IStatus status = new Status(
								 IStatus.ERROR, 
								 Activator.PLUGIN_ID, 
								 NLS.bind("Error on creating URI of the latest revision of {0}", oType),
								 e);
						 Activator.getDefault().getLog().log(status);
					}
				}
			}
		}	
		
		groupingCollector.done();
		monitor.done();
	}

	public ISchedulingRule getRule(Object object) {
		// TODO not sure whether we need to generate something here via the resource rule factory
		return null;
	}

	public boolean isContainer() {
		return true;
	}

	public Object[] getChildren(Object o) {
		return null;
	}

	public ImageDescriptor getImageDescriptor(Object object) {
		// TODO Maybe return a project icon here? However, null seems to work.
		return null;
	}

	public String getLabel(Object o) {
		return project.toString();
	}

	public Object getParent(Object o) {
		return null;
	}
}
