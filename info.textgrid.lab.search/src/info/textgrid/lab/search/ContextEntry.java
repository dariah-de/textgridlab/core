package info.textgrid.lab.search;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.eclipse.core.runtime.PlatformObject;

/**
 * This represents one context within a KWICEntry
 * 
 * @author martin
 * 
 */
public class ContextEntry extends PlatformObject {
	private KWICEntry parent = null;
	private OMElement context = null;

	public ContextEntry(OMElement context) {
		this.context = context;
	}

	public void setParent(KWICEntry parent) {
		this.parent = parent;
	}

	public KWICEntry getParent() {
		return parent;
	}

	/**
	 * This is a hack that will surround the match term with *AAA*term*ZZZ* for
	 * post-processing by the LabelProvider of the ResultViewer.
	 * 
	 * @param contextWithTags
	 * @return
	 */
	public String prettyprint(String contextWithTags) {
		contextWithTags = contextWithTags.replaceAll("</[^:]+:match>", "*ZZZ*");
		contextWithTags = contextWithTags.replaceAll("<[^:<> ]+:match[^>]*>",
				"*AAA*");
		contextWithTags = contextWithTags.replaceFirst("<[^>]+>", ""); // remove
		// <tg:context>
		contextWithTags = contextWithTags.replaceAll("</[^>]+>", ""); // remove
		// </tg:context>
		contextWithTags = contextWithTags.replaceAll("\\n", " ");
		contextWithTags = contextWithTags.replaceAll("\\s+", " ");
		return contextWithTags;
	}

	/**
	 * returns prettyprint(context.toString())
	 */
	public String toString() {
		return prettyprint(context.toString());
	}

}
