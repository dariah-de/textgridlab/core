package info.textgrid.lab.search;

import static info.textgrid.lab.search.Activator.DEBUG_LAZY;
import static info.textgrid.lab.search.Activator.isDebugging;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.FulltextType.Kwic;
import info.textgrid.namespaces.middleware.tgsearch.PathGroupType;
import info.textgrid.namespaces.middleware.tgsearch.PathType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.PendingUpdateAdapter;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.Lists;

/**
 * A lazy content provider for search results.
 * 
 * The idea is as follows:
 * 
 * When the input is set or the item count of the root element needs to be
 * refreshed, we start an initial query with a cutoff at, say, 100 results.
 * 
 * When we have a search result, we set the item's child count to the number of
 * hits. Afterwards, we start adding result items (see below for what that
 * means).
 * 
 * When the TreeViewer wants an element to be updated, we're looking for whether
 * that element is already in our result cache. If it is, we simply call
 * {@link TreeViewer#replace(Object, int, Object)} to show it in tree viewer,
 * otherwise we
 * 
 * @author tv
 * 
 */
public class LazySearchResultProvider implements ILazyTreeContentProvider {

	/**
	 * The number of items to request per chunk.
	 */
	private static final int CHUNK_SIZE = 50;

	/**
	 * The job that actually performs the query in the background.
	 * 
	 * A new instance is created by
	 * {@link LazySearchResultProvider#initiateSearchRequest(SearchRequest)}. Until all
	 * results have been fetched, each time the job is run it will retrieve
	 * another chunk.
	 * 
	 * @author vitt
	 * 
	 */
	private final class SearchRequestJob extends Job {
		private final SearchRequest searchRequest;
		private boolean finished = false;
		private boolean pending = true;
		private int childCount = 0;

		/**
		 * Creates the job. The query must already be initialized
		 * 
		 * @param query
		 * @see LazySearchResultProvider#initiateSearchRequest(SearchRequest)
		 */
		private SearchRequestJob(SearchRequest query) {
			super(NLS.bind("Performing {0} ...", query));
			this.searchRequest = query;
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			SubMonitor progress = SubMonitor.convert(monitor, 150);

			if (finished)
				return new Status(IStatus.WARNING, Activator.PLUGIN_ID, NLS.bind(
						"Already returned all {0} results visible to you of {1}", maxResult, searchRequest));

			
			progress.subTask("Sending query ...");
			int start = 0;
			if (requestedItems.iterator().hasNext())
				start = requestedItems.iterator().next();
			Response result = searchRequest.execute(progress.newChild(40), start, CHUNK_SIZE);
			if (result == null) 
				return Status.CANCEL_STATUS;
			progress.subTask("Retrieving results ...");
			if (progress.isCanceled())
				return Status.CANCEL_STATUS;
			
			progress.worked(5);
			Iterator<ResultType> childElements = result.getResult().iterator();
			progress.worked(5);
			setPending(false);
			int itemCount = 0;
			int fulltextCount = 0;
			while (childElements.hasNext()) {
				if (progress.isCanceled())
					return Status.CANCEL_STATUS;
				ResultType obj = childElements.next();
				ObjectType oType = obj.getObject();
				List<Kwic> kwicList;
				boolean hasPath = false;
				int pathCount = 0;
				try {
					kwicList = obj.getFulltext().get(0).getKwic();
				} catch (IndexOutOfBoundsException e) {
					kwicList = null;
				}
				try {
					ItemEntry entry;
					TextGridObject tgo = TextGridObject.getInstance(oType, true, true);
					TGObjectReference tgoRef = new TGObjectReference(tgo.getLatestURI(), tgo);
					if (obj.getPathResponse() != null) {
						for (PathGroupType pathGroup :obj.getPathResponse().getPathGroup()) {
							for (PathType pathType : pathGroup.getPath()) {
								entry = new ItemEntry(tgoRef, pathType);
								addResult(entry);
								if (pathCount > 0)
									itemCount++;
								pathCount++;
								hasPath = true;
							}
						}
					}	
					progress.worked(1);
					
					// no path information
					if (!hasPath) {
						entry = new ItemEntry(tgoRef, null);
						addResult(entry);
					}
					
					//Fulltext
					if (kwicList != null) {
						for (Kwic item : kwicList) {
							FullTextEntry fulltextEntry = new FullTextEntry(tgoRef,
																			item.getLeft(),
																			item.getMatch(),
																			item.getRight(),
																			item.getXpath());
							fulltextCount++;
							addResult(fulltextEntry);
						}
					}	
				} catch (CoreException e) {
					 IStatus status = new Status(
							 IStatus.ERROR, 
							 Activator.PLUGIN_ID, 
							 NLS.bind("Error when trying to insert object {0} in search result.", oType.toString()),
							 e);
					 Activator.getDefault().getLog().log(status);
				}
			}
			try {
				childCount = Integer.parseInt(result.getHits());
			} catch (NumberFormatException e) {
				childCount = 0;
			}		
			childCount = childCount + fulltextCount + itemCount;
//			System.out.println("Anzahl Einträge: " + childCount);
			if (childCount == 0) {
				addResult(new NoMatchEntry());
				childCount = 1;
			}
			if (!isChildCountSet)
				setResultCount(childCount);
			//TODO check counting (former itemcount)
			if (childCount < CHUNK_SIZE - 3)
				finish();
			
			flushRequestedResults();
			
			return Status.OK_STATUS;
		}

		private void finish() {
			finished = true;
			//setResultCount(maxResult);
		}

		private void setResultCount(final int allHits) {
			UIJob resultCount = new UIJob("Setting result count") {

				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					PlatformUI.getWorkbench().getDisplay().update();
					monitor.beginTask("Set Result Count", 1000);
					monitor.subTask("SubTask");
					for(int i = 1; i<1000; i++)
						monitor.worked(1);
					
					treeViewer.setChildCount(input, allHits);
					treeViewer.refresh(true);
					if (isDebugging(DEBUG_LAZY))
						System.out.println("Adjusted hit count to " + allHits);
					isChildCountSet = true;
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			resultCount.setUser(true);
			resultCount.schedule();
		}

		protected synchronized void setPending(boolean pending) {
			this.pending = pending;
		}

		/**
		 * if <code>true</code>, we don't know yet how many results we have and
		 * should display a 'pending' label.
		 */
		protected synchronized boolean isPending() {
			return pending;
		}
	}

	private TreeViewer treeViewer;
	private SearchRequest input;

	private volatile int maxResult = -1;
	private List<Object> cache = null;
	private Set<Integer> requestedItems = Collections.synchronizedSet(new HashSet<Integer>());
	private SearchRequestJob searchRequestJob;
	private boolean isChildCountSet = false;

	protected synchronized List<Object> getCache() {
		if (cache == null)
			cache = Collections.synchronizedList(Lists.newArrayListWithExpectedSize(100));
		return cache;
	}

	@Override
	public void dispose() {
		if (searchRequestJob != null && searchRequestJob.getState() != Job.NONE)
			searchRequestJob.cancel();
	}

	/**
	 * Called when a new query has been set as input.
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		Assert.isLegal(viewer instanceof TreeViewer, "LazySearchResultProvider can only be used with an TreeViewer.");
		treeViewer = (TreeViewer) viewer;
		isChildCountSet = false;
		if (newInput instanceof SearchRequest) {
			this.input = (SearchRequest) newInput;
			initiateSearchRequest(input);
			treeViewer.refresh(true); // #9307 / TG-1901
		} else if (newInput instanceof ResultViewInitItem) {
			treeViewer.setChildCount(newInput, 1);
			treeViewer.replace(newInput, 0, getPendingUpdateAdapter());
			treeViewer.refresh(true); // (newInput, null);
		}
	}

	/**
	 * Initiates a new search request from the start (i.e. first portion).
	 * 
	 * @param searchRequest
	 */
	private void initiateSearchRequest(final SearchRequest searchRequest) {
		getCache().clear();
		maxResult = -1;
		treeViewer.setChildCount(input, 1);
		treeViewer.replace(input, 0, getPendingUpdateAdapter());
		treeViewer.refresh();

		if (searchRequestJob != null && searchRequestJob.getState() != Job.NONE)
			searchRequestJob.cancel();

		requestedItems.clear();
		searchRequest.getSearchClient().setLIMIT(CHUNK_SIZE);
		searchRequestJob = new SearchRequestJob(searchRequest);
		searchRequestJob.schedule();
	}

	/**
	 * Inserts all items that have been requested and returned into the
	 * treeViewer. May spawn a new fetch job afterwards. Can be run from a
	 * non-UI thread.
	 */
	protected void flushRequestedResults() {
		new UIJob("Displaying results ...") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				Iterator<Integer> iterator = requestedItems.iterator();
				while (iterator.hasNext()) {
					Integer index = iterator.next();
					if (index <= maxResult)
						try {
							final Object element = getCache().get(index);
							treeViewer.replace(input, index, element);
							iterator.remove();
						} catch (ArrayIndexOutOfBoundsException e) {
							// Result isn't there yet -> no problem, remains in
							// set of requested items
						}
				}

				treeViewer.getTree().redraw(); // TG-1234
				StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, "Redrew search result tree"));

				if (!requestedItems.isEmpty() && searchRequestJob.getState() == Job.NONE) 
					searchRequestJob.schedule(); // go on searching

				return Status.OK_STATUS;
			}
		}.schedule();
	}

	/**
	 * Adds a new result to the cache. May be called from any thread.
	 * 
	 * @param singleSearchResult
	 *            the result to add.
	 * 
	 */
	protected void addResult(Object tgoRef) {
		getCache().add(tgoRef);
		maxResult++;
	}

	@Override
	public void updateElement(Object parent, int index) {
		if (parent == input) {
			if (index <= maxResult) {
				treeViewer.replace(parent, index, getCache().get(index));
			} else {
				requestedItems.add(index);
				if (searchRequestJob.getState() == Job.NONE)
					searchRequestJob.schedule();
				if (index == maxResult + 1) {
					treeViewer.replace(parent, index, getPendingUpdateAdapter());
				}
			}
		} else {
			IDeferredWorkbenchAdapter adapter = AdapterUtils.getAdapter(parent, IDeferredWorkbenchAdapter.class);
			if (adapter != null) {
				Object[] children = adapter.getChildren(parent);
				if (children != null)
					treeViewer.replace(parent, index, children[index]);
			}
		}
		// TODO other parent items
	}

	private PendingUpdateAdapter getPendingUpdateAdapter() {
		return new PendingUpdateAdapter();
	}

	@Override
	public void updateChildCount(Object element, int currentChildCount) {
		if (element == input) {
			if (searchRequestJob.isPending() && currentChildCount == 0)
				treeViewer.setChildCount(element, 1);
			else
				treeViewer.setChildCount(element, searchRequestJob.childCount);
			// } else {
			// IDeferredWorkbenchAdapter adapter =
			// AdapterUtils.getAdapter(element,
			// IDeferredWorkbenchAdapter.class);
			// if (adapter != null) {
			// Object[] children = adapter.getChildren(element);
			// if (children != null && children.length > 0)
			// treeViewer.setChildCount(element, children.length);
			// else
			// treeViewer.setHasChildren(element, adapter.isContainer());
			// }
			// // TODO other items
		}
	}

	@Override
	public Object getParent(Object element) {
		if (element == input)
			return null;
		else
			return input;
		// TODO other items

	}

}
