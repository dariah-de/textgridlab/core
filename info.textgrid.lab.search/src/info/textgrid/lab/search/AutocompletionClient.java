package info.textgrid.lab.search;

import info.textgrid.lab.core.model.util.StringToOM;

import java.io.IOException;
import java.net.URLEncoder;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * A content proposal provider that queries the autocompletion code of TG-search
 * in a background job.
 * 
 * <h4>Usage</h4>
 * <p>
 * Initialize the method using the query field and pass it on to the
 * {@link ContentProposalAdapter}.
 * </p>
 * 
 * {@link #getProposals(String, int)} returns rather fast, so this method could
 * be used with a short activation delay.
 * 
 * <h4>Implementation Notes</h4>
 * <p>
 * This implementation should circumvent TG-548 although there is currently no
 * support for slow completion code in Eclipse (TG-548: Lags while entering
 * metadata (e.g. in the import dialog)
 * https://develop.sub.uni-goettingen.de/jira/browse/TG-548).
 * </p>
 * <p>
 * The first call to {@linkplain #getProposals(String, int)
 * getProposals(<var>query</var>, <var>position</var>)} will initiate an
 * auto-completion query for <code>query.subString(0, position)</code> in the
 * background. Further calls will check whether the new <var>query</var> is a
 * substring of that background request. If yes, they will return those of the
 * results the background job has already fetched that match the new
 * <var>query</var>. Otherwise, a new request will be started.
 * </p>
 * <p>
 * In any case, this method returns immediately at the cost of returning an
 * incomplete or empty list of completions.
 * </p>
 * 
 * @author martin, thorsten, Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public abstract class AutocompletionClient implements IContentProposalProvider {

	protected ExtendedContentProposalAdapter contentProposalAdapter;

	public ExtendedContentProposalAdapter getContentProposalAdapter() {
		return contentProposalAdapter;
	}

	public void setContentProposalAdapter(
			ExtendedContentProposalAdapter contentProposalAdapter) {
		this.contentProposalAdapter = contentProposalAdapter;
	}

	/**
	 * A function that converts a {@link String} into a simple
	 * {@link IContentProposal} for the string.
	 * 
	 * @author vitt
	 * 
	 */
	private final class ContentProposalConvertor implements
			Function<String, IContentProposal> {
		public IContentProposal apply(final String result) {
			return new IContentProposal() {

				public String getLabel() {
					return result;
				}

				public String getDescription() {
					return null; // TG-731; shouldn't duplicate label
				}

				public int getCursorPosition() {
					return result.length();
				}

				public String getContent() {
					return result;
				}
			};
		}
	}

	protected static final IContentProposal[] NONE = new IContentProposal[0];

	protected String whereToComplete = "type"; // initially...

	/**
	 * Set (or modify) the element of metadata (e.g. "type" for the text type,
	 * or "agent" for the author) that should be searched for when fragments
	 * have to be completed.
	 * 
	 * @param field
	 */
	public void setWhereToComplete(String field) {
		whereToComplete = field;
	}

	/**
	 * Constructor: set the element of metadata (e.g. "type" for the text type,
	 * or "agent" for the author) that should be searched for when fragments
	 * have to be completed.
	 * 
	 * @param whichField
	 */
	public AutocompletionClient(String whichField) {
		setWhereToComplete(whichField);
	}

	abstract protected AbstractRequest createRequest(String whereToComplete,
			String query);

	protected AbstractRequest request;

	/**
	 * How many characters should be given before beginning the search process.
	 * 
	 * @return
	 */
	protected int getMinimalLength() {
		return 1;
	}

	public IContentProposal[] getProposals(final String contents,
			final int position) {

		if (contents.length() < getMinimalLength())
			return NONE;

		final String query = contents.substring(0, position);
		if ("".equals(query))
			return NONE;

		if (request == null || !request.isApplicableFor(query)) {
			if (request != null)
				request.cancel();
			request = createRequest(whereToComplete, query);
			request.schedule();
		}
		Iterable<String> proposals = request.getResultsFor(query);

		return Iterables.toArray(
				Iterables.transform(proposals, getContentProposalConvertor()),
				IContentProposal.class);
	}

	protected ContentProposalConvertor getContentProposalConvertor() {
		return new ContentProposalConvertor();
	}

	protected OMElement getHttpResponse(String url, String query)
			throws HttpException, IOException, XMLStreamException,
			FactoryConfigurationError {

		HttpClient httpclient = new HttpClient();
		String completeUrl = url + URLEncoder.encode(query, "UTF-8");

		HttpMethod method = new GetMethod(completeUrl);

		httpclient.executeMethod(method);

		String result = new String(method.getResponseBody(), "UTF-8");
		method.releaseConnection();

		int statusCode = method.getStatusCode();

		if (statusCode != 200) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Error: Couldn't fetch results (Reason: "
							+ method.getStatusLine() + ")" + "\nURL: "
							+ completeUrl);

			StatusManager.getManager().handle(status,
			/* StatusManager.SHOW | */StatusManager.LOG);

			return null;
		}

		return StringToOM.getOMElement(result);
	}

	protected void forceOpenPopupIfClosed() {
		if (!contentProposalAdapter.isProposalPopupOpen())
			new UIJob("") {

				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					contentProposalAdapter.openPopup();
					return Status.OK_STATUS;
				}
			}.schedule();
	}
}