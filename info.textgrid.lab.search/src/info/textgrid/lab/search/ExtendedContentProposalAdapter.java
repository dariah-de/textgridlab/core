package info.textgrid.lab.search;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.swt.widgets.Control;

/**
 * An extended {@link ContentProposalAdapter} for getting proposals from the
 * network.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ExtendedContentProposalAdapter extends ContentProposalAdapter {

	public ExtendedContentProposalAdapter(Control control,
			IControlContentAdapter controlContentAdapter,
			IContentProposalProvider proposalProvider, KeyStroke keyStroke,
			char[] autoActivationCharacters) {

		super(control, controlContentAdapter, proposalProvider, keyStroke,
				autoActivationCharacters);
	}

	/**
	 * Open the proposals popup
	 */
	public void openPopup() {
		openProposalPopup();
	}
}
