package info.textgrid.lab.search;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.namespace.QName; //import javax.xml.stream.XMLOutputFactory;
//import javax.xml.stream.XMLStreamException;
//import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;

/**
 * This represents one entry for a KeyWord-In-Context. May have 1..n
 * ContextEntry children which are retrieved deferredly.
 * 
 * @author martin
 * 
 */
public class KWICEntry extends PlatformObject implements
		IDeferredWorkbenchAdapter {
	private String path = null;
	private String pathLabel = null;
	private ContextEntry[] allChildren = null;
	// private OMElement context = null;
	private SingleSearchResult parent = null;

	/**
	 * create a KWIC Entry from an OM Element like this: <tg:entry>
	 * <tg:location> <tg:locitem path="/">CWDS - Band 1, Seite 37, Lemma
	 * abmartern</tg:locitem> </tg:location> <tg:context>für die Kodierungs-
	 * <tg:match>und</tg:match> Korrekturphase</tg:context> </tg:entry>
	 * 
	 * @param ome
	 * @param namespace -
	 *            should be "http://textgrid.info/namespaces/metadata"
	 */
	public KWICEntry(OMElement ome, String namespace) {

		OMElement location = ome.getFirstChildWithName(new QName(namespace,
				"location"));
		
		boolean firstPass = true;
		for (@SuppressWarnings("unchecked")
				Iterator<OMElement> locationIterator = (Iterator<OMElement>) location.getChildrenWithLocalName("locitem"); locationIterator.hasNext();) {
			OMElement itemLocation = locationIterator.next();
			if (firstPass) 
				pathLabel = itemLocation.getText();
			else
				pathLabel = pathLabel + " > " + itemLocation.getText();
			firstPass = false;
		}
		
		path = location.getFirstChildWithName(new QName(namespace, "locitem"))
				.getAttributeValue(new QName("path"));

		ArrayList<ContextEntry> cArray = new ArrayList<ContextEntry>();
		Iterator citer = ome
				.getChildrenWithName(new QName(namespace, "context"));
		while (citer.hasNext()) {
			OMElement nextContext = (OMElement) citer.next();
			ContextEntry ce = new ContextEntry(nextContext);
			ce.setParent(this);
			cArray.add(ce);
		}
		allChildren = cArray.toArray(new ContextEntry[0]);
	}

	public String getPath() {
		return path;
	}

	public String getPathLabel() {
		return pathLabel;
	}

	public ContextEntry[] getChildren() {
		return allChildren;
	}

	public void setParent(SingleSearchResult parent) {
		this.parent = parent;
	}

	public SingleSearchResult getParent() {
		return parent;
	}

	/**
	 * returns the number of ContextEntry Children followed by the XPath where
	 * they were found.
	 */
	public String toString() {
		return allChildren.length + " " + path;
	}

	/**
	 * contains the logic of how to populate, on demand, the children list
	 */
	public void fetchDeferredChildren(Object object,
			IElementCollector collector, IProgressMonitor monitor) {
		monitor.beginTask("Retrieving children of " + toString(),
				IProgressMonitor.UNKNOWN);
		synchronized (this) {
			if (object instanceof KWICEntry) {
				for (ContextEntry ce : allChildren) {
					collector.add(ce, monitor);
				}
				collector.done();
				monitor.done();
			} else {
				throw new IllegalArgumentException(
						MessageFormat
								.format(
										"KWICEntry''s fetchDeferredChildren does not know the children of {0}s like {1}",
										object.getClass(), object));
			}
		}
	}

	public ISchedulingRule getRule(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isContainer() {
		// TODO Auto-generated method stub
		return true;
	}

	public Object[] getChildren(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public ImageDescriptor getImageDescriptor(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLabel(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getParent(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

}
