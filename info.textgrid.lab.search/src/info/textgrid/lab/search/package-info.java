/**
 * The Lab encapsulation of the TG-search functionality.
 * 
 * <h4>Example</h4>
 * 
 * As a usage example, here is some code from the XML editor that displays all
 * XML Schemas available to the user. The original code can be found in
 * {@link info.textgrid.lab.xmleditor.dialogs.SelectSchemaPage}.
 * 
 * <pre>
 * schemaTable = new {@linkplain info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer TextGridObjectTableViewer}(control, SWT.SINGLE
 * 				| SWT.FULL_SELECTION | SWT.VIRTUAL);
 * // ... format the table viewer ...
 * 		{@linkplain info.textgrid.lab.search.ResultHolder ResultHolder} resultHolder = new ResultHolder();
 * 		{@linkplain info.textgrid.lab.search.TextGridQuery TextGridQuery} query = new TextGridQuery(resultHolder);
 * 		Map&lt;String, String&gt; queryFields = new HashMap&lt;String, String&gt;();
 * 		queryFields.put(&quot;format&quot;, &quot;text/xsd+xml&quot;);
 * 		query.setQueryMetadata(queryFields);
 * 		schemaTable.setInput(resultHolder);
 * </pre>
 * 
 * <h4>Subpackages</h4>
 * <dl>
 * <dt>{@link info.textgrid.lab.search.restclient}</dt>
 * <dd>contains a lower-level client for TG-search's REST interface.</dd>
 * </dl>
 */
package info.textgrid.lab.search;


