package info.textgrid.lab.newsearch;

import static info.textgrid.lab.search.Activator.debugLog;
import static info.textgrid.lab.search.Activator.resetLogTimer;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.ConfPlugin;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.search.Activator;
import info.textgrid.lab.search.NoMatchEntry;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;
import org.eclipse.ui.statushandlers.StatusManager;


/**
 * Handles the requests for the tg-search webservice.
 *
 * @author Frank Queens
 *
 */
public class SearchRequest extends PlatformObject implements IDeferredWorkbenchAdapter {

	public class ListAggregationResult {
		public String uri;
		public String title;
		public int count;

		public ListAggregationResult(final String uri, final String title, final int count) {
			this.uri = uri;
			this.title = title;
			this.count = count;
		}
	}

	private SearchClient searchClient = null;
	private ArrayList<TextGridObject> resultList = new ArrayList<TextGridObject>();
	private String queryMetadata;
	private String queryText;
	private String queryBaseline;
	private String revisionURI;
	private String queryRelated;
	private String relatedURI;
	private TargetModus target = TargetModus.BOTH;
	private int wordDistance = -1;

	private boolean isMetadataQuery;
	private boolean isTextQuery;
	private boolean isBaselineQuery;
	private boolean isRevisionQuery;
	private boolean isRelatedQuery;
	private boolean hitCountAvailable = false;
	private boolean isResolvePath = false;
	private boolean allProjects = false;
	private String sid = null;

	public Integer returnedByNow = 0;
	public Integer returnedInThisPortion;

	private int hitCount = 0;

	private String ORDER = "relevance"; //"asc:title";

	private ListenerList hitCountListeners = new ListenerList();
	private ListenerList partSearchReadyListeners = new ListenerList();

	public static enum TargetModus {STRUCTURE, METADATA, BOTH};
	public static enum EndPoint {STANDARD, PUBLIC};

	private static final String TGSearchProjectException = "The webservice TG-Search isn't available at the moment.";

	public SearchRequest() {
		try {
			searchClient = new SearchClient(ConfClient.getInstance()
					.getValue(ConfservClientConstants.TG_SEARCH));
			searchClient.setSid(RBACSession.getInstance().getSID(false));
			searchClient.setLIMIT(100);
			
			IPreferenceStore preferenceStore = ConfPlugin.getDefault().getPreferenceStore();
	        if(preferenceStore.getBoolean(ConfPlugin.COMPRESSED_TRANSFER)) {
	        	searchClient.enableGzipCompression();
	        }
			
		} catch (final OfflineException e) {
			final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"System offline, can not connect to TG-Search.");
			Activator.getDefault().getLog().log(status);
		}

	}

	public SearchRequest(final EndPoint endPoint) {
		try {
			if (endPoint != null) {
				if (endPoint.equals(EndPoint.PUBLIC))
					searchClient = new SearchClient(ConfClient.getInstance()
							.getValue(ConfservClientConstants.TG_SEARCH_PUBLIC));
				else
					searchClient = new SearchClient(ConfClient.getInstance()
							.getValue(ConfservClientConstants.TG_SEARCH));
			} else {
				searchClient = new SearchClient(ConfClient.getInstance()
						.getValue(ConfservClientConstants.TG_SEARCH));
			}
			searchClient.setSid(RBACSession.getInstance().getSID(false));
			searchClient.setLIMIT(100);
			IPreferenceStore preferenceStore = ConfPlugin.getDefault().getPreferenceStore();
	        if(preferenceStore.getBoolean(ConfPlugin.COMPRESSED_TRANSFER)) {
	        	searchClient.enableGzipCompression();
	        }
		} catch (final OfflineException e) {
			final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"System offline, can not connect to TG-Search.");
			Activator.getDefault().getLog().log(status);
		}
	}

	public void setSID(final String sid) {
		searchClient.setSid(sid);
	}

	public Response listProject(final String projectID) {
		try {
			return searchClient.listProject(projectID);
		} catch (final Exception e) {
			Activator.handleError(e, TGSearchProjectException, StatusManager.BLOCK);
			Activator.handleError(e, TGSearchProjectException, StatusManager.LOG);
			return null;
		}
	}

	public Response listAggregation(final String aggregation) {
		try {
			return searchClient.listAggregation(aggregation);
		} catch (final Exception e) {
			Activator.handleError(e, TGSearchProjectException, StatusManager.BLOCK);
			Activator.handleError(e, TGSearchProjectException, StatusManager.LOG);
			return null;
		}
	}

	public Response listTopLevelAggregations() {
		try {
			return searchClient.listToplevelAggregations();
		} catch (final Exception e) {
			Activator.handleError(e, TGSearchProjectException, StatusManager.BLOCK);
			Activator.handleError(e, TGSearchProjectException, StatusManager.LOG);
			return null;
		}
	}

	public TextGridObject[] getResultList() {
		return resultList.toArray(new TextGridObject[0]);
	}

	public void setQueryMetadata(final String queryMetadata) {
		if (!queryMetadata.isEmpty()) {
			this.isMetadataQuery = true;
			this.queryMetadata = queryMetadata;
		}
	}

	public void setQueryText(final String queryText) {
		if (!queryText.isEmpty()) {
			this.isTextQuery = true;
			this.queryText = queryText;
		}
	}

	public void setQueryBaseline(final String queryBaseline) {
		if (!queryBaseline.isEmpty()) {
			this.isBaselineQuery = true;
			this.queryBaseline = queryBaseline;
		}
	}

	public void setQueryRevision(final String revisionURI) {
		if (!revisionURI.isEmpty()) {
			this.isRevisionQuery = true;
			this.revisionURI = revisionURI;
		}
	}

	public void setQueryRelated(final String queryRelated, final String relatedURI) {
		if (!queryRelated.isEmpty()) {
			this.isRelatedQuery = true;
			this.queryRelated = queryRelated;
			this.relatedURI = relatedURI;
		}
	}

	public Response execute(final IProgressMonitor monitor) {
		try {
			Response response = null;
			searchClient.setTarget(target.toString().toLowerCase());
			searchClient.setResolvePath(isResolvePath);
			if (wordDistance != -1)
				searchClient.setWordDistance(wordDistance);
			if (isMetadataQuery) {
				if (isAllProjects())
					response = searchClient.queryAllProjects(queryMetadata, null, RBACSession.getInstance().getSID(false), 0, Integer.MAX_VALUE);
				else
					response = searchClient.query(queryMetadata);
				sid = searchClient.getSid();
			} else if (isTextQuery) {
				if (isAllProjects())
					response = searchClient.queryAllProjects(queryText, null, null, 0, Integer.MAX_VALUE);
				else
					response = searchClient.query(queryText);
				sid = searchClient.getSid();
			} else if (isBaselineQuery) {
				response = searchClient.queryBaseline(queryBaseline, 0);
				sid = searchClient.getSid();
			} else if (isRevisionQuery) {
				response = searchClient.listRevisionsAndMeta(revisionURI);
				sid = searchClient.getSid();
			} else if (isRelatedQuery) {
				response = searchClient.getRelatedAndMeta(queryRelated, relatedURI);
				sid = searchClient.getSid();
			}
			if (response != null && !hitCountAvailable) {
				try {
					hitCount = Integer.parseInt(response.getHits());
					setHitCountAvailable(true);
				} catch (final NumberFormatException e) {
					hitCount = 0;
				}

			}
			return response;
		} catch (final Exception e) {
			Activator.handleError(e, TGSearchProjectException, StatusManager.BLOCK);
			Activator.handleError(e, TGSearchProjectException, StatusManager.LOG);
			return null;
		}
	}

	public Response execute(final IProgressMonitor monitor, final int start, final int limit) {
		try {
			Response response = null;
			searchClient.setTarget(target.toString().toLowerCase());
			searchClient.setResolvePath(isResolvePath);
			if (wordDistance != -1)
				searchClient.setWordDistance(wordDistance);
			if (isMetadataQuery) {
				if (isAllProjects())
					response = searchClient.queryAllProjects(queryMetadata, ORDER, sid, start, limit);
				else
					response = searchClient.query(queryMetadata, ORDER, sid, start, limit);
				sid = searchClient.getSid();
			} else if (isTextQuery) {
				if (isAllProjects())
					response = searchClient.queryAllProjects(queryText, ORDER, sid, start, limit);
				else
					response = searchClient.query(queryText, ORDER, sid, start, limit);
				sid = searchClient.getSid();
			} else if (isBaselineQuery) {
				response = searchClient.queryBaseline(queryBaseline, ORDER, sid, start, limit);
				sid = searchClient.getSid();
			} else if (isRevisionQuery) {
				response = searchClient.listRevisionsAndMeta(revisionURI);
				sid = searchClient.getSid();
			} else if (isRelatedQuery) {
				response = searchClient.getRelatedAndMeta(queryRelated, relatedURI);
				sid = searchClient.getSid();
			}
			if (response != null && !hitCountAvailable) {
				try {
					hitCount = Integer.parseInt(response.getHits());
					setHitCountAvailable(true);
				} catch (final NumberFormatException e) {
					hitCount = 0;
				}

			}
			setPartSearchReady();
			return response;
		} catch (final Exception e) {
			Activator.handleError(e, TGSearchProjectException, StatusManager.BLOCK);
			Activator.handleError(e, TGSearchProjectException, StatusManager.LOG);
			return null;
		}
	}

	public SearchClient getSearchClient() {
		return searchClient;
	}

	@Override
	public Object[] getChildren(final Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImageDescriptor getImageDescriptor(final Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabel(final Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getParent(final Object o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fetchDeferredChildren(final Object object,
			final IElementCollector collector, final IProgressMonitor monitor) {

		monitor.beginTask("Search running", IProgressMonitor.UNKNOWN);
		synchronized (this) {

			if (object instanceof SearchRequest) {

				final boolean noItems = true;

				resetLogTimer();
				debugLog("SearchResult fetching: Starting query ...");
				final Response searchResult = execute(monitor);
				debugLog("SearchResult fetching: Query done");

				final Iterator<?> iter = searchResult.getResult().iterator();
				debugLog("SearchResult fetching: got child elements");
				while (iter.hasNext()) {
					final Object obj = iter.next();

					if (obj instanceof ResultType) {
						final ObjectType oType = ((ResultType) obj).getObject();
						if (oType == null) continue;
						try {
							if (oType.getGeneric().getProvided() != null) {
								final TextGridObject tgo = TextGridObject.getInstance(oType, true, true);
								collector.add(tgo, monitor);
							} else {
								final RestrictedTextGridObject rtgo = new RestrictedTextGridObject(oType.getGeneric().getGenerated().getTextgridUri().getValue());
								collector.add(rtgo, monitor);
							}
						} catch (final CoreException e) {
							 final IStatus status = new Status(
									 IStatus.ERROR,
									 Activator.PLUGIN_ID,
									 "Error while preparing search result.",
									 e);
							 Activator.getDefault().getLog().log(status);
						}
					}
				}

				// no matches found?
				if (noItems) {
					collector.add(new NoMatchEntry(), monitor);
				}

				collector.done();
				monitor.done();
				debugLog("SearchResult fetching: done");
			} else {
				throw new IllegalArgumentException(
						MessageFormat
								.format(
										"SearchResult''s fetchDeferredChildren does not know the children of {0}s like {1}",
										object.getClass(), object));
			}
		}
	}

	@Override
	public boolean isContainer() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ISchedulingRule getRule(final Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setHitCountAvailable(final boolean flag) {
		hitCountAvailable = flag;
		final Event e = new Event();
		e.doit = flag;
		notifyHitCountListeners(e);
	}

	public void addHitCountListener(final Listener l) {
		hitCountListeners.add(l);
	}

	public void removeHitCountListener(final Listener l) {
		hitCountListeners.remove(l);
	}

	protected void notifyHitCountListeners(final Event event) {
		final Object[] listeners = hitCountListeners.getListeners();
		for (final Object listener : listeners) {
			((Listener) listener).handleEvent(event);
		}
	}

	public void setPartSearchReady() {
		final Event e = new Event();
		e.doit = true;
		notifyPartSearchReadyListeners(e);
	}

	public void addPartSearchReadyListener(final Listener l) {
		partSearchReadyListeners.add(l);
	}

	public void removePartSearchReadyListener(final Listener l) {
		partSearchReadyListeners.remove(l);
	}

	protected void notifyPartSearchReadyListeners(final Event event) {
		final Object[] listeners = partSearchReadyListeners.getListeners();
		for (final Object listener : listeners) {
			((Listener) listener).handleEvent(event);
		}
	}

	public int getHitCount() {
		return this.hitCount;
	}

	/**
	 * Checks whether the delivered uri represents the
	 * latest revision of the TG object.
	 * @param uri
	 * @return
	 */
	public static boolean isTgoLatestRevision(final String uri) {
		final SearchRequest searchRequest = new SearchRequest();
		final Revisions revisions = searchRequest.getSearchClient().listRevisions(uri);
		final List<BigInteger> revisionsList = revisions.getRevision();
		if (!revisionsList.isEmpty()) {
			final BigInteger thisRevision = new BigInteger(uri.split("\\.")[1]);
			BigInteger lastRevision = Collections.max(revisionsList);
			return lastRevision.equals(thisRevision);
		}
		return false;
	}

	/**
	 * Masks special characters in the delivered query string.
	 * The function should be called before a search request
	 * is submitted.
	 * @param query
	 * @return query string with masked characters
	 */
	public static String maskSpecialCharacters(final String query) {
		final String result = query.replace("+", "\\+");
		return result;
	}

	public void setTarget(final TargetModus target) {
		this.target = target;
	}

	public void setWordDistance(final int wordDistance) {
		this.wordDistance = wordDistance;
	}

	public void setResolvePath(final boolean value) {
		this.isResolvePath = value;
	}

	public void setAllProjects(final boolean allProjects) {
		this.allProjects = allProjects;
	}

	/**
	 * If true, also look for world-readable objects in projects where our user
	 * doesn't have any permissions. Downside is that it is slower especially
	 * for large result sets.
	 */
	public boolean isAllProjects() {
		return allProjects;
	}

	/**
	 * Function delivers a map of all objects in
	 * an aggregation.
	 * @param uri
	 * 			aggregations uri
	 * @param aggregationURIs
	 * 			map of objects (represented by uri)
	 * @return
	 */
	public HashMap<String,HashMap<String,Integer>> gatherAggregationURIs(final String uri, HashMap<String,HashMap<String,Integer>> aggregationURIs ) {

		HashMap<String,Integer> titleCount;

		final Response agg = searchClient.listAggregation(uri);

		for (final ResultType res : agg.getResult()) {
			final String aggUri = res.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
			final String title = res.getObject().getGeneric().getProvided().getTitle().get(0);
			final String format = res.getObject().getGeneric().getProvided().getFormat();
			int count;
			if (aggregationURIs.containsKey(aggUri)) {
				titleCount = aggregationURIs.get(aggUri);
				count = titleCount.get(title);
				count++;
				titleCount.put(title, count);
			} else {
				titleCount = new HashMap<String,Integer>();
				titleCount.put(title, 1);
				aggregationURIs.put(aggUri, titleCount);
			}
			if (format.contains("aggregation")) {
				aggregationURIs = gatherAggregationURIs(aggUri, aggregationURIs);
			}
		}
		return aggregationURIs;
	}


	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder("query: ");
		if (isTextQuery) result.append(' ').append(queryText);
		if (isMetadataQuery) result.append(' ').append(queryMetadata);
		return result.toString();
	}
}

