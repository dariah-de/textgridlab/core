package info.textgrid.lab.ui.core;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.handlers.DefaultOpenHandler;
import info.textgrid.lab.ui.core.handlers.DefaultOpenHandlerTest;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.junit.Test;

/**
 * More tests covering {@link AbstractOpenHandler} functionality are in 
 * {@link DefaultOpenHandlerTest}. 
 */
public class AbstractOpenHandlerTest {

	/**
	 * Test the editor-not-specified case (editor can be found via eclipse content type)
	 * @throws CoreException 
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testOpenEditorTextGridObject() throws CoreException {
		TextGridObject object = TextGridObject.getNewObjectInstance("TGPR42", "example/test");
		OpenHandlerContribution contribution = OpenHandlerContribution.contributionsFor(object.getContentType(true)).get(1);
		assertNull("Editor unexpectedly specified in contribution -- probably this test is broken", contribution.getEditor());
	
		IOpenHandler handler = contribution.getHandler();
		handler.open(object);
		
		IEditorPart editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findEditor(AdapterUtils.getAdapter(object, IFileEditorInput.class));
		assertNotNull("Editor has not been opened :-(", editor);
	}

	@SuppressWarnings("deprecation")
	@Test(expected=NoEditorFoundException.class)
	public void testOpenEditorStrangeObject() throws CoreException {
		TextGridObject object = TextGridObject.getNewObjectInstance("TGPR42", "example/test.binary");
		OpenHandlerContribution contribution = OpenHandlerContribution.bestContributionFor(object.getContentType(true));
		assertNotNull("No contribution found", contribution);
		
		DefaultOpenHandler handler = new DefaultOpenHandler(contribution);
		handler.open(object);
	}
	
}
