package info.textgrid.lab.ui.core;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.ui.core.test.ExampleTestIIHandler;

import java.util.List;

import org.junit.Test;

/**
 * These tests depend on correct contributions in the test plugin's plugin.xml.
 */
public class OpenHandlerContributionTest {

	@Test
	public void testContributionsFor() {
		TGContentType testContentType = TGContentType.getContentType("example/test");
		List<OpenHandlerContribution> contributions = OpenHandlerContribution.contributionsFor(testContentType);
		assertEquals(2, contributions.size());
	}

	@Test
	public void testBestContributionFor() {
		TGContentType testContentType = TGContentType.getContentType("example/test");
		OpenHandlerContribution contribution = OpenHandlerContribution.bestContributionFor(testContentType);
		assertEquals("Open Example Test", contribution.getLabel());
	}

	/**
	 * just one example for a property getter -- should return the correct value if specified and null otherwise
	 */
	@Test
	public void testGetPerspective() {
		TGContentType testContentType = TGContentType.getContentType("example/test");
		List<OpenHandlerContribution> contributions = OpenHandlerContribution.contributionsFor(testContentType);
		assertEquals("info.textgrid.lab.welcome.XMLEditorPerspective", contributions.get(0).getPerspective());
		assertEquals(null, contributions.get(1).getPerspective());
	}

	@Test
	public void testGetHandler() {
		TGContentType testContentType = TGContentType.getContentType("example/test");
		List<OpenHandlerContribution> contributions = OpenHandlerContribution.contributionsFor(testContentType);
		assertNull(contributions.get(0).getHandler());
		assertNotNull(contributions.get(1).getHandler());
		assertTrue(contributions.get(1).getHandler() instanceof ExampleTestIIHandler);
	}

}
