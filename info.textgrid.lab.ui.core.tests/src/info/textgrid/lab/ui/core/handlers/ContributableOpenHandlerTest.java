package info.textgrid.lab.ui.core.handlers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.junit.Test;

public class ContributableOpenHandlerTest {

	@SuppressWarnings("deprecation")
	@Test
	public void testOpenObject() throws CoreException {
		TextGridObject object = TextGridObject.getNewObjectInstance("TGPR42", "example/test");
		ContributableOpenHandler.openObject(object);
		IEditorPart editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findEditor(AdapterUtils.getAdapter(object, IFileEditorInput.class));
		assertNotNull(editor);
		assertTrue(editor instanceof ITextEditor);
	}

}
