package info.textgrid.lab.ui.core.handlers;

import static org.junit.Assert.assertEquals;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.OpenHandlerContribution;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.PlatformUI;
import org.junit.Before;
import org.junit.Test;

public class DefaultOpenHandlerTest {
	
	private TextGridObject object;
	private OpenHandlerContribution contribution;
	private DefaultOpenHandler handler;
	

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws CoreException {
		object = TextGridObject.getNewObjectInstance("TGPR42", "example/test");
		contribution = OpenHandlerContribution.bestContributionFor(object.getContentType(false));
		handler = new DefaultOpenHandler(contribution);
	}
	
	@Test 
	public void testOpenHandler() {
		DefaultOpenHandler openHandler = new DefaultOpenHandler(contribution);
		assertEquals(contribution, openHandler.getContribution());
	}
	
	@Test
	public void testGetContribution() {
		OpenHandlerContribution actualContribution = handler.getContribution();
		assertEquals(contribution, actualContribution);
	}

	@Test
	public void testOpen() throws CoreException {
		handler.open(object);
		String currentPerspectiveID = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getPerspective().getId();
		assertEquals(contribution.getPerspective(), currentPerspectiveID);
		
	}


}
