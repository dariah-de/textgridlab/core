package info.textgrid.lab.ui.core.test;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.AbstractOpenHandler;
import info.textgrid.lab.ui.core.IOpenHandler;

import org.eclipse.core.runtime.CoreException;

public class ExampleTestIIHandler extends AbstractOpenHandler implements
		IOpenHandler {

	public ExampleTestIIHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void open(TextGridObject textGridObject) throws CoreException {
		showPerspective();
		openEditor(textGridObject);
		System.out.println("ExampleOpenHandler called");
	}

}
