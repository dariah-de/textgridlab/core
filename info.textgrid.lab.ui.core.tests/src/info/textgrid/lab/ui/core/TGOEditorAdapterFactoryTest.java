package info.textgrid.lab.ui.core;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IFileEditorInput;
import org.junit.Test;

public class TGOEditorAdapterFactoryTest {
	
	@Test
	public void testGetAdapter() {
		TGOEditorAdapterFactory factory = new TGOEditorAdapterFactory();
		TextGridObject object = TextGridObject.getNewObjectInstance("TGPR42", "text/xml");
		IFileEditorInput input = (IFileEditorInput) factory.getAdapter(object, IFileEditorInput.class);
		assertNotNull("TGO.getAdapter returned null for IFileEditorInput.class!", input);
		assertTrue("TGO.getAdapter's result is not an IFileEditorInput, but a " + input.getClass().getCanonicalName(), input instanceof IFileEditorInput);
	}
	

	@Test
	public void testAdapterFramework() {
		TextGridObject object = TextGridObject.getNewObjectInstance("TGPR42", "text/xml");
		Object adapter = object.getAdapter(IFileEditorInput.class);
		assertNotNull(adapter);
		IFile file = ((IFileEditorInput) adapter).getFile();
		assertNotNull(file);
		TextGridObject object2 = (TextGridObject) file.getAdapter(TextGridObject.class);
		assertSame(object2, object);
	}

}
