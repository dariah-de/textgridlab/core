package info.textgrid.lab.core.swtutils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import info.textgrid.lab.core.swtutils.IntervalMultimap.Interval;
import info.textgrid.lab.core.swtutils.IntervalMultimap.OverlapsComparator;

import org.junit.Test;

import com.google.common.base.Joiner;

public class IntervalMultimapTest {
	
	@Test
	public void testIntervalCreation() {
		new IntervalMultimap.Interval(0, 0);
		new IntervalMultimap.Interval(0, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testItervalCreationInvalid() {
		new IntervalMultimap.Interval(2, 0);
	}

	@Test
	public void testIntervalOverlaps() {
		Interval i13 = new Interval(1, 3);
		Interval i23 = new Interval(2, 3);
		Interval i34 = new Interval(3, 4);
		Interval i45 = new Interval(4, 5);
		Interval i15 = new Interval(1, 5);

		assertTrue(i13.overlaps(i23));
		assertTrue(i23.overlaps(i13));
		assertFalse(i13.overlaps(i34));
		assertFalse(i34.overlaps(i13));
		assertTrue(i13.overlaps(i13));
		assertTrue(i15.overlaps(i45));
		assertTrue(i45.overlaps(i15));
		assertTrue(i15.overlaps(i15));
		assertTrue(i15.overlaps(i45));
		assertFalse(i13.overlaps(i45));
		assertFalse(i45.overlaps(i13));
	}

	@Test
	public void testIntervalCompareTo() {
		Interval i13 = new Interval(1, 3);
		Interval i23 = new Interval(2, 3);
		Interval i45 = new Interval(4, 5);
		assertTrue("! [1, 3] < [2, 3]", i13.compareTo(i23) < 0);
		assertTrue("! [1, 3] < [4, 5]", i13.compareTo(i45) < 0);
		assertTrue("! [4, 5] < [1, 3]", i45.compareTo(i13) > 0);
		assertTrue("! [1, 3] == [1, 3]", i13.compareTo(i13) == 0);
	}

	@Test
	public void testOverlapsComparator() {
		OverlapsComparator comparator = new IntervalMultimap.OverlapsComparator();
		Interval i13 = new Interval(1, 3);
		Interval i23 = new Interval(2, 3);
		Interval i45 = new Interval(4, 5);
		assertEquals(0, comparator.compare(i13, i23));
		assertEquals(0, comparator.compare(i23, i13));
		assertTrue(comparator.compare(i13, i45) < 0);
	}

	@Test
	public void testFindMinimalIntervals() {
		IntervalMultimap<String> map = new IntervalMultimap<String>();
		map.add(new Interval(2, 3), "2 bis 3");
		map.add(new Interval(1, 1), "1 bis 1");
		map.add(new Interval(1, 3), "1 bis 3");
		map.add(new Interval(2, 2), "[2 bis 2]");
		System.out.println(map);
		Iterable<Interval> intervals = map.findIntervals(new Interval(2));
		assertEquals("[1,3), [2,2), [2,3)", Joiner.on(", ").join(intervals));
		assertEquals("[1,1), [1,3]", Joiner.on(", ").join(
				map.findIntervals(new Interval(1))));
	}
}
