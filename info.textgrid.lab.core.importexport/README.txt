== Tasks ==

=== Concepts ===

==== Link Rewriting ====

when importing a file file.xml that contains a link to file otherfile.xml to
textgrid, the link to othefile.xml inside file.xml should be rewritten to
address otherfile.xml's TextGrid URI (e.g.:textgrid:15b3), not its file name.
As in some cases the file name (e.g., 'otherfile.xml') might occur some different use that shall not be rewritten, we offer various rewrite methods.

=== Import ===

Task: Import a set of files into TextGrid that have never been there.
* Select one or more files on the local file system
* if > 1 file, link rewriting may occur (links between several files in the import set get rewritten to the respective textgrid URIs)
** configure rewriting heuristic to use for each file to import (default: suitable method is auto detected)
** enter metadata for each object
*** only (first) title and content type
*** 'apply to selected' / 'apply to all' available (probably)
**** variables ${filename}, ${fileext}, ${dirname}, ${n} can be used
** objects which have a valid .meta file: Metadata is read from the .meta file (e.g., foo.xml.meta for foo.xml)
* Import: Also writes mapping spec to TextGridRep (optional?) that contains list of files imported, original file name and rewriting settings

=== Export ===

* Select set of TGOs
* we will recurse into aggregations and also export works, we automatically create .meta files
* Link rewriting as with import


=== Re-Import ===

Task: Export a set of objects from TextGrid (see Export), work on them with external tools, re-import the updated versions into TextGrid

* Let the export write a mapping spec
* open the mapping spec in the import wizard
* finish will update (overwrite) the modified objects from the local hard disk
* Rewriting as in Import

=== Re-Export ===

Task: Like Re-Import, but start with files on your local hard disk

* Let the import write a mapping spec
* To export, open the mapping spec in the export wizard & specify a target directory
* finish will export the objects to the local hard disk according to the mapping spec

== UI ==

=== Import / Export  ===

Basically a list/tree/table of files (import) / TGOs (export) which you can modify using drag & drop & direct manipulation. To add files (import) / TGOs (export) drop them from local file manager (explorer, finder, nautilus …) or from the navigator / search results view (export) to the list. You can manipulate the following fields directly in the list:

* Import:
** title
** content type
** more metadata by selecting the entry in the list and modifying its metadata in the metadata view.
* Export:
** file name
* Both:
** rewrite method

We try to guess reasonable defaults.

It is possible to read/write the contents of the UI to a import/export mapping specification (TGO (or file?)).
