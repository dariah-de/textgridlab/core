package info.textgrid.lab.core.importexport;

import info.textgrid._import.ImportObject;
import info.textgrid.lab.core.model.TextGridObject;

import java.net.URI;

import org.eclipse.core.runtime.IAdapterFactory;

public class ImportObjectAdaptor implements IAdapterFactory {


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if ((adaptableObject instanceof ImportObject) && adapterType.isAssignableFrom(TextGridObject.class)) {
			return TextGridObject.getInstanceOffline(URI.create(((ImportObject) adaptableObject).getTextgridUri()));
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	// as the interface dictates ...
	public Class[] getAdapterList() {
		return new Class[] { TextGridObject.class };
	}

}
