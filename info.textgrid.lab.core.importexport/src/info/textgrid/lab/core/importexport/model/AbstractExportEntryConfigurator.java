package info.textgrid.lab.core.importexport.model;

import info.textgrid.lab.core.importexport.ImportPlugin;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public abstract class AbstractExportEntryConfigurator implements IExportEntryConfigurator {

	private static List<IExportEntryConfigurator> configurators;

	protected static List<IExportEntryConfigurator> getConfigurators() {
		if (configurators == null) {
			Builder<IExportEntryConfigurator> builder = ImmutableList.builder();
			IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
					ImportPlugin.PLUGIN_ID, "exportEntryConfigurator"); //$NON-NLS-1$
			for (IConfigurationElement element : configurationElements) {
				try {
					Object configurator = element.createExecutableExtension("class"); //$NON-NLS-1$
					if (configurator != null && configurator instanceof IImportEntryConfigurator)
						builder.add((IExportEntryConfigurator) configurator);
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
				}
			}
			configurators = builder.build();
		}
		return configurators;
	}
}
