package info.textgrid.lab.core.importexport.model;

import info.textgrid._import.ImportSpec;
import info.textgrid._import.RewriteMethod;
import info.textgrid._import.XmlConfiguration;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.eclipse.osgi.util.NLS;

import com.google.common.collect.Maps;

public class RewriteSetup {

	private static final RewriteSetup NONE = new RewriteSetup(RewriteMethod.NONE);
	private RewriteMethod method;
	private XmlConfiguration configuration;
	private URI uri;
	private static LinkedHashMap<URI, RewriteSetup> internalSetups;

	protected RewriteMethod getMethod() {
		return method;
	}

	protected XmlConfiguration getConfiguration() {
		return configuration;
	}

	protected URI getUri() {
		return uri;
	}

	public RewriteSetup(URI uri, XmlConfiguration configuration) {
		this.uri = uri;
		this.configuration = configuration;
		this.method = RewriteMethod.XML;
	}

	protected RewriteSetup(RewriteMethod method) {
		this.method = method;
	}

	public RewriteSetup(final ImportSpec spec, final URI uri) {
		this.method = RewriteMethod.XML;
		this.uri = uri;
	}

	public static LinkedHashMap<URI, RewriteSetup> getInternal() {
		if (internalSetups == null) {
			internalSetups = Maps.newLinkedHashMap();
			internalSetups.put(URI.create("none"), NONE); //$NON-NLS-1$
			for (Entry<URI, XmlConfiguration> entry : ConfigurableXMLRewriter.getInternalConfigurations().entrySet()) {
				internalSetups.put(entry.getKey(), new RewriteSetup(entry.getKey(), entry.getValue()));
			}
			// internalSetups.add(new RewriteSetup(RewriteMethod.TEXT));
		}
		return internalSetups;
	}

	public String toString() {
		switch (method) {
		case NONE:
			return Messages.RewriteSetup_Method_None;
		case TEXT:
			return Messages.RewriteSetup_Method_Plain_Text;
		case XML:
			return NLS.bind(Messages.RewriteSetup_Label_URI_Format, getConfiguration() == null ? Messages.RewriteSetup_Custom
					: getConfiguration().getDescription(), getUri());
		}
		return super.toString();
	}

	public static RewriteSetup of(RewriteMethod rewriteMethod, String rewriteConfig) {
		if (rewriteMethod == RewriteMethod.NONE)
			return NONE;
		if (rewriteMethod == RewriteMethod.XML) {
			try {
				URI uri = new URI(rewriteConfig);
				return getInternal().get(uri);
			} catch (URISyntaxException e) {
				throw new IllegalArgumentException(e);
			}
		}
		return null;
				
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RewriteSetup other = (RewriteSetup) obj;
		if (method != other.method)
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

}
