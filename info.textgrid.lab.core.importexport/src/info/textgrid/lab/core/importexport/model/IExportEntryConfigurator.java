package info.textgrid.lab.core.importexport.model;

import info.textgrid.lab.core.model.TextGridObject;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Implementors do object type specific things when an object is added to an
 * export set.
 * 
 * @see IImportEntryConfigurator
 */
public interface IExportEntryConfigurator {

	/**
	 * Configure the given export entry any further.
	 * 
	 * This method is called from
	 * {@link ImportModel#addObject(TextGridObject, IProgressMonitor)} after the
	 * entry has been basically setup. It does contain a {@link File} and a
	 * pre-configured {@link TextGridObject}. The TextGridObject already exists,
	 * and the file has been basically setup with its name, path and extension.
	 * 
	 * <p>
	 * Implementors are expected to check (as quick as possible) whether they
	 * are applicable and return immediately if not. If an implementor is
	 * applicable it should perform any further operations to pre-configure the
	 * entry and the object to export etc. This can also include adding other
	 * files.
	 * </p>
	 * 
	 * @param entry
	 *            the import entry that can optionally be modified.
	 * @param monitor
	 */
	public void configureExport(final ImportEntry entry, final IProgressMonitor monitor);

}
