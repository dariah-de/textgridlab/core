/**
 * This package adds TextGridLab specific behavior to the generic import
 * specification.
 * 
 * {@link info.textgrid.lab.core.importexport.model.ImportModel} is the generic
 * entry point, it models one import specification along with all the files
 * inside. This class also provides static methods helping in loading and saving
 * an import model. Classes in this package do not directly interact with the 
 * user interface.
 */
package info.textgrid.lab.core.importexport.model;

