package info.textgrid.lab.core.importexport.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Helper supporting the import of non-file things.
 * 
 * If the thing you want to import isn't really a file but rather something else
 * that can deliver an input stream, or if your file needs further preprocessing
 * before it will be imported, you can configure a supplier with the import
 * entry that provides access.
 * 
 * @author tv
 * 
 */
public interface ISpecialImportEntrySupplier {

	/**
	 * Returns an import stream from the special to import.
	 */
	public InputStream getInputStream() throws IOException;

	/**
	 * Delivers a (pseudo-) file representing the entry.
	 */
	public File getFile();

}