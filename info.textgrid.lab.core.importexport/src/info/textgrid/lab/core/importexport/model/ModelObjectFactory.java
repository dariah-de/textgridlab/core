package info.textgrid.lab.core.importexport.model;

import info.textgrid._import.ImportObject;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.ObjectFactory;

/**
 * This object factory is a drop-in replacement for {@link ObjectFactory} that
 * creates model objects instead. See <a
 * href="https://jaxb.dev.java.net/guide/Adding_behaviors.html"
 * >https://jaxb.dev.java.net/guide/Adding_behaviors.html</a>.
 * 
 * @author vitt
 * 
 */
public class ModelObjectFactory extends ObjectFactory {

	/**
	 * Creates a new {@link ImportEntry} instead.
	 * 
	 * @see info.textgrid._import.ObjectFactory#createImportObject()
	 */
	@Override
	public ImportObject createImportObject() {
		return new ImportEntry();
	}

	/**
	 * Creates a new {@link ImportModel} instead.
	 * 
	 * @see info.textgrid._import.ObjectFactory#createImportSpec()
	 */
	@Override
	public ImportSpec createImportSpec() {
		return new ImportModel();
	}

}
