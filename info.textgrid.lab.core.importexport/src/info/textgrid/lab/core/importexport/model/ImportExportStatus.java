package info.textgrid.lab.core.importexport.model;

import info.textgrid.lab.core.importexport.ImportPlugin;

import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

/**
 * A status
 * 
 * @author vitt
 * 
 */
public class ImportExportStatus extends Status {

	private ImportEntry entry;
	private Operation operation;

	public final Operation getOperation() {
		return operation;
	}

	public enum Operation {
		IMPORT, EXPORT
	};

	public ImportEntry getEntry() {
		return entry;
	}

	public ImportExportStatus(int severity, ImportEntry entry, Operation operation, String message, Throwable exception) {
		super(severity, ImportPlugin.PLUGIN_ID, message == null ? (exception == null ? NLS.bind(
				Messages.ImportExportStatus_NoDetailsAvailable, entry) : exception.getLocalizedMessage()) : message, exception);
		this.entry = entry;
		this.operation = operation;
	}

}
