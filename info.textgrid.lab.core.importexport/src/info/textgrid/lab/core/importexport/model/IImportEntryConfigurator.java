package info.textgrid.lab.core.importexport.model;

import info.textgrid.lab.core.model.TextGridObject;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Implementors do file type specific stuff for a given import enty.
 * 
 * @author vitt
 * 
 */
public interface IImportEntryConfigurator {

	/**
	 * Configure the given import entry any further.
	 * 
	 * This method is called from
	 * {@link ImportModel#addFile(java.io.File, boolean)} after the entry has
	 * been basically setup. It does contain a {@link File} and a pre-configured
	 * {@link TextGridObject}. The TextGridObject has been setup with a content
	 * type and a guessed title field.
	 * 
	 * <p>
	 * If we have found an existing metadata file for the entry, the
	 * {@linkplain ImportEntry#getLocalMetadata() import entry's local metadata
	 * field} will be non-<code>null</code> and the metadata will already have
	 * been loaded to the {@link TextGridObject}.
	 * </p>
	 * 
	 * <p>
	 * Implementors are expected to check (as quick as possible) whether they
	 * are applicable and return immediately if not. If an implementor is
	 * applicable it should perform any further operations to pre-configure the
	 * object to import etc. This can also include adding other files.
	 * </p>
	 * 
	 * @param entry
	 *            the import entry that can optionally be modified.
	 * @param monitor
	 */
	public void configureImport(final ImportEntry entry, final IProgressMonitor monitor);

}
