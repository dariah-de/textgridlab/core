package info.textgrid.lab.core.importexport.model;

import info.textgrid.lab.core.importexport.ImportPlugin;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public abstract class AbstractImportEntryConfigurator implements IImportEntryConfigurator {

	private final static class AskJob extends UIJob {
		private final String[] dialogButtonLabels;
		private final int defaultButton;
		private final String dialogMessage;
		private final String dialogTitle;
		private int result;
	
		private AskJob(String name, String[] dialogButtonLabels, int defaultButton, String dialogMessage, String dialogTitle) {
			super(name);
			this.dialogButtonLabels = dialogButtonLabels;
			this.defaultButton = defaultButton;
			this.dialogMessage = dialogMessage;
			this.dialogTitle = dialogTitle;
		}
	
		@Override
		public IStatus runInUIThread(final IProgressMonitor monitor) {
			final MessageDialog dialog = new MessageDialog(null, dialogTitle, null, dialogMessage, MessageDialog.QUESTION,
					dialogButtonLabels, defaultButton);
			result = dialog.open();
			return Status.OK_STATUS;
		}
	
		public int getDialogResult() {
			return result;
		}
	
	}

	private static List<IImportEntryConfigurator> configurators;

	public static List<IImportEntryConfigurator> getConfigurators() {
		if (configurators == null) {
			Builder<IImportEntryConfigurator> builder = ImmutableList.builder();
			IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
					ImportPlugin.PLUGIN_ID, "importEntryConfigurator"); //$NON-NLS-1$
			for (IConfigurationElement element : configurationElements) {
				try {
					Object configurator = element.createExecutableExtension("class"); //$NON-NLS-1$
					if (configurator != null && configurator instanceof IImportEntryConfigurator)
						builder.add((IImportEntryConfigurator) configurator);
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
				}
			}
			configurators = builder.build();
		}
		return configurators;
	}

	/**
	 * Displays a dialog to ask the user for something. This method works from
	 * both the UI thread or a background thread. In any case, it returns only
	 * after the user has closed the dialog.
	 * 
	 * @param dialogTitle
	 *            The title text of the dialog
	 * @param dialogMessage
	 *            The message to display in the dialog window.
	 * @param defaultButton
	 *            The index of the button from the following list of buttons
	 *            that should be the default (starts with 0)
	 * @param dialogButtonLabels
	 *            A label for each button to display in the dialog. Labels may
	 *            contain '&' to mark the acellerator character
	 * @return The index of the button that the user has pressed, or
	 *         {@link SWT#DEFAULT} ({@value SWT#DEFAULT}) if the user has
	 *         canceled the dialog.
	 */
	protected int ask(final String dialogTitle, final String dialogMessage, final int defaultButton, final String... dialogButtonLabels) {
	
		final AskJob job = new AskJob(dialogTitle, dialogButtonLabels, defaultButton, dialogMessage, dialogTitle);
	
		if (Display.getCurrent() != null) {
			job.runInUIThread(null);
			return job.getDialogResult();
		} else {
			job.schedule();
			try {
				job.join();
			} catch (InterruptedException e) {
				return SWT.DEFAULT;
			}
			return job.getDialogResult();
		}
	
	}
}
