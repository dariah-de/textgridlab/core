package info.textgrid.lab.core.importexport.model;

import info.textgrid._import.ImportObject;
import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.export.aggregations.IAggregation;
import info.textgrid.utils.export.aggregations.IAggregationEntry;

import java.io.File;
import java.net.URI;
import java.util.List;

import javax.xml.bind.JAXB;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * An entry in the import wizard.
 *
 * This is an extended version of the {@link ImportObject}, it is a child /
 * entry in an {@link ImportModel}. Clients should use {@link #getObjectRef()}
 * to access the {@link TextGridObject} imported by this entry and
 * {@link #getLocalFile()} to get access to the file.
 *
 * @author vitt
 *
 */
public class ImportEntry extends ImportObject implements IAggregationEntry, IAggregation {

	private TGObjectReference objectRef;
	private File file;
	private File metadataFile;
	private final List<ImportEntry> children = Lists.newArrayList();
	private ImportModel model;
	private RewriteSetup rewriteSetup;
	private ISpecialImportEntrySupplier supplier;
	private URI existingURI;
	private String existingProjectID;
	private Optional<ImportEntry> parent = Optional.absent();

	private boolean isInitialized = false;

	/**
	 * Returns the URI for the import object. If there is a prepared URI, that
	 * one is returned.
	 *
	 * @see info.textgrid._import.ImportObject#getTextgridUri()
	 */
	@Override
	public String getTextgridUri() {
		if (getObjectRef() == null && super.getTextgridUri() != null)
			setObjectRef(new TGObjectReference(super.getTextgridUri(),
					TextGridObject.getInstanceOffline(URI.create(super.getTextgridUri()))));
		if (getObjectRef() == null)
			return null;
		if (getObjectRef().getRefUri().startsWith(TextGridObject.SCHEMA.concat(":"))) //$NON-NLS-1$
			return getObjectRef().getRefUri();
		if (getObject().getPreparedURI() != null)
			return getObject().getPreparedURI().toString();
		else
			return getObject().getURI().toString();
	}

	public ImportEntry() {
		super();
	}

	/**
	 * Simple internal constructor that sets up this entry for importing a file.
	 * Does the guesswork and creates a new TextGridObject that is to be
	 * imported.
	 *
	 * TODO read .meta file, revision handling
	 *
	 * @param file
	 *            The file to import
	 * @param targetProject
	 *            preliminary target project
	 * @param supplier
	 *            optional supplier for special entries or <code>null</code>
	 * @param importModel
	 *            the model this entry will belong to
	 * @param monitor
	 *            progress reporting
	 * @see ImportModel#addFile(File, boolean, IProgressMonitor,
	 *      ISpecialImportEntrySupplier)
	 */
	ImportEntry(final File file, final TextGridProject targetProject, final ISpecialImportEntrySupplier supplier,
			final ImportModel importModel, final IProgressMonitor monitor) {
		this();
		final IProgressMonitor progress = monitor;

		// Basic setup
		setModel(importModel);
		setLocalData(file.getPath());
		setSupplier(supplier);

		// Create basic textgrid object
		TGContentType contentType = TGContentType.getByFilename(file.getPath());
		if (contentType == null)
			contentType = TGContentType.getContentType(TGContentType.UNKNOWN_ID);
		final TextGridObject textGridObject = TextGridObject.getNewObjectInstance(targetProject, contentType);
		progress.worked(20);

		// special handling for directories ,TODO handle this in a configurer
		if (file.isDirectory()) {
			textGridObject.setContentType(TGContentType.getContentType("text/tg.aggregation+xml")); //$NON-NLS-1$
			setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:textgrid#aggregation")); //$NON-NLS-1$
		}

		// Fallback: Initialize the title field from the file name.
		String title;
		final String name = file.getName();
		if (contentType.isUnknown())
			title = name;
		else
			title = FilenameUtils.removeExtension(name); // name.substring(0,
															// name.lastIndexOf('.'));
		if ("".equals(title)) //$NON-NLS-1$
			title = name;
		textGridObject.setTitle(title);

		// XXX check whether we want to refactor this?
		setLocalData(file.getPath());
		setTextgridUri(textGridObject.getURI().toString());

		// read provided metadata, if applicable. This will overwrite the
		// metadata guessed before.
		final File metaFile = new File(file.getPath().concat(".meta")); //$NON-NLS-1$
		tryReadMetafile(metaFile, progress, false);

		isInitialized = true;

		progress.worked(10);
	}

	/**
	 * Tries to read the given meta file for the current object. If successful,
	 * initialize the meta field, the reimport fields and so on.
	 *
	 * @param metaFile
	 *            The file from which to read
	 * @param progress
	 *            A monitor to report progress
	 * @param forceNewObject
	 *            if <code>true</code>, create a new {@link TextGridObject} with
	 *            a new URI after successfully reading the metadata -- this is
	 *            useful if you want to reimport existing files.
	 */
	private void tryReadMetafile(final File metaFile, final IProgressMonitor progress, final boolean forceNewObject) {
		TextGridObject textGridObject = getObject();
		if (metaFile.exists()) {
			final ObjectType metadata = ImportModel.readMetadata(metaFile);
			progress.worked(10);
			if (metadata != null && metadata.getGeneric() != null && metadata.getGeneric().getProvided() != null
					&& metadata.getGeneric().getProvided().getFormat() != null) {
				try {
					setExistingURI(URI.create(metadata.getGeneric().getGenerated().getTextgridUri().getValue()));
					setExistingProjectID(metadata.getGeneric().getGenerated().getProject().getId());
					if (forceNewObject) {
						textGridObject = TextGridObject.getNewObjectInstance(
								existingProjectID, metadata.getGeneric()
										.getProvided().getFormat());
						setObjectRef(new TGObjectReference(textGridObject
								.getURI().toString(), textGridObject));
					}
				} catch (final NullPointerException e) {/* ignored -- we just don't set the original URI */
				}
				textGridObject.setMetadata(ImportModel.clearGenerated(metadata));
				setLocalMetadata(metaFile.getPath());
			} else
				StatusManager.getManager().handle(
						new Status(IStatus.WARNING, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_Could_not_read_meta,
								metaFile.getName())), StatusManager.SHOW | StatusManager.LOG);
		}
	}

	public TextGridObject getObject() {
		final TGObjectReference ref = getObjectRef();
		if (ref == null)
			return null;
		return ref.getTgo();
	}

	@Override
	public void setTextgridUri(final String value) {
		final URI uri = URI.create(value);
		if (uri != null)
			setObjectRef(new TGObjectReference(value, TextGridObject.getInstanceOffline(uri)));
	}

	/**
	 * <p>
	 * Returns the file name for the local data file as it will be represented
	 * in the specification file. This file name is typically relative to the
	 * path returned by {@link ImportModel#getRoot()}.
	 * </p>
	 * <p>
	 * Clients that wish to access the actual file should use
	 * {@link #getLocalFile()} instead to avoid messing with relative path
	 * resolution.
	 * </p>
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 *
	 * @see info.textgrid._import.ImportObject#getLocalData()
	 */
	@Override
	public String getLocalData() {
		if (getFile() == null)
			if (super.getLocalData() == null)
				return null;
			else
				setLocalData(super.getLocalData()); // initializes file

		if (getModel() != null && getModel().getRoot() != null && getFile().isAbsolute())
			return new Path(getFile().getPath()).makeRelativeTo(getModel().getRoot()).toPortableString();

		return new Path(getFile().getPath()).toPortableString();
	}

	@Override
	public void setLocalData(final String value) {
		if (getModel() != null && getModel().getRoot() != null && !new File(value).isAbsolute())
			setFile(new File(getModel().getRoot().toFile(), value));
		else
			setFile(new File(value));
	}

	/**
	 * Adjusts the internal file object for an updated root folder.
	 * @param oldRoot the previous root.
	 */
	protected void adjustFile(final Optional<IPath> oldRoot) {
		if (getModel() != null && getModel().getRoot() != null) {
			final IPath relativePath;
			if (getFile().isAbsolute() && oldRoot.isPresent())
				relativePath = new Path(getFile().getPath()).makeRelativeTo(oldRoot.get());
			else
				relativePath = new Path(getFile().getPath());
			setFile(getModel().getRoot().append(relativePath).toFile());
		}
	}


	public void setObjectRef(final TGObjectReference object) {
		this.objectRef = object;
		// if (objectRef != null)
		 super.setTextgridUri(object.getRefUri());
	}

	public TGObjectReference getObjectRef() {
		return objectRef;
	}

	public void addChild(final ImportEntry importEntry) {
		children.add(importEntry);
		importEntry.parent = Optional.of(this);
	}

	public boolean mayHaveChildren() {
		return true;
	}

	public List<ImportEntry> getChildEntries() {
		return children;
	}

	void setModel(final ImportModel model) {
		this.model = model;
	}

	public ImportModel getModel() {
		return model;
	}

	public ImportEntry addFile(final File file, final IProgressMonitor monitor, final ISpecialImportEntrySupplier supplier) {
		if (model == null)
			throw new IllegalStateException("Cannot add files before associating a model."); //$NON-NLS-1$
		final ImportEntry entry = model.addFile(file, false, monitor, supplier);
		addChild(entry);
		return entry;
	}

	public ImportEntry addObject(final TGObjectReference textGridObject, final File parent, final IProgressMonitor monitor) {
		if (model == null)
			throw new IllegalStateException("Cannot add files before associating a model."); //$NON-NLS-1$
		final ImportEntry entry = model.addObject(textGridObject, false, parent, monitor);
		addChild(entry);
		return entry;
	}

	public RewriteSetup getRewriteSetup() {
		if (rewriteSetup == null || rewriteSetup.getMethod() != getRewriteMethod()
				|| (getRewriteConfig() != null && !rewriteSetup.getUri().equals(URI.create(getRewriteConfig())))) {
			rewriteSetup = RewriteSetup.of(getRewriteMethod(), getRewriteConfig());
			if (rewriteSetup == null)
				rewriteSetup = new RewriteSetup(getModel(), URI.create(getRewriteConfig()));
		}
		return rewriteSetup;
	}

	public void setRewriteSetup(final RewriteSetup setup) {
		rewriteSetup = setup;
		setRewriteMethod(setup.getMethod());
		if (setup.getUri() == null)
			setRewriteConfig(null);
		else
			setRewriteConfig(setup.getUri().toString());
	}

	public File getLocalFile() {
		return getFile();
	}

	void setFile(final File file) {
		this.file = file;
		super.setLocalData(getLocalData());
	}

	File getFile() {
		return file;
	}

	public File getLocalMetadataFile() {
		if (metadataFile == null) {

			final String md = getLocalMetadata();
			if (md == null)
				metadataFile = new File(getLocalFile().getPath()
						.concat(".meta")); //$NON-NLS-1$
			else {
				final File localMD = new File(md);
				if (!localMD.isAbsolute() && getModel() != null
						&& getModel().getRoot() != null)
					metadataFile = getModel().getRoot().append(md).toFile();
				else
					metadataFile = localMD;
			}
		}
		return metadataFile;
	}

	@Override
	public void setLocalMetadata(final String value) {
		metadataFile = null;
		super.setLocalMetadata(value);
	}

	public void setOriginalURI(final URI uri) {
	}

	public void setSupplier(final ISpecialImportEntrySupplier supplier) {
		this.supplier = supplier;
	}

	public ImportEntry addFile(final File child, final IProgressMonitor monitor) {
		return addFile(child, monitor, null);
	}

	public ISpecialImportEntrySupplier getSupplier() {
		return supplier;
	}

	@Override
	public String toString() {
		return String.format("ImportEntry [TextGrid URI = %s, Local Data = %s, Rewrite Setup = %s]", getTextgridUri(), //$NON-NLS-1$
				getLocalData(), getRewriteSetup());
	}

	/**
	 * This method sanitizes those values of the {@link ImportEntry} for which
	 * {@link ImportEntry} supplies special handling <em>and</em> which are
	 * serialized using {@link JAXB}. It is called internally before saving and
	 * after loading from JAXB just to make sure everything is up-to-date.
	 */
	protected void sanitize() {
		if (file == null && localData != null)
			setLocalData(localData);
		else if (file != null)
			localData = getLocalData();

		if (objectRef == null && textgridUri != null)
			setTextgridUri(textgridUri);
		else if (objectRef != null && textgridUri == null)
			textgridUri = getTextgridUri();
	}

	private void setExistingURI(final URI existingURI) {
		this.existingURI = existingURI;
	}

	/**
	 * Returns the URI of the URI if this is a reimport.
	 */
	private URI getExistingURI() {
		return existingURI;
	}

	/**
	 * If this is a reimport, return the existing object, else <code>null</code>
	 * . The existing object may not yet have been initialized.
	 */
	public TextGridObject getExistingObject() {
		if (isReimport())
			return TextGridObject.getInstanceOffline(getExistingURI());
		else
			return null;
	}

	/**
	 * Returns <code>true</code> if this file has previously been exported from
	 * the repository.
	 */
	public boolean isReimport() {
		return existingURI != null;
	}

	/**
	 * Returns <code>true</code> if this object will be re-imported as a new
	 * revision, i.e. {@linkplain #isReimport() it's a reimport},
	 * {@linkplain ImportModel#isImportAsNewRevisions() we should try to import
	 * as new revisions} and the original object has been exported from the
	 * current {@linkplain ImportModel#getTargetProject() target project}.
	 */
	public boolean isNewRevisionImport() {
		return isReimport() && model.importAsNewRevision() && model.getTargetProject().getId().equals(getExistingProjectID());
	}

	private void setExistingProjectID(final String existingProjectID) {
		this.existingProjectID = existingProjectID;
	}

	String getExistingProjectID() {
		return existingProjectID;
	}

	protected void initializeLoadedEntryForImport(final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor, 10);
		if (!isInitialized) {
			tryReadMetafile(getLocalMetadataFile(), progress.newChild(10), true);
			isInitialized = true;
		} else
			progress.worked(10);
		progress.done();

	}

	@Override
	public ImmutableList<? extends IAggregationEntry> getChildren() {
		return ImmutableList.copyOf(getChildEntries());
	}

	@Override
	public ObjectType getMetadata() {
		try {
			return getObject().getMetadataForReading();
		} catch (final CrudServiceException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public Optional<? extends IAggregation> getParent() {
		return parent;
	}

	@Override
	public URI getTextGridURI() {
		return URI.create(this.getTextgridUri());
	}

	@Override
	public Optional<? extends IAggregationEntry> getWork() {
		// TODO proper implementation when we start to use this.
		return Optional.absent();
	}
}
