package info.textgrid.lab.core.importexport.model;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.importexport.model.messages"; //$NON-NLS-1$
	public static String ImportExportStatus_NoDetailsAvailable;
	public static String ImportModel_Adding;
	public static String ImportModel_Adding_x;
	public static String ImportModel_Cancel;
	public static String ImportModel_Confirm_Dialog;
	public static String ImportModel_Could_not_read_meta;
	public static String ImportModel_dirty_metadata_on_import;
	public static String ImportModel_Error_Exporting;
	public static String ImportModel_Error_Exporting_Metadata;
	public static String ImportModel_Error_fetching_URIs;
	public static String ImportModel_Error_Importing_x;
	public static String ImportModel_Error_Rewriting_Metadata;
	public static String ImportModel_Error_x_while_y;
	public static String ImportModel_Export_complete;
	public static String ImportModel_ExportContentsSuccess;
	public static String ImportModel_Exporting_x_objects;
	public static String ImportModel_Exporting_x_to_y;
	public static String ImportModel_ExportMetadataSucess;
	public static String ImportModel_Failed_To_Create_Unmarshaller;
	public static String ImportModel_Import_Complete;
	public static String ImportModel_Imported_x_to_y;
	public static String ImportModel_Importing;
	public static String ImportModel_Importing_x;
	public static String ImportModel_invalid_fragment;
	public static String ImportModel_MissingReferenceX;
	public static String ImportModel_Object_instead_of_Metadata;
	public static String ImportModel_Overwrite;
	public static String ImportModel_Rewriting;
	public static String ImportModel_XML_Error_Rewriting_Export;
	public static String ImportModel_XML_Error_Rewriting_Import;
	public static String LegacyRewritingSupplier_FailedToRewriteLegacyFile;
	public static String RewriteSetup_Custom;
	public static String RewriteSetup_Label_URI_Format;
	public static String RewriteSetup_Method_None;
	public static String RewriteSetup_Method_Plain_Text;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
