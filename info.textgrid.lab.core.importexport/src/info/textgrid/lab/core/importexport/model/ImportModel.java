package info.textgrid.lab.core.importexport.model;

import info.textgrid._import.ImportObject;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.ObjectFactory;
import info.textgrid._import.RevisionPolicy;
import info.textgrid._import.RevisionPolicyType;
import info.textgrid._import.RewriteMethod;
import info.textgrid._import.XmlConfiguration;
import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ImportExportStatus.Operation;
import info.textgrid.lab.core.importexport.ui.OverwriteDialog;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.AdapterUtils.AdapterNotFoundException;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.export.filenames.DefaultFilenamePolicy;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.ws.WebServiceException;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.FileBackedOutputStream;

/**
 * <p>
 * Data model for the import / export wizard. This extends the basic ImportSpec
 * with TextGridLab specific stuff.
 * </p>
 *
 * <p>
 * You should use the {@link #load(Source)} method to load a spec. The model
 * maintains both a list of {@linkplain #getChildren() children} (which are to
 * be displayed at root level) and {@linkplain #getImportObject() entries}
 * (which are <em>somewhere</em> in the model). Use the
 * {@linkplain ImportEntry#addFile(File, IProgressMonitor) appropriate child's
 * addFile method} to add an entry to a child (and to this model).
 * </p>
 *
 * @author vitt
 *
 */
@XmlRootElement(name = "importSpec")
public class ImportModel extends ImportSpec {

	public interface ModelListener {
		public void rootChanged(final ImportModel model, final IPath oldRoot);
	}

	private final ListenerList listeners = new ListenerList();

	private TextGridProject targetProject;

	public TextGridProject getTargetProject() {
		return targetProject;
	}

	private IPath root;
	private final List<ImportEntry> children = Collections.synchronizedList(new ArrayList<ImportEntry>()); // for
	// direct
	// children
	private List<RewriteSetup> availableSetups;

	private boolean importAsNewRevisions;

	private MultiStatus lastResult;

	private List<ImportObject> nonSynchronizedImportObjects;

	private Set<File> files = null;

	public Set<File> getFiles() {
		if (files == null) {
			final HashSet<File> result = Sets.newHashSetWithExpectedSize(importObject.size());
			for (final ImportObject object : importObject)
				try {
					result.add(((ImportEntry) object).getLocalFile().getCanonicalFile());
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			files = result;
		}
		return files;
	}

	public ImportModel() {
		super();
		importObject = Collections.synchronizedList(new ArrayList<ImportObject>());
		if (revisionPolicy == null) {
			revisionPolicy = new RevisionPolicy();
			revisionPolicy.setImport(RevisionPolicyType.LATEST);
		}
	}

	public List<ImportEntry> getChildren() {
		return children;
	}

	public static ImportModel load(final Source source) throws JAXBException {
		final Unmarshaller unmarshaller = createUnmarshaller();
		final JAXBElement<ImportSpec> element = unmarshaller.unmarshal(source,
				ImportSpec.class);
		final ImportModel model = (ImportModel) element.getValue();
		if (source.getSystemId() != null) {
			final URI uri = URI.create(source.getSystemId());
			if (!uri.isAbsolute() || uri.getScheme().equals("file")) { //$NON-NLS-1$
				final File modelFile = new File(uri);
				if (modelFile.exists() && !modelFile.isDirectory())
					model.setRoot(new Path(modelFile.getParentFile()
							.getAbsolutePath()));
			}
		}
		model.sanitize();

		return model;
	}

	/**
	 * Creates a ready configured {@link Unmarshaller} that will unmarshal
	 * import specifications to lab-aware objects. Unmarshalling a complete
	 * import specification will, e.g., return an {@link ImportModel} object.
	 *
	 * @throws DataBindingException
	 *             with a wrapped {@link JAXBException} if configuring the
	 *             unmarshaller goes wrong (which would probably be a bug)
	 */
	public static Unmarshaller createUnmarshaller() throws DataBindingException {
		JAXBContext context;
		try {
			context = JAXBContext.newInstance(ObjectFactory.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();
			try {
				// for external JAXBs
				unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory", //$NON-NLS-1$
						new info.textgrid.lab.core.importexport.model.ModelObjectFactory());
			} catch (final PropertyException e) {
				// for the JAXB shipped with Java 6
				unmarshaller.setProperty("com.sun.xml.internal.bind.ObjectFactory", //$NON-NLS-1$
						new info.textgrid.lab.core.importexport.model.ModelObjectFactory());
			}
			return unmarshaller;
		} catch (final JAXBException e) {
			throw new DataBindingException(Messages.ImportModel_Failed_To_Create_Unmarshaller, e);
		}
	}

	public void save(final Result result) {
		sanitize();
		JAXB.marshal(new JAXBElement<ImportSpec>(new QName("http://textgrid.info/import", "importSpec"), ImportSpec.class, this), //$NON-NLS-1$ //$NON-NLS-2$
				result);
	}

	/**
	 * Adds a file to import to the current list of files. This creates an
	 * {@link ImportObject} and a {@link TextGridObject} and sets up metadata
	 * according to a heuristic.
	 *
	 * @param file
	 *            the file to add
	 * @param supplier
	 * @return the entry that has been added or modified
	 */
	public ImportEntry addFile(final File file, final IProgressMonitor monitor, final ISpecialImportEntrySupplier supplier) {
		return addFile(file, true, monitor, supplier);
	}

	/**
	 * Adds a file to import to the current list of files. This creates an
	 * {@link ImportObject} and a {@link TextGridObject} and sets up metadata
	 * according to a heuristic.
	 *
	 * @param file
	 *            the file to add
	 * @param global
	 *            if <code>true</code>, the file is added to the
	 *            {@linkplain #getChildren() list of direct children} for this
	 *            model.
	 * @param monitor
	 * @param supplier
	 *            a special entry supplier, or <code>null</code>.
	 * @return the entry that has been added or modified
	 */
	protected ImportEntry addFile(final File file, final boolean global, final IProgressMonitor monitor,
			final ISpecialImportEntrySupplier supplier) {
		final SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(Messages.ImportModel_Adding, file), 100);

		// find duplicate
		try {
			final File canonicalFile = file.getCanonicalFile();
			final ImportEntry duplicate = (ImportEntry) findEntry(new Predicate<ImportObject>() {

				@Override
				public boolean apply(final ImportObject o) {
					try {
						return canonicalFile.equals(((ImportEntry) o).getFile().getCanonicalFile());
					} catch (final IOException e) {
						e.printStackTrace();
						return false;
					}
				}

			});
			return duplicate;
		} catch (final NoSuchElementException e) { /* just go on */
			System.out.println("Didn't find a duplicate for " + file); //$NON-NLS-1$
		} catch (final IOException e) {
			e.printStackTrace();
		}

		// Try to guess content type from file ext
		final String name = file.getName();
		final String ext = name.substring(name.lastIndexOf('.') + 1);

		// First check: Is it a metadata file?
		if ("meta".equalsIgnoreCase(ext)) { //$NON-NLS-1$
			final ObjectType readMetadata = readMetadata(file);
			progress.worked(25);
			if (readMetadata != null) {
				final File candidate = new File(FilenameUtils.removeExtension(file.getPath()));
				try {
					final ImportEntry importEntry = (ImportEntry) Iterables.find(getImportObject(), new Predicate<ImportObject>() {

						@Override
						public boolean apply(final ImportObject importObject) {
							return candidate.equals(new File(importObject.getLocalData()));
						}
					});
					progress.worked(10);
					progress.done();

					return importEntry;
				} catch (final NoSuchElementException e) {
					progress.subTask(NLS.bind(Messages.ImportModel_Object_instead_of_Metadata, file, candidate));
					return addFile(candidate, global, progress, null);
				}
			}
		}
		// No, it is not.

		// First setup of the TGO and the Entry.
		final ImportEntry importEntry = new ImportEntry(file, targetProject, supplier, this, progress);
		final TextGridObject textGridObject = importEntry.getObject();

		updateRootPath(file);

		progress.worked(10);

		progress.worked(20);

		final List<IImportEntryConfigurator> configurators = AbstractImportEntryConfigurator.getConfigurators();
		progress.setWorkRemaining(100 * configurators.size());
		for (final IImportEntryConfigurator configurator : configurators)
			configurator.configureImport(importEntry, progress.newChild(100));

		// TG-1251 detection
		try {
			if ("".equals(importEntry.getObject().getTitle())) //$NON-NLS-1$
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, MessageFormat.format(
								"The import object {0}'s object, {1}, has an empty title.", importEntry, importEntry.getObject()), //$NON-NLS-1$
								new IllegalStateException()));
		} catch (final CoreException e) {
			StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
		}

		files = null;
		getImportObject().add(importEntry);
		if (global)
			children.add(importEntry);

		// moved to AggregationConfiguration etc.
		// if (file.isDirectory()) { // add all children
		// File[] files = file.listFiles();
		// if (files != null)
		// for (File direntry : files)
		// importEntry.addFile(direntry);
		// }

		progress.done();

		return importEntry;
	}

	public ImportObject findEntry(final Predicate<? super ImportObject> predicate) throws NoSuchElementException {
		final List<ImportObject> list = getImportObject();
		synchronized(list) {
			return Iterables.find(list, predicate);
		}
	}

	@Override
	public synchronized List<ImportObject> getImportObject() {
		if (nonSynchronizedImportObjects == null) {
			nonSynchronizedImportObjects = importObject;
			importObject = Collections.synchronizedList(nonSynchronizedImportObjects);
		}
		return importObject;
	}

	boolean mayOverwriteMetadata(final ImportEntry importEntry, final URI existingURI) {

		final UIJob askJob = new UIJob(Messages.ImportModel_Confirm_Dialog) {

			@Override
			public IStatus runInUIThread(final IProgressMonitor monitor) {
				final MessageDialog dialog = new MessageDialog(null, NLS.bind(Messages.ImportModel_Adding_x,
						importEntry.getLocalData()), null, NLS.bind(Messages.ImportModel_dirty_metadata_on_import,
						importEntry.getLocalData(), existingURI), MessageDialog.CONFIRM, new String[] {
						Messages.ImportModel_Overwrite, Messages.ImportModel_Cancel }, 0);
				final int result = dialog.open();
				return result == 0 ? Status.OK_STATUS : Status.CANCEL_STATUS;
			}
		};
		askJob.setSystem(true);
		askJob.schedule();
		try {
			askJob.join();
		} catch (final InterruptedException e) {
			return false;
		}
		return askJob.getResult().isOK();

	}

	/**
	 * Creates an entry for the given object and adds it to the model. This also
	 * triggers object specific initialization of the entry. It may trigger
	 * adding more entries.
	 *
	 * @param objectReference
	 *            A reference to the {@link TextGridObject} to be exported.
	 * @param global
	 *            if <code>true</code>, also add the object to the specific list
	 *            of children.
	 * @param parentFile
	 *            if non-<code>null</code>, the filename to export will be a
	 *            child of this filename.
	 * @param monitor
	 *            TODO
	 * @return the {@link ImportEntry} that has been added, or <code>null</code>
	 *         if no {@link ImportEntry} has been added for some reason.
	 */
	protected ImportEntry addObject(final TGObjectReference objectReference, final boolean global, final File parentFile,
			final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(Messages.ImportModel_Adding, objectReference), 100);

		// find duplicate
		try {
			final ImportEntry duplicate = (ImportEntry) findEntry(new Predicate<ImportObject>() {

				@Override
				public boolean apply(final ImportObject o) {
					final ImportEntry entry = (ImportEntry) o;
					return entry.getObjectRef().getRefUri().equals(objectReference.getRefUri())
							|| entry.getObject().getURI().equals(objectReference.getTgo().getURI());

				}

			});
			return duplicate;
		} catch (final NoSuchElementException e) {
			// No duplicate, go on ...
		}

		final ImportEntry entry = new ImportEntry();
		entry.setModel(this);
		entry.setObjectRef(objectReference);

		progress.worked(10);

		final URI guessedName = DefaultFilenamePolicy.INSTANCE.getFilename(entry, false);

		progress.worked(20);

//			final String fileName = uriPart + "." + extension; //$NON-NLS-1$
//			final File file = new File(parentFile, fileName);
		final File file = new File(parentFile, guessedName.toString());
		entry.setFile(file);

		final List<IExportEntryConfigurator> configurators = AbstractExportEntryConfigurator.getConfigurators();
		progress.setWorkRemaining(50 * configurators.size() + 5);
		for (final IExportEntryConfigurator configurator : configurators)
			configurator.configureExport(entry, progress.newChild(50));

		getImportObject().add(entry);

		if (global)
			children.add(entry);

		progress.done();
		return entry;
	}

	private void updateRootPath(final File file) {
		// update the root path. TODO factor this out of disply code?
		final IPath path = new Path(file.getAbsolutePath());
		IPath newRoot = root == null ? path.removeLastSegments(1) : root;

		if (newRoot != null)
			while (!newRoot.isPrefixOf(path))
				newRoot = newRoot.removeLastSegments(1);
		setRoot(newRoot);
	}

	public void setRoot(final IPath root) {
		final Optional<IPath> oldRoot = Optional.fromNullable(this.root);
		this.root = root;

		for (final ImportObject object : getImportObject()) {
			final ImportEntry entry = (ImportEntry) object;
			entry.adjustFile(oldRoot);
		}

		if (oldRoot.isPresent() || (root != null && !root.equals(oldRoot.orNull())))
			for (final Object rawListener : listeners.getListeners()) {
				final ModelListener listener = (ModelListener) rawListener;
				listener.rootChanged(this, oldRoot.orNull());
			}
	}

	/**
	 * Loads metadata from a file to the TGO. Leaves the TGO alone if the
	 * metadata could not be loaded.
	 *
	 * @param metaFile
	 *
	 * @return metadata object or null.
	 */
	static ObjectType readMetadata(final File metaFile) {
		if (metaFile.exists()) {
			final ObjectType metadata = JAXB.unmarshal(metaFile, ObjectType.class);
			if (metadata.getGeneric() != null)
				return metadata;
			else {
				final MetadataContainerType container = JAXB.unmarshal(metaFile, MetadataContainerType.class);
				if (container.getObject() != null && container.getObject().getGeneric() != null)
					return container.getObject();
			}
		}
		return null;
	}

	static ObjectType clearGenerated(final ObjectType metadata) {
		if (metadata != null && metadata.getGeneric() != null)
			metadata.getGeneric().setGenerated(null);
		return metadata;
	}

	public void setTargetProject(final TextGridProject project) {
		targetProject = project;
	}

	private IStatus rewriteMetadata(final ImportEntry entry, final ImportMapping mapping, final IProgressMonitor monitor) {
		// Rewrite metadata
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(mapping, false);
		rewriter.configure(URI.create("internal:textgrid#metadata")); //$NON-NLS-1$
		byte[] bytes;
		try {
			final TextGridObject textGridObject = entry.getObject();
			final String serialized = textGridObject.serialize();
			bytes = serialized.getBytes("UTF-8"); //$NON-NLS-1$
			final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length);

            final IPath parentPath = new Path(entry.getLocalData()).removeLastSegments(1);
            if (parentPath.segmentCount() > 0)
                rewriter.setBase(URI.create(parentPath.addTrailingSeparator().toString()));

			rewriter.rewrite(bais, baos);
			final Reader reader = new StringReader(baos.toString("UTF-8")); //$NON-NLS-1$
			final ObjectType newMetadata = JAXB.unmarshal(reader, ObjectType.class);
			textGridObject.setMetadata(newMetadata);
		} catch (final Exception e) {
			return new Status(IStatus.WARNING, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_Error_Rewriting_Metadata,
					entry.getLocalData()), e);
		}
		return Status.OK_STATUS;
	}

	public IStatus doImport(final TextGridProject project, final IProgressMonitor monitor) throws IllegalStateException {
		final SubMonitor progress = SubMonitor.convert(monitor, Messages.ImportModel_Importing, 20 + getImportObject().size() * 100);

		final MultiStatus result = new MultiStatus(ImportPlugin.PLUGIN_ID, 0, Messages.ImportModel_Import_Complete, null);
		final ImportMapping mapping = new ImportMapping(this);

		try {
			TextGridObject.fetchURIs(Iterables.transform(Iterables.filter(getImportObject(), new Predicate<ImportObject>() {

				@Override
				public boolean apply(final ImportObject input) {
					return !((ImportEntry) input).isNewRevisionImport();
				}
			}), new Function<ImportObject, TextGridObject>() {

				@Override
				public TextGridObject apply(final ImportObject arg0) {
					return ((ImportEntry) arg0).getObject();
				}
			}), progress.newChild(20));
		} catch (final CoreException e) {
			result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, Messages.ImportModel_Error_fetching_URIs, e));
		} catch (final WebServiceException e) {
			result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, Messages.ImportModel_Error_fetching_URIs, e));
		}

		for (final ImportObject importObject : getImportObject()) {
			if (progress.isCanceled()) {
				result.merge(Status.CANCEL_STATUS);
				StatusManager.getManager().handle(result, StatusManager.LOG);
				return result;
			}
			final ImportEntry entry = (ImportEntry) importObject;
//			System.err.println(MessageFormat.format("Importing to {0}; refURI={1}; objectURI={2}; preparedURI={3}", importObject.getTextgridUri(), entry.getObjectRef().getRefUri(), entry.getObjectRef().getTgo().getURI(),
//					entry.getObjectRef().getTgo().getPreparedURI()));
			progress.subTask(NLS.bind(Messages.ImportModel_Importing_x, entry.getLocalData()));
			TextGridObject textGridObject = entry.getObject();
			try {
				if (entry.isNewRevisionImport()) {
					// if it's a new revision import, we prepare a new revision
					// right now and set its metadata to the metadata of the
					// object
					// currently used as 'new' object, minus the generated
					// stuff.
					final TextGridObject newRevision = entry.getExistingObject().prepareNewRevision(progress.newChild(5));
					final ObjectType metadata = textGridObject.getMetadata();
					metadata.getGeneric().setGenerated(newRevision.getMetadataForReading().getGeneric().getGenerated());
					newRevision.setMetadata(metadata);
					textGridObject = newRevision;
				} else
					progress.worked(5);
				textGridObject.setProject(project);
				InputStream inputStream;
				inputStream = openInputStream(result, mapping, importObject, entry);
				final IFile textGridFile = AdapterUtils.getAdapterChecked(textGridObject, IFile.class);
				progress.worked(10);

				final IStatus metadataStatus = rewriteMetadata(entry, mapping, progress.newChild(10));
				if (!metadataStatus.isOK())
					result.add(metadataStatus);

				textGridFile.setContents(inputStream, IResource.FORCE, progress.newChild(80));
				importObject.setTextgridUri(textGridObject.getURI().toString());
				result.add(new ImportExportStatus(IStatus.OK, entry, ImportExportStatus.Operation.IMPORT, NLS.bind(
						Messages.ImportModel_Imported_x_to_y, importObject.getLocalData(), textGridObject), null));
			} catch (final FileNotFoundException e) {
				result.add(new ImportExportStatus(IStatus.ERROR, entry, ImportExportStatus.Operation.IMPORT, NLS.bind(
						Messages.ImportModel_Error_x_while_y, importObject.getLocalData(), e), e));
			} catch (final AdapterNotFoundException e) {
				result.add(new ImportExportStatus(IStatus.ERROR, entry, ImportExportStatus.Operation.IMPORT, NLS.bind(
						Messages.ImportModel_Error_x_while_y, importObject.getLocalData(), e), e));
			} catch (final CoreException e) {
				result.add(new ImportExportStatus(IStatus.ERROR, entry, ImportExportStatus.Operation.IMPORT, NLS.bind(
						Messages.ImportModel_Error_x_while_y, importObject.getLocalData(), e), e));
			} catch (final IOException e) {
				result.add(new ImportExportStatus(IStatus.ERROR, entry, ImportExportStatus.Operation.IMPORT, NLS.bind(
						Messages.ImportModel_Error_x_while_y, importObject.getLocalData(), e), e));
			} catch (final RuntimeException e) { // TODO TGEFSRuntimeError
													// specific handling
				result.add(new ImportExportStatus(IStatus.ERROR, entry, ImportExportStatus.Operation.IMPORT, NLS.bind(
						Messages.ImportModel_Error_x_while_y, importObject.getLocalData(), e), e));
			}
		}
		lastResult = result;
		StatusManager.getManager().handle(result);
		return result;
	}

	private static InputStream openInputStream(final MultiStatus result, final ImportMapping mapping,
			final ImportObject importObject, final ImportEntry entry) throws IOException, FileNotFoundException {
		InputStream inputStream = null;
		if (importObject.getRewriteMethod().equals(RewriteMethod.XML)) {

			final FileBackedOutputStream cache = new FileBackedOutputStream(1024*1024);
					InputStream localFileStream;
					try {
						try {
							if (entry.getSupplier() != null)
								localFileStream = entry.getSupplier().getInputStream();
							else
								localFileStream = new FileInputStream(entry.getLocalFile());
							final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(mapping, false);
							rewriter.configure(new URI(entry.getRewriteConfig()));
                            final IPath parentPath = new Path(entry.getLocalData()).removeLastSegments(1);
                            if (parentPath.segmentCount() > 0)
                                rewriter.setBase(URI.create(parentPath.addTrailingSeparator().toString()));
							rewriter.recordMissingReferences();
							try {
								rewriter.rewrite(localFileStream, cache);
								final Set<String> missingReferences = rewriter.getMissingReferences();
								if (missingReferences != null) {
									for (String ref : missingReferences) {
										URI uri = URI.create(ref);
										if (!uri.isAbsolute() || "file".equals(uri.getScheme())) //$NON-NLS-1$
											result.add(new ImportExportStatus(IStatus.WARNING, entry, Operation.IMPORT, NLS.bind(Messages.ImportModel_MissingReferenceX, uri), null));
									}
								}
								inputStream = cache.getSupplier().getInput();
							} catch (final XMLStreamException e) {
								// TG-1610
								result.add(new ImportExportStatus(IStatus.WARNING, entry, Operation.IMPORT, NLS.bind(
										Messages.ImportModel_XML_Error_Rewriting_Import, importObject.getLocalData(),
										e.getLocalizedMessage()), e));
							}
						} finally {
							cache.close();
						}
					} catch (final FileNotFoundException e) {
						result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
								Messages.ImportModel_Error_Importing_x, importObject.getLocalData(), e)));
					} catch (final IllegalArgumentException e) {
						result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
								Messages.ImportModel_Error_Importing_x, importObject.getLocalData(), e)));
					} catch (final URISyntaxException e) {
						result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
								Messages.ImportModel_Error_Importing_x, importObject.getLocalData(), e)));
					} catch (final IOException e) {
						result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
								Messages.ImportModel_Error_Importing_x, importObject.getLocalData(), e)));
					} catch (final Throwable e) {
						result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
								Messages.ImportModel_Error_Importing_x, importObject.getLocalData(), e)));
					}
		}
		if (inputStream == null)
			if (entry.getSupplier() != null)
				inputStream = entry.getSupplier().getInputStream();
			else
				inputStream = new FileInputStream(entry.getLocalFile());
		return inputStream;
	}

	public IStatus doExport(final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.ImportModel_Exporting_x_objects, getImportObject().size()), getImportObject().size() * 100 + 20);

		// TG-1145 Check for existing files
		if (!OverwriteDialog.mayContinue(this, progress.newChild(10)))
			return Status.CANCEL_STATUS;

		final MultiStatus result = new MultiStatus(ImportPlugin.PLUGIN_ID, 0, Messages.ImportModel_Export_complete, null);
		final ImportMapping mapping = new ImportMapping(this);
		final ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, true);
		metadataRewriter.configure(RewriteSetup.of(RewriteMethod.XML, "internal:textgrid#metadata").getConfiguration()); //$NON-NLS-1$

		progress.worked(10);

		for (final ImportObject importObject : getImportObject()) {
			if (progress.isCanceled()) {
				result.merge(Status.CANCEL_STATUS);
				StatusManager.getManager().handle(result, StatusManager.LOG);
				return result;
			}
			// 0. Initialization
			final ImportEntry entry = (ImportEntry) importObject;
			progress.subTask(NLS.bind(Messages.ImportModel_Exporting_x_to_y, entry.getObjectRef(), entry.getLocalFile()));
			final File localFile = resolve(entry.getLocalFile());

			// 1. create parent directories, if neccessary
			final File parent = localFile.getParentFile();
			if (parent != null && !parent.exists())
				parent.mkdirs();

			progress.worked(10);

			// 2. copy file contents. Two options:
			try {
				final IFile remoteFile = AdapterUtils.getAdapterChecked(entry.getObjectRef(), IFile.class);
				progress.worked(10);
				final InputStream remoteContents = remoteFile.getContents();
				final FileOutputStream localContents = new FileOutputStream(localFile);
				progress.worked(10);

				if (RewriteMethod.XML.equals(entry.getRewriteMethod())) {
					// 2a. rewriting using the configurable xml rewriter
					final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(mapping, true);
					rewriter.configure(entry.getRewriteSetup().getConfiguration());

                    final IPath parentPath = new Path(entry.getLocalData()).removeLastSegments(1);
                    if (parentPath.segmentCount() > 0)
                        rewriter.setBase(URI.create(parentPath.addTrailingSeparator().toString()));

					rewriter.rewrite(remoteContents, localContents);
					localContents.close();
					remoteContents.close();
					progress.worked(50);
				} else {
					// 2b. just plain copy, using java.nio Channels
					final ReadableByteChannel remoteChannel = Channels.newChannel(remoteContents);
					localContents.getChannel().transferFrom(remoteChannel, 0, Long.MAX_VALUE);
					localContents.close();
					remoteContents.close();
					result.add(new ImportExportStatus(IStatus.OK, entry, ImportExportStatus.Operation.EXPORT, NLS.bind(
							Messages.ImportModel_ExportContentsSuccess, entry.getObject(), localFile), null));
					progress.worked(50);
				}
			} catch (final AdapterNotFoundException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_Error_Exporting,
						entry.getObject()), e));
			} catch (final CoreException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_Error_Exporting,
						entry.getObject()), e));
			} catch (final FileNotFoundException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_Error_Exporting,
						entry.getObject()), e));
			} catch (final IOException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_Error_Exporting,
						entry.getObject()), e));
			} catch (final XMLStreamException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
						Messages.ImportModel_XML_Error_Rewriting_Export, entry.getObject(), e.getLocalizedMessage()), e));
			}

			// 3. save metadata, rewriting
			try {
				final String metadata = entry.getObject().serialize();
				progress.worked(10);
				final ByteArrayInputStream metadataInput = new ByteArrayInputStream(metadata.getBytes("UTF-8")); //$NON-NLS-1$
				final File localMetadataFile = resolve(entry.getLocalMetadataFile());
				IPath parentPath = new Path(localMetadataFile.getPath()).makeRelativeTo(getRoot()).removeLastSegments(1);
				if (parentPath.segmentCount() > 0)
					metadataRewriter.setBase(URI.create(parentPath.addTrailingSeparator().toString()));
				else 
					metadataRewriter.setBase(null);
				final FileOutputStream metadataOutput = new FileOutputStream(localMetadataFile);
				metadataRewriter.rewrite(metadataInput, metadataOutput);
				progress.worked(20);
				metadataOutput.close();
				result.add(new ImportExportStatus(IStatus.OK, entry, ImportExportStatus.Operation.EXPORT, NLS.bind(
						Messages.ImportModel_ExportMetadataSucess, entry.getObject(), localMetadataFile), null));
			} catch (final UnsupportedEncodingException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
						Messages.ImportModel_Error_Exporting_Metadata, entry.getObjectRef()), e));
			} catch (final FileNotFoundException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
						Messages.ImportModel_Error_Exporting_Metadata, entry.getObjectRef()), e));
			} catch (final IOException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
						Messages.ImportModel_Error_Exporting_Metadata, entry.getObjectRef()), e));
			} catch (final XMLStreamException e) {
				result.add(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
						Messages.ImportModel_Error_Exporting_Metadata, entry.getObjectRef()), e));
			}
		}
		return result;
	}

	/**
	 * Resolves the given file against the root, if applicable
	 *
	 * @param localFile
	 *            file to resolve
	 * @return either <var>localFile</var> or a file
	 */
	public File resolve(final File localFile) {
		if (getRoot() == null || localFile.isAbsolute())
			return localFile;
		else
			return getRoot().append(localFile.getPath()).toFile();

	}

	public List<RewriteSetup> getAvailableSetups() {
		if (availableSetups == null) {
			availableSetups = Lists.newArrayList();

			for (final XmlConfiguration config : getXmlConfiguration())
				try {
					availableSetups.add(new RewriteSetup(new URI(null, null, config.getId()), config));
				} catch (final URISyntaxException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportModel_invalid_fragment,
									config.getId())));
				}

			availableSetups.addAll(RewriteSetup.getInternal().values());
		}
		return availableSetups;
	}

	public IPath getRoot() {
		return root;
	}

	public void addListener(final ModelListener listener) {
		listeners.add(listener);
	}

	public void removeListener(final ModelListener listener) {
		listeners.remove(listener);
	}

	public ImportEntry addObject(final TGObjectReference object, final IProgressMonitor monitor) {
		return addObject(object, true, null, monitor);
	}

	/**
	 * If true, try to create new revisions
	 */
	public void setImportAsNewRevisions(final boolean reImport) {
		this.importAsNewRevisions = reImport;
	}

	/**
	 * Returns <code>true</code> if we try to create new revisions for
	 * reimported objects. Note that we only create new revisions for entries
	 * for which {@linkplain ImportEntry#isNewRevisionImport() all criteria are
	 * true}.
	 */
	public boolean isImportAsNewRevisions() {
		return importAsNewRevisions;
	}

	public void remove(final TreePath path) {
		getImportObject().remove(path.getLastSegment());
		final TreePath parentPath = path.getParentPath();
		if (parentPath != null && parentPath.getLastSegment() != null) {
			final Object parent = parentPath.getLastSegment();
			if (parent instanceof ImportEntry)
				((ImportEntry) parent).getChildEntries().remove(path.getLastSegment());
			else if (parent == this)
				getChildren().remove(path.getLastSegment());
			else
				throw new IllegalArgumentException(MessageFormat.format(
						"What is _this_: {0}? It should be a treePath from the model ...", path)); //$NON-NLS-1$

		} else
			getChildren().remove(path.getLastSegment());
	}

	public ImportEntry addFile(final File file, final IProgressMonitor monitor) {
		return addFile(file, monitor, null);
	}

	protected synchronized void sanitize() {
		final boolean fillChildren = getChildren().isEmpty();
		synchronized (getImportObject()) {
			for (final ImportObject object : getImportObject()) {
				final ImportEntry entry = (ImportEntry) object;
				entry.setModel(this);
				if (fillChildren)
					children.add(entry);
				entry.sanitize();
			}
		}
	}

	public void initializeLoadedModelForImport(final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor);
		progress.setWorkRemaining(getImportObject().size() * 10);
		for (final ImportObject io : getImportObject())
			((ImportEntry) io).initializeLoadedEntryForImport(progress.newChild(10));
		progress.done();
	}

	/**
	 * Returns true if files <em>may</em> be imported as new revisions.
	 */
	public boolean importAsNewRevision() {
		return importAsNewRevisions;
	}

	/**
	 * Returns a set of projects that are "reimport" projects, i.e. that the
	 * entry comes from.
	 */
	public Set<String> getReimportProjectIDs() {
		final LinkedHashSet<String> result = Sets.newLinkedHashSet();
		for (final ImportObject object : importObject) {
			final ImportEntry entry = (ImportEntry) object;
			if (entry.isReimport())
				result.add(entry.getExistingProjectID());
		}
		return result;
	}

	/**
	 * Checks whether all local files in this spec exist. Indicator for what we should suggest to the user.
	 */
	public LocalFileStatus checkLocalFiles(final IProgressMonitor monitor) {
		int relative = 0;
		final Builder<File> missingFiles = ImmutableList.<File>builder();
		final SubMonitor progress = SubMonitor.convert(monitor, getImportObject().size());
		for (final ImportObject importObject : getImportObject()) {
			final ImportEntry entry = (ImportEntry) importObject;
			if (!entry.getFile().exists()) {
				missingFiles.add(entry.getFile());
				if (!entry.getFile().isAbsolute())
					relative++;
			}
			progress.worked(1);
		}
		progress.done();
		return new LocalFileStatus(getImportObject().size(), missingFiles.build(), relative);
	}

	/**
	 * Value object describing the results from {@link ImportModel#checkLocalFiles(IProgressMonitor)}
	 */
	public static final class LocalFileStatus {
		public final int total;
		public final int missing;
		public final int relative;
		public final ImmutableList<File> missingFiles;

		private LocalFileStatus(final int total, final ImmutableList<File> missingFiles, final int relative) {
			this.total = total;
			this.missingFiles = missingFiles;
			this.missing = missingFiles.size();
			this.relative = relative;
		}

		/**
		 * All local files have been found.
		 */
		public boolean ok() {
			return missing == 0;
		}

		/**
		 * All local files are missing.
		 *
		 * If this is true, there are two to get a usable import spec
		 * <ul>
		 * <li>User {@linkplain ImportModel#setRoot(IPath) sets a new root} and we check again</li>
		 * <li>User wanted the export editor instead
		 * </ul>
		 */
		public boolean allMissing() {
			return total == missing;
		}
	}
}
