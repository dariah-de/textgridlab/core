/**
 * <h3>Basic Concepts</h3>
 * <p>
 * Importing and exporting stuff to / from TextGrid via the lab works by
 * selecting a set of files / objects and then importing or exporting the whole
 * set. Links within the set are taken care of using a configurable link
 * rewriting mechanism if they don't point outside of the import set.
 * </p>
 * <h3>Code overview</h3>
 * <p>
 * The basic data model and the link rewriting is taken care of by the (not Lab
 * specific) {@linkplain info.textgrid.utils.linkrewriter link rewriting
 * library}. This plugin builds an intermediate model layer in the
 * {@link info.textgrid.lab.core.importexport.model} package. The
 * {@link info.textgrid.lab.core.importexport.model.ImportModel} class also
 * contains the code to actually perform the import and export.
 * </p>
 * <p>
 * There is user interface for import and export at
 * {@link info.textgrid.lab.core.importexport.ui}.
 * </p>
 * <h3>Contributions</h3>
 * <p>
 * Clients can contribute small configurators for
 * {@linkplain info.textgrid.lab.core.importexport.model.IImportEntryConfigurator
 * import} or
 * {@linkplain info.textgrid.lab.core.importexport.model.IExportEntryConfigurator
 * export} that can pre-configure the import model when a specific file / object
 * is added to it, e.g., by configuring a rewrite method or adding additional
 * files.
 * </p>
 * 
 */
package info.textgrid.lab.core.importexport;

