package info.textgrid.lab.core.importexport;

import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Locale;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ImportPlugin extends AbstractUIPlugin {


	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.core.importexport"; //$NON-NLS-1$

	// The shared instance
	private static ImportPlugin plugin;

	/**
	 * The content type ID used for import / export specifications.
	 */
	public static final String CONTENT_TYPE_ID = "text/tg.imex+xml"; //$NON-NLS-1$

	public static boolean useNewGUI() {
		return true;
		//		return isDebugging(PLUGIN_ID + "/new-gui"); //$NON-NLS-1$
	}

	public static boolean isDebugging(final String option) {
		return "true".equalsIgnoreCase(Platform.getDebugOption(option)); //$NON-NLS-1$
	}
	
	/**
	 * The constructor
	 */
	public ImportPlugin() {
	}

	// Images
	public static final String IMG_REIMPORT_NEW = "reimport-new"; //$NON-NLS-1$
	public static final String IMG_REIMPORT_REVISION = "reimport-revision"; //$NON-NLS-1$
	public static final String IMG_STATUS_ERROR = "status-error"; //$NON-NLS-1$
	public static final String IMG_STATUS_OK = "status-ok"; //$NON-NLS-1$
	public static final String IMG_STATUS_INFO = "status-info"; //$NON-NLS-1$
	public static final String IMG_STATUS_WARNING = "status-warning"; //$NON-NLS-1$

	public static final String IMG_IMPORT = "icon-import"; //$NON-NLS-1$
	public static final String IMG_EXPORT = "icon-export"; //$NON-NLS-1$
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);
		reg.put(IMG_REIMPORT_NEW, imageDescriptorFromPlugin(PLUGIN_ID, "icons/127-Importiere_Neuelement_in_Import.gif")); //$NON-NLS-1$
		reg.put(IMG_REIMPORT_REVISION, imageDescriptorFromPlugin(PLUGIN_ID, "icons/128-Importiere_Vorhandenes_in_Import.gif")); //$NON-NLS-1$
		;
		reg.put(IMG_STATUS_ERROR, imageDescriptorFromPlugin(PLUGIN_ID, "icons/error_st_obj.gif")); //$NON-NLS-1$
		reg.put(IMG_STATUS_WARNING, imageDescriptorFromPlugin(PLUGIN_ID, "icons/warning_st_obj.gif")); //$NON-NLS-1$
		reg.put(IMG_STATUS_INFO, imageDescriptorFromPlugin(PLUGIN_ID, "icons/info_st_obj.gif")); //$NON-NLS-1$
		reg.put(IMG_STATUS_OK, imageDescriptorFromPlugin(PLUGIN_ID, "icons/ok_st_obj.gif")); //$NON-NLS-1$
		reg.put(IMG_IMPORT, imageDescriptorFromPlugin(PLUGIN_ID, "icons/011-Import-Perspektive.gif")); //$NON-NLS-1$
		reg.put(IMG_EXPORT, imageDescriptorFromPlugin(PLUGIN_ID, "icons/012-Export-Perspektive.gif")); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		StatusManager.getManager().handle(
				new Status(IStatus.INFO, PLUGIN_ID, MessageFormat.format("Working in a {0} locale, default encoding {1}", //$NON-NLS-1$
						Locale.getDefault(), Charset.defaultCharset().toString())));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ImportPlugin getDefault() {
		return plugin;
	}

}
