package info.textgrid.lab.core.importexport;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Perspective for the import editor.
 * 
 * @author vitt
 * 
 */
public class ImportPerspective implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		// center: editor area
		// right: metadata editor
		// bottom, optional: progress stuff org.eclipse.ui.views.ProgressView
		layout.setEditorAreaVisible(true);
		layout.addView("info.textgrid.lab.core.metadataeditor.view", IPageLayout.RIGHT, 0.7f, layout.getEditorArea()); //$NON-NLS-1$
		layout.addPlaceholder("org.eclipse.ui.views.ProgressView", IPageLayout.BOTTOM, 0.8f, layout.getEditorArea()); //$NON-NLS-1$
	}

}
