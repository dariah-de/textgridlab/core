package info.textgrid.lab.core.importexport.configurators;

import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;
import info.textgrid.lab.core.importexport.model.ImportEntry;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;

public class ZipFileSupplier extends AbstractAggregationSupplier implements ISpecialImportEntrySupplier {

	private final ZipFile zipFile;

	public ZipFileSupplier(final ZipFile zipFile, final ImportEntry entry) {
		super(entry);
		this.zipFile = zipFile;
	}

	@Override
	protected String[] getAggregationEntries() {
		final ArrayList<? extends ZipEntry> entries = Collections.list((Enumeration<? extends ZipEntry>) zipFile.entries());
		final String[] result = new String[entries.size()];
		int i = 0;
		for (final ZipEntry entry : entries)
			result[i++] = FilenameUtils.getName(zipFile.getName()) + "/" + entry.getName(); //$NON-NLS-1$
		return result;
	}

	@Override
	public File getFile() {
		return new File(zipFile.getName());
	}

}
