package info.textgrid.lab.core.importexport.configurators;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapters;
import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.AbstractImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IExportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.RewriteSetup;
import info.textgrid.lab.core.model.AggregationReader;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AsyncStatusManager;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Configurator for all kinds of aggregation. Reads the aggregation and adds its
 * children.
 * 
 * @author vitt
 * 
 */
public class AggregationConfigurator extends AbstractImportEntryConfigurator implements IImportEntryConfigurator,
		IExportEntryConfigurator {

	private static final String REWRITE_URL = "internal:textgrid#aggregation";  //$NON-NLS-1$

	public void configureImport(ImportEntry entry, IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, 120);
		try {
			if (isAggregation(entry.getObject())) {
				entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, REWRITE_URL));
				File file = entry.getLocalFile();
				progress.setTaskName(NLS.bind(Messages.AggregationConfigurator_Adding_Children, entry.getLocalData()));
				if (file.isDirectory()) {
					File[] files = file.listFiles();
					entry.setSupplier(new DirectoryAggregationSupplier(file, entry));
					progress.worked(20); 
					if (progress.isCanceled()) return;
					progress.setWorkRemaining(10 * files.length + 10);
					for (File child : files)
						entry.addFile(child, progress.newChild(10));
				} else {
					try {
						List<URI> uris = AggregationReader.read(new StreamSource(file), false);
						progress.worked(20);
						if (progress.isCanceled()) return;
						progress.setWorkRemaining(uris.size() * 10 + 10);
						for (URI uri : uris) {
							if (uri.isAbsolute()) {
								// TODO handle file:/// URIs, report anything
								// else
								StatusManager.getManager().handle(
										new Status(
												IStatus.WARNING,
												ImportPlugin.PLUGIN_ID,
												NLS.bind(
														Messages.AggregationConfigurator_Unknown_Aggregated_Resource,
														entry.getLocalData(), uri)));
								progress.worked(10);
							} else {
								String path = uri.getPath();
								if (path != null) {
									File child = new File(file.getParent(), path);
									entry.addFile(child, progress.newChild(10));
									if (progress.isCanceled()) return;
								}
								// TODO else error handling
							}
						}
						entry.setSupplier(new ListAggregationSupplier(file, uris, entry));
					} catch (XMLStreamException e) {
						StatusManager.getManager().handle(
								new Status(
										IStatus.ERROR,
										ImportPlugin.PLUGIN_ID,
										NLS.bind(
												Messages.AggregationConfigurator_AggregationParseFailed,
												file), e), StatusManager.SHOW | StatusManager.LOG);
					} catch (URISyntaxException e) {
						StatusManager.getManager().handle(
								new Status(
										IStatus.ERROR,
										ImportPlugin.PLUGIN_ID,
										NLS.bind(
												Messages.AggregationConfigurator_AggregationParseFailed,
												file), e), StatusManager.SHOW | StatusManager.LOG);
					}
				}
				if (entry.getObject().getMetadata().getEdition() != null) {
					String work = entry.getObject().getMetadata().getEdition().getIsEditionOf();
					if (work != null && !"".equals(work)) { //$NON-NLS-1$
						File workfile = new File(file.getParent(), work);
						if (workfile.exists())
							entry.getModel().addFile(workfile, progress.newChild(10));
					}
				}
			}
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
		}
		progress.done();
	}

	private boolean isAggregation(final TextGridObject object) {
		try {
			return object.getContentType(false).getId().contains("aggregation"); //$NON-NLS-1$
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
		}
		return false;
	}

	public void configureExport(final ImportEntry entry, final IProgressMonitor monitor) {
		
		if (isAggregation(entry.getObject())) {
			SubMonitor progress = SubMonitor.convert(monitor, 120);
			entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, REWRITE_URL));
			// Exported aggregations live a kind of double life: They are
			// exported as aggregation files, with the corresponding extension,
			// but there is also a directory created for them, without the
			// corresponding extension.
			File dir = new File(FilenameUtils.removeExtension(entry.getLocalData()));
			Aggregation aggregation = getAdapter(entry.getObject(), Aggregation.class);
			progress.worked(10);
			TGObjectReference[] references = new TGObjectReference[0];
			if (aggregation != null) {
				references = getAdapters(aggregation.getChildren(), TGObjectReference.class, false);
				progress.worked(20);
			} else {
				try {
				  references = AggregationReader.list(entry.getObject(), false).toArray(references); 
				  progress.worked(10);
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
				} catch (OfflineException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, e
									.getLocalizedMessage(), e));
				}
			}
			progress.setWorkRemaining(100 * references.length + 100);
			for (TGObjectReference reference : references) {
				try {
					entry.addObject(reference, dir, progress.newChild(100));
				} catch (IllegalStateException e) {
					if (e.getCause() != null && e.getCause() instanceof CrudServiceException) {
						CrudServiceException cause = (CrudServiceException) e.getCause();
						AsyncStatusManager.handle(new Status(
										IStatus.ERROR,
										ImportPlugin.PLUGIN_ID,
										MessageFormat
												.format(Messages.AggregationConfigurator_FailedToAddEntry,
														entry.getObject(),
														reference,
														cause.getLocalizedMessage()),
										e),
										StatusManager.SHOW | StatusManager.LOG);
					}
				}
			}

			try {
				if (entry.getObject().getMetadata().getEdition() != null) {
					String work = entry.getObject().getMetadata().getEdition().getIsEditionOf();
					if (work != null && !"".equals(work)) { //$NON-NLS-1$
						entry.getModel().addObject(new TGObjectReference(URI.create(work)), progress.newChild(100));
					}
				}
			} catch (Exception e) {
				StatusManager.getManager().handle(new Status(IStatus.WARNING, ImportPlugin.PLUGIN_ID, 
						NLS.bind(Messages.AggregationConfigurator_WorkProblem, entry.getObject()), e), 
						StatusManager.SHOW | StatusManager.LOG);
			}
		}
	}

}
