package info.textgrid.lab.core.importexport.configurators;

import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;
import info.textgrid.lab.core.importexport.model.ImportEntry;

import java.io.File;
import java.net.URI;
import java.util.List;

public class ListAggregationSupplier extends AbstractAggregationSupplier implements ISpecialImportEntrySupplier {

	private final List<URI> uris;
	private final File file;

	public ListAggregationSupplier(final File child, final List<URI> uris, final ImportEntry entry) {
		super(entry);
		file = child;
		this.uris = uris;
	}

	@Override
	protected String[] getAggregationEntries() {
		final String[] result = new String[uris.size()];
		int pos = 0;
		for (final URI uri : uris)
			result[pos++] = uri.toString();
		return result;
	}

	@Override
	public File getFile() {
		return file;
	}

}
