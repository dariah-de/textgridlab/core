package info.textgrid.lab.core.importexport.configurators;

import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.importexport.model.AbstractImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.RewriteSetup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;

public class ZipImportConfigurator extends AbstractImportEntryConfigurator implements IImportEntryConfigurator {

	private static final String EXTENSION = "zip"; //$NON-NLS-0$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$

	public void configureImport(ImportEntry entry, IProgressMonitor monitor) {
		if (FilenameUtils.isExtension(entry.getLocalData(), EXTENSION)) {
			try {
				ZipFile zipFile = new ZipFile(entry.getLocalFile());
				entry.getObject().setContentType(Aggregation.CONTENT_TYPE);
				entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:textgrid#aggregation")); //$NON-NLS-0$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$
				entry.setSupplier(new ZipFileSupplier(zipFile, entry));
				ArrayList<? extends ZipEntry> entries = Collections.list(zipFile.entries());
				for (ZipEntry zipEntry : entries) {
					ZipEntrySupplier supplier = new ZipEntrySupplier(zipFile, zipEntry);
					entry.addFile(supplier.getFile(), monitor, supplier);
				}

			} catch (ZipException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
