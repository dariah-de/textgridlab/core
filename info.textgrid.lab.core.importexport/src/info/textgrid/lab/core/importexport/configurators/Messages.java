package info.textgrid.lab.core.importexport.configurators;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.importexport.configurators.messages"; //$NON-NLS-1$
	public static String AbstractAggregationSupplier_FailedToCanonicalizeWarning;
	public static String AbstractAggregationSupplier_NotImportedWarning;
	public static String AggregationConfigurator_Adding_Children;
	public static String AggregationConfigurator_Aggregation_invalid_URI;
	public static String AggregationConfigurator_AggregationParseFailed;
	public static String AggregationConfigurator_Error_reading_aggregation;
	public static String AggregationConfigurator_FailedToAddEntry;
	public static String AggregationConfigurator_Unknown_Aggregated_Resource;
	public static String AggregationConfigurator_WorkProblem;
	public static String SizeWarningConfigurator_ProblematicallyLargeWarning;
	public static String SizeWarningConfigurator_TooLargeWarningTitle;
	public static String XMLConfigurator_AnalyzingXMLStructure;
	public static String XMLConfigurator_ErrorAnalyzingXMLStructure;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
