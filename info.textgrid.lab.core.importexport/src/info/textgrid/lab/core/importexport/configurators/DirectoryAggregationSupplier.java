package info.textgrid.lab.core.importexport.configurators;

import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;
import info.textgrid.lab.core.importexport.model.ImportEntry;

import java.io.File;

public class DirectoryAggregationSupplier extends AbstractAggregationSupplier implements ISpecialImportEntrySupplier {

	private final File directory;

	public DirectoryAggregationSupplier(final File directory, final ImportEntry entry) {
		super(entry);
		this.directory = directory;
	}

	@Override
	protected String[] getAggregationEntries() {
		final String[] list = directory.list();
		for (int i = 0; i < list.length; i++)
			list[i] = directory.getName() + "/" + list[i]; //$NON-NLS-1$
		return list;
	}

	@Override
	public File getFile() {
		return directory;
	}

}
