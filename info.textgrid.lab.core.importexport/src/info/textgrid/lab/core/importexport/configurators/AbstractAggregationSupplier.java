package info.textgrid.lab.core.importexport.configurators;

import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Set;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

/**
 * Partial implementation of {@link ISpecialImportEntrySupplier} that creates an
 * aggregation stream from a {@linkplain #getAggregationEntries() list of
 * aggregation entries}.
 * 
 * @author tv
 * 
 */
public abstract class AbstractAggregationSupplier {

	private static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"; //$NON-NLS-1$
	private static final String ORE_NS = "http://www.openarchives.org/ore/terms/"; //$NON-NLS-1$
	private ImportEntry entry;

	public AbstractAggregationSupplier(final ImportEntry entry) {
		super();
		this.entry = entry;
	}

	public InputStream getInputStream() throws IOException {

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			final XMLStreamWriter xml = XMLOutputFactory.newInstance().createXMLStreamWriter(baos, "UTF-8"); //$NON-NLS-1$
			xml.writeStartDocument();
			xml.setPrefix("rdf", RDF_NS); //$NON-NLS-1$
			xml.setPrefix("ore", ORE_NS); //$NON-NLS-1$
			xml.writeStartElement(RDF_NS, "RDF"); //$NON-NLS-1$
			xml.writeNamespace("rdf", RDF_NS); //$NON-NLS-1$
			xml.writeNamespace("ore", ORE_NS); //$NON-NLS-1$
			xml.writeStartElement(RDF_NS, "Description"); //$NON-NLS-1$
			// xml.writeAttribute(RDF_NS, "about", getFile().getName());

			for (final String entry : filterAggregationEntries(getAggregationEntries())) {
				xml.writeCharacters("\n  "); //$NON-NLS-1$
				xml.writeEmptyElement(ORE_NS, "aggregates"); //$NON-NLS-1$
				xml.writeAttribute(RDF_NS, "resource", entry); //$NON-NLS-1$
			}
			xml.writeCharacters("\n"); //$NON-NLS-1$
			xml.writeEndDocument();
			xml.close();
			baos.close();
		} catch (final XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		OutputStreamWriter writer = new OutputStreamWriter(baos, "UTF-8"); //$NON-NLS-0$ //$NON-NLS-1$ //$NON-NLS-1$
		//
		//		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" //$NON-NLS-1$
		//				+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n" //$NON-NLS-1$
		//				+ "  <rdf:Description xmlns:tei=\"http://www.tei-c.org/ns/1.0\" rdf:about=\"" + getFile().getName() + "\">\n"); //$NON-NLS-1$ //$NON-NLS-2$
		//
		// for (String entry : getAggregationEntries())
		//			writer.write("    <ore:aggregates rdf:resource=\"" + entry + "\"/>\n"); //$NON-NLS-1$ //$NON-NLS-2$
		//
		//		writer.write("  </rdf:Description>\n" + "</rdf:RDF>\n"); //$NON-NLS-1$ //$NON-NLS-2$
		// writer.close();
		System.out.println(baos.toString("UTF-8")); //$NON-NLS-1$

		return new ByteArrayInputStream(baos.toByteArray());
	}

	/**
	 * Removes aggregation entries that are neither in this import set (cf. TG-1470) nor already TextGrid URIs (cf. TG-1786)
	 */
	protected Collection<String> filterAggregationEntries(final String[] aggregationEntries) {
		if (entry == null)
			return ImmutableList.copyOf(aggregationEntries);
		else {
			final Set<File> importedFiles = entry.getModel().getFiles();

			final Predicate<String> usableEntry = new Predicate<String>() {

				@Override
				public boolean apply(final String uri) {
					if (uri.startsWith(TextGridObject.SCHEMA + ":")) //$NON-NLS-1$
						return true;	//  TG-1786
					
					File file;
					try {
						file = new File(getFile().getParent(), uri).getCanonicalFile();
						boolean imported = importedFiles.contains(file);
						if (!imported)
							StatusManager.getManager().handle(
									new Status(IStatus.WARNING, ImportPlugin.PLUGIN_ID, NLS.bind(
											Messages.AbstractAggregationSupplier_NotImportedWarning,
											uri, entry.getLocalData())));
						// TODO add to current result
						return imported;
					} catch (IOException e) {
						StatusManager.getManager().handle(
								new Status(
										IStatus.ERROR,
										ImportPlugin.PLUGIN_ID,
										NLS.bind(
												Messages.AbstractAggregationSupplier_FailedToCanonicalizeWarning,
												uri, entry), e));
					}
					return true;
				}
			};
			return Collections2.filter(ImmutableList.copyOf(aggregationEntries), usableEntry);

		}
	}

	protected abstract String[] getAggregationEntries();

	public abstract File getFile();

}