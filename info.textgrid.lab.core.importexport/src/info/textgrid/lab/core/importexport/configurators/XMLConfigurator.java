package info.textgrid.lab.core.importexport.configurators;

import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.AbstractImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IExportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.RewriteSetup;
import info.textgrid.lab.core.model.TGContentType;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

public class XMLConfigurator extends AbstractImportEntryConfigurator implements IImportEntryConfigurator, IExportEntryConfigurator {

	private static final String TEI_NS = "http://www.tei-c.org/ns/1.0"; //$NON-NLS-1$
	private static final TGContentType XML_CONTENT_TYPE = TGContentType.of("text/xml"); //$NON-NLS-1$
	private static final QName TEI_CORPUS = new QName(TEI_NS, "teiCorpus"); //$NON-NLS-1$
	private static final QName TEI = new QName(TEI_NS, "TEI"); //$NON-NLS-1$

	public void configureImport(ImportEntry entry, IProgressMonitor monitor) {
		try {
			if (XML_CONTENT_TYPE.equals(entry.getObject().getContentType(false))) {
				final SubMonitor progress = SubMonitor.convert(monitor,
						NLS.bind(Messages.XMLConfigurator_AnalyzingXMLStructure, entry.getLocalFile()),
						10);
				FileInputStream stream = new FileInputStream(entry.getLocalFile());
				analyzeStream(stream, entry, progress);
				stream.close();
				progress.done();
			}
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
		} catch (IOException e) {
			StatusManager.getManager().handle(
					new Status(
							IStatus.WARNING,
							ImportPlugin.PLUGIN_ID,
							NLS.bind(
									Messages.XMLConfigurator_ErrorAnalyzingXMLStructure,
									entry), e));
		} catch (XMLStreamException e) {
			StatusManager.getManager().handle(
					new Status(
							IStatus.WARNING,
							ImportPlugin.PLUGIN_ID,
							NLS.bind(
									Messages.XMLConfigurator_ErrorAnalyzingXMLStructure,
									entry), e));
		}
	}
	
	public void configureExport(ImportEntry entry, IProgressMonitor monitor) {
		try {
			if (XML_CONTENT_TYPE.equals(entry.getObject().getContentType(false))) {
				// don't want to analyze remote's content
				entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:tei#tei")); //$NON-NLS-1$
			}
		} catch (CoreException e) {
		}
	}

	private void analyzeStream(InputStream stream, ImportEntry entry, final SubMonitor progress)
			throws FactoryConfigurationError, XMLStreamException {
		progress.worked(2);
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		XMLStreamReader reader = factory.createXMLStreamReader(stream);
		progress.worked(2);
		reader.nextTag(); // will move to the document element
		progress.worked(4);
		QName rootElementName = reader.getName();
		if (TEI.equals(rootElementName) || TEI_CORPUS.equals(rootElementName))
			entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:tei#tei")); //$NON-NLS-1$
		else if (reader.getEncoding() != null && !"UTF-8".equalsIgnoreCase(reader.getEncoding())) //$NON-NLS-1$
			entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:textgrid#encoding")); //$NON-NLS-1$
		reader.close();
		progress.worked(2);
	}

}
