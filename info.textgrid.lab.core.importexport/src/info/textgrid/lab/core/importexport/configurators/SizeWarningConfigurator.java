package info.textgrid.lab.core.importexport.configurators;

import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.IStatusAdapterConstants;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * TG-1095 / TG-1382
 * 
 * @deprecated This is fixed by us using CXF, cf.
 *
 */
@Deprecated 
public class SizeWarningConfigurator implements IImportEntryConfigurator {


	public static final long WARNING_SIZE = 10 * 1024 * 1024;
	private final boolean ok;

	public SizeWarningConfigurator() {
		super();
		String version = System.getProperty("java.version"); //$NON-NLS-1$
		String[] versions = version.split("\\."); //$NON-NLS-1$
		ok = Integer.decode(versions[1]) > 6;
	}

	public void configureImport(ImportEntry entry, IProgressMonitor monitor) {
		if (!ok && entry.getLocalFile().length() > WARNING_SIZE) {
			StatusAdapter adapter = new StatusAdapter(
					new Status(
							IStatus.WARNING,
							ImportPlugin.PLUGIN_ID,
							NLS.bind(
									Messages.SizeWarningConfigurator_ProblematicallyLargeWarning,
					entry.getLocalData())));
			adapter.setProperty(IStatusAdapterConstants.TITLE_PROPERTY, NLS.bind(Messages.SizeWarningConfigurator_TooLargeWarningTitle, entry.getLocalFile().getName()));

			StatusManager.getManager().handle(adapter, StatusManager.SHOW);
		}
	}
}
