package info.textgrid.lab.core.importexport.configurators;

import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipEntrySupplier implements ISpecialImportEntrySupplier {

	private final ZipFile zipFile;
	private final ZipEntry zipEntry;

	ZipEntrySupplier(ZipFile zipFile, ZipEntry zipEntry) {
		super();
		this.zipFile = zipFile;
		this.zipEntry = zipEntry;
	}

	public InputStream getInputStream() throws IOException {
		return zipFile.getInputStream(zipEntry);
	}

	public File getFile() {
		return new File(zipFile.getName(), zipEntry.getName());
	}

}
