package info.textgrid.lab.core.importexport;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class ExportPerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		// center: editor area
		// left: navigator, optional: search results
		// bottom, optional: progress stuff org.eclipse.ui.views.ProgressView
		layout.setEditorAreaVisible(true);
		IFolderLayout sourceFolder = layout.createFolder("source", IPageLayout.LEFT, 0.3f, layout.getEditorArea()); //$NON-NLS-1$
		sourceFolder.addView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
		sourceFolder.addPlaceholder("info.textgrid.lab.search.views.ResultView"); //$NON-NLS-1$
		layout.addPlaceholder("org.eclipse.ui.views.ProgressView", IPageLayout.BOTTOM, 0.8f, layout.getEditorArea()); //$NON-NLS-1$
	}

}
