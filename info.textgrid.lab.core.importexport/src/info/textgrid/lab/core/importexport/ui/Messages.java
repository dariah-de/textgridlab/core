package info.textgrid.lab.core.importexport.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.importexport.ui.messages"; //$NON-NLS-1$
	public static String ExportHandler_could_not_open_editor;
	public static String ExportPart_Browse;
	public static String ExportPart_Drop_Message;
	public static String ExportPart_Target_directory;
	public static String ExportPart_Target_directory_desc;
	public static String ImportExportPart_Adding_files;
	public static String ImportExportPart_Adding_objects;
	public static String ImportExportPart_Cancel;
	public static String ImportExportPart_Could_not_load_import_definition;
	public static String ImportExportPart_drop;
	public static String ImportExportPart_Export;
	public static String ImportExportPart_Export_Part_Title;
	public static String ImportExportPart_Export_Part_Title_File;
	public static String ImportExportPart_Exporting;
	public static String ImportExportPart_File;
	public static String ImportExportPart_File_Desc;
	public static String ImportExportPart_Format;
	public static String ImportExportPart_Format_Desc;
	public static String ImportExportPart_GenericTitle;
	public static String ImportExportPart_Import;
	public static String ImportExportPart_Import_Done;
	public static String ImportExportPart_Import_Part_Title;
	public static String ImportExportPart_Import_Part_Title_File;
	public static String ImportExportPart_Importing;
	public static String ImportExportPart_ImportRootPrompt;
	public static String ImportExportPart_ImportRootTitle;
	public static String ImportExportPart_LoadingImex;
	public static String ImportExportPart_Local_File;
	public static String ImportExportPart_NoFileFoundChoice;
	public static String ImportExportPart_OpenAnyway;
	public static String ImportExportPart_OpenForExport;
	public static String ImportExportPart_Remove;
	public static String ImportExportPart_Remove_Tooltip;
	public static String ImportExportPart_RewriteSetupNotURI;
	public static String ImportExportPart_Rewriting;
	public static String ImportExportPart_Rewriting_Desc;
	public static String ImportExportPart_Save_as_TextGrid_Title;
	public static String ImportExportPart_Save_As_Title;
	public static String ImportExportPart_Save_As_What;
	public static String ImportExportPart_Save_Failed;
	public static String ImportExportPart_Save_to_Target_Project;
	public static String ImportExportPart_SaveImportSpecAsTitle;
	public static String ImportExportPart_Saving_x;
	public static String ImportExportPart_SelectRootFolder;
	public static String ImportExportPart_Target_Project;
	public static String ImportExportPart_TextGridRep;
	public static String ImportExportPart_Title;
	public static String ImportExportPart_Title_Desc;
	public static String ImportExportPart_Title_Validation_Warning;
	public static String ImportExportPart_Updating;
	public static String ImportExportPart_Updating_Display;
	public static String ImportExportPart_Updating_input;
	public static String ImportExportPart_Updating_Viewer;
	public static String ImportExportPart_URI;
	public static String ImportExportPart_URI_Desc;
	public static String ImportExportPart_x_Import_New_Object;
	public static String ImportExportPart_x_Reimport_New_Object;
	public static String ImportExportPart_x_Reimport_New_Revision_y;
	public static String ImportHandler_could_not_open_editor;
	public static String ImportPart_Add;
	public static String ImportPart_Add_Error;
	public static String ImportPart_Add_Tooltip;
	public static String ImportPart_AddFilesDialogTitle;
	public static String ImportPart_Reimport_Message;
	public static String OverwriteDialog_CancelButton;
	public static String OverwriteDialog_CheckingProgressMessage;
	public static String OverwriteDialog_DialogMessage;
	public static String OverwriteDialog_ExistingFileColumnTitle;
	public static String OverwriteDialog_ExistingMetaColumnTitle;
	public static String OverwriteDialog_OverwriteButton;
	public static String OverwriteDialog_TGOColumnTitle;
	public static String OverwriteDialog_Title;
	public static String ResultPage_Cancelled;
	public static String ResultPage_Error;
	public static String ResultPage_Export_Successful;
	public static String ResultPage_File;
	public static String ResultPage_Import_Finished;
	public static String ResultPage_Import_Some_Problems;
	public static String ResultPage_Import_Successful;
	public static String ResultPage_Info;
	public static String ResultPage_Message;
	public static String ResultPage_Export_Some_Problems;
	public static String ResultPage_Export_Finished;
	public static String ResultPage_Save_to_File;
	public static String ResultPage_Save_to_TextGrid;
	public static String ResultPage_SaveInfo;
	public static String ResultPage_Status;
	public static String ResultPage_Success;
	public static String ResultPage_URI;
	public static String ResultPage_Warning;
	public static String TargetProjectCombo_x_create_new_revisions;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
