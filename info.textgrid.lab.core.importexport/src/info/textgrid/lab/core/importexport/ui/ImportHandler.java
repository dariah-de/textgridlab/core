package info.textgrid.lab.core.importexport.ui;

import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.ui.ImportPart.AddFilesJob;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.DropSetValues;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.part.NullEditorInput;

public class ImportHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			IEditorInput input = null;
			TextGridProject project = null;
			ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
			
			if (currentSelection != null && !currentSelection.isEmpty() && currentSelection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) currentSelection).getFirstElement();
				TextGridObject object = AdapterUtils.getAdapter(firstElement, TextGridObject.class);
				if (object != null) {
					if (ImportPlugin.CONTENT_TYPE_ID.equals(object.getContentTypeID()))
						input = AdapterUtils.getAdapter(object, IEditorInput.class);
					else {
						project = object.getProjectInstance();
					}
				} else {
					project = AdapterUtils.getAdapter(firstElement, TextGridProject.class);
				}
			}

			if (input == null)
				input = new NullEditorInput();

			IWorkbenchWindow activeWorkbenchWindow = HandlerUtil.getActiveWorkbenchWindow(event);
			if (activeWorkbenchWindow == null)
				activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
			final ImportPart editor = (ImportPart) activePage.openEditor(input,
					"info.textgrid.lab.core.importexport.ImExEditor"); //$NON-NLS-1$
			
			if (project != null) {
				editor.setProject(project);
			}
			
			PlatformUI.getWorkbench().showPerspective("info.textgrid.lab.core.importexport.import", //$NON-NLS-1$
					activeWorkbenchWindow);
			
			// Is the handler called from the drop adapter?
			if ("true".equals(event.getParameters().get("info.textgrid.lab.core.importexport.import.usedroppedset"))) { //$NON-NLS-1$ //$NON-NLS-2$
				if (DropSetValues.getProject() != null) {
					editor.getProjectCombo().addSelectionChangedListener(new ISelectionChangedListener() {
						
						@Override
						public void selectionChanged(SelectionChangedEvent event) {
							editor.new AddFilesJob(DropSetValues.getValues()).schedule();
							editor.getProjectCombo().removeSelectionChangedListener(this);
						}
					});
					editor.setProject(DropSetValues.getProject());
				}	
			}
			
		} catch (PartInitException e) {
			throw new ExecutionException(Messages.ImportHandler_could_not_open_editor, e);
		} catch (WorkbenchException e) {
			throw new ExecutionException(Messages.ImportHandler_could_not_open_editor, e);
		} catch (CoreException e) {
			throw new ExecutionException(Messages.ImportHandler_could_not_open_editor, e);
		}
		return null;
	}

}
