package info.textgrid.lab.core.importexport.ui;

import static info.textgrid.lab.core.importexport.ImportPlugin.CONTENT_TYPE_ID;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.part.NullEditorInput;

public class ExportHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			IEditorInput input = null;

			// if the current object is an IMEX spec, create an IEditorInput from it to open it in the editor later. 
			ISelection selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				TextGridObject object = AdapterUtils.getAdapter(((IStructuredSelection) selection).getFirstElement(),
						TextGridObject.class);
				if (object != null && CONTENT_TYPE_ID.equals(object.getContentTypeID())) {
					input = (IEditorInput) object.getAdapter(IEditorInput.class);
				}
			}

			ExportPart editor = null;
			
			if (input == null) {
				input = new NullEditorInput();
				
				// if an export editor is currently open, use it (#8574)
				IEditorPart activeEditor = HandlerUtil.getActiveEditor(event);
				if (activeEditor instanceof ExportPart)
					editor = (ExportPart) activeEditor;
			}
			
			if (editor == null)
				// otherwise, open a new export editor
				editor = (ExportPart) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().openEditor(input,
					"info.textgrid.lab.core.importexport.ExportEditor"); //$NON-NLS-1$
			PlatformUI.getWorkbench().showPerspective("info.textgrid.lab.core.importexport.export", //$NON-NLS-1$
					HandlerUtil.getActiveWorkbenchWindow(event));
			if (selection instanceof IStructuredSelection)
				editor.addSelection((IStructuredSelection) selection);
		} catch (PartInitException e) {
			throw new ExecutionException(Messages.ExportHandler_could_not_open_editor, e);
		} catch (WorkbenchException e) {
			throw new ExecutionException(Messages.ExportHandler_could_not_open_editor, e);
		} catch (CoreException e) {
			throw new ExecutionException(Messages.ExportHandler_could_not_open_editor, e);
		}

		return null;
	}

}
