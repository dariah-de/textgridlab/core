package info.textgrid.lab.core.importexport.ui;

import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.ImportModel;
import info.textgrid.lab.core.importexport.model.ImportModel.ModelListener;
import info.textgrid.lab.core.model.ModelAdaptorFactory;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.utils.ProjectCombo;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

public class ImportPart extends ImportExportPart {

	private static final String REIMPORT_TEXT = Messages.ImportPart_Reimport_Message;
	private Label reimportLabel;
	private final class ImportDropAdapter extends ViewerDropAdapter {
		private ImportDropAdapter(final Viewer viewer) {
			super(viewer);
			setExpandEnabled(true);
			// setFeedbackEnabled(true);
		}

		@Override
		public boolean performDrop(final Object data) {
			if (data instanceof String[]) {
				final String[] filenames = (String[]) data;

				new AddFilesJob(filenames).schedule();

			}
			return true;
		}

		@Override
		public boolean validateDrop(final Object target, final int operation, final TransferData transferType) {
			System.out.println(MessageFormat.format("validateDrop. Target: {0}; transfer: {1}", target, transferType)); //$NON-NLS-1$
			return target == null || target instanceof ImportModel || target instanceof ImportEntry
					&& ((ImportEntry) target).mayHaveChildren();
		}
	}

	/**
	 * A job that adds the files specified by the filenames passed in through
	 * the constructor to the import model.
	 */
	protected class AddFilesJob extends Job {
		private final String[] filenames;
		ImportEntry entry;

		public AddFilesJob(final String... filenames) {
			super(Messages.ImportExportPart_Adding_files);
			this.filenames = filenames;
			this.setUser(true);
		}

		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			final SubMonitor progress = SubMonitor.convert(monitor, 100 * filenames.length);

			for (final String filename : filenames) {
				if (progress.isCanceled())
					return Status.CANCEL_STATUS;
				ImportEntry lastEntry = getImportModel().addFile(new File(filename), progress.newChild(100));
				if (entry == null)
					entry = lastEntry;
			}
			final Set<String> reimportProjects = getImportModel().getReimportProjectIDs();

			new UIJob(Messages.ImportExportPart_Updating_Viewer) {

				@Override
				public IStatus runInUIThread(final IProgressMonitor monitor) {
					if (getContent().isDisposed())
						return Status.CANCEL_STATUS;

					setDirty(true);
					getObjectsViewer().refresh();
					if (entry != null)
						getObjectsViewer().setSelection(new StructuredSelection(entry));
					getProjectCombo().setReimportProjects(reimportProjects);
					showReimportLabel(!reimportProjects.isEmpty());
					return Status.OK_STATUS;
				}
			}.schedule();
			return Status.OK_STATUS;
		}
	}

	public ImportPart() {
		super();
		setExport(false);
	}

	@Override
	public void createPartControl(final Composite parent) {
		super.createPartControl(parent);
		getObjectsViewer().addDropSupport(DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK,
				new Transfer[] { FileTransfer.getInstance() }, new ImportDropAdapter(getObjectsViewer()));
		ImageRegistry registry = JFaceResources.getImageRegistry();
		System.out.println(registry);
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.core.importexport.ImportEditor"); //$NON-NLS-1$
	}

	@Override
	protected void createAreaBelowObjectsViewer(final Composite content) {
		reimportLabel = new Label(content, SWT.WRAP);
		reimportLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false));
		reimportLabel.setText("xxx"); //$NON-NLS-1$
		reimportLabel.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
		reimportLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_INFO_FOREGROUND));
		reimportLabel.setVisible(false);

	}

	private void showReimportLabel(boolean show) {
		reimportLabel.setText(show ? REIMPORT_TEXT : "---"); //$NON-NLS-1$
		reimportLabel.setVisible(show);
		reimportLabel.pack(true);
		getContent().layout(true, true);
	}

	protected void createTopArea(final Composite parent) {
		final Composite topGroup = new Composite(parent, SWT.NONE);
		topGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		topGroup.setLayout(new GridLayout(3, false));
	
		final Label rootLabel = new Label(topGroup, SWT.NONE);
		rootLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		rootLabel.setText(Messages.ImportExportPart_drop);
	
		final Button addButton = new Button(topGroup, SWT.FLAT);
		addButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.END, false, false));
		addButton.setText(Messages.ImportPart_Add);
		addButton.setToolTipText(Messages.ImportPart_Add_Tooltip);

		addButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				final FileDialog dialog = new FileDialog(getSite().getShell(), SWT.OPEN | SWT.MULTI);
				dialog.setText(Messages.ImportPart_AddFilesDialogTitle);
				
				// #11680 -- Mac OS X 10.9.1 seems to show an empty dialog on first try
				final IPath root = getImportModel().getRoot();
				if (root == null && (dialog.getFilterPath() == null || dialog.getFilterPath().isEmpty()))
					dialog.setFilterPath(System.getProperty("user.home", "/")); //$NON-NLS-1$ //$NON-NLS-2$
				else
					dialog.setFilterPath(root.toOSString());
				
				final String result = dialog.open();
				if (result != null) {
					final String[] fileNames = dialog.getFileNames();
					final String[] fullNames = new String[fileNames.length];
					try {
						for (int i = 0; i < fileNames.length; i++)
							fullNames[i] = new File(dialog.getFilterPath(), fileNames[i]).getCanonicalPath();
						new AddFilesJob(fullNames).schedule();
					} catch (final IOException e1) {
						StatusManager.getManager().handle(
								new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
										Messages.ImportPart_Add_Error, e1.getLocalizedMessage()), e1),
								StatusManager.SHOW | StatusManager.LOG);
					}
				}
			}
		});
	
		createRemoveButton(topGroup);

		getImportModel().addListener(new ModelListener() {
	
			@Override
			public void rootChanged(final ImportModel model, final IPath oldRoot) {
				new UIJob("") { //$NON-NLS-1$
	
					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						if (!rootLabel.isDisposed())
							rootLabel.setText(model.getRoot().toPortableString());
						return Status.OK_STATUS;
					}
	
				}.schedule();
			}
	
		});
	}

	@Override
	protected Composite createTargetArea(final Composite content) {
		final Composite targetComposite = new Composite(content, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(targetComposite);
		GridLayoutFactory.fillDefaults().numColumns(3).applyTo(targetComposite);
		final Label targetProject = new Label(targetComposite, SWT.NONE);
		targetProject.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		targetProject.setText(Messages.ImportExportPart_Target_Project);
		setProjectCombo(new TargetProjectCombo(targetComposite));
		GridDataFactory.fillDefaults().grab(true, false).applyTo(getProjectCombo().getControl());
		getProjectCombo().setNeedsValue(true); // TODO initialize project if
												// possible

		getProjectCombo().addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				if (!event.getSelection().isEmpty()) {
					getImportModel().setTargetProject(
							AdapterUtils.getAdapter(((IStructuredSelection) event.getSelection()).getFirstElement(),
									TextGridProject.class));
					getImportModel().setImportAsNewRevisions(getProjectCombo().isNewRevision());
					getObjectsViewer().refresh(true);
					if (noProjectDisabledState != null) {
						noProjectDisabledState.restore();
						noProjectDisabledState = null;
					}
				} else if (noProjectDisabledState == null) {
					noProjectDisabledState = ControlEnableState.disable(importGroup);
				}
			}
		});

		return targetComposite;
	}

	public void setProject(final TextGridProject project) {
		final ProjectCombo combo = getProjectCombo();
		combo.setProject(project);
	}

	protected void perform() {
		final ControlEnableState enableState = ControlEnableState.disable(getContent());
		final TextGridProject project = AdapterUtils.getAdapter(
				((IStructuredSelection) getProjectCombo().getSelection()).getFirstElement(), TextGridProject.class);
		if (project == null) {
			// TODO error handling
			return;
		}
		final Job importJob = new Job(Messages.ImportExportPart_Importing) {
	
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				return getImportModel().doImport(project, monitor);
			}
		};
		importJob.addJobChangeListener(new JobChangeAdapter() {
			@Override
			public void done(final IJobChangeEvent event) {
				new UIJob(Messages.ImportExportPart_Import_Done) {
	
					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						
						resultPage.setInput(event.getResult());
						enableState.restore();
						if (ImportPlugin.useNewGUI()) {
							if (event.getResult().getSeverity() <= IStatus.WARNING)
									setDirty(false);
							pages.showPage(resultPage);
							getNextButton().setEnabled(true);
						}
						return Status.OK_STATUS;
					}
				}.schedule();
			}
		});
		importJob.setUser(true);
		importJob.schedule();
	}

}
