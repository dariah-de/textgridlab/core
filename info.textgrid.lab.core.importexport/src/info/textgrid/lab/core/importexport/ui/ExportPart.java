package info.textgrid.lab.core.importexport.ui;

import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ImportModel;
import info.textgrid.lab.core.importexport.model.ImportModel.ModelListener;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

public class ExportPart extends ImportExportPart {

	protected void createTopArea(Composite parent) {
		Composite topArea = new Composite(parent, SWT.NONE);
		topArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		topArea.setLayout(new GridLayout(2, false));

		Label label = new Label(topArea, SWT.WRAP);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		label.setText(Messages.ExportPart_Drop_Message);

		createRemoveButton(topArea);
	}

	private final class ExportDropAdapter extends ViewerDropAdapter {
		private ExportDropAdapter(final Viewer viewer) {
			super(viewer);
		}

		@Override
		public boolean validateDrop(final Object target, final int operation, final TransferData transferType) {
			return true;
		}

		@Override
		public boolean performDrop(final Object data) {
			System.out.println("Dropped " + data); //$NON-NLS-1$
			boolean result = false;
			// no, it's a selection, of course.
			if (data instanceof IStructuredSelection) {
				addSelection((IStructuredSelection) data);
				result = true;
			}
			return result;
		}
	}

	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		getObjectsViewer().addDropSupport(DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK,
				new Transfer[] { LocalSelectionTransfer.getTransfer() }, new ExportDropAdapter(getObjectsViewer()));
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.core.importexport.ExportEditor"); //$NON-NLS-1$
	}

	protected boolean ignoreNextRootChange;
	public ExportPart() {
		super();
		setExport(true);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Composite createTargetArea(final Composite content) {
		final Composite targetComposite = new Composite(content, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(targetComposite);
		GridLayoutFactory.fillDefaults().numColumns(4).applyTo(targetComposite);

//		final Label label = new Label(targetComposite, SWT.NONE);
//		label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
//		label.setText("&Target directory: ");

		final Text targetText = new Text(targetComposite, SWT.SINGLE | SWT.LEAD | SWT.BORDER);
		targetText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		targetText.setMessage(Messages.ExportPart_Target_directory);
		targetText.setEditable(false);
		
		
		targetText.addModifyListener(new ModifyListener() {
			

			@Override
			public void modifyText(ModifyEvent e) {
				ignoreNextRootChange = true;
				getImportModel().setRoot(new Path(targetText.getText().trim()));
				getObjectsViewer().refresh(true);
			}
		});
		
		getImportModel().addListener(new ModelListener() {
			

			@Override
			public void rootChanged(ImportModel model, IPath oldRoot) {
				if (ignoreNextRootChange)
					ignoreNextRootChange = false;
				else if (!targetText.isDisposed())
					targetText.setText(model.getRoot().toOSString());
			}	
		});

		final Button browseTargetButton = new Button(targetComposite, SWT.PUSH);
		browseTargetButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		browseTargetButton.setText(Messages.ExportPart_Browse);

		browseTargetButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(getSite().getShell());
				dialog.setMessage(Messages.ExportPart_Target_directory_desc);
				dialog.setText(Messages.ExportPart_Target_directory);
				dialog.setFilterPath(targetText.getText());
				String newPath = dialog.open();
				if (newPath != null)
					targetText.setText(newPath);
			}

		});

		return targetComposite;
	}

	protected void perform() {
		final ControlEnableState enableState = ControlEnableState.disable(getContent());
		final Job exportJob = new Job(Messages.ImportExportPart_Exporting) {
	
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				return getImportModel().doExport(monitor);
			}
	
		};
		exportJob.addJobChangeListener(new JobChangeAdapter() {
			@Override
			public void done(final IJobChangeEvent event) {
				new UIJob(Messages.ImportExportPart_Import_Done) {
	
					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						resultPage.setInput(event.getResult());
						enableState.restore();
						if (ImportPlugin.useNewGUI()) {
							if (event.getResult().getSeverity() <= IStatus.WARNING)
								setDirty(false);
							pages.showPage(resultPage);
							getNextButton().setEnabled(true);
						}
						return Status.OK_STATUS;
					}
				}.schedule();
			}
		});
		exportJob.setUser(true);
		exportJob.schedule();
	
	}

}
