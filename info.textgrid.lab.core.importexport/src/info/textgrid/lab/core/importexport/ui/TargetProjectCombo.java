package info.textgrid.lab.core.importexport.ui;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.utils.ProjectCombo;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;
import info.textgrid.lab.ui.core.utils.UpdatingDeferredListContentProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

public class TargetProjectCombo extends ProjectCombo {

	@Override
	public void textGridProjectChanged(Event event, TextGridProject project) {
		if (isLoadDone())
			switch (event) {
			case CREATED:
				add(new ProjectEntry(project, false));
				break;
			case DELETED:
				remove(new ProjectEntry(project, false));
				remove(new ProjectEntry(project, true));
				break;
			}
	}

	public void setProject(TextGridProject project) {
		selectedProject = project;
		if (isLoadDone())
			setSelection(new StructuredSelection(new ProjectEntry(project, false)));
	}

	private final class ContentProvider extends UpdatingDeferredListContentProvider {
		private Set<TextGridProject> projects = Collections.synchronizedSet(new HashSet<TextGridProject>());
		private boolean internalUpdate = false;

		public void setNextUpdateInternal(final boolean internalUpdate) {
			this.internalUpdate = internalUpdate;
		}

		public boolean isNextUpdateInternal() {
			return internalUpdate;
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			if (projects != null)
				projects.clear();

			super.inputChanged(viewer, oldInput, newInput);
		}

		public Object[] getElements(Object inputElement) {
			if (isNextUpdateInternal() && !projects.isEmpty()) {
				setNextUpdateInternal(false);
				return getPreparedObjectEntries();
			} else
				return super.getElements(inputElement);
		}

		private ProjectEntry[] getPreparedObjectEntries() {
			final ArrayList<ProjectEntry> result = Lists.newArrayListWithExpectedSize(projects.size() + 1);
			for (final TextGridProject project : projects) {
				if (getReimportProjects().contains(project.getId()))
					result.add(new ProjectEntry(project, true));
				result.add(new ProjectEntry(project, false));
			}
			return result.toArray(new ProjectEntry[0]);
		}

		protected void addElements(Object[] elements, IProgressMonitor monitor) {
			final ArrayList<ProjectEntry> result = Lists.newArrayList();
			for (final Object element : elements) {
				TextGridProject project = AdapterUtils.getAdapter(element, TextGridProject.class);
				projects.add(project);
				if (getReimportProjects().contains(project.getId()))
					result.add(new ProjectEntry(project, true));
				else
					result.add(new ProjectEntry(project, false));
			}
			super.addElements(result.toArray(), monitor);
		}
	}

	private static class ProjectEntry implements IAdaptable {
	
		public final TextGridProject project;
		public final boolean revision;
	
		public ProjectEntry(final TextGridProject project, final boolean revision) {
			super();
			this.project = project;
			this.revision = revision;
		}
	
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((project == null) ? 0 : project.hashCode());
			result = prime * result + (revision ? 1231 : 1237);
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ProjectEntry other = (ProjectEntry) obj;
			if (project == null) {
				if (other.project != null)
					return false;
			} else if (!project.equals(other.project))
				return false;
			if (revision != other.revision)
				return false;
			return true;
		}

		public String toString() {
			return "ProjectEntry [project=" + project + ", revision=" + revision + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		@Override
		@SuppressWarnings({ "unchecked", "rawtypes" })
		// due to Eclipse API
		public Object getAdapter(final Class adapter) {
			if (adapter.isAssignableFrom(TextGridProject.class))
				return project;
			return null;
		}
	
	}

	public void init(LEVELS level) {
		UpdatingDeferredListContentProvider contentProvider = new ContentProvider();
		setContentProvider(contentProvider);
		contentProvider.addDoneListener(this);

		setLabelProvider(new TGODefaultLabelProvider() {

			public String getText(Object element) {
				if (element instanceof ProjectEntry) {
					ProjectEntry entry = (ProjectEntry) element;
					if (entry.revision)
						return NLS.bind(Messages.TargetProjectCombo_x_create_new_revisions, super.getText(entry.project));
					else
						return super.getText(entry.project);
				}
				return super.getText(element);
			}

		});
		super.init(level);
	}

	private Set<String> reimportProjects = ImmutableSet.of();

	public TargetProjectCombo(Composite parent) {
		super(parent, TextGridProjectRoot.LEVELS.EDITOR);
	}

	public synchronized void setReimportProjects(final Set<String> reimportProjects) {
		this.reimportProjects = Collections.synchronizedSet(reimportProjects);
		refreshReimport();
	}

	public synchronized Set<String> getReimportProjects() {
		return reimportProjects;
	}

	public void refreshReimport() {
		((ContentProvider) getContentProvider()).setNextUpdateInternal(true);
		refresh();
	}

	/**
	 * Returns true if a 'new revision' entry has been selected in the control,
	 * false otherwise.
	 */
	public boolean isNewRevision() {
		if (!getControl().isDisposed()) {
			IStructuredSelection selection = (IStructuredSelection) getSelection();
			if (!selection.isEmpty())
				return ((ProjectEntry) selection.getFirstElement()).revision;
		}
		return false;
	}

}
