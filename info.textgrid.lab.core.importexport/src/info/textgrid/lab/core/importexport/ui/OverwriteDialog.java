package info.textgrid.lab.core.importexport.ui;

import info.textgrid._import.ImportObject;
import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.ImportModel;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.Lists;

/**
 * Checks for files that would be overwritten. If any, the user is asked. Use
 * {@link #mayContinue(ImportModel, IProgressMonitor)} as an entry point to this
 * class.
 * 
 * @author vitt
 * @see "https://develop.sub.uni-goettingen.de/jira/browse/TG-1145"
 */
public class OverwriteDialog extends MessageDialog {

	protected static class OverwriteEntry {

		public final ImportEntry entry;
		public final boolean fileExists;
		public final boolean metaExists;
		public final File metaFile;
		public final File file;

		public OverwriteEntry(final ImportModel model, final ImportEntry object) {
			this.entry = object;
			file = model.resolve(entry.getLocalFile());
			this.fileExists = file.exists();
			metaFile = model.resolve(entry.getLocalMetadataFile());
			this.metaExists = metaFile.exists();
		}

		public boolean somethingExists() {
			return fileExists || metaExists;
		}

	}

	private final OverwriteEntry[] entries;

	/**
	 * @wbp.parser.constructor
	 */
	protected OverwriteDialog(final Shell parentShell, final OverwriteEntry[] entries) {
		super(
				parentShell,
				Messages.OverwriteDialog_Title,
				null,
				Messages.OverwriteDialog_DialogMessage,
				QUESTION, new String[] { Messages.OverwriteDialog_CancelButton, Messages.OverwriteDialog_OverwriteButton }, 1);
		this.entries = entries;
	}

	protected Control createCustomArea(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		TableViewer viewer = new TableViewer(parent, SWT.FULL_SELECTION);
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		TableViewerColumn objectColumn = new TableViewerColumn(viewer, SWT.LEFT);
		objectColumn.setLabelProvider(new ColumnLabelProvider() {

			public Image getImage(Object element) {
				if (element instanceof OverwriteEntry)
					try {
						return ((OverwriteEntry) element).entry.getObject().getContentType(false).getImage(true);
					} catch (CoreException e) {
						StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
					}
				return null;
			}

			public String getText(Object element) {
				if (element instanceof OverwriteEntry)
					try {
						return ((OverwriteEntry) element).entry.getObject().getTitle();
					} catch (CoreException e) {
						StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
					}
				return super.getText(element);
			}
		});
		objectColumn.getColumn().setText(Messages.OverwriteDialog_TGOColumnTitle);
		objectColumn.getColumn().setWidth(137);
		
		TableViewerColumn fileColumn = new TableViewerColumn(viewer, SWT.LEFT);
		fileColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				if (element instanceof OverwriteEntry) {
					if (((OverwriteEntry) element).fileExists)
						return ((OverwriteEntry) element).file.getPath();
					else
						return ""; //$NON-NLS-1$
				}
				return super.getText(element);
			}
		});

		fileColumn.getColumn().setMoveable(true);
		fileColumn.getColumn().setText(Messages.OverwriteDialog_ExistingFileColumnTitle);
		fileColumn.getColumn().setWidth(150);

		TableViewerColumn metaColumn = new TableViewerColumn(viewer, SWT.LEFT);
		metaColumn.getColumn().setMoveable(true);
		metaColumn.getColumn().setText(Messages.OverwriteDialog_ExistingMetaColumnTitle);
		metaColumn.getColumn().setWidth(150);
		metaColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				if (element instanceof OverwriteEntry) {
					if (((OverwriteEntry) element).metaExists)
						return ((OverwriteEntry) element).metaFile.getPath();
					else
						return ""; //$NON-NLS-1$
				}
				return super.getText(element);
			}

		});

		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setInput(entries);

		return viewer.getControl();
	}

	/**
	 * Check (including user query) whether the export would overwrite some
	 * files the user wants to keep.
	 * 
	 * This method (which may be called from a background job) first checks
	 * whether one or more of the files the export would generate already
	 * exists. If so, it asks the user whether it may overwrite them or would
	 * rather cancel.
	 * 
	 * @param model
	 *            The configured model which is to be checked
	 * @param monitor
	 *            a progress monitor for reporting progress (during the check),
	 *            may be <code>null</code> if no progress reporting is required
	 * @return <code>true</code>, if none of the files already exists or if the
	 *         user explicitly allows overwriting. <code>false</code> if the
	 *         user does not allow overwriting or if the check has been
	 *         cancelled using the progress monitor.
	 */
	public static boolean mayContinue(final ImportModel model, final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor, Messages.OverwriteDialog_CheckingProgressMessage, model.getImportObject().size() + 1);

		// collect all entries for which at least one file exists
		final List<OverwriteEntry> entries = Lists.newArrayList();
		for (final ImportObject object : model.getImportObject()) {
			if (progress.isCanceled())
				return false;
			final OverwriteEntry entry = new OverwriteEntry(model, (ImportEntry) object);
			if (entry.somethingExists())
				entries.add(entry);
			progress.worked(1);
		}

		if (entries.size() == 0)
			return true;

		// If there are entries, ask the user -> divert to UI thread
		UIJob uiJob = new UIJob("") {	//$NON-NLS-1$

			public IStatus runInUIThread(IProgressMonitor monitor) {
				OverwriteDialog dialog = new OverwriteDialog(null, entries.toArray(new OverwriteEntry[0]));
				dialog.setBlockOnOpen(true);
				int result = dialog.open();
				if (result == 1)
					return Status.OK_STATUS;
				else
					return Status.CANCEL_STATUS;
			}
		};
		uiJob.setSystem(true);
		uiJob.schedule();
		try {
			uiJob.join();
			return uiJob.getResult().isOK();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}

	}


}
