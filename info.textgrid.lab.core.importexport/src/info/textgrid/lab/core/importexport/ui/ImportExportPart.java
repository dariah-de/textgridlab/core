package info.textgrid.lab.core.importexport.ui;

import info.textgrid._import.ImportObject;
import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.ImportModel;
import info.textgrid.lab.core.importexport.model.ImportModel.LocalFileStatus;
import info.textgrid.lab.core.importexport.model.RewriteSetup;
import info.textgrid.lab.core.metadataeditor.MetaDataView;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.AdapterUtils.AdapterNotFoundException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.internal.events.ResourceDelta;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.internal.part.NullEditorInput;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.wb.swt.ResourceManager;

import com.google.common.base.Predicate;

public abstract class ImportExportPart extends EditorPart {

	private final class AddObjectsJob extends Job {
		private final List<?> objects;
		private ImportEntry entry;

		private AddObjectsJob(final String name, final List<?> objects) {
			super(name);
			this.objects = objects;
		}

		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			final SubMonitor progress = SubMonitor.convert(monitor, objects.size() * 100);
			for (final Object o : objects) {
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;

				final TGObjectReference reference = AdapterUtils.getAdapter(o, TGObjectReference.class);
				if (reference != null)
					entry = getImportModel().addObject(reference, progress.newChild(100));
				else {
					final TextGridObject textGridObject = AdapterUtils.getAdapter(o, TextGridObject.class);
					if (textGridObject != null)
						entry = getImportModel().addObject(
								new TGObjectReference(textGridObject.getURI().toString(), textGridObject),
								progress.newChild(100));
				}

				new UIJob(Messages.ImportExportPart_Updating_Display) {

					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						TreeViewer viewer = getObjectsViewer();
						if (viewer.getControl().isDisposed())
							return Status.CANCEL_STATUS;

						viewer.refresh(true);
						if (entry != null)
							viewer.setSelection(new StructuredSelection(entry), true);
						return Status.OK_STATUS;
					}

				}.schedule();

			}
			return Status.OK_STATUS;
		}
	}

	private final class ImportModelContentProvider implements ITreeContentProvider {
		@Override
		public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public boolean hasChildren(final Object element) {
			// final ImportSpec importSpec = AdapterUtils.getAdapter(element,
			// ImportSpec.class);
			// return importSpec != null &&
			// !importSpec.getImportObject().isEmpty();
			if (element instanceof ImportModel && !((ImportModel) element).getImportObject().isEmpty())
				return true;
			else if (element instanceof ImportEntry)
				return !((ImportEntry) element).getChildEntries().isEmpty();
			else
				return false;
		}

		@Override
		public Object getParent(final Object element) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object[] getElements(final Object inputElement) {
			return getChildren(inputElement);
		}

		@Override
		public Object[] getChildren(final Object parentElement) {
			if (parentElement instanceof ImportModel)
				return ((ImportModel) parentElement).getChildren().toArray();
			else if (parentElement instanceof ImportEntry)
				return ((ImportEntry) parentElement).getChildEntries().toArray();
			else
				return new Object[0];
		}
	}

	private TargetProjectCombo projectCombo;
	private ImportModel importModel;
	private TreeViewer objectsViewer;
	private ITextGridObjectListener textGridObjectListener;
	protected boolean export;
	private Button performButton;
	private Composite content;

	protected Composite getContent() {
		return content;
	}

	protected ControlEnableState noProjectDisabledState;
	protected Group importGroup;
	private boolean dirty;
	private IResourceChangeListener resourceChangeListener;
	protected PageBook pages;
	protected ResultPage resultPage;
	private Button nextButton;

	public ImportExportPart() {
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		if (importModel != null) {
			final IFile file = AdapterUtils.getAdapter(getEditorInput(), IFile.class);
			if (file != null) {
				final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				try {
					importModel.save(new StreamResult(outputStream));
					file.setContents(new ByteArrayInputStream(outputStream.toByteArray()), true, true, monitor);
					setDirty(false);
				} catch (final CoreException e) {
					StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
				}
			} else if (getEditorInput() instanceof FileStoreEditorInput) {
				final URI targetURI = ((FileStoreEditorInput) getEditorInput()).getURI();
				try {
					final IFileStore store = EFS.getStore(targetURI);
					final OutputStream outputStream = store.openOutputStream(EFS.NONE, monitor);
					importModel.save(new StreamResult(outputStream));
					outputStream.close();
					setDirty(false);
				} catch (final CoreException e) {
					StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
				} catch (final IOException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(
									Messages.ImportExportPart_Save_Failed, targetURI,
									e.getLocalizedMessage()), e), StatusManager.LOG | StatusManager.SHOW);
				}
			} else
				doSaveAs();
		}
	}

	@Override
	public void doSaveAs() {
		int selection = new MessageDialog(getSite().getShell(), Messages.ImportExportPart_Save_As_Title, null,
				Messages.ImportExportPart_Save_As_What, MessageDialog.QUESTION_WITH_CANCEL, new String[] {
						Messages.ImportExportPart_TextGridRep, Messages.ImportExportPart_Local_File, Messages.ImportExportPart_Cancel }, isExport() ? 1 : 0).open();
		switch (selection) {
		case 0:
			saveAsTextGridObject(true);
			break;
		case 1:
			saveAsLocalFile();
			break;
		}
	}

	public void saveAsTextGridObject(final boolean forceNew) {
		TextGridObject object = null;
		if (getEditorInput() instanceof IFileEditorInput)
			object = AdapterUtils.getAdapter(((IFileEditorInput) getEditorInput()).getFile(), TextGridObject.class);
		if (object == null || forceNew) {
			object = TextGridObject.getNewObjectInstance(getImportModel().getTargetProject(),
					TGContentType.of(ImportPlugin.CONTENT_TYPE_ID));
			final InputDialog dialog = new InputDialog(
					getSite().getShell(),
					Messages.ImportExportPart_Save_as_TextGrid_Title,
					NLS.bind(
							Messages.ImportExportPart_Save_to_Target_Project,
							getImportModel().getTargetProject()), "", new IInputValidator() { //$NON-NLS-1$

						@Override
						public String isValid(final String newText) {
							if (newText == null || newText.trim().isEmpty())
								return Messages.ImportExportPart_Title_Validation_Warning;
							else
								return null;
						}
					});
			if (dialog.open() != Window.CANCEL)
				object.setTitle(dialog.getValue().trim());
			else
				return;

			final IEditorInput input = AdapterUtils.getAdapter(object, IEditorInput.class);
			setInputWithNotify(input);
		}
		new Job(NLS.bind(Messages.ImportExportPart_Saving_x, object)) {

			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				doSave(monitor);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		}.schedule();
	}

	public void saveAsLocalFile() {
		final FileDialog fileDialog = new FileDialog(getSite().getShell(), SWT.SAVE);
		fileDialog.setFilterExtensions(new String[] { "." //$NON-NLS-1$
				+ TGContentType.getContentType(ImportPlugin.CONTENT_TYPE_ID).getExtension() });
		fileDialog.setFilterNames(new String[] { TGContentType.getContentType(ImportPlugin.CONTENT_TYPE_ID).getDescription() });
		fileDialog.setText(Messages.ImportExportPart_SaveImportSpecAsTitle);
		final String fileName = fileDialog.open();
		if (fileName != null) {
			final URI fileURI = new File(fileName).toURI();
			try {
				final FileStoreEditorInput input = new FileStoreEditorInput(EFS.getStore(fileURI));
				setPartName(input.getName());
				setInputWithNotify(input);
				doSave(new NullProgressMonitor());
			} catch (final CoreException e) {
				StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
			}
		}
	}

	@Override
	public void init(final IEditorSite site, final IEditorInput input) throws PartInitException {
		setSite(site);
		resourceChangeListener = new IResourceChangeListener() {

			@Override
			@SuppressWarnings("restriction")
			// debugging output
			public void resourceChanged(final IResourceChangeEvent event) {
				if (getEditorInput() instanceof IFileEditorInput) {
					final IFile inputFile = ((IFileEditorInput) getEditorInput()).getFile();
					final IResourceDelta delta = event.getDelta().findMember(inputFile.getFullPath());
					if (delta != null)
						if ((delta.getFlags() & IResourceDelta.MOVED_TO) != 0) {
							final IPath newPath = delta.getMovedToPath();
							final IResource newFile = inputFile.getWorkspace().getRoot().findMember(newPath);
							new UIJob(Messages.ImportExportPart_Updating_input) {

								@Override
								public IStatus runInUIThread(final IProgressMonitor monitor) {
									setInputWithNotify(new FileEditorInput((IFile) newFile));
									return Status.OK_STATUS;
								}
							}.schedule();
						} else
							StatusManager.getManager().handle(
									new Status(IStatus.INFO, ImportPlugin.PLUGIN_ID,
											"There was a resource change event to our input which we didn't handle: " //$NON-NLS-1$
													+ ((ResourceDelta) delta).toDebugString()));
				}
			}
		};
		ResourcesPlugin.getWorkspace().addResourceChangeListener(resourceChangeListener, IResourceChangeEvent.POST_CHANGE);
		final IFile file = AdapterUtils.getAdapter(input, IFile.class);
		if (file != null)
			try {
				URI uri = file.getLocationURI();
				if (!uri.isAbsolute() || uri.getScheme().equals(TextGridObject.SCHEMA_EFS)) {
					final TextGridObject object = AdapterUtils.getAdapter(input, TextGridObject.class);
					uri = object.getURI();
				}
				importModel = ImportModel.load(new StreamSource(file
						.getContents(), uri.toString()));
				if (!export)
					importModel.initializeLoadedModelForImport(null);
			} catch (final JAXBException e) {
				throw new PartInitException(NLS.bind(Messages.ImportExportPart_Could_not_load_import_definition, input,
						e.getLocalizedMessage()), e);
			} catch (final CoreException e) {
				throw new PartInitException(NLS.bind(Messages.ImportExportPart_Could_not_load_import_definition, input,
						e.getLocalizedMessage()), e);
			}
		else if (input instanceof FileStoreEditorInput)
			// It's probably a local file
			try {
				URI uri = ((FileStoreEditorInput) input).getURI();
				final IFileStore fileStore = EFS.getStore(uri);
				importModel = ImportModel.load(new StreamSource(fileStore
						.openInputStream(EFS.NONE, new NullProgressMonitor()),
						uri.toString()));
				setPartName(input.getName());
				if (!export) {
					handleMissingFiles(input);
					importModel.initializeLoadedModelForImport(null);
				}
			} catch (final CoreException e) {
				throw new PartInitException(NLS.bind(Messages.ImportExportPart_Could_not_load_import_definition, input,
						e.getLocalizedMessage()), e);
			} catch (final JAXBException e) {
				throw new PartInitException(NLS.bind(Messages.ImportExportPart_Could_not_load_import_definition, input,
						e.getLocalizedMessage()), e);
			}
		else
			setPartName(Messages.ImportExportPart_GenericTitle);
		setInputWithNotify(input);
	}

	private void handleMissingFiles(final IEditorInput input) throws PartInitException {
		LocalFileStatus localFileStatus;
		while ((localFileStatus = importModel.checkLocalFiles(null)).allMissing()) {
			MessageDialog dialog = new MessageDialog(
					getSite().getShell(),
					NLS.bind(Messages.ImportExportPart_LoadingImex, input.getName()),
					null,
					NLS.bind(Messages.ImportExportPart_NoFileFoundChoice, localFileStatus.missingFiles.get(0)),
					MessageDialog.QUESTION,
					new String[] {
						Messages.ImportExportPart_SelectRootFolder,
						Messages.ImportExportPart_OpenForExport,
						Messages.ImportExportPart_OpenAnyway
					},
					1);
			dialog.setBlockOnOpen(true);
			int choice = dialog.open();
			switch(choice) {
			case 0:	// Select Folder
				DirectoryDialog directoryDialog = new DirectoryDialog(getSite().getShell());
				directoryDialog.setFilterPath(importModel.getRoot().toOSString());
				directoryDialog.setFilterPath(Messages.ImportExportPart_ImportRootTitle);
				directoryDialog.setMessage(NLS.bind(Messages.ImportExportPart_ImportRootPrompt, localFileStatus.missingFiles.get(0)));
				String newRoot = directoryDialog.open();
				if (newRoot != null)
					importModel.setRoot(new Path(newRoot));
				break;
			case 1: // Open Export Editor
				final IWorkbenchPage page = getSite().getWorkbenchWindow().getActivePage();
				page.openEditor(input, "info.textgrid.lab.core.importexport.ExportEditor"); //$NON-NLS-1$
				new UIJob("") { //$NON-NLS-1$
					
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						page.closeEditor(ImportExportPart.this, false);
						return Status.OK_STATUS;
					}
				}.schedule();
				return;
			default:
				return;
			}
		}
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return getEditorInput() != null && getImportModel() != null;
	}

	private void setErrorMessage(final String message) {
		// FIXME better implementation
		StatusManager.getManager().handle(new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, message),
				StatusManager.SHOW | StatusManager.LOG);
	}

	public ImportModel getImportModel() {
		if (importModel == null)
			importModel = new ImportModel();
		return importModel;
	}

	private void setObjectsViewer(final TreeViewer objectsViewer) {
		this.objectsViewer = objectsViewer;
	}

	TreeViewer getObjectsViewer() {
		return objectsViewer;
	}

	private static TreeViewerColumn makeColumn(final TreeViewer treeViewer, final int style, final String text,
			final String tooltip, final int width) {
		final TreeViewerColumn result = new TreeViewerColumn(treeViewer, style);
		result.getColumn().setText(text);
		result.getColumn().setToolTipText(tooltip);
		result.getColumn().setWidth(width);
		result.getColumn().setMoveable(true);
		return result;
	}

	protected void addSelection(final IStructuredSelection selection) {
		final List<?> list = selection.toList();

		AddObjectsJob addObjectsJob = new AddObjectsJob(Messages.ImportExportPart_Adding_objects, list);
		addObjectsJob.setUser(true);
		addObjectsJob.schedule();
	}

	@Override
	public void createPartControl(final Composite parent) {
		pages = new PageBook(parent, SWT.NONE);
		content = new Composite(pages, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(content);
		final GridLayout contentLayout = GridLayoutFactory.fillDefaults().create();
		content.setLayout(contentLayout);

		importGroup = new Group(content, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(importGroup);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(importGroup);

		createTopArea(importGroup);

		setObjectsViewer(new TreeViewer(importGroup, SWT.FULL_SELECTION));

		GridDataFactory.fillDefaults().grab(true, true).applyTo(getObjectsViewer().getTree());
		getObjectsViewer().getTree().setHeaderVisible(true);

		getObjectsViewer().addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				if (event.getSelection().isEmpty()) {
					MetaDataView metadataView = (MetaDataView) getSite().getPage().findView(MetaDataView.ID);
					if (metadataView != null)
						metadataView.setVisible(false);
				}
			}
		});

		createFileColumn(getObjectsViewer());
		createUriColumn(getObjectsViewer());
		createTitleColumn(getObjectsViewer());
		createFormatColumn(getObjectsViewer());
		createRewriteColumn(getObjectsViewer());

		getObjectsViewer().setContentProvider(new ImportModelContentProvider());

		getObjectsViewer().setInput(getImportModel());

		createAreaBelowObjectsViewer(content);

		final Composite targetArea = createTargetArea(content);
		createBottomButtons(targetArea);

		hookTextGridObjectListener();
		getSite().setSelectionProvider(getObjectsViewer());

		if (!export && getProjectCombo().getSelection().isEmpty()) {
			getProjectCombo().getControl().setFocus();
			noProjectDisabledState = ControlEnableState.disable(importGroup);
		}

		resultPage = new ResultPage(pages, this);
		pages.showPage(getContent());

	}

	protected void createTopArea(final Composite parent) {
	}

	protected void createAreaBelowObjectsViewer(final Composite content2) {
		// TODO Auto-generated method stub

	}

	protected abstract Composite createTargetArea(Composite content);

	private void hookTextGridObjectListener() {
		textGridObjectListener = new ITextGridObjectListener() {

			@Override
			public void textGridObjectChanged(final Event event, final TextGridObject object) {
				if (event == Event.METADATA_CHANGED)
					if (importModel != null && getObjectsViewer() != null)
						try {
							final ImportObject importObject = importModel.findEntry(new Predicate<ImportObject>() {
								@Override
								public boolean apply(final ImportObject input) {
									return input.getTextgridUri().equals(object.getURI().toString());
								}
							});
							new UIJob(Messages.ImportExportPart_Updating) {

								@Override
								public IStatus runInUIThread(final IProgressMonitor monitor) {
									if (getObjectsViewer().getTree() != null && !getObjectsViewer().getTree().isDisposed())
										getObjectsViewer().refresh(importObject);
									return Status.OK_STATUS;
								}
							}.schedule();

						} catch (final NoSuchElementException e) {
							// ignore: We don't care about changed objects
							// we don't have in our list
						}
			}

		};

		TextGridObject.addListener(textGridObjectListener);
	}

	private void createBottomButtons(final Composite targetComposite) {
		final Composite buttonBar = new Composite(targetComposite, SWT.NONE);
		buttonBar.setLayoutData(new GridData(SWT.END, SWT.FILL, false, true));
		final RowLayout layout = new RowLayout();
		layout.center = true;
		layout.wrap = false;
		buttonBar.setLayout(layout);

		performButton = new Button(buttonBar, SWT.PUSH);
		// performButton.setLayoutData(new GridData(SWT.END, SWT.CENTER, false,
		// false));
		performButton.setText(isExport() ? Messages.ImportExportPart_Export : Messages.ImportExportPart_Import);
		if (ImportPlugin.useNewGUI())
			performButton.setImage(ImportPlugin.getDefault().getImageRegistry().get(
					isExport() ? ImportPlugin.IMG_EXPORT : ImportPlugin.IMG_IMPORT));
		performButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				perform();
			}
		});
		getSite().getShell().setDefaultButton(performButton);

		if (ImportPlugin.useNewGUI())
			createNextButton(buttonBar);
	}

	protected abstract void perform();

	@Override
	public void dispose() {
		TextGridObject.removeListener(textGridObjectListener);
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(resourceChangeListener);
		super.dispose();
	}

	private void createFormatColumn(final TreeViewer importObjects) {
		final TreeViewerColumn contentTypeColumn = makeColumn(importObjects, SWT.LEFT, Messages.ImportExportPart_Format,
				Messages.ImportExportPart_Format_Desc, 100);

		contentTypeColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(final Object element) {
				return getContentType(element).getDescription();
			}

			@Override
			public Image getImage(final Object element) {
				return getContentType(element).getImage(false);
			}

			private TGContentType getContentType(final Object element) {
				final TextGridObject object = AdapterUtils.getAdapter(element, TextGridObject.class);
				if (object != null)
					try {
						return object.getContentType(false);
					} catch (final CoreException e) {
						StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
					}
				// Fallback
				return TGContentType.getContentType(TGContentType.UNKNOWN_ID);
			}
		});

		if (!isExport())
			contentTypeColumn.setEditingSupport(new EditingSupport(importObjects) {

				private ComboBoxViewerCellEditor editor;

				@Override
				protected CellEditor getCellEditor(final Object element) {
					if (editor == null) {
						editor = new ComboBoxViewerCellEditor(importObjects.getTree());
						editor.setContenProvider(new IStructuredContentProvider() {

							@Override
							public void dispose() {
							}

							@Override
							public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
							}

							@Override
							public Object[] getElements(final Object inputElement) {
								return TGContentType.getContentTypes(false);
							}

						});
						editor.setInput(TGContentType.class);
					}
					return editor;
				}

				@Override
				protected boolean canEdit(final Object element) {
					return AdapterUtils.getAdapter(element, TextGridObject.class) != null;
				}

				@Override
				protected Object getValue(final Object element) {
					try {
						return AdapterUtils.getAdapterChecked(element, TextGridObject.class).getContentType(true);
					} catch (final AdapterNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (final CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return null;
				}

				@Override
				protected void setValue(final Object element, final Object value) {
					final TextGridObject object = AdapterUtils.getAdapter(element, TextGridObject.class);
					if (object != null)
						// FIXME Hack here -- better encapsulate this somehow
						if (value == null) {
							// nothing selected, but maybe some text;
							final String mimeType = editor.getViewer().getCCombo().getText();
							if (mimeType != null && !"".equals(mimeType)) { //$NON-NLS-1$
								object.setContentType(TGContentType.of(mimeType.trim()));
								object.notifyMetadataEditor();
								editor.getViewer().refresh();
							}
						} else {
							object.setContentType((TGContentType) value);
							object.notifyMetadataEditor();
						}
				}

			});
	}

	private void createTitleColumn(final TreeViewer importObjects) {
		final TreeViewerColumn titleColumn = makeColumn(importObjects, SWT.LEFT, Messages.ImportExportPart_Title,
				Messages.ImportExportPart_Title_Desc, 200);

		titleColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(final Object element) {
				if (element instanceof ImportObject)
					try {
						return TextGridObject.getInstanceOffline(URI.create(((ImportObject) element).getTextgridUri())).getTitle();
					} catch (final CoreException e) {
						StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
					}
				return ""; //$NON-NLS-1$
			}
		});

		if (!isExport())
			titleColumn.setEditingSupport(new EditingSupport(importObjects) {

				private TextCellEditor editor = null;

				@Override
				protected void setValue(final Object element, final Object value) {
					final TextGridObject object = AdapterUtils.getAdapter(element, TextGridObject.class);
					if (value instanceof String) {
						object.setTitle((String) value);
						object.notifyMetadataEditor();
					}
				}

				@Override
				protected Object getValue(final Object element) {
					final TextGridObject object = AdapterUtils.getAdapter(element, TextGridObject.class);
					String title = null; // Fallback
					if (object != null)
						try {
							title = object.getTitle();
						} catch (final CoreException e) {
							StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
						}
					if (title == null)
						title = ""; //$NON-NLS-1$
					return title;
				}

				@Override
				protected CellEditor getCellEditor(final Object element) {
					if (editor == null)
						editor = new TextCellEditor(importObjects.getTree());
					return editor;
				}

				@Override
				protected boolean canEdit(final Object element) {
					return AdapterUtils.getAdapter(element, TextGridObject.class) != null;
				}
			});
	}

	private void createUriColumn(final TreeViewer importObjects) {
		final TreeViewerColumn uriColumn = makeColumn(importObjects, SWT.LEFT, Messages.ImportExportPart_URI,
				Messages.ImportExportPart_URI_Desc, 100);
		uriColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(final Object element) {
				if (element instanceof ImportEntry) {
					final ImportEntry entry = (ImportEntry) element;
					if (entry.isNewRevisionImport())
						return entry.getExistingObject().getLatestURI() + ".*"; //$NON-NLS-1$
					else
						return entry.getTextgridUri();
				} else
					return ""; //$NON-NLS-1$
			}

			@Override
			public Image getImage(final Object element) {
				if (element instanceof ImportEntry) {
					final ImportEntry entry = (ImportEntry) element;
					if (entry.isReimport())
						if (entry.isNewRevisionImport())
							return ImportPlugin.getDefault().getImageRegistry().get(ImportPlugin.IMG_REIMPORT_REVISION);
						else
							return ImportPlugin.getDefault().getImageRegistry().get(ImportPlugin.IMG_REIMPORT_NEW);
				}
				return super.getImage(element);
			}

			@Override
			public String getToolTipText(final Object element) {
				if (element instanceof ImportEntry) {
					final ImportEntry entry = (ImportEntry) element;
					if (entry.isReimport())
						if (entry.isNewRevisionImport())
							return NLS.bind(Messages.ImportExportPart_x_Reimport_New_Revision_y, entry.getLocalFile().getName(),
									entry.getExistingObject());
						else
							return NLS.bind(Messages.ImportExportPart_x_Reimport_New_Object, entry.getLocalFile().getName());
					else
						return NLS.bind(Messages.ImportExportPart_x_Import_New_Object, entry.getLocalFile().getName());
				}
				return super.getToolTipText(element);
			}

		});

	}

	private void createFileColumn(final TreeViewer importObjects) {
		final TreeViewerColumn fileColumn = makeColumn(importObjects, SWT.LEFT, Messages.ImportExportPart_File,
				Messages.ImportExportPart_File_Desc, 200);

		fileColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(final Object element) {
				if (element instanceof ImportObject) {
					final ImportObject o = (ImportObject) element;
					final Path path = new Path(o.getLocalData());
					return /*root == null? */path.toOSString() /*: path.makeRelativeTo(root).toOSString()*/;
				} else
					return element.toString();
			}
		});
		// if (isExport())
		// fileColumn.setEditingSupport(new EditingSupport(importObjects) {
		//
		// private DialogCellEditor editor;
		//
		// @Override
		// protected void setValue(final Object element, final Object value) {
		// if (element instanceof ImportObject && value instanceof String) {
		// ((ImportObject) element).setLocalData((String) value);
		//						((ImportObject) element).setLocalMetadata(value.toString().concat(".meta")); //$NON-NLS-1$
		// setDirty(true);
		// getViewer().refresh(element);
		// }
		// }
		//
		// @Override
		// protected Object getValue(final Object element) {
		// if (element instanceof ImportEntry) {
		// return ((ImportEntry) element).getLocalData();
		// }
		// return null;
		// }
		//
		// @Override
		// protected CellEditor getCellEditor(final Object element) {
		// if (editor == null)
		// editor = new DialogCellEditor(importObjects.getTree()) {
		//
		// // TODO add proper base path handling
		// // TODO maybe use an Edit control for fast
		// // manipulation?
		// // TODO maybe add file extension filters according
		// // to
		// // the content type
		// @Override
		// protected Object openDialogBox(final Control cellEditorWindow) {
		// final FileDialog dialog = new
		// FileDialog(cellEditorWindow.getShell());
		// dialog.setText(Messages.ImportExportPart_Export_to);
		// final IPath root = getImportModel().getRoot();
		// if (root != null)
		// dialog.setFilterPath(root.toOSString());
		// dialog.setFileName(getValue().toString());
		// final TGContentType[] contentTypes =
		// TGContentType.getContentTypes(false);
		// final String[] extensions = new String[contentTypes.length];
		// final String[] names = new String[contentTypes.length];
		// int index = 0;
		// for (int i = 0; i < contentTypes.length; i++) {
		// names[i] = contentTypes[i].getDescription();
		// extensions[i] = contentTypes[i].getExtension();
		// try {
		// if (contentTypes[i] == ((ImportEntry)
		// element).getObject().getContentType(false))
		// index = i;
		// } catch (final CoreException e) {
		// StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
		// }
		// }
		// dialog.setFilterExtensions(extensions);
		// dialog.setFilterNames(names);
		// dialog.setFilterIndex(index);
		//
		// final String selectedFilename = dialog.open();
		// if (selectedFilename != null && root == null) {
		// final Path selectedPath = new Path(selectedFilename);
		// getImportModel().setRoot(selectedPath.removeLastSegments(1));
		// }
		//
		// return selectedFilename;
		// }
		// };
		// return editor;
		// }
		//
		// @Override
		// protected boolean canEdit(final Object element) {
		// // TODO Auto-generated method stub
		// return element instanceof ImportObject;
		// }
		// });
	}

	private void createRewriteColumn(final TreeViewer importObjects) {
		final TreeViewerColumn rewriteColumn = makeColumn(importObjects, SWT.LEFT, Messages.ImportExportPart_Rewriting,
				Messages.ImportExportPart_Rewriting_Desc, 100);

		rewriteColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(final Object element) {
				if (element instanceof ImportEntry)
					return ((ImportEntry) element).getRewriteSetup().toString();
				return super.getText(element);
			}

		});
		rewriteColumn.setEditingSupport(new EditingSupport(importObjects) {

			private ComboBoxViewerCellEditor editor;

			@Override
			protected void setValue(final Object element, final Object value) {
				if (element instanceof ImportEntry && value instanceof RewriteSetup) {
					((ImportEntry) element).setRewriteSetup((RewriteSetup) value);
					importObjects.refresh(element);
					setDirty(true);
				}
				if (value == null) {
					// some text that has been entered manually.
					String text = editor.getViewer().getCCombo().getText();
					if (element instanceof ImportEntry)
						try {
							URI uri = new URI(text.trim());
							RewriteSetup setup = new RewriteSetup(getImportModel(), uri);
							final List<RewriteSetup> setups = getImportModel().getAvailableSetups();
							if (!setups.contains(setup)) {
								setups.add(setup);
								editor.setInput(setups);
							}
							((ImportEntry) element).setRewriteSetup(setup);
						} catch (URISyntaxException e) {
							StatusManager.getManager().handle(
									new Status(IStatus.ERROR, ImportPlugin.PLUGIN_ID, NLS.bind(Messages.ImportExportPart_RewriteSetupNotURI,
											text), e));

						}
				}
			}

			@Override
			protected Object getValue(final Object element) {
				if (element instanceof ImportEntry)
					return ((ImportEntry) element).getRewriteSetup();
				return null;
			}

			@Override
			protected CellEditor getCellEditor(final Object element) {
				if (editor == null) {
					editor = new ComboBoxViewerCellEditor(importObjects.getTree());
					editor.setContenProvider(new ArrayContentProvider());
					editor.setInput(getImportModel().getAvailableSetups());
				}
				return editor;
			}

			@Override
			protected boolean canEdit(final Object element) {
				if (element instanceof ImportObject) {
					ImportObject importObject = (ImportObject) element;
					if (!importObject.getRewriteMethod().equals(RewriteMethod.NONE))
						return true; // always allow resetting it ...
					if (importObject instanceof ImportEntry) {
						ImportEntry entry = (ImportEntry) importObject;
						String mimeType;
						try {
							mimeType = entry.getObject().getContentType(true).getId();
						if (mimeType.contains("xml") || mimeType.startsWith("text/"))
							return true;
						else
							return false;
						} catch (CoreException e) {
							StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
						}
					}
					return true;	// in case of doubt: allow;
				} else
					return false;
			}
		});
	}

	@Override
	public void setFocus() {
		if(content != null)
			content.setFocus();
	}

	void setDirty(final boolean dirty) {
		this.dirty = dirty;
		UIJob uiJob = new UIJob("Set dirty") { //$NON-NLS-1$

			@Override
			public IStatus runInUIThread(final IProgressMonitor monitor) {
				firePropertyChange(PROP_DIRTY);
				return Status.OK_STATUS;
			}
		};
		uiJob.setSystem(true);
		uiJob.schedule();
	}

	protected void setExport(final boolean export) {
		this.export = export;
	}

	public void setProjectCombo(final TargetProjectCombo projectCombo) {
		this.projectCombo = projectCombo;
	}

	public TargetProjectCombo getProjectCombo() {
		return projectCombo;
	}

	protected boolean isExport() {
		return export;
	}

	// public for testing
	@SuppressWarnings("restriction")
	@Override
	public void setInput(final IEditorInput input) {
		super.setInput(input);
		String title;
		if (input instanceof NullEditorInput)
			title = isExport() ? Messages.ImportExportPart_Export_Part_Title : Messages.ImportExportPart_Import_Part_Title;
		else
			title = NLS.bind(isExport() ? Messages.ImportExportPart_Export_Part_Title_File : Messages.ImportExportPart_Import_Part_Title_File, input.getName());

		setPartName(title);
		setContentDescription(input.getToolTipText());
		firePropertyChange(PROP_TITLE);
	}

	@Override
	// super sets the field directly :-(
	protected void setInputWithNotify(final IEditorInput input) {
		setInput(input);
		firePropertyChange(PROP_INPUT);
	}

	protected void createRemoveButton(final Composite topGroup) {
		final Button remButton = new Button(topGroup, SWT.FLAT);
		remButton.setLayoutData(new GridData(SWT.END, SWT.CENTER, false, false));
		remButton.setText(Messages.ImportExportPart_Remove);
		remButton.setToolTipText(Messages.ImportExportPart_Remove_Tooltip);
		remButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				final ITreeSelection selection = (ITreeSelection) getObjectsViewer().getSelection();
				for (final TreePath path : selection.getPaths())
					getImportModel().remove(path);
				getObjectsViewer().refresh();
			}

		});
	}

	protected void createNextButton(final Composite targetArea) {
		nextButton = new Button(targetArea, SWT.FLAT);
		getNextButton().setImage(ResourceManager.getPluginImage("org.eclipse.ui", "/icons/full/elcl16/forward_nav.gif")); //$NON-NLS-1$ //$NON-NLS-2$
		getNextButton().addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				pages.showPage(pages.getChildren()[1]);
			}
		});
		getNextButton().setEnabled(false);
	}

	protected Button getNextButton() {
		return nextButton;
	}


}
