package info.textgrid.lab.core.importexport.ui;

import info.textgrid.lab.core.importexport.ImportPlugin;
import info.textgrid.lab.core.importexport.model.ImportExportStatus;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.wb.swt.ResourceManager;

public class ResultPage extends Composite {
	private TreeViewer resultViewer;
	private Label summaryLabel;
	private Button back;
	private ImportExportPart editor;
	private boolean isImport;

	private final class StatusSeverityLabelProvider extends ColumnLabelProvider {
		@Override
		public Image getImage(final Object element) {
			if (element instanceof IStatus) {
				final IStatus status = (IStatus) element;
				switch (status.getSeverity()) {
				case IStatus.OK:
					return ImportPlugin.getDefault().getImageRegistry().get(ImportPlugin.IMG_STATUS_OK);
				case IStatus.INFO:
					return ImportPlugin.getDefault().getImageRegistry().get(ImportPlugin.IMG_STATUS_INFO);
				case IStatus.WARNING:
					return ImportPlugin.getDefault().getImageRegistry().get(ImportPlugin.IMG_STATUS_WARNING);
				case IStatus.ERROR:
					return ImportPlugin.getDefault().getImageRegistry().get(ImportPlugin.IMG_STATUS_ERROR);
				}
			}
			return null;
		}

		@Override
		public String getText(final Object element) {
			if (element instanceof IStatus) {
				final IStatus status = (IStatus) element;
				switch (status.getSeverity()) {
				case IStatus.OK:
					return Messages.ResultPage_Success;
				case IStatus.INFO:
					return Messages.ResultPage_Info;
				case IStatus.WARNING:
					return Messages.ResultPage_Warning;
				case IStatus.ERROR:
					return Messages.ResultPage_Error;
				case IStatus.CANCEL:
					return Messages.ResultPage_Cancelled;
				}
			}
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * A content provider for trees if {@link IStatus} elements.
	 */
	private final class StatusTreeContentProvider implements ITreeContentProvider {

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
		}

		@Override
		public Object[] getElements(final Object inputElement) {
			return getChildren(inputElement);
		}

		@Override
		public Object[] getChildren(final Object parentElement) {
			if (parentElement instanceof IStatus) {
				final IStatus status = (IStatus) parentElement;
				return status.getChildren();
			}
			return new IStatus[0];
		}

		@Override
		public Object getParent(final Object element) {
			return null;
		}

		@Override
		public boolean hasChildren(final Object element) {
			if (element instanceof IStatus)
				return ((IStatus) element).isMultiStatus();
			return false;
		}
	}

	/**
	 * Create contents of the PageBookView Page.
	 * 
	 * @param parent
	 * @param editor
	 *            TODO
	 */
	public ResultPage(final Composite parent, final ImportExportPart editor) {
		super(parent, SWT.NONE);
		this.editor = editor;
		this.isImport = (editor instanceof ImportPart);

		setLayout(new GridLayout(1, false));

		final CLabel titleLable = new CLabel(this, SWT.NONE);
		titleLable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		titleLable.setText(isImport ? Messages.ResultPage_Import_Finished
				: Messages.ResultPage_Export_Finished);

		summaryLabel = new Label(this, SWT.WRAP);
		summaryLabel
				.setText(isImport ? Messages.ResultPage_Import_Some_Problems
						: Messages.ResultPage_Export_Some_Problems);

		resultViewer = new TreeViewer(this, SWT.BORDER | SWT.FULL_SELECTION);
		final Tree tree = resultViewer.getTree();
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		final TreeViewerColumn statusColumn = new TreeViewerColumn(resultViewer, SWT.LEFT);
		statusColumn.setLabelProvider(new StatusSeverityLabelProvider());
		final TreeColumn tblclmnNewColumn = statusColumn.getColumn();
		tblclmnNewColumn.setMoveable(true);
		tblclmnNewColumn.setWidth(63);
		tblclmnNewColumn.setText(Messages.ResultPage_Status);

		final TreeViewerColumn messageColumn = new TreeViewerColumn(resultViewer, SWT.NONE);
		final TreeColumn tblclmnMessage = messageColumn.getColumn();
		tblclmnMessage.setWidth(290);
		tblclmnMessage.setText(Messages.ResultPage_Message);
		messageColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(final Object element) {
				if (element instanceof IStatus)
					return ((IStatus) element).getMessage();
				else
					return super.getText(element);
			}

		});

		final TreeViewerColumn obectColumn = new TreeViewerColumn(resultViewer, SWT.NONE);
		final TreeColumn tblclmnObject = obectColumn.getColumn();
		tblclmnObject.setWidth(100);
		tblclmnObject.setText(Messages.ResultPage_URI);
		obectColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public Image getImage(final Object element) {
				if (element instanceof ImportExportStatus) {
					final ImportExportStatus status = (ImportExportStatus) element;
					if (status.getEntry() != null && status.getEntry().getObject() != null)
						try {
							return status.getEntry().getObject().getContentType(false).getImage(true);
						} catch (final CoreException e) {
							StatusManager.getManager().handle(e, ImportPlugin.PLUGIN_ID);
						}
				}
				return null;
			}

			@Override
			public String getText(final Object element) {
				if (element instanceof ImportExportStatus) {
					final ImportExportStatus status = (ImportExportStatus) element;
					return status.getEntry().getTextgridUri();
				}
				return null;
			}

		});

		final TreeViewerColumn fileColumn = new TreeViewerColumn(resultViewer, SWT.NONE);
		final TreeColumn tblclmnFile = fileColumn.getColumn();
		tblclmnFile.setWidth(100);
		tblclmnFile.setText(Messages.ResultPage_File);
		fileColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(final Object element) {
				if (element instanceof ImportExportStatus) {
					final ImportExportStatus status = (ImportExportStatus) element;
					if (status.getEntry() != null)
						return status.getEntry().getLocalData();
				}
				return null;
			}

		});

		resultViewer.setContentProvider(new StatusTreeContentProvider());

		final Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		final Label saveLabel = new Label(this, SWT.WRAP);
		saveLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		saveLabel.setText(Messages.ResultPage_SaveInfo);

		final Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(4, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 1, 1));

		back = new Button(composite, SWT.FLAT);
		back.setImage(ResourceManager.getPluginImage("org.eclipse.ui", "/icons/full/elcl16/backward_nav.gif")); //$NON-NLS-1$ //$NON-NLS-2$
		back.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(final SelectionEvent e) {
				if (parent instanceof PageBook) {
					final PageBook pages = (PageBook) parent;
					pages.showPage(pages.getChildren()[0]);
				}
			}
			
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				widgetSelected(e);
			}
		});

		final Label space = new Label(composite, SWT.SHADOW_NONE | SWT.CENTER);
		space.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 1, 1));

		final Button btnSaveToTextgrid = new Button(composite, SWT.NONE);
		btnSaveToTextgrid.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				editor.saveAsTextGridObject(false);
			}
		});
		btnSaveToTextgrid.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnSaveToTextgrid.setImage(ResourceManager.getPluginImage("org.eclipse.ui", "/icons/full/etool16/save_edit.gif")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSaveToTextgrid.setText(Messages.ResultPage_Save_to_TextGrid);

		final Button btnSaveToFile = new Button(composite, SWT.NONE);
		btnSaveToFile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnSaveToFile.setImage(ResourceManager.getPluginImage("info.textgrid.lab.ui.core", "icons/017-speichere-DateiLokal2.gif")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSaveToFile.setText(Messages.ResultPage_Save_to_File);
		btnSaveToFile.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				editor.saveAsLocalFile();
			}

		});
	}

	public void setInput(final IStatus result) {
		if (!isDisposed()) {
			if (result.isOK())
				summaryLabel
						.setText(isImport ? Messages.ResultPage_Import_Successful
								: Messages.ResultPage_Export_Successful);
			else
				summaryLabel
						.setText(isImport ? Messages.ResultPage_Import_Some_Problems
								: Messages.ResultPage_Export_Some_Problems);
			resultViewer.setInput(result);
		}
	}
}