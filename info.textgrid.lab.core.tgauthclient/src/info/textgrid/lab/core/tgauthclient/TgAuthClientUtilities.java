package info.textgrid.lab.core.tgauthclient;

import info.textgrid.lab.conf.ConfPlugin;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.Tgextra;

import java.net.URL;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Some utility methods for TGAuth clients.
 * @TODO need to add proxy settings as they were present in RBACClientUtilies
 */
public class TgAuthClientUtilities {

	private static Tgextra service;

	protected static Tgextra getService() {
		if (service == null) {
			final URL wsdlLocation = PortTgextra.class.getResource("/wsdl/tgextra.wsdl");
			Assert.isNotNull(wsdlLocation, "tgextra.wsdl not found -- this is probaby a bug");
			service = new Tgextra(wsdlLocation, new QName(
					"http://textgrid.info/namespaces/middleware/tgauth",
					"tgextra"));
		}
		return service;
	}

	/**
	 * Please use {@link #getCrudServiceStub()} instead.
	 * 
	 * @return
	 * @see #getCrudServiceStub()
	 */
	private static String getTGauthEPR() {

		try {
			return ConfClient.getInstance().getValue(
					ConfservClientConstants.TG_AUTH);
		} catch (OfflineException e) {
			OnlineStatus.netAccessFailed(
					"Could not retrieve TG-Auth EPR from configuration server",
					e);
		}
		return null;
	}

	/**
	 * Always use this method to get a configured serviceStub ...
	 * 
	 * @return a service stub with current timeout/proxy options
	 */
	public static PortTgextra getTgAuthServiceStub() {
		PortTgextra stub = getService().getTgextra();
		BindingProvider bindingProvider = (BindingProvider) stub;
		Map<String, Object> requestContext = bindingProvider
				.getRequestContext();
		requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getTGauthEPR());
		// Timeouts in milliseconds
		bindingProvider.getRequestContext().put(
				"com.sun.xml.internal.ws.request.timeout", 60000);
		bindingProvider.getRequestContext().put(
				"com.sun.xml.internal.ws.connect.timeout", 60000);
		bindingProvider.getRequestContext().put(
				"com.sun.xml.ws.request.timeout", 60000);
		bindingProvider.getRequestContext().put(
				"com.sun.xml.ws.connect.timeout", 60000);
		
		Client client = ClientProxy.getClient(stub);
		
        IPreferenceStore preferenceStore = ConfPlugin.getDefault().getPreferenceStore();
        if(preferenceStore.getBoolean(ConfPlugin.COMPRESSED_TRANSFER)) {
        	client.getInInterceptors().add(new GZIPInInterceptor());
        	client.getOutInterceptors().add(new GZIPOutInterceptor());
        }

		return stub;
	}

}
