package info.textgrid.lab.core.tgauthclient;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class TGAuthClientPlugin extends Plugin implements BundleActivator {
	
	private static TGAuthClientPlugin instance; 

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;
	}
	
	
	public static TGAuthClientPlugin getDefault() {
		return instance;
	}
	

	
	
}
