package info.textgrid.lab.core.swtutils;

import java.lang.reflect.Array;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;

/**
 * Generics aware utilities to deal with <a href=
 * "http://www.eclipse.org/articles/article.php?file=Article-Adapters/index.html"
 * >Eclipse's implementation of the Adapter pattern</a>.
 */
public abstract class AdapterUtils {

	/**
	 * Thrown by {@link AdapterUtils#getAdapterChecked(Object, Class)} when no
	 * adapter has been found.
	 * 
	 * @see AdapterUtils#getAdapterChecked(Object, Class)
	 */
	public static class AdapterNotFoundException extends CoreException {

		public Class<?> getType() {
			return type;
		}

		public Object getSource() {
			return source;
		}

		private static final long serialVersionUID = -4244601168380258375L;
		private Class<?> type;
		private Object source;

		public AdapterNotFoundException(Object source, Class<?> type) {
			super(new Status(IStatus.ERROR, Activator.PLUGIN_ID, MessageFormat.format(
					"Could not adapt {0} ({1}) to {2}.", source, source.getClass(), type)));
			this.source = source;
			this.type = type;
		}

	}

	/**
	 * Returns the given <var>source</var> object adapted to the
	 * <var>type</var>, or null if no adapter has been found.
	 * 
	 * This method exists for two reasons: It works even with sources that do
	 * not implement {@link IAdaptable} (which is perfectly OK for the
	 * platform's {@link IAdapterManager}), and it uses generics to avoid all
	 * these class cast orgies. You can simply write something like:
	 * 
	 * <pre>
	 * TargetType t = AdapterUtils.getAdapter(sourceObject, TargetType.class)
	 * </pre>
	 * 
	 * 
	 * @param <T>
	 *            The type to adapt to.
	 * @param source
	 *            The object that needs to be adapted.
	 * @param type
	 *            The class of the target type.
	 * @return The <var>type/var> adapter for <var>source</var>, or
	 *         <code>null</code> if none could be found. Note that his does not
	 *         load plugins.
	 */
	@SuppressWarnings("unchecked")
	// Checked by generics and adapter semantics
	public static <T> T getAdapter(Object source, Class<T> type) {
		
		if (source == null)
			return null;

		if (type.isInstance(source))
			return (T) source;

		if (source instanceof IAdaptable)
			return (T) ((IAdaptable) source).getAdapter(type);

		return (T) Platform.getAdapterManager().getAdapter(source, type);
	}

	/**
	 * A version of {@link #getAdapter(Object, Class)} that throws an
	 * {@link AdapterNotFoundException} if no adapter could be found.
	 * 
	 * @param <T>
	 *            The target type
	 * @param source
	 *            The object to adapt
	 * @param type
	 *            The target type's class
	 * @return The source object adapted to the target type, never null
	 * @throws AdapterNotFoundException
	 *             if no adapter coud be found
	 */
	public static <T> T getAdapterChecked(Object source, Class<T> type)
			throws AdapterNotFoundException {
		
		T adapter = getAdapter(source, type);
		if (adapter == null)
			throw new AdapterNotFoundException(source, type);
		else
			return adapter;
	}

	/**
	 * Adapts a whole array of items to the target type.
	 * 
	 * @param <T>
	 *            The type of the individual target objects.
	 * @param sources
	 *            An array of source objects to convert.
	 * @param type
	 *            The class of the individual target objects.
	 * @param withNulls
	 *            Determines what happens with objects that adapt to
	 *            <code>null</code>: If <code>true</code>, insert a
	 *            <code>null</code> in the result array, if <code>false</code>,
	 *            omit the item from the result array.
	 * @return The array of adapters for the source objects.
	 * @see #getAdapter(Object, Class)
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] getAdapters(Object[] sources, Class<T> type,
			boolean withNulls) {
		List<T> result = new ArrayList<T>(sources.length);
		for (Object source : sources) {
			T target = getAdapter(source, type);
			if (withNulls || target != null)
				result.add(target);
		}
		return result.toArray((T[]) Array.newInstance(type, 0));
	}

}
