package info.textgrid.lab.core.swtutils;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;

/**
 * An {@link IStatus} object that wraps an embedded CoreException.
 * 
 * If you need to convert a {@link CoreException} to an {@link IStatus}, it is
 * not always a good idea to use <code>e.{@linkplain CoreException#getStatus()
 * getStatus()}</code>, since you would lose the stack trace. This wrapper is a
 * MultiStatus that embeds the given CoreException and does remember the stack
 * trace (in the outer status).
 * 
 * @author vitt
 */
public class CoreExceptionStatus extends MultiStatus implements IStatus {

	public CoreExceptionStatus(final String pluginID, final CoreException cause, final String message, final Object... args) {
		super(pluginID, 0, new IStatus[] { cause.getStatus() }, MessageFormat.format(message, args), cause);
	}

	public CoreExceptionStatus(final CoreException cause) {
		this(cause.getStatus().getPlugin(), cause, cause.getMessage());
	}

	public CoreExceptionStatus(final CoreException cause, final String message, final Object... args) {
		this(cause.getStatus().getPlugin(), cause, MessageFormat.format(message, args));
	}

	public CoreExceptionStatus(final String pluginID, final CoreException cause) {
		this(pluginID, cause, cause.getMessage());
	}

}
