package info.textgrid.lab.core.swtutils;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * A small tool helping to log to the Eclipse log file. Instantiate with the
 * plugin id and then call any of the log*(...) method. As a convenience, this
 * will also return the created status object.
 */
public class LogUtil {

	public int getStyle() {
		return style;
	}

	public void setStyle(int style) {
		this.style = style;
	}

	public String getPluginId() {
		return pluginId;
	}

	private String pluginId;
	private int style = StatusManager.LOG;

	public LogUtil(String pluginId) {
		this.pluginId = pluginId;
	}

	/**
	 * Creates and handles a status object.
	 * 
	 * @param severity the severity of the error. This is one of {@link IStatus}' severity constants, see {@link IStatus#getSeverity()}.
	 * @param cause an exception that has caused the problem. May be <code>null</code> if message is not <code>null</code>. If this is a {@link CoreException}, the method will generate a MultiStatus containing the nested status object and the severity argument will be ignored. 
	 * @param message a message describing the problem to log. This may contain patterns as described for {@link MessageFormat#format(String, Object...)}. It may be null if a cause has been given. 
	 * @param arguments optional arguments that get merged into the message as described at {@link MessageFormat#format(String, Object...)}.
	 * @return the status object that has been created and logged.
	 */
	public IStatus log(final int severity, final Throwable cause,
			final String message, final Object... arguments) {
		String msg;
		IStatus status;
		if (message != null) {
			msg = MessageFormat.format(message, arguments);
		} else if (cause != null) {
			msg = cause.getMessage();
			if (msg == null || msg.length() == 0)
				msg = cause.getClass().getSimpleName();
		} else
			throw new IllegalArgumentException(
					"You cannot log an error without providing at least an error message or a cause.");

		if (cause instanceof CoreException) {
			status = new MultiStatus(pluginId, 0,
					new IStatus[] { ((CoreException) cause).getStatus() }, msg, cause);
		} else {
			status = new Status(severity, pluginId, msg, cause);
		}
		StatusManager.getManager().handle(status, style);

		return status;
	}

	public IStatus logError(final Throwable e, final String message,
			final Object... arguments) {
		return log(IStatus.ERROR, e, message, arguments);
	}

	public IStatus logError(final Throwable e) {
		return log(IStatus.ERROR, e, null);
	}

	public IStatus log(int severity, final String message,
			final Object... arguments) {
		return log(severity, null, message, arguments);
	}

	public IStatus logError(final String message, final Object... arguments) {
		return log(IStatus.ERROR, message, arguments);
	}

	public IStatus trace(final String message, final Object... arguments) {
		return log(IStatus.INFO, message, arguments);
	}
}
