package info.textgrid.lab.core.swtutils.annotations;

import java.util.Iterator;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelExtension2;
import org.eclipse.jface.viewers.AbstractTableViewer;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

public class AnnotationViewerController implements ISelectionChangedListener {

	private AbstractTableViewer viewer;
	private ITextEditor textEditor;
	private IAnnotationModel annotationModel;

	private boolean synchronizingSelection = true;

	public AnnotationViewerController(AbstractTableViewer viewer) {
		this.setViewer(viewer);
	}

	protected void setViewer(AbstractTableViewer viewer) {
		this.viewer = viewer;
	}

	public AbstractTableViewer getViewer() {
		return viewer;
	}

	public ITextEditor getTextEditor() {
		return textEditor;
	}

	public IAnnotationModel getAnnotationModel() {
		return annotationModel;
	}

	protected void associateWith(ITextEditor textEditor) {

		removeEditorSelectionListener();

		this.textEditor = textEditor;
		IDocumentProvider documentProvider = textEditor.getDocumentProvider();
		if (documentProvider != null) {
			annotationModel = documentProvider.getAnnotationModel(textEditor.getEditorInput());
			getViewer().setInput(annotationModel);
		}

		if (synchronizingSelection)
			registerEditorSelectionListener();
	}

	private void registerEditorSelectionListener() {
		if (this.textEditor != null && this.textEditor.getSelectionProvider() != null) {
			ISelectionProvider selectionProvider = this.textEditor.getSelectionProvider();
			if (selectionProvider instanceof IPostSelectionProvider)
				((IPostSelectionProvider) selectionProvider).addPostSelectionChangedListener(this);
			else
				selectionProvider.addSelectionChangedListener(this);
		}
	}

	private void removeEditorSelectionListener() {
		if (this.textEditor != null && this.textEditor.getSelectionProvider() != null) {
			ISelectionProvider selectionProvider = this.textEditor.getSelectionProvider();
			if (selectionProvider instanceof IPostSelectionProvider)
				((IPostSelectionProvider) selectionProvider).removePostSelectionChangedListener(this);
			else
				selectionProvider.removeSelectionChangedListener(this);
		}
	}

	/**
	 * @return the current active document in the current editor
	 */
	public IDocument getCurrentDocument() {
		if (textEditor != null) {
			IDocumentProvider documentProvider = textEditor.getDocumentProvider();
			if (documentProvider != null)
				return documentProvider.getDocument(textEditor.getEditorInput());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public void selectionChanged(SelectionChangedEvent event) {
		if (synchronizingSelection) {
			if (event.getSelection() instanceof ITextSelection) {
				ITextSelection textSelection = (ITextSelection) event.getSelection();
				if (annotationModel instanceof IAnnotationModelExtension2) {
					Iterator<Annotation> iterator = ((IAnnotationModelExtension2) annotationModel).getAnnotationIterator(
							textSelection.getOffset(),
							textSelection.getLength(), true, true);
					if (iterator.hasNext()) {
						Annotation annotation = iterator.next();
						viewer.setSelection(new StructuredSelection(annotation), true);
					}
				}
			}
		}
	}

}
