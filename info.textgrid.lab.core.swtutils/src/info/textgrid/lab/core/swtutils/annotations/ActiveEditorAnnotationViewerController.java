/**
 * 
 */
package info.textgrid.lab.core.swtutils.annotations;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.jface.viewers.AbstractTableViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.texteditor.ITextEditor;

public class ActiveEditorAnnotationViewerController extends AnnotationViewerController implements IPartListener {

	private Object activeEditor;
	
	public ActiveEditorAnnotationViewerController(AbstractTableViewer viewer) {
		super(viewer);
	}

	public void partActivated(IWorkbenchPart part) {
		if (part instanceof IEditorPart) {
			setActiveEditor((IEditorPart) part);
			ITextEditor textEditor = AdapterUtils.getAdapter(getActiveEditor(), ITextEditor.class);
			if (textEditor != null) {
				associateWith(textEditor);
			}
		}
	}

	public void partBroughtToTop(IWorkbenchPart part) {}

	public void partClosed(IWorkbenchPart part) {}

	public void partDeactivated(IWorkbenchPart part) {}

	public void partOpened(IWorkbenchPart part) {}

	private void setActiveEditor(final Object activeEditor) {
		if (this.activeEditor == activeEditor)
			return;

		if (activeEditor != null) {
		}

		this.activeEditor = activeEditor;
	}

	private Object getActiveEditor() {
		return activeEditor;
	}

}