package info.textgrid.lab.core.swtutils.annotations;

import info.textgrid.lab.core.swtutils.Activator;

import java.util.Iterator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.AnnotationModelEvent;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelListener;
import org.eclipse.jface.text.source.IAnnotationModelListenerExtension;
import org.eclipse.jface.viewers.AbstractTableViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.progress.UIJob;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;

/**
 * A content provider to display {@link Annotation}s.
 * 
 * The content provider can be associated with {@link AbstractTableViewer}s that
 * are fed with {@link IAnnotationModel}s. The content provider with
 * automatically update the {@link AbstractTableViewer} when the
 * {@link IAnnotationModel} changes.
 * 
 * <p>
 * If you don't want to have all annotations the model delivers in your viewer, you can {@linkplain #setFilter(Predicate) assign a filter to this viewer}.
 * </p>
 * 
 * @author tv
 * 
 */
public class AnnotationContentProvider implements IStructuredContentProvider, IAnnotationModelListenerExtension,
		IAnnotationModelListener {

	private AbstractTableViewer viewer;
	
	private Predicate<Annotation> filter;

	private IAnnotationModel annotationModel;

	/**
	 * Sets a filter for this content provider. The content provider will only
	 * provide annotations for which the filter returns <code>true</code> to the
	 * associated viewer. <code>null</code> means no filtering.
	 * 
	 * <p>
	 * If you call this method after you have initialized the associated viewer,
	 * the viewer will be {@linkplain Viewer#refresh() refreshed}.
	 * </p>
	 */
	public void setFilter(final Predicate<Annotation> filter) {
		this.filter = filter;
		if (viewer != null)
			viewer.refresh();
	}

	/**
	 * If a {@linkplain #setFilter(Predicate) filter has been set}, return only
	 * those elements of the input for which the filter returns
	 * <code>true</code>. Returns the unmodified input otherwise.
	 */
	protected Iterator<Annotation> filter(final Iterator<Annotation> unfiltered) {
		if (filter == null)
			return unfiltered;
		else
			return Iterators.filter(unfiltered, filter);
	}

	/**
	 * If a {@linkplain #setFilter(Predicate) filter has been set}, return only
	 * those elements of the input for which the filter returns
	 * <code>true</code>. Returns the unmodified input otherwise.
	 * 
	 * <p>
	 * Note that this method creates an iterator over the input array and
	 * allocates a new array after filtering, so if your input isn't an array,
	 * use {@link #filter(Iterator)}.
	 * </p>
	 */
	protected Annotation[] filter(final Annotation[] unfiltered) {
		if (filter == null) {
			return unfiltered; 
		} else {
			return Iterators.toArray(Iterators.filter(Iterators.forArray(unfiltered), filter), Annotation.class);
		}
	}

	public void dispose() {
		if (annotationModel != null) {
			annotationModel.removeAnnotationModelListener(this);
		}
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

		Assert.isLegal(viewer instanceof AbstractTableViewer, "AnnotationContentProvider must be used with a Table Viewer.");
		this.viewer = (AbstractTableViewer) viewer;

		if (oldInput != null && oldInput instanceof IAnnotationModel) {
			((IAnnotationModel) oldInput).removeAnnotationModelListener(this);
			annotationModel = null;
		}

		if (newInput != null && newInput instanceof IAnnotationModel) {
			annotationModel = (IAnnotationModel) newInput;
			annotationModel.addAnnotationModelListener(this);
		}

		if (!viewer.getControl().isDisposed())
			viewer.refresh();
	}

	@SuppressWarnings("unchecked")
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof IAnnotationModel) {
			Iterator annotationIterator = ((IAnnotationModel) inputElement).getAnnotationIterator();
			Annotation[] annotations = Iterators.toArray(filter(annotationIterator), Annotation.class);
			return annotations;
		}
		return null;
	}

	public void modelChanged(final AnnotationModelEvent event) {
		UIJob refreshJob = new UIJob("Refreshing annotation view") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
				if (viewer == null || viewer.getControl().isDisposed())
					return Status.CANCEL_STATUS;

				if (event.isWorldChange())
					viewer.refresh();
				else {
					viewer.add(filter(event.getAddedAnnotations()));
					viewer.remove(filter(event.getRemovedAnnotations()));
					for (Annotation changedAnnotation : filter(event.getChangedAnnotations()))
						viewer.refresh(changedAnnotation, true);
				}
				return Status.OK_STATUS;
				} catch (RuntimeException e) {
					Activator.handleError(e, "Exception refreshing annotation view for event {0}", event);
					throw e;
				}
			}
		};
		refreshJob.setSystem(true);
		refreshJob.schedule();
	}

	public void modelChanged(IAnnotationModel model) {
		viewer.refresh();
	}

}
