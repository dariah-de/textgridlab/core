package info.textgrid.lab.core.swtutils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Text;

/**
 * A text field decorator displays a string label in an empty text field, as
 * long as the field is not focused.
 * 
 * @author tv
 * @deprecated use {@link Text#setMessage(String)} instead, since it is
 *             supported on all platforms since Eclipse 3.5
 */
public class TextFieldDecorator implements FocusListener {

	private final Text control;
	private final String message;
	private String defaultString = "";
	private boolean displaying = false;
	private Color defaultForeground;
	private Color messageForeground;

	
	/** 
	 * Creates a new TextFieldDecorator and assigns it with the given text field.
	 *  
	 * @param control
	 * 		the text field to decorate.
	 * @param message
	 *      the help message to display when the empty field is unfocused.
	 */
	public TextFieldDecorator(final Text control, final String message) {
		this.control = control;
		this.message = message;

		messageForeground = control.getDisplay().getSystemColor(SWT.COLOR_GRAY);
		control.addFocusListener(this);
		
		// Check for focus first?
		addDecoration();
	}

	public void focusGained(FocusEvent e) {
		if (displaying) {
			removeDecoration();
		}
	}

	public void focusLost(FocusEvent e) {
		if (defaultString.equals(control.getText()))
			addDecoration();
	}
	
	/**
	 * Returns the “real” text content for the field, i.e. this will not contain the message.
	 * @return
	 */
	public String getText() {
		if (displaying)
			return defaultString;
		else
			return control.getText();
	}

	private void removeDecoration() {
		control.setText(defaultString);
		control.setForeground(defaultForeground);
		displaying = false;
	}

	private void addDecoration() {
		control.setText(message);
		control.setForeground(messageForeground);
		displaying = true;
	}

	public void dispose() {
		control.removeFocusListener(this);
	}

}
