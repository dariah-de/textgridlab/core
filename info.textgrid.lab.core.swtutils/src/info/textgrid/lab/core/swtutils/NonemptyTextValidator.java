/**
 * 
 */
package info.textgrid.lab.core.swtutils;

public class NonemptyTextValidator extends TextValidator {
	public boolean validate() {
		return (textWidget != null) && !textWidget.getText().equals(""); //$NON-NLS-1$
	}
}