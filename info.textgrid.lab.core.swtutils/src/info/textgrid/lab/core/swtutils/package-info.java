/**
 * Some utilities to ease the use of Eclipse and SWT classes. Does not depend on
 * any other TextGridLab specific classes, so everybody can use it.
 */
package info.textgrid.lab.core.swtutils;

