package info.textgrid.lab.core.swtutils;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.AbstractListViewer;
import org.eclipse.jface.viewers.AbstractTableViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.DeferredTreeContentManager;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;
import org.eclipse.ui.progress.PendingUpdateAdapter;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.progress.WorkbenchJob;

/**
 * A content provider for {@link AbstractListViewer}s or
 * {@link AbstractTableViewer}s that uses a background job to add content to the
 * viewer.
 * 
 * <p>
 * This content provider can be used for list viewers when fetching content for
 * the viewer is a long running operation, but provides partial results.
 * Elements are added to the list as they come. Until the fetching process is
 * finished, a {@linkplain PendingUpdateAdapter placeholder object} is appended
 * to the list to signify that we're still fetching.
 * </p>
 * 
 * <p>
 * To use, your viewer's {@linkplain StructuredViewer#setInput(Object) input}
 * must adapt to a {@link IDeferredWorkbenchAdapter} (or implement this
 * interface). This content provider will call your input's
 * {@link IDeferredWorkbenchAdapter#fetchDeferredChildren(Object, IElementCollector, IProgressMonitor)}
 * method in a background thread to do the actual fetching. Your
 * {@link LabelProvider} should label the {@link PendingUpdateAdapter} as
 * something like “Pending ...”.
 * </p>
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de>
 * @see DeferredTreeContentManager
 */
public class DeferredListContentProvider implements IStructuredContentProvider, ISelectionChangedListener {
	
		public interface IDoneListener {
		/**
		 * Called when the list has finished loading and the
		 * {@link PendingUpdateAdapter} has been removed.
		 * 
		 * This method is always called in the UI thread.
		 * 
		 * @param viewer
		 *            The viewer that had been filled.
		 */
		public void loadDone(Viewer viewer);
	}
	
    private Viewer viewer;
	private AbstractListViewer listViewer = null;
	private AbstractTableViewer tableViewer = null;
	private ListenerList doneListeners = new ListenerList();
	private ISelection oldSelection = null;
	private boolean restoreSelection = false;

	/**
	 * Due to the nature of the deferred content provider
	 * {@link StructuredViewer}'s standard way of preserving the selection
	 * during a {@linkplain StructuredViewer#refresh()} doesn't work. Thus, this
	 * class offers an alternative mechanism which can be activated by calling
	 * {@linkplain #setRestoreSelection(boolean)
	 * <code>setRestoreSelection(<b>true</b>)</code>}. If you do so, we install
	 * a selection listener on the viewer which will remember the last
	 * <em>non-empty</em> selection of the viewer, and which will restore that
	 * after loading has been finished.
	 */
	public boolean isRestoreSelection() {
		return restoreSelection;
	}

	public void setRestoreSelection(boolean restoreSelection) {
		this.restoreSelection = restoreSelection;
		if (restoreSelection && viewer != null)
			viewer.addSelectionChangedListener(this);
	}

	private Class<?> targetClass = null;
	
	public DeferredListContentProvider() {
		super();
	}

	/**
	 * Creates a new {@link DeferredListContentProvider}.
	 * 
	 * @param targetClass
	 *            if != <code>null</code>, all results are adapted to the target
	 *            class before they are added to the viewer. If an object cannot
	 *            be adapted to <var>targetClass</var> (i.e.
	 *            {@link AdapterUtils#getAdapter(Object, Class) returns null},
	 *            the object is not added.
	 * 
	 */
	public DeferredListContentProvider(Class<?> targetClass) {
		this();
		this.targetClass = targetClass;
	}
	
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (this.viewer != null)
			viewer.removeSelectionChangedListener(this);
		if (viewer instanceof AbstractListViewer) {
			this.listViewer = (AbstractListViewer) viewer;
			this.viewer = viewer;
		} else if (viewer instanceof AbstractTableViewer) {
			this.tableViewer = (AbstractTableViewer) viewer;
			this.viewer = viewer;
		} else {
			throw new IllegalArgumentException(MessageFormat.format(
					"A deferred list content provider must be associated with an "
							+ "AbstractListViewer or AbstractTableViewer, "
							+ "but {0} is an {1}.", viewer,
					viewer.getClass().getSimpleName()));
		}
		viewer.addSelectionChangedListener(this);
	}

	/**
	 * Returns an initial list of elements (containing the
	 * {@link PendingUpdateAdapter}) and initiates the background fetching of
	 * the actual elements.
	 * 
	 * @see IDeferredWorkbenchAdapter#fetchDeferredChildren(Object,
	 *      IElementCollector, IProgressMonitor)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	public Object[] getElements(Object inputElement) {
		PendingUpdateAdapter pendingUpdateAdapter = new PendingUpdateAdapter();
		startFetchingChildren(inputElement, pendingUpdateAdapter);
		return new Object[] { pendingUpdateAdapter };
	}

	/**
	 * Starts the actual fetch job.
	 * 
	 * @param inputElement
	 * @param placeholder
	 * @throws IllegalArgumentException
	 *             if the inputElement cannot be adapted to an
	 *             {@link IDeferredWorkbenchAdapter}.
	 */
	protected void startFetchingChildren(Object inputElement, PendingUpdateAdapter placeholder) {
		
		final IDeferredWorkbenchAdapter adapter = getAdapter(inputElement);
		if (adapter == null) {
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"Can only fetch children in background for an "
											+ "IDeferredWorkbenchAdapter, but there is none for {0}",
									inputElement));
		}
		final IElementCollector collector = createElementCollector(
				inputElement,
				placeholder);
		
		Job job = new Job("Fetching list of projects") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				adapter.fetchDeferredChildren(adapter, collector,
						monitor);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		};
		job.schedule();

	}

	/**
	 * Creates the {@link IElementCollector} which transfers the objects from
	 * the {@link IDeferredWorkbenchAdapter} to the {@link AbstractListViewer}.
	 * 
	 * @param inputElement
	 * @param placeholder
	 * @return
	 */
	protected IElementCollector createElementCollector(
			final Object inputElement, final PendingUpdateAdapter placeholder) {
		return new IElementCollector() {

			public void add(Object element, IProgressMonitor monitor) {
				add(new Object[] { element }, monitor);				
			}

			public void add(Object[] elements, IProgressMonitor monitor) {
				addElements(elements, monitor);
			}

			public void done() {
				runDoneJob(placeholder);
			}
			
		};
	}
	
	/**
	 * Starts an {@link UIJob} to insert the given elements into the viewer.
	 * Called by the {@link IElementCollector}. This may be called from a non-UI
	 * thread.
	 * 
	 * @param elements
	 *            The elements to add.
	 * @param monitor
	 */
	protected void addElements(final Object[] elements, IProgressMonitor monitor) {
		WorkbenchJob updateJob = new WorkbenchJob("Adding elements ...") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				if (viewer.getControl().isDisposed()
						|| monitor.isCanceled())
					return Status.CANCEL_STATUS;
				final Object[] realElements = targetClass == null ? elements
						: AdapterUtils
								.getAdapters(elements, targetClass, false);
				
				if (listViewer != null)
					listViewer.add(realElements);
				else
					tableViewer.add(realElements);
				return Status.OK_STATUS;
			}
		};
		updateJob.setSystem(true);
		updateJob.schedule();
	}

	/**
	 * Starts the {@link UIJob} that removes the placeholder from the
	 * listviewer. This method may be called from a non-UI thread.
	 * 
	 * @param placeholder
	 */
	protected void runDoneJob(final PendingUpdateAdapter placeholder) {
		if (!PlatformUI.isWorkbenchRunning())
			return;
		WorkbenchJob clearJob = new WorkbenchJob("Removing pending message ...") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				if (viewer.getControl().isDisposed())
					return Status.CANCEL_STATUS;
				if (listViewer != null)
					listViewer.remove(placeholder);
				else
					tableViewer.remove(placeholder);
				if (restoreSelection && oldSelection != null) {
					viewer.setSelection(oldSelection, true);
					System.out.println("Restored old selection: " + oldSelection);
				}

				notifyDoneListeners();
				return Status.OK_STATUS;
			}
		};
		clearJob.setSystem(true);
		clearJob.schedule();
	}

	/**
	 * Tries desperately to convert the element to an
	 * {@link IDeferredWorkbenchAdapter}.
	 * 
	 * @param element
	 *            The element to convert.
	 * @return The {@link IDeferredWorkbenchAdapter} for <var>element</var> or
	 *         null if none could be found.
	 */
	protected IDeferredWorkbenchAdapter getAdapter(Object element) {
		if (element instanceof IDeferredWorkbenchAdapter)
			return (IDeferredWorkbenchAdapter) element;
		if (element instanceof IAdaptable)
			return (IDeferredWorkbenchAdapter) ((IAdaptable) element)
					.getAdapter(IDeferredWorkbenchAdapter.class);
		return (IDeferredWorkbenchAdapter) Platform.getAdapterManager()
				.getAdapter(element, IDeferredWorkbenchAdapter.class);
	}

	public void dispose() {
		// TODO Auto-generated method stub

	}

	/**
	 * The given listener will be notified when the loading has finished and the
	 * {@link PendingUpdateAdapter} has been removed.
	 * 
	 * @param listener
	 */
	public void addDoneListener(IDoneListener listener) {
		doneListeners.add(listener);
	}

	public void removeDoneListener(IDoneListener listener) {
		doneListeners.remove(listener);
	}
	
	protected void notifyDoneListeners() {
		for (Object listener : doneListeners.getListeners()) {
			((IDoneListener) listener).loadDone(viewer);
		}
	}

	public Class<?> getTargetClass() {
		return targetClass;
	}

	public void selectionChanged(SelectionChangedEvent event) {
		ISelection selection = event.getSelection();
		if (selection != null && !selection.isEmpty()) {
			this.oldSelection = selection;
			System.out.println("Saved selection: " + selection);
		}
	}
	
	
	
}
