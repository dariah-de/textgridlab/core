/**
 * 
 */
package info.textgrid.lab.core.swtutils;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

/**
 * A static class holding various globally available platform utilities. This
 * class should never be instatiated.
 * 
 * @author tv
 */
public class PlatformUtils {
	
	protected PlatformUtils() {
	}

	/**
	 * Executes the given command without any context. If something goes wrong,
	 * an error is logged and the corresponding error status is returned.
	 * Otherwise, {@link Status#OK_STATUS} is returned.
	 * 
	 * @param commandID
	 *            The ID of the command to run.
	 */
	public static IStatus executeCommand(String commandID) {
		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getService(ICommandService.class);
		try {
			commandService.getCommand(commandID).executeWithChecks(new ExecutionEvent());
		} catch (ExecutionException e1) {
			return Activator.handleError(e1,
					"Could not execute command {0}: {1}",
					commandID,
					e1.getMessage());
		} catch (NotDefinedException e1) {
			return Activator.handleError(e1,
					"Command {0} is not defined. {1}",
					commandID,
					e1.getMessage());
		} catch (NotEnabledException e1) {
			return Activator.handleError(e1,
					"Command {0} is not enabled. {1}",
					commandID,
					e1.getMessage());
		} catch (NotHandledException e1) {
			return Activator.handleError(e1,
					"Command {0} was not handled: {1}",
					commandID,
					e1.getMessage());
		}
		return Status.OK_STATUS;
	};

}
