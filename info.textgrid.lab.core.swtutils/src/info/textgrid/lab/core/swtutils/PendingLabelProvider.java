package info.textgrid.lab.core.swtutils;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.progress.PendingUpdateAdapter;

/**
 * A simple {@link LabelProvider} that provides a label for the
 * {@link PendingUpdateAdapter} used with deferred content providers. It works
 * like a {@link LabelProvider} otherwise.
 * 
 * @author tv
 * 
 */
public class PendingLabelProvider extends LabelProvider {

	private String pendingText = "Pending ...";
	
	/**
	 * Returns the label used for the {@link PendingUpdateAdapter}.
	 */
	public String getPendingText() {
		return pendingText;
	}

	/**
	 * Sets the label used for the {@link PendingUpdateAdapter}.
	 * 
	 * @param pendingText
	 *            the new label.
	 */
	public void setPendingText(String pendingText) {
		this.pendingText = pendingText;
	}

	/**
	 * Constructs a new {@link PendingLabelProvider} with a default
	 * {@linkplain #getPendingText() pending text}.
	 */
	public PendingLabelProvider() {
		super();
	}

	/**
	 * Constructs a new {@link PendingLabelProvider} with the given
	 * {@linkplain #getPendingText() pending text}.
	 * 
	 * @param pendingText
	 *            The text to show for {@link PendingUpdateAdapter}s.
	 */
	public PendingLabelProvider(String pendingText) {
		super();
		this.pendingText = pendingText;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof PendingUpdateAdapter) {
			return pendingText;
		}
		return super.getText(element);
	}

}
