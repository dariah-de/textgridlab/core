package info.textgrid.lab.core.swtutils;

import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.IElementCollector;

/**
 * A forwarding {@link IElementCollector} that adds new objects in chunks.
 * 
 * 
 * 
 * @author tv
 * @see IDeferredWorkbenchAdapter
 *
 */
public class ChunkingElementCollector implements IElementCollector {
	
	private IElementCollector target;
	private int chunkSize;
	private ArrayList<Object> cache;

	/**
	 * Creates a new forwarding chunking element collector.
	 * 
	 * @param target The element collector that the elements are finally added to.
	 * @param chunkSize The maximum number of objects per chunk
	 */
	public ChunkingElementCollector(final IElementCollector target, final int chunkSize) {
		this.target = target;
		this.chunkSize = chunkSize;
		this.cache = new ArrayList<Object>(chunkSize);
	}

	/**
	 * Adds the element to the internal cache. If the <var>chunkSize</var>
	 * elements have been added, add the current chunk to the target element
	 * collector.
	 */
	public void add(Object element, IProgressMonitor monitor) {
		cache.add(element);
		flushCacheIfFull(monitor);

	}

	private void flushCacheIfFull(IProgressMonitor monitor) {
		if (cache.size() >= chunkSize) {
			target.add(cache.toArray(), monitor);
			cache.clear();
		}
	}

	public void add(Object[] elements, IProgressMonitor monitor) {
		flushCacheIfFull(monitor);
		target.add(elements, monitor);
	}

	public void done() {
		target.add(cache.toArray(), null);
		cache.clear();
		target.done();
	}

}
