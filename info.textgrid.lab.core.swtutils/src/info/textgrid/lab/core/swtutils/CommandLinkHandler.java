package info.textgrid.lab.core.swtutils;

import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * A {@link ISelectionListener} for {@link Link}s that executes the command
 * referenced in the link. Use the command ID as the href value in the link.
 * 
 * <h4>Example Code</h4>
 * 
 * <pre>
 * 	Link importLink = new Link(parent, SWT.NONE);
 * 	importLink.setText(&quot;• Use the &lt;a href=\&quot;info.textgrid.lab.core.importexport.import\&quot;&gt;Import&lt;/a&gt; tool.&quot;);
 * 	importLink.addSelectionListener(new CommandLinkHandler());
 * </pre>
 */
public class CommandLinkHandler extends SelectionAdapter {
	@Override
	public void widgetSelected(SelectionEvent e) {
		IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);
		try {
			handlerService.executeCommand(e.text, null);
		} catch (CommandException e1) {
			StatusManager.getManager().handle(
					new Status(IStatus.ERROR, "", NLS.bind("Could not execute command {0}: {1}", e.text,
							e1.getLocalizedMessage()), e1));
		}
	}
}