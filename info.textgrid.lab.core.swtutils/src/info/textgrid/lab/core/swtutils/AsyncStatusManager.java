package info.textgrid.lab.core.swtutils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

public abstract class AsyncStatusManager {
	public static void handle(final StatusAdapter statusAdapter, final int style) {
		new UIJob(statusAdapter.getStatus().getMessage()) {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				StatusManager.getManager().handle(statusAdapter, style);
				return Status.OK_STATUS;
			}
			
		}.schedule();
	}
	public static void handle(final IStatus status, final int style) {
		handle(new StatusAdapter(status), style);
	}
}

