package info.textgrid.lab.core.swtutils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

/**
 * A message dialog which also allows the user to register a set of
 * check buttons.  
 * @author Frank Queens
 *
 */

public class MessageDialogWithCheckbuttons extends MessageDialog {
	
	ArrayList<Button> checkButtons = new ArrayList<Button>();
	boolean[] buttonValues;
	String[] messages;

	public MessageDialogWithCheckbuttons(Shell parentShell, String dialogTitle,
			Image dialogTitleImage, String dialogMessage, int dialogImageType,
			String[] dialogButtonLabels, int defaultIndex, String[] messages) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage,
				dialogImageType, dialogButtonLabels, defaultIndex);
		
		buttonValues = new boolean[messages.length];
		this.messages = messages;
		
        setButtonLabels(dialogButtonLabels);
	}
	
	/**
     * @see Dialog#createDialogArea(Composite)
     */
    protected Control createDialogArea(Composite parent) {
        Composite dialogAreaComposite = (Composite) super
                .createDialogArea(parent);
        createCheckButtons(dialogAreaComposite);
        return dialogAreaComposite;
    }

    /**
     * Creates a the buttons and sets the corresponding text.
     * 
     * @param parent
     *            The composite in which the toggle button should be placed;
     *            must not be <code>null</code>.
     */
    protected void createCheckButtons(Composite parent) {
    	for (int i=0; i<messages.length; i++) {
    		Button button = new Button(parent, SWT.CHECK | SWT.LEFT);
     		button.setText(messages[i]);
			checkButtons.add(button);
			buttonValues[i] = false;
	        button.setFont(parent.getFont());
	        
	        button.addSelectionListener(new SelectionAdapter() {

	            public void widgetSelected(SelectionEvent e) {
	            	int i = checkButtons.indexOf(e.getSource());
	            	buttonValues[i] = ((Button)e.getSource()).getSelection();
	            }

	        });
		}
    }
    
    /**
     * Gives the selection state of a button.
     * @param index	
     * 			button index
     * @return
     * 			checkbutton is selected (true/false)
     */			 
    public boolean getCheckButtonState(int index) {
    	return buttonValues[index];
    }

}
