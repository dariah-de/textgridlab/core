package info.textgrid.lab.core.swtutils;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;

/**
 * Ensures that only one of a group of controls has a selection.
 * 
 * Install this class on some controls using the {@link #on(Control...)} factory
 * method. You may {@link #add(Control...)} or {@link #remove(Control...)}
 * controls from the list of managed items later.
 * 
 * If the user selects something in one of the controls managed by this class,
 * the selections from all other controls managed are removed.
 * 
 * @author vitt
 * 
 */
public class ExclusiveSelection implements DisposeListener, Listener {

	private Set<Control> controls = new HashSet<Control>();

	protected ExclusiveSelection(Control... controls) {
		super();
		add(controls);
	}

	public static ExclusiveSelection on(Control... controls) {
		return new ExclusiveSelection(controls);
	}

	public void add(Control... targets) {
		for (Control target : targets) {
			controls.add(target);
			target.addDisposeListener(this);
			target.addListener(SWT.Selection, this);
		}
	}

	public void remove(Control... targets) {
		for (Control target : targets) {
			controls.remove(target);
			target.removeListener(SWT.Selection, this);
			target.removeDisposeListener(this);
		}
	}

	public void widgetDisposed(DisposeEvent e) {
		if (e.widget instanceof Control)
			controls.remove(e.widget);
	}

	public void handleEvent(Event event) {
		if (event.type == SWT.Selection) {
			for (Control control : controls) {
				if (event.widget != control) {
					if (control instanceof Button)
						((Button) control).setSelection(false);
					else if (control instanceof List)
						((List) control).deselectAll();
					else if (control instanceof Table)
						((Table) control).deselectAll();
					else if (control instanceof Combo)
						((Combo) control).deselectAll();
				}
			}
		}
	}
}
