package info.textgrid.lab.core.swtutils;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Set;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;

import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;

public class IntervalMultimap<T> {

	/**
	 * A comparator for intervals that considers overlapping intervals equal.
	 */
	static class OverlapsComparator implements Comparator<Interval> {
		public int compare(Interval o1, Interval o2) {
			if (o1.overlaps(o2))
				return 0;
			else
				return o1.compareTo(o2);
		}
	}

	/**
	 * An interval is a range [<var>start</var>,<var>end</var>) of two integers
	 * with <var>start</var> &#8804; <var>end</var>.
	 */
	public static class Interval implements Comparable<Interval>, IRegion {
		private int start;
		private int end;

		/**
		 * Creates a new interval ranging from <var>start</var> to
		 * <var>end</var>.
		 * 
		 * @throws IllegalArgumentException
		 *             if <var>start</var> > <var>end</var>.
		 */
		public Interval(final int start, final int end) throws IllegalArgumentException {
			super();
			if (start > end)
				throw new IllegalArgumentException("An interval's start cannot be after its end.");
			this.start = start;
			this.end = end;
		}

		/**
		 * Creates a new zero-length interval at <var>point</var>.
		 * 
		 * @param point
		 */
		public Interval(int point) {
			this(point, point);
		}

		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + end;
			result = prime * result + start;
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Interval))
				return false;
			final Interval other = (Interval) obj;
			if (end != other.end)
				return false;
			if (start != other.start)
				return false;
			return true;
		}

		public String toString() {
			return MessageFormat.format("[{0},{1})", start, end);
		}

		public int compareTo(Interval o) {
			int result = start - o.start;
			if (result == 0)
				result = end - o.end;
			return result;
		}

		Interval successor() {
			return new Interval(start, end + 1);
		}

		/**
		 * Returns true if this interval and <var>other</var> overlap.
		 */
		public boolean overlaps(final Interval other) {
			return startsInside(other) || other.startsInside(this);
		}

		/** true if this interval's start is inside the other interval. */
		private boolean startsInside(final Interval other) {
			return other.start <= start && start < other.end;
		}

		/**
		 * returns an interval of length 0 that starts immediately after this
		 * interval.
		 */
		public Interval succInterval() {
			return new Interval(end + 1, end + 1);
		}

		public int getStart() {
			return start;
		}

		public int getEnd() {
			return end;
		}

		public int getLength() {
			return end - start;
		}

		public int getOffset() {
			return start;
		}

		public static Interval of(final IRegion region) {
			return new Interval(region.getOffset(), region.getOffset()
					+ region.getLength());
		}

		public static Interval of(final Position position) {
			return new Interval(position.offset, position.offset
					+ position.length);
		}

		public static Interval of(int position) {
			return new Interval(position);
		}

		public static Interval of(int startOffset, int endOffset) {
			return new Interval(startOffset, endOffset);
		}
	}

	// OverlapsComparator(),
																								// Ordering.natural());
	
	/** maps intervals to target objects */
	private final Multimap<Interval, T> intervalMap = HashMultimap.create();

	public void add(final Interval interval, final T object) {
		intervalMap.put(interval, object);
	}

	/**
	 * Returns all intervals that overlap with the given interval
	 * <var>query</var>.
	 */
	public Iterable<Interval> findIntervals(final Interval query) {
		return Iterables.filter(intervalMap.keys(), new Predicate<Interval>() {

			public boolean apply(Interval input) {
				return query.overlaps(input);
			}

		});
	}

	/**
	 * Returns the set of all values associated with an interval that overlaps
	 * with the given interval (<var>query</var>)
	 */
	public Set<T> getOverlappingValues(final Interval query) {
		final ImmutableSet.Builder<T> builder = new ImmutableSet.Builder<T>();
		for (Interval interval : intervalMap.keySet()) {
			if (query.overlaps(interval)) {
				builder.addAll(intervalMap.get(interval));
			}
		}
		return builder.build();
	}

	public String toString() {
		return Joiner.on(", ").withKeyValueSeparator(": ").join(
				intervalMap.asMap());
	}

	public void remove(final T value) {
		intervalMap.values().remove(value);
	}

}
