package info.textgrid.lab.core.errorhandler;

import info.textgrid.lab.core.model.AbstractResource;

import org.apache.axis2.AxisFault;

public interface IAxisFaultAnalyzer {

	public LabError analyze(final AxisFault fault, final AbstractResource resource);

}
