package info.textgrid.lab.core.errorhandler;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusAdapter;

import com.sun.istack.NotNull;

/**
 * A lab status is an implementation of {@link IStatus} and
 * {@link StatusAdapter} that implements TextGridLab specific behaviour.
 * 
 * This involves especially three kinds of special behaviour:
 * <ol>
 * <li>A plugin-driven analyzation of the fault that created the status</li>
 * <li>A knowledge of the resource (or resources) the status handles</li>
 * <li>A separate user and detail message for the stuff.</li>
 * </ol>
 * 
 * 
 * @author tv
 * 
 */
public class LabStatus extends StatusAdapter implements IStatus {

	// ////////////////////////// Creation ////////////////////////////

	protected LabStatus(int severity, String pluginId, int code, String message, Throwable exception) {
		super(new Status(severity, pluginId, code, message, exception));
	}

	public static class Builder {
		private Throwable cause;

		public Builder cause(final Throwable cause) {
			this.cause = cause;
			return this;
		}

		private int severity;

		public Builder severity(final int severity) {
			this.severity = severity;
			return this;
		}

		private int code;

		public Builder code(final int code) {
			this.code = code;
			return this;
		}

		private String message;

		public Builder message(@NotNull final String message, final Object... arguments) {
			this.message = MessageFormat.format(message, arguments);
			return this;
		}

		private String pluginId = ErrorHandlerPlugin.PLUGIN_ID;

		public LabStatus build() {
			return new LabStatus(severity, pluginId, code, message, cause);
		}
	}

	// //////////////////// IStatus implementation ////////////////////////////

	public IStatus[] getChildren() {
		return getStatus().getChildren();
	}

	public int getCode() {
		return getStatus().getCode();
	}

	public Throwable getException() {
		return getStatus().getException();
	}

	public String getMessage() {
		return getStatus().getMessage();
	}

	public String getPlugin() {
		return getStatus().getPlugin();
	}

	public int getSeverity() {
		return getStatus().getSeverity();
	}

	public boolean isMultiStatus() {
		return getStatus().isMultiStatus();
	}

	public boolean isOK() {
		return getStatus().isOK();
	}

	public boolean matches(int severityMask) {
		return getStatus().matches(severityMask);
	}

}
