package info.textgrid.lab.core.errorhandler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.statushandlers.IStatusAdapterConstants;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

public class DemoErrorHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		StatusAdapter labStatus = new LabStatus.Builder().message("Die Message.").cause(new Exception("Beispiel-Exception")).build();
		labStatus.setProperty(IStatusAdapterConstants.TITLE_PROPERTY, "Fehlertitel");
		labStatus.setProperty(IStatusAdapterConstants.EXPLANATION_PROPERTY, "Explanation");
		labStatus.setProperty(IStatusAdapterConstants.HINT_PROPERTY, "Hint");
		StatusManager.getManager().handle(labStatus, StatusManager.SHOW);
		return null;
	}

}
