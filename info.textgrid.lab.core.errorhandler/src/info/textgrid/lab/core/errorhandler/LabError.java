package info.textgrid.lab.core.errorhandler;

import info.textgrid.lab.core.model.AbstractResource;

public class LabError {

	private static long counter = 0;

	private final long serial;

	private final Throwable cause;

	private Throwable rootCause;
	private AbstractResource resource;

	private String userMessage;
	private String userDetail;

	private String logMessage;
	// logDetail?

	public LabError(Throwable cause, AbstractResource resource, String userMessage, String userDetail, String logMessage) {
		super();
		synchronized (getClass()) {
			serial = ++counter;
		}

		this.cause = cause;
		this.resource = resource;
		this.userMessage = userMessage;
		this.userDetail = userDetail;
		this.logMessage = logMessage;
	}

	public Throwable getRootCause() {
		if (rootCause != null)
			return rootCause;
		if (cause == null)
			return null;

		rootCause = cause;
		while (rootCause.getCause() != null)
			rootCause = rootCause.getCause();
		return rootCause;

	}

}
