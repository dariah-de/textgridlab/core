/**
 * This package intends to provide a set of helpers for error handling that
 * hopefully evolve to a general framework for error handling.
 * 
 * <h3>Error Handling Components</h3>
 * <p>
 * There are basically three components involved:
 * </p>
 * <ol>
 * <li><a href="#analyze">analyzing errors and extracting information</a></li>
 * <li><a href="#group">tracking and grouping related errors</a></li>
 * <li><a href="#report">reporting errors to the user, and logging them</a></li>
 * </ol>
 * 
 * <h4 id="analyze">Analyzing errors</h4>
 * <p>
 * The analyze framework involves the task of analyzing incoming exceptions and
 * transforming them to a uniform status element, as well as extracting that
 * kind of information that can be communicated to the user.
 * </p>
 * <p>
 * {@link info.textgrid.lab.core.errorhandler.analyze}
 * </p>
 * 
 * <h4 id="group">Grouping and tracking</h4>
 * <p>
 * The grouping and tracking framework groups and tracks :) errors that have a
 * common root cause and offers status handlers to be notified of more specific
 * error messages.
 * </p>
 * <p>
 * {@link info.textgrid.lab.core.errorhandler.track}
 * </p>
 * 
 * <h4 id="report">Reporting</h4>
 * <p>
 * The reporting framework reports errors to the user.
 * </p>
 * 
 * 
 * 
 */
package info.textgrid.lab.core.errorhandler;
