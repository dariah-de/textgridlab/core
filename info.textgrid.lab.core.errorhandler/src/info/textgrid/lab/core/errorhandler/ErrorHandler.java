package info.textgrid.lab.core.errorhandler;

import info.textgrid.lab.core.model.AbstractResource;

import java.text.MessageFormat;

import javax.xml.namespace.QName;

import org.apache.axis2.AxisFault;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class ErrorHandler {
	private Multimap<Throwable, LabError> rootCauses = HashMultimap.create();

	/** Handles axis faults. */
	public void handleError(final AxisFault e, final AbstractResource resource, final String message,
			final Object... arguments) {

		final LabError error = new LabError(e, resource, MessageFormat.format(message, arguments), null, extractAxisFaultDetails(e));
		add(error);
	}

	private void add(final LabError error) {
		final Throwable rootCause = error.getRootCause();
		if (rootCause != null) {
			rootCauses.put(rootCause, error);
		}
	}

	/**
	 * Creates a string containing details of the given axis fault
	 */
	public static String extractAxisFaultDetails(final AxisFault e) {
		QName faultCode = e.getFaultCode();
		String faultDetailMessage = e.getFaultDetailElement().getText();
		String reason = e.getFaultReasonElement().getText();
		return MessageFormat.format("Fault code: {0}\nDetail Message: {1}\nReason: {2}", faultCode, faultDetailMessage, reason);
	}

}
