package info.textgrid.lab.core.aggregations.ui;

import static org.junit.Assert.*;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.treeWriter.AggregationWriter;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class TestAggregationWriter {

	@Test
	public void testWriteAggregation() throws XMLStreamException, CoreException, UnsupportedEncodingException {
		
		Aggregation aggregation = new Aggregation(null, "Example Aggregation", null, null);
		AggregationWriter writer = new AggregationWriter();
		ByteArrayOutputStream outputStream = writer.writeAggregation(aggregation, false, null);
		String output = outputStream.toString("UTF8");
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
		"<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ore=\"http://www.openarchives.org/ore/terms/\">" +
		"<rdf:Description />" +
		"</rdf:RDF>";
		assertEquals(expected, output.replace("'", "\""));
	}
	
	@Test
	public void testNestedAggregation() throws XMLStreamException, CoreException {
		Aggregation outer = new Aggregation(null, "Example Aggregation", null, null);
		Aggregation inner = new Aggregation(outer, "Nested Aggregation", null, null);
		ByteArrayOutputStream outputStream = new AggregationWriter().writeAggregation(outer, false, null);
		String actual = outputStream.toString();
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
		"<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ore=\"http://www.openarchives.org/ore/terms/\">" +
		"<rdf:Description>" +
		"<ore:aggregates>" + 
		inner.getObject().getURI().toString() +
		"</ore:aggregates>" +
		"</rdf:Description>" + 
		"</rdf:RDF>";
		assertEquals(expected, actual.replace("'", "\""));
		outer.save(null, false);
	}

}
