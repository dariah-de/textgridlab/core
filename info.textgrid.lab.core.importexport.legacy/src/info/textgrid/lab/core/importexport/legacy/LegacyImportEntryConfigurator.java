package info.textgrid.lab.core.importexport.legacy;

import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXB;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;

public class LegacyImportEntryConfigurator implements IImportEntryConfigurator {

	public void configureImport(ImportEntry entry, IProgressMonitor monitor) {
		final File oldMetadata = new File(entry.getLocalFile().getParent(), "." + entry.getLocalFile().getName() + ".meta"); //$NON-NLS-1$ //$NON-NLS-2$
		if (oldMetadata.exists() && !entry.getLocalMetadataFile().exists()) {
			try {
				ObjectType converted = convertOldMetadataFile(oldMetadata);
				entry.getObject().setMetadata(converted);
				// normalize content type
				entry.getObject().setContentType(TGContentType.of(converted.getGeneric().getProvided().getFormat()));
				entry.setSupplier(new LegacyRewritingSupplier(entry));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private ObjectType convertOldMetadataFile(File oldMetadata) throws IOException, TransformerException {
		StreamSource mdSource = new StreamSource(oldMetadata);
		try {
			final InputStream xslStream = FileLocator.openStream(LegacyImportPlugin.getDefault().getBundle(),
					new Path("resources").append("old2new-metadata.xsl"), false); //$NON-NLS-1$ //$NON-NLS-2$
			final StreamSource xslSource = new StreamSource(xslStream);
			final Transformer transformer = TransformerFactory.newInstance().newTransformer(xslSource);
			final ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
			final StreamResult result = new StreamResult(resultStream);
			// progress.worked(10);
			transformer.transform(mdSource, result);
			resultStream.close();
			ByteArrayInputStream convertedStream = new ByteArrayInputStream(resultStream.toByteArray());
			ObjectType metadata = JAXB.unmarshal(convertedStream, ObjectType.class);

			return metadata;

		} catch (final TransformerConfigurationException e) {
			throw new IllegalStateException(e);
		} catch (final TransformerFactoryConfigurationError e) {
			throw new IllegalStateException(e);
		}

	}

}
