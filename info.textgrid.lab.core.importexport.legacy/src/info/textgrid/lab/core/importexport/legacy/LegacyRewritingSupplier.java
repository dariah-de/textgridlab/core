package info.textgrid.lab.core.importexport.legacy;

import info.textgrid._import.ImportObject;
import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.ImportModel;
import info.textgrid.lab.core.importexport.model.Messages;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.namespaces.metadata.core._2010.IdentifierType;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * Preprocesses files exported from the old TextGridLab beta, replacing URIs
 * with file names.
 */
public class LegacyRewritingSupplier implements ISpecialImportEntrySupplier {

	private ImportEntry entry;

	public LegacyRewritingSupplier(ImportEntry entry) {
		this.entry = entry;
	}

	/**
	 * Returns a mapping for the current object that maps legacy URIs to file
	 * names for the given import model.
	 */
	protected ImportMapping getMapping(final ImportModel model) {
		final ImportMapping mapping = new ImportMapping();
		for (ImportObject entry0 : model.getImportObject()) {
			ImportEntry entry = (ImportEntry) entry0;
			try {
				final List<IdentifierType> identifiers = entry.getObject().getMetadataForReading().getGeneric().getProvided().getIdentifier();
				String oldURI = Iterables.find(identifiers, new Predicate<IdentifierType>() {

					public boolean apply(IdentifierType identifier) {
						return "textgrid-beta".equals(identifier.getType()); //$NON-NLS-1$
					}

				}).getValue();
				mapping.add(oldURI, entry.getLocalData(), entry.getLocalMetadata(), RewriteMethod.NONE);
			} catch (CrudServiceException e) {
				StatusManager.getManager().handle(e, LegacyImportPlugin.PLUGIN_ID);
			} catch (NoSuchElementException e) {
				// If it doesn't have a textgrid-beta identifier, it is not an
				// old object
			}
		}
		return mapping;
	}

	public InputStream getInputStream() throws IOException {
		if (entry.getRewriteMethod().equals(RewriteMethod.NONE))
			return new FileInputStream(entry.getLocalFile());

		final ImportMapping mapping = getMapping(entry.getModel());
		System.out.println(mapping);
		final PipedOutputStream outputStream = new PipedOutputStream();
		final Thread rewriteThread = new Thread(Messages.ImportModel_Rewriting + entry.getLocalData()) {
			@Override
			public void run() {
				InputStream localFileStream;
				try {
					localFileStream = new FileInputStream(entry.getLocalFile());
					ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(mapping, true);
					rewriter.recordMissingReferences();
					rewriter.configure(new URI(entry.getRewriteConfig()));
					rewriter.rewrite(localFileStream, outputStream);
					System.err.println("Missing references -- legacy import: " + rewriter.getMissingReferences()); //$NON-NLS-1$
				} catch (Exception e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, LegacyImportPlugin.PLUGIN_ID, NLS.bind(
									Messages.LegacyRewritingSupplier_FailedToRewriteLegacyFile, entry.getLocalData(), e.getLocalizedMessage()), e));
				} finally {
					try {
						outputStream.close();
					} catch (IOException e) {
						StatusManager.getManager().handle(
								new Status(IStatus.ERROR, LegacyImportPlugin.PLUGIN_ID, NLS.bind(
										Messages.LegacyRewritingSupplier_FailedToRewriteLegacyFile, entry.getLocalData(), e.getLocalizedMessage()),
										e));
					}
				}
			}
		};
		InputStream inputStream = new PipedInputStream(outputStream);
		rewriteThread.start();

		return inputStream;
	}

	public File getFile() {
		return entry.getLocalFile();
	}

}
