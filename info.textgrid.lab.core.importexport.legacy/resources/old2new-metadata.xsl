<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:old="http://textgrid.info/namespaces/metadata/core/2008-07-24"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://textgrid.info/namespaces/metadata/core/2010 http://www.textgrid.info/schemas/textgrid-metadata_2010.xsd
    http://textgrid.info/namespaces/metadata/core/2008-07-24 http://www.textgrid.info/schemas/textgrid-metadata_2008-07-24.xsd
    "
    version="1.0">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="*[local-name() = 'tgObjectMetadata']">
        <object>
            <generic>
                <provided>
                    <xsl:for-each select="old:descriptive/old:title">
                        <title>
                            <xsl:value-of select="."/>
                        </title>
                    </xsl:for-each>
                    <xsl:for-each select="old:descriptive/old:identifier">
                        <identifier>
                            <xsl:if test="@type">
                                <xsl:attribute name="type">
                                    <xsl:value-of select="@type"/>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="."/>
                        </identifier>
                    </xsl:for-each>
                    <xsl:if test="old:administrative/old:middleware/old:uri">
                        <identifier type="textgrid-beta">
                            <xsl:value-of select="old:administrative/old:middleware/old:uri"/>
                        </identifier>
                    </xsl:if>
                    <format>
                        <xsl:value-of select="old:administrative/old:client/old:format"/>
                    </format>
                    <notes>
                        <xsl:value-of select="old:descriptive/old:note"/>
                    </notes>
                </provided>
            </generic>
            <item>
                <xsl:for-each select="old:descriptive/old:agent[@role='rightsHolder']">
                    <rightsHolder>
                        <xsl:if test="@id">
                            <xsl:attribute name="id">
                                <xsl:value-of select="@id"/>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="."/>
                    </rightsHolder>
                </xsl:for-each>
            </item>
            <custom>
                <xsl:for-each select="old:custom/*">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
                <!-- for reference, a complete copy of the old metadata record -->
                <old:tgObjectMetadata>
                    <xsl:copy-of select="*"/>
                </old:tgObjectMetadata>
            </custom>
        </object>
    </xsl:template>

</xsl:stylesheet>
