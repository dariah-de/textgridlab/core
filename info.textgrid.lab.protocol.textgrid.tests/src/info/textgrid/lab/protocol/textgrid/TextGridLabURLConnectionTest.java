/**
 * 
 */
package info.textgrid.lab.protocol.textgrid;

import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runners.JUnit4;

public class TextGridLabURLConnectionTest {

	@Test(expected = FileNotFoundException.class)
	public void testTextGridLabURLConnection() throws IOException {
		final URL doesnotexist = new URL("textgrid:doesnotexist"); //$NON-NLS-1$
		final URLConnection noConnection = doesnotexist.openConnection();
		noConnection.connect();
		fail("textgrid:doesnotexist should not exist");//$NON-NLS-1$
	}

	@Test
	public void testGetInputStream() throws IOException {
		final URL approvedServices = new URL("textgrid:hk21"); //$NON-NLS-1$
		InputStream stream = approvedServices.openStream();
		Assert.assertTrue("Stream is empty?", stream.read() >= 0); //$NON-NLS-1$
		stream.close();
	}

}
