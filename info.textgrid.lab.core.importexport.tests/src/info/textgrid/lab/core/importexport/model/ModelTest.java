package info.textgrid.lab.core.importexport.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import info.textgrid._import.ImportSpec;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.namespaces.middleware.tgauth.TgextraStub.ProjectInfo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.Test;



public class ModelTest {
	
	/**
	 * Tests loading a model.
	 * @throws JAXBException
	 */
	@Test
	public void testLoad() throws JAXBException {
		StreamSource source = new StreamSource(getClass().getClassLoader().getResourceAsStream("examplespec.xml"));
		ImportSpec model = ImportModel.load(source);
		assertNotNull(model);
		assertTrue(model instanceof ImportModel);
		assertTrue(model.getImportObject().size() > 0);
		assertTrue(model.getImportObject().get(0) instanceof ImportEntry);
	}

	/**
	 * Loads a spec and then saves it again.
	 */
	@Test
	public void testLoadSave() throws JAXBException, UnsupportedEncodingException {
		StreamSource source = new StreamSource(getClass().getClassLoader().getResourceAsStream("examplespec.xml"));
		ImportModel model =  ImportModel.load(source);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		model.save(new StreamResult(baos));
		assertTrue(baos.size() > 0);
		System.out.println(baos.toString("UTF-8"));
	}

	/**
	 * Creates a model with one entry through the model, saves it, and loads it
	 * for verification.
	 */
	@Test
	public void testSaveNewModel() throws IOException, CrudServiceException, ProjectDoesNotExistException, JAXBException {
		ImportModel model = new ImportModel();
		ProjectInfo pi = new ProjectInfo();
		pi.setId("dummy");
		model.setTargetProject(TextGridProject.getProjectInstance(pi));
		File file1 = File.createTempFile("Test", ".xml");
		file1.deleteOnExit();
		model.addFile(file1, null);
		File file2 = File.createTempFile("Test", ".xsd");
		file2.deleteOnExit();
		model.addFile(file2, null);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		model.save(new StreamResult(baos));
		assertTrue(baos.size() > 0);

		System.out.println("ModelTest.testSaveNewModel()");
		ImportModel model2 = ImportModel.load(new StreamSource(new ByteArrayInputStream(baos.toByteArray())));
		System.out.println(baos.toString("UTF-8"));
		assertEquals(2, model2.getImportObject().size());
		assertNotNull("Entry's URL is null", model2.getImportObject().get(0).getTextgridUri());
		assertNotNull("Entry's file is null", model2.getImportObject().get(0).getLocalData());
	}

}
