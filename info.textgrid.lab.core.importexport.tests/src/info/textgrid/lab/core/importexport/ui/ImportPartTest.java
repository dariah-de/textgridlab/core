package info.textgrid.lab.core.importexport.ui;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import info.textgrid._import.ImportObject;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.ImportModel;
import info.textgrid.lab.core.importexport.ui.ImportPart.AddFilesJob;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.TextGridProject;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import junit.framework.Assert;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.internal.part.NullEditorInput;
import org.junit.Test;

public class ImportPartTest {

	@SuppressWarnings("restriction")
	@Test
	public void testSaveUI() throws IOException, CoreException, InterruptedException, ProjectDoesNotExistException, JAXBException {
		File imexFile = File.createTempFile("UnitTest", ".imex");
		File importFile1 = File.createTempFile("UnitTest", ".xml");
		importFile1.deleteOnExit();
		File importFile2 = File.createTempFile("UnitTest", ".jpg");
		importFile2.deleteOnExit();
		// imexFile.deleteOnExit();
		FileStoreEditorInput editorInput = new FileStoreEditorInput(EFS.getStore(imexFile.toURI()));
	
		ImportPart editor = (ImportPart) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
				new NullEditorInput(),
				"info.textgrid.lab.core.importexport.ImExEditor");
		editor.setInput(editorInput);
		ImportModel model = editor.getImportModel();
		model.setTargetProject(TextGridProject.getProjectInstance("TGPR5"));
		// we don't really need to be logged since we just need a valid project,
		// we don't need write access until we actually import stuff (which we
		// never do)
		

		AddFilesJob addFilesJob = editor.new AddFilesJob(importFile1.getAbsolutePath(), importFile2.getAbsolutePath());
		addFilesJob.schedule();
		addFilesJob.join();

		Assert.assertEquals(2, model.getImportObject().size());
		Assert.assertTrue("Import object is an import object", model.getImportObject().get(0) instanceof ImportObject);
		Assert.assertTrue("Import object is an import entry", model.getImportObject().get(0) instanceof ImportEntry);

		System.out.println(model.getImportObject().get(0));

		editor.doSave(new NullProgressMonitor());

		ImportModel model2 = ImportModel.load(new StreamSource(imexFile));

		assertEquals(model.getImportObject().size(), model2.getImportObject().size());
		System.out.println("ImportPartTest.testSaveUI()");
		System.out.println(model2.getImportObject().get(0));

		assertEquals(2, model2.getImportObject().size());
		assertNotNull("Entry's URL is null", model2.getImportObject().get(0).getTextgridUri());
		assertNotNull("Entry's file is null", model2.getImportObject().get(0).getLocalData());
	}

}
