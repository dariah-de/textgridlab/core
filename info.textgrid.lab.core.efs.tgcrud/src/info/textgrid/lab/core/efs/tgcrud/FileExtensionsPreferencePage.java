package info.textgrid.lab.core.efs.tgcrud;

import info.textgrid.lab.core.swtutils.MultipleInputDialog;
import info.textgrid.lab.core.swtutils.SWTFactory;
import info.textgrid.lab.core.swtutils.TextValidator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.ColumnLayoutData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import com.ibm.icu.text.MessageFormat;

/**
 * Preference page for creating and configuring simple launch variables.
 * 
 * @see org.eclipse.debug.core.variables.IValueVariable
 * @see org.eclipse.debug.core.variables.ISimpleVariableRegistry
 */
public class FileExtensionsPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage {

	class NonemptyREValidator extends TextValidator {
		public boolean validate() {
			Pattern pattern = null;
			try {
				if(textWidget != null && !textWidget.getText().equals("")) { //$NON-NLS-1$
					pattern = Pattern.compile(textWidget.getText());
				}
			} catch(Exception e) {
				// swallow any exceptions - pattern will be null anyway. 
			}
			return pattern != null;
		}
	}
	
	private TableViewer extensionTable;
	protected Button envAddButton;
	protected Button envEditButton;
	protected Button envRemoveButton;

	protected SimpleExtensionContentProvider extensionsContentProvider = new SimpleExtensionContentProvider();

	protected static final String CONTENT_TYPE_LABEL = "TextGrid Content Type (Regular Expression)";
	protected static final String FILE_EXTENSION_LABEL = "File Extension";

	protected static final String FILE_EXTENSION_PREFERENCE_KEY = "info.textgrid.lab.core.efs.tgcrud.FileExtensionPreferencePage"; //$NON-NLS-1$
	protected static final String FILE_EXTENSION_PREFERENCE_PAGE = "info.textgrid.lab.core.efs.tgcrud.FileExtensionPreferencePage_context"; //$NON-NLS-1$

	protected static String[] variableTableColumnProperties = { 
		"contentType", //$NON-NLS-1$
		"extension" //$NON-NLS-1$
		};
	protected String[] variableTableColumnHeaders = { "TextGrid Content Type",
			"File Extension" };
	protected ColumnLayoutData[] variableTableColumnLayouts = {
			new ColumnWeightData(70), new ColumnWeightData(30) };

	public FileExtensionsPreferencePage() {
		setDescription("Configures the mapping between TextGrid Content Types (as regular expression matching the objects' content type metadata field) and the internal file extensions.");
	}

	/**
	 * @see PreferencePage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		super.createControl(parent);
		PlatformUI.getWorkbench().getHelpSystem().setHelp(getControl(),
				FILE_EXTENSION_PREFERENCE_PAGE);
	}

	protected Control createContents(Composite parent) {
		noDefaultAndApplyButton();
		Font font = parent.getFont();
		// The main composite
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		layout.numColumns = 2;
		composite.setLayout(layout);
		composite.setFont(font);

		createTable(composite);
		createButtons(composite);

		return composite;
	}

	/**
	 * Creates and configures the table containing launch configuration
	 * variables and their associated value.
	 */
	private void createTable(Composite parent) {
		Font font = parent.getFont();
		// Create table composite
		Composite tableComposite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		layout.numColumns = 1;
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.heightHint = 150;
		gridData.widthHint = 400;
		tableComposite.setLayout(layout);
		tableComposite.setLayoutData(gridData);
		tableComposite.setFont(font);
		// Create table
		extensionTable = new TableViewer(tableComposite, SWT.BORDER
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION);
		Table table = extensionTable.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setFont(font);
		gridData = new GridData(GridData.FILL_BOTH);
		extensionTable.getControl().setLayoutData(gridData);
		extensionTable.setContentProvider(extensionsContentProvider);
		extensionTable.setColumnProperties(variableTableColumnProperties);
		extensionTable.addFilter(new VariableFilter());
		extensionTable.setComparator(new ViewerComparator() {
			public int compare(Viewer iViewer, Object e1, Object e2) {
				if (e1 == null) {
					return -1;
				} else if (e2 == null) {
					return 1;
				} else {
					return ((MappingWrapper) e1).getContentType()
							.compareToIgnoreCase(
									((MappingWrapper) e2).getContentType());
				}
			}
		});

		extensionTable
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						handleTableSelectionChanged(event);
					}
				});

		extensionTable.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				if (!extensionTable.getSelection().isEmpty()) {
					handleEditButtonPressed();
				}
			}
		});
		extensionTable.getTable().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent event) {
				if (event.character == SWT.DEL && event.stateMask == 0) {
					handleRemoveButtonPressed();
				}
			}
		});

		for (int i = 0; i < variableTableColumnHeaders.length; i++) {
			TableColumn tc = new TableColumn(table, SWT.NONE, i);
			tc.setResizable(variableTableColumnLayouts[i].resizable);
			tc.setText(variableTableColumnHeaders[i]);
		}

		// Try restoring column widths from preferences, if widths aren't
		// stored, init columns to default
		if (!restoreColumnWidths()) {
			restoreDefaultColumnWidths();
		}

		extensionTable.setInput(Activator.getDefault().getTypeExtensionMappings());
		extensionTable.setLabelProvider(new SimpleVariableLabelProvider());
	}

	/**
	 * Creates the new/edit/remove buttons for the variable table
	 * 
	 * @param parent
	 *            the composite in which the buttons should be created
	 */
	private void createButtons(Composite parent) {
		// Create button composite
		Composite buttonComposite = new Composite(parent, SWT.NONE);
		GridLayout glayout = new GridLayout();
		glayout.marginHeight = 0;
		glayout.marginWidth = 0;
		glayout.numColumns = 1;
		GridData gdata = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		buttonComposite.setLayout(glayout);
		buttonComposite.setLayoutData(gdata);
		buttonComposite.setFont(parent.getFont());

		// Create buttons
		envAddButton = SWTFactory
				.createPushButton(buttonComposite, "Add", null);
		envAddButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				handleAddButtonPressed();
			}
		});
		envEditButton = SWTFactory.createPushButton(buttonComposite, "Edit",
				null);
		envEditButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				handleEditButtonPressed();
			}
		});
		envEditButton.setEnabled(false);
		envRemoveButton = SWTFactory.createPushButton(buttonComposite,
				"Remove", null);
		envRemoveButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				handleRemoveButtonPressed();
			}
		});
		envRemoveButton.setEnabled(false);
	}

	private void handleAddButtonPressed() {
		boolean done = false;
		String content_type = null;
		String extension = null;
		while (!done) {

			MultipleInputDialog dialog = new MultipleInputDialog(getShell(),
					"New Content Type - File Type Mapping");
			dialog.addTextField(CONTENT_TYPE_LABEL, content_type, new NonemptyREValidator());
			dialog.addTextField(FILE_EXTENSION_LABEL, extension, false);

			if (dialog.open() != Window.OK) {
				done = true;
			} else {
				content_type = dialog.getStringValue(CONTENT_TYPE_LABEL).trim();
				extension = dialog.getStringValue(FILE_EXTENSION_LABEL);
				done = addExtension(content_type, extension);
			}
		}
	}

	/**
	 * Attempts to create and add a new content type / file extension mapping
	 * with the given properties.
	 * Returns whether the operation completed successfully (either the variable
	 * was added successfully, or the user cancelled the operation). Returns
	 * false if the name is null or the user chooses not to overwrite an
	 * existing variable.
	 * 
	 * @param content_type
	 *            name of the variable, cannot be <code>null</code> or empty.
	 * @param extension
	 *            value of the variable or <code>null</code>
	 * @return whether the operation completed successfully
	 */
	private boolean addExtension(String content_type, String extension) {
		if (content_type == null || content_type.length() == 0) {
			MessageDialog
					.openError(getShell(), "Invalid Content Type",
							"The value you provided is not a valid TextGrid content type.");
			return false;
		}
		List editedExtensions = extensionsContentProvider
				.getWorkingSetVariables();
		Iterator iter = editedExtensions.iterator();
		while (iter.hasNext()) {
			MappingWrapper currentExtension = (MappingWrapper) iter.next();
			if (!currentExtension.isRemoved()) {
				String currentContentType = currentExtension.getContentType();
				if (currentContentType.equals(content_type)) {
					MessageDialog dialog = new MessageDialog(
							getShell(),
							"Confirmation Request",
							null,
							MessageFormat
									.format(
											"Replace the file extension for {0}?",
											new String[] { content_type }),
							MessageDialog.QUESTION, new String[] {
									IDialogConstants.YES_LABEL,
									IDialogConstants.NO_LABEL,
									IDialogConstants.CANCEL_LABEL }, 0);
					int overWrite = dialog.open();
					if (overWrite == 0) {
						currentExtension.setExtension(extension);
						extensionTable.update(currentExtension, null);
						return true;
					} else if (overWrite == 1) {
						return false;
					} else {
						return true; // Cancel was pressed, return true so
										// operation is ended
					}
				}
			}
		}
		MappingWrapper newVariable = new MappingWrapper(content_type, extension);
		extensionsContentProvider.addMapping(newVariable);
		extensionTable.refresh();
		return true;
	}

	private void handleEditButtonPressed() {
		IStructuredSelection selection = (IStructuredSelection) extensionTable
				.getSelection();
		MappingWrapper mappingWrapper = (MappingWrapper) selection
				.getFirstElement();
		if (mappingWrapper == null) {
			return;
		}
		String extension = mappingWrapper.getExtension();
		String content_type = mappingWrapper.getContentType();
		MultipleInputDialog dialog = new MultipleInputDialog(
				getShell(),
				MessageFormat
						.format(
								"Edit {0} Mapping",
								new String[] { content_type }));
		dialog.addTextField(CONTENT_TYPE_LABEL, content_type, new NonemptyREValidator());
		dialog.addTextField(FILE_EXTENSION_LABEL, extension, false);

		if (dialog.open() == Window.OK) {
			extension = dialog.getStringValue(FILE_EXTENSION_LABEL);
			if (extension != null) {
				mappingWrapper.setExtension(extension);
			}
			extensionTable.update(mappingWrapper, null);
		}
	}

	/**
	 * Remove the selection variables.
	 */
	private void handleRemoveButtonPressed() {
		IStructuredSelection selection = (IStructuredSelection) extensionTable
				.getSelection();
		List mappingsToRemove = selection.toList();
		MappingWrapper[] variables = (MappingWrapper[]) mappingsToRemove
				.toArray(new MappingWrapper[0]);
		for (int i = 0; i < variables.length; i++) {
			variables[i].setRemoved(true);
		}
		extensionTable.refresh();
	}

	/**
	 * Responds to a selection changed event in the variable table
	 * 
	 * @param event
	 *            the selection change event
	 */
	protected void handleTableSelectionChanged(SelectionChangedEvent event) {
		IStructuredSelection selection = ((IStructuredSelection) event
				.getSelection());
		MappingWrapper extension = (MappingWrapper) selection
				.getFirstElement();
		if (extension == null) {
			envEditButton.setEnabled(false);
			envRemoveButton.setEnabled(false);
		} else {
			envEditButton.setEnabled(selection.size() == 1);
			envRemoveButton.setEnabled(selection.size() > 0);
		}
	}

	public void init(IWorkbench workbench) {
	}

	/**
	 * Clear the variables.
	 */
	protected void performDefaults() {
		extensionsContentProvider.init();
		extensionTable.refresh();
		super.performDefaults();
	}

	/**
	 * Sets the saved state for reversion.
	 */
	public boolean performOk() {
		extensionsContentProvider.saveChanges();
		saveColumnWidths();
		return super.performOk();
	}


	public void saveColumnWidths() {
		StringBuffer widthPreference = new StringBuffer();
		for (int i = 0; i < extensionTable.getTable().getColumnCount(); i++) {
			widthPreference.append(extensionTable.getTable().getColumn(i)
					.getWidth());
			widthPreference.append(',');
		}
		if (widthPreference.length() > 0) {
			Activator.getDefault().getPreferenceStore().setValue(
					FILE_EXTENSION_PREFERENCE_KEY, widthPreference.toString());
		}
	}

	private boolean restoreColumnWidths() {
		String[] columnWidthStrings = Activator.getDefault()
				.getPreferenceStore().getString(FILE_EXTENSION_PREFERENCE_KEY)
				.split(","); //$NON-NLS-1$
		int columnCount = extensionTable.getTable().getColumnCount();
		if (columnWidthStrings.length != columnCount) {
			return false; // Preferred column sizes not stored correctly.
		}
		for (int i = 0; i < columnCount; i++) {
			try {
				int columnWidth = Integer.parseInt(columnWidthStrings[i]);
				extensionTable.getTable().getColumn(i).setWidth(columnWidth);
			} catch (NumberFormatException e) {
				Activator.getDefault().
				handleProblem(IStatus.WARNING, 
						"Problem loading persisted column sizes for StringVariablePreferencesPage",//$NON-NLS-1$
						e); 
			}
		}
		return true;
	}

	private void restoreDefaultColumnWidths() {
		TableLayout layout = new TableLayout();
		for (int i = 0; i < variableTableColumnLayouts.length; i++) {
			layout.addColumnData(variableTableColumnLayouts[i]);
		}
		extensionTable.getTable().setLayout(layout);
	}

	private class SimpleExtensionContentProvider implements
			IStructuredContentProvider {
		/**
		 * The content provider stores mapping wrappers for use during editing.
		 */
		private LinkedList<MappingWrapper> fWorkingSet = Activator.getDefault().getTypeExtensionMappings();

		public Object[] getElements(Object inputElement) {
			return fWorkingSet.toArray();
		}

		/**
		 * Adds the given mapping to the list of wrappers maintained by this content provider.
		 * 
		 * @param mapping
		 *            variable to add
		 */
		public void addMapping(MappingWrapper mapping) {
			fWorkingSet.add(mapping);
		}

		/**
		 * Required by IStructuredContentProvider, the implementation does nothing.
		 */
		public void dispose() {
		}

		/**
		 * Re-initializes the content provider unless newInput == null.
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			if (newInput == null) {
				return;
			}
			init();
		}

		/**
		 * Saves the edited variable state to the preferences store of the plug-in info.textgrid.lab.core.efs.tgcrud.
		 */
		public void saveChanges() {
			Activator.getDefault().setTypeExtensionMappings(fWorkingSet);
		}

		/**
		 * Re-initializes to the mappings currently stored in the preferences store.
		 */
		public void init() {
			fWorkingSet = Activator.getDefault().getTypeExtensionMappings();
		}

		/**
		 * Returns the 'working set' of variables
		 * 
		 * @return the working set of variables (not yet saved)
		 */
		public List getWorkingSetVariables() {
			return fWorkingSet;
		}

	}

	private class SimpleVariableLabelProvider extends LabelProvider implements
			ITableLabelProvider, IColorProvider {
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		public String getColumnText(Object element, int columnIndex) {
			if (element instanceof MappingWrapper) {
				MappingWrapper variable = (MappingWrapper) element;
				switch (columnIndex) {
				case 0:
					StringBuffer name = new StringBuffer();
					name.append(variable.getContentType());
					return name.toString();
				case 1:
					String value = variable.getExtension();
					if (value == null) {
						value = ""; //$NON-NLS-1$
					}
					return value;
				}
			}
			return null;
		}

		public Color getForeground(Object element) {
			return null;
		}

		public Color getBackground(Object element) {
			return null;
		}
	}

	class VariableFilter extends ViewerFilter {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers
		 * .Viewer, java.lang.Object, java.lang.Object)
		 */
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {
			return !((MappingWrapper) element).isRemoved();
		}

	}
}
