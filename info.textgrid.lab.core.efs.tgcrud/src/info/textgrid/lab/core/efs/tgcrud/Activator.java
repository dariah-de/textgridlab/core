package info.textgrid.lab.core.efs.tgcrud;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.core.efs.tgcrud";

	// Debug option name for service calls, see .options file
	public static final String DEBUG_SERVICE = PLUGIN_ID
			+ "/debug/servicecalls";
	
	// Debug option name for local file system
	public static final String DEBUG_LOCALFILE = PLUGIN_ID
			+ "/debug/localfile";
	
	// Debug option name for tracing all READ accesses to the log file
	public static final String DEBUG_READS = PLUGIN_ID + "/debug/reads";

	// Debug option name for tracing all LoggingInputStream operations to the
	// log file
	public static final String DEBUG_STREAMS = PLUGIN_ID + "/debug/streams";


	private static final String TYPE_EXTENSION_MAPPING_PREFS = "TypeExtensionMappingPreferences";

	// The shared instance
	private static Activator plugin;
	
	private LinkedHashMap<Pattern,String> typeExtensionMappingCache = null;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		/* TODO: set default content type / file extension mappings, 
		 *   cf. info.textgrid.lab.conf.ConfPlugin.java for an example */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	

	/**
	 * If the debugging option for services is set, write arguments to standard
	 * output.
	 * 
	 * @param msg
	 * @param args
	 */
	public static void logServiceCall(String msg, Object... args) {
		if (isDebugging(DEBUG_SERVICE)) {
			StatusManager.getManager().handle(new Status(IStatus.INFO,  PLUGIN_ID, 
					NLS.bind(msg, args)), StatusManager.LOG);
		}

	}

	/**
	 * Are we debugging with the given option?
	 * 
	 * @param option
	 * @return
	 */
	public static boolean isDebugging(String option) {
		return getDefault().isDebugging()
				&& "true".equalsIgnoreCase(Platform.getDebugOption(option)); //$NON-NLS-0$
	}

	public IStatus handleProblem(Throwable e) {
		return handleProblem(IStatus.ERROR, null, e);
	}

	public IStatus handleProblem(int severity, String message, Throwable cause) {
		if (message == null || message.equals("")) {
			if (cause != null)
				message = cause.getLocalizedMessage();
		}
		IStatus status = new Status(severity, PLUGIN_ID, message, cause);
		StatusManager.getManager().handle(
				status,
				status.getSeverity() == IStatus.ERROR ? StatusManager.SHOW
						| StatusManager.LOG : StatusManager.LOG);
		return status;
	}

	/**
	 * Cares for the given error. The <var>message</var> and the <var>args</var>
	 * are passed to {@link NLS#bind(String, Object[])} and the complete
	 * argument list to {@link #handleProblem(int, String, Throwable)}.
	 * 
	 * @param cause
	 *            The exception that caused the problem (or null)
	 * @param message
	 *            The message, use {0}, {1} ... for arguments
	 * @param args
	 *            Exactly as many arguments as referenced in the message
	 * @return the {@link IStatus} object created while handling the error
	 */
	public static IStatus handleError(Throwable cause, String message,
			Object... args) {
		return Activator.getDefault().handleProblem(IStatus.ERROR,
				NLS.bind(message, args), cause);
	}
	
	LinkedList<MappingWrapper> getTypeExtensionMappings() {
		String prefString = getPreferenceStore().getString(TYPE_EXTENSION_MAPPING_PREFS);
		String[] encodedMappings = prefString.split("\n", 0);
		LinkedList<MappingWrapper> wrapperList = new LinkedList<MappingWrapper>();
		for(String encodedMapping : encodedMappings) {
			if(encodedMapping != null && !"".equals(encodedMapping)) {
				String[] mapping = encodedMapping.split("\t", 2);
				String type = (mapping.length > 0 && mapping[0] != null) ? mapping[0] : "";
				String ext = (mapping.length > 1 && mapping[1] != null) ? mapping[1] : "";
				if(!"".equals(type) && !"".equals(ext)) {
					MappingWrapper wrapper = new MappingWrapper(type, ext);
					wrapperList.add(wrapper);
				}
			}
		}
		return wrapperList;
	}
	
	synchronized void setTypeExtensionMappings(LinkedList<MappingWrapper> wrapperList) {
		typeExtensionMappingCache = null;
		StringBuffer prefStringBuffer = new StringBuffer();
		for(MappingWrapper wrapper : wrapperList) {
			prefStringBuffer.append('\n');
			prefStringBuffer.append(wrapper.getContentType());
			prefStringBuffer.append('\t');
			prefStringBuffer.append(wrapper.getExtension());
		}
		getPreferenceStore().setValue(TYPE_EXTENSION_MAPPING_PREFS, prefStringBuffer.toString());
	}
	
	/**
	 * The public interface to the mapping between content types (as specified
	 * in the TextGrid Metadata contentType element) and the file extensions used
	 * internally to facilitate the selection of appropriate editors.
	 * @return A map from regular expression patterns describing content types
	 *         to file extensions.
	 */
	public synchronized LinkedHashMap<Pattern, String> getContentTypeExtensionMap() {
		if (typeExtensionMappingCache == null) {
			LinkedList<MappingWrapper> wrapperList = getTypeExtensionMappings();
			typeExtensionMappingCache = new LinkedHashMap<Pattern, String>();
			for(MappingWrapper wrapper : wrapperList) {
				try {
					Pattern contentTypePattern = Pattern.compile(wrapper.getContentType());
					String extension = wrapper.getExtension();
					typeExtensionMappingCache.put(contentTypePattern, extension);
				} catch(Throwable e) {
					handleError(e, "Content Type Pattern '{0}' is not a valid regular expression", 
							wrapper.getContentType());
				}
			}
		}
		return typeExtensionMappingCache;
	}
}
