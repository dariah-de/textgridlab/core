/**
 * 
 */
package info.textgrid.lab.core.efs.tgcrud;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TextGridFaultType;


/**
 * Signals a runtime error from TextGrid's Eclipse File System implementation.
 * 
 * @author Christoph Ludwig (Worms University of Applied Sciences) for TextGrid
 *
 */
public class TextGridEFSRuntimeError extends RuntimeException {
	private static final long serialVersionUID = 9124916896082774204L;
	private TextGridFaultType serviceFault;

	public TextGridEFSRuntimeError(final String msg) {
		super(msg);
	}

	public synchronized Throwable initCause(final Throwable cause) {
		final Throwable initCause = super.initCause(cause);
		serviceFault = CrudServiceException.getFaultType(cause, TextGridFaultType.class);
		if (serviceFault == null && cause.getCause() != null)
			serviceFault = CrudServiceException.getFaultType(cause.getCause(), TextGridFaultType.class);
		return initCause;
	}

	public String getMessage() {
		if (serviceFault != null)
			return super.getMessage() + "\n" + serviceFault.getFaultMessage() + "\n" + serviceFault.getCause();
		else
			return super.getMessage();
	}

}
