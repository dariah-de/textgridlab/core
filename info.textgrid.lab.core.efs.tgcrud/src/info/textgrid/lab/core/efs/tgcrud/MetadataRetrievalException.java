package info.textgrid.lab.core.efs.tgcrud;

@SuppressWarnings("serial")
public class MetadataRetrievalException extends RuntimeException {

	public MetadataRetrievalException() {
		super();
	}

	public MetadataRetrievalException(String message) {
		super(message);
	}

	public MetadataRetrievalException(Throwable cause) {
		super(cause);
	}

	public MetadataRetrievalException(String message, Throwable cause) {
		super(message, cause);
	}

}
