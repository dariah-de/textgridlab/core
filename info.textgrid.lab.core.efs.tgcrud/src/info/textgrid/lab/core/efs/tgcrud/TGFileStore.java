/**
 * 
 */
package info.textgrid.lab.core.efs.tgcrud;

import static info.textgrid.lab.core.efs.tgcrud.Activator.DEBUG_LOCALFILE;
import static info.textgrid.lab.core.efs.tgcrud.Activator.isDebugging;
import static info.textgrid.lab.core.efs.tgcrud.Activator.logServiceCall;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event;
import info.textgrid.lab.core.model.util.LongOperation;
import info.textgrid.lab.core.model.util.ModelUtil;
import info.textgrid.lab.core.swtutils.LogUtil;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.ui.core.dialogs.TGCrudWarning;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectFactory;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DeleteType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ReadType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.ws.Holder;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileInfo;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.provider.FileInfo;
import org.eclipse.core.filesystem.provider.FileStore;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

public class TGFileStore extends FileStore implements IFileStore {

	IFileInfo cachedFileInfo = null; // TODO Add real caching w/timeout
	URI efsUri;
	private final String cxfUri;
	private final URI textGridURI;

	public TGFileStore(final URI uri) throws CoreException {
		super();
		this.efsUri = uri;
		textGridURI = TextGridObject.computeTextGridURI(uri);
		this.cxfUri = textGridURI.toASCIIString().trim();
	}
	
	
	/*
	 * cf. #17291 https://projects.gwdg.de/issues/17291
	 * 
	 * Maybe we can still optimize this?
	 * 
	 * (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileStore#copyFile(org.eclipse.core.filesystem.IFileInfo, org.eclipse.core.filesystem.IFileStore, int, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected void copyFile(IFileInfo sourceInfo, IFileStore destination, int options, IProgressMonitor monitor) throws CoreException {
		SubMonitor progress = SubMonitor.convert(monitor);
		try {
			if ((options & EFS.OVERWRITE) == 0 && destination.fetchInfo().exists())
				throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID, MessageFormat.format("Destination {0} exists.", destination))); //$NON-NLS-1$
			InputStream in = null;
			OutputStream out = null;
			try {
				in = doOpenInputStream(EFS.NONE, progress.newChild(90), false);
				out = destination.openOutputStream(EFS.NONE, progress.newChild(10));
				ByteStreams.copy(in, out);
				int attrOptions = EFS.SET_ATTRIBUTES | EFS.SET_LAST_MODIFIED;
				destination.putInfo(sourceInfo, attrOptions, null);
				StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, MessageFormat.format("Copied {0} to {1} without replacing {0}''s metadata", sourceInfo, destination)), StatusManager.LOG);
			} catch (CoreException e) {
				in.close();
				if (out != null) out.close();
				//if we failed to write, try to cleanup the half written file
				if (!destination.fetchInfo(0, null).exists())
					destination.delete(EFS.NONE, null);
				throw e;
			}
		} catch (IOException e) {
			StatusManager.getManager().handle(new Status(IStatus.WARNING, Activator.PLUGIN_ID, MessageFormat.format("I/O error while copying {0} to {1}", sourceInfo, destination), e));
		} finally {
			progress.done();
		}
	}

	/**
	 * We don't have children.
	 * 
	 * @see #getParent()
	 * @see org.eclipse.core.filesystem.provider.FileStore#childNames(int,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public String[] childNames(final int options, final IProgressMonitor monitor)
			throws CoreException {
		return new String[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#fetchInfo(int,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public synchronized IFileInfo fetchInfo(final int options,
			final IProgressMonitor monitor) throws CoreException {
		if (cachedFileInfo == null)
			cachedFileInfo = readFileInfo(monitor);
		return cachedFileInfo;
	}

	/**
	 * Reads the file information to the file information cache.
	 * 
	 * @return
	 * 
	 * @throws CoreException
	 */
	protected FileInfo readFileInfo(IProgressMonitor monitor)
			throws CoreException {

		if (monitor == null)
			monitor = new NullProgressMonitor();

		monitor.beginTask(NLS.bind("Getting some metadata for {0}", efsUri), 6);
		URI textgridUri = TextGridObject.computeTextGridURI(efsUri);
		TextGridObject object = TextGridObject.getInstance(textgridUri, true);
		monitor.worked(2);

		FileInfo fileInfo = new FileInfo();
		fileInfo.setExists(true); // TODO implementation for not-yet-created
		// Objects
		fileInfo.setLastModified(object.getLastModified().getTimeInMillis());
		monitor.worked(1);
		fileInfo.setDirectory(false);
		fileInfo.setLength(object.getSize());
		monitor.worked(1);
		fileInfo.setName(efsUri.toString()); // FIXME: This will annoy our users.
		monitor.worked(1);
		if (object.isToOpenAsReadOnly())
			fileInfo.setAttribute(EFS.ATTRIBUTE_READ_ONLY, true);

		monitor.done();

		return fileInfo;
	}

	@Override
	public void putInfo(final IFileInfo info, final int options, final IProgressMonitor monitor)
			throws CoreException {
		// TODO Some implementation
		super.putInfo(info, options, monitor);
		invalidateFileInfoCache();
	}

	protected synchronized void invalidateFileInfoCache() {
		cachedFileInfo = null;
	}

	/**
	 * We don't have children.
	 * 
	 * @see #getParent()
	 * @see org.eclipse.core.filesystem.provider.FileStore#getChild(java.lang.String)
	 */
	@Override
	public IFileStore getChild(final String name) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#getName()
	 */
	@Override
	public String getName() {
		return efsUri.toString();
		// FIXME is this a good idea?
	}

	/**
	 * Poor TextGridObjects do not have parents. Instead, each one can create an
	 * experimental parent.
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#getParent()
	 * @category EXPERIMENTAL
	 */
	@Override
	public IFileStore getParent() {
		return new TGDummyParent(this); // maybe return a singleton?
	}
	
	@Override
	public InputStream openInputStream(final int options, final IProgressMonitor monitor) throws CoreException {

		if (Activator.isDebugging(Activator.DEBUG_READS))
			StatusManager.getManager().handle(
					new CoreException(new Status(IStatus.INFO, Activator.PLUGIN_ID, NLS.bind("Reading from {0}", cxfUri))),
					Activator.PLUGIN_ID);

		return new LongOperation<InputStream>() {

			@Override
			public InputStream run(final IProgressMonitor monitor) throws CoreException {
				return doOpenInputStream(options, monitor, true);
			}
			
		}.runInJob(monitor, NLS.bind("Reading from {0} ...", getTextGridObject()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#openInputStream(int,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	/**
	 * @deprecated Use {@link #doOpenInputStream(int,IProgressMonitor,boolean)} instead
	 */
	protected InputStream doOpenInputStream(final int options, IProgressMonitor monitor)
			throws CoreException {
				return doOpenInputStream(options, monitor, true);
			}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#openInputStream(int,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	protected InputStream doOpenInputStream(final int options, IProgressMonitor monitor, boolean setMetadata)
			throws CoreException {

		if (monitor == null)
			monitor = new NullProgressMonitor();
		monitor.beginTask(NLS.bind("Opening {0} for input", getTextGridObject()), IProgressMonitor.UNKNOWN);
		
		ModelUtil.checkNonUIThread("Tried to open an input stream for {0} from an UI thread", getTextGridObject());
		
		// checking for permissions is not sensible here: If we aren't allowed
		// to read, we aren't allowed to read the metadata, as well.

		ReadType readType = new ReadType();
		String logInfo = logsession.getInstance().getloginfo();
		readType.setLogParameter(logInfo);
		String sid = RBACSession.getInstance().getSID(false);
		readType.setSessionId(sid);
		readType.setUri(cxfUri);

		try {
			Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
			metadataHolder.value = new ObjectFactory().createMetadataContainerType();
			Holder<DataHandler> dataHandler = new Holder<DataHandler>();
			
			TGCrudService crudServiceStub = CrudClientUtilities
					.getCrudServiceStub();
			
			logServiceCall("Calling TGcrud#read({0}, {1}, {2}) (from TGFileStore#openInputStream)",  logInfo, sid, efsUri);
			try {
				crudServiceStub.read(sid, logInfo, cxfUri, metadataHolder, dataHandler);
			} catch (ProtocolNotImplementedFault e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logServiceCall("... succeeded: TGcrud#read(..., {0})", efsUri);

			TextGridObject textGridObject = TextGridObject.getInstance(
					metadataHolder.value.getObject(), true, setMetadata);
			textGridObject.refreshWorkspaceIfNeccessary();	// TG-301

			DataHandler data = dataHandler.value;
			if (data != null)
				return new LoggingInputStream(data.getInputStream(), new LogUtil(Activator.PLUGIN_ID));
			else
				return new LoggingInputStream(new ByteArrayInputStream(new byte[0]), new LogUtil(Activator.PLUGIN_ID));

		} catch (RemoteException e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (ObjectNotFoundFault e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (IoFault e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (AuthFault e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (IOException e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (MetadataParseFault e) {
			throw new CrudServiceException(e, textGridURI);
		} finally {
			monitor.done();
		}

	}

	@Override
	public OutputStream openOutputStream(final int options, final IProgressMonitor monitor)
			throws CoreException {
		
		if ((options & EFS.APPEND) != 0)
			throw new CoreException(new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, NLS.bind(
							"Appending is not implemented in TextGrid ({0})",
							efsUri)));
		
		final TextGridObject textGridObject = getTextGridObject();
		
		if (textGridObject.getPreparedNewVersion() != null)
			return openNewVersionOutputStream(options, monitor, textGridObject);
		
		if (textGridObject.getPreparedNewRevision() != null)
			return openNewRevisionOutputStream(options, monitor, textGridObject);
		
		
		textGridObject.assertPermission(ITextGridPermission.UPDATE,
				"Access denied: You are not allowed to update {0}.",
				textGridObject);
		
		try {
			final ExceptionPassingPipedOutputStream outputStream = new ExceptionPassingPipedOutputStream();
			final ExceptionPassingPipedInputStream inputStream = new ExceptionPassingPipedInputStream(
					outputStream);
			
			outputStream.setBlockingClose(true); // wait for our exceptions! // TG-1603
			outputStream.setCloseExceptionHandler(new CloseExceptionHandler(
					this, "Save (update) failed"));
			
			Thread serviceThread = new Thread() {

				@Override
				public void run() {
					DataSource dataSource = new InputStreamDataSource(
							inputStream, "application/octet-stream",
							TGFileStore.this.getName());
					DataHandler dataHandler = new DataHandler(dataSource);
					
					Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
					metadataHolder.value = new ObjectFactory().createMetadataContainerType();
					try {
						metadataHolder.value.setObject(textGridObject.getMetadata());
					} catch (CrudServiceException e1) {
						Activator.handleError(e1, "Strange metadata error saving {0}", textGridObject);
					}
					

					try {
						Activator.logServiceCall(
								"Calling TGcrud#update({0}, {1}, {2}, ...)",
									logsession.getInstance().getloginfo(),
									RBACSession.getInstance().getSID(false),
									textGridObject);
						TGCrudService crudServiceStub = CrudClientUtilities
								.getCrudServiceStub();
						crudServiceStub.update(RBACSession.getInstance().getSID(false),
												logsession.getInstance().getloginfo(),
												metadataHolder,
												dataHandler);

						updateMetadata(metadataHolder.value.getObject());
						final List<Warning> crudWarnings = metadataHolder.value.getObject().getGeneric().getGenerated().getWarning();
						final String tgObjTitle = textGridObject.getTitle();
						final String tgString = textGridObject.toString();
						
						UIJob uiJob = new UIJob("TGCrudWarning") {
		                      @Override
		                      public IStatus runInUIThread(final IProgressMonitor monitor) {
		                         //Show Warnings
		                         TGCrudWarning dlg = TGCrudWarning.getInstance(crudWarnings, tgObjTitle, tgString);
		                         if (dlg != null)
		                        	 dlg.OpenDialog();
		                         return Status.OK_STATUS;
		                      }
		                };
						uiJob.setSystem(true); // don't display this in the
												// progress area
		                uiJob.schedule();

						textGridObject.setMetadataDirty(false);
						TextGridObject.notifyListeners(Event.SAVED,
								textGridObject);
						TextGridObject.notifyListeners(Event.METADATA_SAVED,
								textGridObject);

					} catch (CrudServiceException e) {
						inputStream.passOnException(newIOException(e, null));
					} catch (UpdateConflictFault e) {
						String tgoTitle;
						try {
							tgoTitle = TGFileStore.this.getTextGridObject().getTitle();
						} catch (CrudServiceException e1) {
							tgoTitle = TGFileStore.this.getName();
						} catch (CoreException e1) {
							tgoTitle = TGFileStore.this.getName();
						}
						inputStream
								.passOnException(newIOException(
										e,
										"An update conflict occured: Someone else has saved {0} after you have last opened it.",
										tgoTitle));
					} catch (MetadataParseFault e) {
						inputStream.passOnException(newIOException(e, null));
					} catch (ObjectNotFoundFault e) {
						inputStream.passOnException(newIOException(e, null));
					} catch (IoFault e) {
						inputStream.passOnException(newIOException(e, null));
					} catch (AuthFault e) {
						inputStream.passOnException(newIOException(e, null));
					} catch (CoreException e) {
						inputStream.passOnException(newIOException(e, null));
					} catch (RuntimeException e) {
						inputStream.passOnException(e);
					} finally {
						inputStream.done();
					}
					
				}
			};
			serviceThread.setName(MessageFormat.format(
					"Updating {0} in the TextGridRep", getName()));
			serviceThread.start();
			
			
			return outputStream;
		} catch (IOException e) {
			throw new CoreException(Activator.handleError(e, MessageFormat
					.format("Could not open an output stream for {0}: {1}",
							textGridObject, e.getMessage()), e));
		}
	}

	private OutputStream openNewVersionOutputStream(final int options, final IProgressMonitor monitor, final TextGridObject textGridObject)
			throws CoreException {

		TextGridObject newVersion = textGridObject.getPreparedNewVersion();
		Assert.isNotNull(newVersion, NLS.bind("There is no prepared new version for {0}.", textGridObject));

		IFileStore newVersionStore = EFS.getStore(newVersion.getEFSURI());
		textGridObject.cancelNewVersion();
		return newVersionStore.openOutputStream(options, monitor);
	}
	
	private OutputStream openNewRevisionOutputStream(final int options, final IProgressMonitor monitor, final TextGridObject textGridObject)
	throws CoreException {

		TextGridObject newRevision = textGridObject.getPreparedNewRevision();
		Assert.isNotNull(newRevision, NLS.bind("There is no prepared new revision for {0}.", textGridObject));
		
		IFileStore newRevisionStore = EFS.getStore(newRevision.getEFSURI());
		textGridObject.cancelNewRevision();
		return newRevisionStore.openOutputStream(options, monitor);
	}

	/**
	 * A syntactic sugar method crafting new, encapsulating IOExceptions.
	 * 
	 * @param cause
	 *            the cause that caused the trouble.
	 * @param message
	 *            some message to describe the error in more detail. May be null
	 *            if you provide a cause.
	 * @param args
	 *            optional arguments for the message, see
	 *            {@link MessageFormat#format(String, Object...)}
	 * @return a new {@link IOException} encapsulating the given information.
	 */
	protected IOException newIOException(final Throwable cause, final String message, final Object... args) {
		IOException e;
		if (message != null)
			e = new IOException(MessageFormat.format(message, args), cause);
		else if (cause != null)
			e = new IOException(MessageFormat.format("An error ({0}) occurred during input/output handling for {1}: {2}",
					cause.getClass().getSimpleName(),
 getName(), cause.getLocalizedMessage()), cause);
		else
			e = new IOException(
					MessageFormat
							.format(
									"An unknown error occured during input/output handling for {0}. Please scold the programmer for not providing more details.",
									getName()));
		return e;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#toURI()
	 */
	@Override
	public URI toURI() {
		return efsUri;
	}

	@Override
	public void delete(final int options, IProgressMonitor monitor)
			throws CoreException {
		if (monitor == null)
			monitor = new NullProgressMonitor();
		monitor.beginTask(NLS.bind("Deleting {0}", efsUri), 6); // FIXME
		
		String logInfo = logsession.getInstance().getloginfo();
		String rbacSession = RBACSession.getInstance().getSID(true);
		
		ModelUtil.checkNonUIThread("Deleting {0} from an UI-Thread", getTextGridObject());
		
		if (getTextGridObject().isComplete()) // TG-576
			getTextGridObject().assertPermission(ITextGridPermission.DELETE, "Access denied: You are not allowed to delete {0}",
					getTextGridObject());

		DeleteType deleteType = new DeleteType();
		deleteType.setLogParameter(logInfo);
		deleteType.setSessionId(rbacSession);
		try {
			logServiceCall("Calling TGCrudServiceStub#delete({0}, {1}, {2}) ...", deleteType.getLogParameter(), deleteType.getSessionId(), cxfUri);
			TGCrudService crudServiceStub = CrudClientUtilities
					.getCrudServiceStub();
			crudServiceStub.delete(rbacSession, logInfo, cxfUri);
			monitor.worked(1);
			logServiceCall("... delete({0}) succeeded", efsUri);
			monitor.worked(5);

		} catch (ObjectNotFoundFault e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (RelationsExistFault e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (IoFault e) {
			throw new CrudServiceException(e, textGridURI);
		} catch (AuthFault e) {
			throw new CrudServiceException(e, textGridURI);
		}

		monitor.done();
	}

	@Override
	public boolean isParentOf(final IFileStore other) {
		return false;
	}

	/**
	 * Updates the metadata from the given XML fragment.
	 * 
	 * @param element
	 * @throws CrudServiceException
	 */
	public synchronized void updateMetadata(final ObjectType element) throws CrudServiceException {
		URI textgridUri = TextGridObject.computeTextGridURI(efsUri);
		TextGridObject.getInstance(textgridUri, false).setMetadata(element);
		invalidateFileInfoCache();
	}

	public TextGridObject getTextGridObject() throws CrudServiceException {
		URI textgridUri = TextGridObject.computeTextGridURI(efsUri);
		return TextGridObject.getInstance(textgridUri, false);
	}

	
	/**
	 * A new caching toLocalFile method for filestores from
	 * {@link TextGridObject}s.
	 * 
	 * <p>
	 * This has two advantages over the default implementation:
	 * </p>
	 * <ol>
	 * <li>It creates file extensions. Some code in WST relies on it,
	 * unfortunately ...</li>
	 * <li>If the lastModified date has not changed, the file is not downloaded.
	 * </li>
	 */
	@Override
	public File toLocalFile(final int options, final IProgressMonitor monitor)
			throws CoreException {
		
		if ((options & EFS.CACHE) == 0) {
			if (isDebugging(DEBUG_LOCALFILE))
				System.out.println(MessageFormat.format(
						"not caching {0} -- missing CACHE option",
						getTextGridObject()));
			return null;
		}
		
		
		SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(
				"Creating local copy for {0}", getTextGridObject()), 100);
		if (isDebugging(DEBUG_LOCALFILE))
			System.out.println(MessageFormat.format("Caching {0} ...",
					getTextGridObject()));
		
		// Create cache; compose local file name
		File cacheFolder = Activator.getDefault().getStateLocation().append(
				"cache").toFile();
		if (!cacheFolder.exists())
			cacheFolder.mkdirs();
		progress.worked(10);
		// care for windows paths and % translation
		String localFileName = getTextGridObject().getURI().toASCIIString()
				.replaceAll(
				"[%:/\\*?\"<>|]", "_");
		TGContentType contentType = getTextGridObject().getContentType(false);
		if (contentType != null)
			localFileName = localFileName + "." + contentType.getExtension();
		
		progress.worked(10);
		if (progress.isCanceled())
			return null;
		File localFile = new File(cacheFolder, localFileName);
		progress.worked(10);
		
		// can we spare the download?
		if (localFile.exists()) {
			if (isDebugging(DEBUG_LOCALFILE))
				System.out.print(MessageFormat.format(
						"  Local file {0} exists", localFile.getName()));
			
			Calendar remoteLastModified = getTextGridObject().getLastModified();
			Calendar localLastModified = Calendar.getInstance();
			localLastModified.setTimeInMillis(localFile.lastModified() + 2000);
			if (!localLastModified.before(remoteLastModified)) {
				if (isDebugging(DEBUG_LOCALFILE))
					System.out.println(" and is up to date. No download.");
				return localFile; // no download neccessary
			}
		}
		
		progress.worked(20);
		if (progress.isCanceled())
			return null;
		
		if (isDebugging(DEBUG_LOCALFILE))
			System.out.print(MessageFormat.format("  Downloading to {0} ... ",
					localFile.getName()));

		// no, we must download
		IFileStore localFileStore = EFS.getLocalFileSystem().fromLocalFile(
				localFile);
		try {
			copy(localFileStore, EFS.OVERWRITE, progress.newChild(45));
			localFile.setLastModified(getTextGridObject().getLastModified()
					.getTimeInMillis());
			if (isDebugging(DEBUG_LOCALFILE))
				System.out.println("sucess.");
		} finally {
			localFile.deleteOnExit();
			progress.worked(5);
		}
		
		return localFile;
	}
}
