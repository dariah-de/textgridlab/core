/**
 * 
 */
package info.textgrid.lab.core.efs.tgcrud;

import java.net.URI;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.provider.FileSystem;

/**
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 *
 */
public class TGNewFileFileSystem extends FileSystem {


	/**
	 * 
	 */
	public TGNewFileFileSystem() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileSystem#getStore(java.net.URI)
	 */
	@Override
	public IFileStore getStore(URI uri) {
		// TODO Auto-generated method stub
		return new TGNewFileFileStore(uri);
	}

}
