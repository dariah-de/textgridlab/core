/**
 * 
 */
package info.textgrid.lab.core.efs.tgcrud;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

import javax.activation.DataSource;

class InputStreamDataSource implements DataSource {
	private final InputStream inputStream;
	private String contentType;
	private String name;

	public InputStreamDataSource(InputStream inputStream,
			String contentType, String name) {
		this.inputStream = inputStream;
		this.contentType = contentType;
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	public String getName() {
		return name;
	}

	public OutputStream getOutputStream() throws IOException {
		throw new IOException(
				MessageFormat
						.format(
								"Somebody accessed the data source's output stream for {0}. This is probably not a good idea. (You might also call it a bug.)",
								getName()));
	}
}