package info.textgrid.lab.core.efs.tgcrud;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.text.MessageFormat;
import java.util.Date;

/**
 * This specialization of a {@link PipedInputStream} / {@link PipedOutputStream}
 * pair allows to pass on exceptions from the input to the output end.
 * 
 * @author tv
 * @see ExceptionPassingPipedInputStream
 */
public class ExceptionPassingPipedInputStream extends PipedInputStream {
	
	private ExceptionPassingPipedOutputStream src;

	/**
	 * Connects this stream with a matching
	 * {@link ExceptionPassingPipedOutputStream}. If you pass in something else,
	 * an {@link IllegalArgumentException} is thrown.
	 * 
	 * @throws IllegalArgumentException
	 *             when passing in something that is not an
	 *             {@link ExceptionPassingPipedOutputStream}
	 * @see java.io.PipedInputStream#connect(java.io.PipedOutputStream)
	 */
	@Override
	public void connect(PipedOutputStream source)
			throws IOException,
			IllegalArgumentException {
		if (source instanceof ExceptionPassingPipedOutputStream) {
			this.src = (ExceptionPassingPipedOutputStream) source;
			src.setSink(this);
		}
		else
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"Must be connected with a ExceptionPassingPipedInputStream, not with a plain {0} like {1}",
									src.getClass().getSimpleName(), src));
			
		super.connect(source);
	}

	public ExceptionPassingPipedInputStream() {
		super();
	}

	public ExceptionPassingPipedInputStream(
			ExceptionPassingPipedOutputStream src) throws IOException {
		super(src);
	}

	/**
	 * Pass the given exception to the connected output stream, and subsequently close this stream. 
	 * 
	 * @param e
	 *            must be either an {@link IOException} or a
	 *            {@link RuntimeException}
	 */
	public synchronized void passOnException(Throwable e) {
//		 System.err.println(MessageFormat.format("{0} passing on {1} ...", new Date(), e));
		if (src != null) {
			src.setException(e);
			try {
				this.close();
			} catch (IOException exceptionThatShouldNeverOccur) {
				throw new RuntimeException("Received an IOException from PipedInputStream.close(), but this method's implementation is known not to throw any exceptions. Strange.", exceptionThatShouldNeverOccur);
			}
			notifyAll();
		}
	}
	
	public synchronized void done() {
//		 System.out.print("Passing done permit ...");
		src.done();
//		 System.out.println(" done.");
	}

	synchronized void setSource(ExceptionPassingPipedOutputStream src) {
		this.src = src;
	}

}
