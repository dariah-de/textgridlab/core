package info.textgrid.lab.core.efs.tgcrud;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.text.MessageFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 * This specialization of a {@link PipedInputStream} / {@link PipedOutputStream}
 * pair allows to pass on exceptions from the input to the output end.
 * 
 * @author tv
 * @see ExceptionPassingPipedInputStream
 */
public class ExceptionPassingPipedOutputStream extends PipedOutputStream {

	/**
	 * An exception handler for exceptions that have been passed in from the
	 * fellow {@link ExceptionPassingPipedInputStream} but not handled when
	 * someone tries to close this stream. Use
	 * {@link ExceptionPassingPipedOutputStream#setCloseExceptionHandler(ICloseExceptionHandler)}
	 * to set this.
	 * 
	 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a>
	 *         (tv)
	 * 
	 */
	public interface ICloseExceptionHandler {
		/**
		 * Handles the given exception.
		 * 
		 * @param exception
		 *            The exception that has not yet been cleaned up. Will most
		 *            probably be either an {@link IOException} or a
		 *            {@link RuntimeException}.
		 * @return if the handler returns true,
		 *         {@link ExceptionPassingPipedOutputStream#close()} will
		 *         consider the exception completely handled. Otherwise, it will
		 *         throw it again.
		 * @throws IOException
		 *             The method may throw an IOException if needed. It does
		 *             not need to re-throw the passed in exception, it could
		 *             simply return false to indicate that further processing
		 *             is needed.
		 */
		public boolean handleException(Throwable exception) throws IOException;
	}

	private ICloseExceptionHandler closeExceptionHandler = null;

	/**
	 * Sets an exception handler for exceptions not yet thrown when the stream's
	 * {@link #close()} method is called.
	 * 
	 * Close will first wait for the partner to call {@link #done()} if it
	 * {@link #isBlockingClose()}, then check whether an unhandled exception is
	 * still remaining and a handler has been set and if so, call the handler's
	 * handleException method. If the latter returns <code>false</code>, the
	 * exception will be thrown (potentially wrapped in an
	 * {@link OtherSideException}).
	 * 
	 * 
	 * @param handler
	 *            the handler
	 */
	public void setCloseExceptionHandler(ICloseExceptionHandler handler) {
		this.closeExceptionHandler = handler;
	}

	/**
	 * If you pass in an exception that is neither an IOException nor a
	 * RuntimeException, it is encapsulated to this.
	 * 
	 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a>
	 *         (tv)
	 * 
	 */
	public static class OtherSideException extends IOException {
		
		private static final long serialVersionUID = 929216423625298296L;

		public OtherSideException(Throwable cause) {
			super(MessageFormat.format(
					"A {0} occurred on the reading side of the pipe: {1}",
					cause.getClass().getSimpleName(), cause.getMessage()));
			initCause(cause);
		}
	}

	private volatile boolean closed = false;
	private boolean blockingClose = false;
	private Semaphore mayQuitClose = new Semaphore(0);
	private Throwable exception = null; // synchronized access only!
	private PipedInputStream sink;
	

	public ExceptionPassingPipedOutputStream() {
		// TODO Auto-generated constructor stub
	}

	public ExceptionPassingPipedOutputStream(
			ExceptionPassingPipedInputStream snk)
			throws IOException {
		super(snk);
	}

	/**
	 * if true, do not return from {@link #close()} until someone has called the
	 * done() method on the connected {@link ExceptionPassingPipedOutputStream}
	 */
	public void setBlockingClose(boolean blockingClose) {
		this.blockingClose = blockingClose;
	}

	/**
	 * if true, do not return from {@link #close()} until someone has called the
	 * done() method on the connected {@link ExceptionPassingPipedOutputStream}.
	 * 
	 * The default is <code>false</code>, call
	 * {@link #setBlockingClose(boolean)} to force block behaviour.
	 */
	public boolean isBlockingClose() {
		return blockingClose;
	}

	synchronized void setException(Throwable e) {
		this.exception = e;
	}

	/**
	 * The last, yet unthrown exception passed in by
	 * {@link ExceptionPassingPipedInputStream#passOnException(Throwable)}
	 * 
	 * @return the other side's exception or <code>null</code>.
	 */
	public synchronized Throwable getException() {
		return exception;
	}

	/**
	 * Helper method that throws an exception if the other side has called
	 * {@link ExceptionPassingPipedInputStream#passOnException(Throwable)}.
	 * Every exception passed in this way is thrown only once (i.e. the field is
	 * cleared).
	 * 
	 * @throws IOException
	 *             if the other side has passed us a {@link IOException}
	 * @throws RuntimeException
	 *             if the other side has passed us a {@link RuntimeException}
	 * @throws OtherSideException
	 *             (which is an {@link IOException}) if the other side has
	 *             passed in something else, which you will find in the
	 *             exception's cause field
	 */
	protected synchronized void tryThrowException() throws IOException {
//		 System.out.println(MessageFormat.format("{0} Trying to throw {1}.", new Date(), exception));
		if (exception == null)
			return;
		if (exception instanceof IOException) {
			IOException e = (IOException) exception;
			exception = null;
			throw e;
		}
		if (exception instanceof RuntimeException) {
			RuntimeException e = (RuntimeException) exception;
			exception = null;
			throw e;
		}
		OtherSideException otherSideException = new OtherSideException(
				exception);
		exception = null;
		throw otherSideException;
	}

	@Override
	public synchronized void flush() throws IOException {
		tryThrowException();
		super.flush();
		tryThrowException();
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		tryThrowException();
		super.write(b, off, len);
		tryThrowException();
	}

	@Override
	public void write(int b) throws IOException {
		tryThrowException();
		super.write(b);
		tryThrowException();
	}

	@Override
	public void close() throws IOException {
//		 System.err.println("{0} close started ...");
		super.close();
		if (closed)
			return;
		if (blockingClose) {
			try {
//				 System.out.println(MessageFormat.format(
//				 "Trying to acquire done permit, available: {0}",
//				 mayQuitClose.availablePermits()));
				mayQuitClose.acquire();
//				 System.out.println("Acquired done permit.");
			} catch (InterruptedException e) {
				IOException ioe = new IOException(
						"Interrupted while waiting for close lock.");
				ioe.initCause(e);
				throw ioe;
			}
		}
		closed = true;
		if (closeExceptionHandler != null && exception != null)
			if (closeExceptionHandler.handleException(exception))
				exception = null;
		tryThrowException();
	}

	@Override
	public synchronized void connect(PipedInputStream snk) throws IOException {
		super.connect(snk);
		this.sink = snk;
		if (sink instanceof ExceptionPassingPipedInputStream)
			((ExceptionPassingPipedInputStream) sink).setSource(this);
	}

	/** Exclusively called by the partner on connect. */
	synchronized void setSink(ExceptionPassingPipedInputStream sink) {
		this.sink = sink;
	}

	public void done() {
		mayQuitClose.release();
	}
}
