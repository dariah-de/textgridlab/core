/**
 * Holds the implementation of the Eclipse file system that accesses TextGrid objects via TGcrud.
 * 
 * Clients usually do not interfere with these classes directly, but use the implementation
 * in the model and resource layers.
 * 
 *  @see info.textgrid.lab.core.model.TextGridObject
 *  @see org.eclipse.core.resources.IFile
 *  @see info.textgrid.lab.core.model
 *  @see org.eclipse.core.resources
 *  @see org.eclipse.core.filesystem.EFS
 */
package info.textgrid.lab.core.efs.tgcrud;