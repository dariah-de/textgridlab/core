package info.textgrid.lab.core.efs.tgcrud;

import java.io.InputStream;
import java.net.URI;

import org.eclipse.core.filesystem.IFileInfo;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.provider.FileInfo;
import org.eclipse.core.filesystem.provider.FileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * This is a dummy parent class for {@link TGFileStore}s. An instance of this
 * is returned by all TGFileStores when their {@link TGFileStore#getParent()}
 * method is called, since Eclipse does not expect a <b><code>null</code></b>
 * result.
 * 
 * This works around <a href="http://www.textgrid.de/trac/tickets/62>#62</a>.
 * Everything should be considered highly EXPERIMENTAL, there are probably
 * better solutions for this.
 * 
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * @category EXPERIMENTAL
 */
public class TGDummyParent extends FileStore {

	private IFileStore child = null;

	TGDummyParent(IFileStore child) {
		this.child = child;
	}

	@Override
	public String[] childNames(int options, IProgressMonitor monitor)
			throws CoreException {
		return child == null ? new String[0] : new String[] { child.getName() };
	}

	@Override
	public IFileInfo fetchInfo(int options, IProgressMonitor monitor)
			throws CoreException {
		FileInfo fileInfo = new FileInfo(getName());
		fileInfo.setExists(true);
		fileInfo.setDirectory(true);
		return fileInfo;
	}

	@Override
	public IFileStore getChild(String name) {
		name = name.replace("\0", "");
		if (child != null && name.equals(child.getName()))
			return child;
		return null; // TODO
	}

	@Override
	public String getName() {
		return child.getName().concat(".parent");
	}

	@Override
	public IFileStore getParent() {
		return null; // FIXME endless loop? what to return else?
	}

	@Override
	public InputStream openInputStream(int options, IProgressMonitor monitor)
			throws CoreException {
		throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				"Cannot open streams on dummy parent."));
	}

	/**
	 * Returns a dummy URI.
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#toURI()
	 * @category EXPERIMENTAL
	 */
	@Override
	public URI toURI() {
		return URI.create(child.toURI().toString().concat("/parent"));
	}

	/**
	 * mkdir() calls cause all that fuzz.
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#mkdir(int,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 * @category EXPERIMENTAL
	 */
	@Override
	public IFileStore mkdir(int options, IProgressMonitor monitor)
			throws CoreException {
		return this;
	}

}
