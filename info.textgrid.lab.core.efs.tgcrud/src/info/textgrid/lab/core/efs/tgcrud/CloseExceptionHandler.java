/**
 * 
 */
package info.textgrid.lab.core.efs.tgcrud;

import static info.textgrid.lab.core.efs.tgcrud.Activator.handleError;
import info.textgrid.lab.core.efs.tgcrud.ExceptionPassingPipedOutputStream.ICloseExceptionHandler;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.IOException;
import java.net.URI;

import org.eclipse.core.filesystem.IFileStore;

/**
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 *
 */
class CloseExceptionHandler implements ICloseExceptionHandler {
	
	private IFileStore fileStore;
	private String message;

	public CloseExceptionHandler(IFileStore fileStore, String message) {
		this.fileStore = fileStore;
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see info.textgrid.lab.core.efs.tgcrud.ExceptionPassingPipedOutputStream.ICloseExceptionHandler#handleException(java.lang.Throwable)
	 */
	public boolean handleException(Throwable exception) throws IOException {
		if (exception instanceof IOException) {
			IOException ioe = (IOException) exception;

			Throwable cause = ioe.getCause();
			if (cause == null)
				cause = ioe;
			Activator.logServiceCall("{0}: {1}", message, cause);
			handleError(ioe, "{2} while working on {0}: {1}", fileStore, cause, cause.getClass().getSimpleName());
			URI textgridUri = TextGridObject
					.computeTextGridURI(fileStore
					.toURI());
			TextGridEFSRuntimeError runtimeError = new TextGridEFSRuntimeError(
					"Error updating/creating " + textgridUri.toString());
			runtimeError.initCause(ioe);
			throw runtimeError;
		}
		
		
		return false;
	}

}
