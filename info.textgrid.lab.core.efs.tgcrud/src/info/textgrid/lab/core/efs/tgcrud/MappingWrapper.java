package info.textgrid.lab.core.efs.tgcrud;

class MappingWrapper {
	protected String fNewContentType = null;
	protected String fNewExtension = null;
	boolean fRemoved = false;
	boolean fAdded = false;

	public MappingWrapper() {
	}

	public MappingWrapper(String contentType, String extension) {
		fNewContentType = contentType;
		fNewExtension = extension;
		fAdded = true;
	}

	public boolean isAdded() {
		return fAdded;
	}

	public String getContentType() {
		if (fNewContentType == null) {
			return "";
		}
		return fNewContentType;
	}

	public void setContentType(String contentType) {
		fNewContentType = contentType;
	}

	public String getExtension() {
		if (fNewExtension == null) {
			return "";
		}
		return fNewExtension;
	}

	public void setExtension(String extension) {
		fNewExtension = extension;
	}

	public boolean isChanged() {
		return !fAdded && !fRemoved;
	}

	public boolean isRemoved() {
		return fRemoved;
	}

	public void setRemoved(boolean removed) {
		fRemoved = removed;
	}
}