/**
 * 
 */
package info.textgrid.lab.core.efs.tgcrud;

import static info.textgrid.lab.core.efs.tgcrud.Activator.logServiceCall;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.LogUtil;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.ui.core.dialogs.TGCrudWarning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectFactory;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.rmi.RemoteException;
import java.text.MessageFormat;

import javax.activation.DataHandler;
import javax.xml.ws.Holder;

import org.eclipse.core.filesystem.IFileInfo;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.provider.FileInfo;
import org.eclipse.core.filesystem.provider.FileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 *
 */
public class TGNewFileFileStore extends FileStore implements IFileStore {

	private URI uri;

	/**
	 * Create a new new file store from URI
	 * @todo document
	 * @param uri
	 */
	public TGNewFileFileStore(URI uri) {
		super();
		this.uri = uri;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileStore#childNames(int, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public String[] childNames(int options, IProgressMonitor monitor)
			throws CoreException {
		return new String[0];
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileStore#fetchInfo(int, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IFileInfo fetchInfo(int options, IProgressMonitor monitor)
			throws CoreException {
		FileInfo fileInfo = new FileInfo(getName());
		fileInfo.setExists(true);
		fileInfo.setLastModified(1);
		return fileInfo;
	}

	/** Returns null because we don't have children.
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#getChild(java.lang.String)
	 */
	@Override
	public IFileStore getChild(String name) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileStore#getName()
	 */
	@Override
	public String getName() {
		return new Path(uri.getPath()).lastSegment().toString();
	}


	/**
	 * New files need a parent, so they get a dummy one. 
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#getParent()
	 * @see TGDummyParent
	 * @category EXPERIMENTAL
	 */
	@Override
	public IFileStore getParent() {
		return new TGDummyParent(this);
	}

	/**
	 * Returns an input stream with the object's
	 * {@linkplain TextGridObject#setInitialContent(byte[]) initial content}, if
	 * available, or an empty input stream otherwise.
	 * 
	 * @see org.eclipse.core.filesystem.provider.FileStore#openInputStream(int,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 * @see TextGridObject#setInitialContent(byte[])
	 */
	@Override
	public InputStream openInputStream(int options, IProgressMonitor monitor)
			throws CoreException {
		byte[] initialContent = getTextGridObject().getInitialContent();
		InputStream result;

		if (initialContent == null)
			result = new ByteArrayInputStream(new byte[0]);
		else
			result = new ByteArrayInputStream(initialContent);
		
		return result;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileStore#toURI()
	 */
	@Override
	public URI toURI() {
		return uri;
	}

	@Override
	public OutputStream openOutputStream(int options, IProgressMonitor monitor)
			throws CoreException {
		
		ExceptionPassingPipedOutputStream outputStream = new ExceptionPassingPipedOutputStream();
		outputStream.setBlockingClose(true); // TG-1603
		outputStream.setCloseExceptionHandler(new CloseExceptionHandler(this,
				"Save (create) failed"));
		
		final TextGridObject tgo = getTextGridObject();
		final TextGridProject project = tgo.getProjectInstance();
		project
				.assertPermission(
						ITextGridPermission.CREATE,
						"Access denied: You do not have the permission to create new objects in {0}.",
						project);
		
		try {
			final ExceptionPassingPipedInputStream inputStream = new ExceptionPassingPipedInputStream(
					outputStream);
			
			
			
			
			Thread serviceThread = new Thread() {

				@Override
				public void run() {
					try {
					try {
						try {
							Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
							metadataHolder.value = new ObjectFactory().createMetadataContainerType();
							metadataHolder.value.setObject(tgo.getMetadata());
							
							DataHandler dataHandler = new DataHandler(
									new InputStreamDataSource(new LoggingInputStream(
										inputStream, new LogUtil(Activator.PLUGIN_ID)),
											"application/octet-stream",
											TGNewFileFileStore.this
															.getName()));
							
							//Revision?
							String URI = tgo.getPreparedURI() == null? null : tgo.getPreparedURI().toString();
							boolean isRevision = false;
							if (!tgo.getRevisionURI().equals("")) {
								isRevision = true;
								URI = tgo.getRevisionURI();
							}

							logServiceCall(
									"Calling TGcrud#create({0}, {1}, {2}) ... (from TGNewFileOutputStream#close)",
										logsession.getInstance().getloginfo(),
										RBACSession.getInstance().getSID(false), 
										metadataHolder.value.getObject());
							TGCrudService crudServiceStub = CrudClientUtilities
									.getCrudServiceStub();
							crudServiceStub.create(RBACSession.getInstance().getSID(false), 
													logsession.getInstance().getloginfo(), 
													URI,
													isRevision, 
													project.getId(),
													metadataHolder, 
													dataHandler);

							final ObjectType newMetadata = metadataHolder.value.getObject();
							logServiceCall("... #create succeeded => {0}",
									newMetadata.toString());
							
							tgo.setRevisionURI("");

							UIJob uiJob = new UIJob("TGCrudWarning") {
			                      @Override
			                      public IStatus runInUIThread(IProgressMonitor monitor) {
			                         //Show Warnings
			                         TGCrudWarning dlg;
									 try {
										dlg = TGCrudWarning.getInstance(newMetadata.getGeneric().getGenerated().getWarning(), tgo.getTitle(), tgo.toString());
										if (dlg != null)
											dlg.OpenDialog();
									 } catch (CoreException e) {
											StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
									 }                   
			                         return Status.OK_STATUS;
			                      }
			                };
								uiJob.setSystem(true); // don't display this in
								// the progress area
			                uiJob.schedule();
							
							/*
							 * The result fails to serialize with Axis 1.3 – cf.
							 * https://issues.apache.org/jira/browse/AXIS2-3227
							 */
							getTextGridObject().move(newMetadata);

							tgo.setMetadataDirty(false);
							tgo.setInitialContent(null);
							TextGridObject.notifyListeners(Event.CREATED, tgo);

						} catch (CoreException e) {
							IOException ioe = new IOException(e
.getLocalizedMessage(), e);
							throw ioe;
						} catch (MetadataParseFault e) {
							IOException ioe = new RemoteException(e
.getLocalizedMessage(), e);
							throw ioe;
						} catch (IoFault e) {
							IOException ioe = new RemoteException(e
.getLocalizedMessage(), e);
							throw ioe;
						} catch (AuthFault e) {
							IOException ioe = new RemoteException(e
									.getLocalizedMessage(), e);
							throw ioe;
						}
					} catch (MetadataRetrievalException e) {
						Throwable cause = e.getCause();
						if (cause == null)
							cause = e;
						Activator
								.logServiceCall(
										"TGcrud#update failed in TGNewFileOutputStream#close() with {0}",
										cause);
						TextGridObject
								.notifyListeners(
										ITextGridObjectListener.Event.METADATA_INCOMPLETE,
										tgo);
						
						String msg = NLS
								.bind(
										"Cannot save TextGridObject {0} because its metadata is invalid. "
												+ "Please check and fix it in the metadata view.",
										tgo);
						// for some reason the error dialog presented by the editor
						// shows only the cause's
						// message, not the exception's message. Therefore, we need
						// to wrap the
						// exception twice. Ugly!!
						throw new MetadataRetrievalException(msg,
								new MetadataRetrievalException(msg, e));
					}
					} catch (Throwable e) { // we're handling runtime exceptions
											// here as well in order to pass
											// them on to the calling thread.
						// Somebody silently ignores our exceptions, so we need
						// to create some feedback here.
						Throwable cause = e.getCause();
						if (cause == null)
							cause = e;
						logServiceCall(
								"TGcrud#create failed in TGNewFileOutputStream#close with {0}",
								cause);
						// Experiment: Pass this on.
						inputStream.passOnException(e);
						// handleError(
						// cause,
						// "An {0} occured during a call to TGcrud.create for {2}: {1}",
						// cause.getClass().getSimpleName(), cause
						// .getLocalizedMessage(), tgo);
						// // throw new InvocationTargetException(e);
					} finally {
						inputStream.done();
					}
					

				}


			};
			serviceThread.setName(MessageFormat.format(
					"Creating {0} in the TextGridRep", getName()));
			serviceThread.start();

		} catch (IOException e) {
			throw new CrudServiceException(e, tgo.getURI());
		}
		
		
		return outputStream;
		
		
	}

	public TextGridObject getTextGridObject() throws CrudServiceException {
		return TextGridObject.getInstance(uri, false);
	}

}
