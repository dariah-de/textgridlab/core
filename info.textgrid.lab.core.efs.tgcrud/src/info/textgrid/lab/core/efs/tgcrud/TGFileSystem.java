package info.textgrid.lab.core.efs.tgcrud;

import java.net.URI;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.provider.FileSystem;
import org.eclipse.core.runtime.CoreException;

/**
 * @author tv
 *
 */
public class TGFileSystem extends FileSystem {

	
	/**
	 * 
	 */
	public TGFileSystem() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.filesystem.provider.FileSystem#getStore(java.net.URI)
	 */
	@Override
	public IFileStore getStore(URI uri) {
		// TODO Auto-generated method stub
		try {
			return new TGFileStore(uri);
		} catch (CoreException e) {
			Activator.getDefault().handleProblem(e);
		}
		return null;
	}

}
