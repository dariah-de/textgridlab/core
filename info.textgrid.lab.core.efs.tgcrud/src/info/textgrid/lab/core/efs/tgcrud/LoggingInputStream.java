package info.textgrid.lab.core.efs.tgcrud;

import info.textgrid.lab.core.swtutils.LogUtil;

import java.io.IOException;
import java.io.InputStream;


/**
 * An InputStream that logs all occuring IOExceptions and RuntimeErrors to a {@link LogUtil}. 
 * 
 * @author tv
 */
public class LoggingInputStream extends InputStream {

	private final InputStream delegate;
	private final LogUtil log;
	private boolean trace = false;

	/**
	 * Creates a new logging input stream around the given input stream that logs all exceptions to the given logger.
	 * 
	 * @param delegate the input stream to do the actual work
	 * @param log the {@link LogUtil} that will log exceptions.
	 */
	public LoggingInputStream(InputStream delegate,
			LogUtil log) {
		super();
		this.delegate = delegate;
		this.log = log;
		trace = Activator.isDebugging(Activator.DEBUG_STREAMS);
	}

	@Override
	public int read() throws IOException {
		try {
			int i = delegate.read();
			if (trace)
				log.trace("Read one byte");
			return i;
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}

	@Override
	public int available() throws IOException {
		try {
			int available = delegate.available();
			if (trace)
				log.trace("Queried availability ({0})", available);
			return available;
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}

	@Override
	public void close() throws IOException {
		try {
			delegate.close();
			if (trace)
				log.trace("Closing stream.");
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		try {
			int read = delegate.read(b, off, len);
			if (trace)
				log.trace("Read {0} of {1} bytes.", read, len);
			return read;
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}

	@Override
	public int read(byte[] b) throws IOException {
		try {
			int read = delegate.read(b);
			if (trace)
				log.trace("Read {0} bytes.", read);
			return read;
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}

	@Override
	public synchronized void reset() throws IOException {
		try {
			delegate.reset();
			if (trace)
				log.trace("Reset stream.");
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}

	@Override
	public long skip(long n) throws IOException {
		try {
			return delegate.skip(n);
		} catch (IOException e) {
			log.logError(e);
			throw e;
		} catch (RuntimeException e) {
			log.logError(e);
			throw e;
		}
	}
}
