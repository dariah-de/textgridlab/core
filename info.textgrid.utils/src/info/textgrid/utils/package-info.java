/**
 * Generic, non-lab-specific utility classes.
 * 
 * Currently, this plug-in contains some collection specific utilities in
 * {@link info.textgrid.util.collection} and some tools for dealing with text in
 * {@link info.textgrid.util.text}.
 */
package info.textgrid.utils;

