package info.textgrid.util.collection;

import java.util.Collection;
import java.util.Iterator;

/**
 * A Filter is a wrapper around an other {@link Collection}, the base
 * collection, that handles only members of a certain type.
 * 
 * <p>
 * 
 * Filters are purely virtual. Every operation that manipulates the filter
 * collection directly manipulates the base collection as well: E.g., if you
 * {@linkplain #remove(Object) remove} an item from the filtered collection, the
 * same item is removed from the base collection. This has the advantage of
 * being a live link and working with every base collection, but it makes
 * operations like {@link #size()}, {@link #toArray()} and
 * {@link #toArray(Object[])} potentially rather slow (because they must iterate
 * throw the base collection).
 * 
 * <h4>Example:</h4>
 * 
 * <pre>
 * class Shape {};
 * class Circle extends Shape{};
 * ...
 * List&lt;Shape&gt; shapes = new ArrayList&lt;Shape&gt;();
 * Collection&lt;Circle&gt; circles = new Filter&lt;Circle&gt;(shapes, Circle.class);
 * 
 * </pre>
 * 
 * @param <E>
 *            The type of the elements contained in this filter.
 * 
 */
public class Filter<E> implements Collection<E>, Iterable<E> {

	private Collection<? super E> base;
	private Class<E> filterType;

	/**
	 * Creates a filter based on the given collection. The filter is a virtual
	 * collection containing only those items in the base collection that are
	 * assignment compatible to the given filterType.
	 * 
	 * <h4>Example</h4>
	 * 
	 * <pre>
	 * Collection&lt;Number&gt; numbers = new ArrayList&lt;Number&gt;();
	 * Collection&lt;Integer&gt; integers = new Filter&lt;Integer&gt;(numbers, Integer.class);
	 * </pre>
	 * 
	 * @param base
	 *            The base collection, the elements of which are of a supertype
	 *            of filterType
	 * @param filterType
	 *            The class for the return type of this filter. (Required
	 *            because generics are a compile-time only thing)
	 * 
	 */
	public Filter(Collection<? super E> base, Class<E> filterType) {
		super();
		this.base = base;
		this.filterType = filterType;
	}

	/**
	 * Returns the base collection for this filter. Manipulations in the base
	 * collection appear in the filtered collection as well, and vice versa.
	 * 
	 * @return the base collection
	 */
	public Collection<? super E> getBase() {
		return base;
	}

	/**
	 * The (virtual) filter collection contains of exactly those members of the
	 * original collection that are assignment compatible to the filter type.
	 * 
	 * @return the filter type
	 */
	public Class<E> getFilterType() {
		return filterType;
	}

	public boolean add(E o) {
		return base.add(o);
	}

	public boolean addAll(Collection<? extends E> c) {
		return base.addAll(c);
	}

	/**
	 * Removes all objects from the underlying collection that are of this
	 * filter's filter type. After this, {@link #isEmpty()} is true, but not
	 * necessarily <code>{@link #getBase()}.{@linkplain Collection#isEmpty()
	 * isEmpty()}</code>.
	 * 
	 * @see java.util.Collection#clear()
	 */
	public void clear() {
		Iterator<E> iterator = iterator();
		while (iterator.hasNext()) {
			iterator.next();
			iterator.remove();
		}
	}

	public boolean contains(Object o) {
		return isAllowed(o) && base.contains(o); 
	}

	public boolean containsAll(Collection<?> c) {
		if (!base.containsAll(c))
			return false;

		for (Object o : c)
			if (!isAllowed(o))
				return false;

		return true;
	}

	/**
	 * Returns true iff the given object would be allowed as a member of this
	 * collection.
	 * 
	 * @param o
	 *            the object to test.
	 * @return whether <var>o</var> may be in instance of
	 *         {@link #getFilterType()}.
	 */
	public boolean isAllowed(Object o) {
		return filterType.isInstance(o);
	}

	public boolean isEmpty() {
		if (base.isEmpty())
			return true;
		return !iterator().hasNext();
	}

	/**
	 * Returns an Iterator that iterates only over the filtered elements of the
	 * base collection.
	 * 
	 * @see java.util.Collection#iterator()
	 * @see FilteringIterator
	 */
	public FilteringIterator<E> iterator() {
		return new FilteringIterator<E>(base.iterator(), filterType);
	}

	public boolean remove(Object o) {
		return base.remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		return base.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		Iterator<E> iterator = iterator();
		boolean result = false;
		while (iterator.hasNext()) {
			if (!c.contains(iterator.next())) {
				iterator.remove();
				result = true;
			}
		}
		return result;
	}

	public int size() {
		int count = 0;
		Iterator<E> iterator = iterator();
		while (iterator.hasNext()) {
			iterator.next();
			count++;
		}
		return count;
	}

	public Object[] toArray() {
		return toArray(new Object[0]);
	}

	@SuppressWarnings("unchecked")
	public <T> T[] toArray(T[] a) {
		// because size() is to expensive, we simply try here ...
		T[] result = a;
		int pos = 0;
		Iterator<E> iter = iterator();

		while (iter.hasNext()) {
			if (pos == a.length) {
				// allocate a new array. Now we must count.
				// I've copied the following trick from ArrayList,
				result = (T[]) java.lang.reflect.Array.newInstance(a.getClass()
						.getComponentType(), size());
				System.arraycopy(a, 0, result, 0, pos);
			}
			result[pos] = (T) iter.next();
			pos++;
		}
		
		if (pos < result.length)
			result[pos] = null;

		return result;
	}

}
