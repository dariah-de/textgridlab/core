package info.textgrid.util.collection;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * The {@link Iterator} implementation returned by {@link Filter#iterator()}.
 */
public class FilteringIterator<E> implements Iterator<E> {

	/**
	 * @return the filterType
	 */
	public Class<E> getFilterType() {
		return filterType;
	}

	private Iterator<? super E> base;
	/**
	 * hasNext() sets the nextElement field to the next element to be returned
	 * by {@link #next()}, {@link #next()} itself resets this to null when it
	 * returns the item. nextElement being not null means the base iterator has
	 * already proceeded further.
	 */
	private E nextElement = null;
	private Class<E> filterType;

	FilteringIterator(Iterator<? super E> iterator, Class<E> filterType) {
		base = iterator;
		this.filterType = filterType;
		// TODO Auto-generated constructor stub
	}

	// has the side effect of filling the nextElement field.
	@SuppressWarnings("unchecked")
	public boolean hasNext() {
		if (nextElement != null)
			return true;
		while (base.hasNext()) {
			Object next = base.next();
			if (filterType.isInstance(next)) {
				nextElement = (E) next;
				return true;
			}
		}
		return false;
	}

	// uses hasNext()s side effect
	public E next() {
		if (!hasNext())
			throw new NoSuchElementException();

		E next = nextElement;
		nextElement = null;
		return next;
	}

	/**
	 * Removes the current element from the underlying collection. This method
	 * may only be called immediately after a call to {@link #next()}. If you
	 * call {@link #hasNext()} after {@link #next()} but before
	 * {@link #remove()}, we must throw an {@link IllegalStateException} because
	 * the underlying {@link Iterator} has already iterated further.
	 * 
	 * @see java.util.Iterator#remove()
	 * @throws UnsupportedOperationException
	 *             if the {@linkplain Filter#getBase() underlying collection}'s
	 *             iterator does not support remove
	 * @throws IllegalStateException
	 *             if you have called a method of this iterator after the last
	 *             successful {@link #next()} operation
	 */
	public void remove() {
		if (nextElement != null)
			throw new IllegalStateException(
					"You may not call remove() between hasNext() and next() while iterating over a Filter collection.");
		base.remove();
	}

}
