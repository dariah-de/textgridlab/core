package info.textgrid.util.text;


public class TextUtils {

	/**
	 * Utility function that returns a representation of the given string with
	 * control characters replaced with unicode symbols representing those
	 * control characters. Uses ↵ for newline (\n), ⇥ for tab (\t), ␡ for
	 * delete, otherwise symbols from ␀ to ␟ and � for other control characters.
	 * 
	 * @param in
	 *            The string to convert.
	 * @param showSpace
	 *            if true, space characters are replaced with ␣.
	 * @return A string of equal length, with control characters replaced.
	 */
	public static String visualizeControlChars(String in, boolean showSpace) {
		StringBuilder out = new StringBuilder(in);

		for (int i = 0; i < out.length(); i++) {
			
			if (out.charAt(i) == '\n') {
				out.setCharAt(i, '\u21B5'); // ↵
			} else if (out.charAt(i) == '\t') {
				out.setCharAt(i, '\u21E5'); // ⇥
			} else if (out.charAt(i) < ' ') { // ␀ .. ␙
				out.setCharAt(i, (char) (out.charAt(i) + 0x2400));
			} else if (out.charAt(i) == ' ' && showSpace) {
				out.setCharAt(i, '\u2423'); // ␣
			} else if (out.charAt(i) == '\u007f') {
				out.setCharAt(i, '\u2421'); // ␡
			} else if (Character.isISOControl(out.charAt(i))) {
				out.setCharAt(i, '\uFFFD'); // �, yes, the literal
											// unrepresentable char
			}
			
		}

		return out.toString();
	}
}
