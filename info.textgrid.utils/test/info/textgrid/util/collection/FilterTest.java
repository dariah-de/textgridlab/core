package info.textgrid.util.collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

public class FilterTest {
	
	public class Super {};
	public class Sub extends Super {};
	
	protected Collection<Super> prepareCollection(Super containedItem) {
		ArrayList<Super> list = new ArrayList<Super>(4);
		list.add(new Super());
		list.add(new Sub());
		list.add(new Super());
		list.add(containedItem);
		return list;
	}
	
	@Test
	public void testSize() {
		Collection<Super> collection = prepareCollection(new Sub());
		Filter<Sub> filter = new Filter<Sub>(collection, Sub.class);
		assertEquals(2, filter.size());
	}

	@Test
	public void testContains() {
		Sub sub = new Sub();
		Collection<Super> collection = prepareCollection(sub);
		Filter<Sub> filter = new Filter<Sub>(collection, Sub.class);
		assertTrue(filter.contains(sub));
	}

	@Test
	public void testAdd() {
		Collection<Super> collection = prepareCollection(new Super());
		Filter<Sub> filter = new Filter<Sub>(collection, Sub.class);
		Sub sub = new Sub();
		filter.add(sub);
		assertTrue("Filter does not contain added object", filter.contains(sub));
		assertTrue("Base collection does not contain added object", collection
				.contains(sub));
	}

	@Test
	public void testRemove() {
		Sub sub = new Sub();
		Collection<Super> collection = prepareCollection(sub);
		Filter<Sub> filter = new Filter<Sub>(collection, Sub.class);
		filter.remove(sub);
		assertTrue(!filter.contains(sub));
		assertTrue(!collection.contains(sub));
	}
	
	@Test
	public void testClear() {
		Collection<Super> collection = prepareCollection(new Sub());
		Filter<Sub> filter = new Filter<Sub>(collection, Sub.class);
		filter.clear();
		assertTrue(filter.isEmpty());
		assertEquals(2, collection.size());
	}

	@Test
	public void testToArrayTArray() {
		Super[] source = new Super[] { new Super(), new Sub(), new Super(),
				new Sub() };
		Super[] expected = new Super[] { source[1], source[3] };
		ArrayList<Super> coll = new ArrayList<Super>(Arrays.asList(source));
		Filter<Sub> filter = new Filter<Sub>(coll, Sub.class);
		
		Sub[] filteredArray = filter.toArray(new Sub[0]);
		assertArrayEquals(expected, filteredArray);

		Sub[] filtered2 = filter.toArray(new Sub[2]);
		assertArrayEquals(expected, filtered2);

		Sub[] filtered1 = filter.toArray(new Sub[1]);
		assertArrayEquals(expected, filtered1);
	}

}
