package info.textgrid.lab.ui.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * This perspective arranges the TextGrid views and lets the editorArea visible.
 * It should be used when a file is opened for editing, e.g., in the XML editor.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv)
 */
public class EditPerspectiveFactory implements IPerspectiveFactory {

	public static final String ID = "info.textgrid.lab.ui.perspectives.edit"; //$NON-NLS-1$

	public void createInitialLayout(IPageLayout layout) {

		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true); // needed for our editors

		layout.addPerspectiveShortcut(ID);

		IFolderLayout rightTop = layout.createFolder(ID + ".right-top", //$NON-NLS-1$
				IPageLayout.RIGHT, 0.7f, editorArea);
		IFolderLayout rightBottom = layout.createFolder(ID + ".right-bottom", //$NON-NLS-1$
				IPageLayout.BOTTOM, 0.5f, ID + ".right-top"); //$NON-NLS-1$
		IFolderLayout bottom = layout.createFolder(ID
				+ ".bottom", IPageLayout.BOTTOM, 0.75f, editorArea); //$NON-NLS-1$
		IFolderLayout left = layout.createFolder(ID + ".left", IPageLayout.LEFT, //$NON-NLS-1$
				0.4f, editorArea);


		left.addView("info.textgrid.lab.search.ui.views.metadata"); //$NON-NLS-1$
		left.addView(IPageLayout.ID_OUTLINE);
		rightBottom.addView("info.textgrid.lab.woerterbuchnetz.dictionarySelection"); //$NON-NLS-1$
		rightTop.addView(IPageLayout.ID_PROP_SHEET);

		bottom.addView("info.textgrid.lab.woerterbuchnetz.dictonaryWeb"); //$NON-NLS-1$

		bottom.addPlaceholder(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addPlaceholder(IPageLayout.ID_PROGRESS_VIEW);
		bottom.addPlaceholder("org.eclipse.pde.runtime.LogView"); //$NON-NLS-1$
		

	}

}
