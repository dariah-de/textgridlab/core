package info.textgrid.lab.ui.core.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.ui.core.dialogs.messages"; //$NON-NLS-1$
	public static String AllMetadataDialog_Changes;
	public static String AllMetadataDialog_CouldntReloadMetadata;
	public static String AllMetadataDialog_Creation;
	public static String AllMetadataDialog_DataContributor;
	public static String AllMetadataDialog_EM_CoundntReadMetadata;
	public static String AllMetadataDialog_GenericTitle;
	public static String AllMetadataDialog_LastChange;
	public static String AllMetadataDialog_MIMEType;
	public static String AllMetadataDialog_ObjectProperties;
	public static String AllMetadataDialog_ObjectSize;
	public static String AllMetadataDialog_ObjectTitle;
	public static String AllMetadataDialog_Permissions;
	public static String AllMetadataDialog_Permissions2;
	public static String AllMetadataDialog_ProjectID;
	public static String AllMetadataDialog_ReadMetadata;
	public static String AllMetadataDialog_Refresh;
	public static String AllMetadataDialog_ReloadMetadata;
	public static String AllMetadataDialog_RevisionNumber;
	public static String AllMetadataDialog_TextGridUri;
	public static String AllMetadataDialog_TimeCreated;
	public static String AllMetadataDialog_TitleName;
	public static String AllMetadataDialog_UI_KnownMetadata;
	public static String AllMetadataDialog_UpdateGUI;
	public static String MetadataPage_PageName;
	public static String MetadataPage_UI_EnterLater;
	public static String MetadataPage_UI_EnterName;
	public static String NewObjectWizard_DialogTitle;
	public static String NewObjectWizard_EM_CouldNotFindEditor;
	public static String NewObjectWizard_EM_CouldNotMakePersistent;
	public static String NewObjectWizard_PageTitle;
	public static String NewObjectWizard_WindowTitle;
	public static String OpenURIDialog_EM_NotValid;
	public static String OpenURIDialog_EM_Scheme;
	public static String OpenURIDialog_IM_EnterOrPaste;
	public static String OpenURIDialog_IM_EnterOrPasteInField;
	public static String OpenURIDialog_IM_ExportURI;
	public static String OpenURIDialog_IM_OpenTGURI;
	public static String OpenURIDialog_IM_OpenURI;
	public static String OpenURIDialog_IM_OpenURIDefault;
	public static String OpenURIDialog_LabelOpen;
	public static String TGCrudWarning_UM_DoNotShowAgain;
	public static String TGCrudWarning_WarningIntro;
	public static String NewTGOPage_create_new_project_Error;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
