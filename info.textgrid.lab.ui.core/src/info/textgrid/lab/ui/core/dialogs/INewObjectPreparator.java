/**
 * 
 */
package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jface.wizard.IWizard;

/**
 * The New Object Preparator coordinates the content type specific preparation
 * steps for creating a new TextGrid object.
 * 
 * <h3>Lifecycle</h3>
 * <ol>
 * <li>After the page of the New Object Wizard allowing the user to select the
 * content type for his new object has been completed, the first contribution to
 * the extension point <tt>info.textgrid.lab.ui.core.newObjectContributor</tt>
 * for the content type selected is searched. If it has been found, it is loaded
 * and parsed. The {@link INewObjectPreparator} configured therein is then
 * instantiated via
 * {@link IConfigurationElement#createExecutableExtension(String)}, i.e. using
 * the zero-argument constructor.</li>
 * <li>The wizard introduces itself to the new preparator using
 * {@link #setWizard(ITextGridWizard)}.</li>
 * <li>The wizard creates the {@link ITextGridWizardPage}s defined in the
 * extension point (if any). Each new wizard page is
 * {@linkplain ITextGridWizardPage#init(ITextGridWizard, INewObjectPreparator)
 * initialized with the wizard and the preparator}.</li>
 * <li>The pages are added to the wizard.</li>
 * <li>{@link #initializeObject(TextGridObject)} is called to allow the
 * preparator to, well, initialize the object before any other wizard page is
 * shown.</li>
 * <li>The user can flip through the {@link MetadataPage} and the content type
 * specific wizard pages.</li>
 * <li>When the user clicks the Finish button,
 * {@link #performFinish(TextGridObject)} is called.</li> </ul>
 * 
 * 
 */
public interface INewObjectPreparator {
	
	/**
	 * Notifies this new object preparator of the wizard in which it runs.
	 */
	public void setWizard(ITextGridWizard wizard);

	/**
	 * This method is called after {@link #setWizard(ITextGridWizard)}, but
	 * before the metadata page or the first content page is shown. It may be
	 * used to initialize certain data in the object, e.g. that should be loaded
	 * into the metadata page.
	 * 
	 * @param textGridObject
	 *            The TextGridObject to create. This would typically be
	 *            {@linkplain TextGridObject#isNew() new} and contain just
	 *            project and content type.
	 */
	public void initializeObject(TextGridObject textGridObject);

	/**
	 * Does whatever should be done after finishing the dialog. This is called
	 * after the user has pressed the Finish button in the wizard.
	 * 
	 * @param textGridObject
	 *            The {@link TextGridObject} as it has been set up by the
	 *            wizard.
	 * @return true if the finish has been performed, false if it has been
	 *         refused.
	 * @see NewObjectWizard#defaultPerformFinish(),
	 *      {@link IWizard#performFinish()}
	 */
	public boolean performFinish(TextGridObject textGridObject);
}
