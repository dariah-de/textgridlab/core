/**
 * 
 */
package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.metadataeditor.MetaDataSection;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * @author tv, Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class MetadataPage extends WizardPage implements ITextGridWizardPage {

	private MetaDataSection metaDataSection = null;
	private TextGridObject textGridObject;
	private boolean needsCompleteMetadata;
	private ITextGridWizard wizard;

	private Composite container = null;
	//private Composite mdPageComposite = null;
	//private ScrolledPageArea mdPageArea = null;
	private ScrolledForm mdPageScrolledForm = null;

	/**
	 * @param pageName
	 */
	public MetadataPage(String pageName) {
		super(pageName, Messages.MetadataPage_PageName, null);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isPageComplete() {
		return (!needsCompleteMetadata || (textGridObject != null && textGridObject
				.isSubmittable()));
	}

	/**
	 * @param pageName
	 * @param title
	 * @param titleImage
	 */
	public MetadataPage(String pageName, String title,
			ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NONE);
		container.setLayoutData(parent.getLayoutData());
		container.setLayout(parent.getLayout());

		// mdPageArea = new ScrolledPageArea(container);
		// mdPageComposite = mdPageArea.getPageAreaComposite();
		// mdPageScrolledForm = mdPageArea.getPageAreaScrolledForm();

		String info = Messages.MetadataPage_UI_EnterName;
		if (!isNeedsCompleteMetadata())
			info = info + Messages.MetadataPage_UI_EnterLater;
		setMessage(info);
		// infoLabel.setText(info);

		// GridDataFactory.fillDefaults().grab(true, true).applyTo(
		// mdPageComposite);
		// GridDataFactory.fillDefaults().grab(true,
		// true).applyTo(mdPageScrolledForm);

		metaDataSection = new MetaDataSection(container, mdPageScrolledForm,
				wizard.getTextGridObject());

		setControl(container);
	}

	public void finishPage() {
		metaDataSection.updateTextGridObject();
	}

	public void init(ITextGridWizard wizard, INewObjectPreparator preparator) {
		this.wizard = wizard;
	}

	public void loadObject(TextGridObject textGridObject) {
		if (metaDataSection != null)
			metaDataSection.setNewTGObject(textGridObject, true);
	}

	public void setNeedsCompleteMetadata(boolean needsCompleteMetadata) {
		this.needsCompleteMetadata = needsCompleteMetadata;
	}

	public boolean isNeedsCompleteMetadata() {
		return needsCompleteMetadata;
	}

}
