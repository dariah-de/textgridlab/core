package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * A dialog to display all metadata of a given object in relatively raw form.
 * 
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 * @author Julian Dabbert
 * 
 */
public class AllMetadataDialog extends TitleAreaDialog {

	private Text textgridUriTextBox;
	private Text datacontributorTextBox;
	private Text fileformatTextBox;
	private Text filenameTextBox;
	private Text filesizeTextBox;
	private Text permissionsTextBox;
	private Text projectnameTextBox;
	private Text revisionnumberTextBox;
	private Text timecreatedTextBox;
	private Text timeoflastchangeTextBox;
	private TextGridObject textGridObject;
	private Text xmlTextField;
	private Button refreshButton;
	private String fileName;

	private TextGridObjectChangeListener textGridObjectChangeListener;

	public AllMetadataDialog(Shell parentShell, TextGridObject object) {
		super(parentShell);
		setShellStyle(SWT.BORDER | SWT.CLOSE | SWT.MIN | SWT.RESIZE);
		this.textGridObject = object;
		this.textGridObjectChangeListener = new TextGridObjectChangeListener();
		TextGridObject.addListener(this.textGridObjectChangeListener);
	}

	private class TextGridObjectChangeListener implements
			ITextGridObjectListener {
		@Override
		public void textGridObjectChanged(Event event, TextGridObject object) {
			if (event == Event.METADATA_CHANGED_FROM_OUTSIDE) {
				reloadMetaData();
				
			}
		}
	}

	@Override
	public boolean close() {
		TextGridObject.removeListener(this.textGridObjectChangeListener);
		this.textGridObjectChangeListener = null;
		this.textGridObject = null;
		return super.close();
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		refreshButton = createButton(parent, IDialogConstants.RETRY_ID,
				Messages.AllMetadataDialog_Refresh, false);
		refreshButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				reloadMetaData();	
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				reloadMetaData();
			}
		});
		
		createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.CLOSE_LABEL, true);
	}

	/**
	 * Displays the technical metadata in a dialog. One tab displays often
	 * available information for human quick access, the other embeds all
	 * available metadata in a default Browser object results in an xml tree
	 * structure
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
//		getFileName();
		setTitle(Messages.AllMetadataDialog_TitleName);
		setDialogMessage();
		getShell().setText(Messages.AllMetadataDialog_GenericTitle);

		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		TabFolder tabFolder = new TabFolder(container, SWT.NONE);
		tabFolder.setBounds(0, 0, 490, 398);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		TabItem tbtmDefaultMetadata = new TabItem(tabFolder, SWT.NONE);
		tbtmDefaultMetadata.setText("Default Metadata"); //$NON-NLS-1$

		Composite composite = new Composite(tabFolder, SWT.NONE);
		tbtmDefaultMetadata.setControl(composite);
		composite.setLayout(new FormLayout());

		Group grpObjectProperties = new Group(composite, SWT.NONE);
		FormData fd_grpObjectProperties = new FormData();
		fd_grpObjectProperties.right = new FormAttachment(100, 4);
		fd_grpObjectProperties.left = new FormAttachment(0);
		grpObjectProperties.setLayoutData(fd_grpObjectProperties);
		grpObjectProperties.setFont(SWTResourceManager.getFont("Lucida Grande", //$NON-NLS-1$
				11, SWT.BOLD));
		grpObjectProperties
				.setText(Messages.AllMetadataDialog_ObjectProperties);
		grpObjectProperties.setLayout(new FormLayout());

		Label lblNewLabel = new Label(grpObjectProperties, SWT.NONE);
		FormData fd_lblFileName = new FormData();
		fd_lblFileName.top = new FormAttachment(0, 10);
		fd_lblFileName.left = new FormAttachment(0);
		lblNewLabel.setLayoutData(fd_lblFileName);
		lblNewLabel.setText(Messages.AllMetadataDialog_ObjectTitle);

		filenameTextBox = new Text(grpObjectProperties, SWT.BORDER);
		FormData fd_lblFilenamecontent = new FormData();
		fd_lblFilenamecontent.right = new FormAttachment(100, -5);
		fd_lblFilenamecontent.top = new FormAttachment(0, 10);
		fd_lblFilenamecontent.left = new FormAttachment(0, 116);
		filenameTextBox.setLayoutData(fd_lblFilenamecontent);
		formatTextBox(filenameTextBox);
		filenameTextBox.setText(fileName);

		Label lblNewLabel_1 = new Label(grpObjectProperties, SWT.NONE);
		FormData fd_lblFileFormat = new FormData();
		fd_lblFileFormat.top = new FormAttachment(0, 35);
		fd_lblFileFormat.left = new FormAttachment(0);
		lblNewLabel_1.setLayoutData(fd_lblFileFormat);
		lblNewLabel_1.setText(Messages.AllMetadataDialog_MIMEType);

		fileformatTextBox = new Text(grpObjectProperties, SWT.BORDER);
		FormData fd_lblFileformatcontent = new FormData();
		fd_lblFileformatcontent.right = new FormAttachment(100, -5);
		fd_lblFileformatcontent.top = new FormAttachment(0, 35);
		fd_lblFileformatcontent.left = new FormAttachment(0, 116);
		fileformatTextBox.setLayoutData(fd_lblFileformatcontent);
		formatTextBox(fileformatTextBox);

		Label lblNewLabel_3 = new Label(grpObjectProperties, SWT.NONE);
		FormData fd_lblFileSize = new FormData();
		fd_lblFileSize.top = new FormAttachment(0, 60);
		fd_lblFileSize.left = new FormAttachment(0);
		lblNewLabel_3.setLayoutData(fd_lblFileSize);
		lblNewLabel_3.setText(Messages.AllMetadataDialog_ObjectSize);

		filesizeTextBox = new Text(grpObjectProperties, SWT.BORDER);
		FormData fd_lblFilesizecontent = new FormData();
		fd_lblFilesizecontent.right = new FormAttachment(100, -5);
		fd_lblFilesizecontent.top = new FormAttachment(0, 60);
		fd_lblFilesizecontent.left = new FormAttachment(0, 116);
		filesizeTextBox.setLayoutData(fd_lblFilesizecontent);
		filesizeTextBox.setEditable(false);
		formatTextBox(filesizeTextBox);

		Group grpChanges = new Group(composite, SWT.NONE);
		FormData fd_grpChanges = new FormData();
		fd_grpChanges.top = new FormAttachment(0, 103);
		fd_grpChanges.left = new FormAttachment(0);
		fd_grpChanges.right = new FormAttachment(100, 4);
		grpChanges.setLayoutData(fd_grpChanges);
		grpChanges.setFont(SWTResourceManager.getFont("Lucida Grande", 11, //$NON-NLS-1$
				SWT.BOLD));
		grpChanges.setText(Messages.AllMetadataDialog_Changes);
		grpChanges.setLayout(new FormLayout());

		Label lblNewLabel_2 = new Label(grpChanges, SWT.NONE);
		FormData fd_lblRevisionNumber = new FormData();
		fd_lblRevisionNumber.top = new FormAttachment(0, 10);
		fd_lblRevisionNumber.left = new FormAttachment(0);
		lblNewLabel_2.setLayoutData(fd_lblRevisionNumber);
		lblNewLabel_2.setText("Revision:"); //$NON-NLS-1$

		revisionnumberTextBox = new Text(grpChanges, SWT.BORDER);
		FormData fd_lblRevisionnumbercontent = new FormData();
		fd_lblRevisionnumbercontent.right = new FormAttachment(100, -5);
		fd_lblRevisionnumbercontent.top = new FormAttachment(0, 10);
		fd_lblRevisionnumbercontent.left = new FormAttachment(0, 116);
		revisionnumberTextBox.setLayoutData(fd_lblRevisionnumbercontent);
		formatTextBox(revisionnumberTextBox);

		Label lblNewLabel_4 = new Label(grpChanges, SWT.NONE);
		FormData fd_lblTimeOfLast = new FormData();
		fd_lblTimeOfLast.top = new FormAttachment(0, 35);
		fd_lblTimeOfLast.left = new FormAttachment(0);
		lblNewLabel_4.setLayoutData(fd_lblTimeOfLast);
		lblNewLabel_4.setText("Last change:"); //$NON-NLS-1$

		timeoflastchangeTextBox = new Text(grpChanges, SWT.BORDER);
		FormData fd_lblTimeoflastchangecontent = new FormData();
		fd_lblTimeoflastchangecontent.right = new FormAttachment(100, -5);
		fd_lblTimeoflastchangecontent.top = new FormAttachment(0, 35);
		fd_lblTimeoflastchangecontent.left = new FormAttachment(0, 116);
		timeoflastchangeTextBox.setLayoutData(fd_lblTimeoflastchangecontent);
		formatTextBox(timeoflastchangeTextBox);

		Group grpCreation = new Group(composite, SWT.NONE);
		FormData fd_grpCreation = new FormData();
		fd_grpCreation.left = new FormAttachment(0);
		fd_grpCreation.right = new FormAttachment(100, 4);
		fd_grpCreation.top = new FormAttachment(0, 181);
		grpCreation.setLayoutData(fd_grpCreation);
		grpCreation.setFont(SWTResourceManager.getFont("Lucida Grande", 11, //$NON-NLS-1$
				SWT.BOLD));
		grpCreation.setText(Messages.AllMetadataDialog_Creation);
		grpCreation.setLayout(new FormLayout());

		Label lblNewLabel_5 = new Label(grpCreation, SWT.NONE);
		FormData fd_lblDataContributor = new FormData();
		fd_lblDataContributor.top = new FormAttachment(0, 10);
		fd_lblDataContributor.left = new FormAttachment(0);
		lblNewLabel_5.setLayoutData(fd_lblDataContributor);
		lblNewLabel_5.setText("Data contributor:"); //$NON-NLS-1$

		datacontributorTextBox = new Text(grpCreation, SWT.BORDER);
		FormData fd_lblDatacontributorcontent = new FormData();
		fd_lblDatacontributorcontent.right = new FormAttachment(100, -5);
		fd_lblDatacontributorcontent.top = new FormAttachment(0, 10);
		fd_lblDatacontributorcontent.left = new FormAttachment(0, 116);
		datacontributorTextBox.setLayoutData(fd_lblDatacontributorcontent);
		formatTextBox(datacontributorTextBox);

		Label lblNewLabel_6 = new Label(grpCreation, SWT.NONE);
		FormData fd_lblTimeCreated = new FormData();
		fd_lblTimeCreated.top = new FormAttachment(0, 35);
		fd_lblTimeCreated.left = new FormAttachment(0);
		lblNewLabel_6.setLayoutData(fd_lblTimeCreated);
		lblNewLabel_6.setText("Time created:"); //$NON-NLS-1$

		timecreatedTextBox = new Text(grpCreation, SWT.BORDER);
		FormData fd_lblTimecreatedcontent = new FormData();
		fd_lblTimecreatedcontent.right = new FormAttachment(100, -5);
		fd_lblTimecreatedcontent.top = new FormAttachment(0, 35);
		fd_lblTimecreatedcontent.left = new FormAttachment(0, 116);
		timecreatedTextBox.setLayoutData(fd_lblTimecreatedcontent);
		formatTextBox(timecreatedTextBox);

		Group grpPermissions = new Group(composite, SWT.NONE);
		FormData fd_grpServerStorage = new FormData();
		fd_grpServerStorage.left = new FormAttachment(0);
		fd_grpServerStorage.right = new FormAttachment(100, 4);
		fd_grpServerStorage.top = new FormAttachment(0, 260);
		grpPermissions.setLayoutData(fd_grpServerStorage);
		grpPermissions.setFont(SWTResourceManager.getFont("Lucida Grande", 11, //$NON-NLS-1$
				SWT.BOLD));
		grpPermissions.setText(Messages.AllMetadataDialog_Permissions);
		grpPermissions.setLayout(new FormLayout());

		Label lblNewLabel_7 = new Label(grpPermissions, SWT.NONE);
		FormData fd_lblProjectName = new FormData();
		fd_lblProjectName.top = new FormAttachment(0, 10);
		fd_lblProjectName.left = new FormAttachment(0);
		lblNewLabel_7.setLayoutData(fd_lblProjectName);
		lblNewLabel_7.setText(Messages.AllMetadataDialog_ProjectID);

		projectnameTextBox = new Text(grpPermissions, SWT.BORDER);
		FormData fd_lblProjectnamecontent = new FormData();
		fd_lblProjectnamecontent.right = new FormAttachment(100, -5);
		fd_lblProjectnamecontent.top = new FormAttachment(0, 10);
		fd_lblProjectnamecontent.left = new FormAttachment(0, 116);
		projectnameTextBox.setLayoutData(fd_lblProjectnamecontent);
		formatTextBox(projectnameTextBox);

		Label lblNewLabel_8 = new Label(grpPermissions, SWT.NONE);
		FormData fd_lblPermissions = new FormData();
		fd_lblPermissions.top = new FormAttachment(0, 35);
		fd_lblPermissions.left = new FormAttachment(0);
		lblNewLabel_8.setLayoutData(fd_lblPermissions);
		lblNewLabel_8.setText(Messages.AllMetadataDialog_Permissions2);

		permissionsTextBox = new Text(grpPermissions, SWT.BORDER);
		FormData fd_lblPermissionscontent = new FormData();
		fd_lblPermissionscontent.right = new FormAttachment(100, -5);
		fd_lblPermissionscontent.top = new FormAttachment(0, 35);
		fd_lblPermissionscontent.left = new FormAttachment(0, 116);
		permissionsTextBox.setLayoutData(fd_lblPermissionscontent);
		formatTextBox(permissionsTextBox);

		Label lblNewLabel_9 = new Label(grpPermissions, SWT.NONE);
		FormData fd_lblTextgridUri = new FormData();
		fd_lblTextgridUri.top = new FormAttachment(0, 60);
		fd_lblTextgridUri.left = new FormAttachment(0);
		lblNewLabel_9.setLayoutData(fd_lblTextgridUri);
		lblNewLabel_9.setText(Messages.AllMetadataDialog_TextGridUri);

		textgridUriTextBox = new Text(grpPermissions, SWT.BORDER);
		FormData fd_lblTextgridUricontent = new FormData();
		fd_lblTextgridUricontent.right = new FormAttachment(100, -5);
		fd_lblTextgridUricontent.top = new FormAttachment(0, 60);
		fd_lblTextgridUricontent.left = new FormAttachment(0, 116);
		textgridUriTextBox.setLayoutData(fd_lblTextgridUricontent);
		formatTextBox(textgridUriTextBox);

		Label incompleteWarning = new Label(parent, SWT.WRAP);
		incompleteWarning.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
				false));
		incompleteWarning.setText(Messages.AllMetadataDialog_UI_KnownMetadata);

		reloadMetaData();

		TabItem tbtmRawMetadata = new TabItem(tabFolder, SWT.NONE);
		tbtmRawMetadata.setText("Raw Metadata"); //$NON-NLS-1$

		Composite compositeRaw = new Composite(tabFolder, SWT.NONE);
		tbtmRawMetadata.setControl(compositeRaw);
		compositeRaw.setLayout(new GridLayout(1, false));

		xmlTextField = new Text(compositeRaw, SWT.BORDER | SWT.READ_ONLY | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		xmlTextField.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		return area;
	}

	private void setDialogMessage() {
		getFileName();
		setMessage("\t\t" + fileName); //$NON-NLS-1$
	}

	private String getFileName() {
		fileName = "name"; //$NON-NLS-1$
		try {
			fileName = textGridObject.getTitle();
		} catch (CoreException e) {
			fileName = textGridObject.toString();
			try {
				fileName = textGridObject.getNameCandidate();
			} catch (CoreException e1) {
				Activator.handleError(e1, "problem retrieving name candidate"); //$NON-NLS-1$
			}
		}
		return fileName;
	}

	/**
	 * Return the initial size of the dialog. different for (mac & win) and
	 * linux.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 620);
	}

	/**
	 * reloads the metadata on the object and refreshes the visuals reload
	 * metadata in object is implied in tgo.getmetadata()
	 */
	private void reloadMetaData() {
		Job reloadJob = new Job(NLS.bind(
				Messages.AllMetadataDialog_ReloadMetadata,
				this.textGridObject.toString())) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					textGridObject.reloadMetadata(true);
				} catch (CrudServiceException e) {
					Activator.handleError(e,
							Messages.AllMetadataDialog_CouldntReloadMetadata,
							textGridObject);
				}
				return new Status(IStatus.OK, Activator.PLUGIN_ID,
						Messages.AllMetadataDialog_ReadMetadata);
			}
		};
		reloadJob.addJobChangeListener(new JobChangeAdapter() {
			public void done(IJobChangeEvent event) {
				if (event.getResult().isOK()) {
					UIJob uiJob = new UIJob(
							Messages.AllMetadataDialog_UpdateGUI) {
						public IStatus runInUIThread(IProgressMonitor monitor) {
							try {
								updateGui();
							} catch (CrudServiceException e) {
								return Activator
										.handleError(
												e,
												Messages.AllMetadataDialog_EM_CoundntReadMetadata,
												(Object[]) null);
							}
							return Status.OK_STATUS;
						}
					};
					uiJob.schedule();
				}
			}
		});
		reloadJob.setUser(true);
		reloadJob.schedule();
	}

	private static void formatTextBox(Text preText) {
		preText.setEditable(false);
		preText.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
	}

	/**
	 * Creates and opens a new non-blocking {@link AllMetadataDialog}.
	 * 
	 * @param textGridObject
	 *            The object to display
	 * @param parentShell
	 *            The parent shell for the dialog.
	 * @return The dialog.
	 */
	public static AllMetadataDialog show(TextGridObject textGridObject,
			Shell parentShell) {
		AllMetadataDialog dialog = new AllMetadataDialog(parentShell,
				textGridObject);
		dialog.setBlockOnOpen(false);
		dialog.open();
		return dialog;
	}

	/**
	 * updates the textbox contents with the latest data from the textgridobject
	 * 
	 * @throws CrudServiceException
	 */
	private void updateGui() throws CrudServiceException {
		ObjectType metadataForReading = textGridObject.getMetadataForReading();

		String formatContentString = metadataForReading.getGeneric()
				.getProvided().getFormat();
		String permissionsContentString = metadataForReading.getGeneric()
				.getGenerated().getPermissions();
		String createdContentString = metadataForReading.getGeneric()
				.getGenerated().getCreated().toString();
		String lastModifiedContentString = metadataForReading.getGeneric()
				.getGenerated().getLastModified().toString();
		String revisionContentString = String.valueOf(metadataForReading
				.getGeneric().getGenerated().getRevision());
		String textgridUriContentString = metadataForReading.getGeneric()
				.getGenerated().getTextgridUri().getValue();
		//solved TG-1836
		String projectContentString = metadataForReading.getGeneric()
				.getGenerated().getProject().getId();
		String dataContributorContentString = metadataForReading.getGeneric()
				.getGenerated().getDataContributor();
		String extentContentString = String.valueOf(metadataForReading
				.getGeneric().getGenerated().getExtent());
		int parsedExtent = Integer.parseInt(extentContentString);
		extentContentString = " Byte"; //$NON-NLS-1$
		if (parsedExtent > 1024) {
			parsedExtent = parsedExtent / 1024;
			extentContentString = " KiloByte"; //$NON-NLS-1$
		}
		if (parsedExtent > 1024) {
			parsedExtent = parsedExtent / 1024;
			extentContentString = " MegaByte"; //$NON-NLS-1$
		}
		extentContentString = parsedExtent + extentContentString;
		
		setDialogMessage();
		filenameTextBox.setText(getFileName());
		fileformatTextBox.setText(formatContentString);
		filesizeTextBox.setText(extentContentString);
		revisionnumberTextBox.setText(revisionContentString);
		timeoflastchangeTextBox.setText(lastModifiedContentString);
		projectnameTextBox.setText(projectContentString);
		permissionsTextBox.setText(permissionsContentString);
		textgridUriTextBox.setText(textgridUriContentString);
		datacontributorTextBox.setText(dataContributorContentString);
		timecreatedTextBox.setText(createdContentString);
		xmlTextField.setText(this.textGridObject.serialize());
	}

}
