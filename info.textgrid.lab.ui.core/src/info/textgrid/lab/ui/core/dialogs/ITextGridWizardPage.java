/**
 * 
 */
package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.jface.wizard.IWizardPage;

/**
 * A wizard page for a wizard handling {@link TextGridObject}s.
 */
public interface ITextGridWizardPage extends IWizardPage {

	/**
	 * This method is called once for every page, before the page is added.
	 * 
	 * @param wizard
	 *            The wizard this page has been added to.
	 * @param preparator
	 *            The preparator class as defined in the extension point.
	 */
	public void init(ITextGridWizard wizard, INewObjectPreparator preparator);

	/**
	 * Using this method the page is asked to load the data / initialize itself
	 * from the given {@link TextGridObject}. It is usually called when the page
	 * is activated.
	 * 
	 * @param textGridObject
	 *            The {@link TextGridObject} for this page. If <code>null</code>
	 *            , the wizard page creates a new {@link TextGridObject}.
	 */
	public void loadObject(TextGridObject textGridObject);

	/**
	 * Finish whatever is needed to finish for this page.
	 * <p>
	 * Implementors should be aware that this may be called multiple times. In
	 * the default implementation, it will be called whenever the page is left
	 * for the next page or when the finish button is pressed on the current
	 * page.
	 * </p>
	 */
	public void finishPage();
	
}
