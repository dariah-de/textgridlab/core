package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.jface.wizard.IWizard;

public interface ITextGridWizard extends IWizard {

	/**
	 * Tries to set this wizard's current TextGridObject. Returns false if not
	 * applicable.
	 * 
	 * @param textGridObject
	 *            the new {@link TextGridObject} for the wizard.
	 * @return whether the wizard accepted the new value.
	 */
	public abstract boolean setTextGridObject(TextGridObject textGridObject);

	/**
	 * Returns the current {@link TextGridObject} handled by this wizard.
	 */
	public abstract TextGridObject getTextGridObject();

}