package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

/**
 * A dialog to display the warnings of the TGCrud-Response. 
 * 
 * @author Frank Queens for <a href="http://www.textgrid.de/">TextGrid</a> (fq)
 */
public class TGCrudWarning {
	
	private static TGCrudWarning instance = null;
	private static Boolean ToggleState = false;
	private MessageDialogWithToggle msg;
	private String DlgWarningMessage;
	private static final String TITLE = "TextGrid-CRUD Warning: ";
	private static final String TOGGLEMESSAGE = Messages.TGCrudWarning_UM_DoNotShowAgain;
	private static final String PLUGIN_ID = "info.textgrid.lab.core.efs.tgcrud"; //$NON-NLS-1$
	public static final String PREF_CRUDWARNING = "crudwarning"; //$NON-NLS-1$
	public static final String WARNINGINTRO = Messages.TGCrudWarning_WarningIntro;


	public static TGCrudWarning getInstance(List<Warning> warningMessages, String tgObjectTitle, String tgString)
	   {
			if (warningMessages.size() > 0) { 
				ToggleState = info.textgrid.lab.conf.ConfPlugin.getDefault().getPreferenceStore().getBoolean(PREF_CRUDWARNING);
				 if (instance == null) {
			       instance = new TGCrudWarning(warningMessages, tgObjectTitle, tgString);
			     } else {
			       instance.NewMessageDialogWithToggle(warningMessages, tgObjectTitle, tgString);	 
			     }
			     return instance;
			}
			return null;
	   }

	private void NewMessageDialogWithToggle(List<Warning> warningMessages, String tgObjectTitle, String tgString)
	{
		DlgWarningMessage = Joiner.on("\n").join(Iterables.transform(warningMessages, new Function<Warning, String>() {
			public String apply(Warning warning) {
				return warning.getValue();
			}
		}));
		//TODO: format warning messages for display
		this.msg = new MessageDialogWithToggle(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				TITLE + tgObjectTitle, 
                null, 
                WARNINGINTRO + "\n\nWarning: " + DlgWarningMessage,
                MessageDialog.WARNING, 
                new String[] {"Ok"}, 
                1, 
                TOGGLEMESSAGE, 
                !ToggleState);
		if (!DlgWarningMessage.equals("*INIT"))
		{	
			StatusManager.getManager().handle(new Status(IStatus.WARNING, PLUGIN_ID, "TextGrid-Object: " + tgString + "; Message: " + DlgWarningMessage));
		} 	
	}
	
	private TGCrudWarning(List<Warning> WarningMessages, String tgObjectTitle, String tgString) 
	{
		this.NewMessageDialogWithToggle(WarningMessages, tgObjectTitle, tgString);
		ToggleState = false;
	}
	
	public void OpenDialog() 
	{
		if (!this.msg.getToggleState())
		{	
			info.textgrid.lab.conf.ConfPlugin.getDefault().getPreferenceStore().setValue(PREF_CRUDWARNING, !this.msg.getToggleState());
			if (msg.open() != -1) 
		   	{	
		   		ToggleState = msg.getToggleState();
		   	}
		}
			
	}
}
