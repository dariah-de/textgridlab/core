/**
 * 
 */
package info.textgrid.lab.ui.core.dialogs;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.Activator;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.dialogs.IPageChangingListener;
import org.eclipse.jface.dialogs.PageChangingEvent;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * A Wizard to create new objects.
 * 
 * Clients may call {@link #setProject(TextGridProject)} to pre-select a
 * project.
 * 
 * @author tv
 */
public class NewObjectWizard extends Wizard implements ITextGridWizard,
		IPageChangingListener {

	class TypeSpecificAspects {

		class PageDescriptor {
			public ITextGridWizardPage getPage() {
				return page;
			}

			public String getTitle() {
				return title;
			}

			public boolean isRequired() {
				return required;
			}

			private ITextGridWizardPage page;
			private String title;
			private boolean required;

			public PageDescriptor(IConfigurationElement element)
					throws CoreException {
				page = (ITextGridWizardPage) element
						.createExecutableExtension("page");
				title = element.getAttribute("title");
				page.init(NewObjectWizard.this,
						TypeSpecificAspects.this.getPreparator());
				if (title != null && !"".equals(title))
					page.setTitle(title);

			}
		}

		private boolean showMetadataPage;
		private boolean needsValidMetadata;
		private INewObjectPreparator preparator;
		private List<PageDescriptor> pageDescriptors = new ArrayList<PageDescriptor>();
		private TGContentType contentType;
		private IContributor contributor;

		@Override
		public String toString() {
			return MessageFormat.format(
					"TypeSpecificAspects for {0} from {3}: {1}, {2}",
					getContentType(), getPreparator(), pageDescriptors,
					contributor);
		}

		public TypeSpecificAspects(IConfigurationElement configurationElement) {
			try {
				setContentType(TGContentType
						.getContentType(configurationElement
								.getAttribute("contentType")));
				setShowMetadataPage(!"false".equals(configurationElement
						.getAttribute("showMetadataPage")));
				setNeedsValidMetadata("true".equals(configurationElement
						.getAttribute("needsValidMetadata")));
				setPreparator((INewObjectPreparator) configurationElement
						.createExecutableExtension("preparator"));
				getPreparator().setWizard(NewObjectWizard.this);
				contributor = configurationElement.getContributor();

				IConfigurationElement[] pageElements = configurationElement
						.getChildren("wizardPage");

				for (IConfigurationElement element : pageElements) {
					try {
						PageDescriptor pageDescriptor = new PageDescriptor(
								element);
						pageDescriptors.add(pageDescriptor);
						addPage(pageDescriptor.getPage());
					} catch (CoreException e) {
						Activator
								.handleError(
										e,
										"Could not add a content type specific page to the new wizard.\nThe offending element is {0} in {1} with page implementation {2}. Please file a bug report.",
										element, element.getContributor()
												.getName(), element
												.getAttribute("page"));
					}
				}

			} catch (CoreException e) {
				IStatus status = Activator
						.handleError(e,
								"Could not prepare type specific parts of the New dialog.");
				if (dialog != null)
					dialog.setErrorMessage(status.getMessage());
			}
		}

		public void setShowMetadataPage(boolean showMetadataPage) {
			this.showMetadataPage = showMetadataPage;
		}

		public boolean isShowMetadataPage() {
			return showMetadataPage;
		}

		public void setNeedsValidMetadata(boolean needsValidMetadata) {
			this.needsValidMetadata = needsValidMetadata;
			metadataPage.setNeedsCompleteMetadata(needsValidMetadata);
		}

		public boolean isNeedsValidMetadata() {
			return needsValidMetadata;
		}

		public void setPreparator(INewObjectPreparator preparator) {
			this.preparator = preparator;
		}

		public INewObjectPreparator getPreparator() {
			return preparator;
		}

		public void setContentType(TGContentType contentType) {
			this.contentType = contentType;
		}

		public TGContentType getContentType() {
			return contentType;
		}
	}

	private TextGridProject currentProject = null;
	private TGContentType currentContentType = null;
	private TextGridObject textGridObject = null;
	private NewTGOPage newTGOPage;
	private MetadataPage metadataPage;
	private WizardDialog dialog;
	private TypeSpecificAspects typeSpecificAspects = null;

	@Override
	public void addPages() {
		super.addPages();

		setNeedsProgressMonitor(true);

		setWindowTitle(Messages.NewObjectWizard_WindowTitle);

		newTGOPage = new NewTGOPage(currentProject, currentContentType);
		newTGOPage.init(this, null);
		addPage(newTGOPage);
		metadataPage = new MetadataPage(Messages.NewObjectWizard_PageTitle);
		metadataPage.init(this, null);
		addPage(metadataPage);

		IWizardContainer container = getContainer();
		if (container instanceof WizardDialog) {
			dialog = (WizardDialog) container;
			dialog.addPageChangingListener(this);
		}
	}

	/**
	 * Opens XML Editor Perspective as default
	 * 
	 * @see defaultPerformFinish(String perspectiveID)
	 * @deprecated
	 * @return
	 */
	public boolean defaultPerformFinish() {
		return defaultPerformFinish("info.textgrid.lab.welcome.XMLEditorPerspective"); //$NON-NLS-1$
	}

	/**
	 * Tries to open a matching editor for the current {@link TextGridObject}.
	 * This implementation is used for new files which do not have a custom new
	 * dialog contributor.
	 * 
	 * @param perspectiveID
	 *            the perspective to be opened for the new object
	 * @return true iff an editor could be opened
	 */
	public boolean defaultPerformFinish(String perspectiveID) {

		if (getTextGridObject() == null)
			return false; // should not happen

		IFile file = (IFile) getTextGridObject().getAdapter(IFile.class);

		try {
			if (file != null) {

				IContentType eclipseContentType;
				eclipseContentType = getTextGridObject().getContentType(false)
						.getEclipseContentType();

				IEditorDescriptor editor = null;
				IEditorRegistry editorRegistry = PlatformUI.getWorkbench()
						.getEditorRegistry();
				if (eclipseContentType != null)
					editor = editorRegistry.getDefaultEditor(file.getName(),
							eclipseContentType);
				if (editor == null) {
					IContentDescription contentDescription = file
							.getContentDescription();
					if (contentDescription != null)
						editor = editorRegistry.getDefaultEditor(
								file.getName(),
								contentDescription.getContentType());
				}
				if (editor != null) {
					IWorkbenchPage page = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage();
					FileEditorInput input = new FileEditorInput(file);
					IEditorPart editorPart = page.openEditor(input,
							editor.getId());
					ITextEditor textEditor = getAdapter(editorPart,
							ITextEditor.class);
					if (textEditor != null) {
						IDocumentProvider documentProvider = textEditor
								.getDocumentProvider();
						IDocument document = documentProvider
								.getDocument(input);
						if (document != null)
							try {
								document.replace(0, 0, "");
							} catch (BadLocationException e) {
								StatusManager
										.getManager()
										.handle(new Status(
												IStatus.ERROR,
												Activator.PLUGIN_ID,
												NLS.bind(
														"Could not mark the document {0} as modified", //$NON-NLS-1$
														input), e));
							}
					}
					PlatformUI.getWorkbench().showPerspective(
							perspectiveID,
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow());
					return true;
				}

			} else
				return false;

		} catch (CoreException e) {
			Activator.handleError(e,
					Messages.NewObjectWizard_EM_CouldNotFindEditor,
					textGridObject);
			return false;
		}
		return false;
	}

	/**
	 * Set the project that should be pre-selected.
	 */
	public void setProject(TextGridProject project) {
		this.currentProject = project;
	}
	
	/**
	 * Set the content type that should be pre-selected.
	 */
	public void setContentType(TGContentType contentType) {
		this.currentContentType = contentType;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.lab.ui.core.dialogs.ITextGridWizard#setTextGridObject(info
	 * .textgrid.lab.core.model.TextGridObject)
	 */
	public boolean setTextGridObject(TextGridObject textGridObject) {
		TextGridObject oldTextGridObject = this.textGridObject;
		this.textGridObject = textGridObject;
		if (textGridObject != oldTextGridObject)
			metadataPage.loadObject(textGridObject);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.lab.ui.core.dialogs.ITextGridWizard#getTextGridObject()
	 */
	public TextGridObject getTextGridObject() {
		return textGridObject;
	}

	/**
	 * Displays the wizard.
	 * 
	 * @param activeShell
	 * @param project
	 */
	public void showDialog(Shell activeShell, TextGridProject project, TGContentType contentType) {
		setProject(project);
		setContentType(contentType);
		
		WizardDialog dialog = new WizardDialog(activeShell, this);
		dialog.setTitle(Messages.NewObjectWizard_DialogTitle);
		dialog.setBlockOnOpen(false);
		dialog.open();
	}

	public void handlePageChanging(PageChangingEvent event) {
		if (event.getTargetPage() == getNextPage((IWizardPage) event
				.getCurrentPage()))
			((ITextGridWizardPage) event.getCurrentPage()).finishPage();

		if (event.getCurrentPage() == newTGOPage && typeSpecificAspects == null) {
			// initialize the content type specific part of the dialogue
			initTypeSpecificAspects();
		}
	}

	protected void initTypeSpecificAspects() {
		IConfigurationElement[] elements = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(
						"info.textgrid.lab.ui.core.newObjectContributor"); //$NON-NLS-1$
		for (IConfigurationElement element : elements) {
			if (newTGOPage.getContentType().getId()
					.equals(element.getAttribute("contentType"))) {
				typeSpecificAspects = new TypeSpecificAspects(element);
				break;
			}
		}
	}

	@Override
	public boolean performFinish() {
		((ITextGridWizardPage) getContainer().getCurrentPage()).finishPage();

		try {
			if (textGridObject != null
					&& "text/tg.work+xml".equals(textGridObject
							.getContentTypeID())) {

				if (textGridObject.getMetadata().getWork() == null)
					textGridObject.setWorkMetadata(null);

				IFile file = (IFile) getTextGridObject()
						.getAdapter(IFile.class);
				file.setContents(
						new ByteArrayInputStream("work-object".getBytes("UTF-8")),
						IResource.FORCE, null);
			}

			if (typeSpecificAspects == null)
				initTypeSpecificAspects();

			if (typeSpecificAspects != null
					&& typeSpecificAspects.getPreparator() != null)
				return typeSpecificAspects.getPreparator().performFinish(
						textGridObject);
			else
				return defaultPerformFinish();

		} catch (CoreException e1) {
			Activator.handleError(e1, Messages.NewObjectWizard_EM_CouldNotMakePersistent);
		} catch (UnsupportedEncodingException e) {
			Activator.handleError(e, Messages.NewObjectWizard_EM_CouldNotMakePersistent);
		}

		return false;
	}

	public TypeSpecificAspects getTypeSpecificAspects() {
		if (typeSpecificAspects == null)
			initTypeSpecificAspects();

		return typeSpecificAspects;
	}

	@Override
	public boolean canFinish() {

		/*
		 * #22826 if we don't have an actual TGObject -> can't finish
		 */
		if (getTextGridObject() == null){
			return false;
			
		}

		/*
		 * #22826 if the actual TGObject has an empty title -> can't finish
		 */
		try {
			if (getTextGridObject().hasEmptyTitle()){
				return false;
			}
		} catch (CoreException e) {
			StatusManager.getManager().handle(
				new Status(
					IStatus.ERROR, Activator.PLUGIN_ID, 
					"Core exception for getTextGridObject().hasEmptyTitle()",
					e
				));
		}

		if (getContainer().getCurrentPage() == newTGOPage && typeSpecificAspects == null) {
			TGContentType contentType = newTGOPage.getContentType();
			if (contentType != null) {
				IConfigurationElement[] elements = Platform.getExtensionRegistry().getConfigurationElementsFor("info.textgrid.lab.ui.core.newObjectContributor"); //$NON-NLS-1$
				for (IConfigurationElement element : elements)
					if (element.getAttribute("contentType").equals(contentType.getId()))
						if ("false".equalsIgnoreCase(element.getAttribute("allowFastFinish")))
							return false;
			}
		}
		return super.canFinish();
	}

}
