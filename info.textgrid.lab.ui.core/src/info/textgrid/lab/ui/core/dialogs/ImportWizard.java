package info.textgrid.lab.ui.core.dialogs;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static info.textgrid.lab.ui.core.Activator.PLUGIN_ID;
import info.textgrid.lab.core.browserfix.TextGridLabBrowser;
import info.textgrid.lab.core.metadataeditor.MetaDataSection;
import info.textgrid.lab.core.metadataeditor.ScrolledPageArea;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.utils.ProjectCombo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckable;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

@Deprecated
public class ImportWizard extends Wizard {

	private static final String META_SUFFIX = ".meta";

	/**
	 * Copied from org.eclipse.jface.internal.text.revisions.RevisionPainter.
	 * HoverInformationControlCreator
	 */
	private static final String fgStyleSheet = "/* Font definitions */\n" + //$NON-NLS-1$
			"body, h1, h2, h3, h4, h5, h6, p, table, td, caption, th, ul, ol, dl, li, dd, dt {font-family: sans-serif; font-size: 9pt }\n"
			+ //$NON-NLS-1$ 
			"pre				{ font-family: monospace; font-size: 9pt }\n"
			+ //$NON-NLS-1$
			"\n"
			+ //$NON-NLS-1$
			"/* Margins */\n"
			+ //$NON-NLS-1$
			"body	     { overflow: auto; margin-top: 0; margin-bottom: 4; margin-left: 3; margin-right: 0 }\n"
			+ //$NON-NLS-1$ 
			"h1           { margin-top: 5; margin-bottom: 1 }	\n"
			+ //$NON-NLS-1$
			"h2           { margin-top: 25; margin-bottom: 3 }\n"
			+ //$NON-NLS-1$
			"h3           { margin-top: 20; margin-bottom: 3 }\n"
			+ //$NON-NLS-1$
			"h4           { margin-top: 20; margin-bottom: 3 }\n"
			+ //$NON-NLS-1$
			"h5           { margin-top: 0; margin-bottom: 0 }\n"
			+ //$NON-NLS-1$
			"p            { margin-top: 10px; margin-bottom: 10px }\n"
			+ //$NON-NLS-1$
			"pre	         { margin-left: 6 }\n"
			+ //$NON-NLS-1$
			"ul	         { margin-top: 0; margin-bottom: 10 }\n"
			+ //$NON-NLS-1$ 
			"li	         { margin-top: 0; margin-bottom: 0 } \n"
			+ //$NON-NLS-1$
			"li p	     { margin-top: 0; margin-bottom: 0 } \n"
			+ //$NON-NLS-1$
			"ol	         { margin-top: 0; margin-bottom: 10 }\n"
			+ //$NON-NLS-1$
			"dl	         { margin-top: 0; margin-bottom: 10 }\n"
			+ //$NON-NLS-1$
			"dt	         { margin-top: 0; margin-bottom: 0; font-weight: bold }\n"
			+ //$NON-NLS-1$ 
			"dd	         { margin-top: 0; margin-bottom: 0 }\n" + //$NON-NLS-1$
			"\n" + //$NON-NLS-1$
			"/* Styles and colors */\n" + //$NON-NLS-1$
			"a:link	     { color: #0000FF }\n" + //$NON-NLS-1$
			"a:hover	     { color: #000080 }\n" + //$NON-NLS-1$
			"a:visited    { text-decoration: underline }\n" + //$NON-NLS-1$
			"h4           { font-style: italic }\n" + //$NON-NLS-1$ 
			"strong	     { font-weight: bold }\n" + //$NON-NLS-1$
			"em	         { font-style: italic }\n" + //$NON-NLS-1$
			"var	         { font-style: italic }\n" + //$NON-NLS-1$
			"th	         { font-weight: bold }\n" + //$NON-NLS-1$
			"";

	protected String introHtml = "<html><head><title>Importing Files</title>"
			+ "<style type=\"text/css\">"
			+ fgStyleSheet
			+ "</style></head>"
			+ "<body><p>This wizard is intended to import a small set of files from your "
			+ "local filesystem to a single project in the TextGrid repository. To import,</p>"
			+ "<ol>"
			+ "<li>select the files to import using the Add button</li>"
			+ "<li>select the target project below</li>"
			+ "<li>on the next page, enter the neccessary metadata for each file</li>"
			+ "<li>click <em>Finish</em> to actually perform the insert.</li>"
			+ "</ol>"
			+ "<p>To import larger sets of data, please <a href=\"mailto:info@textgrid.de\">contact the TextGrid team</a>.";

	public ImportWizard(ISelection currentSelection) {
		if (currentSelection instanceof IStructuredSelection) {
			Object selected = ((IStructuredSelection) currentSelection)
					.getFirstElement();
			TextGridProject project = getAdapter(selected,
					TextGridProject.class);
			if (project == null) {
				TextGridObject object = getAdapter(selected,
						TextGridObject.class);
				if (object != null)
					try {
						project = object.getProjectInstance();
					} catch (CoreException e) {
						StatusManager.getManager().handle(e,
								Activator.PLUGIN_ID);
					}
			}
			this.project = project;
		}
	}

	@Override
	public void addPages() {
		super.addPages();

		filesPage = new SelectImportFilesPage("selectImportFiles");
		metadataPage = new EnterMetadataPage("enterMetadata");
		addPage(filesPage);
		addPage(metadataPage);
		setWindowTitle("Import files to the TextGridRep");
		setNeedsProgressMonitor(true);
	}

	SortedSet<File> fileSet = new TreeSet<File>();
	Map<File, TextGridObject> objectMap = new HashMap<File, TextGridObject>();
	TextGridProject project = null;

	private boolean useMetaFiles = true;

	private SelectImportFilesPage filesPage;

	private EnterMetadataPage metadataPage;

	public class SelectImportFilesPage extends WizardPage {

		@Override
		public void setVisible(boolean visible) {
			super.setVisible(visible);
			setTitle("Select files");
			setMessage("Select some local files to import and the target project.");
		}

		private Composite dialogArea;
		private ListViewer fileListViewer;
		private Button removeButton;
		private Button addButton;
		private ProjectCombo projectViewer;
		private Button useMetaFilesButton;

		protected SelectImportFilesPage(String pageName) {
			super(pageName);
		}

		public void createControl(Composite parent) {
			dialogArea = new Composite(parent, SWT.NONE);
			setControl(dialogArea);
			GridLayoutFactory.fillDefaults().applyTo(dialogArea);
			GridDataFactory.fillDefaults().grab(true, true).applyTo(dialogArea);

			createIntroduction();
			createFileList();
			createMetadataButton();
			createTargetProjectGroup();

			validatePage();
		}

		/**
		 * Creates an introduction to importing files.
		 */
		private void createIntroduction() {
			Browser intro = TextGridLabBrowser.createBrowser(dialogArea);
			intro.setText(introHtml);
			GridDataFactory.fillDefaults().grab(true, false).hint(300, 150)
					.applyTo(intro);
			intro.pack();
		}

		/**
		 * Creates the file list and the related controls (add button etc.)
		 */
		private void createFileList() {
			Composite listButtons = new Composite(dialogArea, SWT.NONE);

			addButton = new Button(listButtons, SWT.PUSH);
			addButton.setText("&Add files to import ...");

			removeButton = new Button(listButtons, SWT.PUSH);
			removeButton.setText("&Remove from file list");

			GridLayoutFactory.fillDefaults().numColumns(2).generateLayout(
					listButtons);

			fileListViewer = new ListViewer(dialogArea, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			GridDataFactory.fillDefaults().grab(true, true).applyTo(
					fileListViewer.getControl());

			fileListViewer.setContentProvider(new ArrayContentProvider());
			fileListViewer.setLabelProvider(new LabelProvider());

			addButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN
							| SWT.MULTI);
					fileDialog.setText("Import Files");
					if (fileDialog.open() != null) {
						File dir = new File(fileDialog.getFilterPath())
								.getAbsoluteFile();
						for (String fileName : fileDialog.getFileNames()) {
							File file = new File(dir, fileName);
							fileSet.add(file);
						}
					}
					fileListViewer.setInput(fileSet.toArray());
					validatePage();
				}
			});

			removeButton.addSelectionListener(new SelectionAdapter() {

				@SuppressWarnings("unchecked")
				@Override
				public void widgetSelected(SelectionEvent e) {
					IStructuredSelection selection = (IStructuredSelection) fileListViewer
							.getSelection();
					Iterator<File> iterator = selection.iterator();
					while (iterator.hasNext()) {
						File file = iterator.next();
						fileSet.remove(file);
					}
					fileListViewer.setInput(fileSet.toArray());
					validatePage();
				}
			});

			fileListViewer
					.addSelectionChangedListener(new ISelectionChangedListener() {

						public void selectionChanged(SelectionChangedEvent event) {
							removeButton.setEnabled(!event.getSelection()
									.isEmpty());
							validatePage();
						}

					});
		}

		/**
		 * Creates the Use .meta Files button
		 */
		private void createMetadataButton() {
			useMetaFilesButton = new Button(dialogArea, SWT.CHECK);
			useMetaFilesButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
					true, false));
			useMetaFilesButton
					.setText("Read .meta files for &metadata if available");
			useMetaFilesButton
					.setToolTipText("If checked, the import wizard will load descriptive metadata "
							+ "from a file named foo.meta or .foo.meta for each file foo, "
							+ "if such a file exists. "
							+ "The file must conform to the TextGrid metadata schema.");

			useMetaFilesButton.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					useMetaFiles = useMetaFilesButton.getSelection();
				}

			});

			useMetaFilesButton.setSelection(true);
		}

		/**
		 * Creates the target project selection controls
		 */
		private void createTargetProjectGroup() {
			new Label(dialogArea, SWT.NONE).setText("Target &project:");

			projectViewer = new ProjectCombo(dialogArea, LEVELS.EDITOR);
			projectViewer.setProject(project);
			GridDataFactory.fillDefaults().grab(true, false).applyTo(
					projectViewer.getControl());
			projectViewer
					.addSelectionChangedListener(new ISelectionChangedListener() {
						public void selectionChanged(SelectionChangedEvent event) {
							project = (TextGridProject) ((IStructuredSelection) event
									.getSelection()).getFirstElement();
							validatePage();
						}
					});
		}

		/**
		 * Validates the current page, setting error messages and the page's
		 * completion status
		 */
		protected void validatePage() {
			StringBuilder error = new StringBuilder();
			boolean complete = true;
			if (fileListViewer.getList().getItemCount() < 1) {
				error
						.append("Use the Add button to select some files to import. ");
				removeButton.setEnabled(false);
				complete = false;
			}

			if (project == null) {
				error.append("Select a project to import your files into. ");
				complete = false;
			}

			if (error.length() > 0)
				setErrorMessage(error.toString());
			else
				setErrorMessage(null);

			setPageComplete(complete);
		}

	}

	public class EnterMetadataPage extends WizardPage {

		private Composite mdPageComposite;
		private TableViewer filesViewer;
		private MetaDataSection metaDataSection;
		private Group metadataGroup;
		private Group fileViewerGroup;
		private ComboViewer contentTypeViewer;
		private ScrolledPageArea mdPageArea = null;
		private ScrolledForm mdPageScrolledForm = null;

		private File file = null;
		private TextGridObject object;
		private FocusAdapter focusListener;
		private KeyAdapter keyListener;
		private TableViewerColumn viewerColumn;

		protected class FileListListener implements ISelectionChangedListener,
				ICheckStateListener {

			/**
			 * When the file list's selection changes, we switch the metadata to
			 * the new object.
			 */
			public void selectionChanged(SelectionChangedEvent event) {
				File newFile = (File) ((IStructuredSelection) event
						.getSelection()).getFirstElement();
				if (newFile != file && object != null) {
					metaDataSection.updateTextGridObject();
				}
				file = newFile;

				if (file != null) {
					object = objectMap.get(file);
					if (object == null) {
						object = prepareObject(file);
						objectMap.put(file, object);
					}
					try {
						TGContentType contentType = object
								.getContentType(false);
						if (contentType != null)
							contentTypeViewer.setSelection(
									new StructuredSelection(contentType), true);
						metaDataSection.setNewTGObject(object, !metaDataSection
								.isProjectFileLoaded());
						metadataGroup.setText(NLS
								.bind("Metadata for {0}", file));
						addGlobalListeners(metadataGroup, focusListener,
								keyListener);
					} catch (CoreException e) {
						Activator
								.handleProblem(
										IStatus.WARNING,
										"Could not initialize object. This is probably a bug.",
										e);
					}
				}
			}

			public void checkStateChanged(CheckStateChangedEvent event) {
				TextGridObject textGridObject = objectMap.get(event
						.getElement());
				if (textGridObject == null)
					return;
				if (textGridObject.isSubmittable() != event.getChecked()) {
					ICheckable checkable = event.getCheckable();
					if (checkable == null)
						return;

					checkable.setChecked(event.getElement(), !event
							.getChecked());
					filesViewer.refresh(event.getElement());
				}
			}
		}

		/**
		 * This method is called on various event (mainly focus changes) and
		 * updates the validation status of various parts of this page.
		 */
		public void validatePage() {
			StringBuilder message = new StringBuilder();
			int type = NONE;
			if (file != null && object != null) {
				try {
					TGContentType contentType = getAdapter(
							((IStructuredSelection) contentTypeViewer
									.getSelection()).getFirstElement(),
							TGContentType.class);
					if (contentType != null
							&& contentType != object.getContentType(true))
						object.setContentType(contentType);
					metaDataSection.updateTextGridObject();
				} catch (CoreException e) {
					Activator
							.handleError(
									e,
									"An error occured while updating the metadata for {0}: {1}",
									object, e.getStatus());
				}
				if (object.isSubmittable()) {
					setErrorMessage(null);
					setPageComplete(true);
				} else {
					message
							.append(NLS
									.bind(
											"You need to complete {0}'s metadata before it can be imported. ",
											file));
					type = ERROR;
				}
				filesViewer.refresh(file);
			}
			
			int checked = getSubmittedObjects();
			int all = filesViewer.getTable().getItemCount();
			if (checked == 0) {
				type = ERROR;
				message.append("No files have valid metadata.");
				setPageComplete(false);
			} else if (checked < all) {
				if (type != ERROR)
					type = WARNING;
				message
						.append("If you click finish now, only those files that have complete metadata (checked) will be imported. ");
			} else {
				type = INFORMATION;
				message
						.append(NLS
								.bind(
										"Click Finish to import the files into the project {0} ",
										project));
				setPageComplete(true);
			}
			if (type == ERROR) {
				setErrorMessage(message.toString());
			} else {
				setMessage(message.toString(), type);
				setErrorMessage(null);
			}
		}

		protected EnterMetadataPage(String pageName) {
			super(pageName);
		}

		public class ImportLabelProvider extends LabelProvider implements
				ILightweightLabelDecorator {

			public void decorate(Object element, IDecoration decoration) {
				if (element instanceof File) {
					TextGridObject object = objectMap.get(element);

					if (object != null && !object.isSubmittable())
						decoration.addSuffix("incomplete");
				}
			}
			
			public Image getImage(Object element) {
				TextGridObject tgo = objectMap.get(element);
				if (tgo == null)
					return PlatformUI.getWorkbench().getSharedImages().getImage(
							ISharedImages.IMG_OBJS_ERROR_TSK);
				if (tgo.isSubmittable()) {
					AbstractUIPlugin plugin = Activator.getDefault();
					ImageRegistry imageRegistry = plugin.getImageRegistry();
					return imageRegistry.get("complete");
				} else 
					return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_ERROR_TSK);
			}

		}

		/**
		 * Adds the given listeners to all children
		 * 
		 * @param target
		 * @param focusListener
		 * @param keyListener
		 *            TODO
		 */
		protected void addGlobalListeners(Control target,
				FocusListener focusListener, KeyListener keyListener) {
			target.addFocusListener(focusListener);
			target.addKeyListener(keyListener);
			if (target instanceof Composite) {
				Composite composite = (Composite) target;
				for (Control child : composite.getChildren())
					addGlobalListeners(child, focusListener, keyListener);
			}
		}

		public void createControl(Composite parent) {
			mdPageArea = new ScrolledPageArea(parent);

			mdPageComposite = mdPageArea.getPageAreaComposite();
			mdPageScrolledForm = mdPageArea.getPageAreaScrolledForm();

			setControl(mdPageComposite);

			GridLayoutFactory.fillDefaults().numColumns(2).applyTo(
					mdPageComposite);

			fileViewerGroup = new Group(mdPageComposite, SWT.NONE);
			fileViewerGroup.setLayoutData(new GridData(SWT.NONE, SWT.FILL, false,
					false));
			fileViewerGroup.setLayout(new GridLayout(1, false));
			fileViewerGroup.setText("Metadata complete");

			final Table filesTable = new Table(fileViewerGroup, SWT.READ_ONLY | 
					SWT.V_SCROLL | SWT.H_SCROLL);
			filesViewer = new TableViewer(filesTable);	
			filesViewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
					true));

			viewerColumn = new TableViewerColumn(filesViewer, SWT.NONE);
			viewerColumn.getColumn().setWidth(400);

			GridData data = GridDataFactory.fillDefaults().grab(false, true)
					.create();
			filesViewer.getControl().setLayoutData(data);

			FileListListener fileListListener = new FileListListener();
			filesViewer.addSelectionChangedListener(fileListListener);
			filesViewer.setLabelProvider(new ImportLabelProvider());
			filesViewer.setContentProvider(new ArrayContentProvider());
			filesViewer.setInput(fileSet.toArray());

			metadataGroup = new Group(mdPageComposite, SWT.NONE);
			metadataGroup.setText("Metadata for the selected file");
			GridLayoutFactory.fillDefaults().applyTo(metadataGroup);
			GridDataFactory.fillDefaults().grab(false, false).applyTo(
					metadataGroup);
			Composite contentTypeArea = new Composite(metadataGroup, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(2).applyTo(
					contentTypeArea);

			Label contentTypeLabel = new Label(contentTypeArea, SWT.NONE);
			contentTypeLabel.setText("Content Type:");
			contentTypeViewer = new ComboViewer(contentTypeArea, SWT.READ_ONLY);
			contentTypeViewer.setLabelProvider(new LabelProvider() {

				@Override
				public Image getImage(Object element) {
					if (element instanceof TGContentType) {
						TGContentType contentType = (TGContentType) element;
						return contentType.getImage(false);
					}
					return super.getImage(element);
				}

			});
			contentTypeViewer.setContentProvider(new ArrayContentProvider());
			contentTypeViewer.setInput(TGContentType.getContentTypes(true));

			metaDataSection = new MetaDataSection(metadataGroup,
					mdPageScrolledForm, null);

			focusListener = new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					validatePage();
				}

			};
			keyListener = new KeyAdapter() {

				/**
				 * make PageUp and PageDown switch files in the file list
				 */
				@Override
				public void keyReleased(KeyEvent e) {
					int selectionIndex = filesTable.getSelectionIndex();
					if (e.keyCode == SWT.PAGE_UP) {
						if (selectionIndex > 0) {
							filesViewer.setSelection(new StructuredSelection(
									filesViewer
											.getElementAt(selectionIndex - 1)));
							e.doit = false;
						}
					} else if (e.keyCode == SWT.PAGE_DOWN) {
						if (selectionIndex < filesTable.getSelectionCount()) {
							filesViewer.setSelection(new StructuredSelection(
									filesViewer
											.getElementAt(selectionIndex + 1)));
							e.doit = false;
						}
					}
				}
			};
			addGlobalListeners(mdPageComposite, focusListener, keyListener);

			validatePage();
		}

		@Override
		public void setVisible(boolean visible) {

			if (visible == true) {
				filesViewer.setInput(fileSet.toArray());
				setTitle("Metadata");
				setMessage("Please enter the metadata for each file to import.");
				super.setVisible(visible);

				prepareMetadata();

				if (filesViewer.getSelection().isEmpty())
					if (!fileSet.isEmpty())
						filesViewer.setSelection(new StructuredSelection(
								fileSet.first()));
			}
		}

		private void prepareMetadata() {
			try {
				getContainer().run(true, false, new IRunnableWithProgress() {

					public void run(IProgressMonitor monitor)
							throws InvocationTargetException,
							InterruptedException {
						SubMonitor progress = SubMonitor.convert(monitor,
								"Preparing metadata", 10 * fileSet.size());
						for (final File file : fileSet) {
							if (!objectMap.containsKey(file)) {
								final TextGridObject object = prepareObject(file);
								objectMap.put(file, object);
								UIJob validateJob = new UIJob(
										"Setting checked status") {

									@Override
									public IStatus runInUIThread(
											IProgressMonitor monitor) {
										filesViewer.refresh();
										return Status.OK_STATUS;
									}
								};
								validateJob.setSystem(true);
								validateJob.schedule();
							}
							progress.worked(10);
						}

						new UIJob("Validating Page") {

							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								validatePage();
								return Status.OK_STATUS;
							}
						}.schedule();
					}

				});
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public int getSubmittedObjects() {
			int counter = 0;
			for(Map.Entry<File, TextGridObject> e : objectMap.entrySet()){
				  if (e.getValue().isSubmittable()) {
					  counter++;
				  }	  
				}
			return counter;
		}
	}

	@Override
	public boolean performFinish() {
		if (metadataPage == null)
			return false;

		IRunnableWithProgress importRunnable = new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				SubMonitor progress = SubMonitor.convert(monitor, fileSet
						.size() * 100);

				List<IStatus> failures = new LinkedList<IStatus>();

				for (File localFile : fileSet) {
					if (progress.isCanceled())
						throw new InterruptedException();

					progress.subTask(NLS.bind("Importing {0} ...", localFile));
					TextGridObject object = objectMap.get(localFile);
					if (object == null || !object.isSubmittable()) {
						failures
								.add(new Status(
										IStatus.WARNING,
										PLUGIN_ID,
										NLS
												.bind(
														"Skipped {0} due to incomplete metadata.",
														localFile)));
					} else {
						IFile gridFile = getAdapter(object, IFile.class);
						try {
							gridFile.setContents(
									new FileInputStream(localFile),
									IFile.FORCE, progress.newChild(100));
						} catch (FileNotFoundException e) {
							failures.add(new Status(IStatus.ERROR, PLUGIN_ID,
									NLS.bind("Local file {0} was not found.",
											localFile), e));
						} catch (CoreException e) {
							failures
									.add(new MultiStatus(
											PLUGIN_ID,
											0,
											new IStatus[] { e.getStatus() },
											NLS
													.bind(
															"Failed to import {0}: {1}",
															localFile,
															e
																	.getLocalizedMessage()),
											e));
						}
					}
				}

				if (!failures.isEmpty()) {
					MultiStatus result = new MultiStatus(
							PLUGIN_ID,
							0,
							failures.toArray(new IStatus[0]),
							NLS
									.bind(
											"{0} of {1} files could not be imported. See the details for the individual reasons.",
											failures.size(), fileSet.size()),
							null);
					Activator.getDefault().getLog().log(result);
					throw new InvocationTargetException(new CoreException(
							result));
				}
			}

		};

		try {
			getContainer().run(true, true, importRunnable);
			return true;
		} catch (InvocationTargetException e) {
			// Something failed. e will always contain a CoreException with a
			// meaningful status.
			if (e.getCause() instanceof CoreException) {
				ErrorDialog.openError(getShell(),
						"Some files could not be imported.", null,
						((CoreException) e.getCause()).getStatus());
				return true;
			} else
				Activator
						.handleError(
								e.getCause(),
								"An {0} occurred while importing files to the TextGridRep: {1}.\nThis comes somewhat unexpected, please file a bug report.",
								e.getCause(), e.getLocalizedMessage());
		} catch (InterruptedException e) {
			// Canceled.
		}
		return false;
	}

	private TextGridObject prepareObject(File file) {
		// Try to determine the content type by file extension
		// ...
		String fileName = file.getName();
		int i = fileName.lastIndexOf('.');
		TGContentType contentType = null;
		if (i != -1)
			contentType = TGContentType.findByExtension(fileName
					.substring(i + 1));
		if (contentType == null)
			contentType = TGContentType
					.findMatchingContentType(fileName.substring(i + 1));
		if (contentType == null)
			contentType = TGContentType
					.getContentType(TGContentType.UNKNOWN_ID);

		TextGridObject object = TextGridObject.getNewObjectInstance(project,
				contentType);

		if (useMetaFiles) {
			File metaFile = new File(file.getParent(), file.getName()
					+ META_SUFFIX);
			if (!metaFile.canRead())
				metaFile = new File(file.getParent(), "." + file.getName()
						+ META_SUFFIX);

			if (metaFile.canRead()) {

				try {
					object.loadMetadata(new FileInputStream(metaFile));
					objectMap.put(file, object);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return object;
	}
	
	

	public static void showWizard(Shell activeShell, ISelection currentSelection) {
		WizardDialog dialog = new WizardDialog(activeShell, new ImportWizard(
				currentSelection));
		dialog.open();
	}

}
