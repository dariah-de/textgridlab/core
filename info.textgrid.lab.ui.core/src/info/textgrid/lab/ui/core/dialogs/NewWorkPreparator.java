package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.metadataeditor.MetaDataView;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

public class NewWorkPreparator implements INewObjectPreparator {

	private ITextGridWizard wizard;

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;
	}

	public void initializeObject(TextGridObject textGridObject) {
		// TODO Auto-generated method stub

	}

	public boolean performFinish(TextGridObject textGridObject) {
		MetaDataView metadataView;
		try {
			metadataView = (MetaDataView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(
					MetaDataView.ID);
			metadataView.setMetadataInView(textGridObject, true);
			return true;
		} catch (PartInitException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			if (wizard.getContainer() instanceof TitleAreaDialog)
				((TitleAreaDialog) wizard).setErrorMessage(e.getMessage());
			return false;
		}
	}

}
