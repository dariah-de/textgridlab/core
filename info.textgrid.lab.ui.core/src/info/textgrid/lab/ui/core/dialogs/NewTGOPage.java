package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.Messages;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;
import info.textgrid.lab.ui.core.utils.UpdatingDeferredListContentProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.progress.UIJob;

import com.google.common.base.Predicate;

/**
 * The first and generic page of the New TextGrid Object Wizard. Asks the user
 * to provide project and content type.
 * 
 * @author tv
 */
public class NewTGOPage extends WizardPage implements
		ISelectionChangedListener, ITextGridWizardPage {
	
	private TextGridObject textGridObject = null;
	@Deprecated private static final String CT_TIFF = "image/tiff"; //$NON-NLS-1$
	@Deprecated private static final String CT_JPG = "image/jpeg"; //$NON-NLS-1$

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (!visible)
			tryInitializeTGO();
	}
	
	public TextGridObject getTextGridObject() {
		if (textGridObject == null && isPageComplete())
			tryInitializeTGO();
		return textGridObject;
	}

	private void tryInitializeTGO() {
		if (textGridObject == null) {
			textGridObject = createTextGridObject();
			
		}
	}

	/**
	 * Returns the project selected by the user (after the page has been
	 * completed).
	 */
	public TextGridProject getProject() {
		return project;
	}

	/**
	 * Returns the content type selected by the user (after the page has been
	 * completed).
	 */
	public TGContentType getContentType() {
		return contentType;
	}

	private ComboViewer typeViewer;
	private TableViewer projectViewer;
	private TextGridProject currentProject;
	private TGContentType currentContentType;
	private TextGridProject project;
	private TGContentType contentType;
	private Label projectDescLabel;
	private NewObjectWizard wizard;

	protected NewTGOPage() {
		this(null, null);
	}

	/**
	 * Creates a new wizard page to select a new dialog.
	 * 
	 * @param project
	 *            if not null, this project will be preselected.
	 * @see NewObjectWizard
	 */
	public NewTGOPage(TextGridProject project, TGContentType contentType) {
		super(Messages.NewTGOPage_pageTitle, Messages.NewTGOPage_pageDescription, null);
		this.currentProject = project;
		this.currentContentType = contentType;
	}
	
	
	// ========== No interesting API beyond this point =================

	public void createControl(Composite parent) {
		
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));
		setControl(control);

		Label intro = new Label(control, SWT.WRAP);
		intro.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		intro
				.setText(Messages.NewTGOPage_UI_Intro);
		
		
		
		setMessage(Messages.NewTGOPage_metadataHint);

		createProjectGroup(control);
		createTypeGroup(control);
	}

	private void createProjectGroup(Composite control) {
		Group projectGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		projectGroup
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		projectGroup.setLayout(new GridLayout(1, true));
		projectGroup.setText(Messages.NewTGOPage_project);

		//TG-1943
		Link link = new Link(projectGroup, SWT.NONE);
		link
		.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
		false));
	    String message = "<a>"+Messages.NewTGOPage_Label_createNewProjectLink+"</a>";
	    link.setText(message);
//	    link.setSize(400, 100);
	    link.addSelectionListener((new SelectionAdapter(){
	        @Override
	        public void widgetSelected(SelectionEvent e) {
//	               System.out.println("You have selected: "+e.text);
	             //execute command
					try {
						PlatformUI.getWorkbench().showPerspective("info.textgrid.lab.welcome.AdministrationPerspective", PlatformUI.getWorkbench().getActiveWorkbenchWindow());
					} catch (WorkbenchException e1) {
						Activator.handleError(e1,info.textgrid.lab.ui.core.dialogs.Messages.NewTGOPage_create_new_project_Error);
					}
					IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					IViewPart view = window.getActivePage().findView("info.textgrid.lab.projectadmin.views.UserManagement");
					IHandlerService service = (IHandlerService) view.getSite().getService(IHandlerService.class);
					try {
						service.executeCommand("info.textgrid.lab.projectadmin.createProject", null);
						
						Job job = new Job("...Timer Job") {
							boolean timer = true;
							@SuppressWarnings("static-access")
							@Override
							protected IStatus run(IProgressMonitor monitor) {
							    PlatformUI.getWorkbench().getDisplay().getDefault().asyncExec(new Runnable() {
							        @Override
							        public void run() {
							            while(timer)
							            {
							                try {
							                	//TODO
												Thread.sleep(500);
												projectViewer.refresh();
												timer = false;
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												Activator.handleError(e,info.textgrid.lab.ui.core.dialogs.Messages.NewTGOPage_create_new_project_Error);
											}
							                
							            }
							        }
							    });
							    return Status.OK_STATUS;
							}
							};
							job.schedule();
							
					} catch (ExecutionException e2) {
						// TODO Auto-generated catch block
						Activator.handleError(e2,info.textgrid.lab.ui.core.dialogs.Messages.NewTGOPage_create_new_project_Error);
					} catch (NotDefinedException e2) {
						// TODO Auto-generated catch block
						Activator.handleError(e2,info.textgrid.lab.ui.core.dialogs.Messages.NewTGOPage_create_new_project_Error);
					} catch (NotEnabledException e2) {
						// TODO Auto-generated catch block
						Activator.handleError(e2,info.textgrid.lab.ui.core.dialogs.Messages.NewTGOPage_create_new_project_Error);
					} catch (NotHandledException e2) {
						// TODO Auto-generated catch block
						Activator.handleError(e2,info.textgrid.lab.ui.core.dialogs.Messages.NewTGOPage_create_new_project_Error);
					}
	        }
	    }));
		
		
//		XXX re-enable when DAASI has added the Editor role to new projects automatically
//		newProjectLink
//				.setText("You may also <a href=\"#createProject\">create a new project</a> first.");
//		newProjectLink.addSelectionListener(new SelectionAdapter() {
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				super.widgetSelected(e);
//				if ("#createProject".equals(e.text)) {
//					PlatformUtils
//							.executeCommand("info.textgrid.lab.projectadmin.createProject");
//				}
//			}
//		});

		
		projectViewer = new TableViewer(projectGroup, SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		GridData projectTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
		projectTreeData.heightHint = 200;
		projectViewer.getTable().setLayoutData(projectTreeData);
		projectViewer.setLabelProvider(new TGODefaultLabelProvider(true));

		final UpdatingDeferredListContentProvider contentProvider = new UpdatingDeferredListContentProvider();
		projectViewer.setContentProvider(contentProvider);
		projectViewer.setSorter(new ViewerSorter());
		projectViewer.setInput(TextGridProjectRoot.getInstance(TextGridProjectRoot.LEVELS.EDITOR)); 
		projectViewer.addSelectionChangedListener(this);


		if (currentProject != null) {
			contentProvider.addDoneListener(new IDoneListener() {
				public void loadDone(Viewer viewer) {
					projectViewer.setSelection(new StructuredSelection(
							currentProject), true);
					contentProvider.removeDoneListener(this);
				}
			});
		}
		
		projectDescLabel = new Label(projectGroup, SWT.WRAP);
		GridData projectDescData = new GridData(SWT.FILL, SWT.FILL, true, false);

		projectDescData.heightHint = 48;
		projectDescLabel.setLayoutData(projectDescData);

	}
	
	private void createTypeGroup(Composite control) {
		Group typeGroup = new Group(control, SWT.NONE);
		typeGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		typeGroup.setLayout(new GridLayout(1, false));
		typeGroup.setText(Messages.NewTGOPage_type);

		Combo typeControl = new Combo(typeGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		typeControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false));

		typeViewer = new ComboViewer(typeControl);
		typeViewer.addSelectionChangedListener(this);
		typeViewer.setContentProvider(new ArrayContentProvider());
		typeViewer.setLabelProvider(new LabelProvider() {

			@Override
			public Image getImage(Object element) {
				if (element instanceof TGContentType) {
					TGContentType contentType = (TGContentType) element;
					return contentType.getImage(false);
				}
				return super.getImage(element);
			}

		});
		typeViewer.setInput(TGContentType.getContentTypes(new Predicate<TGContentType>() {
			public boolean apply(TGContentType input) {
				return input.isCreatable() && !input.isInternal();
			}
		}));
		
		typeViewer.setSorter(new ViewerSorter());
		
		// Set default. Maybe try dialog settings here?
		TGContentType defaultContentType;
		if (currentContentType != null)
			defaultContentType = currentContentType;
		else 
			defaultContentType = TGContentType.getContentType("text/xml"); //$NON-NLS-1$
		
		if (defaultContentType == null
				&& TGContentType.getContentTypes(true).length > 0)
			defaultContentType = TGContentType.getContentTypes(true)[0];

		if (defaultContentType != null)
			typeViewer
					.setSelection(new StructuredSelection(defaultContentType));
		// else no content types have been confgured, i.e. something went
		// utterly wrong.
	}
	

	public void selectionChanged(SelectionChangedEvent event) {
		if (event.getSelectionProvider() == projectViewer) {
			Object selectedObject = extractSelectedObject(event);
			if (selectedObject instanceof TextGridProject) {
				project = (TextGridProject) selectedObject;
				projectDescLabel.setText(NLS.bind("{0} ({1})", project
						.getDescription(), project.getId()));
				setMessage(Messages.NewTGOPage_metadataHint);
				checkCreatePermission(project);
			} else {
				project = null;
				projectDescLabel.setText("");
			}
		} else if (event.getSelectionProvider() == typeViewer)
			contentType = (TGContentType) extractSelectedObject(event);
		
		StringBuilder msg = new StringBuilder();
		boolean complete = true;
		if (project == null) {
			complete = false;
			msg.append(Messages.NewTGOPage_selectProjectError);
		}
		if (contentType == null) {
			complete = false;
			msg.append(Messages.NewTGOPage_selectTypeError);
		}
		
		if (complete) {
			setErrorMessage(null);
		}
		else
			setErrorMessage(msg.toString());
		
		setPageComplete(complete);
	}

	
	private Job checkPermissionJob = null;
	protected void checkCreatePermission(final TextGridProject project) {
		if (checkPermissionJob != null)
			checkPermissionJob.cancel();

		checkPermissionJob = new Job(MessageFormat.format(
				Messages.NewTGOPage_UI_CheckPermission, project)) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				
				try {
					String warning = null;
					if (!project.hasPermissions(ITextGridPermission.CREATE))
						if (project
								.hasPermissions(ITextGridPermission.DELEGATE))
							warning = MessageFormat
									.format(
											Messages.NewTGOPage_UI_DelegateWarning,
											project);
						else
							warning = MessageFormat
									.format(
											Messages.NewTGOPage_UI_ElseWarning,
											project);
					
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;

					if (warning != null) {
						final String message = warning;
						new UIJob(Messages.NewTGOPage_JobName) {

							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								setMessage(message, WARNING);
								return Status.OK_STATUS;
							}

						}.schedule();
					}
				} catch (CoreException e) {
					return e.getStatus();
				}
				return Status.OK_STATUS;
			}
		};
		checkPermissionJob.setSystem(true);
		checkPermissionJob.schedule();
	}

	protected static Object extractSelectedObject(SelectionChangedEvent event) {
		if (event.getSelection() instanceof IStructuredSelection)
			return ((IStructuredSelection) event.getSelection()).getFirstElement();
		return null;
	}

	public void finishPage() {
		if (wizard.getTextGridObject() == null) {
			wizard.setTextGridObject(createTextGridObject());
			typeViewer.getControl().setEnabled(false);
			projectViewer.getControl().setEnabled(false);
			setMessage(Messages.NewTGOPage_UI_ObjectCreated);
		} 
	}

	private TextGridObject createTextGridObject() {
		TextGridObject object = TextGridObject.getNewObjectInstance(getProject(), getContentType());
		if (wizard.getTypeSpecificAspects() != null) {
			INewObjectPreparator preparator = wizard.getTypeSpecificAspects()
					.getPreparator();
			if (preparator != null)
				preparator.initializeObject(object);
		}
		return object;
	}

	public void init(ITextGridWizard wizard, INewObjectPreparator preparator) {
		if (wizard instanceof NewObjectWizard)
			this.wizard = (NewObjectWizard) wizard;
		else
			throw new IllegalArgumentException("NewTGOPage may only be used with the NewObjectWizard.");

	}

	public void loadObject(TextGridObject textGridObject) {
		// Currently, we ignore this; we don't support modifying existing
		// objects
	}
}
