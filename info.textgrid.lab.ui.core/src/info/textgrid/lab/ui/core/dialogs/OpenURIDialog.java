package info.textgrid.lab.ui.core.dialogs;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.handlers.ExportHandler;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class OpenURIDialog extends TitleAreaDialog {

	private static final int EXPORT = 101;
	private Text text;
	private URI uri;
	private boolean valid;
	private String input = "";

	/**
	 * returns the value currently or initially shown in the input text field.
	 * 
	 * @see #setInput(String)
	 * @see #getURI()
	 */
	public String getInput() {
		return input;
	}

	/**
	 * Sets the initial content for the input
	 */
	public void setInput(final String input) {
		this.input = input;
		if (text != null && !text.isDisposed())
			text.setText(input);
	}

	/** true if the input field contains a valid URI */
	public boolean isValid() {
		return valid;
	}

	public URI getURI() {
		return uri;
	}


	public OpenURIDialog(Shell parentShell) {
		super(parentShell);
	}

	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		Label uriLabel = new Label(area, SWT.NONE);
		uriLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		uriLabel.setText("&URI:"); //$NON-NLS-1$

		text = new Text(area, SWT.SINGLE | SWT.LEAD | SWT.BORDER | SWT.SEARCH);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		text.setText(input);
		text.setMessage(Messages.OpenURIDialog_IM_EnterOrPaste);
		text.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				validate();
			}
		});
		text.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				validate();
			}
		});

		getShell().setText(Messages.OpenURIDialog_IM_OpenURI);
		setTitle(Messages.OpenURIDialog_IM_OpenTGURI);
		setMessage(Messages.OpenURIDialog_IM_EnterOrPasteInField);

		return area;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		getButton(OK).setText(Messages.OpenURIDialog_LabelOpen);
		getButton(OK).setToolTipText(Messages.OpenURIDialog_IM_OpenURIDefault);
		createButton(parent, EXPORT, "E&xport ...", false);
		getButton(OK).setToolTipText(Messages.OpenURIDialog_IM_ExportURI);
	}

	protected void validate() {
		if (text != null && !text.isDisposed()) {
			boolean valid = false;
			String userInput = text.getText().trim();
			try {
				URI uri = new URI(userInput);
				if ("textgrid".equalsIgnoreCase(uri.getScheme())) {
					valid = true;
				} else if ("hdl".equalsIgnoreCase(uri.getScheme())) {	//#10872
					valid = true;
				} else {
					setErrorMessage(NLS.bind(Messages.OpenURIDialog_EM_Scheme, uri.getScheme()));
				}

			} catch (URISyntaxException e) {
				setErrorMessage(NLS.bind(Messages.OpenURIDialog_EM_NotValid, userInput, e.getLocalizedMessage()));
			}
			if (valid)
				setErrorMessage(null);

			this.valid = valid;
			if (getButton(OK) != null)
				getButton(OK).setEnabled(valid);
			if (getButton(EXPORT) != null)
				getButton(EXPORT).setEnabled(valid);
		}
			
	}

	protected void okPressed() {
		try {
			uri = new URI(text.getText());
			super.okPressed();
		} catch (URISyntaxException e) {
			setErrorMessage(NLS.bind(Messages.OpenURIDialog_EM_NotValid, text.getText(), e.getLocalizedMessage()));
		}
	}

	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == EXPORT) {
			try {
				uri = new URI(text.getText());
				TextGridObject object = TextGridObject.getInstanceOffline(uri);
				ExportHandler.exportObjects(getShell(), object);
			} catch (URISyntaxException e) {
				setErrorMessage(NLS.bind(Messages.OpenURIDialog_EM_NotValid, text.getText(), e.getLocalizedMessage()));
			}
		}
	}
}
