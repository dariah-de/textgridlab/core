package info.textgrid.lab.ui.core;

import java.net.URL;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.ui.core"; //$NON-NLS-1$
	public static final String COMPLETE_IMAGE_ID = "complete"; //$NON-NLS-1$
	public static final String LOADING_IMAGE_ID = "loading"; //$NON-NLS-1$
	public static final String RESTRICTED_IMAGE_ID = "restricted_object"; //$NON-NLS-1$
	public static final String FORBIDDEN_IMAGE_ID = "forbidden_object"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

		// Jobs that fail don't always produce a log file message. We're forcing
		// one here ...
		Job.getJobManager().addJobChangeListener(new JobChangeAdapter() {
			@Override
			public void done(IJobChangeEvent event) {
				IStatus result = event.getResult();
				if ((result.getSeverity() & (IStatus.ERROR | IStatus.WARNING)) != 0) {
					MultiStatus status = new MultiStatus(PLUGIN_ID, 0, new IStatus[] { result }, NLS.bind(
							Messages.Activator_EM_JobError, event.getJob(),
							result.getMessage()), null);
					StatusManager.getManager().handle(status, StatusManager.LOG);
				}
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Handles a problem by writing it to the log file and maybe showing a
	 * dialog.
	 * 
	 * @param severity
	 *            one of {@link IStatus#ERROR}, {@link IStatus#WARNING,
	 *            IStatus#INFO, IStatus#OK}
	 * @param message
	 *            the message to log or <code>null</code>
	 * @param cause
	 *            the exception that caused the problem or <code>null</code>.
	 */
	public static IStatus handleProblem(int severity, String message,
			Throwable cause) {
		if ((message == null || message.length() < 1))
			if (cause != null)
				message = cause.getLocalizedMessage();

		IStatus status = new Status(severity, PLUGIN_ID, message, cause);
		StatusManager.getManager().handle(
				status,
				severity == IStatus.ERROR ? StatusManager.LOG
						| StatusManager.SHOW : StatusManager.LOG);
		return status;
	}

	public static void handleError(PartInitException e, String string,
			IFile file) {
		// TODO Auto-generated method stub

	}

	public IStatus handleProblem(Throwable e) {
		return handleProblem(IStatus.ERROR, e, null);
	}

	/**
	 * Cares for the given error. The <var>message</var> and the <var>args</var>
	 * are passed to {@link NLS#bind(String, Object[])} and the complete
	 * argument list to {@link #handleProblem(int, String, Throwable)}.
	 * 
	 * @param cause
	 *            The exception that caused the problem (or null)
	 * @param message
	 *            The message, use {0}, {1} ... for arguments
	 * @param args
	 *            Exactly as many arguments as referenced in the message
	 * @return the {@link IStatus} object created while handling the error
	 */
	public static IStatus handleError(Throwable cause, String message,
			Object... args) {
		return handleProblem(IStatus.ERROR, cause, message, args);
	}

	/**
	 * Cares for the given problem. The <var>message</var> and the
	 * <var>args</var> are passed to {@link NLS#bind(String, Object[])} and the
	 * complete argument list to {@link #handleProblem(int, String, Throwable)}.
	 * 
	 * @param severity
	 *            One of the severity constants defined in {@link IStatus}.
	 *            Currently, IStatus.ERROR will pop up an error dialogue.
	 * @param cause
	 *            The exception that caused the problem (or null)
	 * @param message
	 *            The message, use {0}, {1} ... for arguments
	 * @param args
	 *            Exactly as many arguments as referenced in the message
	 * @return the {@link IStatus} object created while handling the error
	 */
	public static IStatus handleProblem(int severity, Throwable cause,
			String message, Object... args) {
		return Activator
				.handleProblem(severity,
				NLS.bind(message, args), cause);
	}
	
	protected void initializeImageRegistry(ImageRegistry registry) {
		// Bundle bundle = Platform.getBundle(PLUGIN_ID);
		// IPath path = new Path("icons/complete_status.gif");
		// URL url = Platform.find(bundle, path);
		// ImageDescriptor desc = ImageDescriptor.createFromURL(url);
		// registry.put(COMPLETE_IMAGE_ID, desc);

		registerImage(registry, COMPLETE_IMAGE_ID, "icons/complete_status.gif"); //$NON-NLS-1$
		registerImage(registry, LOADING_IMAGE_ID, "icons/loading.png"); //$NON-NLS-1$
		registerImage(registry, RESTRICTED_IMAGE_ID, "icons/restricted_object.gif"); //$NON-NLS-1$
		registerImage(registry, FORBIDDEN_IMAGE_ID, "icons/forbidden.gif"); //$NON-NLS-1$
    }

	/**
	 * Registers the image in the image registry.
	 * 
	 * @param registry
	 *            the image registry to use
	 * @param key
	 *            the key by which the image will be adressable
	 * @param fullPath
	 *            the full path to the image, relative to the plugin directory.
	 */
	private void registerImage(ImageRegistry registry, String key, String fullPath) {
		URL imageURL = FileLocator.find(getBundle(), new Path(fullPath), null);
		registry.put(key, ImageDescriptor.createFromURL(imageURL));
	}
}
