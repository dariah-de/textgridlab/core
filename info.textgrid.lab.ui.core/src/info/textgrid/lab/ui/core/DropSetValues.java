package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.model.TextGridProject;

/**
 * Static help class to buffer the list of items,
 * which has been moved via drag & drop into the 
 * navigator. Probably there exists a better way
 * to manage this.
 */
public final class DropSetValues {
	
	private static String[] values = null;
	private static TextGridProject project = null;
	
	public final static void setValues(String[] stringValues) {
		values = stringValues;
	}
	
	public final static String[] getValues() {
		return values;
	}

	public static TextGridProject getProject() {
		return project;
	}

	public static void setProject(TextGridProject project) {
		DropSetValues.project = project;
	}
}

