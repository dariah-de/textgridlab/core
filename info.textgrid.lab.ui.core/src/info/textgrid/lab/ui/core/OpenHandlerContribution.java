package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.model.TGContentType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;

/**
 * Represents a contribution to the extension point
 * info.textgrid.lab.ui.core.openObject
 */
public class OpenHandlerContribution {

	private static final String EXTENSION_POINT_OPEN = "info.textgrid.lab.ui.core.openObject"; //$NON-NLS-1$
	private final TGContentType contentType;
	private final String perspective;
	private final String editor;
	private final String label;
	private final String description;
	private final IOpenHandler handler;
	private final int priority;

	public static final Comparator<OpenHandlerContribution> PRIORITY_COMPARATOR = new Comparator<OpenHandlerContribution>() {
		public int compare(final OpenHandlerContribution o1,
				final OpenHandlerContribution o2) {
			return o2.getPriority() - o1.getPriority();
		}
	};

	private OpenHandlerContribution(IConfigurationElement openHandlerElement,
			TGContentType contentType) {
		this.contentType = contentType; // do we need this?
		perspective = openHandlerElement.getAttribute("perspective"); //$NON-NLS-1$
		editor = openHandlerElement.getAttribute("editor"); //$NON-NLS-1$
		label = openHandlerElement.getAttribute("label"); //$NON-NLS-1$
		description = openHandlerElement.getAttribute("description"); //$NON-NLS-1$
		priority = Integer.valueOf(openHandlerElement.getAttribute("priority")); //$NON-NLS-1$
		IOpenHandler handler = null;
		if (openHandlerElement.getAttribute("handler") != null) //$NON-NLS-1$
			try {
				handler = (IOpenHandler) openHandlerElement
						.createExecutableExtension("handler"); //$NON-NLS-1$
				handler.setContribution(this);
			} catch (CoreException e) {
				Activator
						.handleProblem(
								IStatus.WARNING,
								"Could not instantiate handler for an open contribution",
								e);
			}
		this.handler = handler;
	}

	/**
	 * Return a list of open handler contributions for the given contentType.
	 * 
	 * @param contentType
	 *            The content type for which a list of open handlers should be
	 *            returned.
	 * @return list of open handler contributions, in no particular order.
	 */
	public static List<OpenHandlerContribution> contributionsFor(
			final TGContentType contentType) {
		List<OpenHandlerContribution> result = new ArrayList<OpenHandlerContribution>();
		IConfigurationElement[] configurationElementsFor = Platform
				.getExtensionRegistry().getConfigurationElementsFor(
						EXTENSION_POINT_OPEN);
		for (IConfigurationElement openHandlerElement : configurationElementsFor)
			for (IConfigurationElement contentTypeElement : openHandlerElement
					.getChildren("contentType")) //$NON-NLS-1$
				if (contentType.getId().equals(
						contentTypeElement.getAttribute("id"))) //$NON-NLS-1$
					result.add(OpenHandlerContribution.of(openHandlerElement,
							contentType));
		Collections.sort(result, PRIORITY_COMPARATOR);

		return result;
	}

	/**
	 * Returns the best contribution for the given content type (i.e. the one
	 * with the highest priority)
	 * 
	 * @param contentType
	 * @return
	 */
	public static OpenHandlerContribution bestContributionFor(
			final TGContentType contentType) {
		List<OpenHandlerContribution> contributions = contributionsFor(contentType);
		if (contributions.size() == 0)
			return null;
		else
			return contributions.get(0);
	}

	protected static OpenHandlerContribution of(
			IConfigurationElement openHandlerElement, TGContentType contentType) {
		return new OpenHandlerContribution(openHandlerElement, contentType);
	}

	public TGContentType getContentType() {
		return contentType;
	}

	public String getPerspective() {
		return perspective;
	}

	public String getEditor() {
		return editor;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}

	public IOpenHandler getHandler() {
		return handler;
	}

	public int getPriority() {
		return priority;
	}

}
