package info.textgrid.lab.ui.core.menus;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.util.Date;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;

public class TGOpenWithMenuContributionItem extends ContributionItem {
	
	public TGOpenWithMenuContributionItem() {
	}
 
	public TGOpenWithMenuContributionItem(String id) {
		super(id);
	}
 
	@Override
	public void fill(Menu menu, int index) {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		ISelection selection = window.getActivePage().getSelection();

//		IFile iSSRFile = AdapterUtils.getAdapter(firstSSRElement, IFile.class);
//		if (iSSRFile != null) {
//			openWithSubMenu.add(new TGOpenWithMenu(getSite().getPage(), iSSRFile));
//			// Other plug-ins can contribute there actions here
//			manager.add(openWithSubMenu);
//		}
		
		
		//Here you could get selection and decide what to do
		//You can also simply return if you do not want to show a menu
 
		//create the menu item
		MenuItem menuItem = new MenuItem(menu, SWT.CHECK, index);
		menuItem.setText("My menu item (" + new Date() + ")");
		menuItem.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.err.println("Dynamic menu selected");
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}


}
