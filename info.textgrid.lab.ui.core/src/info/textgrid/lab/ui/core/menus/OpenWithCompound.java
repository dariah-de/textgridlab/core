package info.textgrid.lab.ui.core.menus;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.AdapterUtils.AdapterNotFoundException;
import info.textgrid.lab.ui.core.OpenHandlerContribution;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;

@Deprecated
public class OpenWithCompound extends CompoundContributionItem {

	public OpenWithCompound() {
		// TODO Auto-generated constructor stub
	}

	public OpenWithCompound(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected IContributionItem[] getContributionItems() {
		
		List<IContributionItem> contributionItems = new ArrayList<IContributionItem>();

		try {
			ISelection selection = getSelection();
			if (selection instanceof IStructuredSelection) {
				if (((IStructuredSelection) selection).size() == 1) {
					TextGridObject object = AdapterUtils.getAdapterChecked(
							((IStructuredSelection) selection)
									.getFirstElement(), TextGridObject.class);

					List<OpenHandlerContribution> contributions = OpenHandlerContribution
							.contributionsFor(object.getContentType(false));
					for (OpenHandlerContribution contribution : contributions)
						contributionItems.add(new OpenContributionItem(contribution, object));
				}
			}

			// TODO improve error handling
		} catch (AdapterNotFoundException e) {
//			Activator.getDefault().handleProblem(IStatus.WARNING, "Tried to fill the Open With menu for something else than a TextGridObject", e);
			// Just ignore that, since the guard in plugin.xml isn't always effective
			
		}
		catch (CoreException e) {
			Activator.handleError(e);
		}

		return contributionItems.toArray(new IContributionItem[0]);
	}

	private ISelection getSelection() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getSelection();
	}

}
