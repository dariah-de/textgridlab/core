package info.textgrid.lab.ui.core.menus;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.ui.core.menus.messages"; //$NON-NLS-1$
	public static String OpenObjectService_EM_NoEditor;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
