package info.textgrid.lab.ui.core.menus;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.locking.LockingPartListener;
import info.textgrid.lab.ui.core.locking.LockingService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.FileEditorInput;

/**
 * Service class to organize the "open TextGrid object"-scenario 
 * more generic. Depending on the content type of an object it 
 * can be open via a <i>openObject</i> extension or a registered  
 * eclipse editor.  
 * 
 * @author Frank Queens for TextGrid
 *
 */
public class OpenObjectService {
	
	private class CommandSettings {
		private String command;
		private String parameter_uri;
		private String parameter_readonly;
		
		public CommandSettings(String command, String parameter_uri, String parameter_readonly) {
			this.command = command;
			this.parameter_uri = parameter_uri;
			this.parameter_readonly = parameter_readonly;
		}

		public String getCommand() {
			return command;
		}

		public String getParameterUri() {
			return parameter_uri;
		}
		
		public String getParameterReadOnly() {
			return parameter_readonly;
		}
		
		public boolean hasCommand() {
			return command != null;
		}
		
		@SuppressWarnings("unused")
		public boolean hasParameter() {
			return parameter_uri != null;
		}
	}
	
	private static OpenObjectService instance;
	private static Map<String, ArrayList<IConfigurationElement>> ContentTypeMap = null;
	
	
	public synchronized static OpenObjectService getInstance() 
    {
    	if (instance == null) {
        	instance = new OpenObjectService();
        	CreateContentTypeMap();
        }
        return instance;
    }
	
	/**
	 * Tries to open the given TextGrid object. 
	 * First it is checked if there exists an 
	 * <i>openObject</i> extension for the objects 
	 * content type. If not the eclipse editor 
	 * registry is searched for a hit. 
	 * @param object
	 * 				TextGrid object to open
	 * @param mode 
	 * 				selects the method to find an editor to 
	 * 				open the object
	 * 				<br>1 - via openObject extension 
	 * 				<br>2 - via eclipse editor registry
	 * 				<br>0 - both
	 * @param readonly
	 * 				if true object is opened as read only,
	 * 				locking isn't necessary
	 * @return
	 * 		  true if opened object, else false
	 */
	public boolean openObject(TextGridObject object, int mode, boolean readonly) {
		String contentType = null;
		try {
			if (!readonly) {
				if (!userHasEditRights(object))
					readonly = true;
			}	
			
			TGContentType tgContentType = object.getContentType(true);
			if (tgContentType != null) {
				contentType = tgContentType.getId();
			} else {
				Activator
				.handleError(
						null,
						Messages.OpenObjectService_EM_NoEditor,
						object.getContentTypeID(), object);
				return false;
			}	
		} catch (CoreException e) {
			Activator.getDefault().handleProblem(e);
			return false;
		}
		
		if (mode < 2) {
			CommandSettings extensionCommand = findExtensionCommand(contentType);
			if (extensionCommand != null && extensionCommand.hasCommand()) {
				// open via openObject extension
				ParameterizedCommand paramCommand = setCommandParameter(extensionCommand, object, readonly);
				callExtensionCommand(paramCommand);
				return true;
			}
		}	
		
		if (mode != 1) {
			// open via eclipse editor
			LockingService lockingService = LockingService.getInstance();
			if (readonly) {
				object.setOpenAsReadOnly(true);
			} else {
				if (!lockingService.lockObject(object, true))
					object.setOpenAsReadOnly(true);
			}	
			IFile file = null;
			if (file == null && object != null)
				file = getAdapter(object, IFile.class);
			IEditorDescriptor editorDescriptor = findDefaultEditor(contentType, file);
			IFileEditorInput input = new FileEditorInput(file);
			try {
				IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(input,
						editorDescriptor.getId());
				editorPart.getSite().getPage().addPartListener(new LockingPartListener(object));
			} catch (PartInitException e) {
				Activator.getDefault().handleProblem(e);
				return false;
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Delivers the command of an <i>openObject</i> extension
	 * which can be used to open the object in the lab. 
	 * If there are more one extensions, the extension with
	 * the highest priority is selected. 
	 * @param contentType
	 * 				contentType an open command is searched for
	 * @return
	 * 		the command ID
	 */
	private CommandSettings findExtensionCommand(String contentType) {
		//exists an openObject extension for the content type? 
		ArrayList<IConfigurationElement> extensionList = null;
		extensionList = ContentTypeMap.get(contentType);
		
		if (extensionList != null) {
			//get element with highest priority
			IConfigurationElement selectedElement = null;
			String selectedPriority = null;
			
			for (IConfigurationElement element : extensionList) {
				if ((selectedElement == null) || 
					(Integer.parseInt(element.getAttribute("priority")) > (Integer.parseInt(selectedPriority)))) { //$NON-NLS-1$
					selectedElement = element;
					selectedPriority = element.getAttribute("priority"); //$NON-NLS-1$
				} 
			}
			return new CommandSettings(selectedElement.getAttribute("command"), 
									   selectedElement.getAttribute("parameter_uri"),
									   selectedElement.getAttribute("parameter_readonly"));
		}
		return null;
	}
	
	
	/**
	 * Call the given Command.
	 * @param extensionCommand
	 * 				command to call
	 */
	private void callExtensionCommand(ParameterizedCommand extensionCommand) {
		IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);
		try {
			handlerService.executeCommand(extensionCommand, null);
		} catch (ExecutionException e) {
			Activator.getDefault().handleProblem(e);
		} catch (NotDefinedException e) {
			Activator.getDefault().handleProblem(e);
		} catch (NotEnabledException e) {
			Activator.getDefault().handleProblem(e);
		} catch (NotHandledException e) {
			Activator.getDefault().handleProblem(e);
		}
//		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench()
//				.getActiveWorkbenchWindow().getService(ICommandService.class);
//		Command command = commandService.getCommand(extensionCommand);
//			try {
//				IParameter parameter = command.getParameter(extensionCommand+"parameter");
//				Parameterization parm = new Parameterization(parameter, contentType);
//		 		ParameterizedCommand parmCommand = new ParameterizedCommand(
//		 				command, new Parameterization[] { parm });
//		 		handlerService.executeCommand(parmCommand, null);
//			} catch (NotDefinedException e1) {
//				Activator.handleError(e);
//			} catch (ExecutionException e) {
//				Activator.handleError(e);
//			} catch (NotEnabledException e) {
//				Activator.handleError(e);
//			} catch (NotHandledException e) {
//				Activator.handleError(e);
//			}
	}
	
	private ParameterizedCommand setCommandParameter(CommandSettings extensionCommand, TextGridObject object, boolean readonly) {
		ParameterizedCommand parmCommand = null;
		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getService(ICommandService.class);
		Command command = commandService.getCommand(extensionCommand.getCommand()); //$NON-NLS-1$
		IParameter parameter1, parameter2;
		
		try {
			parameter1 = command.getParameter(extensionCommand.getParameterUri());
			parameter2 = command.getParameter(extensionCommand.getParameterReadOnly());
			Parameterization parm1 = new Parameterization(parameter1, object.getURI().toString());
			Parameterization parm2 = new Parameterization(parameter2, new Boolean(readonly).toString());
	 		parmCommand = new ParameterizedCommand(
	 				command, new Parameterization[] { parm1, parm2 });
		} catch (NotDefinedException e) {
			Activator.getDefault().handleProblem(e);
		}
		
		return parmCommand;
	}
	
	/**
	 * Tries to find a eclipse editor to open an 
	 * object with the given content type.
	 * @param contentType
	 * 			  contentType an editor should be 
	 * 				found for	
	 * @param file
	 * 			representation for the TextGrid object
	 * @return
	 * 		  matching editor descriptor or null		
	 */
	private IEditorDescriptor findDefaultEditor(String contentType, IFile file) {
		IEditorDescriptor editorDescriptor = null;
		IContentType eclipseContentType = null;
		IEditorRegistry editorRegistry = PlatformUI.getWorkbench()
				.getEditorRegistry();

		TGContentType tgContentType = TGContentType.getContentType(contentType);
		if (contentType != null)
			eclipseContentType = tgContentType.getEclipseContentType();

		editorDescriptor = editorRegistry.getDefaultEditor(
				file.getName(), eclipseContentType);
		return editorDescriptor;
	}
	
	/**
	 * Fills a static map with all <i>openObject</i> extensions
	 * for the different content types. 
	 */
	private static void CreateContentTypeMap() {
		ContentTypeMap = new HashMap<String, ArrayList<IConfigurationElement>>();
		ArrayList<IConfigurationElement> extensionList;
		
		IConfigurationElement elements[] = Platform.getExtensionRegistry()
				.getConfigurationElementsFor("info.textgrid.lab.ui.core.openObject"); //$NON-NLS-1$

		for (IConfigurationElement element : elements) {
			IConfigurationElement children[] = element.getChildren();
			
			for (IConfigurationElement child : children) {
				if (child.getName().equals("contentType")) { //$NON-NLS-1$
					if (ContentTypeMap.containsKey(child.getAttribute("id"))){ //$NON-NLS-1$
						extensionList = ContentTypeMap.get(child.getAttribute("id")); //$NON-NLS-1$
					} else {
						extensionList = new ArrayList<IConfigurationElement>();
					}	
					extensionList.add(element);
					ContentTypeMap.put(child.getAttribute("id"), extensionList); //$NON-NLS-1$
				}
			}
		}
	}
	
	/**
	 * It is checked if the user has the right to edit the object.
	 * @param tgo
	 * 			object to check for rights
	 * @return
	 * 			true if the user is allowed to edit the object, else false 
	 */
	@SuppressWarnings("static-access")
	private boolean userHasEditRights(TextGridObject tgo) {
		try {
			return tgo.getProjectInstance().checkForRole(tgo.getProjectInstance().getId(), TextGridProject.TG_STANDARD_ROLE_EDITOR);
		} catch (CoreException e) {
			Activator.getDefault().handleProblem(e);
		}
		return false;
	}

}
