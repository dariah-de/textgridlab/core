package info.textgrid.lab.ui.core.menus;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.IOpenHandler;
import info.textgrid.lab.ui.core.OpenHandlerContribution;
import info.textgrid.lab.ui.core.handlers.DefaultOpenHandler;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;

@Deprecated
public class OpenContributionItem extends ActionContributionItem implements
		IContributionItem {

	public static class OpenHandlerAction extends Action implements IAction {

		private final OpenHandlerContribution contribution;
		private final TextGridObject textGridObject;

		public OpenHandlerAction(final OpenHandlerContribution contribution, final TextGridObject textGridObject) {
			this.contribution = contribution;
			this.textGridObject = textGridObject;
		}

		@Override
		public String getText() {
			return contribution.getLabel();
		}

		@Override
		public String getDescription() {
			return contribution.getDescription();
		}


		private IOpenHandler getHandler() {
			if (contribution.getHandler() == null)
				return new DefaultOpenHandler(contribution);
			else
				return contribution.getHandler();
		}

		
		@Override
		public void run() {
			final IOpenHandler handler = getHandler();
			try {
				handler.open(textGridObject);
			} catch (CoreException e) {
				Activator.handleError(e);
			}
		}
		
		

	}


	public OpenContributionItem(final  OpenHandlerContribution contribution, final TextGridObject textGridObject) {
		super(new OpenHandlerAction(contribution, textGridObject));
	}

}
