package info.textgrid.lab.ui.core.menus;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TGContentType;

import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;

import com.google.common.base.Predicate;

/**
 * The given menu is filled with items for each creatable and internal
 * content types. The item command opens the new object dialog with
 * preselected project and content type. 
 * @author Frank Queens
 */
public class NewObjectMenu extends ContributionItem {
	
	class CTComparator implements Comparator<TGContentType> {
		@Override
		public int compare(TGContentType o1, TGContentType o2) {
			return o2.getDescription().compareTo(o1.getDescription());
		}
	}

	public NewObjectMenu() {
	}

	public NewObjectMenu(String id) {
		super(id);
	}
	
	/**
     * Fills the dynamic menu for new objects
     */
	@Override
    public void fill(Menu menu, int index) {
		final ICommandService commandService = (ICommandService) PlatformUI.getWorkbench()
											.getActiveWorkbenchWindow().getService(ICommandService.class);
		
		final IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench()
											.getActiveWorkbenchWindow().getService(IHandlerService.class);

		
		TGContentType[] contentTypes = TGContentType.getContentTypes(new Predicate<TGContentType>() {
			public boolean apply(TGContentType input) {
				return input.isCreatable() && !input.isInternal();
			}
		});
		
		Arrays.sort(contentTypes, new CTComparator());
		
		for (final TGContentType contentType : contentTypes) {
			if (contentType.getId().contains("aggregation")) //$NON-NLS-1$
				continue;
			else if (contentType.getId().contains("text/tg.work+xml")) //$NON-NLS-1$
				continue;
			
			MenuItem item = new MenuItem(menu, SWT.NONE, index);
			item.setText(contentType.getDescription());
			item.setImage(contentType.getImage(false));
			
			Listener listener = new Listener() {
	            public void handleEvent(Event event) {
	                switch (event.type) {
	                case SWT.Selection:
	                	Command command = commandService.getCommand("info.textgrid.lab.ui.core.addobject"); //$NON-NLS-1$
	                	try {
							IParameter parameter = command.getParameter("info.textgrid.lab.ui.core.addobject.contenttype"); //$NON-NLS-1$
							Parameterization parm = new Parameterization(parameter, contentType.getId());
					 		ParameterizedCommand parmCommand = new ParameterizedCommand(
					 				command, new Parameterization[] { parm });
					 		try {
								handlerService.executeCommand(parmCommand, null);
							} catch (ExecutionException e) {
								Activator.handleError(e);
							} catch (NotEnabledException e) {
								Activator.handleError(e);
							} catch (NotHandledException e) {
								Activator.handleError(e);
							}
						} catch (NotDefinedException e) {
							Activator.handleError(e);
						}
	                    break;
	                }
	            }
	        };
	        item.addListener(SWT.Selection, listener);
		}
    }

}
