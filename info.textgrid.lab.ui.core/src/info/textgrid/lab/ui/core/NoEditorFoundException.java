package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

/**
 * Returned if no editor could be found to open the given object.
 */
public class NoEditorFoundException extends CoreException {

	public NoEditorFoundException(final TextGridObject object) {
		super(buildStatus(object));
	}

	private static IStatus buildStatus(final TextGridObject object) {
		try {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(Messages.NoEditorFoundException_EM_NoEditorFound, object, object.getContentType(false)));
		} catch (final CoreException e) {
			return e.getStatus();
		}
	}

	private static final long serialVersionUID = 7935674832591834907L;

}
