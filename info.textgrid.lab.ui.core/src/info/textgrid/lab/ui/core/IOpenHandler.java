package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;

/**
 * Interface for custom open handlers in TextGridLab.
 * 
 * <p>
 * If you need special behaviour aside from opening an editor and switching
 * perspectives, implement this interface and specify your implementation in the
 * <var>handler</var> field of your contribution to the extension point <var>
 * info.textgrid.lab.ui.core.openObject</var>.
 * </p>
 * 
 * <p>
 * Your class is instantiated with its default constructor and then initialized 
 * with the corresponding contribution by a call to {@link #setContribution(OpenHandlerContribution)}.
 * 
 * You may inherit your implementation from {@link AbstractOpenHandler}. If you
 * do not specify an implementation, DefaultOpenHandler is used.
 * </p>
 * 
 * 
 */
public interface IOpenHandler {

	/**
	 * Inform the open handler about the contribution it is used for. Implementations
	 * might want to save the contribution (the {@link AbstractOpenHandler} does so)
	 * and extract fields like the target perspective from it.
	 * 
	 * <p>
	 * The framework guarantees that this method is called with a valid contribution before
	 * any calls to the {@link #open(TextGridObject)} method.
	 * </p>
	 * 
	 * @param contribution The contribution for which this handler has been instantiated.
	 */
	public void setContribution(final  OpenHandlerContribution contribution);

	
	public void open(final  TextGridObject textGridObject) throws CoreException;

}
