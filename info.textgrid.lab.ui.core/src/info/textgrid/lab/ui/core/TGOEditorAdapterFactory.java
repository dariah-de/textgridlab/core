package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.part.FileEditorInput;

/**
 * Adapts TextGridObjects to IFileEditorInputs. This is here since
 * IFileEditorInput is specified in Eclipse's UI plugins.
 */
public class TGOEditorAdapterFactory implements IAdapterFactory {

	public static final Class<?>[] ADAPTER_LIST = { IFileEditorInput.class, IEditorInput.class };

	public Object getAdapter(Object adaptableObject,
			@SuppressWarnings("unchecked") Class adapterType) {
		IFile file = AdapterUtils.getAdapter(adaptableObject, IFile.class);
		if (file != null)
			return new FileEditorInput(file);

		return null;
	}

	public Class<?>[] getAdapterList() {
		return ADAPTER_LIST;
	}

}
