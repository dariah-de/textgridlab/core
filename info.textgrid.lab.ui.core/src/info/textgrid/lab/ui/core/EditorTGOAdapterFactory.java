package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.IFileEditorInput;

/**
 * Adapts {@link IFileEditorInput}s to {@link TextGridObject}s.
 */
public class EditorTGOAdapterFactory implements IAdapterFactory {

	public static final Class<?>[] ADAPTER_LIST = new Class<?>[] { TextGridObject.class };

	@SuppressWarnings("unchecked")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof IFileEditorInput)
			return AdapterUtils.getAdapter(((IFileEditorInput) adaptableObject).getFile(), adapterType);

		return null;
	}

	@SuppressWarnings("unchecked")
	public Class[] getAdapterList() {
		return ADAPTER_LIST;
	}

}