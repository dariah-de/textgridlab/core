/**
 * This plugin contains core UI functionality for the TextGridLab. Here you can
 * find generic, not tool-specific frameworks,
 * {@linkplain info.textgrid.lab.ui.core.dialogs dialogs},
 * {@linkplain info.textgrid.lab.ui.core.handlers handlers},
 * {@linkplain info.textgrid.lab.ui.perspectives perspectives},
 * {@linkplain info.textgrid.lab.ui.core.menus menus and menu items},
 * {@linkplain info.textgrid.lab.ui.core.testers testers} and
 * {@linkplain info.textgrid.lab.ui.core.utils utilities}.
 */
package info.textgrid.lab.ui.core;

