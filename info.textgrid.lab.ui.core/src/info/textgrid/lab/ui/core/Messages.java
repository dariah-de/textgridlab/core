package info.textgrid.lab.ui.core;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.ui.core.messages"; //$NON-NLS-1$
	public static String NewTGOPage_Label_createNewProjectLink;
	public static String Activator_EM_JobError;
	public static String NewTGOPage_isBroken;
	public static String NewTGOPage_JobName;
	public static String NewTGOPage_Label_ProjectLink;
	public static String NewTGOPage_metadataHint;
	public static String NewTGOPage_pageDescription;
	public static String NewTGOPage_pageTitle;
	public static String NewTGOPage_project;
	public static String NewTGOPage_selectProjectError;
	public static String NewTGOPage_selectTypeError;
	public static String NewTGOPage_type;
	public static String NewTGOPage_UI_CheckPermission;
	public static String NewTGOPage_UI_DelegateWarning;
	public static String NewTGOPage_UI_ElseWarning;
	public static String NewTGOPage_UI_Intro;
	public static String NewTGOPage_UI_ObjectCreated;
	public static String NoEditorFoundException_EM_NoEditorFound;
	public static String TGContentType_XML;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
