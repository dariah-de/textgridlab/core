package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class IsAggregationPropertyTester extends PropertyTester {

	final String AGGREGATION = "aggregation";
	
	public IsAggregationPropertyTester() {
		// TODO Auto-generated constructor stub
	}

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		
		TextGridObject tgo = AdapterUtils.getAdapter(receiver, TextGridObject.class);
		if(tgo != null) {
			try {
				if (tgo.getContentTypeID().contains(AGGREGATION))
					return true;
				else 
					return false;
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"Could not retrieve content type of " 
								+ receiver.toString(), e);
				Activator.getDefault().getLog().log(status);
			}
		}
		return false;
	}

}
