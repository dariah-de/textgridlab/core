package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.TextGridProject;

import org.eclipse.core.expressions.PropertyTester;

public class HasProjectFilePropertyTester extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if (receiver != null && receiver instanceof TextGridProject) {
			String pf = ((TextGridProject) receiver).getProjectfile();
			// System.err.println(":  " + pf + " " + (pf != null &&
			// !"".equals(pf)));
			return (pf != null && !"".equals(pf));
		}

		return false;
	}

}
