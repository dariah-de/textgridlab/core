package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class IsWebpublisherPropertyTester extends PropertyTester {

	final String WEBPUBLISHER = "text/tg.webpublisher+xml";
	
	public IsWebpublisherPropertyTester() {
		// TODO Auto-generated constructor stub
	}

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if(receiver != null && receiver instanceof TextGridObject) {
			try {
				if (((TextGridObject)receiver).getContentTypeID().equals(WEBPUBLISHER))
					return true;
				else 
					return false;
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"Could not retrieve content type of " 
								+ ((TextGridObject)receiver).getURI().toString(), e);
				Activator.getDefault().getLog().log(status);
			}
		}
		return true;
	}

}
