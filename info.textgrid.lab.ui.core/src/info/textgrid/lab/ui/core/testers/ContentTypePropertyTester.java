package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.statushandlers.StatusManager;

public class ContentTypePropertyTester extends PropertyTester {

	public ContentTypePropertyTester() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		TextGridObject textGridObject = AdapterUtils.getAdapter(receiver, TextGridObject.class);
		if (textGridObject != null) {
			try {
				if (expectedValue.equals(textGridObject.getContentType(false).getEclipseContentTypeID()))
					return true;
				if (expectedValue.equals("org.eclipse.wst.xml.core.xmlsource") && textGridObject.getContentType(false).equals(TGContentType.getContentType("text/xml")))
					return true;
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}
		return false;
	}
}
