package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.utils.TextGridObjectTransfer;

import org.eclipse.core.expressions.PropertyTester;

public class HasCopyInfoPropertyTester extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if(receiver != null && receiver instanceof TextGridProject) {
			if (TextGridObjectTransfer.getTransfer().getCopiedSelectionAsUriList() != null)
				return true;
			else 
				return false;
		}
		return false;
	}

}
