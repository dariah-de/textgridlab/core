/**
 * 
 */
package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.TextGridProject;
import org.eclipse.core.expressions.PropertyTester;


/**
 * A PropertyTester class which controls whether the user has a leader-role 
 * on the selected project or not.
 * 
 * @author yahya
 *
 */
public class IsLeaderPropertyTester extends PropertyTester {
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.expressions.IPropertyTester#test(java.lang.Object, java.lang.String, java.lang.Object[], java.lang.Object)
	 */
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		
		if(receiver != null && receiver instanceof TextGridProject) {
			return ((TextGridProject)receiver).iAmLeader();
		}
		
		return false;
	}
}
