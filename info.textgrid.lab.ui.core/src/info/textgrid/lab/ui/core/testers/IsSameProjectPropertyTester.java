package info.textgrid.lab.ui.core.testers;

import java.util.Iterator;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

public class IsSameProjectPropertyTester extends PropertyTester {

	public IsSameProjectPropertyTester() {
	}

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		// get receivers project
		TextGridProject project = null;
		TextGridObject tgo = AdapterUtils.getAdapter(receiver, TextGridObject.class);
		if(tgo == null) {
			if (receiver instanceof TextGridProject)
				project = (TextGridProject) receiver;
			return false;
		} else {
			try {
				project = tgo.getProjectInstance();
			} catch (CoreException e) {
				return false;
			}
		}

		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		ISelection selection = page.getSelection();
		
		// compare projects
		if (!selection.isEmpty()) {
			for (Iterator<?> iterator = ((IStructuredSelection) selection).iterator(); iterator
					.hasNext();) {
				Object obj = iterator.next();
				TextGridObject item = AdapterUtils.getAdapter(obj, TextGridObject.class);
				if (item != null) {
					try {
						if (!item.getProjectInstance().equals(project))
							return false;
					} catch (CoreException e) {
						return false;
					}
				} else {
					return false;
				}
			}
		} 
		return true;
	}
}

