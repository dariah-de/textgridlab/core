package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.statushandlers.StatusManager;

public class HasCreateRightPropertyTester extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if(receiver != null && receiver instanceof TextGridProject) {
			try {
				if (((TextGridProject)receiver).hasPermissions(ITextGridPermission.CREATE))
					return true;
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}
		return false;
	}

}

