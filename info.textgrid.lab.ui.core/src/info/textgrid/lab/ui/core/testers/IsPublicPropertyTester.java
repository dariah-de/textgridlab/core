package info.textgrid.lab.ui.core.testers;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.statushandlers.StatusManager;

public class IsPublicPropertyTester extends PropertyTester {
	
	public IsPublicPropertyTester() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		TextGridObject textGridObject = AdapterUtils.getAdapter(receiver, TextGridObject.class);
		if (textGridObject != null) {
			try {
				if (expectedValue.equals(textGridObject.isPublic()))
					return true;
				else 
					return false;
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}
		return false;
	}
}
