package info.textgrid.lab.ui.core;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

@Deprecated
public abstract class AbstractOpenHandler implements IOpenHandler {

	private OpenHandlerContribution contribution;

	public void setContribution(final OpenHandlerContribution contribution) {
		this.contribution = contribution;
	}

	public OpenHandlerContribution getContribution() {
		return contribution;
	}

	/**
	 * If the contribution defines a perspective, show the perspective
	 * (otherwise do nothing).
	 * 
	 * @param window
	 *            The workbench window that should show the perspective. Use
	 *            {@link #showPerspective()} for the current window.
	 * @return the workbench page described by the perspective, or the current
	 *         workbench page if no perspective specified.
	 * @throws WorkbenchException
	 *             if the perspective could not be opened.
	 * @see IWorkbench#showPerspective(String, IWorkbenchWindow)
	 */
	public IWorkbenchPage showPerspective(IWorkbenchWindow window)
			throws WorkbenchException {
		if (getContribution().getPerspective() != null)
			return PlatformUI.getWorkbench().showPerspective(
					getContribution().getPerspective(), window);
		else
			return window.getActivePage();
	}

	/**
	 * If the contribution defines a perspective, show the perspective
	 * (otherwise do nothing).
	 * 
	 * @return the workbench page described by the perspective, or the current
	 *         workbench page if no perspective specified.
	 * @throws WorkbenchException
	 *             if the perspective could not be opened.
	 * @see IWorkbench#showPerspective(String, IWorkbenchWindow)
	 */
	public IWorkbenchPage showPerspective() throws WorkbenchException {
		return showPerspective(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow());
	}

	/**
	 * Try to open the object in an editor on the given workbench page.
	 * 
	 * <p>
	 * If the contribution specifies an editor, this editor is used, otherwise
	 * the implementation tries to find the default editor using Eclipse's
	 * mechanisms.
	 * </p>
	 * 
	 * @param object
	 *            The object to open
	 * @param activePage
	 *            The workbench page in which this object can be opened.
	 * @return The editor that has been opened.
	 * @throws NoEditorFoundException
	 *             if no editor for this object could be determined.
	 * @throws PartInitException
	 *             if an editor was found, but somehow failed to open.
	 * @throws CoreException
	 *             if determining the content type fails (probably due to read
	 *             errors from the repository).
	 * @see #openEditor(TextGridObject)
	 * @see TGContentType#getEclipseContentType()
	 */
	public IEditorPart openEditor(final TextGridObject object,
			final IWorkbenchPage activePage) throws CoreException {
		IFileEditorInput input = AdapterUtils.getAdapter(object,
				IFileEditorInput.class);

		if (getContribution().getEditor() != null) {
			return activePage.openEditor(input, getContribution().getEditor(),
					true);
		} else {
			IEditorDescriptor editorDescriptor = findEditorDescriptor(object);
			if (editorDescriptor != null)
				return activePage.openEditor(input, editorDescriptor.getId());
			else
				throw new NoEditorFoundException(object);
		}
	}

	/**
	 * Tries to find the default editor descriptor for the given {@link TextGridObject} <em>without considering the Open contribution</em>.
	 * @param object The TextGridObject for which we try to find an exception.
	 * @return A matching editor descriptor or null if none could be found.
	 * @throws CoreException if something goes wrong while trying to determine the <var>object</var>'s content type.
	 */
	public static IEditorDescriptor findEditorDescriptor(final TextGridObject object) throws CoreException {
		IFileEditorInput input = AdapterUtils.getAdapter(object, IFileEditorInput.class);
		IEditorDescriptor editorDescriptor = null;
		IEditorRegistry editorRegistry = PlatformUI.getWorkbench()
				.getEditorRegistry();

		TGContentType contentType = object.getContentType(false);
		IContentType eclipseContentType = null;
		if (contentType != null)
			eclipseContentType = contentType.getEclipseContentType();
		editorDescriptor = editorRegistry.getDefaultEditor(input.getName(),
				eclipseContentType);
		return editorDescriptor;
	}

	/**
	 * Try to open the object in an editor.
	 * 
	 * <p>
	 * If the contribution specifies an editor, this editor is used, otherwise
	 * the implementation tries to find the default editor using Eclipse's
	 * mechanisms.
	 * </p>
	 * 
	 * @param object
	 *            The object to open
	 * @return The editor that has been opened.
	 * @throws NoEditorFoundException
	 *             if no editor for this object could be determined.
	 * @throws PartInitException
	 *             if an editor was found, but somehow failed to open.
	 * @throws CoreException
	 *             if determining the content type fails (probably due to read
	 *             errors from the repository).
	 */
	public IEditorPart openEditor(final  TextGridObject object)
			throws CoreException {
		IWorkbenchPage activePage = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		return openEditor(object, activePage);
	}

	public abstract void open(TextGridObject textGridObject)
			throws CoreException;

}
