package info.textgrid.lab.ui.core.locking;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.ui.core.locking.messages"; //$NON-NLS-1$
	public static String LockingJob_Cancel;
	public static String LockingJob_IM_NoConnection;
	public static String LockingJob_IM_NoExclusiveAccess;
	public static String LockingJob_IM_PleaseRetry;
	public static String LockingJob_Retry;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
