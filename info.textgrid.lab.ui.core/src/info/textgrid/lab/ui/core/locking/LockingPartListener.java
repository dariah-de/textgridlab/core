package info.textgrid.lab.ui.core.locking;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Listener removes existing locking job when
 * part is closed.
 * @author Frank Queens
 *
 */

public class LockingPartListener implements IPartListener2 {
	TextGridObject object;
	
	public LockingPartListener(TextGridObject object) {
		this.object = object;
	}
	

	@Override
	public void partActivated(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partClosed(IWorkbenchPartReference partRef) {
		if (partRef instanceof IEditorReference) {
			try {
				final IEditorInput editorInput = ((IEditorReference) partRef).getEditorInput();
				final TextGridObject editedObject = AdapterUtils.getAdapter(editorInput, TextGridObject.class);
				if (editedObject == null) {
					StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID,
							MessageFormat.format("Watchdog for {2} ignores that part {0} ({1}), editing unknown object {3}, has been closed", //$NON-NLS-1$
									partRef.getPartName(), partRef.getId(), object, editorInput == null? "(no input)" : editorInput.getName()))); //$NON-NLS-1$
				} else if (editedObject.equals(this.object)) {
					doUnlockObject(partRef);
				} else {
					StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, 
							MessageFormat.format("Watchdog for {2} ignores that part {0} ({1}) has been closed", 
									partRef.getPartName(), partRef.getId(), object)));
				}
			} catch (PartInitException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}
	}


	private void doUnlockObject(IWorkbenchPartReference partRef) {
		StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, 
				MessageFormat.format("Part {0} ({1}) closed: Object {2} will be unlocked, says {3}", 
						partRef.getPartName(), partRef.getId(), object, this)));
		LockingService.getInstance().unlockObject(object);
		object.setOpenAsReadOnly(false);
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partOpened(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partHidden(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partVisible(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
		
	}

}
