package info.textgrid.lab.ui.core.locking;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

import javax.xml.ws.WebServiceException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;


public class LockingJob extends Job {
	
	private final TextGridObject object;
	private final boolean message;
	private final String sid;
	private final TGCrudService crudServiceStub;
	private boolean locked;
	public LockingJob(String name, TextGridObject object, boolean message) {
		super(name);
		this.object = object;
		this.message = message;
		this.sid = RBACSession.getInstance().getSID(false);
		this.crudServiceStub = CrudClientUtilities.getCrudServiceStub();
		addJobChangeListener(new JobChangeLogger("Locking job for {0}", object));
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			try {
				//TG-1894
				if (object.isPublic()) {
					//No Locking necessary because it's already read-only
					return Status.OK_STATUS;
				} else {
					locked = crudServiceStub.lock(sid, "", object.getURI().toString()); //$NON-NLS-1$
				    StatusManager.getManager().handle(new Status(locked? IStatus.INFO : IStatus.WARNING, Activator.PLUGIN_ID,
						NLS.bind(locked? "Locked {0}" : "Failed to lock {0}", object)));
				}
				
				if (!locked) {
					if (message) {
						UIJob uiJob = new UIJob("Locking Problem") { //$NON-NLS-1$
							
							@Override
							public IStatus runInUIThread(IProgressMonitor monitor) {
								MessageDialog
								.openWarning(
										PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
										"Locking Problem", 
										NLS.bind(Messages.LockingJob_IM_NoExclusiveAccess, object.toString()));
								return Status.OK_STATUS;
							}
						};
						uiJob.schedule();
					}	
					LockingService.getInstance().removeJobFromRegistry(object);
					return Status.CANCEL_STATUS;
				}
			} catch (final WebServiceException e) {
				return new Status(IStatus.WARNING, 
						Activator.PLUGIN_ID,
						NLS.bind(Messages.LockingJob_IM_NoConnection, object.toString()),
						e);	
			}
			this.schedule(LockingService.LOCKING_DELAY);
		} catch (AuthFault e) {
			Activator.handleError(e, e.getMessage(), object);
		} catch (IoFault e) {
			Activator.handleError(e, e.getMessage(), object);
		} catch (ObjectNotFoundFault e) {
			Activator.handleError(e, e.getMessage(), object);
		} catch (CoreException e) {
			Activator.handleError(e, e.getMessage(), object);
		}
		return Status.OK_STATUS;	
	}

	public boolean isLocked() {
		return locked;
	}	
		
}
