package info.textgrid.lab.ui.core.locking;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;


/**
 * Service class to control the lock/unlock mechanism of 
 * tg-crud for TextGrid objects. Locking an object creates a 
 * {@link LockingJob} which refreshes the locking automatically. 
 * @author Frank Queens for TextGrid
 *
 */
public class LockingService {
	//TG-crud locks the object for 30 minutes, we renew the lock
	//after 29 minutes (1740000 milliseconds), so we can be sure
	//to keep it 
	public final static long LOCKING_DELAY = 1740000;
	
	private static LockingService instance;
	private static Map<TextGridObject, LockingJob> jobRegistry = new ConcurrentHashMap<TextGridObject, LockingJob>();
	 
    private LockingService() {}

    public synchronized static LockingService getInstance() 
    {
    	if (instance == null) {
        	instance = new LockingService();
        }
        return instance;
    }
    
    /**
     * Tries to lock the given TextGridObject for exclusive access 
     * by calling the tg-crud locking mechanism. 
     * @param object
     * 				TextGridObjekt to lock
     * @param message
     * 				true: a message is shown, if the object
     * 						can't be locked
     * @return true if the object can be locked, else false
     */
    public boolean lockObject(TextGridObject object, boolean message) {
    	if (isNull(object))
    		return false;
    	
    	LockingJob lockingJob = jobRegistry.get(object);
    	
    	if (lockingJob == null) {
    		
    		try {
				if (!object.hasPermissions(ITextGridPermission.UPDATE))
					return false;
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}	// we don't need to lock read-only stuff
    		
    		lockingJob = new LockingJob(NLS.bind("Locking object {0}", object.toString()), object, message);
			jobRegistry.put(object, lockingJob);
    	} else {
    		// user has already locked the object
    		return true;
    	}
    	
		try {
			IStatus status;
			do {
				lockingJob.schedule();
				lockingJob.join();
				status = lockingJob.getResult();
			} while (status.getSeverity() == IStatus.ERROR && retryLock(object, status));
			if (!lockingJob.isLocked()) {
				return false;
			} else {
				return true;
			}
		} catch (InterruptedException e) {
			Activator.handleError(e, e.getMessage(), object);
			return false;
		}
    }
    
    
    private boolean retryLock(TextGridObject object, IStatus status) {
    	if (Display.getCurrent() == null) {
    		// we cannot offer a retry outside of the UI thread, cf. TG-2048
    		StatusManager.getManager().handle(status);
    		return false;
    	} else {
			MessageDialog dialog = new MessageDialog(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),
					"Locking Problem", 
					null, NLS.bind(Messages.LockingJob_IM_NoConnection,
							object.toString(), status.getMessage()), MessageDialog.QUESTION,
					new String[] { Messages.LockingJob_Retry,
							Messages.LockingJob_Cancel }, 1);
			dialog.setBlockOnOpen(true);
			boolean retry = dialog.open() == 0;
			MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, 0, retry? "Locking problem, trying again": "Locking problem, canceling", null);
			multiStatus.add(status);
			StatusManager.getManager().handle(multiStatus);
			return retry;
    	}
	}

	/**
     * Removes a possible locking job from the registry and unlocks the 
     * given TextGridObject by calling the tg-crud unlock funkction.
     * @param object
     * 			TextGridObject to unlock
     * @return
     * 			true if object could be unlocked, else false
     */
    public boolean unlockObject(TextGridObject object) {
    	if (isNull(object))
    		return false;
    	LockingJob lockingJob = jobRegistry.get(object);
    	if (lockingJob != null) {
    		removeJobFromRegistry(object);
    		String sid = RBACSession.getInstance().getSID(false);
    		TGCrudService crudServiceStub = CrudClientUtilities.getCrudServiceStub();
    		try {
				boolean unlocked = crudServiceStub.unlock(sid, "", object.getURI().toString());
				StatusManager.getManager().handle(new Status(unlocked? IStatus.INFO : IStatus.WARNING, Activator.PLUGIN_ID,
						NLS.bind(unlocked? "Unlocked {0}" : "Failed to unlock {0}", object), new Exception("This unlock was caused by ...")));
				return unlocked;
			} catch (AuthFault e) {
				Activator.handleError(e, e.getMessage(), object);
			} catch (IoFault e) {
				Activator.handleError(e, e.getMessage(), object);
			} catch (ObjectNotFoundFault e) {
				Activator.handleError(e, e.getMessage(), object);
			}
    	}
    	return false;
    }
    
    /**
     * Removes a possible locking job from the registry
     * @param object
     * 			TextGridObject the job should be initiated for
     * @return
     * 			true, if a job was removed, else false
     */
    public boolean removeJobFromRegistry(TextGridObject object) {
    	if (isNull(object))
    		return false;
    	LockingJob lockingJob = jobRegistry.get(object);
    	if (lockingJob != null) {
    		lockingJob.cancel();
    		jobRegistry.remove(object);
    		return true;
    	}
    	return false;
    }
    
    private boolean isNull(TextGridObject argument) {
    	if (argument == null) {
			StatusManager.getManager()
					.handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
							"Locking service called with null instead of a TGO", new NullPointerException()),
							StatusManager.LOG);
			return true;
    	} else
    		return false;
    }
    
    /**
     * Exists a locking job for the given TextGrid object.
     * @param object
     * 			a TextGrid object 
     * @return
     * 			true if a locking job exists, else false
     */
    public boolean objectIsLocked(TextGridObject object) {
    	return (!isNull(object) && jobRegistry.containsKey(object));
    }
    
    
}
