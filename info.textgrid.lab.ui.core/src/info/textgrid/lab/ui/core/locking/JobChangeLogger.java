package info.textgrid.lab.ui.core.locking;

import info.textgrid.lab.ui.core.Activator;

import java.sql.Time;
import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.ui.statushandlers.StatusManager;

public class JobChangeLogger implements IJobChangeListener {

	private String name;

	public JobChangeLogger(String name, Object... args) {
		this.name = MessageFormat.format(name, args);
	}
	
	private void log(final String message, final Object... args) {
		final String composedMessage = formatMessage(message, args);
		StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, composedMessage), StatusManager.LOG);
	}

	private String formatMessage(final String message, final Object... args) {
		final String composedMessage = MessageFormat.format(message, args) + ": " + name;
		return composedMessage;
	}

	@Override
	public void aboutToRun(IJobChangeEvent event) {
		log("about to run");
	}

	@Override
	public void awake(IJobChangeEvent event) {
		log("awake");
	}

	@Override
	public void done(IJobChangeEvent event) {
		MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, 0, formatMessage("done"), null);
		multiStatus.add(event.getResult());
		StatusManager.getManager().handle(multiStatus);
	}

	@Override
	public void running(IJobChangeEvent event) {
		log("running");
	}

	@Override
	public void scheduled(IJobChangeEvent event) {
		log("scheduled in {0} min", event.getDelay() / 60000);
	}

	@Override
	public void sleeping(IJobChangeEvent event) {
		log("sleeping");
	}

}
