package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.metadataeditor.MetaDataView;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * 
 * A handler to open a TextGridObject's Metadata in the Metadata-Editor
 * 
 * @author hausner
 * 
 */
public class OpenMDEHandler extends AbstractHandler {
	private static final String PARAMETER_URI_ID = "info.textgrid.lab.ui.core.commands.OpenMDE.parameter.uri";
	private static final String PARAMETER_READONLY_ID = "info.textgrid.lab.ui.core.commands.OpenMDE.parameter.readonly";

	public Object execute(ExecutionEvent event) throws ExecutionException {
		String parameterURI = null;
		String parameterReadOnly = null;
		TextGridObject tgo = null; 

		//get parameter
		parameterURI = event.getParameter(PARAMETER_URI_ID);
		parameterReadOnly = event.getParameter(PARAMETER_READONLY_ID);
		
		//get TextGrid object
		if (parameterURI != null) {
			try {
				tgo = TextGridObject.getInstance(new URI(parameterURI), false);
			} catch (CrudServiceException e) {
				Activator.handleError(e, e.getMessage(), parameterURI);
			} catch (URISyntaxException e) {
				Activator.handleError(e, e.getMessage(), parameterURI);
			}
		} else {
			ISelection selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				tgo = AdapterUtils.getAdapter(obj, TextGridObject.class);
			}	
		}
		
		if (tgo != null) {
			try {
				((MetaDataView) PlatformUI
						.getWorkbench()
						.getActiveWorkbenchWindow()
						.getActivePage()
						.showView(
								"info.textgrid.lab.core.metadataeditor.view", //$NON-NLS-1$
								null, IWorkbenchPage.VIEW_VISIBLE))
						.setMetadataInView(tgo, true);
	
			} catch (PartInitException e) {
				Activator.handleError(e, Messages.OpenMDEHandler_CouldNotOpenME);
			}
		}

		return null;

	}
}