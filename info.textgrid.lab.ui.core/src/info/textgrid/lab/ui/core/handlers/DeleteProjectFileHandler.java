package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectFile;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;

import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

public class DeleteProjectFileHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {

			Object obj = ((IStructuredSelection) selection).getFirstElement();

			if (obj instanceof TextGridProject) {
				final TextGridProject tgp = (TextGridProject) ((IAdaptable) obj)
						.getAdapter(TextGridProject.class);

				if (tgp != null) {

					if (!MessageDialog
							.openQuestion(
									HandlerUtil.getActiveShell(event),
									Messages.DeleteProjectFileHandler_DeleteProjectFile,
									Messages.DeleteProjectFileHandler_IM_WantToDelete))
						return null;

					Job delJob = new Job(Messages.DeleteProjectFileHandler_DeletingFile) {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							try {
								if (monitor == null)
									monitor = new NullProgressMonitor();

								monitor.beginTask(
										Messages.DeleteProjectFileHandler_DeletingFile, 100);

								TextGridProjectFile pf = new TextGridProjectFile(
										TextGridProject.getProjectInstance(tgp
												.getId()));

								TextGridObject projectFile = pf
										.getProjectFileObject(false, monitor);
								// = TextGridObject
								// .getInstance(new URI(tgp
								// .getProjectfile()), false);

								monitor.worked(20);

								tgp.unsetProjectFile();

								monitor.worked(20);

								projectFile.delete(monitor);

								System.err.println(Messages.DeleteProjectFileHandler_FileDeleted);

								monitor.done();
								return Status.OK_STATUS;
							} catch (CrudServiceException e) {
								Activator.handleError(e);
							} catch (OfflineException e) {
								Activator.handleError(e);
							} catch (RemoteException e) {
								Activator.handleError(e);
							} catch (AuthenticationFault e) {
								Activator.handleError(e);
							} catch (CoreException e) {
								Activator.handleError(e);
							} catch (ProjectDoesNotExistException e) {
								Activator.handleError(e);
							}
							return Status.CANCEL_STATUS;
						}
					};

					delJob.setUser(true);
					delJob.schedule();

				}
			}
		}
		return null;
	}
}
