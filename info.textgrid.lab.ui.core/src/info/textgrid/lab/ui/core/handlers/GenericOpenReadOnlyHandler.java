package info.textgrid.lab.ui.core.handlers;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.menus.OpenObjectService;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class GenericOpenReadOnlyHandler extends AbstractHandler implements IHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSel = (IStructuredSelection) selection;

			for (Object o : structuredSel.toList())
				openObject(o);
		} else {
			Activator.handleProblem(IStatus.ERROR, NLS.bind(
					Messages.OpenHandler_CannotOpenSelection, selection), null);
		}

		return null;
	}

	public static IEditorPart openObject(Object o) {
		
		TextGridObject textGridObject = getAdapter(o, TextGridObject.class);
		OpenObjectService.getInstance().openObject(textGridObject, 0, true);
		return null;
	}
}
