package info.textgrid.lab.ui.core.handlers;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.search.FullTextEntry;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * A generic handler to open TextGrid objects.
 * 
 * This is a very basic handler. It should be replaced with something able to
 * handle different content types and probably configurable.
 * 
 * 
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv)
 */
@Deprecated
public class OpenHandler extends AbstractHandler implements IHandler {

	public OpenHandler() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSel = (IStructuredSelection) selection;

			for (Object o : structuredSel.toList())
				openObject(o, HandlerUtil.getActiveWorkbenchWindow(event).getActivePage(), event);
		} else {
			Activator.handleProblem(IStatus.ERROR, NLS.bind(
					Messages.OpenHandler_CannotOpenSelection, selection), null);
		}

		return null;
	}

	/**
	 * Tries to open the object o in an editor.
	 * 
	 * @param o
	 *            The object to open. Should be adaptable to {@link IFile}.
	 * @param workbenchPage
	 *            The workbench page in which the editor should be opened.
	 * @param event TODO
	 * @return The editor with the opened file, or null if no acceptable editor
	 *         could be found.
	 */
	public static IEditorPart openObject(Object o, IWorkbenchPage workbenchPage, ExecutionEvent event) {

		TextGridObject textGridObject = getAdapter(o, TextGridObject.class);
		IFile file = getAdapter(o, IFile.class);
		if (file == null && textGridObject != null)
			file = getAdapter(textGridObject, IFile.class);

		final IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);
		IContentType eclipseContentType = null;

		if (textGridObject != null) {
			// if TGO is an image say that a graphical editor will be available
			// in version 1

			try {
				String format = textGridObject.getContentTypeID();
				if (format.contains("image") || format.equals("text/linkeditorlinkedfile")) {
					try {
						handlerService.executeCommand("info.textgrid.lab.linkeditor.rcp_linkeditor.defaultOpen", null);
					} catch (CommandException e) {
						Activator.handleError(e, "Could not open the text image link editor.");
					}
					return null;
				} else if (format.equals("text/ttle")) {
					try {
						handlerService.executeCommand("info.textgrid.lab.ttle.defaultOpen", null);
					} catch (CommandException e) {
						Activator.handleError(e, "Could not open the text text link editor.");
					}
					return null;
				} else if (format.contains("projectfile")) {
					MessageDialog
							.openInformation(
									null,
									null,
									"The object you tried to open is a project file, a TextGrid internal helper object that cannot be edited directly.\n"
											+ "Please use the Metadata Template-Editor to edit it!");
					return null;
				} else if (format.equals("text/tg.work+xml")) {
					try {
						handlerService.executeCommand(
								"info.textgrid.lab.ui.core.commands.OpenMDE",
								null);
					} catch (CommandException e) {
						IStatus status = new Status(IStatus.ERROR,
								Activator.PLUGIN_ID,
								"Could not open Metadata Editor", e);
						Activator.getDefault().getLog().log(status);
					}
					return null;
				} else if (format.equals("application/x-digilib-json")){
					try {
						handlerService
								.executeCommand(
										"de.mpg.mpiwg.itgroup.textgrid.digilib.showImage",
										null);
					} catch (CommandException e) {
						IStatus status = new Status(IStatus.ERROR,
								Activator.PLUGIN_ID,
								"Could not open Digilib viewer", e);
						Activator.getDefault().getLog().log(status);
					}
					return null;
				}
			} catch (CoreException e1) {
				Activator.handleError(e1,
						"Could not extract {0}'s content type: {1}",
						textGridObject, e1.getMessage());
			}
			try {

				IEditorDescriptor editorDescriptor = null;
				IEditorRegistry editorRegistry = PlatformUI.getWorkbench()
						.getEditorRegistry();

				TGContentType contentType = textGridObject
						.getContentType(false);
				if (contentType != null)
					eclipseContentType = contentType.getEclipseContentType();

				editorDescriptor = editorRegistry.getDefaultEditor(
						file.getName(), eclipseContentType);

				if (editorDescriptor != null) {

					// switch to XML-Perspective when the XML-Editor is opened
					// if the current active perspective not the
					// linkEditor-perspective
					if (editorDescriptor
							.getId()
							.equals("info.textgrid.lab.xmleditor.mpeditor.MPEditorPart")) {

						// if the linkeditor perspective active, then add
						// this xml-object to the linkEditor
						if ("info.textgrid.lab.linkeditor.rcp_linkeditor.perspective"
								.equals(PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage().getPerspective()
										.getId())) {
							try {
								handlerService.executeCommand("info.textgrid.lab.linkeditor.rcp_linkeditor.defaultOpen", null);
							} catch (CommandException e) {
								StatusManager.getManager().handle(
										new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Could not open text image link editor.", e));
							}
							return null;
						}

						// if the ttle perspective active, then add
						// this xml-object to the linkEditor
						if ("info.textgrid.lab.ttle.perspective"
								.equals(PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage().getPerspective()
										.getId())) {
							try {
								handlerService.executeCommand("info.textgrid.lab.ttle.defaultOpen", null);
							} catch (CommandException e) {
								StatusManager.getManager().handle(
										new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Could not open text text link editor.", e));
							}
							return null;
						}
						
						try {

							PlatformUI
									.getWorkbench()
									.showPerspective(
											"info.textgrid.lab.welcome.XMLEditorPerspective",
											PlatformUI.getWorkbench()
													.getActiveWorkbenchWindow());
						} catch (WorkbenchException e) {
							IStatus status = new Status(IStatus.ERROR,
									Activator.PLUGIN_ID,
									"Could not open XML Editor Perspective!", e);
							Activator.getDefault().getLog().log(status);
						}

					} else if (editorDescriptor.getId().equals(
							"info.textgrid.lab.core.aggregations.ui.editor")) {

						try {

							PlatformUI
									.getWorkbench()
									.showPerspective(
											"info.textgrid.lab.core.aggregations.ui.AggregationCompositionPerspective",
											PlatformUI.getWorkbench()
													.getActiveWorkbenchWindow());
						} catch (WorkbenchException e) {
							IStatus status = new Status(
									IStatus.ERROR,
									Activator.PLUGIN_ID,
									"Could not open Aggregation Composer Perspective!",
									e);
							Activator.getDefault().getLog().log(status);
						}
						IEditorInput input = (IEditorInput) getAdapter(
								new TGObjectReference(textGridObject.getURI()),
								IEditorInput.class);
						return workbenchPage.openEditor(input,
								editorDescriptor.getId());
					}

					IFileEditorInput input = new FileEditorInput(file);
					
					IEditorPart editorPart = workbenchPage.openEditor(input,
							editorDescriptor.getId());
					
					if (o instanceof FullTextEntry) {
						callSelectByIDCommand((FullTextEntry) o);
					}

					return editorPart;
				} else {
					Activator
							.handleError(
									null,
									"Could not find an editor that can handle a {0}, so I cannot open {1}.",
									contentType, textGridObject);
				}

			} catch (CoreException e) {
				Activator.handleError(e, "Could not open {0}.", textGridObject);
			}

		} else {
			Activator
					.handleError(
							null,
							"Cannot open {0}. The open handler can currently only handle things that can be made a TextGridObject, but it has been called on the {1} {0}, which cannot.",
							o, o == null ? null : o.getClass().getSimpleName());
			return null;
		}

		return null;
	}
	
	/**
	 * Calls the command to position the content of
	 * the XML editor to the given search entry. 
	 * @param entry
	 * 			represents a search hit
	 */
	private static void callSelectByIDCommand(FullTextEntry entry) {
		IHandlerService handlerService = (IHandlerService) PlatformUI
		.getWorkbench().getService(IHandlerService.class);
		
		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getService(ICommandService.class);
		
		Command command = commandService.getCommand("info.textgrid.lab.xmleditor.mpeditor.selectByID");
		
		IParameter parameterID;
		IParameter parameterSearchTerm;
		try {
			parameterID = command.getParameter("info.textgrid.lab.xmleditor.mpeditor.selectByID.ID");
			Parameterization parameter1 = new Parameterization(parameterID, entry.getXpath());
			parameterSearchTerm = command.getParameter("info.textgrid.lab.xmleditor.mpeditor.selectByID.SearchTerm");
			Parameterization parameter2 = new Parameterization(parameterSearchTerm, entry.getMatch());
	 		ParameterizedCommand parmCommand = new ParameterizedCommand(
	 				command, new Parameterization[] { parameter1, parameter2 });
	 		handlerService.executeCommand(
	 				parmCommand,
					null);
		} catch (NotDefinedException e) {
			Activator.handleError(e, "Could not select {0} in XML editor.", entry);
		} catch (ExecutionException e) {
			Activator.handleError(e, "Could not select {0} in XML editor.", entry);
		} catch (NotEnabledException e) {
			Activator.handleError(e, "Could not select {0} in XML editor.", entry);
		} catch (NotHandledException e) {
			Activator.handleError(e, "Could not select {0} in XML editor.", entry);
		}
	}
 }
