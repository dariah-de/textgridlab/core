package info.textgrid.lab.ui.core.handlers;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.ui.core.handlers.messages"; //$NON-NLS-1$
	public static String DeactivateProjectDialog_CheckPermissions;
	public static String DeactivateProjectDialog_DeactivateProject;
	public static String DeactivateProjectDialog_IM_NotAllowed;
	public static String DeactivateProjectDialog_IM_Online;
	public static String DeactivateProjectDialog_IM_OperationFailed;
	public static String DeactivateProjectDialog_IM_OperationNotPermitted;
	public static String DeactivateProjectDialog_IM_OperationNotSucceed;
	public static String DeactivateProjectDialog_IM_OperationPermitted;
	public static String DeactivateProjectDialog_Info1;
	public static String DeactivateProjectDialog_Info2;
	public static String DeleteEmptyProjectDialog_0;
	public static String DeleteEmptyProjectDialog_CheckPermissions;
	public static String DeleteEmptyProjectDialog_CheckPermissionsWait;
	public static String DeleteEmptyProjectDialog_ContainsResources;
	public static String DeleteEmptyProjectDialog_CouldNotDelete;
	public static String DeleteEmptyProjectDialog_DeletePermanently;
	public static String DeleteEmptyProjectDialog_DeletingFailed;
	public static String DeleteEmptyProjectDialog_EM_NotAllowed;
	public static String DeleteEmptyProjectDialog_EM_NotEmpty;
	public static String DeleteEmptyProjectDialog_EM_NotOnline;
	public static String DeleteEmptyProjectDialog_NotPermitted;
	public static String DeleteEmptyProjectDialog_OperationPermitted;
	public static String DeleteEmptyProjectDialog_PublishedResources;
	public static String DeleteHandler_Cancel;
	public static String DeleteHandler_CheckboxLabel1;
	public static String DeleteHandler_CheckboxLabel2;
	public static String DeleteHandler_Delete;
	public static String DeleteHandler_DeleteObjects;
	public static String DeleteHandler_DeleteProjectFile;
	public static String DeleteHandler_Deleting;
	public static String DeleteHandler_DeletingObjects;
	public static String DeleteHandler_EM_AccessDenied;
	public static String DeleteHandler_EM_ContentType;
	public static String DeleteHandler_EM_CrudNotAvailable;
	public static String DeleteHandler_EM_DeleteProjectFile;
	public static String DeleteHandler_EM_DeletingFailed;
	public static String DeleteHandler_EM_ErrorOccured;
	public static String DeleteHandler_EM_NothingToDelete;
	public static String DeleteHandler_EM_Skipped;
	public static String DeleteHandler_IM_DeleteObjects;
	public static String DeleteHandler_IM_DeleteObjectsAndChildren;
	public static String DeleteHandler_IM_DeletingRevision;
	public static String DeleteHandler_IM_WantToDelete;
	public static String DeleteHandler_IM_WantToDelete2;
	public static String DeleteHandler_Title_NothingToDelete;
	public static String DeleteProjectFileHandler_DeleteProjectFile;
	public static String DeleteProjectFileHandler_DeletingFile;
	public static String DeleteProjectFileHandler_FileDeleted;
	public static String DeleteProjectFileHandler_IM_WantToDelete;
	public static String ExportHandler_Canceled;
	public static String ExportHandler_EM_KindOfSelection;
	public static String ExportHandler_EM_MultiStatus;
	public static String ExportHandler_EM_ShouldBePrevented;
	public static String ExportHandler_EM_TargetDirectory;
	public static String ExportHandler_EM_UserRequest;
	public static String ExportHandler_EM_UserRequest2;
	public static String ExportHandler_Export;
	public static String ExportHandler_ExportTo;
	public static String ExportHandler_Failed1;
	public static String ExportHandler_Failed2;
	public static String ExportHandler_Finished;
	public static String ExportHandler_IM_SelectDirectory;
	public static String ExportHandler_NoTGObject;
	public static String ExportHandler_SuccessfullExport;
	public static String NewObject_DialogTitle;
	public static String OpenHandler_CannotOpenSelection;
	public static String OpenHandler_CouldNotOpenIn;
	public static String OpenHandler_MustAdaptToIFile;
	public static String OpenHandler_NonAdaptableObject;
	public static String OpenHandler_NoWillingEditor;
	public static String OpenMDEHandler_CouldNotOpenME;
	public static String OpenURIHandler_CouldNotOpenObject;
	public static String PublishDialog_0;
	public static String PublishDialog_1;
	public static String ReactivateProjectDialog_IM_ListOfProjects;
	public static String ReactivateProjectDialog_IM_ReactivateDeactivated;
	public static String ReactivateProjectDialog_IM_ReactivateOutOf;
	public static String ReactivateProjectDialog_NothingRestored;
	public static String ReactivateProjectDialog_Reactivate;
	public static String ReloadMetadataHandler_JobName;
	public static String ReloadMetadataHandler_MultiStatus;
	public static String ReloadMetadataHandler_NameSubTask;
	public static String ReloadMetadataHandler_StatusMessage;
	public static String SaveLocalCopyHandler_EM_CouldNotSave;
	public static String SaveLocalCopyHandler_EM_NoContentType;
	public static String SaveLocalCopyHandler_EM_SaveNotSupported;
	public static String SaveLocalCopyHandler_SaveAs;
	public static String ShowRawMetadataHandler_JobName;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
