package info.textgrid.lab.ui.core.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;

public class OpenProjectFileHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		MessageDialog
		.openInformation(
				null,
				null,
				"The object you tried to open is a project file, a TextGrid internal helper object that cannot be edited directly.\n"
						+ "Please use the Metadata Template-Editor to edit it!");
		return null;
	}

	

}
