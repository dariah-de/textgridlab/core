package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

public class AddObjectHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		NewObjectWizard wizard = new NewObjectWizard();
		TextGridProject project = null;
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection != null && selection instanceof IStructuredSelection) {
			Object selected = ((IStructuredSelection) selection).getFirstElement();
			if (selected != null && selected instanceof TextGridProject) {
				project = (TextGridProject) selected;
			}
		}
		
		String contentType = event.getParameter("info.textgrid.lab.ui.core.addobject.contenttype"); //$NON-NLS-1$
		
		wizard.showDialog(HandlerUtil.getActiveShell(event), project, TGContentType.getContentType(contentType));
		return null;
	}
}


