package info.textgrid.lab.ui.core.handlers;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.dialogs.OpenURIDialog;
import info.textgrid.lab.ui.core.menus.OpenObjectService;

import java.net.URI;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public class OpenURIHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		OpenURIDialog dialog = new OpenURIDialog(HandlerUtil.getActiveShell(event));

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof ITextSelection) {
			dialog.setInput(((ITextSelection) selection).getText().trim());
		} else if (selection instanceof IStructuredSelection) {
			TextGridObject object = getAdapter(((IStructuredSelection) selection).getFirstElement(), TextGridObject.class);
			if (object != null)
				dialog.setInput(object.getURI().toString());
		}
		// TODO ITextEditor handling

		if (dialog.open() == Dialog.OK) {
			URI uri = dialog.getURI();
			try {
				IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
				if (window == null) {
					window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				}
				GenericOpenHandler.openObject(TextGridObject.getInstance(uri, false));
			} catch (CrudServiceException e) {
				Activator.handleError(e, Messages.OpenURIHandler_CouldNotOpenObject, uri);
			}

		}

		return null;
	}

}
