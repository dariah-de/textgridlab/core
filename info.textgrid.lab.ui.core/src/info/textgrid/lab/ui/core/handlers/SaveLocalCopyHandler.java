package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.text.IDocument;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.ITextEditor;


public class SaveLocalCopyHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		final IEditorPart editorPart = HandlerUtil.getActiveEditor(event);
		final ITextEditor textEditor = AdapterUtils.getAdapter(editorPart, ITextEditor.class);
		if (textEditor == null) {
			final IStatus status = Activator.handleProblem(IStatus.ERROR, null, Messages.SaveLocalCopyHandler_EM_SaveNotSupported, editorPart, editorPart == null? null : editorPart.getClass());
//			ErrorDialog.openError(HandlerUtil.getActiveShell(event), "Save a copy", "Save a copy is currently not supported for the current editor type.", status);
		} else {
			final IEditorInput inputFile = textEditor.getEditorInput();
			final IFile file = AdapterUtils.getAdapter(inputFile, IFile.class);
			final TextGridObject textGridObject = AdapterUtils.getAdapter(file, TextGridObject.class);
			final IDocument document = textEditor.getDocumentProvider().getDocument(inputFile);

			final FileDialog dialog = new FileDialog(HandlerUtil.getActiveShell(event), SWT.SAVE);
			dialog.setText(NLS.bind(Messages.SaveLocalCopyHandler_SaveAs, file));
			dialog.setFileName(inputFile.getName());
			if (textGridObject != null) {
				try {
					TGContentType contentType = null;
					contentType = textGridObject.getContentType(false);
					dialog.setFilterExtensions(new String[] { contentType.getExtension()});
					dialog.setFilterNames(new String[] { contentType.getDescription() });
				} catch (final CoreException e) {
					Activator.handleProblem(IStatus.WARNING, e, Messages.SaveLocalCopyHandler_EM_NoContentType, e.getMessage());
				}
			}

			dialog.setOverwrite(true);
			final String fileName = dialog.open();

			// TODO background job
			if (fileName != null) {
				final File target = new File(fileName).getAbsoluteFile();
				if (target.getParentFile() != null && !target.getParentFile().exists())
					target.getParentFile().mkdirs();
				try {
					final FileOutputStream outputStream = new FileOutputStream(target);
					final OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
					writer.write(document.get());
					writer.flush();
					outputStream.close();
				} catch (final IOException e) {
					Activator.handleError(e, Messages.SaveLocalCopyHandler_EM_CouldNotSave, file, target, e.getMessage());
				}
			}
		}

		return null;
	}

}
