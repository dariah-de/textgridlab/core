package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth.DeactivateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class DeactivateProjectDialog extends Dialog {
	Map<TextGridProject, Boolean> projectsToDeactivate = new HashMap<TextGridProject, Boolean>();
	Map<TextGridProject, String> projectsMessage = new HashMap<TextGridProject, String>();
	Button okButton;
	Job currentJob;
	PortTgextra stub;
	TableViewer tableViewer;
	Label statusLabel;

	private static final String DEACTIVATE_INFO1 = Messages.DeactivateProjectDialog_Info1;
	private static final String DEACTIVATE_INFO2 = Messages.DeactivateProjectDialog_Info2;

	class DeleteTableContentProvider extends LabelProvider {

		public Image getImage(Object element) {
			if (element instanceof TextGridProject) {
				if (projectsToDeactivate.get(element))
					return Activator.getDefault().getImageRegistry()
							.getDescriptor(Activator.COMPLETE_IMAGE_ID)
							.createImage();
				else
					return Activator.getDefault().getImageRegistry()
							.getDescriptor(Activator.FORBIDDEN_IMAGE_ID)
							.createImage();
			}
			return null;
		}

		public String getText(Object element) {
			if (element instanceof TextGridProject) {
				if (projectsMessage.get(element) != null) {
					return element.toString() + " - "
							+ projectsMessage.get(element);
				}
			}
			return element.toString();
		}
	}

	public DeactivateProjectDialog(Shell shell, Object[] obj) {
		super(shell);
		initialise(obj);
	}

	public DeactivateProjectDialog(IShellProvider parentShell, Object[] obj) {
		super(parentShell);
		initialise(obj);
	}

	protected void initialise(Object[] obj) {
		for (int i = 0; i < obj.length; i++) {
			if (obj[i] instanceof TextGridProject) {
				projectsToDeactivate.put((TextGridProject) obj[i], false);
			}
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, true).setFocus();
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		OnlineStatus
				.isOnlineWithNotification(Messages.DeactivateProjectDialog_IM_Online);
		okButton.setEnabled(false);// TODO XXXX should be false, only
									// provisorically enabled for TG-635
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CANCEL_ID) {
			if (currentJob != null) {
				currentJob.cancel();
			}
			this.close();
		} else if (buttonId == IDialogConstants.OK_ID) {

			DeactivateProjectRequest request = new DeactivateProjectRequest();
			request.setAuth(RBACSession.getInstance().getSID(true));
			request.setLog(logsession.getInstance().getloginfo());

			boolean deactivated = true;

			for (TextGridProject p : projectsToDeactivate.keySet()) {
				if (!projectsToDeactivate.get(p))
					continue;
				request.setProject(p.getId());

				try {
					BooleanResponse br = stub.deactivateProject(request);
					deactivated = br.isResult();
				} catch (AuthenticationFault e) {
					Activator.handleError(e,
							Messages.DeactivateProjectDialog_IM_NotAllowed, p);
				}
			}

			if (!deactivated) {
				MessageDialog
						.open(MessageDialog.ERROR,
								null,
								Messages.DeactivateProjectDialog_IM_OperationFailed,
								Messages.DeactivateProjectDialog_IM_OperationNotSucceed,
								SWT.NONE);
			} else {
				AuthBrowser.sessionChanged();
			}
			this.close();

		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Group g = new Group(parent, SWT.NONE);
		g.setLayout(new GridLayout());

		tableViewer = new TableViewer(g, SWT.BORDER | SWT.V_SCROLL);
		tableViewer.setLabelProvider(new DeleteTableContentProvider());
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setSorter(new ViewerSorter());
		tableViewer.setInput(projectsToDeactivate.keySet());
		GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false);
		gd.heightHint = 100;
		gd.widthHint = 500;
		tableViewer.getControl().setLayoutData(gd);

		statusLabel = new Label(g, SWT.NONE);
		statusLabel.setText(DEACTIVATE_INFO1 + DEACTIVATE_INFO2);

		getShell().setText(Messages.DeactivateProjectDialog_DeactivateProject);

		launchCheckJob(projectsToDeactivate);

		return g;
	}

	private void launchCheckJob(final Map<TextGridProject, Boolean> projectMap) {
		currentJob = new Job(Messages.DeactivateProjectDialog_CheckPermissions) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Messages.DeactivateProjectDialog_CheckPermissions,
						IProgressMonitor.UNKNOWN);

				stub = TextGridProject.ensureStubIsLoaded();
				monitor.worked(1);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;

				boolean projectToDeactivate = false;

				for (final TextGridProject project : projectMap.keySet()) {
					boolean canDelete = true;
					String whyNot = "";

					if (!project.iAmLeader()) {
						canDelete = false;
						whyNot = NLS.bind(Messages.DeactivateProjectDialog_IM_OperationNotPermitted,
								TextGridProject
										.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER));
					}

					monitor.worked(1);
					if (monitor.isCanceled()
							|| tableViewer.getControl().isDisposed()) {
						return Status.CANCEL_STATUS;
					}

					if (canDelete) {
						projectsToDeactivate.put(project, true);
						projectsMessage.put(project, Messages.DeactivateProjectDialog_IM_OperationPermitted);
						projectToDeactivate = true;
					} else {
						projectsMessage.put(project, whyNot);
					}

					tableViewer.getControl().getDisplay()
							.asyncExec(new Runnable() {
								public void run() {
									tableViewer.refresh(project);
									tableViewer.reveal(project);
								}
							});
				}

				final boolean deactivate = projectToDeactivate;
				tableViewer.getControl().getDisplay().asyncExec(new Runnable() {
					public void run() {
						if (deactivate)
							okButton.setEnabled(true);
						statusLabel.setText(DEACTIVATE_INFO1);
					}
				});

				return Status.OK_STATUS;
			}
		};
		currentJob.setPriority(Job.LONG);
		currentJob.schedule();
	}

}
