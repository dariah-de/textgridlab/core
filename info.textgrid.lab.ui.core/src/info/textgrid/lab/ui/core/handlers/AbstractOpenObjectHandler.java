package info.textgrid.lab.ui.core.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.ui.IEditorPart;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.locking.LockingPartListener;
import info.textgrid.lab.ui.core.locking.LockingService;

/**
 * Abstract class for custom open handlers in TextGridLab.
 * <p>
 * If you use the extension point <var>info.textgrid.lab.ui.core.openObject</var> 
 * your handler should inherit form this class. So you can use the functionality
 * to support locking for the opened TextGrid object.
 * </p>
 * @author Frank Queens
 *
 */
public abstract class AbstractOpenObjectHandler extends AbstractHandler {

	@SuppressWarnings("unused")
	private void lockObject(TextGridObject object) {
		LockingService lockingService = LockingService.getInstance();
		if (!lockingService.lockObject(object, true))
			object.setOpenAsReadOnly(true);
	}
	
	@SuppressWarnings("unused")
	private void addEditorPartListener(IEditorPart editorPart, TextGridObject object) {
		editorPart.getSite().getPage().addPartListener(new LockingPartListener(object));
	}
}
