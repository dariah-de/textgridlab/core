package info.textgrid.lab.ui.core.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

public class ReactivateProjectHandler extends AbstractHandler implements
		IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ReactivateProjectDialog rpDialog = new ReactivateProjectDialog(
				HandlerUtil.getActiveShell(event));
		rpDialog.open();

		return null;
	}

}
