package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

public class ReloadMetadataHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection != null && selection instanceof IStructuredSelection) {
			final TextGridObject[] objects = AdapterUtils.getAdapters(
					((IStructuredSelection) selection).toArray(),
					TextGridObject.class, false);
			Job reloadJob = new Job(NLS.bind(
					Messages.ReloadMetadataHandler_JobName,
					objects.length)) {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					SubMonitor progress = SubMonitor.convert(monitor,
							objects.length);
					MultiStatus result = new MultiStatus(Activator.PLUGIN_ID,
							0, NLS.bind(
									Messages.ReloadMetadataHandler_MultiStatus,
									objects.length), null);

					for (TextGridObject object : objects) {
						progress.subTask(NLS.bind(
								Messages.ReloadMetadataHandler_NameSubTask, object));
						try {
							object.reloadMetadata(true);
							object.notifyMetadataEditor();
							
							result
									.add(new Status(
											IStatus.OK,
											Activator.PLUGIN_ID,
											NLS
													.bind(
															Messages.ReloadMetadataHandler_StatusMessage,
															object)));
						} catch (CrudServiceException e) {
							result.add(e.getStatus());
						}
						progress.worked(1);
					}
					progress.done();
					StatusManager.getManager().handle(result);
					
					return result;
				}
			};
			reloadJob.setUser(true);
			reloadJob.schedule();
		}

		return null;
	}

}
