package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth.GetDeactivatedProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetDeactivatedProjectsResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;
import info.textgrid.namespaces.middleware.tgauth.ReactivateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.UnknownProjectFault;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * Reactivate a previously deactivated TextGridProject. Modal Dialogue. Accesses
 * RBAC directly.
 * 
 * @author martin
 */
public class ReactivateProjectDialog extends TrayDialog {
	Group g;
	Label statusLabel;
	Button closeButton;
	Button reactivateButton;
	TableViewer projectViewer;

	public ReactivateProjectDialog(Shell shell) {
		super(shell);
		// TODO Auto-generated constructor stub
		initialize();
	}

	public ReactivateProjectDialog(IShellProvider parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
		initialize();
	}

	protected void initialize() {
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		reactivateButton = createButton(parent, -123, Messages.ReactivateProjectDialog_Reactivate, true);
		// reactivateButton.setFocus();
		closeButton = createButton(parent, IDialogConstants.CLOSE_ID,
				IDialogConstants.CLOSE_LABEL, false);
		closeButton.setFocus();
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CLOSE_ID)
			this.close();
		else if (buttonId == -123) {
			ArrayList<TextGridProject> toBeReactivated = getTextGridProjectsForReactivation();
			ArrayList<TextGridProject> success = new ArrayList<TextGridProject>();
			ArrayList<TextGridProject> failed = new ArrayList<TextGridProject>();

			try {
				PortTgextra tgextra = TgAuthClientUtilities
						.getTgAuthServiceStub();
				String me = RBACSession.getInstance().getSID(false);

				for (TextGridProject p : toBeReactivated) {
					ReactivateProjectRequest rpa = new ReactivateProjectRequest();
					rpa.setAuth(me);
					rpa.setLog("");
					rpa.setProject(p.getId());
					// System.out.println("Reactivating " + p.getId() + "...");
					BooleanResponse br = tgextra.reactivateProject(rpa);
					if (br.isResult()) {
						success.add(p);
					} else {
						failed.add(p);
					}
				}
			} catch (AuthenticationFault e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownProjectFault e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				informUser(NLS.bind(Messages.ReactivateProjectDialog_IM_ReactivateOutOf,
						success.size(), toBeReactivated.size()));
				if (success.size() > 0) {
					TextGridProjectRoot.getInstance().resetProjects();
					projectViewer.setInput(getDeactivatedProjects());
				}
				projectViewer.getTable().deselectAll();
			}
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 300;
		parentGD.widthHint = 400;
		parentGD.verticalAlignment = GridData.FILL;

		g = new Group(parent, SWT.NONE);
		g.setLayout(new GridLayout(1, false));
		g.setText(Messages.ReactivateProjectDialog_IM_ReactivateDeactivated);

		Label hint = new Label(g, SWT.WRAP);
		hint.setText(NLS
				.bind(Messages.ReactivateProjectDialog_IM_ListOfProjects,
						TextGridProject
								.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)));

		projectViewer = new TableViewer(g, SWT.MULTI | SWT.V_SCROLL
				| SWT.H_SCROLL | SWT.VIRTUAL | SWT.BORDER);
		GridData projectTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
		projectTreeData.heightHint = 200;
		projectViewer.getTable().setLayoutData(projectTreeData);

		projectViewer.setLabelProvider(new TGODefaultLabelProvider(true));
		projectViewer.setContentProvider(new ArrayContentProvider());

		projectViewer.setInput(getDeactivatedProjects());
		// projectViewer.addSelectionChangedListener(this);

		Composite c = new Composite(g, SWT.FILL);
		GridData cGD = new GridData();
		cGD.horizontalAlignment = GridData.FILL;
		cGD.grabExcessVerticalSpace = true;
		cGD.grabExcessHorizontalSpace = true;
		cGD.heightHint = 100;
		cGD.widthHint = 400;
		cGD.verticalAlignment = GridData.FILL;
		c.setLayout(new GridLayout(2, false));
		c.setLayoutData(cGD);

		Label stLabel = new Label(c, SWT.LEFT);
		stLabel.setText("Status: ");
		statusLabel = new Label(c, SWT.WRAP);

		GridData stGD = new GridData();
		stGD.horizontalAlignment = GridData.FILL;
		stGD.grabExcessVerticalSpace = true;
		stGD.grabExcessHorizontalSpace = true;
		stGD.heightHint = 80;
		stGD.widthHint = 200;
		stGD.verticalAlignment = GridData.FILL;

		statusLabel.setLayoutData(stGD);
		statusLabel.setText(Messages.ReactivateProjectDialog_NothingRestored);
		statusLabel
				.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));

		return g;
	}

	private ArrayList<TextGridProject> getDeactivatedProjects() {
		ArrayList<TextGridProject> allDeactivatedProjects = new ArrayList<TextGridProject>();
		try {
			PortTgextra tgextra = TgAuthClientUtilities.getTgAuthServiceStub();
			;
			GetDeactivatedProjectsRequest gdpr = new GetDeactivatedProjectsRequest();
			gdpr.setAuth(RBACSession.getInstance().getSID(true));
			gdpr.setLog("");
			GetDeactivatedProjectsResponse response = tgextra
					.getDeactivatedProjects(gdpr);
			List<ProjectInfo> pis = response.getProject();

			if (pis != null)
				for (ProjectInfo pi : pis) {
					// System.out.println(pi.getId());
					allDeactivatedProjects.add(TextGridProject
							.getProjectInstance(pi));
				}
		} catch (RemoteException e) {
			Activator.handleError(e, "Caught Remote Exception", e.getMessage());
		}
		return allDeactivatedProjects;
	}

	private ArrayList<TextGridProject> getTextGridProjectsForReactivation() {
		ArrayList<TextGridProject> rp = new ArrayList<TextGridProject>();
		IStructuredSelection spss = (IStructuredSelection) projectViewer
				.getSelection();
		Iterator<?> i = spss.iterator();
		while (i.hasNext()) {
			Object o = i.next();
			if (o instanceof TextGridProject) {
				rp.add((TextGridProject) o);
			}
		}
		return rp;
	}

	public void informUser(String message) {
		statusLabel.setText(message);
		statusLabel.pack();
	}

}
