package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth.DeleteProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.GetNumberOfResourcesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetNumberOfResourcesResponse;
import info.textgrid.namespaces.middleware.tgauth.NotEmptyFault;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class DeleteEmptyProjectDialog extends TrayDialog {
	Map<TextGridProject, Boolean> projectsDelete = new HashMap<TextGridProject, Boolean>();
	Map<TextGridProject, String> projectsMessage = new HashMap<TextGridProject, String>();
	Button okButton;
	boolean okEnabled = false;
	PortTgextra stub = null;
	Job currentJob;
	TableViewer tableViewer;
	Label statusLabel;
	
	class DeleteTableContentProvider extends LabelProvider {

		public Image getImage(Object element) {
			if (element instanceof TextGridProject) {
				if (projectsDelete.get(element))
					return Activator.getDefault()
					.getImageRegistry()
					.getDescriptor(Activator.COMPLETE_IMAGE_ID).createImage();
				else 
					return Activator.getDefault()
					.getImageRegistry()
					.getDescriptor(Activator.FORBIDDEN_IMAGE_ID).createImage();
			}
			return null;
		}
		
		public String getText(Object element) {
			if (element instanceof TextGridProject) {
				if (projectsMessage.get(element) != null) {
					return element.toString() + Messages.DeleteEmptyProjectDialog_0 + projectsMessage.get(element);
				}
			}
			return element.toString();
		}		
	}

	public DeleteEmptyProjectDialog(Shell shell, Object[] obj) {
		super(shell);
		initialise(obj);
	}

	public DeleteEmptyProjectDialog(IShellProvider parentShell,
			Object[] obj) {
		super(parentShell);
		initialise(obj);
	}

	protected void initialise(Object[] obj) {
		for (int i=0; i < obj.length; i++) {
			if (obj[i] instanceof TextGridProject) {
				projectsDelete.put((TextGridProject)obj[i], false);
			}
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, true).setFocus();
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		OnlineStatus
				.isOnlineWithNotification(Messages.DeleteEmptyProjectDialog_EM_NotOnline);
		okButton.setEnabled(false); // TODO XXXX should be false, only provisorically enabled for TG-635
	}

	@Override
	protected void okPressed() {
		// do nothing
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CANCEL_ID) {
			if (currentJob != null) {
				currentJob.cancel();
			}
			this.close();
		} else if (buttonId == IDialogConstants.OK_ID) {

			DeleteProjectRequest request = new DeleteProjectRequest();
			request.setAuth(RBACSession.getInstance().getSID(true));
			request.setLog(logsession.getInstance().getloginfo());
			
			boolean deleted = true;
			TextGridProject currentProject = null;
			
			for (TextGridProject p : projectsDelete.keySet()) {
				if (!projectsDelete.get(p))
					continue;
				request.setProject(p.getId());
				currentProject = p;
	
				try {
					BooleanResponse br = stub.deleteProject(request);
					deleted = br.isResult();
				} catch (AuthenticationFault e) {
					Activator.handleError(e,
							Messages.DeleteEmptyProjectDialog_EM_NotAllowed, projectsDelete);
					deleted = false;
				} catch (NotEmptyFault e) {
					Activator.handleError(e, Messages.DeleteEmptyProjectDialog_EM_NotEmpty, projectsDelete);
					deleted = false;
				}
			}	

			if (!deleted) {
				MessageDialog
						.open(MessageDialog.ERROR,
								null,
								NLS.bind(Messages.DeleteEmptyProjectDialog_DeletingFailed, currentProject.getName()),
								NLS.bind(Messages.DeleteEmptyProjectDialog_CouldNotDelete, currentProject.getName()),
								SWT.NONE);
			} else {
				AuthBrowser.sessionChanged();
			}
			this.close();

		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Group g = new Group(parent, SWT.NONE);
		g.setLayout(new GridLayout(1, false));
		
		tableViewer = new TableViewer(g, SWT.BORDER | SWT.V_SCROLL);
		tableViewer.setLabelProvider(new DeleteTableContentProvider());
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setSorter(new ViewerSorter());
		tableViewer.setInput(projectsDelete.keySet());
		GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false);
		gd.heightHint = 100;
		gd.widthHint = 500;
		tableViewer.getControl().setLayoutData(gd);
		
		statusLabel = new Label(g, SWT.NONE);
		statusLabel.setText(Messages.DeleteEmptyProjectDialog_CheckPermissionsWait);
		
		getShell().setText(Messages.DeleteEmptyProjectDialog_DeletePermanently);

		launchCheckJob(projectsDelete); // this one checks, sets checkPerm Text and
								// enables the okButton if everything is o.k.

		return g;
	}

	private void launchCheckJob(final Map<TextGridProject, Boolean> projectMap) {
		currentJob = new Job(Messages.DeleteEmptyProjectDialog_CheckPermissions) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Messages.DeleteEmptyProjectDialog_CheckPermissions,
						IProgressMonitor.UNKNOWN);

				stub = TextGridProject.ensureStubIsLoaded();
				monitor.worked(1);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				
				boolean projectToDelete = false;
				
				for (final TextGridProject project : projectMap.keySet()) {
					boolean canDelete = true;
					String whyNot = ""; //$NON-NLS-1$

					if (!project.iAmLeader()) {
						canDelete = false;
						whyNot = NLS.bind(Messages.DeleteEmptyProjectDialog_NotPermitted,
								TextGridProject
										.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER));
					}
					monitor.worked(1);
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
	
					if (canDelete) { // need to check further
						GetNumberOfResourcesRequest gnr = new GetNumberOfResourcesRequest();
						gnr.setAuth(RBACSession.getInstance().getSID(false));
						gnr.setLog(logsession.getInstance().getloginfo());
						gnr.setProject(project.getId());
	
						GetNumberOfResourcesResponse number = new GetNumberOfResourcesResponse();
						number = stub.getNumberOfResources(gnr);
	
						if (number.getPublicresources() > 0) {
							canDelete = false;
							whyNot = Messages.DeleteEmptyProjectDialog_PublishedResources;
						} else if (number.getAllresources() > 0) {
							canDelete = false;
							whyNot = NLS.bind(Messages.DeleteEmptyProjectDialog_ContainsResources,
									number.getAllresources());
						}
					}
	
					monitor.worked(1);
					if (monitor.isCanceled() || tableViewer.getControl().isDisposed()) {
						return Status.CANCEL_STATUS;
					}
					
					if (canDelete) {
						projectsDelete.put(project, true);
						projectsMessage.put(project, Messages.DeleteEmptyProjectDialog_OperationPermitted);
						projectToDelete = true;
					} else {
						projectsMessage.put(project, whyNot);
					}
					
				    tableViewer.getControl().getDisplay().asyncExec(new Runnable() {
						public void run() {
							tableViewer.refresh(project);
							tableViewer.reveal(project);
						}
					});
				}	
				
				final boolean delete = projectToDelete;
				tableViewer.getControl().getDisplay().asyncExec(new Runnable() {
					public void run() {
						if (delete)
							okButton.setEnabled(true);
						statusLabel.setText(""); //$NON-NLS-1$
					}
				});

				return Status.OK_STATUS;
			}
		};
		currentJob.setPriority(Job.LONG);
		currentJob.schedule();
	}
}
