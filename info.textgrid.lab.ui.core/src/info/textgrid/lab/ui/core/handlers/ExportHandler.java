/**
 * 
 */
package info.textgrid.lab.ui.core.handlers;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapters;
import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 * 
 */
@Deprecated
public class ExportHandler extends AbstractHandler implements IHandler {

	private static class ExportRunnable implements IRunnableWithProgress {

		private String targetPath;
		private TextGridObject[] textGridObjects;
		private MultiStatus status = new MultiStatus(
				Activator.PLUGIN_ID,
				0,
				Messages.ExportHandler_EM_MultiStatus,
				null);

		public ExportRunnable(String targetPath,
				TextGridObject[] textGridObjects) {
			this.targetPath = targetPath;
			this.textGridObjects = textGridObjects;
		}

		public void run(IProgressMonitor monitor)
				throws InvocationTargetException, InterruptedException {
			SubMonitor progress = SubMonitor.convert(monitor,
					MessageFormat
					.format(Messages.ExportHandler_ExportTo, targetPath),
					10 + 100 * textGridObjects.length);
			File targetDir = new File(targetPath);
			if (!targetDir.isDirectory())
				if (!targetDir.mkdirs())
					throw new InvocationTargetException(
							new IOException(
									MessageFormat
											.format(
													Messages.ExportHandler_EM_TargetDirectory,
													targetDir)));
			progress.worked(10);
			
			for (TextGridObject textGridObject : textGridObjects) {
				if (progress.isCanceled())
					throw new InterruptedException(Messages.ExportHandler_EM_UserRequest);
				
				progress.subTask(MessageFormat.format(Messages.ExportHandler_Export,
						textGridObject));
				
				IFile file = getAdapter(textGridObject, IFile.class);
				progress.worked(10);

				try {
					File targetFile = new File(targetDir, textGridObject.getURIBasedName());
					progress.subTask(MessageFormat.format(Messages.ExportHandler_Export,
							textGridObject)); // we know the title by now
					IFileStore remoteStore = EFS
							.getStore(file.getLocationURI());
					IFileStore localStore = EFS.getLocalFileSystem()
							.fromLocalFile(targetFile);
					progress.worked(10);
					remoteStore.copy(localStore, EFS.OVERWRITE, progress
							.newChild(70));
					
					if (progress.isCanceled())
						throw new InterruptedException(
								Messages.ExportHandler_EM_UserRequest2);

					File metaFile = new File(targetDir, "."
							+ textGridObject.getURIBasedName().concat(".meta"));//$NON-NLS-0$ //$NON-NLS-1$
					BufferedOutputStream metaStream = new BufferedOutputStream(
							new FileOutputStream(metaFile));
					textGridObject.getMetadataXML().serialize(metaStream);
					metaStream.close();
					progress.worked(10);
					
					status.add(new Status(IStatus.OK, Activator.PLUGIN_ID,
							MessageFormat.format(
									Messages.ExportHandler_SuccessfullExport,
									textGridObject, targetFile)));

				} catch (CoreException e) {
					status.add(exportFailed(e, textGridObject));
				} catch (FileNotFoundException e) {
					status.add(exportFailed(e, textGridObject));
				} catch (XMLStreamException e) {
					status.add(exportFailed(e, textGridObject));
				} catch (IOException e) {
					status.add(exportFailed(e, textGridObject));
				}
				
			} // for loop over textGridObjects

			if (status.getSeverity() >= IStatus.WARNING)
				throw new InvocationTargetException(new CoreException(status));
		}
		
		private IStatus exportFailed(Throwable e, TextGridObject textGridObject) {
			if (e instanceof CoreException) {
				CoreException ce = (CoreException) e;
				return new MultiStatus(Activator.PLUGIN_ID, 0,
						new IStatus[] { ce.getStatus() }, MessageFormat
						.format(Messages.ExportHandler_Failed1,
								textGridObject), e);
			}
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, MessageFormat
					.format(Messages.ExportHandler_Failed2, textGridObject), e);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		ISelection sel = HandlerUtil.getCurrentSelection(event);
		if (sel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) sel;
			TextGridObject[] textGridObjects = getAdapters(selection.toArray(),
					TextGridObject.class, false);
			if (textGridObjects.length == 0) {
				throw new ExecutionException(
						MessageFormat
								.format(
										Messages.ExportHandler_NoTGObject +
										Messages.ExportHandler_EM_ShouldBePrevented,
										selection));
			}
			
			// Target directory
			Shell shell = HandlerUtil
					.getActiveShell(event);
			exportObjects(shell, textGridObjects);
						
		} else {
			throw new ExecutionException(MessageFormat.format(Messages.ExportHandler_EM_KindOfSelection +
					Messages.ExportHandler_EM_ShouldBePrevented,
									sel)); 
		}
		
		
		return null;
	}

	/**
	 * Opens a dialog querying for a target directory and exports the given
	 * TextGridObject(s) and their metadata there.
	 * 
	 * @param shell
	 * @param textGridObjects
	 */
	public static void exportObjects(final Shell shell, final TextGridObject... textGridObjects) {
		final DirectoryDialog dialog = new DirectoryDialog(shell);
		dialog.setText("Export");
		dialog.setMessage(Messages.ExportHandler_IM_SelectDirectory);
		final String targetPath = dialog.open();
		if (targetPath == null)
			return; // cancelled

		final IProgressService progressService = PlatformUI.getWorkbench().getProgressService();

		final ExportRunnable exportRunnable = new ExportRunnable(targetPath, textGridObjects);
		try {
			progressService.run(true, true, exportRunnable);
		} catch (final InvocationTargetException e) {
			if (e.getCause() instanceof CoreException)
				StatusManager.getManager().handle(((CoreException) e.getCause()).getStatus(),
						StatusManager.LOG | StatusManager.SHOW);
		} catch (final InterruptedException e) {
			Activator.handleProblem(IStatus.INFO, e, Messages.ExportHandler_Canceled);
		}
		if (exportRunnable.status.getSeverity() < IStatus.WARNING)
			StatusManager.getManager().handle(
					new MultiStatus(Activator.PLUGIN_ID, 0, exportRunnable.status.getChildren(), Messages.ExportHandler_Finished, null));
	}

}
