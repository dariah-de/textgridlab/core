/**
 * 
 */
package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.model.AggregationReader;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.MessageDialogWithCheckbuttons;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.Sets;

/**
 * Creates a job that deletes the selected TextGridObjects. There is no security
 * net. Har har.
 * 
 * @author tv
 */
public class DeleteHandler extends AbstractHandler implements IHandler {

	private final class DeleteJob extends Job {
		private final TextGridObject[] textGridObjects;
		private final boolean recurse;
		private final boolean deleteRevisions;
		private final Set<TextGridObject> handled;

		private DeleteJob(final String name, final TextGridObject[] textGridObjects, final boolean recurse,
				final boolean deleteRevisions) {
			super(name);
			this.textGridObjects = textGridObjects;
			this.recurse = recurse;
			this.deleteRevisions = deleteRevisions;
			this.handled = Sets.newHashSetWithExpectedSize(textGridObjects.length);
		}

		@Override
		public IStatus run(final IProgressMonitor monitor) {

			final SubMonitor progress = SubMonitor.convert(monitor,
					NLS.bind(recurse ? Messages.DeleteHandler_IM_DeleteObjectsAndChildren : Messages.DeleteHandler_IM_DeleteObjects,
							textGridObjects.length),
							textGridObjects.length * 100);

			final LinkedList<IStatus> errors = new LinkedList<IStatus>();

			for (final TextGridObject tgo : textGridObjects) {
				if (progress.isCanceled())
					return Status.CANCEL_STATUS;

				delete(tgo, null, errors, progress.newChild(100));
			}
			if (errors.size() == 1) {
				final MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, IStatus.ERROR, errors.toArray(new IStatus[0]),
						errors.get(0).getMessage(), null);
				Activator.getDefault().getLog().log(multiStatus);
				return multiStatus;
			} else if (errors.size() > 1) {
				final MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, IStatus.ERROR, errors.toArray(new IStatus[0]),
						Messages.DeleteHandler_EM_ErrorOccured, null);
				Activator.getDefault().getLog().log(multiStatus);
				return multiStatus;
			}
			return Status.OK_STATUS;
		}

		/**
		 * Deletes <var>object</var> and optionally aggregated stuff if
		 * {@link #recurse} is true.
		 * 
		 * @param object
		 *            the object to delete
		 * @param project
		 *            if non-<code>null</code>, delete only if the object is
		 *            within that project. If this is <code>null</code> and
		 *            {@link #recurse} is true, recursive calls will have the
		 *            <var>object</var>'s project in this place.
		 * @param errors
		 *            MultiStatus to log errors.
		 * @param progress
		 *            progress monitor.
		 */
		private void delete(final TextGridObject object, final TextGridProject project, final LinkedList<IStatus> errors, final SubMonitor monitor) {
			final SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(Messages.DeleteHandler_Deleting, object), 10);
			try {
				//TG-1845
				if (progress.isCanceled())
					return;
				if (handled.contains(object)) {
					// Don't delete it twice, cf. TG-1742
					StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, MessageFormat.format("Skipping {0} ({1}), already handled", object, object.getContentType(false)))); //$NON-NLS-1$
					progress.worked(10);
					return;
				} else {
					handled.add(object);
				}
				if (project != null && !project.equals(object.getProjectInstance())) {
					errors.add(new Status(IStatus.INFO, Activator.PLUGIN_ID, NLS.bind(
							Messages.DeleteHandler_EM_Skipped, object, object.getProjectInstance())));
				} else {
					if (recurse && object.getContentTypeID().contains("tg.aggregation")) { //$NON-NLS-1$
						progress.setWorkRemaining(30);
						final List<TGObjectReference> aggregates = AggregationReader.list(object, true);
						progress.worked(5);
						progress.setWorkRemaining(10 + 10 * aggregates.size());
						for (final TGObjectReference aggregate : aggregates)
							delete(aggregate.getTgo(), object.getProjectInstance(), errors,
									progress.newChild(10));
					}
					// TG-1232
					if (object.isPublic()) {
						throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
								NLS.bind(Messages.DeleteHandler_EM_AccessDenied,
										object.getTitle(), object.getURI().toString())));
					}
					if (deleteRevisions) {
						final SearchRequest searchRequest = new SearchRequest();
						searchRequest.setQueryRevision(object.getURI().toString());
						final Response result = searchRequest.execute(progress);
						final Iterator<ResultType> revisions = result.getResult().iterator();
						final SubMonitor revisionProgress = progress.newChild(result.getResult().size());
						revisionProgress.setTaskName(NLS.bind(Messages.DeleteHandler_IM_DeletingRevision, object));
						while (revisions.hasNext()) {
							final ResultType revision = revisions.next();
							if (revision.getTextgridUri() != null) {
								//#10802
								final TextGridObject textGridObject = TextGridObject.getInstance(
										new URI(/*"textgrid:" + */revision.getTextgridUri()), false);
								textGridObject.delete(progress);
								handled.add(textGridObject);
							} //$NON-NLS-1$
							revisionProgress.worked(1);
						}
					} else {
						object.delete(progress.newChild(10));
						handled.add(object);
					}
				}
			} catch (final CoreException e) {
				// We're not logging this explicitly ...
				errors.add(new Status(e.getStatus().getSeverity(), Activator.PLUGIN_ID, MessageFormat.format(
						Messages.DeleteHandler_EM_DeletingFailed, object, e.getMessage()), e));
			} catch (final Exception e) {
				errors.add(new Status(IStatus.ERROR, Activator.PLUGIN_ID, MessageFormat.format(
						Messages.DeleteHandler_EM_CrudNotAvailable, object, e.getMessage()), e));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		Job deleteJob = null;
		boolean recursively = false;
		boolean deleteRevisions = false;
		final ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			final IStructuredSelection sel = (IStructuredSelection) selection;
			final Object[] objects = sel.toArray();
			final TextGridObject[] textGridObjects = AdapterUtils.getAdapters(
					objects, TextGridObject.class, false);

			final String message;
			if (textGridObjects.length == 1)
				message = NLS.bind(Messages.DeleteHandler_IM_WantToDelete, textGridObjects);
			else
				message = NLS.bind(Messages.DeleteHandler_IM_WantToDelete2,
						textGridObjects.length);

			if (textGridObjects.length < 1) {
				MessageDialog
				.openError(
						HandlerUtil.getActiveShell(event),
						Messages.DeleteHandler_Title_NothingToDelete,
						Messages.DeleteHandler_EM_NothingToDelete);
				return null;
			} else {
				final MessageDialogWithCheckbuttons dialog = new MessageDialogWithCheckbuttons(
						HandlerUtil.getActiveShell(event),
						Messages.DeleteHandler_DeleteObjects,
						null,
						message,
						MessageDialog.QUESTION,
						new String[] { Messages.DeleteHandler_Cancel, Messages.DeleteHandler_Delete },
						1,
						new String[] {Messages.DeleteHandler_CheckboxLabel1,
							Messages.DeleteHandler_CheckboxLabel2});

				final int doDelete = dialog.open();
				if (doDelete < 1)
					return null; // Cancel
				recursively = dialog.getCheckButtonState(0);
				deleteRevisions = dialog.getCheckButtonState(1);
			}

			for (final TextGridObject tgo : textGridObjects) {
				try {
					final TGContentType contentType = tgo.getContentType(true);
					if (contentType != null) {
						if (contentType.equals(TGContentType.getContentType("text/tg.projectfile+xml"))) { //$NON-NLS-1$
							if (!MessageDialog
									.openConfirm(
											HandlerUtil.getActiveShell(event),
											Messages.DeleteHandler_DeleteProjectFile,
											NLS.bind(Messages.DeleteHandler_EM_DeleteProjectFile, tgo.getTitle())))
								return null;
						}
					}
				} catch (final CoreException e) {
					Activator.handleError(e,
							Messages.DeleteHandler_EM_ContentType, tgo);
				}
			}

			deleteJob = new DeleteJob(Messages.DeleteHandler_DeletingObjects, textGridObjects, recursively, deleteRevisions);

			deleteJob.setUser(true);
			deleteJob.schedule();
		}
		return deleteJob;
	}
}
