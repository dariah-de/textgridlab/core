package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.AbstractOpenHandler;
import info.textgrid.lab.ui.core.IOpenHandler;
import info.textgrid.lab.ui.core.OpenHandlerContribution;

import org.eclipse.core.runtime.CoreException;

public final class DefaultOpenHandler extends AbstractOpenHandler implements
		IOpenHandler {

	public DefaultOpenHandler(OpenHandlerContribution contribution) {
		setContribution(contribution);
	}
	
	public DefaultOpenHandler() {}

	@Override
	public void open(TextGridObject textGridObject) throws CoreException {
		openEditor(textGridObject);
		showPerspective();
	}
}
