package info.textgrid.lab.ui.core.handlers;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.AllMetadataDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.WorkbenchJob;

public class ShowRawMetadataHandler extends AbstractHandler implements IHandler {

	/**
	 * Opens the metadata dialog. Uses a workbench job to enable multiple instances
	 */
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		// Currently handles only one item
		ISelection selection = HandlerUtil.getCurrentSelectionChecked(event);
		final String jobDeclaration = Messages.ShowRawMetadataHandler_JobName;
		if (selection instanceof IStructuredSelection) {
			final TextGridObject textGridObject = getAdapter(
					((IStructuredSelection) selection).getFirstElement(),
					TextGridObject.class);
			WorkbenchJob job = new WorkbenchJob(jobDeclaration) {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					monitor.beginTask(jobDeclaration, 4);
					AllMetadataDialog.show(textGridObject, HandlerUtil
							.getActiveShell(event));
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			job.setPriority(Job.INTERACTIVE);
			job.schedule();
		} 
		
		return null;
	}

}
