package info.textgrid.lab.ui.core.handlers;


import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public class DeleteEmptyProjectHandler extends AbstractHandler implements IHandler {
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			Object[] obj = ((IStructuredSelection) selection).toArray();
			DeleteEmptyProjectDialog dpd = new DeleteEmptyProjectDialog(PlatformUI
					.getWorkbench().getActiveWorkbenchWindow()
					.getShell(), obj);
			dpd.open();
		}
		return null;
	}

}
