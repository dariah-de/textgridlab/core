package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.IOpenHandler;
import info.textgrid.lab.ui.core.OpenHandlerContribution;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

public class ContributableOpenHandler extends AbstractHandler implements
		IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelectionChecked(event);
		if (selection instanceof IStructuredSelection) {
			MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, 0, "Some objects could not be opened.", null);
			TextGridObject[] objects = AdapterUtils.getAdapters(((IStructuredSelection) selection).toArray(), TextGridObject.class, false);
			for (TextGridObject object : objects) {
				try {
					openObject(object);	
				} catch (CoreException e) {
					multiStatus.add(e.getStatus());
				}
			}
			if (multiStatus.getSeverity() >= IStatus.WARNING)
				StatusManager.getManager().handle(multiStatus);
			if (multiStatus.getSeverity() >= IStatus.ERROR) {
				String title;
				String message;
				// TODO provide more information
					title = "Could not open some objects.";
					message = "Opening some or all of the objects you selected failed. Please see the details for an overview of what went wrong with which object.";
					
				ErrorDialog errorDialog = new ErrorDialog(HandlerUtil.getActiveShell(event), title, message, multiStatus, 0);
				errorDialog.setBlockOnOpen(false);
				errorDialog.open();
			}
		}
		return null;
	}
	
	public static void openObject(TextGridObject object) throws CoreException {
		final OpenHandlerContribution contribution = OpenHandlerContribution.bestContributionFor(object.getContentType(false));
		IOpenHandler handler = null;
		if (contribution != null)
			handler = contribution.getHandler();
		if (handler == null)
			handler = new DefaultOpenHandler(contribution);
		handler.open(object);
	}

}
