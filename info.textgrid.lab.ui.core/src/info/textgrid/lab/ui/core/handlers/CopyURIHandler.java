/**
 * 
 */
package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.core.model.TextGridObject;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Copies a newline-separated list of URIs of the currently selected objects to
 * the clipboard.
 * 
 * @author tv
 */
public class CopyURIHandler extends AbstractHandler implements IHandler {

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		ISelection sel = HandlerUtil.getCurrentSelectionChecked(event);
		StringBuilder text = new StringBuilder();
		
		if (sel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) sel;
			Iterator<?> iterator = selection.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof IAdaptable) {
					TextGridObject object = (TextGridObject) ((IAdaptable) next)
							.getAdapter(TextGridObject.class);
					if (object == null)
						continue;
					
					text.append(object.getURI());
					if (iterator.hasNext())
						text.append('\n');
				}
			}
			if (text.length() > 0) {
				Clipboard clipboard = new Clipboard(HandlerUtil.getActiveShell(event)
						.getDisplay());
				clipboard.setContents(new Object[] { text.toString() },
						new Transfer[] { TextTransfer.getInstance() });
			}
		}
		
		return text.toString();
	}
}
