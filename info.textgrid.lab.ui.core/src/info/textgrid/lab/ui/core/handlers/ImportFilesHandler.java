package info.textgrid.lab.ui.core.handlers;

import info.textgrid.lab.ui.core.dialogs.ImportWizard;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

@Deprecated
public class ImportFilesHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ImportWizard.showWizard(HandlerUtil.getActiveShell(event), HandlerUtil
				.getCurrentSelection(event));
		return null;
	}

}
