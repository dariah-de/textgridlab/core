<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns="http://www.w3.org/1999/xhtml">

    <!--
        
        This simple stylesheet converts a data-oriented XML file to a rather compact XHTML / CSS representation,
        showing elements, text and attributes, but omitting namespace information.  
        
    -->

    <xsl:output method="html"/>
    
    <!-- Elements with complex content -->
    <xsl:template match="*[@*|*]">
        <div class="element">
            <p class="name">
                <xsl:value-of select="local-name()"/>               
            </p>
            <xsl:apply-templates select="@*"/>
            <p class="value"><xsl:apply-templates/></p>
        </div>
    </xsl:template>
    
    <!-- other elements, i.e. those with simple content -->
    <xsl:template match="*">
        <p class="element">
                <span class="name"><xsl:value-of select="local-name()"/></span> : 
                <span class="value"><xsl:value-of select="."/></span>
         </p>
      
    </xsl:template>
    
    <xsl:template match="@*">
        <p class="attribute">
            @<em class="attname"><xsl:value-of select="name()"/></em> = <q class="value"><xsl:value-of select="."/></q>
        </p>
    </xsl:template>
    
    
     <!-- Set up the document header (including the CSS) and body -->
    <xsl:template match="/">
        <html>
            <head>
                <title>XML Structure</title>
                <style type="text/css">
                    body {
                        font-family: sans-serif;
                        font-size: 9pt; 
                    }
                    
                    .element {
                        margin-left: 0.4em;
                        padding-left: 0.6em;
                        
                    }
                    
                    p {
                        padding: 0pt 1pt 0pt 1pt;
                        margin: 0pt 1pt;
                    }
                                        
                    *.element *.name {
                        color: #444;
                        font-weight: bold;
                        margin-left: -0.5em;
                    }
                    
                    .value {
                       color: #008;
                    }
                    
                </style>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
        
    </xsl:template>
    
</xsl:stylesheet>
