package info.textgrid.lab.authn.views;


import info.textgrid.lab.authn.AuthBrowser;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

/*
 * based on the view sample from Europa's plug-in wizard
 * and on CodeSnippet160 
 * (http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.swt.snippets/src/org/eclipse/swt/snippets/Snippet160.java)
 * which threads a value obtained from a JavaScript execution 
 * through the browser's status bar and returns it.
 */

/** 
 * opens the Authentication View with a browser window
 * pointing to the TextGrid Authentication Resource
 * (https://...tgauth/secure/TextGrid-WebAuth....php)
 * <p>
 * Should work with the native browsers on all common
 * platforms, provided JavaScript is activated
 * @author Martin Haase <martin.haase@daasi.de>
 */
public final class AuthNView extends ViewPart {
	AuthBrowser browserComp;
	
//	/**
//	 * Returns the RBAC Session ID. Will be empty if no
//	 * Authentication Action has taken place or Authentication
//	 * failed. It is up to the Caller to check whether it is empty
//	 * and trigger the Authentication view when it deems it appropriate.
//	 * @return String
//	 */
//	public String getRbacSid() {
//		return browserComp.getRbacSid();
//	}
    public void setFocus() {
    	browserComp.getAuthBrowser().setFocus();
    }
    public void createPartControl(Composite parent) {
    	browserComp = new AuthBrowser(parent);
    }
}

