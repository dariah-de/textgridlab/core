package info.textgrid.lab.authn;

import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;

import java.text.MessageFormat;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;

/**
 * A status bar contribution that displays the current user name and has a
 * tooltip with additional details.
 * 
 * When clicked, this opens the login dialog (if not logged in) or the
 * authentication dialog (when logged in).
 * 
 * @author vitt
 * 
 */
public class LoginControlContribution extends WorkbenchWindowControlContribution implements SelectionListener, ISIDChangedListener {

	private Link loginLink;
	private Composite contributionArea;

	public LoginControlContribution() {
		// Auto-generated constructor stub
	}

	public LoginControlContribution(String id) {
		super(id);
		// Auto-generated constructor stub
	}

	protected Control createControl(Composite parent) {
		contributionArea = new Composite(parent, SWT.NONE);
		contributionArea.setLayout(new GridLayout(1, false));

		loginLink = new Link(contributionArea, SWT.NONE);
		setupLoginLink(RBACSession.getInstance().getSID(false), RBACSession.getInstance().getEPPN());
		GridDataFactory.fillDefaults().hint(200, SWT.DEFAULT).align(SWT.CENTER, SWT.CENTER).applyTo(loginLink);
		loginLink.addSelectionListener(this);
		AuthBrowser.addSIDChangedListener(this);

		return contributionArea;
	}

	public void widgetDefaultSelected(SelectionEvent e) {
		widgetSelected(e);
	}

	public void widgetSelected(SelectionEvent e) {
		RBACSession.neverAsk(false);
		if ("".equals(RBACSession.getInstance().getSID(false))) //$NON-NLS-1$
			RBACSession.getInstance().getSID(true);
		else {
			IShellProvider shellProvider = getWorkbenchWindow();
			if (shellProvider == null)
				shellProvider = PlatformUI.getWorkbench().getModalDialogShellProvider();
			new UserIDDialog(shellProvider).open();
		}
	}

	public void sidChanged(final String newSID, final String newEPPN) {
		setupLoginLink(newSID, newEPPN);
	}

	/**
	 * fills the login link with up-to-date data.
	 * 
	 * TODO does this perform any long-running operation in TG-auth*?
	 * 
	 * 
	 * @param sid
	 *            the current session ID
	 * @param eppn
	 *            the current EPPN
	 */
	private void setupLoginLink(final String sid, final String eppn) {
		if (loginLink != null && !loginLink.isDisposed()) {
			// StatusManager.getManager().handle(
			// new Status(IStatus.INFO, Activator.PLUGIN_ID,
			// NLS.bind("Login Control: SID changed to {0} , EPPN {1}", sid,
			// eppn), new Exception()));
			if ("".equals(sid)) { // working anonymously //$NON-NLS-1$
				loginLink.setText(Messages.LoginControlContribution_LoginLink);
				loginLink.setToolTipText(Messages.LoginControlContribution_LoginTooltip);
			} else {
				loginLink.setText("<a>" + eppn + "</a>"); //$NON-NLS-1$ //$NON-NLS-2$
				final UserDetail details = RBACSession.getInstance().getDetails(eppn);
				loginLink.setToolTipText(MessageFormat.format(Messages.LoginControlContribution_LoggedInTooltip,
						details.getEPPN(), details.getName(), details.getMail(), details.getOrganisation()));
			}
		}
	}

	@Override
	public void dispose() {
		if (loginLink != null && !loginLink.isDisposed())
			loginLink.removeSelectionListener(this);
		AuthBrowser.removeSIDChangedListener(this);
		super.dispose();
	}

}
