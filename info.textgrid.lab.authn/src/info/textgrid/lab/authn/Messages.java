package info.textgrid.lab.authn;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.authn.messages"; //$NON-NLS-1$
	public static String AuthBrowser_AuthAt;
	public static String AuthBrowser_AuthenticatedAs;
	public static String AuthBrowser_CouldNotContactConfserv;
	public static String AuthBrowser_LoadingLoginScreen;
	public static String AuthNDialog_BackBtn;
	public static String AuthNDialog_ClosingDialog;
	public static String AuthNDialog_CouldNotContactConfserv;
	public static String AuthNDialog_NeverAskCheckbox;
	public static String AuthNDialog_PasswordResetBtn;
	public static String AuthNDialog_ResetMessage;
	public static String AuthNDialog_ResettingPassword;
	public static String AuthNDialog_StayLoggedIn;
	public static String AuthNDialog_StayLoggedInTooltip;
	public static String AuthNDialog_WayfMessage;
	public static String AutoLogin_TryingAutoLogin;
	public static String LoginControlContribution_LoggedInTooltip;
	public static String LoginControlContribution_LoginLink;
	public static String LoginControlContribution_LoginTooltip;
	public static String ModifyUserAttributesBrowser_BrowserFailed;
	public static String ModifyUserAttributesBrowser_ConfservFailed;
	public static String ModifyUserAttributesBrowser_ModifyingUserData;
	public static String ModifyUserAttributesBrowser_URLFailed;
	public static String PasswordBrowser_ChangingPassword;
	public static String PasswordBrowser_CouldNotContactConfserv;
	public static String PasswordBrowser_CouldNotInstantiateBrowser;
	public static String PasswordBrowser_URLCouldNotBeLoaded;
	public static String RBACClientUtilities_TGAuthError;
	public static String RBACSession_AuthNDialogue;
	public static String RBACSession_FriendRetrieval;
	public static String RBACSession_FriendRetrievalError;
	public static String RBACSession_NameRecordsError;
	public static String RBACSession_RetrievingContacts;
	public static String RBACSession_RetrievingNameRecords;
	public static String RBACSession_UserDetailsRetrieval;
	public static String RBACuser_CouldNotFindOutUserExistance;
	public static String UserIDDialog_ChangePassword;
	public static String UserIDDialog_CouldNotContactConfserv;
	public static String UserIDDialog_CurrentSessionID;
	public static String UserIDDialog_CurrentUserID;
	public static String UserIDDialog_ForgotPassword;
	public static String UserIDDialog_Logout;
	public static String UserIDDialog_LogoutImpossible;
	public static String UserIDDialog_MyUserData;
	public static String UserIDDialog_NotAuthenticated;
	public static String UserIDDialog_ReAuthenticate;
	public static String UserIDDialog_ResettingPassword;
	public static String UserIDDialog_SessionIDExplanation;
	public static String UserIDDialog_WebBrowserNotice;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
