package info.textgrid.lab.authn;

/*import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.namespaces.middleware.tgauth.TgextraStub;

import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties.ProxyProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
*/

/**
 * Some utility methods for RBAC clients.
 * UNUSED!!! See and use i.t.l.core.tgauthclient
 * @author Frank Queens for TextGrid
 * 
 */
public class RBACClientUtilities {
	
	/*private static TgextraStub service;

	protected static TgextraStub getService() {
		if (service == null) {
			try {
				MultiThreadedHttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
				HttpClient httpClient = new HttpClient(httpConnectionManager);
				
				service = new TgextraStub(ConfClient.getInstance().getValue(
						ConfservClientConstants.TG_AUTH));
				
				ServiceClient serviceClient = service._getServiceClient();
				Options options = serviceClient.getOptions();
				options.setTimeOutInMilliSeconds(60000);
				
				if (info.textgrid.lab.conf.ConfPlugin.getDefault()
						.getPreferenceStore().getBoolean("proxy_connection")) { //$NON-NLS-1$
					ProxyProperties proxyProperties = new ProxyProperties();
					proxyProperties.setProxyName(info.textgrid.lab.conf.ConfPlugin
							.getDefault().getPreferenceStore()
							.getString("proxy_connection_http")); //$NON-NLS-1$
					proxyProperties.setProxyPort(info.textgrid.lab.conf.ConfPlugin
							.getDefault().getPreferenceStore()
							.getInt("proxy_connection_port")); //$NON-NLS-1$
					options.setProperty(
							org.apache.axis2.transport.http.HTTPConstants.PROXY,
							proxyProperties);
					options.setProperty(
							org.apache.axis2.transport.http.HTTPConstants.HTTP_PROTOCOL_VERSION,
							org.apache.axis2.transport.http.HTTPConstants.HEADER_PROTOCOL_10);
				}
				
				// set HTTP client
				serviceClient.getOptions().setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Constants.VALUE_TRUE);
				serviceClient.getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
				
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						Messages.RBACClientUtilities_TGAuthError, e);
				Activator.getDefault().getLog().log(status);
			}
		}
		return service;
	}
	
	*//**
	 * Method creates and returns a configured serviceStub ...
	 * 
	 * @return service stub 
	 *//*
	public static TgextraStub getRBACServiceStub() {
		TgextraStub stub = null;
		stub = getService();
		return stub;
	}*/

}
