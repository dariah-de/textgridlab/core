package info.textgrid.lab.authn;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Displays the Browser that does the actual remote authentication. Notifys all
 * SIDListeners with Username (eduPersonPrincipalName) and Session ID. It
 * evaluates a small javascript command on the resulting page and returns the
 * result.
 * 
 * @author martin
 * 
 */
public class AuthBrowser extends Composite {
	private Browser authbrowser;
	private String rbacsid = ""; //$NON-NLS-1$
	private static ListenerList sidChangedListeners = new ListenerList();
	private String webAuth;
	private boolean saveSID;

	public AuthBrowser(Composite parent) {
		super(parent, SWT.FILL);

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 500;
		parentGD.widthHint = 860;
		parentGD.verticalAlignment = GridData.FILL;

		parent.setLayoutData(parentGD);

		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		GridDataFactory labelsFactory = GridDataFactory.createFrom(gridData);

		// try {
		try {
			authbrowser = new Browser(parent, SWT.NONE); // *not* SWT.WEBKIT
															// @since 3.7
		} catch (SWTError e) {
			StatusManager
					.getManager()
					.handle(new Status(
							IStatus.WARNING,
							Activator.PLUGIN_ID,
							"Failed to instantiate Mozilla based browser (no XULrunner?), trying default instead ...")); //$NON-NLS-1$
			authbrowser = new Browser(parent, SWT.NONE);
		}
		// authbrowser = TextGridLabBrowser.createBrowser(parent);
		labelsFactory.applyTo(authbrowser);
		// } catch (SWTError e) {
		// IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
		// "Could not instantiate Browser for Authentication!", e);
		// Activator.getDefault().getLog().log(status);
		// return;
		// }
		setLayout(new GridLayout());
		if (Activator.isDebugging(Activator.DEBUG_NAVIGATION)) {
			authbrowser.addLocationListener(new LocationListener() {

				private void log(final String location, final boolean changing) {
					StatusManager.getManager().handle(new Status(
							IStatus.INFO,
							Activator.PLUGIN_ID,
							MessageFormat.format(changing? "Navigating to {0} ...": "Location is now {0}", location))); //$NON-NLS-1$ //$NON-NLS-2$
				}

				@Override
				public void changing(LocationEvent event) {
					log(event.location, true);
				}

				@Override
				public void changed(LocationEvent event) {
					log(event.location, false);
				}
			});
		}
		authbrowser.setText(Messages.AuthBrowser_LoadingLoginScreen);

		authbrowser.addProgressListener(new ProgressListener() {

			public void changed(ProgressEvent event) {
			}

			/**
			 * Extracts the contents of <meta name="name" content="…"/> in the
			 * page.
			 * 
			 * @param name
			 * @return
			 */
			private String getMetaVar(final String name) {
				final String js = "if (document.getElementsByName('" + name //$NON-NLS-1$
						+ "').length==1) {return document.getElementsByName('" //$NON-NLS-1$
						+ name
						+ "')[0].getAttribute('content');} else {return '';}"; //$NON-NLS-1$
				final String result = (String) authbrowser.evaluate(js);
				return result;
			}

			public void completed(ProgressEvent event) {			
				
				rbacsid = getMetaVar("ePPNplusSID"); //$NON-NLS-1$

				if (rbacsid == "" || rbacsid == "reset") { //$NON-NLS-1$ //$NON-NLS-2$
					return;
				} else {
					String[] ePPNplusSID = rbacsid.split("\\|"); //$NON-NLS-1$
					if (ePPNplusSID.length != 2) {
						// System.out.println("Found not a good SessionID, '"
						// + rbacsid + "'");
						return;
					}
					RBACSession.login(ePPNplusSID[1], ePPNplusSID[0], saveSID);
                    final IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
					if (prefs.getBoolean(Activator.SAVE_IDP)) {
						final String identityProvider = getMetaVar("Shib-Identity-Provider"); //$NON-NLS-1$
						if (identityProvider != null
								&& !identityProvider.isEmpty()) {
							Activator
									.getDefault()
									.getPreferenceStore()
									.setValue(Activator.LAST_IDP,
											identityProvider);
						}
					}
				}
			}
		});
		try {
			ConfClient confClient = ConfClient.getInstance();
			webAuth = confClient.getValue(ConfservClientConstants.AUTHZ);
			// System.out.println("WebAuth: authenticate at " + webAuth);
			IStatus status = new Status(IStatus.OK, Activator.PLUGIN_ID,
					MessageFormat.format(Messages.AuthBrowser_AuthAt, webAuth));
			Activator.getDefault().getLog().log(status);
			gotoStartPage(true);
		} catch (OfflineException e) {
			OnlineStatus.netAccessFailed(
					Messages.AuthBrowser_CouldNotContactConfserv, e);
		}
	}

	public interface ISIDChangedListener {
		public void sidChanged(final String newSID, final String newEPPN);
	}

	public static void addSIDChangedListener(ISIDChangedListener listener) {
		sidChangedListeners.add(listener);
	}

	public static void removeSIDChangedListener(ISIDChangedListener listener) {
		// TODO check if that really works...
		sidChangedListeners.remove(listener);
	}

	protected static void notifySIDListeners(final String newSID,
			final String newEPPN) {
		for (Object listener : sidChangedListeners.getListeners()) {
			((ISIDChangedListener) listener).sidChanged(newSID, newEPPN);
		}
	}

	/**
	 * This is for programs that modify an existing session (e.g. activate a
	 * role)
	 */
	public static void sessionChanged() {
		notifySIDListeners(RBACSession.getInstance().getSID(false), RBACSession
				.getInstance().getEPPN());
	}

	public Browser getAuthBrowser() {
		return authbrowser;
	}

	public void gotoStartPage(boolean useSavedIdP) {
		if (useSavedIdP
				&& Activator.getDefault().getPreferenceStore()
						.getBoolean(Activator.SAVE_IDP)) {
			String lastIdP = Activator.getDefault().getPreferenceStore()
					.getString(Activator.LAST_IDP);
			if (lastIdP != null && !lastIdP.isEmpty()) {
				StatusManager
						.getManager()
						.handle(new Status(
								IStatus.INFO,
								Activator.PLUGIN_ID,
								MessageFormat
										.format("Directing the user to the last used IdP, {0}", //$NON-NLS-1$
												lastIdP)));
				try {
					String url = new StringBuilder().append(webAuth)
							.append(webAuth.contains("?") ? "&" : "?") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							.append("entityID=") //$NON-NLS-1$
							.append(URLEncoder.encode(lastIdP, "UTF-8")) //$NON-NLS-1$
							.toString();
					authbrowser.setUrl(url);
					return;
				} catch (UnsupportedEncodingException e) {
					// UTF-8 not supported? Can't happen.
				}
			}
		}
		
		// in case there's no saved stuff:
		authbrowser.setUrl(webAuth);
	}

	public String getRbacSid() {
		return rbacsid;
	}

	@Override
	public void dispose() {
		authbrowser.dispose();
		super.dispose();
	}

	public void setSaveSID(boolean selection) {
		this.saveSID = selection;
	}

}
