package info.textgrid.lab.authn;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;

public class UserIDDialog extends TrayDialog {
	private static final int LOGOUT = -125;
	private static final int REAUTHENTICATE = -123;
	private static final int FORGOT_PASSWORD = -127;
	private static final int CHANGE_PASSWORD = -124;
	private static final int MY_USER_DATA = -122;
	Label uidlabel;
	boolean shibUser = false;
	private Text sidText;

	public UserIDDialog(Shell shell) {
		super(shell);
		// TODO Auto-generated constructor stub
		initialize();
	}

	public UserIDDialog(IShellProvider parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
		initialize();
	}

	protected void initialize() {
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, MY_USER_DATA, Messages.UserIDDialog_MyUserData, false);
		createButton(parent, CHANGE_PASSWORD, Messages.UserIDDialog_ChangePassword, false);
		createButton(parent, FORGOT_PASSWORD, Messages.UserIDDialog_ForgotPassword, false);
		createButton(parent, REAUTHENTICATE, Messages.UserIDDialog_ReAuthenticate, false);
		createButton(parent, LOGOUT, Messages.UserIDDialog_Logout, false);
		createButton(parent, IDialogConstants.CLOSE_ID,
				IDialogConstants.CLOSE_LABEL, true).setFocus();
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CLOSE_ID)
			this.close();
		else if (buttonId == REAUTHENTICATE) { // Re-Authenticate
			RBACSession.getInstance().reauthenticate();
			update();
		} else if (buttonId == LOGOUT) { // Log out
			//#######
			//TG-1964
			if (IEditorPart.PROP_DIRTY == 257) {
				IWorkbenchPage page = PlatformUI.getWorkbench()
		   				.getActiveWorkbenchWindow().getActivePage();
				page.saveAllEditors(true);
			}
			//#######
			String ePPN = RBACSession.getInstance().getEPPN();
			if (!"".equals(ePPN) && ePPN.indexOf("@textgrid.de") == -1) { //$NON-NLS-1$ //$NON-NLS-2$
				shibUser = true;
			}
			RBACSession.logout();
			update();
			shibUser = false;
		} else if (buttonId == CHANGE_PASSWORD) { // Change Password
			PasswordDialog pwd = new PasswordDialog(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell());
			pwd.open();
		} else if (buttonId == MY_USER_DATA) { // Modify / List User Data
			ModifyUserAttributesDialog udd = new ModifyUserAttributesDialog(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getShell());
			udd.open();
		} else if (buttonId == FORGOT_PASSWORD) {
			URL pwrurl = null;
			try {
				ConfClient confClient = ConfClient.getInstance();
				String pwrurlString = confClient.getValue("PasswordReset"); //$NON-NLS-1$
				pwrurl = URI.create(pwrurlString).toURL();
				PlatformUI
						.getWorkbench()
						.getBrowserSupport()
						.createBrowser(IWorkbenchBrowserSupport.AS_EXTERNAL,
								"xyz123abc456", "Hallo", "egal") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						.openURL(pwrurl);
				MessageBox mb = new MessageBox(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), SWT.OK
						| SWT.ICON_INFORMATION);
				mb.setText(Messages.UserIDDialog_ResettingPassword);
				mb.setMessage(Messages.UserIDDialog_WebBrowserNotice);
				mb.open();
			} catch (OfflineException e) {
				OnlineStatus.netAccessFailed(
						Messages.UserIDDialog_CouldNotContactConfserv, e);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		GridDataFactory labelsFactory = GridDataFactory.createFrom(gridData);

		labelsFactory.applyTo(parent);

		Group g = new Group(parent, SWT.NONE);
		g.setLayout(new GridLayout());
		g.setText(Messages.UserIDDialog_CurrentUserID);
		labelsFactory.applyTo(g);

		gridData.heightHint = 50;
		uidlabel = new Label(g, SWT.CENTER);
		// labelsFactory.applyTo(uidlabel);
		uidlabel.setLayoutData(gridData);

		Group sidGroup = new Group(parent, SWT.NONE);
		sidGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		sidGroup.setLayout(new GridLayout(1, false));
		sidGroup.setText(Messages.UserIDDialog_CurrentSessionID);

		sidText = new Text(sidGroup, SWT.SINGLE | SWT.LEAD | SWT.READ_ONLY);
		sidText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		sidText.setToolTipText(Messages.UserIDDialog_SessionIDExplanation);

		update();
		return g;
	}

	/**
	 * Updates the login information in the dialog. Should be called after
	 * authentication changes.
	 */
	private void update() {
		String uid = RBACSession.getInstance().getEPPN();
		sidText.setText(RBACSession.getInstance().getSID(false));
		if (shibUser) {
			uidlabel.setText(Messages.UserIDDialog_LogoutImpossible);
		} else if (uid.equals("")) { //$NON-NLS-1$
			uidlabel.setText(Messages.UserIDDialog_NotAuthenticated);
		} else {
			uidlabel.setText(uid);
		}
		uidlabel.pack();
	}

}
