package info.textgrid.lab.authn;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

public class ShowUserIDHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		UserIDDialog userIDDialog = new UserIDDialog(HandlerUtil.getActiveShell(event));
		userIDDialog.open();
		
		return null;
	}

}
