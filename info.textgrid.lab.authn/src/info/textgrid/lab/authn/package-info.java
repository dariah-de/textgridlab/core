/**
 * Login and authentication code. Most clients will only need to call
 * 
 * <pre>
 * 	  String sid = {@link RBACSession}.{@linkplain RBACSession#getInstance() getInstance()}.{@linkplain RBACSession#getSID(Boolean) getSID(openDialog)};
 * </pre>
 * 
 * to retrieve the session ID required for calls to TextGrid services and
 * utilities.
 * 
 * <h4>Subpackages</h4>
 * <ul>
 * <li>{@link info.textgrid.lab.authn.views}</li>
 * </ul>
 * 
 */
package info.textgrid.lab.authn;

