package info.textgrid.lab.authn;

import java.text.MessageFormat;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

public class ModifyUserAttributesBrowser extends Composite {
	private Browser udBrowser;

	public ModifyUserAttributesBrowser (Composite parent) {
		super(parent,SWT.FILL);
		
		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 500;
		parentGD.widthHint = 700;
		parentGD.verticalAlignment = GridData.FILL;
		
		parent.setLayoutData(parentGD);

		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		GridDataFactory labelsFactory = GridDataFactory.createFrom(gridData);

		udBrowser = new Browser (parent, SWT.NONE);
		labelsFactory.applyTo(udBrowser);

		setLayout(new GridLayout());

		try {
			ConfClient confClient = ConfClient.getInstance();
			String udchange= confClient.getValue("userdata"); //$NON-NLS-1$
			IStatus status = new Status(IStatus.OK, Activator.PLUGIN_ID,
					MessageFormat.format(Messages.ModifyUserAttributesBrowser_ModifyingUserData, udchange));
			Activator.getDefault().getLog().log(status);
			
			// now, important, get the Sid:
			String Sid = RBACSession.getInstance().getSID(true);
			String ePPN = RBACSession.getInstance().getEPPN();
			
			udchange = udchange + "&Sid=" + Sid + "&ePPN=" + ePPN; //$NON-NLS-1$ //$NON-NLS-2$

			
			
			boolean reached = udBrowser.setUrl(udchange);
			if (!reached) {
				OnlineStatus.netAccessFailed(
						Messages.ModifyUserAttributesBrowser_BrowserFailed,
						new Exception(Messages.ModifyUserAttributesBrowser_URLFailed));
			}			
		} catch (OfflineException e) {
			OnlineStatus.netAccessFailed(
					Messages.ModifyUserAttributesBrowser_ConfservFailed, e);
		}
	}

}
