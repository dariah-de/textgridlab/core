package info.textgrid.lab.authn;


import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class PasswordDialog extends TrayDialog {
	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(newShellStyle | SWT.RESIZE | SWT.MAX);
	}


	PasswordBrowser browserComp;
	PasswordDialog thisDialog;

	protected PasswordDialog(IShellProvider parentShell) {
		super(parentShell);
		initialize();//parentShell.getShell());
	}

	public PasswordDialog(Shell parentShell) {
		super(parentShell);
		initialize();
	}
	
	protected void initialize() {
		thisDialog = this;
	}
	
	protected void initialize(Composite parent) {
 	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, true);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CLOSE_ID) 
			this.close();
	}

	
	@Override
	protected Control createDialogArea(Composite parent) {
		browserComp = new PasswordBrowser(parent);
		return browserComp;
	}
	

}
