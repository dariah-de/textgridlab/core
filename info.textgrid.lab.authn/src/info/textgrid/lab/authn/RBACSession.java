package info.textgrid.lab.authn;

import info.textgrid.lab.conf.ConfPlugin;
import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;
import info.textgrid.namespaces.middleware.tgauth.Friend;
import info.textgrid.namespaces.middleware.tgauth.GetFriendsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetFriendsResponse;
import info.textgrid.namespaces.middleware.tgauth.GetNamesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetNamesResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * The session of the currently logged-in user; see {@link #getSID(boolean)} to
 * retrieve the session ID required by many TextGrid services and utilities.
 * 
 * @author TextGrid / DAASI International GmbH / Martin Haase
 */
public final class RBACSession implements
		org.eclipse.jface.util.IPropertyChangeListener {
	private static final String ENV_OVERRIDE_VAR = "TEXTGRIDLAB_SID"; //$NON-NLS-1$
	private static String sid = ""; //$NON-NLS-1$
	private static String ePPN = ""; //$NON-NLS-1$
	private static RBACSession rbacsession;
	private static boolean neverAsk = false;
	private static boolean authDialogIsOpen = false;
	private static HashMap<String, UserDetail> nameCache = new HashMap<String, UserDetail>();
	private static ArrayList<Friend> friends = new ArrayList<Friend>();
	private static PortTgextra stub = null;

	private RBACSession() {
		ConfPlugin.getDefault().getPreferenceStore()
				.addPropertyChangeListener(this);
	}

	/**
	 * Retrieve a singleton representing the current session.
	 */
	public static RBACSession getInstance() {
		if (rbacsession == null) {
			rbacsession = new RBACSession();
		}
		return rbacsession;
	}

	/**
	 * set the session ID to a new value.
	 * 
	 * @param sID
	 */
	private synchronized static void setSID(String sID) { // FIXME why public?
		sid = sID;
		friends = new ArrayList<Friend>();
	}

	/**
	 * sets the EPPN (that is the Shibboleth-wide user ID).
	 * 
	 * @param eppn
	 */
	private synchronized static void setEPPN(String eppn) { // FIXME why public?
		ePPN = eppn;
	}
	
	/**
	 * Log the user in using the specified SID and ePPN and notify listeners. 
	 * 
	 * @param save if <code>true</code>, save SID and ePPN for reuse.
	 */
	public static void login(final String sid, final String eppn, boolean save) {
		setSID(sid);
		setEPPN(eppn);
		final IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		if (save) {
			prefs.setValue(Activator.LAST_SID, sid);
			prefs.setValue(Activator.LAST_EPPN, eppn);
		} else {
			prefs.setToDefault(Activator.LAST_SID);
			prefs.setToDefault(Activator.LAST_EPPN);
		}
		IStatus status = new Status(IStatus.OK,
				Activator.PLUGIN_ID, NLS.bind(Messages.AuthBrowser_AuthenticatedAs, eppn));
		StatusManager.getManager().handle(status);
		new UIJob("") { //$NON-NLS-1$
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				AuthBrowser.notifySIDListeners(sid, eppn);
				return Status.OK_STATUS;
			}
		}.schedule();
	}
	
	public static void logout() {
		final String oldSID = sid;
		setSID(""); //$NON-NLS-1$
		setEPPN(""); //$NON-NLS-1$
		final IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
        prefs.setToDefault(Activator.LAST_SID);
        prefs.setToDefault(Activator.LAST_EPPN);
        prefs.setToDefault(Activator.LAST_IDP);
        Browser.clearSessions();
        if (oldSID != null && !oldSID.isEmpty()) {
        	new UIJob("") { //$NON-NLS-1$
        		@Override
        		public IStatus runInUIThread(IProgressMonitor monitor) {
        			AuthBrowser.notifySIDListeners("", ""); //$NON-NLS-1$ //$NON-NLS-2$
        			return Status.OK_STATUS;
        		}
        	}.schedule();
        }
	}
	
	public void reauthenticate() {
		Browser.clearSessions();
		showLoginDialogBlocking();	// will care for the rest
	}

	/**
	 * Set to true if you do not want the login dialogue to appear anymore in
	 * this Lab instance, except for Help->Authentication->Re-Authenticate where
	 * it will be set to false before authenticating.
	 * 
	 * @param state
	 */
	public synchronized static void neverAsk(boolean state) {
		neverAsk = state;
	}

	protected synchronized static void authDialogIsOpen(boolean state) {
		authDialogIsOpen = state;
	}

	/**
	 * Retrieve a session ID for the current user.
	 * 
	 * <p>
	 * The session ID is a string generated by the TG-auth* utility after
	 * presenting the user a login dialog. It is used by TG-auth* to identify
	 * the roles and permissions a certain user has. The session ID
	 * <code>""</code> (i.e. the empty string) identifies anonymous users.
	 * </p>
	 * 
	 * @param openAuthNDialogue
	 *            if <code>false</code>, this call will never open a login
	 *            dialog (but return <code>""</code> for unauthenticated users).
	 *            If <code>true</code> <em>and</em> if the user has neither
	 *            logged in yet nor checked the
	 *            <em>don't ask again, I want to work anonymously</em> checkbox
	 *            (see {@link #neverAsk(boolean)}, a login dialog is shown.
	 * @return a session ID. Never <code>null</code>, but may be <code>""</code>
	 *         to identify anonymous users.
	 */
	public String getSID(boolean openAuthNDialogue) {
		String override_sid = java.lang.System.getenv().get(ENV_OVERRIDE_VAR);
		if (override_sid != null) {
			sid = override_sid;
		}
		
		if (openAuthNDialogue && sid == "" && !neverAsk && !authDialogIsOpen) { //$NON-NLS-1$
			showLoginDialogBlocking();
		}
		authDialogIsOpen = false;
		return sid;
	}

	protected void showLoginDialogBlocking() {
		authDialogIsOpen = true;
		if (inUIThread()) {
			startJobDirect();
		} else {
			startJobInUIThread();
		}
	}

	/**
	 * Retrieve the EPPN (the Shibboleth-wide user id) for the user. Never null,
	 * but may be <code>""</code> when working anonymously.
	 */
	public String getEPPN() {
		return ePPN;
	}

	private void startJobInUIThread() {
		// TODO Auto-generated method stub
		UIJob uij = new UIJob(Messages.RBACSession_AuthNDialogue) {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				AuthNDialog aud = new AuthNDialog(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell());
				aud.open();
				return new Status(IStatus.OK, Activator.PLUGIN_ID,
						"opened Authentication Dialogue"); //$NON-NLS-1$
			}
		};
		uij.schedule();
		try {
			uij.join();
		} catch (InterruptedException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Could not wait for AuthN dialogue to be closed!", e); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);
		}
	}

	private void startJobDirect() {
		AuthNDialog aud = new AuthNDialog(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell());
		aud.open();
	}

	private boolean inUIThread() {
		return Display.getCurrent() != null;
	}

	public void propertyChange(PropertyChangeEvent event) {

		// currently, modifications of the preference do not lead to an
		// immediate change of the ConfServ used; instead, a lab restart is
		// suggested (cf. TG-351). If we ever re-enable this feature, we should
		// listen to the ConfServ instead to the preference to avoid race
		// conditions. -tv

		// if (ConfClient.PREF_ENDPOINT.equals(event.getProperty())) {
		// Different end-point, different SID
		// setSID("");
		// getSID(true);
		// }
	}

	public ArrayList<Friend> getFriends(boolean retrieveFromRBAC) {
		if (retrieveFromRBAC) {
			Job job = new Job(Messages.RBACSession_FriendRetrieval) {
				@Override
				public IStatus run(IProgressMonitor monitor) {
					GetFriendsRequest getFreq = new GetFriendsRequest();
					getFreq.setAuth(sid);
					getFreq.setLog(""); //$NON-NLS-1$
					stub = getRBACStub();
					GetFriendsResponse resp = new GetFriendsResponse();
					resp = stub.getFriends(getFreq);
					setFriends(resp);
					return new Status(IStatus.OK, Activator.PLUGIN_ID,
							Messages.RBACSession_RetrievingContacts);
				}
			};
			job.schedule();
			try {
				job.join();
			} catch (InterruptedException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						Messages.RBACSession_FriendRetrievalError, e);
				Activator.getDefault().getLog().log(status);
			}
		}
		return friends;
	}

	private synchronized void setFriends(GetFriendsResponse gfr) {
		Friend[] friendArr = gfr.getFriends().toArray(new Friend[0]);
		if (friendArr == null) {
			friends = new ArrayList<Friend>();
			return;
		}
		Arrays.sort(friendArr, new Comparator<Friend>() {
			public int compare(Friend f1, Friend f2) {
				return f2.getScore().compareTo(f1.getScore());
			}
		});
		friends = new ArrayList<Friend>(Arrays.asList(friendArr));
	}

	private synchronized void setNames(GetNamesResponse gnr) {
		for (UserDetail ud : gnr.getUserdetails()) {
			nameCache.put(ud.getEPPN(), ud);
		}
	}

	PortTgextra getRBACStub() {
		if (stub == null) {
			stub = TgAuthClientUtilities.getTgAuthServiceStub();
		}
		return stub;
	}

	public UserDetail getDetails(String ePPN) {
		if (!nameCache.containsKey(ePPN)) {
			ArrayList<String> a = new ArrayList<String>(1);
			a.add(ePPN);
			retrieveDetailsFromRBAC(a);
		}
		return nameCache.get(ePPN);
	}

	/**
	 * Generates a {@link PersonType} record for the TextGrid user identified by
	 * the given ID.
	 * 
	 * @param ePPN
	 *            {@linkplain #getEPPN()} EPPN} of the user for which to
	 *            retrieve data
	 * @return a {@link PersonType} record with the
	 *         {@linkplain PersonType#getId() ID} filled with the EPPN and the
	 *         {@linkplain PersonType#getValue() value} being the user's name
	 */
	public PersonType getPerson(final String ePPN) {
		PersonType personType = new PersonType();
		personType.setId("eduPersonPrincipalName:" + ePPN.trim()); //$NON-NLS-1$
		UserDetail details = getDetails(ePPN);
		personType.setValue(details != null ? details.getName() : ePPN);
		return personType;
	}

	/**
	 * Returns Full Name for an ePPN-like ID, e.g. "John Doe" for
	 * "john.doe@textgrid.org"
	 * 
	 * @param ePPN
	 * @return
	 */
	public String getFullName(String ePPN) {
		return getDetails(ePPN).getName();
	}

	/**
	 * Returns a {@link PersonType} record for the current user.
	 * 
	 * @see #getPerson(String)
	 * @see #getEPPN()
	 */
	public PersonType getPerson() {
		return getPerson(getEPPN());
	}

	public void retrieveDetailsFromRBAC(ArrayList<String> ePPNs) {
		final ArrayList<String> todo = new ArrayList<String>();

		for (String ePPN : ePPNs) {
			if (!nameCache.containsKey(ePPN)) {
				todo.add(ePPN);
				// preliminary put in order to fill the cache in case
				// some ePPNs will not return details in the following rbac call
				UserDetail ud = new UserDetail();
				ud.setName(ePPN);
				nameCache.put(ePPN, ud);
			}
		}

		Job job = new Job(Messages.RBACSession_UserDetailsRetrieval) {
			@Override
			public IStatus run(IProgressMonitor monitor) {
				GetNamesRequest getNreq = new GetNamesRequest();
				getNreq.setAuth(sid);
				getNreq.setLog(""); //$NON-NLS-1$
				getNreq.getEPPN().addAll(todo);
				stub = getRBACStub();
				GetNamesResponse resp = new GetNamesResponse();
				resp = stub.getNames(getNreq);
				if (resp != null && resp.getUserdetails() != null
						&& resp.getUserdetails().size() > 0) {
					setNames(resp);
				}
				return new Status(IStatus.OK, Activator.PLUGIN_ID,
						Messages.RBACSession_RetrievingNameRecords);
			}
		};
		job.schedule();
		try {
			job.join();
		} catch (InterruptedException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					Messages.RBACSession_NameRecordsError, e);
			Activator.getDefault().getLog().log(status);
		}
	}

}
