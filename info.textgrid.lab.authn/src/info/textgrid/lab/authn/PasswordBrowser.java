package info.textgrid.lab.authn;

import java.text.MessageFormat;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

public class PasswordBrowser extends Composite {
	private Browser pwBrowser;

	public PasswordBrowser (Composite parent) {
		super(parent,SWT.FILL);
		
		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 400;
		parentGD.widthHint = 500;
		parentGD.verticalAlignment = GridData.FILL;
		
		parent.setLayoutData(parentGD);


		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		GridDataFactory labelsFactory = GridDataFactory.createFrom(gridData);

		
		pwBrowser = new Browser (parent, SWT.NONE);
		labelsFactory.applyTo(pwBrowser);
//		setLayout(new GridLayout());

		try {
			ConfClient confClient = ConfClient.getInstance();
			String pwchange= confClient.getValue("pwchange"); //$NON-NLS-1$
			IStatus status = new Status(IStatus.OK, Activator.PLUGIN_ID,
					MessageFormat.format(Messages.PasswordBrowser_ChangingPassword, pwchange));
			Activator.getDefault().getLog().log(status);
			boolean reached = pwBrowser.setUrl(pwchange);
			if (!reached) {
				OnlineStatus.netAccessFailed(
						Messages.PasswordBrowser_CouldNotInstantiateBrowser,
						new Exception(Messages.PasswordBrowser_URLCouldNotBeLoaded));
			}			
		} catch (OfflineException e) {
			OnlineStatus.netAccessFailed(
					Messages.PasswordBrowser_CouldNotContactConfserv, e);
		}
	}


}
