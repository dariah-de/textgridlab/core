package info.textgrid.lab.authn;

import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

public class AuthNDialog extends TrayDialog {
	private static final int RESTART_LOGIN = 123;
	private static final int RESET_PASSWORD = 127;
	AuthBrowser browserComp;
	AuthNDialog thisDialog;
	Button neverAsk;
	ISIDChangedListener sidChangedListener;

	protected AuthNDialog(IShellProvider parentShell) {
		super(parentShell);
		initialize();// parentShell.getShell());
	}

	public AuthNDialog(Shell parentShell) {
		super(parentShell);
		initialize();
	}

	protected void initialize() {
		thisDialog = this;
		this.setHelpAvailable(false);
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
		sidChangedListener = new ISIDChangedListener() {
			public void sidChanged(final String newSID, final String newEPPN) {
				// do not necessarily need to evaluate e
				if (!RBACSession.getInstance().getSID(false).equals("")) { //$NON-NLS-1$
					new UIJob(Messages.AuthNDialog_ClosingDialog) { 
						@Override
						public IStatus runInUIThread(IProgressMonitor m) {
							thisDialog.destroy(); return Status.OK_STATUS; 
							}

					}.schedule();
				}
			}
		};
		AuthBrowser.addSIDChangedListener(sidChangedListener);
	}

	@Deprecated
	protected void destroy() {
		this.close();
	}

	@Override
	public boolean close() {
		AuthBrowser.removeSIDChangedListener(sidChangedListener);
		boolean result = super.close();
		RBACSession.authDialogIsOpen(false);
		return result;
	};

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
//		parent.setLayout(new GridLayout(4, false)); // Extrawurst for neverAsk
//		neverAsk = new Button(parent, SWT.CHECK);
//		neverAsk.setText(Messages.AuthNDialog_NeverAskCheckbox);
		((GridLayout) parent.getLayout()).numColumns++;
		if (Activator.getDefault().getPreferenceStore().getBoolean(Activator.ALLOW_SAVE_SID)) {
			final Button rememberMe = new Button(parent, SWT.CHECK);
			rememberMe.setText(Messages.AuthNDialog_StayLoggedIn);
			rememberMe.setToolTipText(Messages.AuthNDialog_StayLoggedInTooltip);
			rememberMe.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (browserComp != null && !browserComp.isDisposed())
						browserComp.setSaveSID(rememberMe.getSelection());
				}

			});
		}
		
		createButton(parent, RESTART_LOGIN, Messages.AuthNDialog_BackBtn, false);
		createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, false);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CLOSE_ID) {
			close();
		}
		if (buttonId == RESTART_LOGIN) {
			Browser.clearSessions();
			browserComp.gotoStartPage(false);
		}
		if (buttonId == RESET_PASSWORD) {
			URL pwrurl = null;
			try {
				ConfClient confClient = ConfClient.getInstance();
				String pwrurlString = confClient.getValue("PasswordReset"); //$NON-NLS-1$
				pwrurl = URI.create(pwrurlString).toURL();
				PlatformUI
						.getWorkbench()
						.getBrowserSupport()
						.createBrowser(IWorkbenchBrowserSupport.AS_EXTERNAL,
								"info.textgrid.lab.authn.external_browser", "TGauth* Browser", "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						.openURL(pwrurl);
				MessageBox mb = new MessageBox(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), SWT.OK
						| SWT.ICON_INFORMATION);
				mb.setText(Messages.AuthNDialog_ResettingPassword);
				mb.setMessage(Messages.AuthNDialog_ResetMessage); 
				mb.open();
			} catch (OfflineException e) {
				OnlineStatus.netAccessFailed(
						Messages.AuthNDialog_CouldNotContactConfserv, e);
			} catch (MalformedURLException e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getLocalizedMessage(), e));
			} catch (PartInitException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}

		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		browserComp = new AuthBrowser(parent);
		getShell().setText(Messages.AuthNDialog_WayfMessage);
		return browserComp;
	}

}
