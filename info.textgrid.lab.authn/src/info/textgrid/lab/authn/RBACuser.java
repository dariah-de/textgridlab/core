package info.textgrid.lab.authn;

import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.namespaces.middleware.tgauth.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.UserExistsRequest;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class RBACuser {
	public static Boolean checkUserExists(String user) {
		try {
			PortTgextra stub = TgAuthClientUtilities.getTgAuthServiceStub();
			UserExistsRequest uer = new UserExistsRequest();
			String rbacsid = RBACSession.getInstance().getSID(true);
			uer.setAuth(rbacsid);
			uer.setLog(""); //$NON-NLS-1$
			uer.setUsername(user);
			BooleanResponse exists = stub.userExists(uer);
			return exists.isResult();
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					MessageFormat.format( Messages.RBACuser_CouldNotFindOutUserExistance, user), e);
			Activator.getDefault().getLog().log(status);
			return false;
		}
	}
}
