package info.textgrid.lab.authn;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.authn"; //$NON-NLS-1$
	public static final String DEBUG_NAVIGATION = PLUGIN_ID + "/debug/navigation"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	public static String SAVE_IDP = "saveIdP"; //$NON-NLS-1$
	public static String SAVE_SID = "saveSID"; //$NON-NLS-1$
	public static String ALLOW_SAVE_SID = "allowSaveSID"; //$NON-NLS-1$
	
	public static String LAST_IDP = "lastIdP"; //$NON-NLS-1$
	public static String LAST_SID = "lastSID"; //$NON-NLS-1$
	protected static final String LAST_EPPN = "lastEPPN"; //$NON-NLS-1$
	
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		IPreferenceStore prefs = getPreferenceStore();
		prefs.setDefault(SAVE_IDP, true);
		prefs.setDefault(SAVE_SID, false);
		prefs.setDefault(ALLOW_SAVE_SID, true);
		prefs.setDefault(LAST_IDP, ""); //$NON-NLS-1$
		prefs.setDefault(LAST_SID, ""); //$NON-NLS-1$
	}
	

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static boolean isDebugging(final String option) {
		return getDefault().isDebugging() && Boolean.valueOf(Platform.getDebugOption(option));
	}
}
