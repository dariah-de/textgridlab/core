package info.textgrid.lab.authn;

import info.textgrid.lab.core.tgauthclient.TgAuthClientUtilities;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.GetMyUserAttributesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetMyUserAttributesResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.UserAttribute;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

public class AutoLogin implements IStartup {

	/**
	 * Tries to login using saved data (if we are not already logged in ...)
	 * @return 
	 */
	public IStatus autoLogin() {
		String sid = RBACSession.getInstance().getSID(false);
		if (sid == null || sid.isEmpty()) {
			IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
			final String lastSid = prefs.getString(Activator.LAST_SID);
			if (!lastSid.isEmpty()) {
				PortTgextra tgextra = TgAuthClientUtilities.getTgAuthServiceStub();
				GetMyUserAttributesRequest request = new GetMyUserAttributesRequest();
				request.setAuth(lastSid);
				final String lastEPPN = prefs.getString(Activator.LAST_EPPN);
				try {
					GetMyUserAttributesResponse userAttributes = tgextra
							.getMyUserAttributes(request);
					RBACSession.login(lastSid, lastEPPN, true);
					
					StringBuilder sb = new StringBuilder();
					for (UserAttribute attr : userAttributes.getAttribute()) {
						sb.append(MessageFormat.format(
								"{0}={1} ({2} [{3}]) [{4}; {5}]\n", //$NON-NLS-1$
								attr.getName(), attr.getValue(),
								attr.getDisplayname(), attr.getDescription(),
								attr.getLdapname(), attr.getInclass()));
					}
					
					Status status = new Status(IStatus.INFO, Activator.PLUGIN_ID, 
							MessageFormat.format("Automatically logged in as {0} with saved session data.\n\nUser attributes: {1}",	 RBACSession.getInstance().getEPPN(), //$NON-NLS-1$
									sb.toString())
							
							);
					StatusManager.getManager().handle(status);
					return status;
	
				} catch (AuthenticationFault e) {
					Status status = new Status(
							IStatus.WARNING,
							Activator.PLUGIN_ID,
							MessageFormat
									.format("Automatic login using saved session failed: {2}, {0} ({1}). The previous user was {3}.", //$NON-NLS-1$
											e.getFaultInfo().getFaultMessage(),
											e.getFaultInfo().getFaultNo(),
											e.getMessage(),
											lastEPPN));
					StatusManager.getManager().handle(status);
					return status;
				}
			}
		}
		return Status.CANCEL_STATUS;
	}

	@Override
	public void earlyStartup() {
		Job job = new Job(Messages.AutoLogin_TryingAutoLogin) {
	
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				return autoLogin();
			}
			
		};
		job.schedule();
	}

	

}
