/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * A class to create scrolled page areas to use with the metadata's section.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ScrolledPageArea {

	private FormToolkit toolkit = null;
	private Section pageSection = null;

	private Composite pageAreaComposite = null;
	private ScrolledForm scrolledForm = null;

	public ScrolledPageArea(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());

		scrolledForm = toolkit.createScrolledForm(parent);
		scrolledForm.setBackground(parent.getBackground());
		scrolledForm.getBody().setLayout(new GridLayout());

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;

		pageSection = toolkit.createSection(scrolledForm.getBody(),
				ExpandableComposite.EXPANDED);

		pageSection.setBackground(scrolledForm.getBackground());
		pageSection.setLayoutData(gridData);
		pageSection.setLayout(new GridLayout());

		pageSection.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				scrolledForm.reflow(true);
			}
		});

		pageAreaComposite = toolkit.createComposite(pageSection, SWT.BORDER
				| SWT.FILL);
		pageAreaComposite.setBackground(pageSection.getBackground());
		pageAreaComposite.setLayout(new GridLayout(1, true));// TableWrapLayout());

		pageSection.setClient(pageAreaComposite);
	}

	/**
	 * Returns the page's area Composite of the scrolled page to create controls
	 * on it.
	 * 
	 * @return {@link Composite}
	 */
	public Composite getPageAreaComposite() {
		return pageAreaComposite;
	}

	/**
	 * Returns the used scrolledForm.
	 * 
	 * @return {@link ScrolledForm}
	 */
	public ScrolledForm getPageAreaScrolledForm() {
		return scrolledForm;
	}
}
