package info.textgrid.lab.core.metadataeditor;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.metadataeditor.messages"; //$NON-NLS-1$
	public static String MetaDataDialog_cancel;
	public static String MetaDataDialog_makePersistent;
	public static String MetaDataDialog_save;
	public static String MetaDataSection_fetchFailed;
	public static String MetaDataSection_fetchingProjectfile;
	public static String MetaDataSection_getMetadata;
	public static String MetaDataSection_makePersistent;
	public static String MetaDataSection_saved;
	public static String MetaDataSection_saveFailed;
	public static String MetaDataView_autoSave_info;
	public static String MetaDataView_changesDiscarded;
	public static String MetaDataView_changesSaved;
	public static String MetaDataView_copyTEI;
	public static String MetaDataView_copyTEI2Clipboard;
	public static String MetaDataView_fetchingMetadata;
	public static String MetaDataView_fetchingProjectfile;
	public static String MetaDataView_fetchMetadataFailed;
	public static String MetaDataView_lastObject;
	public static String MetaDataView_makePersistent;
	public static String MetaDataView_noMetadata;
	public static String MetaDataView_queryIncomplete;
	public static String MetaDataView_reloadAfterCloseFailed;
	public static String MetaDataView_reloaded;
	public static String MetaDataView_reloadFailed;
	public static String MetaDataView_reloadFailed2;
	public static String MetaDataView_reloading;
	public static String MetaDataView_saveChanges_question;
	public static String MetaDataView_saveChangesQuestion;
	public static String MetaDataView_saveFailed;
	public static String MetaDataView_saveFailedTryAgain;
	public static String ShowMetadataEditorHandler_noPerspective;
	public static String ShowMetadataEditorHandler_noPerspectiveDesc;
	public static String ShowMetadataEditorHandler_openFailed;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
