/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MDTest {

	public class Data {

		public String value;
		public String doc;

		public Data(String value, String doc) {
			this.value = value;
			this.doc = doc;
		}

	}

	private HashMap<String, Data> map = new HashMap<String, MDTest.Data>();
	ArrayList<String> names = new ArrayList<String>();

	private void getAgentRoles() {

		FileInputStream in = null;
		BufferedWriter bufferedWriter = null;

		try {
			in = new FileInputStream("/home/yahya/Desktop/" + "MD-L.xml"); //$NON-NLS-1$ //$NON-NLS-2$
			bufferedWriter = new BufferedWriter(new FileWriter(
					"/home/yahya/Desktop/" + "Out.xml")); //$NON-NLS-1$ //$NON-NLS-2$
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BufferedReader rd = new BufferedReader(new InputStreamReader(in));

		boolean inDoc = false;
		String s, doc = null, value = null;
		try {
			while ((s = rd.readLine()) != null) {
				if (s.contains("value=")) { //$NON-NLS-1$
					inDoc = false;
					doc = null;
					// bufferedWriter.write("<item>");
					// bufferedWriter.write(s.substring(s.indexOf("\"") + 1,
					// s.lastIndexOf("\"")));
					// bufferedWriter.write("</item>\n");
					//
					// System.out.print("<item>");
					// System.out.print(s.substring(s.indexOf("\"") + 1,
					// s.lastIndexOf("\"")));
					// System.out.println("</item>");

					// bufferedWriter.write("c.add(\"");
					// bufferedWriter.write(s.substring(s.indexOf("\"") + 1,
					// s.lastIndexOf("\"")));
					// bufferedWriter.write("\");\n");

					value = s.substring(s.indexOf("\"") + 1, //$NON-NLS-1$
							s.lastIndexOf("\"")); //$NON-NLS-1$

				} else if (s.contains("<xs:documentation")) { //$NON-NLS-1$

					boolean einzeilig = false;
					if (s.contains("</xs:documentation>")) { //$NON-NLS-1$
						einzeilig = true;
					}

					s = s.replaceAll("<.+?>", ""); //$NON-NLS-1$ //$NON-NLS-2$
					doc = s.trim();

					if (einzeilig) {
						map.put(doc, new Data(value, doc));
						names.add(doc);
						continue;
					}

					inDoc = true;

					// doc = s.substring(s.indexOf(">"), endIndex)
				} else if (inDoc && !s.contains("xs:documentation")) { //$NON-NLS-1$
					doc += " " + s.trim(); //$NON-NLS-1$

				} else if (inDoc && s.contains("</xs:documentation>")) { //$NON-NLS-1$

					s = s.replaceAll("<.+?>", ""); //$NON-NLS-1$ //$NON-NLS-2$
					doc += " " + s.trim(); //$NON-NLS-1$

					map.put(doc, new Data(value, doc));
					names.add(doc);
					inDoc = false;
					doc = null;

				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Collections.sort(names);

		// // VARIANTE
		// for (String n : names) {
		//
		// Data d = map.get(n);
		//
		// System.out.print("c.add(\"");
		// System.out.print(d.doc);
		// System.out.println("\");");
		//
		// System.out.print("c.setData(\"");
		// System.out.print(d.doc);
		// System.out.print("\", \"" + d.value);
		// System.out.println("\");");
		//
		// System.out.print("c.setData(\"");
		// System.out.print(d.value);
		// System.out.print("\", \"" + d.doc);
		// System.out.println("\");");
		//
		// System.out.println();
		// }

		int i = 0;

		// VARIANTE
		for (String n : names) {

			Data d = map.get(n);

			System.out.print("a.add(\""); //$NON-NLS-1$
			System.out.print(d.doc);
			System.out.println("\");"); //$NON-NLS-1$

			System.out.print("c.put(\""); //$NON-NLS-1$
			System.out.print(d.doc);
			System.out.print("\", \"" + d.value); //$NON-NLS-1$
			System.out.println("\");"); //$NON-NLS-1$

			System.out.print("c.put(\""); //$NON-NLS-1$
			System.out.print(d.value);
			System.out.print("\", \"" + d.doc); //$NON-NLS-1$
			System.out.println("\");"); //$NON-NLS-1$

			System.out.println();
			i++;
			if (i >= 1000) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i = 0;
			}
		}

		System.err.println(names.size());
	}

	public static void main(String[] args) {

		MDTest m = new MDTest();
		m.getAgentRoles();
	}
}
