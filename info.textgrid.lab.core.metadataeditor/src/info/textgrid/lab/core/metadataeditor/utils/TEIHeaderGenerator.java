package info.textgrid.lab.core.metadataeditor.utils;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;
import info.textgrid.namespaces.middleware.tgsearch.PathGroupType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.PathType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Generates a TEI header for the current object.
 * 
 * Clients can either directly call the static method
 * {@link #generate(TextGridObject, IProgressMonitor)}, or they can
 * {@linkplain #TEIHeaderGenerator(TextGridObject, boolean) create an instance
 * of this class} and {@link #schedule()} it for background execution. When
 * using the latter option, clients can retrieve the genrated header using
 * {@link #getHeader()} after the job has finished. It's also possible to let
 * the job automatically copy the generated header to the clipboard.
 * 
 * FIXME i'm not entirely sure whether this shouldn't move to some utils plugin.
 */
@SuppressWarnings("restriction")
public class TEIHeaderGenerator extends Job {

	private final TextGridObject rootObject;
	private final boolean toClipboard;
	private final boolean toEditor;

	/**
	 * Creates a new header generator job.
	 * 
	 * @param object
	 *            The object for which to generate the TEI header
	 * @param toClipboard
	 *            whether to copy the TEI header to the clipboard and editor
	 *            after it is finished
	 */
	public TEIHeaderGenerator(final TextGridObject object,
			final boolean toClipboard) {
		super(NLS.bind(Messages.TEIHeaderGenerator_generatingHeader, object));
		rootObject = object;
		this.toEditor = this.toClipboard = toClipboard;
	}

	/**
	 * Creates a new header generator job.
	 * 
	 * @param object
	 *            The object for which to generate the TEI header
	 * @param toClipboard
	 *            whether to copy the TEI header to the clipboard after it is
	 *            finished
	 * @param toEditor
	 *            whether to copy the TEI header {@linkplain #toEditor() to the
	 *            open editor} after it is finished.
	 */
	public TEIHeaderGenerator(final TextGridObject object,
			final boolean toClipboard, final boolean toEditor) {
		super(NLS.bind(Messages.TEIHeaderGenerator_generatingHeader, object));
		rootObject = object;
		this.toClipboard = toClipboard;
		this.toEditor = toEditor;
	}

	private static final QName OBJECTS = new QName(
			TextGridObject.TEXTGRID_METADATA_NAMESPACE, "objects"); //$NON-NLS-1$
	private String header;

	/**
	 * Generates a TEI header for the given object.
	 * 
	 * @param rootObject
	 *            the object at the root of the relation tree
	 * @param monitor
	 *            a progress monitor or <code>null</code>
	 * @return a string containing the TEI header
	 * @throws IOException
	 *             when we fail to read the stylesheet
	 * @throws CrudServiceException
	 *             when we fail to read metadata
	 * @throws XMLStreamException
	 *             when the metadata is so invalid we can't concatenate it
	 * @throws TransformerException
	 *             when the transformation to TEI fails
	 */
	public static String generate(final TextGridObject rootObject,
			final IProgressMonitor monitor) throws IOException,
			CrudServiceException, XMLStreamException, TransformerException {
		final SubMonitor progress = SubMonitor.convert(monitor,
				NLS.bind(Messages.TEIHeaderGenerator_generatingHeader, rootObject), 100);
		final StreamSource mdSource = getAllMetadataSource(rootObject,
				progress.newChild(70));

		final InputStream xslStream = FileLocator.openStream(Activator
				.getDefault().getBundle(), new Path("resources") //$NON-NLS-1$
				.append("md2tei.xsl"), false); //$NON-NLS-1$
		final StreamSource xslSource = new StreamSource(xslStream);
		try {
			final Transformer transformer = TransformerFactory.newInstance()
					.newTransformer(xslSource);
			final ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
			final StreamResult result = new StreamResult(resultStream);
			progress.worked(10);
			transformer.transform(mdSource, result);
			final String tei = resultStream.toString("UTF-8"); //$NON-NLS-1$
			progress.worked(20);

			return tei;
		} catch (final TransformerConfigurationException e) {
			throw new IllegalStateException(e);
		} catch (final TransformerFactoryConfigurationError e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Returns a source that contains XML representations of all metadata
	 * records related to the given rootObject in upwards direction.
	 * 
	 * I.e. if you have item <var>i</var> that is part of an edition
	 * <var>e</var> that is associated with work <var>w</var>, you get a source
	 * containing the metadata records of i, e, and w in that order, all
	 * surrounded by a convenient root element.
	 * 
	 * @param monitor
	 *            TODO
	 * @throws OfflineException
	 * @throws XMLStreamException
	 * @throws CrudServiceException
	 * 
	 */
	private static StreamSource getAllMetadataSource(
			final TextGridObject rootObject, final IProgressMonitor monitor)
			throws OfflineException, XMLStreamException, CrudServiceException {
		final SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(
				Messages.TEIHeaderGenerator_fetchingMetadata, rootObject), 50);

		final ArrayList<URI> uris = Lists.newArrayList();
		if (!rootObject.isNew()) {
			final SearchClient searchClient = new SearchClient(ConfClient
					.getInstance().getValue(ConfservClientConstants.TG_SEARCH));
			searchClient.setSid(RBACSession.getInstance().getSID(false));
			final PathResponse response = searchClient.getPath(rootObject
					.getURI().toASCIIString());
			progress.worked(10);

			for (final PathGroupType group : response.getPathGroup())
				for (final PathType path : group.getPath())
					for (final EntryType entry : path.getEntry())
						uris.add(URI.create(entry.getTextgridUri().trim()));
		} else {
			progress.worked(10);
		}
		uris.add(rootObject.getURI());
		Collections.reverse(uris);
		progress.worked(5);
		progress.setWorkRemaining(10 * uris.size() + 10);

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEventWriter xml;
		try {
			xml = XMLOutputFactory.newInstance().createXMLEventWriter(baos,
					"UTF-8"); //$NON-NLS-1$
			final XMLEventFactory events = XMLEventFactory.newInstance();
			xml.setDefaultNamespace(TextGridObject.TEXTGRID_METADATA_NAMESPACE);
			xml.add(events.createStartDocument());
			xml.add(events.createStartElement(
					OBJECTS,
					null,
					ImmutableList
							.of(events
									.createNamespace(TextGridObject.TEXTGRID_METADATA_NAMESPACE))
							.iterator()));
			progress.worked(5);
			for (final URI uri : uris) {
				final TextGridObject object = TextGridObject.getInstance(uri,
						true);
				final ObjectType metadata = object.getMetadataForReading();
				progress.worked(7);
				Marshaller marshaller;
				try {
					marshaller = JAXBContext.newInstance(ObjectType.class)
							.createMarshaller();
					marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
					marshaller.marshal(metadata, xml);
				} catch (final JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				progress.worked(3);
			}

			xml.add(events.createEndDocument());
			xml.close();
			final StreamSource mdSource = new StreamSource(
					new ByteArrayInputStream(baos.toByteArray()));
			progress.worked(5);
			return mdSource;
		} catch (final FactoryConfigurationError e1) {
			throw new IllegalStateException(e1);
		}

	}

	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		try {
			header = generate(rootObject, monitor);
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;

			if (toClipboard || toEditor) {
				final UIJob uiJob = new UIJob("") { //$NON-NLS-1$

					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						if (toClipboard) {
							final Clipboard clipboard = new Clipboard(
									getDisplay());
							clipboard
									.setContents(new Object[] { getHeader() },
											new Transfer[] { TextTransfer
													.getInstance() });
						}

						if (toEditor)
							toEditor();

						return Status.OK_STATUS;
					}
				};
				uiJob.setSystem(true);
				uiJob.schedule();
			}

			return Status.OK_STATUS;
		} catch (final CrudServiceException e) {
			return new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					NLS.bind(
							Messages.TEIHeaderGenerator_readMetadata_failed,
							rootObject, e.getMessage()), e);
		} catch (final IOException e) {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
					Messages.TEIHeaderGenerator_generateHeader_failed,
					rootObject, e.getLocalizedMessage()));
		} catch (final XMLStreamException e) {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
					Messages.TEIHeaderGenerator_generateHeader_failed,
					rootObject, e.getLocalizedMessage()));
		} catch (final TransformerException e) {
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
					Messages.TEIHeaderGenerator_generateHeader_failed,
					rootObject, e.getLocalizedMessage()));
		}
	}

	/**
	 * Returns the object for which this job can run.
	 */
	public TextGridObject getRootObject() {
		return rootObject;
	}

	/**
	 * After this job has successfully finished, returns the header that had
	 * been generated.
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Replaces the existing TEI header for the document with the generated one.
	 * All the following conditions must be true:
	 * <ul>
	 * <li>There must be an open XML editor with {@linkplain #getRootObject()
	 * the current object} in the active workbench window</li>
	 * <li>The XML editor must contain an XML document that contains a teiHeader
	 * element</li>
	 * <li>The new TEI header must already have been generated (
	 * {@link #run(IProgressMonitor)}, {@link #getHeader()})</li>
	 * <li>This method is called from the UI thread.</li>
	 * </ul>
	 * The header is currently inserted in a non-semantic, text-only matter;
	 * i.e. it will probably contain a superfluous namespace declaration and no
	 * indention.
	 */
	public void toEditor() {
		final IDocument document = getEditorDocument(rootObject);
		if (document instanceof IStructuredDocument) {
			final IStructuredModel model = StructuredModelManager
					.getModelManager().getModelForEdit(
							(IStructuredDocument) document);
			if (model != null)
				try {
					final IndexedRegion rootRegion = model.getIndexedRegion(0);
					final Node node = AdapterUtils.getAdapter(rootRegion,
							org.w3c.dom.Node.class);
					if (node == null)
						return;
					final Document doc = node.getOwnerDocument();
					final NodeList teiHeaderElements = doc
							.getElementsByTagName("teiHeader"); //$NON-NLS-1$
					if (teiHeaderElements.getLength() > 0) {
						final Element header = (Element) teiHeaderElements
								.item(0);
						final IndexedRegion headerRegion = AdapterUtils
								.getAdapter(header, IndexedRegion.class);
						document.replace(
								headerRegion.getStartOffset(),
								headerRegion.getEndOffset()
										- headerRegion.getStartOffset(),
								getHeader());
					}
				} catch (final BadLocationException e) {
					StatusManager
							.getManager()
							.handle(new Status(IStatus.ERROR,
									Activator.PLUGIN_ID,
									Messages.TEIHeaderGenerator_replaceHeaderError, e));
				} finally {
					model.releaseFromEdit();
				}
		}
	}

	/**
	 * If the given object is currently open in an XML editor, returns the
	 * corresponding {@link IDocument}, otherwise <code>null</code>.
	 */
	private static IDocument getEditorDocument(TextGridObject object) {
		final IEditorInput input = AdapterUtils.getAdapter(object,
				IEditorInput.class);
		if (input == null)
			return null;

		final IEditorPart editor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findEditor(input);
		final ISourceViewer sourceViewer = AdapterUtils.getAdapter(editor,
				ISourceViewer.class);
		if (sourceViewer == null)
			return null;

		final IDocument document = sourceViewer.getDocument();
		return document;
	}
}
