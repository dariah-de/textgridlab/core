/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.utils.ProjectNavigator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 *         A dialog to select a Work TG-object
 * 
 */
public class FetchTGObjectsDialog extends TitleAreaDialog implements
		ISelectionChangedListener {

	private Text text = null;
	private ProjectNavigator viewer = null;

	private TextGridObject selectedObject = null;

	public FetchTGObjectsDialog(Shell parentShell) {
		super(parentShell);
	}

	public static void openDialog(Text text) {
		FetchTGObjectsDialog dialog = new FetchTGObjectsDialog(PlatformUI
				.getWorkbench().getActiveWorkbenchWindow().getShell());
		dialog.text = text;
		dialog.open();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));

		setTitle(Messages.FetchTGObjectsDialog_selectWorkObject_title);

		Label intro = new Label(control, SWT.WRAP);
		intro.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		intro.setText(Messages.FetchTGObjectsDialog_selectWorkObject_desc);

		createObjectsGroup(control);

		return control;
	}

	private void createObjectsGroup(Composite control) {
		Group objectsGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		objectsGroup
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		objectsGroup.setLayout(new GridLayout(1, true));

		GridData projectTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
		projectTreeData.heightHint = 200;
		objectsGroup.setLayoutData(projectTreeData);

		viewer = new ProjectNavigator(objectsGroup);
		viewer.getControl().setLayoutData(projectTreeData);
		viewer.setWorkFilter();

		viewer.addSelectionChangedListener(this);
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {

		if (event.getSelectionProvider() == viewer) {
			Object object = null;
			if (event.getSelection() instanceof IStructuredSelection) {
				object = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
			}
			if (object != null) {
				selectedObject = AdapterUtils.getAdapter(object,
						TextGridObject.class);
				try {
					if (selectedObject == null
							|| !"text/tg.work+xml".equals(selectedObject //$NON-NLS-1$
									.getContentTypeID())) {
						selectedObject = null;
						setErrorMessage(Messages.FetchTGObjectsDialog_selectWorkObject_error);
						return;
					}
				} catch (CoreException e1) {
					Activator.handleError(e1);
					return;
				}
				setErrorMessage(null);
				try {
					setMessage(NLS.bind(Messages.FetchTGObjectsDialog_selectedWorkObject,
							selectedObject.getNameCandidate()));
				} catch (CoreException e) {
					Activator.handleError(e);
				}
			} else {
				setErrorMessage(Messages.FetchTGObjectsDialog_selectWorkObject_error);
			}
		}
	}

	@Override
	protected void okPressed() {
		if (selectedObject != null) {
			text.setText(selectedObject.getURI().toString());
			super.okPressed();
		}
	}
}