package info.textgrid.lab.core.metadataeditor.utils;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.metadataeditor.utils.messages"; //$NON-NLS-1$
	public static String AddItems_AncientGreek;
	public static String AddItems_Arabic;
	public static String AddItems_Belarusian;
	public static String AddItems_Bulgarian;
	public static String AddItems_Catalan;
	public static String AddItems_Chinese;
	public static String AddItems_Croatian;
	public static String AddItems_Czech;
	public static String AddItems_Dutch;
	public static String AddItems_English;
	public static String AddItems_French;
	public static String AddItems_German;
	public static String AddItems_Hebrew;
	public static String AddItems_Hungarian;
	public static String AddItems_Italian;
	public static String AddItems_Japanese;
	public static String AddItems_Latin;
	public static String AddItems_Macedonian;
	public static String AddItems_MiddleDutch;
	public static String AddItems_MiddleEnglish;
	public static String AddItems_MiddleFrench;
	public static String AddItems_MiddleHighGerman;
	public static String AddItems_MiddleLowGerman;
	public static String AddItems_ModernGreek;
	public static String AddItems_OldDutch;
	public static String AddItems_OldEnglish;
	public static String AddItems_OldFrench;
	public static String AddItems_OldHighGerman;
	public static String AddItems_OldHungarian;
	public static String AddItems_OldRussian;
	public static String AddItems_OldSpanish;
	public static String AddItems_Persian;
	public static String AddItems_Polish;
	public static String AddItems_Portuguese;
	public static String AddItems_Romanian;
	public static String AddItems_Russian;
	public static String AddItems_Serbian;
	public static String AddItems_Slovak;
	public static String AddItems_Spanish;
	public static String AddItems_Swedish;
	public static String AddItems_Ukrainian;
	public static String FetchTGObjectsDialog_selectedWorkObject;
	public static String FetchTGObjectsDialog_selectWorkObject_desc;
	public static String FetchTGObjectsDialog_selectWorkObject_error;
	public static String FetchTGObjectsDialog_selectWorkObject_title;
	public static String OMUtil_itemNotFound;
	public static String TEIHeaderGenerator_fetchingMetadata;
	public static String TEIHeaderGenerator_generateHeader_failed;
	public static String TEIHeaderGenerator_generatingHeader;
	public static String TEIHeaderGenerator_readMetadata_failed;
	public static String TEIHeaderGenerator_replaceHeaderError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
