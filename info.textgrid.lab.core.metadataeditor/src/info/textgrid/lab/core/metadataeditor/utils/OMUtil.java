/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import info.textgrid.lab.core.metadataeditor.Activator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.xpath.AXIOMXPath;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.osgi.util.NLS;
import org.jaxen.JaxenException;

/**
 * This is the class which can be used as a base class for utilities to work
 * with OMItems.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class OMUtil {

	protected List<OMItem> items = new ArrayList<OMItem>();
	private static final String ORDER_CONFIG_FILE = "-order.conf"; //$NON-NLS-1$

	/**
	 * Searches for a certain element in an OMElement and returns it if found
	 * otherwise returns null.
	 * 
	 * @param elementName
	 *            the name of the searched element
	 * @param root
	 *            where to search
	 * @param namespace
	 *            the the namespace of the searched element
	 * @param prefix
	 *            the prefix of the namespace
	 * 
	 * @return OMElement the found element as OMElement or null.
	 * 
	 * @throws JaxenException
	 */
	public static OMElement getElementWithName(String elementName,
			OMElement root, String namespace, String prefix)
			throws JaxenException {

		AXIOMXPath xpath = new AXIOMXPath("child::" + prefix + ":" //$NON-NLS-1$ //$NON-NLS-2$
				+ elementName + " | self::" + prefix + ":" + elementName); //$NON-NLS-1$ //$NON-NLS-2$
		xpath.addNamespace(prefix, namespace);
		OMElement elem = (OMElement) xpath.selectSingleNode(root);

		return elem;
	}

	/**
	 * Searches for a certain element in an OMElement and returns a list of
	 * elements if found otherwise returns null.
	 * 
	 * @param elementName
	 *            the name of the searched element
	 * @param root
	 *            where to search
	 * @param namespace
	 *            the the namespace of the searched element
	 * @param prefix
	 *            the prefix of the namespace
	 * 
	 * @return List a list of the elements or null
	 * 
	 * @throws JaxenException
	 */
	@SuppressWarnings("unchecked")
	public static List getElementListWithName(String elementName,
			OMElement root, String namespace, String prefix)
			throws JaxenException {

		AXIOMXPath xpath = new AXIOMXPath("child::" + prefix + ":" //$NON-NLS-1$ //$NON-NLS-2$
				+ elementName + " | self::" + prefix + ":" + elementName); //$NON-NLS-1$ //$NON-NLS-2$
		xpath.addNamespace(prefix, namespace);
		List elems = xpath.selectNodes(root);

		return elems;
	}

	/**
	 * Searches for a certain element in an OMElement and returns a list of
	 * elements if found otherwise returns null.
	 * 
	 * @param elementName
	 *            the name of the searched element
	 * @param root
	 *            where to search
	 * @param namespace
	 *            the the namespace of the searched element
	 * @param prefix
	 *            the prefix of the namespace
	 * 
	 * @return List a list of the elements or null
	 * 
	 * @throws JaxenException
	 */
	@SuppressWarnings("unchecked")
	public static List getElementListWithName_Deep(String elementName,
			OMElement root, String namespace, String prefix)
			throws JaxenException {

		AXIOMXPath xpath = new AXIOMXPath("//" + prefix + ":" + elementName); //$NON-NLS-1$ //$NON-NLS-2$
		xpath.addNamespace(prefix, namespace);
		List elems = xpath.selectNodes(root);

		return elems;
	}

	/**
	 * Adds a list of {@link OMItem} to this instance.
	 * 
	 * @param items
	 */
	public void add(final List<OMItem> items) {

		if (this.items.isEmpty())
			this.items = items;
	}

	/**
	 * Searches for an {@link OMItem} by his id and returns it if found
	 * otherwise returns null.
	 */
	protected OMItem getOMItemById(String id) {

		for (OMItem item : items) {
			if (item.getId().equals(id))
				return item;
		}
		return null;
	}

	/**
	 * Get an ordered list of all {@link OMItem} objects.
	 * 
	 * @return
	 */
	@Deprecated
	protected List<OMItem> getOrderedItems(String elemName) {
		ArrayList<OMItem> l = new ArrayList<OMItem>();

		URL url = FileLocator.find(Activator.getDefault().getBundle(),
				new Path(elemName + ORDER_CONFIG_FILE), null);

		InputStream in = null;
		try {
			url = FileLocator.resolve(url);
			in = url.openStream();

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));

			String s = null;
			while ((s = reader.readLine()) != null) {
				OMItem item = getOMItemById(s.trim());
				if (item != null) {
					l.add(item);
				}
			}

		} catch (Exception e) {
			Activator.handleError(e, NLS.bind(Messages.OMUtil_itemNotFound, ORDER_CONFIG_FILE));
			return this.items;
		}

		if (l.size() < this.items.size()) {
			for (OMItem i : this.items) {
				if (!l.contains(i))
					l.add(i);
				if (l.size() == this.items.size())
					break;
			}
		}

		return l;
	}
}