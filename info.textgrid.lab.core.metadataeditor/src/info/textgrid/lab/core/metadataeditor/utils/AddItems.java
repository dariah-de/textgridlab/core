/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import java.util.Iterator;

import org.dom4j.Element;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;

/**
 * This class is to add Items to List or Combo control elements.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class AddItems {

	/**
	 * Adds Items to List or Combo control elements.
	 * 
	 * @param control
	 *            the control which gets the items.
	 * @param rootElem
	 *            the xul-element for the list or combo
	 */
	@SuppressWarnings("unchecked")
	public static void addItemsToControl(Control control, Element rootElem) {

		int index = -1;
		int selected = -1;

		if (control instanceof Combo) {
			Combo c = (Combo) control;
			for (Iterator i = rootElem.elementIterator("item"); i.hasNext();) { //$NON-NLS-1$
				Element item = (Element) i.next();
				c.add(item.getText().trim());

				++index;

				if (item.attribute("selected") != null) { //$NON-NLS-1$
					if (item.attribute("selected").getText().equals("true")) //$NON-NLS-1$ //$NON-NLS-2$
						selected = index;
				}
			}

			if (selected >= 0)
				c.select(selected);
		} else if (control instanceof List) {
			List l = (List) control;
			for (Iterator i = rootElem.elementIterator("item"); i.hasNext();) { //$NON-NLS-1$
				Element item = (Element) i.next();
				l.add(item.getText().trim());

				++index;

				if (item.attribute("selected") != null) { //$NON-NLS-1$
					if (item.attribute("selected").getText().equals("true")) //$NON-NLS-1$ //$NON-NLS-2$
						selected = index;
				}
			}

			if (selected >= 0)
				l.select(selected);
		}
	}

	public static void addAgentItems(Combo c) {

		c.add("actor"); //$NON-NLS-1$
		c.add("adapter"); //$NON-NLS-1$
		c.add("analyst"); //$NON-NLS-1$
		c.add("animator"); //$NON-NLS-1$
		c.add("annotator"); //$NON-NLS-1$
		c.add("applicant"); //$NON-NLS-1$
		c.add("architect"); //$NON-NLS-1$
		c.add("arranger"); //$NON-NLS-1$
		c.add("artCopyist"); //$NON-NLS-1$
		c.add("artist"); //$NON-NLS-1$
		c.add("artisticDirector"); //$NON-NLS-1$
		c.add("assignee"); //$NON-NLS-1$
		c.add("associatedName"); //$NON-NLS-1$
		c.add("attributedName"); //$NON-NLS-1$
		c.add("auctioneer"); //$NON-NLS-1$
		c.add("author"); //$NON-NLS-1$
		c.add("authorInQuotations"); //$NON-NLS-1$
		c.add("authorOfAfterword"); //$NON-NLS-1$
		c.add("authorOfDialog"); //$NON-NLS-1$
		c.add("authorOfIntroduction"); //$NON-NLS-1$
		c.add("authorOfScreenplay"); //$NON-NLS-1$
		c.add("bibliographicAntecedent"); //$NON-NLS-1$
		c.add("binder"); //$NON-NLS-1$
		c.add("bindingDesigner"); //$NON-NLS-1$
		c.add("blurbWriter"); //$NON-NLS-1$
		c.add("bookDesigner"); //$NON-NLS-1$
		c.add("bookProducer"); //$NON-NLS-1$
		c.add("bookJacketDesigner"); //$NON-NLS-1$
		c.add("bookplateDesigner"); //$NON-NLS-1$
		c.add("bookseller"); //$NON-NLS-1$
		c.add("calligrapher"); //$NON-NLS-1$
		c.add("cartographer"); //$NON-NLS-1$
		c.add("censor"); //$NON-NLS-1$
		c.add("cinematographer"); //$NON-NLS-1$
		c.add("client"); //$NON-NLS-1$
		c.add("collaborator"); //$NON-NLS-1$
		c.add("collector"); //$NON-NLS-1$
		c.add("collotyper"); //$NON-NLS-1$
		c.add("colorist"); //$NON-NLS-1$
		c.add("commentator"); //$NON-NLS-1$
		c.add("commentatorForWrittenText"); //$NON-NLS-1$
		c.add("compiler"); //$NON-NLS-1$
		c.add("complainant"); //$NON-NLS-1$
		c.add("complainantAppellant"); //$NON-NLS-1$
		c.add("complainantAppellee"); //$NON-NLS-1$
		c.add("composer"); //$NON-NLS-1$
		c.add("compositor"); //$NON-NLS-1$
		c.add("conceptor"); //$NON-NLS-1$
		c.add("conductor"); //$NON-NLS-1$
		c.add("conservator"); //$NON-NLS-1$
		c.add("consultant"); //$NON-NLS-1$
		c.add("consultantToProject"); //$NON-NLS-1$
		c.add("contestant"); //$NON-NLS-1$
		c.add("contestantAppellant"); //$NON-NLS-1$
		c.add("contestantAppellee"); //$NON-NLS-1$
		c.add("contestee"); //$NON-NLS-1$
		c.add("contesteeAppellant"); //$NON-NLS-1$
		c.add("contesteeAppellee"); //$NON-NLS-1$
		c.add("contractor"); //$NON-NLS-1$
		c.add("contributor"); //$NON-NLS-1$
		c.add("copyrightClaimant"); //$NON-NLS-1$
		c.add("copyrightHolder"); //$NON-NLS-1$
		c.add("corrector"); //$NON-NLS-1$
		c.add("correspondent"); //$NON-NLS-1$
		c.add("costumeDesigner"); //$NON-NLS-1$
		c.add("coverDesigner"); //$NON-NLS-1$
		c.add("creator"); //$NON-NLS-1$
		c.add("curatorOfAnExhibition"); //$NON-NLS-1$
		c.add("dancer"); //$NON-NLS-1$
		c.add("dataContributor"); //$NON-NLS-1$
		c.add("dataManager"); //$NON-NLS-1$
		c.add("dedicatee"); //$NON-NLS-1$
		c.add("dedicator"); //$NON-NLS-1$
		c.add("defendant"); //$NON-NLS-1$
		c.add("defendantAppellant"); //$NON-NLS-1$
		c.add("defendantAppellee"); //$NON-NLS-1$
		c.add("degreeGrantor"); //$NON-NLS-1$
		c.add("delineator"); //$NON-NLS-1$
		c.add("depicted"); //$NON-NLS-1$
		c.add("depositor"); //$NON-NLS-1$
		c.add("designer"); //$NON-NLS-1$
		c.add("director"); //$NON-NLS-1$
		c.add("dissertant"); //$NON-NLS-1$
		c.add("distributionPlace"); //$NON-NLS-1$
		c.add("distributor"); //$NON-NLS-1$
		c.add("donor"); //$NON-NLS-1$
		c.add("draftsman"); //$NON-NLS-1$
		c.add("dubiousAuthor"); //$NON-NLS-1$
		c.add("editor"); //$NON-NLS-1$
		c.add("electrician"); //$NON-NLS-1$
		c.add("electrotyper"); //$NON-NLS-1$
		c.add("engineer"); //$NON-NLS-1$
		c.add("engraver"); //$NON-NLS-1$
		c.add("etcher"); //$NON-NLS-1$
		c.add("eventPlace"); //$NON-NLS-1$
		c.add("expert"); //$NON-NLS-1$
		c.add("facsimilist"); //$NON-NLS-1$
		c.add("fieldDirector"); //$NON-NLS-1$
		c.add("filmEditor"); //$NON-NLS-1$
		c.add("firstParty"); //$NON-NLS-1$
		c.add("forger"); //$NON-NLS-1$
		c.add("formerOwner"); //$NON-NLS-1$
		c.add("funder"); //$NON-NLS-1$
		c.add("geographicInformationSpecialist"); //$NON-NLS-1$
		c.add("honoree"); //$NON-NLS-1$
		c.add("host"); //$NON-NLS-1$
		c.add("illuminator"); //$NON-NLS-1$
		c.add("illustrator"); //$NON-NLS-1$
		c.add("inscriber"); //$NON-NLS-1$
		c.add("instrumentalist"); //$NON-NLS-1$
		c.add("interviewee"); //$NON-NLS-1$
		c.add("interviewer"); //$NON-NLS-1$
		c.add("inventor"); //$NON-NLS-1$
		c.add("laboratory"); //$NON-NLS-1$
		c.add("laboratoryDirector"); //$NON-NLS-1$
		c.add("lead"); //$NON-NLS-1$
		c.add("landscapeArchitect"); //$NON-NLS-1$
		c.add("lender"); //$NON-NLS-1$
		c.add("libelant"); //$NON-NLS-1$
		c.add("libelantAppellant"); //$NON-NLS-1$
		c.add("libelantAppellee"); //$NON-NLS-1$
		c.add("libelee"); //$NON-NLS-1$
		c.add("libeleeAppellant"); //$NON-NLS-1$
		c.add("libeleeAppellee"); //$NON-NLS-1$
		c.add("librettist"); //$NON-NLS-1$
		c.add("licensee"); //$NON-NLS-1$
		c.add("licensor"); //$NON-NLS-1$
		c.add("lightingDesigner"); //$NON-NLS-1$
		c.add("lithographer"); //$NON-NLS-1$
		c.add("lyricist"); //$NON-NLS-1$
		c.add("manufacturer"); //$NON-NLS-1$
		c.add("marbler"); //$NON-NLS-1$
		c.add("markupEditor"); //$NON-NLS-1$
		c.add("metadataContact"); //$NON-NLS-1$
		c.add("metalengraver"); //$NON-NLS-1$
		c.add("moderator"); //$NON-NLS-1$
		c.add("monitor"); //$NON-NLS-1$
		c.add("musicCopyist"); //$NON-NLS-1$
		c.add("musicalDirector"); //$NON-NLS-1$
		c.add("musician"); //$NON-NLS-1$
		c.add("narrator"); //$NON-NLS-1$
		c.add("opponent"); //$NON-NLS-1$
		c.add("organizerOfMeeting"); //$NON-NLS-1$
		c.add("originator"); //$NON-NLS-1$
		c.add("other"); //$NON-NLS-1$
		c.add("owner"); //$NON-NLS-1$
		c.add("papermaker"); //$NON-NLS-1$
		c.add("patentApplicant"); //$NON-NLS-1$
		c.add("patentHolder"); //$NON-NLS-1$
		c.add("patron"); //$NON-NLS-1$
		c.add("performer"); //$NON-NLS-1$
		c.add("permittingAgency"); //$NON-NLS-1$
		c.add("photographer"); //$NON-NLS-1$
		c.add("plaintiff"); //$NON-NLS-1$
		c.add("plaintiffAppellant"); //$NON-NLS-1$
		c.add("plaintiffAppellee"); //$NON-NLS-1$
		c.add("platemaker"); //$NON-NLS-1$
		c.add("printer"); //$NON-NLS-1$
		c.add("printerOfPlates"); //$NON-NLS-1$
		c.add("printmaker"); //$NON-NLS-1$
		c.add("processContact"); //$NON-NLS-1$
		c.add("producer"); //$NON-NLS-1$
		c.add("productionManager"); //$NON-NLS-1$
		c.add("productionPersonnel"); //$NON-NLS-1$
		c.add("programmer"); //$NON-NLS-1$
		c.add("projectDirector"); //$NON-NLS-1$
		c.add("proofreader"); //$NON-NLS-1$
		c.add("publicationPlace"); //$NON-NLS-1$
		c.add("publisher"); //$NON-NLS-1$
		c.add("publishingDirector"); //$NON-NLS-1$
		c.add("puppeteer"); //$NON-NLS-1$
		c.add("recipient"); //$NON-NLS-1$
		c.add("recordingEngineer"); //$NON-NLS-1$
		c.add("redactor"); //$NON-NLS-1$
		c.add("renderer"); //$NON-NLS-1$
		c.add("reporter"); //$NON-NLS-1$
		c.add("repository"); //$NON-NLS-1$
		c.add("researchTeamHead"); //$NON-NLS-1$
		c.add("researchTeamMember"); //$NON-NLS-1$
		c.add("researcher"); //$NON-NLS-1$
		c.add("respondent"); //$NON-NLS-1$
		c.add("respondentAppellant"); //$NON-NLS-1$
		c.add("respondentAppellee"); //$NON-NLS-1$
		c.add("responsibleParty"); //$NON-NLS-1$
		c.add("restager"); //$NON-NLS-1$
		c.add("reviewer"); //$NON-NLS-1$
		c.add("rubricator"); //$NON-NLS-1$
		c.add("scenarist"); //$NON-NLS-1$
		c.add("scientificAdvisor"); //$NON-NLS-1$
		c.add("scribe"); //$NON-NLS-1$
		c.add("sculptor"); //$NON-NLS-1$
		c.add("secondParty"); //$NON-NLS-1$
		c.add("secretary"); //$NON-NLS-1$
		c.add("setdesigner"); //$NON-NLS-1$
		c.add("signer"); //$NON-NLS-1$
		c.add("singer"); //$NON-NLS-1$
		c.add("soundDesigner"); //$NON-NLS-1$
		c.add("speaker"); //$NON-NLS-1$
		c.add("sponsor"); //$NON-NLS-1$
		c.add("stageManager"); //$NON-NLS-1$
		c.add("standardsBody"); //$NON-NLS-1$
		c.add("stereotyper"); //$NON-NLS-1$
		c.add("storyteller"); //$NON-NLS-1$
		c.add("supportingHost"); //$NON-NLS-1$
		c.add("surveyor"); //$NON-NLS-1$
		c.add("teacher"); //$NON-NLS-1$
		c.add("technicalDirector"); //$NON-NLS-1$
		c.add("thesisAdvisor"); //$NON-NLS-1$
		c.add("transcriber"); //$NON-NLS-1$
		c.add("translator"); //$NON-NLS-1$
		c.add("typeDesigner"); //$NON-NLS-1$
		c.add("typographer"); //$NON-NLS-1$
		c.add("universityPlace"); //$NON-NLS-1$
		c.add("videographer"); //$NON-NLS-1$
		c.add("vocalist"); //$NON-NLS-1$
		c.add("Witness"); //$NON-NLS-1$
		c.add("woodEngraver"); //$NON-NLS-1$
		c.add("woodcutter"); //$NON-NLS-1$
		c.add("writerOfAccompanyingMaterial"); //$NON-NLS-1$
		c.add("other"); //$NON-NLS-1$

		c.select(15);

	}

	public static void addScriptItems(Combo c) {

		c.add("Arabic"); //$NON-NLS-1$
		c.setData("Arabic", "Arab"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Arab", "Arabic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Armenian"); //$NON-NLS-1$
		c.setData("Armenian", "Armn"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Armn", "Armenian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Avestan"); //$NON-NLS-1$
		c.setData("Avestan", "Avst"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Avst", "Avestan"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Balinese"); //$NON-NLS-1$
		c.setData("Balinese", "Bali"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Bali", "Balinese"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Bamum"); //$NON-NLS-1$
		c.setData("Bamum", "Bamu"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Bamu", "Bamum"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Bassa Vah"); //$NON-NLS-1$
		c.setData("Bassa Vah", "Bass"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Bass", "Bassa Vah"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Batak"); //$NON-NLS-1$
		c.setData("Batak", "Batk"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Batk", "Batak"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Bengali"); //$NON-NLS-1$
		c.setData("Bengali", "Beng"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Beng", "Bengali"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Blissymbols"); //$NON-NLS-1$
		c.setData("Blissymbols", "Blis"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Blis", "Blissymbols"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Book Pahlavi"); //$NON-NLS-1$
		c.setData("Book Pahlavi", "Phlv"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Phlv", "Book Pahlavi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Bopomofo"); //$NON-NLS-1$
		c.setData("Bopomofo", "Bopo"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Bopo", "Bopomofo"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Brahmi"); //$NON-NLS-1$
		c.setData("Brahmi", "Brah"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Brah", "Brahmi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Braille"); //$NON-NLS-1$
		c.setData("Braille", "Brai"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Brai", "Braille"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Buginese"); //$NON-NLS-1$
		c.setData("Buginese", "Bugi"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Bugi", "Buginese"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Buhid"); //$NON-NLS-1$
		c.setData("Buhid", "Buhd"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Buhd", "Buhid"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Carian"); //$NON-NLS-1$
		c.setData("Carian", "Cari"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cari", "Carian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Chakma"); //$NON-NLS-1$
		c.setData("Chakma", "Cakm"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cakm", "Chakma"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cham"); //$NON-NLS-1$
		c.setData("Cham", "Cham"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cham", "Cham"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cherokee"); //$NON-NLS-1$
		c.setData("Cherokee", "Cher"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cher", "Cherokee"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cirth"); //$NON-NLS-1$
		c.setData("Cirth", "Cirt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cirt", "Cirth"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Code for inherited script"); //$NON-NLS-1$
		c.setData("Code for inherited script", "Zinh"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Zinh", "Code for inherited script"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Coptic"); //$NON-NLS-1$
		c.setData("Coptic", "Copt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Copt", "Coptic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cuneiform, Sumero-Akkadian"); //$NON-NLS-1$
		c.setData("Cuneiform, Sumero-Akkadian", "Xsux"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Xsux", "Cuneiform, Sumero-Akkadian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cypriot"); //$NON-NLS-1$
		c.setData("Cypriot", "Cprt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cprt", "Cypriot"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cyrillic"); //$NON-NLS-1$
		c.setData("Cyrillic", "Cyrl"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cyrl", "Cyrillic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Cyrillic (Old Church Slavonic variant)"); //$NON-NLS-1$
		c.setData("Cyrillic (Old Church Slavonic variant)", "Cyrs"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cyrs", "Cyrillic (Old Church Slavonic variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Deseret (Mormon)"); //$NON-NLS-1$
		c.setData("Deseret (Mormon)", "Dsrt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Dsrt", "Deseret (Mormon)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Devanagari (Nagari)"); //$NON-NLS-1$
		c.setData("Devanagari (Nagari)", "Deva"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Deva", "Devanagari (Nagari)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Egyptian demotic"); //$NON-NLS-1$
		c.setData("Egyptian demotic", "Egyd"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Egyd", "Egyptian demotic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Egyptian hieratic"); //$NON-NLS-1$
		c.setData("Egyptian hieratic", "Egyh"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Egyh", "Egyptian hieratic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Egyptian hieroglyphs"); //$NON-NLS-1$
		c.setData("Egyptian hieroglyphs", "Egyp"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Egyp", "Egyptian hieroglyphs"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Ethiopic (Geʻez)"); //$NON-NLS-1$
		c.setData("Ethiopic (Geʻez)", "Ethi"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Ethi", "Ethiopic (Geʻez)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Georgian (Mkhedruli)"); //$NON-NLS-1$
		c.setData("Georgian (Mkhedruli)", "Geor"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Geor", "Georgian (Mkhedruli)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Glagolitic"); //$NON-NLS-1$
		c.setData("Glagolitic", "Glag"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Glag", "Glagolitic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Gothic"); //$NON-NLS-1$
		c.setData("Gothic", "Goth"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Goth", "Gothic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Grantha"); //$NON-NLS-1$
		c.setData("Grantha", "Gran"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Gran", "Grantha"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Greek"); //$NON-NLS-1$
		c.setData("Greek", "Grek"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Grek", "Greek"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Gujarati"); //$NON-NLS-1$
		c.setData("Gujarati", "Gujr"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Gujr", "Gujarati"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Gurmukhi"); //$NON-NLS-1$
		c.setData("Gurmukhi", "Guru"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Guru", "Gurmukhi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Han (Hanzi, Kanji, Hanja)"); //$NON-NLS-1$
		c.setData("Han (Hanzi, Kanji, Hanja)", "Hani"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hani", "Han (Hanzi, Kanji, Hanja)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Han (Simplified variant)"); //$NON-NLS-1$
		c.setData("Han (Simplified variant)", "Hans"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hans", "Han (Simplified variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Han (Traditional variant)"); //$NON-NLS-1$
		c.setData("Han (Traditional variant)", "Hant"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hant", "Han (Traditional variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Hangul (Hangŭl, Hangeul)"); //$NON-NLS-1$
		c.setData("Hangul (Hangŭl, Hangeul)", "Hang"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hang", "Hangul (Hangŭl, Hangeul)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Hanunoo (Hanunóo)"); //$NON-NLS-1$
		c.setData("Hanunoo (Hanunóo)", "Hano"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hano", "Hanunoo (Hanunóo)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Hebrew"); //$NON-NLS-1$
		c.setData("Hebrew", "Hebr"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hebr", "Hebrew"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Hiragana"); //$NON-NLS-1$
		c.setData("Hiragana", "Hira"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hira", "Hiragana"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Hiragana + Katakana"); //$NON-NLS-1$
		c.setData("Hiragana + Katakana", "Hrkt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hrkt", "Hiragana + Katakana"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Imperial Aramaic"); //$NON-NLS-1$
		c.setData("Imperial Aramaic", "Armi"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Armi", "Imperial Aramaic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Indus (Harappan)"); //$NON-NLS-1$
		c.setData("Indus (Harappan)", "Inds"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Inds", "Indus (Harappan)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Inscriptional Pahlavi"); //$NON-NLS-1$
		c.setData("Inscriptional Pahlavi", "Phli"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Phli", "Inscriptional Pahlavi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Inscriptional Parthian"); //$NON-NLS-1$
		c.setData("Inscriptional Parthian", "Prti"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Prti", "Inscriptional Parthian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Japanese (alias for Han + Hiragana + Katakana)"); //$NON-NLS-1$
		c.setData("Japanese (alias for Han + Hiragana + Katakana)", "Jpan"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Jpan", "Japanese (alias for Han + Hiragana + Katakana)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Javanese"); //$NON-NLS-1$
		c.setData("Javanese", "Java"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Java", "Javanese"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Kaithi"); //$NON-NLS-1$
		c.setData("Kaithi", "Kthi"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Kthi", "Kaithi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Kannada"); //$NON-NLS-1$
		c.setData("Kannada", "Knda"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Knda", "Kannada"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Katakana"); //$NON-NLS-1$
		c.setData("Katakana", "Kana"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Kana", "Katakana"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Kayah Li"); //$NON-NLS-1$
		c.setData("Kayah Li", "Kali"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Kali", "Kayah Li"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Kharoshthi"); //$NON-NLS-1$
		c.setData("Kharoshthi", "Khar"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Khar", "Kharoshthi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Khmer"); //$NON-NLS-1$
		c.setData("Khmer", "Khmr"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Khmr", "Khmer"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Khutsuri (Asomtavruli and Nuskhuri)"); //$NON-NLS-1$
		c.setData("Khutsuri (Asomtavruli and Nuskhuri)", "Geok"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Geok", "Khutsuri (Asomtavruli and Nuskhuri)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Korean (alias for Hangul + Han)"); //$NON-NLS-1$
		c.setData("Korean (alias for Hangul + Han)", "Kore"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Kore", "Korean (alias for Hangul + Han)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Kpelle"); //$NON-NLS-1$
		c.setData("Kpelle", "Kpel"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Kpel", "Kpelle"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Lao"); //$NON-NLS-1$
		c.setData("Lao", "Laoo"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Laoo", "Lao"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Latin"); //$NON-NLS-1$
		c.setData("Latin", "Latn"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Latn", "Latin"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Latin (Fraktur variant)"); //$NON-NLS-1$
		c.setData("Latin (Fraktur variant)", "Latf"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Latf", "Latin (Fraktur variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Latin (Gaelic variant)"); //$NON-NLS-1$
		c.setData("Latin (Gaelic variant)", "Latg"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Latg", "Latin (Gaelic variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Lepcha (Róng)"); //$NON-NLS-1$
		c.setData("Lepcha (Róng)", "Lepc"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Lepc", "Lepcha (Róng)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Limbu"); //$NON-NLS-1$
		c.setData("Limbu", "Limb"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Limb", "Limbu"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Linear A"); //$NON-NLS-1$
		c.setData("Linear A", "Lina"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Lina", "Linear A"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Linear B"); //$NON-NLS-1$
		c.setData("Linear B", "Linb"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Linb", "Linear B"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Lisu (Fraser)"); //$NON-NLS-1$
		c.setData("Lisu (Fraser)", "Lisu"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Lisu", "Lisu (Fraser)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Loma"); //$NON-NLS-1$
		c.setData("Loma", "Loma"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Loma", "Loma"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Lycian"); //$NON-NLS-1$
		c.setData("Lycian", "Lyci"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Lyci", "Lycian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Lydian"); //$NON-NLS-1$
		c.setData("Lydian", "Lydi"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Lydi", "Lydian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Malayalam"); //$NON-NLS-1$
		c.setData("Malayalam", "Mlym"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mlym", "Malayalam"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Mandaic, Mandaean"); //$NON-NLS-1$
		c.setData("Mandaic, Mandaean", "Mand"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mand", "Mandaic, Mandaean"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Manichaean"); //$NON-NLS-1$
		c.setData("Manichaean", "Mani"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mani", "Manichaean"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Mathematical notation"); //$NON-NLS-1$
		c.setData("Mathematical notation", "Zmth"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Zmth", "Mathematical notation"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Mayan hieroglyphs"); //$NON-NLS-1$
		c.setData("Mayan hieroglyphs", "Maya"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Maya", "Mayan hieroglyphs"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Meitei Mayek (Meithei, Meetei)"); //$NON-NLS-1$
		c.setData("Meitei Mayek (Meithei, Meetei)", "Mtei"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mtei", "Meitei Mayek (Meithei, Meetei)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Mende"); //$NON-NLS-1$
		c.setData("Mende", "Mend"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mend", "Mende"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Meroitic Cursive"); //$NON-NLS-1$
		c.setData("Meroitic Cursive", "Merc"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Merc", "Meroitic Cursive"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Meroitic Hieroglyphs"); //$NON-NLS-1$
		c.setData("Meroitic Hieroglyphs", "Mero"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mero", "Meroitic Hieroglyphs"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Miao (Pollard)"); //$NON-NLS-1$
		c.setData("Miao (Pollard)", "Plrd"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Plrd", "Miao (Pollard)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Mongolian"); //$NON-NLS-1$
		c.setData("Mongolian", "Mong"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mong", "Mongolian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Moon (Moon code, Moon script, Moon type)"); //$NON-NLS-1$
		c.setData("Moon (Moon code, Moon script, Moon type)", "Moon"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Moon", "Moon (Moon code, Moon script, Moon type)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Myanmar (Burmese)"); //$NON-NLS-1$
		c.setData("Myanmar (Burmese)", "Mymr"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Mymr", "Myanmar (Burmese)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Nabataean"); //$NON-NLS-1$
		c.setData("Nabataean", "Nbat"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Nbat", "Nabataean"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Nakhi Geba ('Na-'Khi ²Ggŏ-¹baw, Naxi Geba)"); //$NON-NLS-1$
		c.setData("Nakhi Geba ('Na-'Khi ²Ggŏ-¹baw, Naxi Geba)", "Nkgb"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Nkgb", "Nakhi Geba ('Na-'Khi ²Ggŏ-¹baw, Naxi Geba)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("New Tai Lue"); //$NON-NLS-1$
		c.setData("New Tai Lue", "Talu"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Talu", "New Tai Lue"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("N’Ko"); //$NON-NLS-1$
		c.setData("N’Ko", "Nkoo"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Nkoo", "N’Ko"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Ogham"); //$NON-NLS-1$
		c.setData("Ogham", "Ogam"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Ogam", "Ogham"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Ol Chiki (Ol Cemet’, Ol, Santali)"); //$NON-NLS-1$
		c.setData("Ol Chiki (Ol Cemet’, Ol, Santali)", "Olck"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Olck", "Ol Chiki (Ol Cemet’, Ol, Santali)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old Hungarian"); //$NON-NLS-1$
		c.setData("Old Hungarian", "Hung"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hung", "Old Hungarian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old Italic (Etruscan, Oscan, etc.)"); //$NON-NLS-1$
		c.setData("Old Italic (Etruscan, Oscan, etc.)", "Ital"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Ital", "Old Italic (Etruscan, Oscan, etc.)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old North Arabian (Ancient North Arabian)"); //$NON-NLS-1$
		c.setData("Old North Arabian (Ancient North Arabian)", "Narb"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Narb", "Old North Arabian (Ancient North Arabian)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old Permic"); //$NON-NLS-1$
		c.setData("Old Permic", "Perm"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Perm", "Old Permic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old Persian"); //$NON-NLS-1$
		c.setData("Old Persian", "Xpeo"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Xpeo", "Old Persian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old South Arabian"); //$NON-NLS-1$
		c.setData("Old South Arabian", "Sarb"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Sarb", "Old South Arabian"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Old Turkic, Orkhon Runic"); //$NON-NLS-1$
		c.setData("Old Turkic, Orkhon Runic", "Orkh"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Orkh", "Old Turkic, Orkhon Runic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Oriya"); //$NON-NLS-1$
		c.setData("Oriya", "Orya"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Orya", "Oriya"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Osmanya"); //$NON-NLS-1$
		c.setData("Osmanya", "Osma"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Osma", "Osmanya"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Pahawh Hmong"); //$NON-NLS-1$
		c.setData("Pahawh Hmong", "Hmng"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Hmng", "Pahawh Hmong"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Palmyrene"); //$NON-NLS-1$
		c.setData("Palmyrene", "Palm"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Palm", "Palmyrene"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Phags-pa"); //$NON-NLS-1$
		c.setData("Phags-pa", "Phag"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Phag", "Phags-pa"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Phoenician"); //$NON-NLS-1$
		c.setData("Phoenician", "Phnx"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Phnx", "Phoenician"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Psalter Pahlavi"); //$NON-NLS-1$
		c.setData("Psalter Pahlavi", "Phlp"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Phlp", "Psalter Pahlavi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Rejang (Redjang, Kaganga)"); //$NON-NLS-1$
		c.setData("Rejang (Redjang, Kaganga)", "Rjng"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Rjng", "Rejang (Redjang, Kaganga)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Reserved for private use (end)"); //$NON-NLS-1$
		c.setData("Reserved for private use (end)", "Qabx"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Qabx", "Reserved for private use (end)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Reserved for private use (start)"); //$NON-NLS-1$
		c.setData("Reserved for private use (start)", "Qaaa"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Qaaa", "Reserved for private use (start)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Rongorongo"); //$NON-NLS-1$
		c.setData("Rongorongo", "Roro"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Roro", "Rongorongo"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Runic"); //$NON-NLS-1$
		c.setData("Runic", "Runr"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Runr", "Runic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Samaritan"); //$NON-NLS-1$
		c.setData("Samaritan", "Samr"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Samr", "Samaritan"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Sarati"); //$NON-NLS-1$
		c.setData("Sarati", "Sara"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Sara", "Sarati"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Saurashtra"); //$NON-NLS-1$
		c.setData("Saurashtra", "Saur"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Saur", "Saurashtra"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Shavian (Shaw)"); //$NON-NLS-1$
		c.setData("Shavian (Shaw)", "Shaw"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Shaw", "Shavian (Shaw)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("SignWriting"); //$NON-NLS-1$
		c.setData("SignWriting", "Sgnw"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Sgnw", "SignWriting"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Sinhala"); //$NON-NLS-1$
		c.setData("Sinhala", "Sinh"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Sinh", "Sinhala"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Sundanese"); //$NON-NLS-1$
		c.setData("Sundanese", "Sund"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Sund", "Sundanese"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Syloti Nagri"); //$NON-NLS-1$
		c.setData("Syloti Nagri", "Sylo"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Sylo", "Syloti Nagri"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Symbols"); //$NON-NLS-1$
		c.setData("Symbols", "Zsym"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Zsym", "Symbols"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Syriac"); //$NON-NLS-1$
		c.setData("Syriac", "Syrc"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Syrc", "Syriac"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Syriac (Eastern variant)"); //$NON-NLS-1$
		c.setData("Syriac (Eastern variant)", "Syrn"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Syrn", "Syriac (Eastern variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Syriac (Estrangelo variant)"); //$NON-NLS-1$
		c.setData("Syriac (Estrangelo variant)", "Syre"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Syre", "Syriac (Estrangelo variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Syriac (Western variant)"); //$NON-NLS-1$
		c.setData("Syriac (Western variant)", "Syrj"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Syrj", "Syriac (Western variant)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tagalog (Baybayin, Alibata)"); //$NON-NLS-1$
		c.setData("Tagalog (Baybayin, Alibata)", "Tglg"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Tglg", "Tagalog (Baybayin, Alibata)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tagbanwa"); //$NON-NLS-1$
		c.setData("Tagbanwa", "Tagb"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Tagb", "Tagbanwa"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tai Le"); //$NON-NLS-1$
		c.setData("Tai Le", "Tale"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Tale", "Tai Le"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tai Tham (Lanna)"); //$NON-NLS-1$
		c.setData("Tai Tham (Lanna)", "Lana"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Lana", "Tai Tham (Lanna)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tai Viet"); //$NON-NLS-1$
		c.setData("Tai Viet", "Tavt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Tavt", "Tai Viet"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tamil"); //$NON-NLS-1$
		c.setData("Tamil", "Taml"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Taml", "Tamil"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Telugu"); //$NON-NLS-1$
		c.setData("Telugu", "Telu"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Telu", "Telugu"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tengwar"); //$NON-NLS-1$
		c.setData("Tengwar", "Teng"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Teng", "Tengwar"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Thaana"); //$NON-NLS-1$
		c.setData("Thaana", "Thaa"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Thaa", "Thaana"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Thai"); //$NON-NLS-1$
		c.setData("Thai", "Thai"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Thai", "Thai"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tibetan"); //$NON-NLS-1$
		c.setData("Tibetan", "Tibt"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Tibt", "Tibetan"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Tifinagh (Berber)"); //$NON-NLS-1$
		c.setData("Tifinagh (Berber)", "Tfng"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Tfng", "Tifinagh (Berber)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Ugaritic"); //$NON-NLS-1$
		c.setData("Ugaritic", "Ugar"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Ugar", "Ugaritic"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Unified Canadian Aboriginal Syllabics"); //$NON-NLS-1$
		c.setData("Unified Canadian Aboriginal Syllabics", "Cans"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Cans", "Unified Canadian Aboriginal Syllabics"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Vai"); //$NON-NLS-1$
		c.setData("Vai", "Vaii"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Vaii", "Vai"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Visible Speech"); //$NON-NLS-1$
		c.setData("Visible Speech", "Visp"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Visp", "Visible Speech"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Warang Citi (Varang Kshiti)"); //$NON-NLS-1$
		c.setData("Warang Citi (Varang Kshiti)", "Wara"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Wara", "Warang Citi (Varang Kshiti)"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("Yi"); //$NON-NLS-1$
		c.setData("Yi", "Yiii"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Yiii", "Yi"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("uncoded script"); //$NON-NLS-1$
		c.setData("uncoded script", "Zzzz"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Zzzz", "uncoded script"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("undetermined script"); //$NON-NLS-1$
		c.setData("undetermined script", "Zyyy"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Zyyy", "undetermined script"); //$NON-NLS-1$ //$NON-NLS-2$

		c.add("unwritten documents"); //$NON-NLS-1$
		c.setData("unwritten documents", "Zxxx"); //$NON-NLS-1$ //$NON-NLS-2$
		c.setData("Zxxx", "unwritten documents"); //$NON-NLS-1$ //$NON-NLS-2$

	}

	@Deprecated
	public static void addLanguageItems(Combo c) {

		c.add(Messages.AddItems_AncientGreek);
		c.setData(Messages.AddItems_AncientGreek, "grc"); //$NON-NLS-1$
		c.setData("grc", Messages.AddItems_AncientGreek); //$NON-NLS-1$

		c.add(Messages.AddItems_Arabic);
		c.setData(Messages.AddItems_Arabic, "ara"); //$NON-NLS-1$
		c.setData("ara", Messages.AddItems_Arabic); //$NON-NLS-1$

		c.add(Messages.AddItems_Belarusian);
		c.setData(Messages.AddItems_Belarusian, "bel"); //$NON-NLS-1$
		c.setData("bel", Messages.AddItems_Belarusian); //$NON-NLS-1$

		c.add(Messages.AddItems_Bulgarian);
		c.setData(Messages.AddItems_Bulgarian, "bul"); //$NON-NLS-1$
		c.setData("bul", Messages.AddItems_Bulgarian); //$NON-NLS-1$

		c.add(Messages.AddItems_Catalan);
		c.setData(Messages.AddItems_Catalan, "cat"); //$NON-NLS-1$
		c.setData("cat", Messages.AddItems_Catalan); //$NON-NLS-1$

		c.add(Messages.AddItems_Chinese);
		c.setData(Messages.AddItems_Chinese, "zho"); //$NON-NLS-1$
		c.setData("zho", Messages.AddItems_Chinese); //$NON-NLS-1$

		c.add(Messages.AddItems_Croatian);
		c.setData(Messages.AddItems_Croatian, "hrv"); //$NON-NLS-1$
		c.setData("hrv", Messages.AddItems_Croatian); //$NON-NLS-1$

		c.add(Messages.AddItems_Czech);
		c.setData(Messages.AddItems_Czech, "ces"); //$NON-NLS-1$
		c.setData("ces", Messages.AddItems_Czech); //$NON-NLS-1$

		c.add(Messages.AddItems_Dutch);
		c.setData(Messages.AddItems_Dutch, "nld"); //$NON-NLS-1$
		c.setData("nld", Messages.AddItems_Dutch); //$NON-NLS-1$

		c.add(Messages.AddItems_English);
		c.setData(Messages.AddItems_English, "eng"); //$NON-NLS-1$
		c.setData("eng", Messages.AddItems_English); //$NON-NLS-1$

		c.add(Messages.AddItems_French);
		c.setData(Messages.AddItems_French, "fra"); //$NON-NLS-1$
		c.setData("fra", Messages.AddItems_French); //$NON-NLS-1$

		c.add(Messages.AddItems_German);
		c.setData(Messages.AddItems_German, "deu"); //$NON-NLS-1$
		c.setData("deu", Messages.AddItems_German); //$NON-NLS-1$

		c.add(Messages.AddItems_Hebrew);
		c.setData(Messages.AddItems_Hebrew, "heb"); //$NON-NLS-1$
		c.setData("heb", Messages.AddItems_Hebrew); //$NON-NLS-1$

		c.add(Messages.AddItems_Hungarian);
		c.setData(Messages.AddItems_Hungarian, "hun"); //$NON-NLS-1$
		c.setData("hun", Messages.AddItems_Hungarian); //$NON-NLS-1$

		c.add(Messages.AddItems_Italian);
		c.setData(Messages.AddItems_Italian, "ita"); //$NON-NLS-1$
		c.setData("ita", Messages.AddItems_Italian); //$NON-NLS-1$

		c.add(Messages.AddItems_Japanese);
		c.setData(Messages.AddItems_Japanese, "jpn"); //$NON-NLS-1$
		c.setData("jpn", Messages.AddItems_Japanese); //$NON-NLS-1$

		c.add(Messages.AddItems_Latin);
		c.setData(Messages.AddItems_Latin, "lat"); //$NON-NLS-1$
		c.setData("lat", Messages.AddItems_Latin); //$NON-NLS-1$

		c.add(Messages.AddItems_Macedonian);
		c.setData(Messages.AddItems_Macedonian, "mkd"); //$NON-NLS-1$
		c.setData("mkd", Messages.AddItems_Macedonian); //$NON-NLS-1$

		c.add(Messages.AddItems_MiddleDutch);
		c.setData(Messages.AddItems_MiddleDutch, "dum"); //$NON-NLS-1$
		c.setData("dum", Messages.AddItems_MiddleDutch); //$NON-NLS-1$

		c.add(Messages.AddItems_MiddleEnglish);
		c.setData(Messages.AddItems_MiddleEnglish, "enm"); //$NON-NLS-1$
		c.setData("enm", Messages.AddItems_MiddleEnglish); //$NON-NLS-1$

		c.add(Messages.AddItems_MiddleFrench);
		c.setData(Messages.AddItems_MiddleFrench, "frm"); //$NON-NLS-1$
		c.setData("frm", Messages.AddItems_MiddleFrench); //$NON-NLS-1$

		c.add(Messages.AddItems_MiddleHighGerman);
		c.setData(Messages.AddItems_MiddleHighGerman, "gmh"); //$NON-NLS-1$
		c.setData("gmh", Messages.AddItems_MiddleHighGerman); //$NON-NLS-1$

		c.add(Messages.AddItems_MiddleLowGerman);
		c.setData(Messages.AddItems_MiddleLowGerman, "gml"); //$NON-NLS-1$
		c.setData("gml", Messages.AddItems_MiddleLowGerman); //$NON-NLS-1$

		c.add(Messages.AddItems_ModernGreek);
		c.setData(Messages.AddItems_ModernGreek, "ell"); //$NON-NLS-1$
		c.setData("ell", Messages.AddItems_ModernGreek); //$NON-NLS-1$

		c.add(Messages.AddItems_OldDutch);
		c.setData(Messages.AddItems_OldDutch, "odt"); //$NON-NLS-1$
		c.setData("odt", Messages.AddItems_OldDutch); //$NON-NLS-1$

		c.add(Messages.AddItems_OldEnglish);
		c.setData(Messages.AddItems_OldEnglish, "ang"); //$NON-NLS-1$
		c.setData("ang", Messages.AddItems_OldEnglish); //$NON-NLS-1$

		c.add(Messages.AddItems_OldFrench);
		c.setData(Messages.AddItems_OldFrench, "fro"); //$NON-NLS-1$
		c.setData("fro", Messages.AddItems_OldFrench); //$NON-NLS-1$

		c.add(Messages.AddItems_OldHighGerman);
		c.setData(Messages.AddItems_OldHighGerman, "goh"); //$NON-NLS-1$
		c.setData("goh", Messages.AddItems_OldHighGerman); //$NON-NLS-1$

		c.add(Messages.AddItems_OldHungarian);
		c.setData(Messages.AddItems_OldHungarian, "ohu"); //$NON-NLS-1$
		c.setData("ohu", Messages.AddItems_OldHungarian); //$NON-NLS-1$

		c.add(Messages.AddItems_OldRussian);
		c.setData(Messages.AddItems_OldRussian, "orv"); //$NON-NLS-1$
		c.setData("orv", Messages.AddItems_OldRussian); //$NON-NLS-1$

		c.add(Messages.AddItems_OldSpanish);
		c.setData(Messages.AddItems_OldSpanish, "osp"); //$NON-NLS-1$
		c.setData("osp", Messages.AddItems_OldSpanish); //$NON-NLS-1$

		c.add(Messages.AddItems_Persian);
		c.setData(Messages.AddItems_Persian, "fas"); //$NON-NLS-1$
		c.setData("fas", Messages.AddItems_Persian); //$NON-NLS-1$

		c.add(Messages.AddItems_Polish);
		c.setData(Messages.AddItems_Polish, "pol"); //$NON-NLS-1$
		c.setData("pol", Messages.AddItems_Polish); //$NON-NLS-1$

		c.add(Messages.AddItems_Portuguese);
		c.setData(Messages.AddItems_Portuguese, "por"); //$NON-NLS-1$
		c.setData("por", Messages.AddItems_Portuguese); //$NON-NLS-1$

		c.add(Messages.AddItems_Romanian);
		c.setData(Messages.AddItems_Romanian, "ron"); //$NON-NLS-1$
		c.setData("ron", Messages.AddItems_Romanian); //$NON-NLS-1$

		c.add(Messages.AddItems_Russian);
		c.setData(Messages.AddItems_Russian, "rus"); //$NON-NLS-1$
		c.setData("rus", Messages.AddItems_Russian); //$NON-NLS-1$

		c.add(Messages.AddItems_Serbian);
		c.setData(Messages.AddItems_Serbian, "srp"); //$NON-NLS-1$
		c.setData("srp", Messages.AddItems_Serbian); //$NON-NLS-1$

		c.add(Messages.AddItems_Slovak);
		c.setData(Messages.AddItems_Slovak, "slk"); //$NON-NLS-1$
		c.setData("slk", Messages.AddItems_Slovak); //$NON-NLS-1$

		c.add(Messages.AddItems_Spanish);
		c.setData(Messages.AddItems_Spanish, "spa"); //$NON-NLS-1$
		c.setData("spa", Messages.AddItems_Spanish); //$NON-NLS-1$

		c.add(Messages.AddItems_Swedish);
		c.setData(Messages.AddItems_Swedish, "swe"); //$NON-NLS-1$
		c.setData("swe", Messages.AddItems_Swedish); //$NON-NLS-1$

		c.add(Messages.AddItems_Ukrainian);
		c.setData(Messages.AddItems_Ukrainian, "ukr"); //$NON-NLS-1$
		c.setData("ukr", Messages.AddItems_Ukrainian); //$NON-NLS-1$

	}
}