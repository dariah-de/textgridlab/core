/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import info.textgrid.lab.core.metadataeditor.elements.AdvancedControlElement;
import info.textgrid.lab.core.metadataeditor.elements.IControl;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class represents the units those build the Metadata editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class OMItem {

	private IControl control = null;

	/**
	 * The constructors those sets a specific control-element to be used as a
	 * building-unit.
	 */
	public OMItem(IControl control) {
		this.control = control;
	}

	/**
	 * Returns the id of the used control-element.
	 */
	public String getId() {
		return control.getId();
	}

	/**
	 * Returns the reference of the used control-element.
	 */
	public String getRef() throws Exception {

		if (control instanceof AdvancedControlElement)
			return ((AdvancedControlElement) control).getRef();

		return null;
	}

	/**
	 * Returns the OMElement of the used control-element.
	 * 
	 * @return {@link OMElement}
	 */
	public OMElement getOMElement() {

		if (control instanceof AdvancedControlElement)
			return ((AdvancedControlElement) control).getOMElement();

		return null;
	}

	/**
	 * Creates the {@link OMElement} for the used control-element.
	 * 
	 * @param omFactory
	 *            the {@link OMFactory}
	 * @param parent
	 *            the parent {@link OMElement}
	 * @param tgNS
	 *            the used namespace
	 * @param asAttribute
	 *            if the element lies in the xul-file as attribute
	 */
	void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace tgNS, boolean asAttribute) {
		control.createOMElement(omFactory, parent, tgNS, asAttribute);
	}

	/**
	 * Sets the {@link OMElement} in the used control-element.
	 * 
	 * @param omelem
	 *            the OMElement to set
	 * @param tg
	 *            determines whether the namespace is a TG-Namespace or
	 *            not(descriptive or custom)
	 */
	void setOMElement(OMElement omelem, boolean tg) {
		control.setOMElement(omelem, tg);
	}

}