/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Control;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class GridDataUtil {

	public static void setGridData(Control control, String attrWidth,
			String attrHeight) {

		GridData gridData = new GridData();

		if (attrHeight != null) {
			if ("100%".equals(attrHeight)) { //$NON-NLS-1$
				gridData.verticalAlignment = GridData.FILL;
				gridData.grabExcessVerticalSpace = true;
				gridData.minimumHeight = control.getParent().getSize().y;
			} else {
				int height = Integer.valueOf(attrHeight);
				gridData.heightHint = height;
				gridData.minimumHeight = height;
			}
		}

		if (attrWidth != null) {
			if ("100%".equals(attrWidth)) { //$NON-NLS-1$
				gridData.horizontalAlignment = GridData.FILL;
				// gridData.grabExcessHorizontalSpace = true;
				gridData.minimumWidth = control.getParent().getSize().x;
			} else {
				int width = Integer.valueOf(attrWidth);
				gridData.widthHint = width;
				gridData.minimumWidth = width;
			}
		}

		control.setLayoutData(gridData);
	}
}