/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;
import info.textgrid.namespaces.middleware.tgsearch.PathGroupType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.PathType;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to implement the meta-path client for the metadata editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class MetadataPath {

	public static ArrayList<EntryType> addEditionEntriesToMetadataEditor(
			final String tgUri) {

		SearchClient client;
		try {
			client = new SearchClient(ConfClient.getInstance().getValue(
					ConfservClientConstants.TG_SEARCH));
		} catch (OfflineException e) {
			Activator.handleError(e);
			return null;
		}

		PathResponse pathResponse = client.getPath(tgUri);

		if (pathResponse != null) {

			List<PathGroupType> pathGrpTypes = pathResponse.getPathGroup();
			ArrayList<EntryType> entries = new ArrayList<EntryType>();
			EntryType currentEntry = null;

			for (PathGroupType t : pathGrpTypes) {

				for (PathType pt : t.getPath()) {

					for (EntryType et : pt.getEntry()) {
						if ("text/tg.edition+tg.aggregation+xml".equals(et //$NON-NLS-1$
								.getFormat())) {
							currentEntry = et;
						}
					}

					if (currentEntry != null) {
						entries.add(currentEntry);
						currentEntry = null;
					}
				}

			}

			if (!entries.isEmpty()) {
				return entries;
			}

		}

		return null;
	}
}