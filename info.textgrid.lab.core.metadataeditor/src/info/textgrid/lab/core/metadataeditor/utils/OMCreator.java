/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import info.textgrid.lab.core.metadataeditor.Activator;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class is derived from the class {@link OMUtil} and was used to create
 * the OMElements from the entered meta data in the Metadata editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class OMCreator extends OMUtil {

	/**
	 * Creates an OMElement from the meta data in the Metadata editor.
	 * 
	 * @param omFactory
	 *            the {@link OMFactory}
	 * @param parent
	 *            the parent "descriptive" or "custom"
	 * @param tgNS
	 *            a proper namespace
	 */
	public void createOMElements(OMFactory omFactory, OMElement parent,
			OMNamespace tgNS) {

		for (OMItem item : items) {
			try {
				if (item.getRef() != null) {
					OMItem it = getOMItemById(item.getRef());
					if (it != null)
						item.createOMElement(omFactory, it.getOMElement(),
								tgNS, true);
				} else {
					item.createOMElement(omFactory, parent, tgNS, false);
				}
			} catch (Exception e) {
				Activator.handleError(e);
			}
		}
	}

}