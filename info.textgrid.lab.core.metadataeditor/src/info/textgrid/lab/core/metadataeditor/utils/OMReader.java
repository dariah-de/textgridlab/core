/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.utils;

import org.apache.axiom.om.OMElement;

/**
 * This class is derived from the class {@link OMUtil} and was used to set the
 * content of the OMElements in the Metadata editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class OMReader extends OMUtil {

	/**
	 * Sets the content of the {@link OMElement} in the Metadata editor.
	 * 
	 * @param root
	 *            the root {@link OMElement}
	 * @param tg
	 *            determines whether the searched elements are in the
	 *            TG-Namespace (descriptive) or not (custom)
	 */
	public void setOMElements(OMElement root, boolean tg) {
		for (OMItem item : items) {
			item.setOMElement(root, tg);
		}
	}
}