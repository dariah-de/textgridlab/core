/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.TextGridObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.jaxen.JaxenException;

import com.logabit.xlayout.XLayout;

public class SourceControlElement implements IRepresentableComposite {

	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private String id = null;
	private String title = null;
	private Composite sectionClient = null;
	private Element dataElement = null;
	private XLayout xlayout = null;
	private String element_name = Messages.SourceControlElement_element_title;
	private String element_name_on_label = Messages.SourceControlElement_element_title;

	private String parent_name = null;

	private Element objectCitationElement = null,
			bibliographicCitationElement = null;

	private ArrayList<IControl> controls = new ArrayList<IControl>();

	private Button toObjectCitation, toBibliographicCitation;

	private String selectedCitationType = null;

	public SourceControlElement createNewSourceControlElementForComplexRepeatable(
			final String id, ScrolledForm f, Control section) {
		SourceControlElement src = new SourceControlElement(id, f, section,
				dataElement, toolkit, xlayout);

		src.objectCitationElement = this.objectCitationElement;
		src.bibliographicCitationElement = this.bibliographicCitationElement;

		return src;
	}

	public SourceControlElement(final String id, ScrolledForm f,
			Control section, Element element, FormToolkit tk,
			final XLayout layout) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;
		this.xlayout = layout;

		title = dataElement.attributeValue("text"); //$NON-NLS-1$

		if (title != null) {
			// sec.setText(title);
			element_name = title;
			sec.setText(element_name);
			element_name_on_label = element_name.replaceAll("\\(.+\\)", ""); //$NON-NLS-1$ //$NON-NLS-2$
		}

		parent_name = dataElement.attributeValue(XLayout.ATTR_ELEMENT_NAME);

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				xlayout.setModified(true);
				form.reflow(true);
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
		sectionClient.setBackground(sec.getBackground());
		sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
		sec.setClient(sectionClient);

		toObjectCitation = toolkit.createButton(sectionClient,
				Messages.SourceControlElement_addObjectCitation, SWT.PUSH | SWT.CENTER);
		toObjectCitation.setLayoutData(new GridData(GridData.FILL,
				GridData.CENTER, true, true));
		toObjectCitation.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				createObjectCitation();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		toBibliographicCitation = toolkit.createButton(sectionClient,
				Messages.SourceControlElement_addBibliographicCitation, SWT.PUSH | SWT.CENTER);
		toBibliographicCitation.setLayoutData(new GridData(GridData.FILL,
				GridData.CENTER, true, true));

		toBibliographicCitation.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				createBibliographicCitation();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

	}

	private void createObjectCitation() {
		if (toObjectCitation != null && !toObjectCitation.isDisposed())
			toObjectCitation.dispose();

		if (toBibliographicCitation != null
				&& !toBibliographicCitation.isDisposed())
			toBibliographicCitation.dispose();

		toObjectCitation = toBibliographicCitation = null;

		xlayout.setDoCollectSourceElements(true);
		xlayout.setCollectingSourceElement(this);
		xlayout.parseTable(objectCitationElement.element("table"), getBody()); //$NON-NLS-1$
		xlayout.setDoCollectSourceElements(false);

		selectedCitationType = "objectCitation"; //$NON-NLS-1$

		form.reflow(true);
		xlayout.fireFocusEvent();
	}

	private void createBibliographicCitation() {
		if (toObjectCitation != null && !toObjectCitation.isDisposed())
			toObjectCitation.dispose();

		if (toBibliographicCitation != null
				&& !toBibliographicCitation.isDisposed())
			toBibliographicCitation.dispose();

		toObjectCitation = toBibliographicCitation = null;

		xlayout.setDoCollectSourceElements(true);
		xlayout.setCollectingSourceElement(this);
		xlayout.parseTable(bibliographicCitationElement.element("table"), //$NON-NLS-1$
				getBody());
		xlayout.setDoCollectSourceElements(false);

		selectedCitationType = "bibliographicCitation"; //$NON-NLS-1$

		form.reflow(true);
		xlayout.fireFocusEvent();
	}

	@Override
	public String getId() {
		return id;
	}

	public Composite getBody() {
		return sectionClient;
	}

	public void addElement(Element elem) {
		if ("objectCitation".equals(elem //$NON-NLS-1$
				.attributeValue(XLayout.ATTR_ELEMENT_NAME)))
			objectCitationElement = elem;
		else
			bibliographicCitationElement = elem;
	}

	public void addControl(IControl control) {
		this.controls.add(control);
	}

	@Override
	public boolean validate() {
		if (!sec.isExpanded())
			return true;

		boolean valid = true;
		for (IControl c : controls) {
			valid &= c.validate();
		}
		return valid;
	}

	private void clearCurrentSourceElement() {

		if (controls.isEmpty())
			return;

		for (IControl c : controls) {
			c.dispose();
		}

		controls.clear();

		if (sectionClient != null && !sectionClient.isDisposed()) {
			sectionClient.dispose();
			sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
			sectionClient.setBackground(sec.getBackground());
			sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
			sec.setClient(sectionClient);
		}

	}

	@Override
	public void dispose() {

		if (toObjectCitation != null && !toObjectCitation.isDisposed())
			toObjectCitation.dispose();

		if (toBibliographicCitation != null
				&& !toBibliographicCitation.isDisposed())
			toBibliographicCitation.dispose();

		for (IControl c : controls) {
			c.dispose();
		}

		controls.clear();

		if (sec != null && !sec.isDisposed())
			sec.dispose();
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {

		if (sec.isExpanded()) {

			if (parent_name != null && !"".equals(parent_name) //$NON-NLS-1$
					&& selectedCitationType != null
					&& !"".equals(selectedCitationType)) { //$NON-NLS-1$
				OMElement source = omFactory.createOMElement(parent_name, ns,
						parent);
				OMElement citation = omFactory.createOMElement(
						selectedCitationType, ns, source);

				for (IControl c : controls) {
					c.createOMElement(omFactory, citation, ns, asAttribute);
				}
			}
		}
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {

		boolean found = false;

		if (parent_name != null && !"".equals(parent_name)) { //$NON-NLS-1$
			OMElement elem = null;

			try {
				if (tg)
					elem = OMUtil.getElementWithName(parent_name, root,
							TextGridObject.TEXTGRID_METADATA_NAMESPACE, "tg"); //$NON-NLS-1$
				else
					elem = OMUtil.getElementWithName(parent_name, root,
							TextGridObject.CUSTOM_NAMESPACE, "cns"); //$NON-NLS-1$
			} catch (JaxenException e) {
				Activator.handleError(e);
				return false;
			}

			if (elem != null) {
				OMElement citation = elem.getFirstElement();

				if (citation != null) {
					clearCurrentSourceElement();
					if (citation.getLocalName().contains("object")) //$NON-NLS-1$
						createObjectCitation();
					else
						createBibliographicCitation();

					for (IControl c : controls) {
						found |= c.setOMElement(citation, tg);
					}
				}
			}
		}

		if ((sec.getExpansionStyle() & ExpandableComposite.TWISTIE) != 0)
			if (found)
				sec.setExpanded(true);
			else
				sec.setExpanded(false);

		return found;
	}

	@Override
	public void clear() {

		if (controls.isEmpty())
			return;

		for (IControl c : controls) {
			c.dispose();
		}

		controls.clear();

		if (sectionClient != null && !sectionClient.isDisposed()) {
			sectionClient.dispose();
			sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
			sectionClient.setBackground(sec.getBackground());
			sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
			sec.setClient(sectionClient);
		}

		toObjectCitation = toolkit.createButton(sectionClient,
				Messages.SourceControlElement_addObjectCitation, SWT.PUSH | SWT.CENTER);
		toObjectCitation.setLayoutData(new GridData(GridData.FILL,
				GridData.CENTER, true, true));
		toObjectCitation.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				createObjectCitation();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		toBibliographicCitation = toolkit.createButton(sectionClient,
				Messages.SourceControlElement_addBibliographicCitation, SWT.PUSH | SWT.CENTER);
		toBibliographicCitation.setLayoutData(new GridData(GridData.FILL,
				GridData.CENTER, true, true));

		toBibliographicCitation.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				createBibliographicCitation();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});
	}

	@Override
	public List<Control> getSWTControls() {
		ArrayList<Control> cons = new ArrayList<Control>();

		if (toObjectCitation != null)
			cons.add(toObjectCitation);

		if (toBibliographicCitation != null)
			cons.add(toBibliographicCitation);

		for (IControl c : controls) {
			if (c instanceof IRepresentableComposite) {
				cons.addAll(((IRepresentableComposite) c).getSWTControls());
			} else if (c instanceof IRepresentableControl) {
				cons.add(((IRepresentableControl) c).getSWTControl());
			}
		}

		return cons;
	}

	@Override
	public List<IControl> getControls() {
		return controls;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public void setExpanded(boolean expand) {
		if (sec != null)
			sec.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		if (sec != null)
			return sec.isExpanded();
		return false;
	}

}