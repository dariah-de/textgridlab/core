/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.dom4j.Node;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.jaxen.JaxenException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.logabit.xlayout.XLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class TextgridDateControlElement implements IRepresentableComposite,
		ICheckableElement {

	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private Composite sectionClient = null;
	private Element dataElement = null;
	private boolean found = false;
	private String id = null;
	private String type = "approxDate"; //$NON-NLS-1$
	private LinkedList<Control> controls = new LinkedList<Control>();

	private String approxDate = ""; //$NON-NLS-1$
	private String approxGregDate = ""; //$NON-NLS-1$
	private String notBeforeGregDate = ""; //$NON-NLS-1$
	private String notAfterGregDate = ""; //$NON-NLS-1$

	private Text txtapproxDate = null;
	private Text txtapproxGregDate = null;
	private Label lblapproxGregDate = null;

	// private Text txtnotBefore = null;
	private Text txtnotBeforeGregDate = null;
	private Label lblnotBeforeGregDate = null;

	// private Text txtnotAfter = null;
	private Text txtnotAfterGregDate = null;
	private Label lblnotAfterGregDate = null;

	// to set the modified-flag
	private XLayout xlayout = null;
	private Color defaultColor;

	private String parent_element_name = null;

	public TextgridDateControlElement(String id, ScrolledForm f,
			Control section, Element element, FormToolkit tk,
			final XLayout layout) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;
		this.xlayout = layout;

		String title = dataElement.attributeValue("text"); //$NON-NLS-1$
		if (title != null) {
			sec.setText(title);
		}

		parent_element_name = dataElement
				.attributeValue(XLayout.ATTR_ELEMENT_NAME);

		this.defaultColor = this.sec.getDisplay().getSystemColor(
				SWT.COLOR_BLACK);

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				xlayout.setModified(true);
				form.reflow(true);
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
		sectionClient.setBackground(sec.getBackground());
		sec.setClient(sectionClient);
		sectionClient.setLayout(new GridLayout());

		try {
			walkElement(dataElement);
		} catch (Exception e1) {
			Activator.handleError(e1);
			return;
		}

		if (type.equals("approxDate")) { //$NON-NLS-1$
			this.createApproxDate();
		} else if (type.equals("dateRange")) { //$NON-NLS-1$
			this.createDateRange();
		}
	}

	private void createApproxDate() {

		// sec.setText("Approx Date");

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		Label _lblapproxGregData = toolkit.createLabel(sectionClient,
				Messages.TextgridDateControlElement_approxGregDate + Messages.TextgridDateControlElement_approxGregDate_example);
		_lblapproxGregData.setBackground(sectionClient.getBackground());

		lblapproxGregDate = _lblapproxGregData;

		this.defaultColor = lblapproxGregDate.getForeground();

		final Text _txtapproxGregData = toolkit.createText(sectionClient,
				approxGregDate, SWT.BORDER /* | SWT.MULTI */);
		_txtapproxGregData
				.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		// GridDataUtil.setGridData(t2, "80", null);

		txtapproxGregDate = _txtapproxGregData;

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Label _lblapproxData = toolkit.createLabel(sectionClient,
				Messages.TextgridDateControlElement_approxDate_original);
		_lblapproxData.setBackground(sectionClient.getBackground());

		if (approxDate != null && "".equals(approxDate.trim())) //$NON-NLS-1$
			approxDate = ""; //$NON-NLS-1$

		final Text _txtapproxDate = toolkit.createText(sectionClient,
				approxDate, SWT.BORDER /* | SWT.MULTI */);
		// GridDataUtil.setGridData(t1, "100%", null);
		_txtapproxDate.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		txtapproxDate = _txtapproxDate;

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Button b = toolkit.createButton(sectionClient, Messages.TextgridDateControlElement_switchDateRange,
				SWT.PUSH);

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		// The listeners for all created controls

		_txtapproxGregData.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				String _s = approxGregDate;
				approxGregDate = _txtapproxGregData.getText();

				// auto filling the approxDate field
				if (_txtapproxDate != null
						&& (approxDate.equals("") || approxDate.equals(_s))) { //$NON-NLS-1$
					approxDate = approxGregDate;
					_txtapproxDate.setText(approxDate);
				}

				xlayout.setModified(true);
			}
		});
		_txtapproxGregData.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
				xlayout.fireFocusEvent();
			}
		});

		_txtapproxDate.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				approxDate = _txtapproxDate.getText();
				xlayout.setModified(true);
			}
		});
		_txtapproxDate.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
				xlayout.fireFocusEvent();
			}
		});

		b.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {

				switchToDateRange();
			}
		});
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		// the current order is related to TG-620

		controls.addLast(_lblapproxGregData);
		controls.addLast(_txtapproxGregData);

		controls.addLast(_lblapproxData);
		controls.addLast(_txtapproxDate);

		controls.add(b);
	}

	private void createDateRange() {

		// sec.setText("Date Range");

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Label _lblapproxGregDateNotBefore = toolkit.createLabel(sectionClient,
				Messages.TextgridDateControlElement_NotBeforeApproxGregDate
						+ Messages.TextgridDateControlElement_approxGregDate_example);
		_lblapproxGregDateNotBefore
				.setBackground(sectionClient.getBackground());

		lblnotBeforeGregDate = _lblapproxGregDateNotBefore;

		this.defaultColor = lblnotBeforeGregDate.getForeground();

		final Text _txtapproxGregDateNotBefore = toolkit.createText(
				sectionClient, notBeforeGregDate, SWT.BORDER /* | SWT.MULTI */);
		_txtapproxGregDateNotBefore.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));
		// GridDataUtil.setGridData(t2, "80", null);

		txtnotBeforeGregDate = _txtapproxGregDateNotBefore;

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// Label _lblNotBefore = toolkit.createLabel(sectionClient,
		// "Not Before, Original Form");
		// _lblNotBefore.setBackground(sectionClient.getBackground());
		//
		// final Text _txtNotBefore = toolkit.createText(sectionClient,
		// notBefore,
		// SWT.BORDER /* | SWT.MULTI */);
		// // GridDataUtil.setGridData(t1, "100%", null);
		// _txtNotBefore.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		//
		// txtnotBefore = _txtNotBefore;

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Label _lblapproxGregDateNotAfter = toolkit.createLabel(sectionClient,
				Messages.TextgridDateControlElement_NotAfterApproxGregDate
						+ Messages.TextgridDateControlElement_approxGregDate_example);
		_lblapproxGregDateNotAfter.setBackground(sectionClient.getBackground());

		lblnotAfterGregDate = _lblapproxGregDateNotAfter;

		final Text _txtapproxGregDateNotAfter = toolkit.createText(
				sectionClient, notAfterGregDate, SWT.BORDER /* | SWT.MULTI */);
		_txtapproxGregDateNotAfter.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));
		// GridDataUtil.setGridData(t4, "80", null);

		txtnotAfterGregDate = _txtapproxGregDateNotAfter;

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		// Label _lblNotAfter = toolkit.createLabel(sectionClient,
		// "Not After, Original Form");
		// _lblNotAfter.setBackground(sectionClient.getBackground());
		//
		// final Text _txtNotAfter = toolkit.createText(sectionClient, notAfter,
		// SWT.BORDER /* | SWT.MULTI */);
		// // GridDataUtil.setGridData(t3, "100%", null);
		// _txtNotAfter.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		//
		// txtnotAfter = _txtNotAfter;

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Button b = toolkit.createButton(sectionClient, Messages.TextgridDateControlElement_switchApproxDate,
				SWT.PUSH);

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// The listeners for all created controls

		_txtapproxGregDateNotBefore.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				// String _s = notBeforeGregDate;
				notBeforeGregDate = _txtapproxGregDateNotBefore.getText();

				xlayout.setModified(true);
			}
		});
		_txtapproxGregDateNotBefore.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
				xlayout.fireFocusEvent();
			}
		});

		_txtapproxGregDateNotAfter.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				// String _s = notAfterGregDate;
				notAfterGregDate = _txtapproxGregDateNotAfter.getText();

				xlayout.setModified(true);
			}
		});
		_txtapproxGregDateNotAfter.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
				xlayout.fireFocusEvent();
			}
		});

		b.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {

				switchToApproxDate();
			}
		});

		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		// the current order is related to TG-620

		controls.addLast(_lblapproxGregDateNotBefore);
		controls.addLast(_txtapproxGregDateNotBefore);

		controls.addLast(_lblapproxGregDateNotAfter);
		controls.addLast(_txtapproxGregDateNotAfter);

		controls.add(b);
	}

	private void switchToApproxDate() {

		while (!controls.isEmpty()) {
			controls.removeLast().dispose();
		}

		form.reflow(true);
		createApproxDate();
		sec.setExpanded(false);
		sec.setExpanded(true);

		type = "approxDate"; //$NON-NLS-1$
		xlayout.setModified(true);
	}

	private void switchToDateRange() {

		while (!controls.isEmpty()) {
			controls.removeLast().dispose();
		}

		form.reflow(true);
		createDateRange();
		sec.setExpanded(false);
		sec.setExpanded(true);

		type = "dateRange"; //$NON-NLS-1$
		xlayout.setModified(true);
	}

	public String getType() {

		return type;
	}

	@Override
	public String getId() {

		return id;
	}

	private boolean validType = true;

	private boolean testInputType(String input) {

		if ("".equals(input)) //$NON-NLS-1$
			return true;

		validType = true;
		SchemaFactory factory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		factory.setErrorHandler(new ErrorHandler() {

			@Override
			public void warning(SAXParseException exception)
					throws SAXException {
				Activator.handleWarning(exception);
			}

			@Override
			public void fatalError(SAXParseException exception)
					throws SAXException {
				// Activator.handleError(exception);
				validType = false;
			}

			@Override
			public void error(SAXParseException exception) throws SAXException {
				// Activator.handleError(exception);
				validType = false;

			}
		});

		try {
			StreamSource ss = new StreamSource(
					new ByteArrayInputStream(
							("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" //$NON-NLS-1$
									+ "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" //$NON-NLS-1$
									+ "<xs:element name=\"test\"><xs:simpleType><xs:union memberTypes=\"xs:date xs:gYear xs:gYearMonth\"/></xs:simpleType></xs:element>" //$NON-NLS-1$
									+ "</xs:schema>").getBytes("UTF-8"))); //$NON-NLS-1$ //$NON-NLS-2$
			StreamSource ss2 = new StreamSource(new ByteArrayInputStream(
					("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<test>" //$NON-NLS-1$ //$NON-NLS-2$
							+ input + "</test>").getBytes("UTF-8"))); //$NON-NLS-1$ //$NON-NLS-2$
			Schema s = factory.newSchema(ss);

			Validator v = s.newValidator();
			v.validate(ss2);
		} catch (UnsupportedEncodingException e) {
			// Activator.handleError(e);
			validType = false;
		} catch (SAXException e) {
			// Activator.handleError(e);
			validType = false;
		} catch (IOException e) {
			// Activator.handleError(e);
			validType = false;
		}

		return validType;
	}

	public boolean checkGregorianDate() {

		if (type.equals("approxDate")) { //$NON-NLS-1$
			String t = txtapproxGregDate.getText();
			boolean valid = (!"".equals(t)) && testInputType(t); //$NON-NLS-1$
			if (!valid) {
				lblapproxGregDate.setForeground(sectionClient.getDisplay()
						.getSystemColor(SWT.COLOR_RED));
				return false;
			} else
				lblapproxGregDate.setForeground(defaultColor);
		} else if (type.equals("dateRange")) { //$NON-NLS-1$

			String t = txtnotBeforeGregDate.getText();
			boolean valid = (!"".equals(t)) && testInputType(t); //$NON-NLS-1$

			if (!valid) {
				lblnotBeforeGregDate.setForeground(sectionClient.getDisplay()
						.getSystemColor(SWT.COLOR_RED));
			} else
				lblnotBeforeGregDate.setForeground(defaultColor);

			t = txtnotAfterGregDate.getText();
			valid = (!"".equals(t)) && testInputType(t); //$NON-NLS-1$
			if (!valid) {
				lblnotAfterGregDate.setForeground(sectionClient.getDisplay()
						.getSystemColor(SWT.COLOR_RED));
				return false;
			} else {
				lblnotAfterGregDate.setForeground(defaultColor);
			}

			return valid;
		}

		return true;
	}

	private void walkElement(Element element) throws Exception {

		for (int i = 0, size = element.nodeCount(); i < size; i++) {
			Node node = element.node(i);
			if (node instanceof Element) {
				Element elem = (Element) node;
				String name = elem.getName();
				if (name != null) {
					if (name.equals("approxDate") || name.equals("dateRange")) { //$NON-NLS-1$ //$NON-NLS-2$
						if (!found) {
							found = true;
							type = name;
						} else {
							throw new Exception(
									Messages.TextgridDateControlElement_approxDateRangeException);
						}
					}

					if (name.equals("approxDate")) { //$NON-NLS-1$
						approxDate = elem.getText();
						if (elem.attributeValue("approximateGregorianDate") != null) { //$NON-NLS-1$
							approxGregDate = elem
									.attributeValue("approximateGregorianDate"); //$NON-NLS-1$
						}
					}

					if (name.equals("notBefore")) { //$NON-NLS-1$
						// notBefore = elem.getText();
						if (elem.attributeValue("approximateGregorianDate") != null) { //$NON-NLS-1$
							notBeforeGregDate = elem
									.attributeValue("approximateGregorianDate"); //$NON-NLS-1$
						}
					}

					if (name.equals("notAfter")) { //$NON-NLS-1$
						// notAfter = elem.getText();
						if (elem.attributeValue("approximateGregorianDate") != null) { //$NON-NLS-1$
							notAfterGregDate = elem
									.attributeValue("approximateGregorianDate"); //$NON-NLS-1$
						}
					}

				}

				walkElement((Element) node);
			}
		}
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace tgNS, boolean dummy) {

		/**
		 * <tns:dateOfCreation date="2009-12-12"></tns:dateOfCreation>
		 */

		if (!sec.isExpanded())
			return;

		OMElement created = omFactory
				.createOMElement(parent_element_name, tgNS);

		if (type.equals("approxDate") && txtapproxGregDate != null //$NON-NLS-1$
				&& !txtapproxGregDate.isDisposed()) {

			String approxGregDateTxt = txtapproxGregDate.getText();

			if (!"".equals(approxGregDateTxt)) //$NON-NLS-1$
				created.addAttribute(omFactory.createOMAttribute("date", null, //$NON-NLS-1$
						approxGregDateTxt));

			created.setText(txtapproxDate.getText());

		} else if (type.equals("dateRange") && txtnotBeforeGregDate != null //$NON-NLS-1$
				&& !txtnotBeforeGregDate.isDisposed()) {

			String notBeforeTxt = txtnotBeforeGregDate.getText(), notAfterTxt = txtnotAfterGregDate
					.getText();

			if (!"".equals(notBeforeTxt)) //$NON-NLS-1$
				created.addAttribute(omFactory.createOMAttribute("notBefore", //$NON-NLS-1$
						null, notBeforeTxt));
			// notBefore.setText(txtnotBefore.getText());

			if (!"".equals(notAfterTxt)) //$NON-NLS-1$
				created.addAttribute(omFactory.createOMAttribute("notAfter", //$NON-NLS-1$
						null, notAfterTxt));
			// notAfter.setText(txtnotAfter.getText());
		}

		parent.addChild(created);
	}

	@Override
	public void clear() {
		approxDate = ""; //$NON-NLS-1$
		approxGregDate = ""; //$NON-NLS-1$
		// notBefore = "";
		notBeforeGregDate = ""; //$NON-NLS-1$
		// notAfter = "";
		notAfterGregDate = ""; //$NON-NLS-1$
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {

		clear();
		switchToApproxDate();
		xlayout.setModified(false);

		if (root == null)
			return false;

		// Iterator children = root.getChildElements();
		OMElement elem = null;
		try {
			elem = OMUtil.getElementWithName(parent_element_name, root,
					TextGridObject.TEXTGRID_METADATA_NAMESPACE, "tg"); //$NON-NLS-1$
		} catch (JaxenException e) {
			Activator.handleError(e);
			return false;
		}

		boolean found = false;

		if (elem != null /* elem.getLocalName().equals(parent_element_name) */) {

			OMAttribute attr = elem.getAttribute(new QName("date")); //$NON-NLS-1$

			if (attr != null) {
				if (!this.type.equals("approxDate")) //$NON-NLS-1$
					switchToApproxDate();

				txtapproxDate.setText(elem.getText());
				txtapproxGregDate.setText(attr.getAttributeValue());

				found = true;
			} else {
				if (elem.getAttribute(new QName("notBefore")) != null //$NON-NLS-1$
						&& elem.getAttribute(new QName("notAfter")) != null) { //$NON-NLS-1$

					if (!this.type.equals("dateRange")) //$NON-NLS-1$
						switchToDateRange();

					attr = elem.getAttribute(new QName("notBefore")); //$NON-NLS-1$

					if (attr != null) {
						txtnotBeforeGregDate.setText(attr.getAttributeValue());
						found = true;
					}

					attr = elem.getAttribute(new QName("notAfter")); //$NON-NLS-1$

					if (attr != null) {
						txtnotAfterGregDate.setText(attr.getAttributeValue());
						found = true;
					}
				} else {
					if (!this.type.equals("approxDate")) //$NON-NLS-1$
						switchToApproxDate();
					txtapproxDate.setText(elem.getText());
					found = true;
				}
			}

			if ((sec.getExpansionStyle() & ExpandableComposite.TWISTIE) != 0)
				if (found)
					sec.setExpanded(true);
				else
					sec.setExpanded(false);

			return found;
		}

		sec.setExpanded(false);

		return false;
	}

	@Override
	public void dispose() {
		for (Control c : controls) {
			c.dispose();
		}
		controls.clear();

		if (sec != null && !sec.isDisposed())
			sec.dispose();
	}

	@Override
	public boolean validate() {
		if (!sec.isExpanded())
			return true;

		return checkGregorianDate();
	}

	@Override
	public List<Control> getSWTControls() {
		return controls;
	}

	@Override
	public List<IControl> getControls() {
		throw new NotImplementedException();
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public void setRequired(boolean required) {
		// throw new NotImplementedException();
		if (required)
			setExpanded(true);
	}

	@Override
	public boolean isRequired() {
		return true;
	}

	@Override
	public void setExpanded(boolean expand) {
		if (sec != null)
			sec.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		if (sec != null)
			return sec.isExpanded();
		return false;
	}
}
