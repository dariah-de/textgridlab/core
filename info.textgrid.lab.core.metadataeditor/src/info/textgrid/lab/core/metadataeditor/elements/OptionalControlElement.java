/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import java.util.ArrayList;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
@Deprecated
public class OptionalControlElement implements IControl {

	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private String id = null;
	private String title = null;
	private Composite sectionClient = null;
	private Element dataElement = null;

	private ArrayList<RepeatControlElement> repElems = null;
	private ArrayList<AdvancedControlElement> controls = null;

	private boolean active = false;

	public OptionalControlElement(final String id, ScrolledForm f,
			Control section, Element element, FormToolkit tk) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;

		title = dataElement.attributeValue("text") + Messages.OptionalControlElement_optional; //$NON-NLS-1$

		if (title != null)
			sec.setText(title);

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {

				form.reflow(true);

				if (sec.isExpanded())
					active = true;
				else
					active = false;
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER);
		sectionClient.setBackground(sec.getBackground());
		sec.setClient(sectionClient);
		sectionClient.setLayout(new TableWrapLayout());
		sectionClient.setSize(250, 300);
	}

	public Composite getBody() {

		return sectionClient;
	}

	@Override
	public String getId() {

		return id;
	}

	public void addRepeatElement(RepeatControlElement elem) {

		if (repElems == null)
			repElems = new ArrayList<RepeatControlElement>();

		repElems.add(elem);
	}

	public void addAdvancedControl(AdvancedControlElement control) {

		if (controls == null)
			controls = new ArrayList<AdvancedControlElement>();

		controls.add(control);
	}

	public boolean isActive() {

		return active;
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {

		if (!active)
			return;

		if (controls != null) {
			for (AdvancedControlElement e : controls)
				e.createOMElement(omFactory, parent, ns, asAttribute);
		}

		if (repElems != null) {
			for (RepeatControlElement e : repElems)
				e.createOMElement(omFactory, parent, ns, asAttribute);
		}
	}

	void setExpanded(boolean expand) {

		if (expand && !sec.isExpanded()) {
			long l = System.currentTimeMillis();
			sec.setExpanded(true);
			System.out.println("In setExpanded: " //$NON-NLS-1$
					+ (System.currentTimeMillis() - l));
			active = true;
		} else if (!expand && sec.isExpanded()) {
			sec.setExpanded(false);
			active = false;
		}
	}

	@Override
	public void dispose() {

		if (controls != null) {
			for (AdvancedControlElement e : controls)
				e.dispose();
			controls.clear();
		}

		if (repElems != null) {
			for (RepeatControlElement e : repElems)
				e.dispose();
			repElems.clear();
		}
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

}
