/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.MetaDataDialog;
import info.textgrid.lab.core.metadataeditor.utils.FetchTGObjectsDialog;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.progress.UIJob;
import org.jaxen.JaxenException;

import com.logabit.xlayout.XLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */

public class StandaloneGroupControlElement implements IRepresentableItemsGroup,
		IAdvancedControlsContainer {

	private String id;
	private Section sec = null;
	private ScrolledForm form = null;
	private Composite sectionClient = null;
	private XLayout xlayout = null;

	private LinkedList<AdvancedControlElement> controls = new LinkedList<AdvancedControlElement>();

	private boolean isEditionOf = false;

	public StandaloneGroupControlElement(String id, Control s, ScrolledForm f,
			FormToolkit toolkit, final XLayout layout) {
		this.id = id;
		this.sec = (Section) s;
		this.form = f;
		this.xlayout = layout;

		if (!id.endsWith("_show")) //$NON-NLS-1$
			this.sec.addExpansionListener(new ExpansionAdapter() {
				@Override
				public void expansionStateChanged(ExpansionEvent e) {
					xlayout.setModified(true);
					form.reflow(true);
				}
			});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
		sectionClient.setBackground(sec.getBackground());
		sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
		sec.setClient(sectionClient);

		if (id.contains("EditionOf")) { //$NON-NLS-1$
			isEditionOf = true;
		}
	}

	public void addAdvancedControl(AdvancedControlElement adv) {
		controls.add(adv);
		adv.setItemsGroup(this);

		if (isEditionOf) {
			if (adv.getSWTControl() instanceof Button) {
				Button browse = (Button) adv.getSWTControl();
				if (browse != null) {
					browse.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(SelectionEvent e) {
							Text t = (Text) getFirstControlByName("isEditionOf"); //$NON-NLS-1$
							if (t != null) {
								FetchTGObjectsDialog.openDialog(t);
							}
						}
					});
				}
			} else if (adv.getSWTControl() instanceof Link) {
				final Link l = (Link) adv.getSWTControl();
				if (l != null) {
					l.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(SelectionEvent e) {
							Text t = (Text) getFirstControlByName("isEditionOf"); //$NON-NLS-1$
							if (t != null) {
								final String uri = t.getText();
								if (!"".equals(uri) //$NON-NLS-1$
										&& uri.startsWith("textgrid:")) { //$NON-NLS-1$

									new Job("") { //$NON-NLS-1$

										@Override
										protected IStatus run(
												IProgressMonitor arg0) {
											try {
												final TextGridObject obj = TextGridObject
														.getInstance(new URI(
																uri), true);

												if (obj != null
														&& obj.isAccessible()) {
													new UIJob("") { //$NON-NLS-1$

														@Override
														public IStatus runInUIThread(
																IProgressMonitor monitor) {
															MetaDataDialog
																	.openMetaDataDialog(obj);
															return Status.OK_STATUS;
														}
													}.schedule();
												}

												return Status.OK_STATUS;
											} catch (CrudServiceException e1) {
												Activator.handleError(e1);
											} catch (URISyntaxException e1) {
												Activator.handleError(e1);
											}
											return Status.CANCEL_STATUS;
										}
									}.schedule();

								} else {
									MessageDialog.openError(l.getShell(),
											Messages.StandaloneGroupControlElement_invalidTGUri_title,
											Messages.StandaloneGroupControlElement_invalidTGUri_description);
								}
							}
						}
					});
				}
			}
		}
	}

	public Composite getBody() {
		return sectionClient;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public boolean validate() {
		if (!sec.isExpanded())
			return true;

		boolean valid = true;

		for (IControl c : this.getControls()) {
			valid &= c.validate();
		}
		return valid;
	}

	@Override
	public void dispose() {
		for (AdvancedControlElement adv : controls) {
			adv.dispose();
		}
		if (sec != null && !sec.isDisposed())
			sec.dispose();
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {
		if (sec.isExpanded())
			for (AdvancedControlElement e : controls) {
				if (e.getRef() == null) {
					e.createOMElement(omFactory, parent, ns, false);
				} else {
					AdvancedControlElement ac = getAdvancedControlElementById(e
							.getRef());
					if (ac != null) {
						e.createOMElement(omFactory, ac.getOMElement(), ns,
								true);
					}
				}
			}
	}

	private AdvancedControlElement getAdvancedControlElementById(String id) {
		for (AdvancedControlElement e : controls) {
			if (e.getId().equals(id) || e.getRefId().equals(id))
				return e;
		}

		return null;
	}

	/**
	 * Search for the first control with the given name
	 * 
	 * @param name
	 * @return
	 */
	@Override
	public Control getFirstControlByName(String name) {
		for (AdvancedControlElement e : controls) {
			if (e.getName().equals(name))
				return e.getSWTControl();
		}
		return null;
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		boolean found = false;
		for (AdvancedControlElement e : controls) {
			if (e.getRef() == null) {
				found |= e.setOMElement(root, tg);
			} else {
				AdvancedControlElement ac = getAdvancedControlElementById(e
						.getRef());
				if (ac != null) {
					OMElement elem = null;

					try {
						if (tg)
							elem = OMUtil.getElementWithName(ac.getName(),
									root,
									TextGridObject.TEXTGRID_METADATA_NAMESPACE,
									"tg"); //$NON-NLS-1$
						else
							elem = OMUtil.getElementWithName(ac.getName(),
									root, TextGridObject.CUSTOM_NAMESPACE,
									"cns"); //$NON-NLS-1$
					} catch (JaxenException e1) {
						Activator.handleError(e1);
					}

					if (elem != null) {
						e.clear();

						Iterator attrs = elem.getAllAttributes();

						while (attrs.hasNext()) {
							OMAttribute attr = (OMAttribute) attrs.next();
							if (e.getName().equals(attr.getLocalName())) {
								e.addContentToControl(attr.getAttributeValue());
								found = true;
								break;
							}
						}
					}
				}
			}
		}

		if ((sec.getExpansionStyle() & ExpandableComposite.TWISTIE) != 0)
			if (found)
				sec.setExpanded(true);
			else
				sec.setExpanded(false);

		return found;
	}

	@Override
	public void clear() {
		for (AdvancedControlElement adv : controls) {
			adv.clear();
		}
	}

	@Override
	public LinkedList<AdvancedControlElement> getAdvancedControls() {
		return controls;
	}

	@Override
	public List<Control> getSWTControls() {
		java.util.List<Control> con = new ArrayList<Control>();
		for (AdvancedControlElement adv : controls) {
			con.add(adv.getSWTControl());
		}
		return con;
	}

	@Override
	public List<IControl> getControls() {
		java.util.List<IControl> con = new ArrayList<IControl>();
		con.addAll(controls);

		return con;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public void setExpanded(boolean expand) {
		if (sec != null)
			sec.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		if (sec != null)
			return sec.isExpanded();
		return false;
	}
}