/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.TextGridObject;

import java.util.LinkedList;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.jaxen.JaxenException;


/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ComplexRepeatItemsGroup implements IRepresentableItemsGroup {

	private LinkedList<IControl> controls = new LinkedList<IControl>();
	private Button remove_btn = null;

	private ComplexRepeatControlElement parent = null;

	public ComplexRepeatItemsGroup(ComplexRepeatControlElement parent) {
		this.parent = parent;
	}

	void addControl(IControl e) {
		controls.addLast(e);
		if (e instanceof AdvancedControlElement)
			((AdvancedControlElement) e).setItemsGroup(this);
	}

	void addRemoveButton(Button b) {
		remove_btn = b;
	}

	void activateRemoveButton(boolean active) {
		if (remove_btn != null && !remove_btn.isDisposed())
			remove_btn.setEnabled(active);
	}

	@Override
	public void clear() {
		for (IControl c : controls) {
			c.clear();
		}
	}

	String getIdByRefId(String refId) {

		for (IControl c : controls) {
			if (c instanceof IAdvancedControlsContainer) {
				IAdvancedControlsContainer adc = (IAdvancedControlsContainer) c;
				LinkedList<AdvancedControlElement> advancedControls = adc
						.getAdvancedControls();

				for (AdvancedControlElement e : advancedControls) {
					if (e.getRefId().equals(refId))
						return e.getId();
				}
			}
		}

		return refId;
	}

	private AdvancedControlElement getAdvancedControlElementById(String id) {

		for (IControl c : controls) {
			if (c instanceof IAdvancedControlsContainer) {
				IAdvancedControlsContainer adc = (IAdvancedControlsContainer) c;
				LinkedList<AdvancedControlElement> advancedControls = adc
						.getAdvancedControls();

				for (AdvancedControlElement e : advancedControls) {
					if (e.getId().equals(id))
						return e;
				}

			}
		}

		return null;
	}

	void createOMElements(OMFactory omFactory, OMElement parent, OMNamespace ns) {
		for (IControl c : controls) {
			if (c instanceof AdvancedControlElement) {
				AdvancedControlElement e = (AdvancedControlElement) c;
				if (e.getRef() != null) {
					AdvancedControlElement ac = getAdvancedControlElementById(e
							.getRef());
					if (ac != null)
						e.createOMElement(omFactory, ac.getOMElement(), ns,
								true);
				} else {
					e.createOMElement(omFactory, parent, ns, false);
				}
			} else {
				c.createOMElement(omFactory, parent, ns, false);
			}
		}
	}

	@SuppressWarnings("unchecked")
	boolean setOMElement(OMElement root, ComplexRepeatControlElement repE,
			boolean tg) {

		boolean found = false;

		String parent_name = repE.getParentName();

		if (parent_name != null && !"".equals(parent_name)) { //$NON-NLS-1$
			OMElement elem = null;
			java.util.List elems = null;

			try {
				if (tg)
					elems = OMUtil.getElementListWithName(parent_name, root,
							TextGridObject.TEXTGRID_METADATA_NAMESPACE, "tg"); //$NON-NLS-1$
				else
					elems = OMUtil.getElementListWithName(parent_name, root,
							TextGridObject.CUSTOM_NAMESPACE, "cns"); //$NON-NLS-1$
			} catch (JaxenException e) {
				Activator.handleError(e);
				return false;
			}

			boolean setForThisGroup = false;

			for (Object o : elems) {

				elem = (OMElement) o;
				if (elem != null) {
					if (!setForThisGroup) {
						for (IControl c : controls) {
							found |= c.setOMElement(elem, tg);
						}
						setForThisGroup = true;
					} else {
						repE.addControlsGroup();
						repE.getAllGroups().getLast()
								.setOMElement(elem, repE, tg);
					}
				}

			}
		}

		return found;
	}

	@Override
	public void dispose() {

		while (!controls.isEmpty()) {
			IControl e = controls.getLast();
			controls.removeLast();
			e.dispose();
		}
		remove_btn.dispose();
	}

	@Override
	public boolean validate() {
		boolean valid = true;
		for (IControl c : controls) {
			valid &= c.validate();
		}
		return valid;
	}

	/**
	 * @throws NotImplementedException
	 */
	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {
		throw new NotImplementedException();
	}

	/**
	 * @throws NotImplementedException
	 */
	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		throw new NotImplementedException();
	}

	@Override
	public String getId() {
		throw new NotImplementedException();
	}

	@Override
	public java.util.List<Control> getSWTControls() {
		LinkedList<Control> swtControls = new LinkedList<Control>();

		for (IControl c : controls) {
			if (c instanceof IRepresentableControl) {
				swtControls.add(((IRepresentableControl) c).getSWTControl());
			} else if (c instanceof IRepresentableComposite) {
				swtControls.addAll(((IRepresentableComposite) c)
						.getSWTControls());
			}
		}
		swtControls.add(remove_btn);

		return swtControls;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public java.util.List<IControl> getControls() {
		return controls;
	}

	@Override
	public Control getFirstControlByName(String name) {

		for (IControl c : controls) {
			if (c instanceof IAdvancedControlsContainer) {
				IAdvancedControlsContainer adc = (IAdvancedControlsContainer) c;
				LinkedList<AdvancedControlElement> advancedControls = adc
						.getAdvancedControls();

				for (AdvancedControlElement e : advancedControls) {
					if (e.getName().equals(name))
						return e.getSWTControl();
				}

			}
		}

		return null;
	}

	@Override
	public void setExpanded(boolean expand) {
		parent.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		return parent.isExpanded();
	}

}
