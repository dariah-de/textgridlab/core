/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.TextGridObject;

import java.util.Iterator;
import java.util.LinkedList;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.jaxen.JaxenException;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class RepeatItemsGroup implements IRepresentableItemsGroup {

	private LinkedList<AdvancedControlElement> advancedControls = new LinkedList<AdvancedControlElement>();
	private String nameOfTheFirstElement = null;
	private Button remove_btn = null;

	private RepeatControlElement parent = null;

	public RepeatItemsGroup(RepeatControlElement parent) {
		this.parent = parent;
	}

	void addControl(AdvancedControlElement e) {
		advancedControls.addLast(e);
		e.setItemsGroup(this);
	}

	void addRemoveButton(Button b) {
		remove_btn = b;
	}

	void activateRemoveButton(boolean active) {
		if (remove_btn != null && !remove_btn.isDisposed())
			remove_btn.setEnabled(active);
	}

	@Override
	public void clear() {
		for (AdvancedControlElement e : advancedControls) {
			e.clear();
		}
	}

	private void setNameOfTheFirstElement() {

		for (AdvancedControlElement e : advancedControls) {
			if (e.getSWTControl() instanceof Text
					|| e.getSWTControl() instanceof Combo
					|| e.getSWTControl() instanceof List) {
				nameOfTheFirstElement = e.getName();
				break;
			}
		}
	}

	public LinkedList<AdvancedControlElement> getAdvancedControls() {

		return advancedControls;
	}

	public LinkedList<AdvancedControlElement> getAdvancedControlsByRef(
			String refId) {

		LinkedList<AdvancedControlElement> controls = new LinkedList<AdvancedControlElement>();

		for (AdvancedControlElement ad : advancedControls) {
			if (ad.getRefId().equals(refId))
				controls.add(ad);
		}

		return controls;
	}

	/**
	 * Search for the first control with the given name
	 * 
	 * @param name
	 * @return
	 */
	@Override
	public Control getFirstControlByName(String name) {
		for (AdvancedControlElement e : advancedControls) {
			if (e.getName().equals(name))
				return e.getSWTControl();
		}
		return null;
	}

	String getIdByRefId(String refId) {
		for (AdvancedControlElement e : advancedControls) {
			if (e.getRefId().equals(refId))
				return e.getId();
		}

		return refId;
	}

	private AdvancedControlElement getAdvancedControlElementById(String id) {
		for (AdvancedControlElement e : advancedControls) {
			if (e.getId().equals(id) || e.getRefId().equals(id))
				return e;
		}

		return null;
	}

	void createOMElements(OMFactory omFactory, OMElement parent, OMNamespace ns) {
		for (AdvancedControlElement e : advancedControls) {
			if (e.getRef() != null) {
				AdvancedControlElement ac = getAdvancedControlElementById(e
						.getRef());
				if (ac != null) {
					e.createOMElement(omFactory, ac.getOMElement(), ns, true);
				}
			} else {
				e.createOMElement(omFactory, parent, ns, false);
			}
		}
	}

	@SuppressWarnings("unchecked")
	boolean setOMElement(OMElement root, RepeatControlElement repE, boolean tg) {
		boolean found = false;

		if (nameOfTheFirstElement == null)
			setNameOfTheFirstElement();

		OMElement elem = null;
		java.util.List elems = null;
		try {
			if (tg)
				elems = OMUtil.getElementListWithName(nameOfTheFirstElement,
						root, TextGridObject.TEXTGRID_METADATA_NAMESPACE, "tg"); //$NON-NLS-1$
			else
				elems = OMUtil.getElementListWithName(nameOfTheFirstElement,
						root, TextGridObject.CUSTOM_NAMESPACE, "cns"); //$NON-NLS-1$
		} catch (JaxenException e) {
			Activator.handleError(e);
			return false;
		}

		int count = elems.size();

		for (Object o : elems) {

			elem = (OMElement) o;
			if (elem != null) {

				for (AdvancedControlElement e : repE.getAllGroups().getLast().advancedControls) {
					if (e.getName().equals(elem.getLocalName())) {
						e.addContentToControl(elem.getText().trim());
						found = true;
						break;
					}
				}

				Iterator attrs = elem.getAllAttributes();

				while (attrs.hasNext()) {
					OMAttribute attr = (OMAttribute) attrs.next();

					for (AdvancedControlElement e : repE.getAllGroups()
							.getLast().advancedControls) {
						if (e.getName().equals(attr.getLocalName())) {
							e.addContentToControl(attr.getAttributeValue()
									.trim());
							found = true;
							break;
						}
					}

				}
				if (--count > 0) {
					repE.addControlsGroup();
				}
			}
		}

		return found;
	}

	@Override
	public void dispose() {

		while (!advancedControls.isEmpty()) {
			AdvancedControlElement e = advancedControls.getLast();
			// e.getControl().setVisible(false);
			e.dispose();
			advancedControls.removeLast();
		}
		remove_btn.dispose();
		// elements.clear();
	}

	@Override
	public boolean validate() {
		boolean valid = true;
		for (AdvancedControlElement adv : getAdvancedControls()) {
			valid &= adv.validate();
		}
		return valid;
	}

	/**
	 * @throws NotImplementedException
	 */
	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {
		throw new NotImplementedException();
	}

	/**
	 * @throws NotImplementedException
	 */
	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		throw new NotImplementedException();
	}

	@Override
	public String getId() {
		throw new NotImplementedException();
	}

	@Override
	public java.util.List<Control> getSWTControls() {
		LinkedList<Control> l = new LinkedList<Control>();

		for (AdvancedControlElement e : advancedControls) {
			l.add(e.getSWTControl());
		}
		l.add(remove_btn);

		return l;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public java.util.List<IControl> getControls() {
		throw new NotImplementedException();
	}

	@Override
	public void setExpanded(boolean expand) {
		parent.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		return parent.isExpanded();
	}
}
