/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import com.logabit.xlayout.XLayout;

public class SwitchControlElement implements IRepresentableComposite {

	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private String id = null;
	private Composite sectionClient = null;
	private Element dataElement = null;
	private XLayout xlayout = null;

	private SwitchGroup switchGrp1, switchGrp2;

	public SwitchControlElement(final String id, ScrolledForm f,
			Control section, Element element, FormToolkit tk,
			final XLayout layout) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;
		this.xlayout = layout;

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				xlayout.setModified(true);
				form.reflow(true);
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
		sectionClient.setBackground(sec.getBackground());
		sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
		sec.setClient(sectionClient);
	}

	public Composite getBody() {
		return sectionClient;
	}

	@Override
	public String getId() {
		return id;
	}

	public void addFirstGroup(SwitchGroup group) {
		switchGrp1 = group;
		switchGrp1.update(this, form, sectionClient, xlayout);
	}

	public void addSecondGroup(SwitchGroup group) {
		switchGrp2 = group;
		switchGrp2.update(this, form, sectionClient, xlayout);

		switchGrp1.create();
		form.reflow(true);
	}

	public void switchToOtherGroup(SwitchGroup activeGrp) {
		if (activeGrp == switchGrp1) {
			switchGrp1.destroy();
			form.reflow(true);
			switchGrp2.create();
		} else {
			switchGrp2.destroy();
			form.reflow(true);
			switchGrp1.create();
		}
		sec.setExpanded(false);
		sec.setExpanded(true);
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {
		switchGrp1.createOMElement(omFactory, parent, ns, asAttribute);
		switchGrp2.createOMElement(omFactory, parent, ns, asAttribute);
	}

	@Override
	public void dispose() {
		switchGrp1.dispose();
		switchGrp2.dispose();

		if (sec != null && !sec.isDisposed())
			sec.dispose();
	}

	@Override
	public boolean validate() {
		boolean valid = true;
		valid &= switchGrp1.validate();
		valid &= switchGrp2.validate();

		return valid;
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		boolean found = false, switchToGrp2 = false;
		found |= switchGrp1.setOMElement(root, tg);
		switchToGrp2 = switchGrp2.setOMElement(root, tg);
		found |= switchToGrp2;

		if (switchToGrp2)
			this.switchToOtherGroup(switchGrp1);

		return found;
	}

	@Override
	public List<Control> getSWTControls() {
		ArrayList<Control> l = new ArrayList<Control>();
		l.addAll(switchGrp1.getControls());
		l.addAll(switchGrp2.getControls());
		return l;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public List<IControl> getControls() {
		throw new NotImplementedException();
	}

	@Override
	public void clear() {
		switchGrp1.clear();
		switchGrp2.clear();
	}

	@Override
	public void setExpanded(boolean expand) {
		if (sec != null)
			sec.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		if (sec != null)
			return sec.isExpanded();
		return false;
	}
}
