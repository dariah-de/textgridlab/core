package info.textgrid.lab.core.metadataeditor.elements;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.metadataeditor.elements.messages"; //$NON-NLS-1$
	public static String AdvancedControlElement_autoCompleteDescription;
	public static String ComplexRepeatControlElement_addAddidtional_button;
	public static String ComplexRepeatControlElement_addAdditional_button;
	public static String ComplexRepeatControlElement_element_title;
	public static String ComplexRepeatControlElement_remove_button;
	public static String OptionalControlElement_optional;
	public static String RepeatControlElement_addAdditional_button;
	public static String RepeatControlElement_element_title;
	public static String RepeatControlElement_notImplemented_exception;
	public static String RepeatControlElement_remove_button;
	public static String SourceControlElement_addBibliographicCitation;
	public static String SourceControlElement_addObjectCitation;
	public static String SourceControlElement_element_title;
	public static String StandaloneGroupControlElement_invalidTGUri_description;
	public static String StandaloneGroupControlElement_invalidTGUri_title;
	public static String TextgridDateControlElement_approxDate_original;
	public static String TextgridDateControlElement_approxDateRangeException;
	public static String TextgridDateControlElement_approxGregDate;
	public static String TextgridDateControlElement_approxGregDate_example;
	public static String TextgridDateControlElement_NotAfterApproxGregDate;
	public static String TextgridDateControlElement_NotBeforeApproxGregDate;
	public static String TextgridDateControlElement_switchApproxDate;
	public static String TextgridDateControlElement_switchDateRange;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
