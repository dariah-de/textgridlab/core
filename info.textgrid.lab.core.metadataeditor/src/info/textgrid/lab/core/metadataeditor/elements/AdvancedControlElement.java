/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.autoCompletion.AutoCompletionUtil;
import info.textgrid.lab.core.metadataeditor.autoCompletion.LocalAutoCompletionClient;
import info.textgrid.lab.core.metadataeditor.autoCompletion.PNDAutoCompletionClient;
import info.textgrid.lab.core.metadataeditor.autoCompletion.TGNAutoCompletionClient;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.search.AutocompletionClient;
import info.textgrid.lab.search.ExtendedContentProposalAdapter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalListener;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.jaxen.JaxenException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.logabit.xlayout.XLayout;

/**
 * This class is a decorator-class for the swt-controls.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class AdvancedControlElement implements IRepresentableControl,
		IAdvancedControlsContainer, ICheckableElement {

	private Control control = null;

	private String id = null;
	private String name = null;
	private String ref = null;

	private String refId = null;
	private int readOnlyFlag = 0;

	private OMElement element = null;
	private XLayout xlayout = null;

	private String inputType = "string"; //$NON-NLS-1$
	private boolean required = false;

	private Label label = null;

	private Color defaultColor;

	private String autoCompletionField = null;

	private IRepresentableItemsGroup itemsGroup = null;

	private FieldDecoration assistFieldIndicator = FieldDecorationRegistry
			.getDefault().getFieldDecoration(
					FieldDecorationRegistry.DEC_CONTENT_PROPOSAL);

	private char[] autoActivationCharacters = null;
	private KeyStroke keyStroke = null; // KeyStroke.getInstance(SWT.CTRL, 32);
										// // CTRL+TAB

	public String getAutoCompletionField() {
		return autoCompletionField;
	}

	public void setAutoCompletionField(String autoCompletionField) {
		this.autoCompletionField = autoCompletionField;

		if (this.autoCompletionField != null
				&& !"".equals(this.autoCompletionField)) { //$NON-NLS-1$
			setAutoCompletion(control, this.autoCompletionField);
		}
	}

	public void setItemsGroup(IRepresentableItemsGroup group) {
		itemsGroup = group;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
		this.defaultColor = this.label.getForeground();

		if (!"string".equals(inputType)) { //$NON-NLS-1$
			if ("TextGrid-Uri".equals(inputType) //$NON-NLS-1$
					&& !this.label.getText().contains(this.inputType)) {
				this.label.setText(this.label.getText() + " (" + this.inputType //$NON-NLS-1$
						+ ")"); //$NON-NLS-1$
			} else if (!this.label.getText().contains("xs:" + this.inputType)) { //$NON-NLS-1$
				this.label.setText(this.label.getText()
						+ " (xs:" //$NON-NLS-1$
						+ this.inputType
						+ ("date".equals(inputType) ? ", YYYY-MM-DD" : ("time" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								.equals(inputType) ? ", hh:mm:ss" : "")) + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}

		if (isRequired() && !this.label.getText().contains("*")) //$NON-NLS-1$
			this.label.setText(this.label.getText() + " *"); //$NON-NLS-1$
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	@Override
	public boolean isRequired() {
		return required;
	}

	@Override
	public void setRequired(boolean required) {
		this.required = required;
		if (required && itemsGroup != null)
			itemsGroup.setExpanded(true);
	}

	/**
	 * The constructor which creates the advanced control.
	 * 
	 * @param control
	 *            the used control
	 * @param id
	 *            the desired id
	 * @param name
	 *            the desired name
	 * @param ref
	 *            the id of the referenced element
	 * @param readOnlyFlag
	 * @param xlayout
	 *            the used {@link XLayout} instance
	 */
	public AdvancedControlElement(Control control, String id, String name,
			String ref, int readOnlyFlag, XLayout xlayout) {

		this.control = control;
		this.id = this.refId = id;
		this.name = name;
		this.ref = ref;
		this.readOnlyFlag = readOnlyFlag;

		this.xlayout = xlayout;

		this.defaultColor = this.control.getDisplay().getSystemColor(
				SWT.COLOR_BLACK);

		// XXX if name is empty, then take the id as element-name
		if (this.name == null) {
			this.name = id;
		}
	}

	/**
	 * @return the {@link #id}.
	 */
	@Override
	public String getId() {

		return id;
	}

	/**
	 * Sets the refId which is used to bind all controls of a
	 * {@link RepeatControlElement} in one unit.
	 * 
	 * @param refId
	 */
	void setRefId(String refId) {

		this.refId = refId;
	}

	/**
	 * @return the {@link #refId}
	 */
	public String getRefId() {

		return refId;
	}

	/**
	 * @return the {@link #name}.
	 */
	public String getName() {

		return name;
	}

	/**
	 * @return the {@link #ref}.
	 */
	public String getRef() {

		return ref;
	}

	/**
	 * @return the {@link #control}.
	 */
	@Override
	public Control getSWTControl() {
		return control;
	}

	/**
	 * @return the {@link #element}
	 */
	public OMElement getOMElement() {

		return element;
	}

	/**
	 * @return the {@link #readOnlyFlag}.
	 */
	public int getReadOnlyFlag() {

		return readOnlyFlag;
	}

	/**
	 * Creates the {@link OMElement} for the actual instance.
	 * 
	 * @param omFactory
	 *            the OM-factory
	 * @param parent
	 *            the parent {@link OMElement}
	 * @param ns
	 *            the Namespace
	 * @param asAttribute
	 *            determines whether the new {@link OMElement} should be bounded
	 *            to the parent-OMElement as attribute or as child element
	 * 
	 */
	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {

		if (control instanceof Label || control instanceof Button)
			return;

		if (!asAttribute)
			createOMElementControl(omFactory, parent, ns);
		else
			createOMElementControlAsAttribute(omFactory, parent);
	}

	/**
	 * Sets the content of the referred {@link OMElement} in this
	 * {@link AdvancedControlElement}.
	 * 
	 * @param root
	 *            the {@link OMElement} to set
	 * @param tg
	 *            determines whether the the Namespace is a TG-Namsespace
	 *            (descriptive) or not (custom)
	 */
	@Override
	public boolean setOMElement(OMElement root, boolean tg) {

		if (control instanceof Label || control instanceof Button)
			return false;

		clear();

		if (root == null)
			return false;

		OMElement elem = null;
		try {
			if (tg)
				elem = OMUtil.getElementWithName(this.name, root,
						TextGridObject.TEXTGRID_METADATA_NAMESPACE, "tg"); //$NON-NLS-1$
			else
				elem = OMUtil.getElementWithName(this.name, root,
						TextGridObject.CUSTOM_NAMESPACE, "cns"); //$NON-NLS-1$
		} catch (JaxenException e) {
			Activator.handleError(e);
		}

		if (elem != null) {
			addContentToControl(elem.getText());
			return true;
		}

		return false;
	}

	/**
	 * Adds the referred content to the control.
	 */
	public void addContentToControl(String content) {

		if (control instanceof Text) {
			Text t = (Text) control;
			setTextValue(t, content);
		} else if (control instanceof Combo) {
			Combo c = (Combo) control;
			String t = (String) c.getData(content);
			if (t != null && !"".equals(t)) //$NON-NLS-1$
				content = t;
			c.setText(content);
		} else if (control instanceof List) {
			List l = (List) control;
			l.setSelection(new String[] { content });
		} else if (control instanceof Button) {
			Button b = (Button) control;
			if ("true".equals(content)) //$NON-NLS-1$
				b.setSelection(true);
			else
				b.setSelection(false);
		}
	}

	/**
	 * Creates the {@link OMElement} as a child element in the parent
	 * {@link OMElement}. This method is used in
	 * 
	 * @see #createOMElement(OMFactory, OMElement, OMNamespace, boolean)
	 * 
	 * @param omFactory
	 * @param parent
	 * @param ns
	 */
	private void createOMElementControl(OMFactory omFactory, OMElement parent,
			OMNamespace ns) {

		String value = ""; //$NON-NLS-1$

		if (control instanceof Text) {
			Text t = (Text) control;
			value = getTextValue(t);
		} else if (control instanceof Combo) {
			Combo c = (Combo) control;
			value = c.getText();
			String t = (String) c.getData(value);
			if (t != null && !"".equals(t)) //$NON-NLS-1$
				value = t;
		} else if (control instanceof List) {
			List l = (List) control;
			if (l.getSelectionIndex() > -1)
				value = l.getItem(l.getSelectionIndex());
		} else if (control instanceof Button) {
			Button b = (Button) control;
			if (b.getSelection()) {
				value = "true"; //$NON-NLS-1$
			} else {
				value = "false"; //$NON-NLS-1$
			}
		} else {
			// XXX no suitable controls
			return;
		}

		element = omFactory.createOMElement(this.name, ns);
		element.setText(value);

		parent.addChild(element);
	}

	/**
	 * Creates the {@link OMElement} as an attribute in the parent
	 * {@link OMElement}. This method is used in
	 * 
	 * @see #createOMElement(OMFactory, OMElement, OMNamespace, boolean)
	 * 
	 * @param omFactory
	 * @param parent
	 */
	private void createOMElementControlAsAttribute(OMFactory omFactory,
			OMElement parent) {

		String value = ""; //$NON-NLS-1$

		if (control instanceof Text) {
			Text t = (Text) control;
			value = getTextValue(t);
		} else if (control instanceof Combo) {
			Combo c = (Combo) control;
			value = c.getText();
			String t = (String) c.getData(value);
			if (t != null && !"".equals(t)) //$NON-NLS-1$
				value = t;
		} else if (control instanceof List) {
			List l = (List) control;
			if (l.getSelectionIndex() > -1)
				value = l.getItem(l.getSelectionIndex());
		} else if (control instanceof Button) {
			Button b = (Button) control;
			if (b.getSelection()) {
				value = "true"; //$NON-NLS-1$
			} else {
				value = "false"; //$NON-NLS-1$
			}
		}

		if (!"".equals(value)) //$NON-NLS-1$
			parent.addAttribute(omFactory.createOMAttribute(this.name, null,
					value));
	}

	private String getTextValue(Text t) {

		String value = t.getText();

		// whitespaces should be collapsed for uris
		// see TG-1645
		if (inputType.toLowerCase().contains("uri")) //$NON-NLS-1$
			value = value.replaceAll("\\s", ""); //$NON-NLS-1$ //$NON-NLS-2$
		
		// // otherwise trim the text if not multi
		// else if ((t.getStyle() & SWT.MULTI) == 0)
		// value = value.trim();

		if (this.autoCompletionField != null
				&& "language".equals(this.autoCompletionField)) { //$NON-NLS-1$
			String res = AutoCompletionUtil.getInstance().getLanguagesMap()
					.get(value);
			if (res != null)
				value = res;
		}

		return value;
	}

	private void setTextValue(Text t, String content) {

		if (this.autoCompletionField != null
				&& "language".equals(this.autoCompletionField)) { //$NON-NLS-1$
			String res = AutoCompletionUtil.getInstance().getLanguagesMap()
					.get(content);
			if (res != null)
				content = res;
		}

		t.setText(content);
	}

	/**
	 * Clears the content of the used control.
	 */
	@Override
	public void clear() {
		if (control == null || control.isDisposed())
			return;

		if (control instanceof Text) {
			((Text) control).setText(""); //$NON-NLS-1$
		} else if (control instanceof Combo) {
			((Combo) control).select(0);
		} else if (control instanceof List) {
			((List) control).select(0);
		} else {
			// XXX no suitable controls
			return;
		}

		if (xlayout != null)
			xlayout.setModified(false);
	}

	/**
	 * Disposes the used control.
	 */
	@Override
	public void dispose() {

		if (control != null)
			control.dispose();
	}

	/**
	 * Validate the content of the control. The functionality of this method is
	 * implemented in this class and in {@link TextgridDateControlElement}
	 */
	@Override
	public boolean validate() {

		if (this.control == null || this.control.isDisposed())
			return true;

		boolean valid = true;
		if (this.control instanceof Text || this.control instanceof Combo) {

			String content = ""; //$NON-NLS-1$

			if (this.control instanceof Text) {
				content = getTextValue((Text) control);
			} else {
				Combo c = (Combo) control;
				content = c.getText();
				String t = (String) c.getData(content);
				if (t != null && !"".equals(t)) //$NON-NLS-1$
					content = t;
			}

			if ("language".equals(name) && !"".equals(content)) { //$NON-NLS-1$ //$NON-NLS-2$
				valid &= AutoCompletionUtil.getInstance().getLanguagesMap()
						.containsKey(content);
			}

			if (this.isRequired()) {
				valid &= !"".equals(content); //$NON-NLS-1$
			}

			if (!valid) {
				colorizeLabel(valid);
				return valid;
			}

			if (!"string".equals(this.inputType)) { //$NON-NLS-1$
				if (this.isRequired() || !"".equals(content)) { //$NON-NLS-1$

					if ("boolean".equals(this.inputType)) { //$NON-NLS-1$
						content = content.trim();
						if ("true".equalsIgnoreCase(content) //$NON-NLS-1$
								|| "false".equalsIgnoreCase(content) //$NON-NLS-1$
								|| "0".equals(content) //$NON-NLS-1$
								|| "1".equalsIgnoreCase(content)) //$NON-NLS-1$
							valid &= true;
						else
							valid = false;

						colorizeLabel(valid);
						return valid;
					}

					valid &= testInputType(content);
					colorizeLabel(valid);
					return valid;
				}
			}
		} // not completed!

		colorizeLabel(valid);

		return valid;
	}

	private void colorizeLabel(boolean valid) {
		if (!valid) {
			if (label != null)
				label.setForeground(label.getDisplay().getSystemColor(
						SWT.COLOR_RED));
		} else {
			if (label != null)
				label.setForeground(defaultColor);
		}
	}

	private boolean validType = true;

	private boolean testInputType(String input) {

		String inputType = this.inputType;

		if ("TextGrid-Uri".equals(inputType)) { //$NON-NLS-1$

			if (!input.startsWith("textgrid:")) //$NON-NLS-1$
				return false;

			inputType = "anyURI"; //$NON-NLS-1$
		}

		validType = true;
		SchemaFactory factory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		factory.setErrorHandler(new ErrorHandler() {

			@Override
			public void warning(SAXParseException exception)
					throws SAXException {
				Activator.handleWarning(exception);
			}

			@Override
			public void fatalError(SAXParseException exception)
					throws SAXException {
				validType = false;
			}

			@Override
			public void error(SAXParseException exception) throws SAXException {
				validType = false;

			}
		});

		try {
			StreamSource ss = new StreamSource(
					new ByteArrayInputStream(
							("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" //$NON-NLS-1$
									+ "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">" //$NON-NLS-1$
									+ "<xs:element name=\"" + this.name //$NON-NLS-1$
									+ "\" type=\"xs:" + inputType //$NON-NLS-1$
									+ "\"></xs:element>" + "</xs:schema>") //$NON-NLS-1$ //$NON-NLS-2$
									.getBytes("UTF-8"))); //$NON-NLS-1$
			StreamSource ss2 = new StreamSource(new ByteArrayInputStream(
					("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<" //$NON-NLS-1$ //$NON-NLS-2$
							+ this.name + ">" + input + "</" + this.name + ">") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							.getBytes("UTF-8"))); //$NON-NLS-1$
			Schema s = factory.newSchema(ss);

			Validator v = s.newValidator();
			v.validate(ss2);
		} catch (UnsupportedEncodingException e) {
			validType = false;
		} catch (SAXException e) {
			validType = false;
		} catch (IOException e) {
			validType = false;
		}

		return validType;
	}

	@Override
	public LinkedList<AdvancedControlElement> getAdvancedControls() {
		LinkedList<AdvancedControlElement> controls = new LinkedList<AdvancedControlElement>();
		controls.add(this);

		return controls;
	}

	private AutocompletionClient proposalProvider = null;

	private void setAutoCompletion(Control control, final String whichField) {
		ControlDecoration typDec = new ControlDecoration(control, SWT.TOP
				| SWT.LEFT);
		typDec.setDescriptionText(Messages.AdvancedControlElement_autoCompleteDescription);
		typDec.setImage(assistFieldIndicator.getImage());
		typDec.setMarginWidth(4);
		typDec.setShowOnlyOnFocus(true);

		if ("language".equals(whichField)) { //$NON-NLS-1$
			proposalProvider = new LocalAutoCompletionClient(whichField);
		} else if ("pnd".equals(whichField)) { //$NON-NLS-1$
			proposalProvider = new PNDAutoCompletionClient(whichField);
		} else if ("tgn".equals(whichField)) { //$NON-NLS-1$
			proposalProvider = new TGNAutoCompletionClient(whichField);
		}

		if (proposalProvider != null) {
			ExtendedContentProposalAdapter adapter = new ExtendedContentProposalAdapter(
					control, new TextContentAdapter(), proposalProvider,
					keyStroke, autoActivationCharacters);
			adapter.setAutoActivationDelay(500);
			adapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);

			proposalProvider.setContentProposalAdapter(adapter);

			if ("pnd".equals(whichField)) { //$NON-NLS-1$
				adapter.setAutoActivationDelay(1300);
				adapter.addContentProposalListener(new IContentProposalListener() {

					@Override
					public void proposalAccepted(IContentProposal proposal) {

						if (itemsGroup != null) {

							Control nameCon = getSWTControl();
							if (nameCon != null && nameCon instanceof Text) {
								Text t = (Text) nameCon;
								String name = t.getText();

								if (name != null && !"".equals(t)) { //$NON-NLS-1$
									t.setText(name.replaceFirst("\\s\\[.+\\]", //$NON-NLS-1$
											"")); //$NON-NLS-1$
								}
							}

							Control idCon = itemsGroup
									.getFirstControlByName("id"); //$NON-NLS-1$
							if (idCon != null && idCon instanceof Text) {

								String id = ((PNDAutoCompletionClient) proposalProvider)
										.getIdByName(proposal.getContent());
								if (id != null)
									((Text) idCon).setText(id);

							}
						}

					}
				});
			} else if ("tgn".equals(whichField)) { //$NON-NLS-1$
				adapter.setAutoActivationDelay(1300);
				adapter.addContentProposalListener(new IContentProposalListener() {

					@Override
					public void proposalAccepted(IContentProposal proposal) {

						if (itemsGroup != null) {
							Control idCon = itemsGroup
									.getFirstControlByName("id"); //$NON-NLS-1$
							if (idCon != null && idCon instanceof Text) {

								String id = ((TGNAutoCompletionClient) proposalProvider)
										.getIdByName(proposal.getContent());
								if (id != null) {
									((Text) idCon).setText(id);
								}

								Control typeCon = itemsGroup
										.getFirstControlByName("type"); //$NON-NLS-1$
								if (typeCon != null && typeCon instanceof Text) {
									((Text) typeCon)
											.setText(TGNAutoCompletionClient.NS);
								}
							}
						}

					}
				});
			}

		}
	}

}
