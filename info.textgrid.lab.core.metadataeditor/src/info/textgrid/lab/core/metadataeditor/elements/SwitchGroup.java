/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ScrolledForm;

import com.logabit.xlayout.XLayout;

public class SwitchGroup implements IControl {

	private Composite sectionClient = null;
	private ScrolledForm form = null;
	private String id = null;
	private Element dataElement = null;
	private XLayout xlayout = null;

	private Button switcherBtn = null;
	private ArrayList<IControl> controls = null;

	private SwitchControlElement switchElem = null;

	private boolean active = false;

	private Composite composite = new Composite(PlatformUI.getWorkbench()
			.getActiveWorkbenchWindow().getShell(), 0);

	public Composite getComposite() {
		return composite;
	}

	public SwitchGroup(final String id, Element element) {
		this.id = id;
		dataElement = element;
	}

	private SwitchGroup getInstance() {
		return this;
	}

	public void create() {
		createRecursive(composite);
		active = true;
	}

	private void createRecursive(Composite comp) {
		for (Control c : comp.getChildren()) {
			if (c instanceof Composite) {
				createRecursive((Composite) c);
				continue;
			}
			c.setParent(sectionClient);
		}
	}

	public void destroy() {
		destroyRecursive(sectionClient);
		active = false;
	}

	private void destroyRecursive(Composite comp) {
		for (Control c : comp.getChildren()) {
			if (c instanceof Composite) {
				destroyRecursive((Composite) c);
				continue;
			}
			c.setParent(composite);
		}
	}

	public void update(SwitchControlElement elem, ScrolledForm form,
			Composite sectionClient, final XLayout xlayout) {
		switchElem = elem;
		this.sectionClient = sectionClient;
		this.form = form;
		this.xlayout = xlayout;
	}

	@Override
	public String getId() {
		return id;
	}

	public void addControl(IControl control) {
		if (controls == null)
			controls = new ArrayList<IControl>();

		controls.add(control);
	}

	public List<Control> getControls() {
		ArrayList<Control> l = new ArrayList<Control>();
		if (active) {
			for (Control c : sectionClient.getChildren())
				l.add(c);
		}

		return l;
	}

	public void addSwitcher(Button switcher) {
		this.switcherBtn = switcher;

		switcherBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				switchElem.switchToOtherGroup(getInstance());
				xlayout.setModified(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {
		if (active)
			for (IControl e : controls)
				e.createOMElement(omFactory, parent, ns, asAttribute);
	}

	@Override
	public void dispose() {
		if (controls != null) {
			for (IControl e : controls)
				e.dispose();
			controls.clear();
		}

		if (switcherBtn != null)
			switcherBtn.dispose();
	}

	@Override
	public boolean validate() {
		if (!active)
			return true;

		boolean valid = true;
		for (IControl e : controls) {
			valid &= e.validate();
		}

		return valid;
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		boolean found = false;
		for (IControl c : controls) {
			found |= c.setOMElement(root, tg);
		}
		return found;
	}

	@Override
	public void clear() {
		for (IControl c : controls) {
			c.clear();
		}
	}
}