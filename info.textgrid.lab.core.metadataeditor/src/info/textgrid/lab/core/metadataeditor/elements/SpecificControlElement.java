/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class SpecificControlElement implements IRepresentableComposite {

	private Composite section = null;
	private String id = null;

	@SuppressWarnings("unused")
	private Element dataElement = null;

	private ArrayList<IControl> controls = new ArrayList<IControl>();

	public SpecificControlElement(final String id, ScrolledForm f,
			Control section, Element element) {
		this.id = id;
		this.section = (Composite) section;
		this.dataElement = element;
	}

	public Composite getBody() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	public void addControlElement(IControl elem) {
		controls.add(elem);
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {

		for (IControl con : controls) {
			con.createOMElement(omFactory, parent, ns, asAttribute);
		}
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		boolean found = false;
		for (IControl con : controls) {
			found |= con.setOMElement(root, tg);
		}

		return found;
	}

	@Override
	public void dispose() {
		for (IControl c : controls) {
			c.dispose();
		}
	}

	@Override
	public boolean validate() {
		boolean valid = true;

		for (IControl c : this.getControls()) {
			valid &= c.validate();
		}
		return valid;
	}

	@Override
	public List<Control> getSWTControls() {
		java.util.List<Control> controls = new ArrayList<Control>();

		for (IControl con : this.controls) {
			if (con instanceof IRepresentableControl) {
				controls.add(((IRepresentableControl) con).getSWTControl());
			} else if (con instanceof IRepresentableComposite) {
				controls.addAll(((IRepresentableComposite) con)
						.getSWTControls());
			}
		}

		return controls;
	}

	@Override
	public List<IControl> getControls() {
		return this.controls;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public void clear() {
		for (IControl c : controls) {
			c.clear();
		}
	}

	@Override
	public void setExpanded(boolean expand) {

	}

	@Override
	public boolean isExpanded() {
		return false;
	}
}