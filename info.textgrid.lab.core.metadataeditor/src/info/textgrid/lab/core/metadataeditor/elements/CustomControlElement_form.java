/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import java.util.ArrayList;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
@Deprecated
public class CustomControlElement_form {

	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private String id = null;
	private Composite sectionClient = null;

	@SuppressWarnings("unused")
	private Element dataElement = null;

	private ArrayList<IControl> controls = new ArrayList<IControl>();

	public CustomControlElement_form(final String id, ScrolledForm f,
			Control section, Element element, FormToolkit tk) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.reflow(true);
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER);
		sectionClient.setBackground(sec.getBackground());
		sec.setClient(sectionClient);
		sectionClient.setLayout(new TableWrapLayout());
		sectionClient.setSize(250, 300);
	}

	public Composite getBody() {

		return sectionClient;
	}

	public String getId() {

		return id;
	}

	public void addRepeatElement(RepeatControlElement elem) {
		controls.add(elem);
	}

	public void addAdvancedControl(AdvancedControlElement control) {
		controls.add(control);
	}

	void createOMElement(OMFactory omFactory, OMElement parent, OMNamespace ns,
			boolean asAttribute) {

		for (IControl con : controls) {
			con.createOMElement(omFactory, parent, ns, asAttribute);
		}
	}

	void setOMElement(OMElement root, boolean tg) {

		for (IControl con : controls) {
			con.setOMElement(root, tg);
		}
	}

	public void dispose() {
		for (IControl c : controls) {
			c.dispose();
		}
	}

}