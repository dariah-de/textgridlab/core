/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import com.logabit.xlayout.XLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class MoreControlElement implements IRepresentableComposite {

	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private String id = null;
	private Composite sectionClient = null;
	private Element dataElement = null;
	private XLayout xlayout = null;

	private ArrayList<IControl> controls = new ArrayList<IControl>();

	public MoreControlElement(final String id, ScrolledForm f, Control section,
			Element element, FormToolkit tk, final XLayout layout) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;
		this.xlayout = layout;

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				xlayout.setModified(true);
				form.reflow(true);
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
		sectionClient.setBackground(sec.getBackground());
		sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
		sec.setClient(sectionClient);
	}

	public Composite getBody() {
		return sectionClient;
	}

	@Override
	public String getId() {
		return id;
	}

	public void addControlElement(IControl elem) {
		controls.add(elem);
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean asAttribute) {
		if (sec.isExpanded())
			for (IControl con : controls) {
				con.createOMElement(omFactory, parent, ns, asAttribute);
			}
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		boolean found = false;
		for (IControl con : controls) {
			found |= con.setOMElement(root, tg);
		}

		if ((sec.getExpansionStyle() & ExpandableComposite.TWISTIE) != 0)
			if (found)
				sec.setExpanded(true);
			else
				sec.setExpanded(false);

		return found;
	}

	@Override
	public void dispose() {
		for (IControl c : controls) {
			c.dispose();
		}

		if (sec != null && !sec.isDisposed())
			sec.dispose();
	}

	@Override
	public boolean validate() {
		if (!sec.isExpanded())
			return true;

		boolean valid = true;

		for (IControl c : this.getControls()) {
			valid &= c.validate();
		}
		return valid;
	}

	@Override
	public List<Control> getSWTControls() {
		java.util.List<Control> controls = new ArrayList<Control>();

		for (IControl con : this.controls) {
			if (con instanceof IRepresentableControl) {
				controls.add(((IRepresentableControl) con).getSWTControl());
			} else if (con instanceof IRepresentableComposite) {
				controls.addAll(((IRepresentableComposite) con)
						.getSWTControls());
			}
		}

		return controls;
	}

	@Override
	public List<IControl> getControls() {
		return this.controls;
	}

	@Override
	public IRepresentableComposite getUnit() {
		return this;
	}

	@Override
	public void clear() {
		for (IControl c : controls)
			c.clear();
	}

	@Override
	public void setExpanded(boolean expand) {
		if (sec != null)
			sec.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		if (sec != null)
			return sec.isExpanded();
		return false;
	}
}