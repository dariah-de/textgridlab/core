/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.elements;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.utils.AddItems;

import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Element;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import com.logabit.xlayout.XLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class RepeatControlElement implements IRepresentableComposite,
		IAdvancedControlsContainer {

	private LinkedList<RepeatItemsGroup> groups = new LinkedList<RepeatItemsGroup>();
	private RepeatItemsGroup group_ = null;
	private Section sec = null;
	private ScrolledForm form = null;
	private FormToolkit toolkit = null;
	private String id = null;
	private String title = null;
	private Composite sectionClient = null;
	private Element dataElement = null;
	private XLayout xlayout = null;
	private int beginCount = 0;
	private boolean empty = true;
	private Button add_btn = null;
	private String element_name = Messages.RepeatControlElement_element_title;
	private String element_name_on_label = Messages.RepeatControlElement_element_title;

	private String parent_name = null;

	private static int suffix_txt = 1;
	private static int suffix_lbl = 1;
	private static int suffix_lst = 1;
	private static int suffix_cmb = 1;
	private static int suffix_btn = 1;

	// private boolean usedInComplexRepeatable = false;

	public RepeatControlElement createNewRepeatControlElementForComplexRepeatable(
			final String id, ScrolledForm f, Control section) {
		RepeatControlElement rep = new RepeatControlElement(id, f, section,
				dataElement, toolkit, xlayout);
		// rep.usedInComplexRepeatable = true;

		return rep;
	}

	public RepeatControlElement(final String id, ScrolledForm f,
			Control section, Element element, FormToolkit tk,
			final XLayout layout) {

		this.id = id;
		sec = (Section) section;
		form = f;
		toolkit = tk;
		dataElement = element;
		this.xlayout = layout;

		title = dataElement.attributeValue("text"); //$NON-NLS-1$

		if (title != null) {
			element_name = title;
			sec.setText(element_name);
			element_name_on_label = element_name.replaceAll("\\(.+\\)", ""); //$NON-NLS-1$ //$NON-NLS-2$
		}

		parent_name = dataElement.attributeValue(XLayout.ATTR_ELEMENT_NAME);

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				xlayout.setModified(true);
				form.reflow(true);
			}
		});

		sectionClient = toolkit.createComposite(sec, SWT.BORDER | SWT.FILL);
		sectionClient.setBackground(sec.getBackground());
		sectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
		sec.setClient(sectionClient);
	}

	void addControlsGroup() {

		if (add_btn != null)
			add_btn.dispose();

		final RepeatItemsGroup group = new RepeatItemsGroup(this);
		Label elemLabel = null;

		for (AdvancedControlElement ac : groups.getFirst()
				.getAdvancedControls()) {

			if (ac.getSWTControl() instanceof Label) {
				Label bb = (Label) ac.getSWTControl();
				if (ac.getId().equals("lbl_seperator")) { //$NON-NLS-1$
					continue;
				}
				Label b = toolkit.createLabel(sectionClient, bb.getText());
				elemLabel = b;
				b.setBackground(sectionClient.getBackground());

				AdvancedControlElement ace = new AdvancedControlElement(b,
						"lbl_dynamic_" + suffix_lbl++, ac.getName(), null, 0, //$NON-NLS-1$
						null);

				group.addControl(ace);
			} else if (ac.getSWTControl() instanceof Text) {
				Text tt = (Text) ac.getSWTControl();
				Text t = toolkit.createText(sectionClient, /* tt.getText() */
				"", tt.getStyle() | /* SWT.MULTI | */ac.getReadOnlyFlag()); //$NON-NLS-1$
				t.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
						true, false));

				if (tt.isListening(SWT.KeyDown)) {
					t.addKeyListener(new KeyListener() {
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
						}

						@Override
						public void keyPressed(KeyEvent e) {
							xlayout.getForm().reflow(true);
						}
					});
				}

				t.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						xlayout.setModified(true);
					}
				});
				t.addFocusListener(new FocusListener() {
					@Override
					public void focusGained(FocusEvent e) {
					}

					@Override
					public void focusLost(FocusEvent e) {
						xlayout.fireFocusEvent();
					}
				});

				String id = "txt_dynamic_" + suffix_txt++; //$NON-NLS-1$
				AdvancedControlElement ace = new AdvancedControlElement(t, id,
						ac.getName(), group.getIdByRefId(ac.getRef()),
						ac.getReadOnlyFlag(), null);
				ace.setRefId(ac.getId());

				ace.setInputType(ac.getInputType());
				ace.setRequired(ac.isRequired());

				ace.setAutoCompletionField(ac.getAutoCompletionField());

				if (elemLabel != null) {
					ace.setLabel(elemLabel);
					elemLabel = null;
				}

				group.addControl(ace);
			} else if (ac.getSWTControl() instanceof Combo) {
				Combo coco = (Combo) ac.getSWTControl();
				Combo com = new Combo(sectionClient, SWT.BORDER
						| ac.getReadOnlyFlag());

				// TG-1582
				if ("formOfNotation".equals(ac.getName())) //$NON-NLS-1$
					AddItems.addScriptItems(com);
				else
					com.setItems(coco.getItems());

				com.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						xlayout.setModified(true);
					}
				});
				com.addFocusListener(new FocusListener() {
					@Override
					public void focusGained(FocusEvent e) {
					}

					@Override
					public void focusLost(FocusEvent e) {
						xlayout.fireFocusEvent();
					}
				});

				int selected = coco.getSelectionIndex();
				if (selected > -1)
					com.select(selected);

				String id = "cmb_dynamic_" + suffix_cmb++; //$NON-NLS-1$
				AdvancedControlElement ace = new AdvancedControlElement(com,
						id, ac.getName(), group.getIdByRefId(ac.getRef()),
						ac.getReadOnlyFlag(), null);
				ace.setRefId(ac.getId());

				ace.setInputType(ac.getInputType());
				ace.setRequired(ac.isRequired());

				ace.setAutoCompletionField(ac.getAutoCompletionField());

				if (elemLabel != null) {
					ace.setLabel(elemLabel);
					elemLabel = null;
				}

				group.addControl(ace);
			} else if (ac.getSWTControl() instanceof List) {
				List ll = (List) ac.getSWTControl();
				List l = new List(sectionClient, SWT.BORDER
						| ac.getReadOnlyFlag());
				l.setItems(ll.getItems());
				l.addSelectionListener(new SelectionListener() {
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}

					@Override
					public void widgetSelected(SelectionEvent e) {
						xlayout.setModified(true);
					}
				});
				l.addFocusListener(new FocusListener() {
					@Override
					public void focusGained(FocusEvent e) {
					}

					@Override
					public void focusLost(FocusEvent e) {
						xlayout.fireFocusEvent();
					}
				});

				int selected = ll.getSelectionIndex();
				if (selected > -1)
					l.select(selected);

				String id = "lst_dynamic_" + suffix_lst++; //$NON-NLS-1$
				AdvancedControlElement ace = new AdvancedControlElement(l, id,
						ac.getName(), group.getIdByRefId(ac.getRef()),
						ac.getReadOnlyFlag(), null);
				ace.setRefId(ac.getId());

				ace.setInputType(ac.getInputType());
				ace.setRequired(ac.isRequired());

				ace.setAutoCompletionField(ac.getAutoCompletionField());

				if (elemLabel != null) {
					ace.setLabel(elemLabel);
					elemLabel = null;
				}

				group.addControl(ace);
			} else if (ac.getSWTControl() instanceof Button) {

				Button bO = (Button) ac.getSWTControl();

				Button b = toolkit.createButton(sectionClient, bO.getText(),
						bO.getStyle() | ac.getReadOnlyFlag());

				b.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
						true, false));

				b.setBackground(bO.getBackground());

				b.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						xlayout.setModified(true);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						// TODO Auto-generated method stub
					}
				});

				b.addFocusListener(new FocusListener() {
					@Override
					public void focusGained(FocusEvent e) {
					}

					@Override
					public void focusLost(FocusEvent e) {
						xlayout.fireFocusEvent();
					}
				});

				String id = "btn_dynamic_" + suffix_btn++; //$NON-NLS-1$
				AdvancedControlElement ace = new AdvancedControlElement(b, id,
						ac.getName(), group.getIdByRefId(ac.getRef()),
						ac.getReadOnlyFlag(), null);
				ace.setRefId(ac.getId());

				ace.setInputType(ac.getInputType());
				ace.setRequired(ac.isRequired());

				ace.setAutoCompletionField(ac.getAutoCompletionField());

				if (elemLabel != null) {
					ace.setLabel(elemLabel);
					elemLabel = null;
				}

				group.addControl(ace);

			} else if (ac.getSWTControl() instanceof Group
					|| ac.getSWTControl() instanceof Composite) {
				// Nothing to do
				// g = new Group(sectionClient, SWT.Activate);
				// g.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			} else {
				try {
					throw new Exception(NLS.bind(
							Messages.RepeatControlElement_notImplemented_exception, id));
				} catch (Exception e1) {
					Activator.handleError(e1);
				}
			}
		}

		elemLabel = null;

		final Button remove_btn = toolkit.createButton(sectionClient, Messages.RepeatControlElement_remove_button,
				SWT.PUSH);
		remove_btn
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						if (groups.size() > 1) {
							groups.remove(group);
							group.dispose();
							updateRemoveButtons(false);
							xlayout.setModified(true);
							form.reflow(true);
						}
					}
				});

		Label sep = new Label(sectionClient, SWT.SEPARATOR | SWT.HORIZONTAL);
		sep.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true,
				false));
		group.addControl(new AdvancedControlElement(sep, "lbl_seperator", null, //$NON-NLS-1$
				null, 0, null));

		group.addRemoveButton(remove_btn);
		groups.addLast(group);

		add_btn = toolkit.createButton(sectionClient, Messages.RepeatControlElement_addAdditional_button
				+ element_name_on_label, SWT.PUSH | SWT.CENTER);
		add_btn.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				true, false));
		add_btn.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				addControlsGroup();
				xlayout.setModified(true);
				xlayout.updateDecorationForAgents();

			}
		});

		updateRemoveButtons(true);

		form.reflow(true);
	}

	private void clearControlsGroups() {

		while (!groups.isEmpty() && groups.size() > 1) {
			groups.removeLast().dispose();
		}
		groups.getFirst().clear();
		xlayout.setModified(false);

		updateRemoveButtons(false);

		form.reflow(true);
	}

	public Composite getBody() {

		return sectionClient;
	}

	public void addAdvancedControl(AdvancedControlElement e) {

		if (e.getSWTControl() instanceof Composite && empty) {
			if (group_ != null) {
				groups.addLast(group_);
				++beginCount;
			}
			group_ = new RepeatItemsGroup(this);
			group_.addControl(e);
			empty = false;
		} else if (group_ != null) {
			group_.addControl(e);
		}
	}

	public void endCollecting() {

		final Button remove_btn = toolkit.createButton(sectionClient, Messages.RepeatControlElement_remove_button,
				SWT.PUSH);
		remove_btn
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						if (groups.size() > 1) {
							groups.remove(group_);
							group_.dispose();
							remove_btn.dispose();
							updateRemoveButtons(false);
							xlayout.setModified(true);
							form.reflow(true);
						}
					}
				});

		Label sep = new Label(sectionClient, SWT.SEPARATOR | SWT.HORIZONTAL);
		sep.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true,
				false));
		group_.addControl(new AdvancedControlElement(sep, "lbl_seperator", //$NON-NLS-1$
				null, null, 0, null));

		add_btn = toolkit.createButton(sectionClient, Messages.RepeatControlElement_addAdditional_button
				+ element_name_on_label, SWT.PUSH | SWT.CENTER);
		add_btn.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				true, false));
		add_btn.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				addControlsGroup();
				xlayout.setModified(true);
				xlayout.updateDecorationForAgents();
			}
		});

		group_.addRemoveButton(remove_btn);
		groups.addLast(group_);

		updateRemoveButtons(true);

		++beginCount;
	}

	@Override
	public String getId() {

		return id;
	}

	public LinkedList<RepeatItemsGroup> getAllGroups() {

		return groups;
	}

	@Override
	public void createOMElement(OMFactory omFactory, OMElement parent,
			OMNamespace ns, boolean dummy) {

		if (sec.isExpanded())
			for (RepeatItemsGroup g : groups) {
				if (parent_name != null && !"".equals(parent_name)) { //$NON-NLS-1$
					g.createOMElements(omFactory,
							omFactory.createOMElement(parent_name, ns, parent),
							ns);
				} else {
					g.createOMElements(omFactory, parent, ns);
				}
			}
	}

	@Override
	public boolean setOMElement(OMElement root, boolean tg) {
		boolean found = false;

		clearControlsGroups();
		found = groups.getLast().setOMElement(root, this, tg);

		if ((sec.getExpansionStyle() & ExpandableComposite.TWISTIE) != 0)
			if (found)
				sec.setExpanded(true);
			else
				sec.setExpanded(false);

		xlayout.updateDecorationForAgents();

		return found;
	}

	@Override
	public void dispose() {

		if (/* usedInComplexRepeatable && */this.sec != null) {
			if (!this.sec.isDisposed())
				this.sec.dispose();
		} else {
			for (RepeatItemsGroup g : groups) {
				g.dispose();
			}
		}

		groups.clear();

		if (add_btn != null)
			add_btn.dispose();

	}

	private void updateRemoveButtons(boolean added) {
		if (added) {
			boolean moreThanOne = groups.size() > 1;
			for (RepeatItemsGroup g : groups) {
				g.activateRemoveButton(moreThanOne);
			}
		} else if (groups.size() == 1) {
			groups.getFirst().activateRemoveButton(false);
		}
	}

	@Override
	public boolean validate() {
		if (!sec.isExpanded())
			return true;

		boolean valid = true;
		for (RepeatItemsGroup g : this.getAllGroups()) {
			valid &= g.validate();
		}
		return valid;
	}

	@Override
	public java.util.List<Control> getSWTControls() {
		java.util.List<Control> controls = new ArrayList<Control>();

		for (RepeatItemsGroup g : groups) {
			controls.addAll(g.getSWTControls());
		}
		controls.add(add_btn);

		return controls;
	}

	@Override
	public IRepresentableComposite getUnit() {
		// Just one group
		return groups.getFirst();
	}

	@Override
	public java.util.List<IControl> getControls() {
		throw new NotImplementedException();
	}

	@Override
	public void clear() {
		for (RepeatItemsGroup g : groups) {
			g.clear();
		}
	}

	@Override
	public LinkedList<AdvancedControlElement> getAdvancedControls() {
		return groups.getFirst().getAdvancedControls();
	}

	@Override
	public void setExpanded(boolean expand) {
		if (sec != null)
			sec.setExpanded(expand);
	}

	@Override
	public boolean isExpanded() {
		if (sec != null)
			return sec.isExpanded();
		return false;
	}

}
