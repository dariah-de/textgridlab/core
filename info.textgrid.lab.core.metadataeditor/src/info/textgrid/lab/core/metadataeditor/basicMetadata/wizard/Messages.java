package info.textgrid.lab.core.metadataeditor.basicMetadata.wizard;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.metadataeditor.basicMetadata.wizard.messages"; //$NON-NLS-1$
	public static String BasicMetadataWizard_title;
	public static String BasicMetadataWizardSelected_windowTitle;
	public static String PageOne_createInfo;
	public static String PageOne_title;
	public static String PageOne_type;
	public static String PageTwo_description;
	public static String PageTwo_title;
	public static String WizardUtils_fetchingProjectfile;
	public static String WizardUtils_readConfigurationFailed;
	public static String WizardUtils_readingProjectfile;
	public static String WizardUtils_savingProjectfile;
	public static String WizardUtils_setDatainProjectfileFailed;
	public static String WizardUtils_writeFailed_rights;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
