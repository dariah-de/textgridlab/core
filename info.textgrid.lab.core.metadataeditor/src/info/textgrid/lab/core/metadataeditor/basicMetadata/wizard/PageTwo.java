/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.basicMetadata.wizard;

import info.textgrid.lab.core.metadataeditor.MetaDataSection;
import info.textgrid.lab.core.model.TextGridProject;

import org.apache.axiom.om.OMElement;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class PageTwo extends WizardPage implements IWizardPage,
		IBasicMetadataWizardPage {

	private Composite container = null;
	private Wizard wizard = null;

	private MetaDataSection metaDataSection = null;

	/**
	 * The default constructor.
	 */
	public PageTwo() {
		super(Messages.PageTwo_title);
		setTitle(Messages.PageTwo_title);

		setMessage(Messages.PageTwo_description);
	}

	/**
	 * Initializes the current page with the Template-editor wizard.
	 */
	@Override
	public void init(Wizard wizard) {
		this.wizard = wizard;
	}

	/**
	 * A call-back method to create the page.
	 * 
	 * @param parent
	 *            the parent composite
	 */
	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(parent.getLayoutData());
		container.setLayout(parent.getLayout());

		metaDataSection = new MetaDataSection(container, null, null);

		setControl(container);
		setPageComplete(true);
	}

	/**
	 * Sets the already created basic metadata in the current page.
	 * 
	 * @param elem
	 * @param type
	 */
	void setLastCreatedOMElement(final OMElement elem, final String type,
			final TextGridProject project) {
		metaDataSection.setMetadataXML(elem, type, project);
	}

	/**
	 * Returns the page container.
	 */
	@Override
	public Control getControl() {
		return container;
	}

	/**
	 * To be called on clicking the next button.
	 */
	@Override
	public void finishPage() {

		if (this.wizard instanceof BasicMetadataWizard)
			((BasicMetadataWizard) wizard).setMetadataOM(metaDataSection
					.getMetadataXML());
		else
			((BasicMetadataWizardSelected) wizard)
					.setMetadataOM(metaDataSection.getMetadataXML());
	}

}
