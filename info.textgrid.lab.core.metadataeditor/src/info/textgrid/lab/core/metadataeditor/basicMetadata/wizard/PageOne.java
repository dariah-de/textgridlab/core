/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.basicMetadata.wizard;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;
import info.textgrid.lab.ui.core.utils.UpdatingDeferredListContentProvider;

import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

/**
 * This is the class for the first page of the basic metadata wizard.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class PageOne extends WizardPage implements IWizardPage,
		IBasicMetadataWizardPage, ISelectionChangedListener {

	private Composite container = null;

	private TableViewer projectViewer;
	private ComboViewer typeViewer;
	private Label projectDescLabel;

	private Wizard wizard = null;

	private TextGridProject project = null;
	private TextGridProject currentSelectedProject = null;
	private String objectType = "item"; //$NON-NLS-1$

	private static String[] OBJECT_TYPES = new String[] { "item", "work", //$NON-NLS-1$ //$NON-NLS-2$
			"edition", "collection" }; //$NON-NLS-1$ //$NON-NLS-2$

	protected PageOne() {
		this(null);
	}

	public PageOne(TextGridProject project) {
		super(Messages.PageOne_title);
		setTitle(Messages.PageOne_title);
		this.currentSelectedProject = project;
	}

	/**
	 * Initializes the current page with the wizard.
	 */
	@Override
	public void init(Wizard wizard) {
		this.wizard = wizard;
	}

	/**
	 * Creates the project viewer which views a list of projects.
	 * 
	 * @param control
	 *            the parent composite
	 */
	private void createProjectGroup(Composite control) {

		Group projectGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		projectGroup
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		projectGroup.setLayout(new GridLayout(1, true));
		projectGroup.setText(""); //$NON-NLS-1$

		projectViewer = new TableViewer(projectGroup, SWT.SINGLE | SWT.V_SCROLL
				| SWT.H_SCROLL | SWT.VIRTUAL | SWT.BORDER | SWT.VIRTUAL);
		GridData projectTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
		projectTreeData.heightHint = 200;
		projectViewer.getTable().setLayoutData(projectTreeData);
		projectViewer.setLabelProvider(new TGODefaultLabelProvider(true));

		projectDescLabel = new Label(projectGroup, SWT.WRAP);
		GridData projectDescData = new GridData(SWT.FILL, SWT.FILL, true, false);
		projectDescData.heightHint = 48;
		projectDescLabel.setLayoutData(projectDescData);

		final UpdatingDeferredListContentProvider contentProvider = new UpdatingDeferredListContentProvider();
		projectViewer.setContentProvider(contentProvider);
		projectViewer.setInput(TextGridProjectRoot
				.getInstance(TextGridProjectRoot.LEVELS.PROJECT_LEADER));
		projectViewer.addSelectionChangedListener(this);

		if (currentSelectedProject != null) {
			contentProvider.addDoneListener(new IDoneListener() {
				@Override
				public void loadDone(Viewer viewer) {
					ISelection sel = new StructuredSelection(
							currentSelectedProject);

					projectViewer.setSelection(sel, true);

					if (sel.equals(projectViewer.getSelection()))
						setPageComplete(true);
					else
						setPageComplete(false);

					// contentProvider.removeDoneListener(this);
				}

			});
		} else {
			setPageComplete(false);
		}

	}

	/**
	 * A call-back method to create the page.
	 * 
	 * @param parent
	 *            the parent composite
	 */
	@Override
	public void createControl(Composite parent) {

		// --- List of the available TG-Projects ---
		container = new Composite(parent, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		container.setLayout(new GridLayout(1, false));

		Label intro = new Label(container, SWT.WRAP);
		intro.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		intro.setText(Messages.PageOne_createInfo);

		setControl(container);
		createProjectGroup(container);

		Group typeGroup = new Group(container, SWT.NONE);
		typeGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		typeGroup.setLayout(new GridLayout(1, false));
		typeGroup.setText(Messages.PageOne_type);

		Combo typeControl = new Combo(typeGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		typeControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false));

		typeViewer = new ComboViewer(typeControl);
		typeViewer.add(OBJECT_TYPES);
		typeViewer.addSelectionChangedListener(this);
		typeViewer.setSelection(new StructuredSelection("item")); //$NON-NLS-1$
	}

	/**
	 * Returns the page container.
	 */
	@Override
	public Control getControl() {

		return container;
	}

	/**
	 * To be called on clicking the next button.
	 */
	@Override
	public void finishPage() {

		if (this.wizard instanceof BasicMetadataWizard)
			((BasicMetadataWizard) wizard).setSelectedProjectType(project,
					objectType);
		else
			((BasicMetadataWizardSelected) wizard).setSelectedProjectType(
					project, objectType);
	}

	/**
	 * This method will be called on changing the selection in the
	 * projectViewer.
	 */
	@Override
	public void selectionChanged(SelectionChangedEvent event) {

		if (event.getSelectionProvider() == projectViewer) {
			Object selectedObject = null;
			if (event.getSelection() instanceof IStructuredSelection) {
				selectedObject = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
			}
			if (selectedObject instanceof TextGridProject) {
				project = (TextGridProject) selectedObject;
				projectDescLabel.setText(NLS.bind("{0} ({1})", //$NON-NLS-1$
						project.getDescription(), project.getId()));
				setPageComplete(true);
			} else {
				project = null;
				projectDescLabel.setText(""); //$NON-NLS-1$
			}
		} else if (event.getSelectionProvider() == typeViewer) {
			Object selectedObject = null;
			if (event.getSelection() instanceof IStructuredSelection) {
				selectedObject = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
			}
			if (selectedObject instanceof String) {
				objectType = (String) selectedObject;
			} else {
				objectType = null;
			}
		}
	}

}
