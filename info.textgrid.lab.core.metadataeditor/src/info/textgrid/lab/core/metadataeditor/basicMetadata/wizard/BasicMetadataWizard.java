/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.basicMetadata.wizard;

import info.textgrid.lab.core.model.TextGridProject;

import org.apache.axiom.om.OMElement;
import org.eclipse.jface.dialogs.IPageChangingListener;
import org.eclipse.jface.dialogs.PageChangingEvent;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * This is the class to create the basic metadata wizard.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class BasicMetadataWizard extends Wizard implements
		IPageChangingListener, IBasicMetadataWizard {

	private PageOne pageOne = null;
	private PageTwo pageTwo = null;
	private WizardDialog dialog = null;
	private WizardUtils wizard_util = null;

	private TextGridProject project = null;
	private OMElement metadataElem = null;

	/**
	 * The default constructor which does the necessary initializations.
	 */
	public BasicMetadataWizard() {

		super();
		wizard_util = new WizardUtils();
		setWindowTitle(Messages.BasicMetadataWizard_title);
		setNeedsProgressMonitor(true);
	}

	/**
	 * Displays the wizard.
	 * 
	 * @param activeShell
	 * @param project
	 */
	public void showDialog(Shell activeShell, TextGridProject project) {

		this.project = project;

		WizardDialog dialog = new WizardDialog(activeShell, this);
		dialog.setBlockOnOpen(false);
		dialog.open();

	}

	/**
	 * A call-back method that gets the selected {@link TextGridProject} and
	 * type from the first page and sets it in the {@link WizardUtils} instance.
	 * 
	 * @param project
	 *            the selected project
	 * @param type
	 */
	@Override
	public void setSelectedProjectType(final TextGridProject project,
			final String type) {
		wizard_util.setData(project, type);
		pageTwo.setLastCreatedOMElement(wizard_util.getLastSavedOMElement(),
				type, project);
	}

	public void setMetadataOM(final OMElement metadata) {
		this.metadataElem = metadata;
	}

	/**
	 * Creates the pages and adds them to the wizard.
	 */
	@Override
	public void addPages() {

		super.addPages();

		pageOne = new PageOne(project);
		pageOne.init(this);
		addPage(pageOne);

		pageTwo = new PageTwo();
		pageTwo.init(this);
		addPage(pageTwo);

		IWizardContainer container = getContainer();
		if (container instanceof WizardDialog) {
			dialog = (WizardDialog) container;
			dialog.addPageChangingListener(this);
		}
	}

	/**
	 * This method will be called on clicking the finish button.
	 */
	@Override
	public boolean performFinish() {

		((IBasicMetadataWizardPage) getContainer().getCurrentPage())
				.finishPage();

		if (metadataElem == null)
			pageTwo.finishPage();

		wizard_util.createAndSaveData(metadataElem);

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IPageChangingListener#handlePageChanging(org
	 * .eclipse.jface.dialogs.PageChangingEvent)
	 */
	@Override
	public void handlePageChanging(PageChangingEvent event) {

		if (event.getTargetPage() == getNextPage((IWizardPage) event
				.getCurrentPage())) {
			((IBasicMetadataWizardPage) event.getCurrentPage()).finishPage();
		}
	}

}
