/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.basicMetadata.wizard;

import static org.eclipse.core.runtime.IProgressMonitor.UNKNOWN;
import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.ProjectFileException;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectFile;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.TgProjectFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.collect.Iterators;

/**
 * .
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class WizardUtils {

	private TextGridProject project = null;
	private String type = null;
	private OMElement lastSavedMetadata = null;

	private TextGridProjectFile projectFile = null;

	/**
	 * Sets the selected {@link TextGridProject}.
	 * 
	 * @param project
	 *            the selected project
	 */
	void setData(final TextGridProject project, final String type) {

		try {
			this.project = TextGridProject.getProjectInstance(project.getId());
			this.type = type;
			projectFile = null;
			lastSavedMetadata = null;
			readConfig();
		} catch (RemoteException e) {
			Activator.handleError(e);
		} catch (CrudServiceException e) {
			Activator.handleError(e);
		} catch (ProjectDoesNotExistException e) {
			Activator.handleError(e);
		}
	}

	/**
	 * Reads the configuration from the project file of the selected project.
	 */
	private void readConfig() {

		if (project == null)
			return;

		Job read_job = new Job(Messages.WizardUtils_readingProjectfile) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();

				monitor.beginTask(Messages.WizardUtils_fetchingProjectfile, UNKNOWN);

				projectFile = new TextGridProjectFile(project);

				try {
					TgProjectFile projectFileData = projectFile
							.getProjectFileData(false, true, monitor);

					OMElement[] appDataChildren = TextGridProjectFile.extractAppDataChildren(projectFileData);
					OMElement basicMetaSection = null;
					if (appDataChildren != null && appDataChildren.length > 0) {
						for (OMElement child : appDataChildren) {
							if (child
									.getQName()
									.equals(TextGridProjectFile.basicMetadataSectionQName)) {
								basicMetaSection = child;
								break;
							}
						}
					}

					if (basicMetaSection != null) {

						OMElement typeOMElem = basicMetaSection
								.getFirstChildWithName(new QName(
										TextGridProjectFile.basicMetadataSectionNS,
										type));

						if (typeOMElem != null) {
							lastSavedMetadata = typeOMElem.getFirstElement();
						}
					}

					monitor.done();
					return Status.OK_STATUS;

				} catch (Exception e) {
					Activator
							.handleError(e,
									Messages.WizardUtils_readConfigurationFailed);
				}
				return Status.CANCEL_STATUS;
			}
		};

		read_job.schedule();

		try {
			read_job.join();
		} catch (InterruptedException e) {
		}

	}

	/**
	 * Return the found metadata in the {@link TextGridProjectFile}.
	 * 
	 * @return a list of {@link ConfigCustomElement}
	 */
	OMElement getLastSavedOMElement() {
		return lastSavedMetadata;
	}

	/**
	 * Creates the necessary data and saves it in the project file.
	 * 
	 * @param templateObjects
	 * @param checkBoxes
	 * @param groups
	 */
	void createAndSaveData(final OMElement metadata) {

		System.out.println(metadata);

		try {
			// OMElement xul_om = StringToOM.getOMElement("save me");
			addMetadataPartToProjectFile(metadata);
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e,
					Messages.WizardUtils_setDatainProjectfileFailed);
		}
	}

	private void addMetadataPartToProjectFile(final OMElement metadata) {

		if (projectFile == null)
			return;

		Job saveJob = new Job(Messages.WizardUtils_savingProjectfile) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					if (monitor == null)
						monitor = new NullProgressMonitor();

					monitor.beginTask(Messages.WizardUtils_savingProjectfile, 100);

					monitor.worked(10);

					TgProjectFile projectFileData = projectFile
							.getProjectFileData(true, true, monitor);
					OMFactory omFactory = OMAbstractFactory.getOMFactory();
					OMElement appDataOM = TextGridProjectFile.extractAppDataXML(projectFileData);
					if (appDataOM == null)
						return Status.CANCEL_STATUS;

					monitor.worked(40);

					OMElement basicMetaDataOM = appDataOM
							.getFirstChildWithName(TextGridProjectFile.basicMetadataSectionQName);

					if (basicMetaDataOM == null) {
						basicMetaDataOM = omFactory
								.createOMElement(TextGridProjectFile.basicMetadataSectionQName);
						appDataOM.addChild(basicMetaDataOM);

					}

					OMElement typeElem = basicMetaDataOM
							.getFirstChildWithName(new QName(
									TextGridProjectFile.basicMetadataSectionNS,
									type));

					if (typeElem != null)
						typeElem.detach();

					typeElem = omFactory.createOMElement(new QName(
							TextGridProjectFile.basicMetadataSectionNS, type));

					typeElem.addChild(metadata);

					basicMetaDataOM.addChild(typeElem);

					monitor.worked(10);

					Iterator<OMElement> childElements = appDataOM.getChildElements();
					final List<Element> newAppData = new LinkedList<Element>();
					if (childElements != null) {
						while (childElements.hasNext()) {
							final OMElement next = childElements.next();
							if (!basicMetaDataOM.getLocalName().equals(next.getLocalName()))
								newAppData.add(toDOM(next));
						}
					}
					newAppData.add(toDOM(basicMetaDataOM));
					projectFileData.getAppData().getAny().clear();
					projectFileData.getAppData().getAny().addAll(newAppData);
				
					projectFile.saveProjectFileData(projectFileData, monitor);

					monitor.done();
				} catch (ProjectFileException e) {
					Activator
							.handleError(
									e,
									Messages.WizardUtils_writeFailed_rights);
					return Status.CANCEL_STATUS;
				} catch (CrudServiceException e) {
					Activator
							.handleError(
									e,
									Messages.WizardUtils_writeFailed_rights);
					return Status.CANCEL_STATUS;
				}

				return Status.OK_STATUS;
			}
		};

		saveJob.setUser(true);
		saveJob.schedule();

	}
	
    /**
     * Converts a given OMElement to a DOM Element. (from Axis2's XMLUtils)
     *
     * @param element
     * @return Returns Element.
     */
    private static Element toDOM(OMElement element) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
			element.serialize(baos);
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory.newDocumentBuilder().parse(bais).getDocumentElement();
        } catch (XMLStreamException e) {
        	throw new IllegalArgumentException(e);
        } catch (SAXException e) {
        	throw new IllegalStateException("Internal error in data model conversion.", e);  //$NON-NLS-1$
		} catch (IOException e) {
			// Its an internal byte stream ...
        	throw new IllegalStateException("Internal error in data model conversion.", e);  //$NON-NLS-1$
		} catch (ParserConfigurationException e) {
        	throw new IllegalStateException("Internal error in data model conversion.", e);  //$NON-NLS-1$
		}
    }

	public static void main(String[] args) {
		SchemaFactory f = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		f.setErrorHandler(new ErrorHandler() {

			@Override
			public void warning(SAXParseException exception)
					throws SAXException {
				exception.printStackTrace();
			}

			@Override
			public void fatalError(SAXParseException exception)
					throws SAXException {
				exception.printStackTrace();

			}

			@Override
			public void error(SAXParseException exception) throws SAXException {
				exception.printStackTrace();

			}
		});

		try {
			StreamSource ss = new StreamSource(
					new ByteArrayInputStream(
							"<?xml version=\"1.0\" encoding=\"UTF-8\"?><xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><xs:element name=\"nr\" type=\"xs:integer\"></xs:element></xs:schema>" //$NON-NLS-1$
									.getBytes("UTF-8"))); //$NON-NLS-1$
			StreamSource ss2 = new StreamSource(new ByteArrayInputStream(
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?><nr>124</nr>" //$NON-NLS-1$
							.getBytes("UTF-8"))); //$NON-NLS-1$
			Schema s = f.newSchema(ss);

			Validator v = s.newValidator();
			v.validate(ss2);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
