package info.textgrid.lab.core.metadataeditor.autoCompletion;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.metadataeditor.autoCompletion.messages"; //$NON-NLS-1$
	public static String PNDAutoCompletionClient_fetching;
	public static String TGNAutoCompletionClient_fetching;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
