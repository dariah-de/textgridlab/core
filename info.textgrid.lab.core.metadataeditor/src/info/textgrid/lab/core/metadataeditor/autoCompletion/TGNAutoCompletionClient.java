/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.autoCompletion;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.search.AbstractRequest;
import info.textgrid.lab.search.AutocompletionClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;
import org.apache.commons.httpclient.HttpException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.jaxen.JaxenException;

/**
 * 
 * An auto-completion client for the Getty Thesaurus of Geographic Names (TGN)
 * service.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class TGNAutoCompletionClient extends AutocompletionClient {

	protected final static String URL = "http://textgridlab.org/tgnsearch/tgnquery.xql?ac="; //$NON-NLS-1$
	public final static String NS = "http://textgrid.info/namespaces/vocabularies/tgn"; //$NON-NLS-1$

	private Map<String, String> geoNamesMap = Collections
			.synchronizedMap(new HashMap<String, String>());

	public TGNAutoCompletionClient(String whichField) {
		super(whichField);
	}

	@Override
	protected AbstractRequest createRequest(String whereToComplete, String query) {
		return new Request(whereToComplete, query);
	}

	private void fetchResult(String query, IProgressMonitor monitor)
			throws HttpException, IOException, XMLStreamException,
			FactoryConfigurationError, JaxenException {

		if (monitor == null)
			monitor = new NullProgressMonitor();

		geoNamesMap.clear();

		monitor.worked(30);

		OMElement response = getHttpResponse(URL, query);

		if (response == null)
			return;

		monitor.worked(50);

		List<OMElement> persons = OMUtil.getElementListWithName("term", //$NON-NLS-1$
				response, NS, "tgn"); //$NON-NLS-1$

		String id, name, path;
		String[] completeName;

		// OMElement pname;

		for (OMElement o : persons) {
			id = o.getAttributeValue(new QName("id")); //$NON-NLS-1$
			// pname = o.getFirstChildWithName(new QName(NS, "pname"));

			name = o.getFirstChildWithName(new QName(NS, "name")).getText(); //$NON-NLS-1$
			path = o.getFirstChildWithName(new QName(NS, "path")).getText(); //$NON-NLS-1$
			// ref = pname.getFirstChildWithName(new QName(NS,
			// "ref")).getText();

			completeName = path.split("\\|"); //$NON-NLS-1$

			path = ""; //$NON-NLS-1$
			for (int i = 0; i < completeName.length; i++) {
				path += completeName[i];
				if (i >= 2)
					break;
				path += "|"; //$NON-NLS-1$
			}

			String n = name + " | " + path.replaceFirst("\\s*\\|\\s*World", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if (n.endsWith("|")) //$NON-NLS-1$
				n = n.substring(0, n.length() - 1);

			geoNamesMap.put(n, id);
		}

		monitor.worked(10);
	}

	@Override
	protected int getMinimalLength() {
		return 3;
	}

	public class Request extends AbstractRequest {

		public Request(String field, String query) {
			super(field, query);
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {

			try {
				SubMonitor progress = SubMonitor.convert(monitor,
						IProgressMonitor.UNKNOWN);

				if (progress.isCanceled())
					return Status.CANCEL_STATUS;

				progress.beginTask(Messages.TGNAutoCompletionClient_fetching, 100);

				fetchResult(query, progress);

				ArrayList<String> l = new ArrayList<String>(
						geoNamesMap.keySet());
				Collections.sort(l);
				addAllProposals(l);

				forceOpenPopupIfClosed();

				progress.done();

				return Status.OK_STATUS;
			} catch (Exception e) {
				Activator.handleError(e);
				return Status.CANCEL_STATUS;
			}
		}

	}

	public String getIdByName(String name) {
		return geoNamesMap.get(name);
	}

}