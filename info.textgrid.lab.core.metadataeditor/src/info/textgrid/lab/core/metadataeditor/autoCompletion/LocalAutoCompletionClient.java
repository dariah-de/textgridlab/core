/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor.autoCompletion;

import info.textgrid.lab.search.AbstractRequest;
import info.textgrid.lab.search.AutocompletionClient;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

public class LocalAutoCompletionClient extends AutocompletionClient {

	public LocalAutoCompletionClient(String whichField) {
		super(whichField);
	}

	@Override
	protected AbstractRequest createRequest(String whereToComplete, String query) {
		return new LocalRequest(whereToComplete, query);
	}

	public class LocalRequest extends AbstractRequest {

		public LocalRequest(String field, String query) {
			super(field, query);
		}

		/**
		 * runs the actual query job. Clients shouldn't call this directly but
		 * instead {@link #schedule()} this job.
		 */
		@Override
		protected IStatus run(IProgressMonitor monitor) {
			SubMonitor progress = SubMonitor.convert(monitor,
					IProgressMonitor.UNKNOWN);

			if (progress.isCanceled())
				return Status.CANCEL_STATUS;

			addAllProposals(AutoCompletionUtil.getInstance().getLanguagesList());

			progress.done();
			return Status.OK_STATUS;
		}

	}
}