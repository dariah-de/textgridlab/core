package info.textgrid.lab.core.metadataeditor;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.core.metadataeditor"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	private static Activator defaultInstance = null;

	/**
	 * The constructor
	 */
	public Activator() {
		super();
		Activator.defaultInstance = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static Activator getDefaultInstance() {
		return defaultInstance;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public IStatus handleProblem(int severity, Throwable e) {
		return handleProblem(severity, null, e);
	}

	public IStatus handleProblem(int severity, String message, Throwable cause) {
		if (message == null || message.equals("")) { //$NON-NLS-1$
			if (cause != null)
				message = cause.getLocalizedMessage();
		}
		IStatus status = new Status(severity, PLUGIN_ID, message, cause);
		StatusManager.getManager().handle(
				status,
				status.getSeverity() == IStatus.ERROR ? StatusManager.SHOW
						| StatusManager.LOG : StatusManager.LOG);
		return status;
	}

	public static IStatus handleError(Throwable cause, String message,
			Object... args) {
		return Activator.getDefault().handleProblem(IStatus.ERROR,
				NLS.bind(message, args), cause);
	}

	public static IStatus handleError(Throwable cause) {
		return Activator.getDefault().handleProblem(IStatus.ERROR, cause);
	}

	public static IStatus handleWarning(Throwable cause, String message,
			Object... args) {
		return Activator.getDefault().handleProblem(IStatus.WARNING,
				NLS.bind(message, args), cause);
	}

	public static IStatus handleWarning(Throwable cause) {
		return Activator.getDefault().handleProblem(IStatus.WARNING, cause);
	}
}
