/**
 * UI to edit the metadata for
 * {@link info.textgrid.lab.core.model.TextGridObject}s.
 * 
 * <h4>Sub- and related packages</h4> The (partly modified) XLayout library:
 * <ul>
 * <li>{@link com.logabit.xlayout}</li>
 * <li>{@link com.logabit.xlayout.handlers}</li>
 * <li>{@link com.logabit.xlayout.source}</li>
 * <li>{@link com.logabit.xlayout.utils}</li>
 * </ul>
 * <p>
 * And native metadata editor code:
 * </p>
 * <ul>
 * <li>{@link info.textgrid.lab.core.metadataeditor}</li>
 * <li>{@link info.textgrid.lab.core.metadataeditor.utils}</li>
 * </ul>
 * 
 */
package info.textgrid.lab.core.metadataeditor;

