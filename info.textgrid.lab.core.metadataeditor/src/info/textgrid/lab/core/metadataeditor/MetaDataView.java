/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor;

import static org.eclipse.core.runtime.IProgressMonitor.UNKNOWN;
import info.textgrid.lab.core.metadataeditor.elements.ICheckableElement;
import info.textgrid.lab.core.metadataeditor.elements.IControl;
import info.textgrid.lab.core.metadataeditor.utils.TEIHeaderGenerator;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.ITextGridProjectListener;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectFile;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.TgProjectFile;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

import com.logabit.xlayout.XLayout;
import com.logabit.xlayout.XLayoutFactory;
import com.logabit.xlayout.source.XLayoutSource;

/**
 * This is the class to create the Metadata editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class MetaDataView extends ViewPart implements IMetadataPart,
		ISelectionListener, ITextGridObjectListener, ITextGridProjectListener,
		ISaveablePart, IPartListener {

	public static final String ID = "info.textgrid.lab.core.metadataeditor.view"; //$NON-NLS-1$

	private XLayoutFactory factory = XLayoutFactory.getFactory();
	private XLayout layout = null;
	private XLayoutSource xmlInputSource = null;

	private Button save = null;
	private Button meta2tei = null;
	private Button reload = null;

	private MessageBox mb_notSaved = null;
	private MessageBox mb_modified = null;

	private TextGridObject tgObj = null;

	// private Job rebuildViewJob = null;

	private Composite parent = null;
	private Composite top = null;

	private String project_id = null;
	private InputStream inputFromOM = null;

	private String defaultPartName = ""; //$NON-NLS-1$

	private OMElement _omElem = null;

	public static enum OBJECT_TYPE {
		ITEM {
			@Override
			public String toString() {
				return "item"; //$NON-NLS-1$
			}
		},
		WORK {
			@Override
			public String toString() {
				return "work"; //$NON-NLS-1$
			}
		},
		EDITION {
			@Override
			public String toString() {
				return "edition"; //$NON-NLS-1$
			}
		},
		COLLECTION {
			@Override
			public String toString() {
				return "collection"; //$NON-NLS-1$
			}
		}
	}

	private OBJECT_TYPE objectType = OBJECT_TYPE.ITEM;

	private boolean editable = true;

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(final Composite parent) {

		defaultPartName = getPartName();

		this.parent = parent;

		TextGridObject.addListener(this);
		TextGridProject.addListener(this);

		getSite().getWorkbenchWindow().getSelectionService()
				.addPostSelectionListener(this);

		getViewSite().getPage().addPartListener(this);

		mb_notSaved = new MessageBox(parent.getShell(), SWT.OK | SWT.ICON_ERROR);

		mb_modified = new MessageBox(parent.getShell(), SWT.YES | SWT.NO
				| SWT.ICON_QUESTION);
		mb_modified
				.setMessage(Messages.MetaDataView_saveChangesQuestion);

		rebuild(false);

		setVisible(false);

		// pre-selection handling
		if (!layout.isVisible()) {

			IEditorPart editor = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
			if (editor != null) {
				IEditorInput input = editor.getEditorInput();

				if (input instanceof IFileEditorInput) {
					IFile fileIn = ((IFileEditorInput) input).getFile();

					if (fileIn != null) {
						TextGridObject tgF = (TextGridObject) fileIn
								.getAdapter(TextGridObject.class);

						if (tgF != null) {
							tgObj = tgF;
							setMetadataInView(tgObj, true);
							return;
						}
					}
				}
			}

			IViewPart publishView = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findView("info.textgrid.lab.tgpublish.client.gui.tgpview"); //$NON-NLS-1$
			if (publishView != null) {
				ISelection sel = publishView.getSite().getSelectionProvider()
						.getSelection();
				if (sel != null && sel instanceof IStructuredSelection) {
					Object obj = ((IStructuredSelection) sel).getFirstElement();
					if (obj != null) {
						TextGridObject tgO = AdapterUtils.getAdapter(obj,
								TextGridObject.class);
						if (tgO == null)
							return;

						tgObj = tgO;
						setMetadataInView(tgObj, true);
						return;
					}
				}
			}

			IViewPart naviView = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
			if (naviView != null) {
				ISelection sel = naviView.getSite().getSelectionProvider()
						.getSelection();
				if (sel != null && sel instanceof IStructuredSelection) {
					Object obj = ((IStructuredSelection) sel).getFirstElement();
					if (obj != null) {
						TextGridObject tgO = AdapterUtils.getAdapter(obj,
								TextGridObject.class);
						if (tgO == null)
							return;

						tgObj = tgO;
						setMetadataInView(tgObj, true);
						return;
					}
				}
			}
			// }
		}
		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(parent,
						"info.textgrid.lab.core.metadataeditor.MetadataEditorView"); //$NON-NLS-1$
	}

	/**
	 * Set the visibility of the content of the Metadata-Editor.
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {

		layout.setVisible(visible);
		if (!visible) {
			this.setContentDescription(Messages.MetaDataView_noMetadata);
		}
		layout.setModified(false);
	}

	/**
	 * Recreates the current view.
	 * 
	 * @param readProjectFile
	 *            a boolean value which determines whether the project file has
	 *            to be read or not.
	 */
	private void rebuild(final boolean readProjectFile) {

		if (isBuildingView)
			return;

		try {
			isBuildingView = true;

			// if (layout != null && layout.getForm() != null)
			// layout.dispose();

			layout = factory.createXLayout();
			layout.registerMetadataPart(this);

			if (top != null)
				top.dispose();

			top = new Composite(parent, SWT.None);
			top.setBackground(parent.getBackground());
			top.setLayout(parent.getLayout());
			top.setLayoutData(parent.getLayoutData());
			top.setData(parent.getData());

			xmlInputSource = null;
			String type = objectType.toString();

			InputStream input_xul = getClass().getClassLoader()
					.getResourceAsStream("metadata-" + type + ".xl.xml"); //$NON-NLS-1$ //$NON-NLS-2$

			boolean xul_inputSource_created = false;

			if (readProjectFile) {
				InputStream custom_input = getInputStreamFromProjectFile();

				if (custom_input != null) {
					try {
						String xul_content = convertStreamToString(input_xul), custom_content = convertStreamToString(custom_input);
						xul_content = xul_content.replaceFirst(
								"<tg:custom-placeholder.*?/>", custom_content); //$NON-NLS-1$

						xmlInputSource = new XLayoutSource("MetaData-XUL" //$NON-NLS-1$
								+ type + "_" //$NON-NLS-1$
								+ String.valueOf(System.currentTimeMillis()),
								new ByteArrayInputStream(
										xul_content.getBytes("UTF-8"))); //$NON-NLS-1$
						xul_inputSource_created = true;
					} catch (Exception e2) {
						Activator.handleError(e2);
						return;
					}

				}
			}

			if (!xul_inputSource_created)
				xmlInputSource = new XLayoutSource("MetaData-XUL" + type //$NON-NLS-1$
				/* + String.valueOf(System.currentTimeMillis()) */, input_xul);

			layout.draw(top, xmlInputSource, type, true);

			editable = true;

			save = (Button) layout.getControl("btnSave"); //$NON-NLS-1$
			if (save != null) {
				save.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						doSave(null);
						super.widgetSelected(e);
					}
				});
			}

			reload = (Button) layout.getControl("btnReload"); //$NON-NLS-1$
			if (reload != null) {
				reload.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						if (tgObj == null)
							return;

						Job reloadJob = new Job(Messages.MetaDataView_reloading) {
							@Override
							protected IStatus run(IProgressMonitor monitor) {
								if (monitor == null)
									monitor = new NullProgressMonitor();
								try {
									tgObj.reloadMetadata(true);
								} catch (CrudServiceException e) {
									Activator
											.handleError(
													e,
													Messages.MetaDataView_reloadFailed,
													tgObj);
									return Status.CANCEL_STATUS;
								}
								return Status.OK_STATUS;
							}
						};

						reloadJob.schedule();

						try {
							reloadJob.join();
						} catch (InterruptedException e1) {
							Activator.handleError(e1);
						}

						if (reloadJob.getResult() != Status.OK_STATUS)
							return;

						setMetadataInView(tgObj, false);
						System.out.println(Messages.MetaDataView_reloaded);

						// &&& +
						// try {
						// XMLPrettyPrinter.prettify(tgObj.getMetadataXML(),
						// System.out);
						//
						// } catch (CrudServiceException e1) {
						// // TODO Auto-generated catch block
						// e1.printStackTrace();
						// } catch (Exception e2) {
						// // TODO Auto-generated catch block
						// e2.printStackTrace();
						// }
						// &&&

						super.widgetSelected(e);
					}
				});
			}

			meta2tei = (Button) layout.getControl("btnMeta2tei"); //$NON-NLS-1$
			if (meta2tei != null && tgObj != null) {
				meta2tei.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent event) {
						System.out.println(Messages.MetaDataView_copyTEI);
						TEIHeaderGenerator generator = new TEIHeaderGenerator(
								tgObj, true);
						generator.setUser(true);
						generator.schedule();
					}
				});
			}
			meta2tei.setToolTipText(Messages.MetaDataView_copyTEI2Clipboard);
			parent.layout(true, true);
			layout.setModified(false);

		} finally {
			isBuildingView = false;
		}
	}

	public static String convertStreamToString(InputStream is) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line + "\n"); //$NON-NLS-1$
		}
		is.close();
		return sb.toString();
	}

	private boolean tgp_selected = false;

	/**
	 * This is a call-back method that will be called when a resource is
	 * selected.
	 */
	@Override
	public void selectionChanged(IWorkbenchPart sourcepart, ISelection selection) {

		boolean isViewVisible = true;
		selectedTGOWhileNotVisible = null;

		try {
			if (!PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().isPartVisible(this)) {
				isViewVisible = false;
			}
		} catch (Exception e) {
		}

		boolean found = false;

		if (sourcepart != null && sourcepart instanceof IEditorPart) {
			IEditorPart ePart = (IEditorPart) sourcepart;
			IEditorInput input = ePart.getEditorInput();

			if (input instanceof IFileEditorInput) {
				IFile fileIn = ((IFileEditorInput) input).getFile();
				found = true;

				if (fileIn != null) {
					TextGridObject tgF = (TextGridObject) fileIn
							.getAdapter(TextGridObject.class);

					if (tgF != null) {
						if (isViewVisible) {
							// if the same TGObject
							if (tgF == tgObj && tgObj != null && !tgp_selected)
								// && (layout != null && layout.isVisible())) //
								// this line caused double-view in MDE
								return;

							setMetadataInView(tgF, tgp_selected);
							tgp_selected = false;
						} else {
							selectedTGOWhileNotVisible = tgF;
						}
					} else {
						askSaving(tgObj);
						setVisible(false);
						tgObj = tgF;
					}
				}
			}
		}

		if (!found && selection instanceof IStructuredSelection) {
			Object obj = ((IStructuredSelection) selection).getFirstElement();

			TextGridObject tgO = AdapterUtils.getAdapter(obj,
					TextGridObject.class);
			if (tgO != null) {

				if (isViewVisible) {
					// if the same TGObject
					if (tgO == tgObj && tgObj != null && !tgp_selected)
						// && (layout != null && layout.isVisible()))
						return;

					setMetadataInView(tgO, tgp_selected);
					tgp_selected = false;
					return;
				} else {
					selectedTGOWhileNotVisible = tgO;
				}
			} else if (sourcepart instanceof CommonNavigator
					&& obj instanceof TextGridProject) {
				tgp_selected = true;
				askSaving(tgObj);
				// setVisible(false);
				return;
			}
		}
	}

	/**
	 * Get the object's type
	 * 
	 * @param obj
	 * @return boolean - type changed or not?
	 * @throws CoreException
	 */
	private boolean checkObjectType(TextGridObject obj) throws CoreException {
		if (obj != null) {
			try {
				ObjectType type = obj.getMetadataForReading();
				OBJECT_TYPE previous_type = objectType;

				if (type.getItem() != null) {
					objectType = OBJECT_TYPE.ITEM;
				} else if (type.getWork() != null) {
					objectType = OBJECT_TYPE.WORK;
				} else if (type.getEdition() != null) {
					objectType = OBJECT_TYPE.EDITION;
				} else if (type.getCollection() != null) {
					objectType = OBJECT_TYPE.COLLECTION;
				} else {
					String contentTypeId = obj.getContentTypeID();
					if (contentTypeId != null && !"".equals(contentTypeId)) { //$NON-NLS-1$
						if (contentTypeId.toLowerCase().contains("tg.work+")) { //$NON-NLS-1$
							objectType = OBJECT_TYPE.WORK;
						} else if (contentTypeId.toLowerCase().contains(
								"tg.edition+")) { //$NON-NLS-1$
							objectType = OBJECT_TYPE.EDITION;
						} else if (contentTypeId.toLowerCase().contains(
								"tg.collection+")) { //$NON-NLS-1$
							objectType = OBJECT_TYPE.COLLECTION;
						} else {
							objectType = OBJECT_TYPE.ITEM;
						}
					}
				}

				if (!objectType.equals(previous_type))
					return true;

			} catch (CrudServiceException e) {
				Activator.handleError(e, Messages.MetaDataView_fetchMetadataFailed);
			}
		}

		return false;
	}

	/**
	 * Sets the metadata of the referred {@link TextGridObject} in the view.
	 * 
	 * @param tgO
	 *            the referred TextGridObject
	 */
	public void setMetadataInView(final TextGridObject tgO,
			final boolean rebuild) {
		try {
			if (this.tgObj != tgO)
				askSaving(tgObj);

			this.tgObj = tgO;

			boolean type_changed = checkObjectType(this.tgObj);

			String tgO_project_id = null;
			tgO_project_id = tgO.getProject();

			if ((tgO_project_id != null && project_id != null && !tgO_project_id
					.equals(project_id))
			/* || checkMetadataChanged(tgO) */|| rebuild || type_changed) {
				rebuild(true);
			}

			project_id = tgO_project_id;

			if (layout == null || layout.getForm() == null
					|| layout.getForm().isDisposed())
				return;

			if (!layout.isVisible()) {
				setVisible(true);
				rebuild(true);
			}

			Label l = layout.getInfoLabel();
			if (l != null) {
				if (tgO.isNew()) {
					l.setForeground(getSite().getShell().getDisplay()
							.getSystemColor(SWT.COLOR_BLUE));
					l.setText(Messages.MetaDataView_autoSave_info);
				} else {
					l.setText(""); //$NON-NLS-1$
				}
			}

			Job getJob = new Job(Messages.MetaDataView_fetchingMetadata) {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					if (monitor == null)
						monitor = new NullProgressMonitor();
					try {
						tgObj.reloadMetadata(true);
						_omElem = tgObj.getMetadataXML();

						new UIJob("") { //$NON-NLS-1$

							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								try {
									if (layout == null || layout.isDisposed()
											|| layout.getForm() == null)
										return Status.CANCEL_STATUS;

									setContentDescription(" " + tgO.getTitle()); //$NON-NLS-1$
									layout.setOMElement(_omElem, objectType
											.toString(), tgObj.getURI()
											.toString());

									if (checkFieldTriggered) {
										if (xPathQuery != null)
											doCheckField(xPathQuery);
										checkFieldTriggered = false;
										xPathQuery = null;
									}
									checkAllFields();

									if (tgO.hasPermissions(ITextGridPermission.UPDATE)) {
										setEditable(true);
										if (tgO.isNew() && save != null) {
											save.setEnabled(false);
										} else if (save != null
												&& !save.isEnabled()) {
											save.setEnabled(true);
										}
									} else {
										setEditable(false);
									}

									layout.setModified(false);
								} catch (CoreException e) {
									setVisible(false);
									Activator.handleError(e);
									return Status.CANCEL_STATUS;
								}

								return Status.OK_STATUS;
							}
						}.schedule();

					} catch (CoreException e) {
						new UIJob("") { //$NON-NLS-1$
							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								setVisible(false);
								return Status.OK_STATUS;
							}
						}.schedule();
						Activator.handleError(e);
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};

			getJob.schedule();

		} catch (CoreException e) {
			setVisible(false);
			Activator.handleError(e);
		}
	}

	/**
	 * Get the InputStream of the project file.
	 * 
	 * @return InputStream
	 */
	private InputStream getInputStreamFromProjectFile() {

		this.inputFromOM = null;

		if (tgObj == null)
			return null;

		TextGridProject project = null;
		try {
			project = tgObj.getProjectInstance();// TextGridProject.getProjectInstance(projectId);
		} catch (CrudServiceException e1) {
			Activator.handleError(e1);
			return null;
		} catch (CoreException e1) {
			Activator.handleError(e1);
			return null;
		}

		if (project == null)
			return null;

		final TextGridProjectFile projectFile = new TextGridProjectFile(project);

		IRunnableWithProgress fetchJob = new IRunnableWithProgress() {

			@Override
			public void run(IProgressMonitor monitor) {
				try {
					if (monitor == null)
						monitor = new NullProgressMonitor();

					monitor.beginTask(Messages.MetaDataView_fetchingProjectfile, UNKNOWN);

					TgProjectFile projectFileData = projectFile
							.getProjectFileData(false, true, monitor);

					OMElement[] appDataChildren = TextGridProjectFile.extractAppDataChildren(projectFileData);

					OMElement mySection = null;
					if (appDataChildren != null && appDataChildren.length > 0) {
						for (OMElement child : appDataChildren) {
							if (child.getQName().equals(
									TextGridProjectFile.metadataSectionQName)) {
								mySection = child;
								break;
							}
						}
					}

					if (mySection != null) {
						OMElement xul_elem = mySection
								.getFirstChildWithName(new QName("tr")); //$NON-NLS-1$

						if (xul_elem != null) {
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							Writer w = new OutputStreamWriter(baos, "UTF-8"); //$NON-NLS-1$
							xul_elem.serialize(w);
							inputFromOM = new ByteArrayInputStream(
									baos.toByteArray());
							w.flush();
							w.close();
							baos.close();
						}
					}

					monitor.done();
				} catch (Exception e) {
					Activator.handleError(e);
				}
			}
		};

		try {
			getSite().getWorkbenchWindow().getWorkbench().getProgressService()
					.busyCursorWhile(fetchJob);
		} catch (InvocationTargetException e) {
			Activator.handleError(e);
		} catch (InterruptedException e) {
			Activator.handleError(e);
		}

		return inputFromOM;
	}

	private TextGridObject selectedTGOWhileNotVisible = null;

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {

		if (parent != null && !parent.isDisposed())
			parent.setFocus();

		// if the same TGObject
		if (selectedTGOWhileNotVisible == null
				|| (selectedTGOWhileNotVisible == tgObj && tgObj != null && !tgp_selected))
			// && (layout != null && layout.isVisible()))
			return;

		try {
			setMetadataInView(selectedTGOWhileNotVisible, tgp_selected);
		} catch (Exception e) {
		}

		tgp_selected = false;
	}

	/**
	 * Asks to save the metadata of the old {@link TextGridObject} before
	 * switching to the newly selected TextGridObject.
	 * 
	 * @param oldTgo
	 *            the old TextGridObject
	 */
	private void askSaving(final TextGridObject oldTgo) {

		if (oldTgo == null)
			return;

		if (oldTgo.isNew()) {
			update();
			return;
		}

		try {
			if (isDirty()) {
				String title = Messages.MetaDataView_lastObject;

				title = oldTgo.getTitle();

				mb_modified
						.setMessage(NLS.bind(Messages.MetaDataView_saveChanges_question, title));
				if (mb_modified.open() == SWT.YES) {
					// if (checkDateError() && checkTitleNotEmptyError() &&
					// checkIdnoTypeNotEmptyError())
					if (!makeChangesPersistent(oldTgo, true))
						System.out.println(Messages.MetaDataView_changesDiscarded);
				} else {
					new Job("") { //$NON-NLS-1$
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							if (monitor == null)
								monitor = new NullProgressMonitor();
							try {
								oldTgo.reloadMetadata(true);
								new UIJob(Messages.MetaDataView_reloading) {
									@Override
									public IStatus runInUIThread(
											IProgressMonitor monitor) {
										layout.setModified(false);
										System.out
												.println(Messages.MetaDataView_changesDiscarded);
										return Status.OK_STATUS;
									}
								}.schedule();
							} catch (CoreException e) {
								Activator.handleError(e,
										Messages.MetaDataView_reloadFailed2);
								return Status.CANCEL_STATUS;
							}
							return Status.OK_STATUS;
						}
					}.schedule();

				}
			}
		} catch (CoreException e) {
			Activator.handleError(e);
		}
	}

	/**
	 * Make the changes of the {@link TextGridObject} persistent.
	 * 
	 * @param tgO
	 *            the TextGridProject to save
	 */
	private boolean makeChangesPersistent(final TextGridObject tgO,
			final boolean loadAfterSaving) {
		OMElement omElem = layout.getOMElement();
		tgO.setMetadataXML(omElem);

		if (tgO.isNew())
			return false;

		// if (!checkDateError() || !checkTitleNotEmptyError()
		// || !checkIdnoTypeNotEmptyError())
		if (!checkAllFields())
			return false;

		// System.out.println("OMElement to Save:" + omElem.toString());
		Job saveJob = new Job(Messages.MetaDataView_makePersistent) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					tgO.reloadMetadata(false);
					tgO.makeMetadataPersistent(monitor);
					System.out.println(Messages.MetaDataView_changesSaved);
					if (!loadAfterSaving)
						new UIJob("") { //$NON-NLS-1$
							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								// To solve the problem of multiple clicks on
								// the save-button
								setMetadataInView(tgO, false);

								layout.setModified(false);

								return Status.OK_STATUS;
							}
						}.schedule();
				} catch (CoreException e) {
					Activator.handleError(e, Messages.MetaDataView_saveFailed);
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		};

		saveJob.schedule();

		return true;
	}

	/**
	 * Sets the current view editable or read-only.
	 * 
	 * @param editable
	 *            true or false
	 */
	private void setEditable(boolean editable) {
		List<Control> controls = layout.getAllControls();
		for (Control c : controls) {
			if (c != null
					&& !c.isDisposed()
					&& (!(c instanceof Label
							|| c instanceof Section
							|| (c instanceof Composite && !(c instanceof Combo)) || c instanceof Link))) {
				c.setEnabled(editable);
			}
		}

		// these 2 buttons should be always enabled
		if (reload != null)
			reload.setEnabled(true);
		if (meta2tei != null)
			meta2tei.setEnabled(true);

		this.editable = editable;
	}

	private boolean isBuildingView = false;

	/**
	 * Validate all required fields.
	 * 
	 */
	private boolean checkAllFields() {

		if (isBuildingView)
			return true;

		boolean valid = true;

		if (layout != null) {
			for (IControl c : layout.getAllControlElements()) {
				valid &= c.validate();
			}
		}

		return valid;
	}

	/**
	 * Updates the metadata editor on changes.
	 */
	@Override
	public void update() {
		checkAllFields();

		OMElement elem = layout.getOMElement();
		if (tgObj != null)
			tgObj.setMetadataXML(elem);
	}

	@Override
	public void dispose() {

		TextGridObject.removeListener(this);
		getSite().getWorkbenchWindow().getSelectionService()
				.removePostSelectionListener(this);
		if (layout != null)
			layout.dispose();

		super.dispose();
	}

	@Override
	public void textGridProjectChanged(
			info.textgrid.lab.core.model.ITextGridProjectListener.Event event,
			TextGridProject project) {
		try {
			if (event == info.textgrid.lab.core.model.ITextGridProjectListener.Event.CONTENT_CHANGED
					&& tgObj != null
					&& project != null
					&& tgObj.getMetadataForReading().getItem() != null
					&& project.equals(tgObj.getProjectInstance())) {

				Job reloadJob = new Job(Messages.MetaDataView_reloading) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						if (monitor == null)
							monitor = new NullProgressMonitor();
						try {
							tgObj.reloadMetadata(true);
						} catch (CrudServiceException e) {
							Activator
									.handleError(
											e,
											Messages.MetaDataView_reloadFailed,
											tgObj);
							return Status.CANCEL_STATUS;
						}
						return Status.OK_STATUS;
					}
				};

				reloadJob.schedule();

				try {
					reloadJob.join();
				} catch (InterruptedException e1) {
					Activator.handleWarning(e1);
				}

				if (reloadJob.getResult() != Status.OK_STATUS)
					return;

				setMetadataInView(tgObj, false);

			}
		} catch (CrudServiceException e) {
			Activator.handleWarning(e);
		} catch (CoreException e) {
			Activator.handleWarning(e);
		}
	}

	/**
	 * This method will be called on changes in the {@link TextGridObject}.
	 */
	@Override
	public void textGridObjectChanged(
			info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event event,
			TextGridObject object) {
		if (object != null && object == tgObj) {
			if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.METADATA_INCOMPLETE) {
				checkAllFields();
			} else if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.CREATED) {
				Job reloadJob = new Job(Messages.MetaDataView_reloading) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						if (monitor == null)
							monitor = new NullProgressMonitor();
						try {
							tgObj.reloadMetadata(true);
						} catch (CrudServiceException e) {
							Activator.handleError(e);
							return Status.CANCEL_STATUS;
						}
						return Status.OK_STATUS;
					}
				};

				reloadJob.schedule();

				try {
					reloadJob.join();
				} catch (InterruptedException e) {
					Activator.handleError(e);
				}

				if (reloadJob.getResult() != Status.OK_STATUS)
					return;

				setMetadataInView(tgObj, false);

			} else if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.DELETED) {
				layout.setModified(false);
				setVisible(false);
			} else if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.METADATA_SAVED) {
				layout.setModified(false);
			} else if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.METADATA_CHANGED_FROM_OUTSIDE) {
				try {
					if (layout.getForm() == null)
						return;

					boolean type_changed = checkObjectType(this.tgObj);

					if (type_changed) {
						rebuild(true);
					}

					if (!layout.isVisible()) {
						setVisible(true);
						rebuild(true);
					}

					boolean mod = layout.isModified();
					layout.setOMElement(tgObj.getMetadataXML(),
							objectType.toString(), tgObj.getURI().toString());
					layout.setModified(mod);
				} catch (CrudServiceException e) {
					Activator.handleError(e);
				} catch (CoreException e) {
					Activator.handleError(e);
				}

			} else if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.METADATA_CHANGED) {

			}
		}

	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		if (isDirty()) {

			if (!checkAllFields()) {
				mb_notSaved
						.setMessage(Messages.MetaDataView_saveFailedTryAgain);
				mb_notSaved.open();

				return;
			}

			update();

			makeChangesPersistent(tgObj, false);
		}
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public boolean isDirty() {
		boolean dirty = layout.isModified();
		if (dirty)
			setPartName(" " + defaultPartName); //$NON-NLS-1$
		else
			setPartName(defaultPartName);

		checkAllFields();

		return dirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public boolean isSaveOnCloseNeeded() {
		if (tgObj != null && tgObj.isNew())
			return false;

		return true;
	}

	@Override
	public boolean validate() {
		return isDirty();
	}

	@Override
	public void partActivated(IWorkbenchPart part) {
		// TODO Auto-generated method stub

	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
		// TODO Auto-generated method stub

	}

	@Override
	public void partClosed(IWorkbenchPart part) {
		if (tgObj != null && layout != null && !layout.isDisposed()
				&& layout.isModified())
			new Job("") { //$NON-NLS-1$
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					if (monitor == null)
						monitor = new NullProgressMonitor();
					try {
						tgObj.reloadMetadata(true);
					} catch (CoreException e) {
						Activator
								.handleWarning(e,
										Messages.MetaDataView_reloadAfterCloseFailed);
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			}.schedule();
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {
		// TODO Auto-generated method stub

	}

	@Override
	public void partOpened(IWorkbenchPart part) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isEditable() {
		return editable;
	}

	/**
	 * Returns the at the moment loaded TextGrid object.
	 * 
	 * @return TextGridObject
	 */
	public TextGridObject getTextGridObject() {
		return this.tgObj;
	}

	private boolean checkFieldTriggered = false;
	private String xPathQuery = null;

	/**
	 * Check the content of a field in the metadata editor using xpath-Query to
	 * select this field
	 * 
	 * @param xpathQuery
	 *            the xpathQuery which contains the path of the field
	 * 
	 */
	public void triggerCheckField(final String xpathQuery) {
		doCheckField(xpathQuery);
		checkFieldTriggered = true;
		this.xPathQuery = xpathQuery;
	}

	private boolean doCheckField(final String xpathQuery) {

		update();

		String[] tokens = xpathQuery.trim().split("/"); //$NON-NLS-1$

		if (tokens == null || tokens.length < 4)
			throw new RuntimeException(NLS.bind(Messages.MetaDataView_queryIncomplete, xpathQuery));

		String fieldPath = ""; //$NON-NLS-1$

		for (int i = 1; i < tokens.length; i++) {
			fieldPath += "/" + tokens[i].replaceFirst(".+:", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		ICheckableElement elem = layout.getPathControlsMap().get(fieldPath);

		if (elem != null) {
			elem.setRequired(true);
			return elem.validate();
		}

		return true;
	}

	public void reloadProjectFile(final TextGridProject project)
			throws CoreException {
		if (tgObj != null && project.equals(tgObj.getProjectInstance())) {
			askSaving(tgObj);
			setMetadataInView(tgObj, true);
		}
	}
}