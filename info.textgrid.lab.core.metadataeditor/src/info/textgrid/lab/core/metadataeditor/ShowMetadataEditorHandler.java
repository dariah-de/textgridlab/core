/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

/**
 * The handler-class for the Metadata editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class ShowMetadataEditorHandler extends AbstractHandler {

	/**
	 * Tries to open the Metadata-View.
	 */
	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();
			if (activePage == null)
				MessageDialog
						.openWarning(PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),
								Messages.ShowMetadataEditorHandler_noPerspective,
								Messages.ShowMetadataEditorHandler_noPerspectiveDesc);
			else
				activePage.showView(
						"info.textgrid.lab.core.metadataeditor.view", null, //$NON-NLS-1$
						IWorkbenchPage.VIEW_ACTIVATE);
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					Messages.ShowMetadataEditorHandler_openFailed, e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}

}