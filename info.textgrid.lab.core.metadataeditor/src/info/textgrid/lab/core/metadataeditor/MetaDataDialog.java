/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor;

import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class MetaDataDialog extends TrayDialog {

	private MetaDataSection metaDataSection = null;
	private TextGridObject tgObj = null;
	private boolean isEditable = true;
	private Button save = null;

	public static void openMetaDataDialog(TextGridObject tgO) {
		MetaDataDialog d = new MetaDataDialog(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell());
		d.addTGObject(tgO);
		d.open();
	}

	protected MetaDataDialog(Shell parentShell) {
		super(parentShell);
		// createLayout();
	}

	public void addTGObject(TextGridObject tg) {
		this.tgObj = tg;
	}

	@Override
	protected Button createButton(Composite parent, int id, String label,
			boolean defaultButton) {

		if (id == 0) {
			save = super.createButton(parent, id, Messages.MetaDataDialog_save, defaultButton);
			save.setText(Messages.MetaDataDialog_save);
			save.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					metaDataSection.updateTextGridObject();
					Job makePersistent = new Job(
							Messages.MetaDataDialog_makePersistent) {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							try {
								tgObj.makeMetadataPersistent(monitor);
							} catch (CoreException e1) {
								Activator.handleError(e1);
								return Status.CANCEL_STATUS;
							}
							return Status.OK_STATUS;
						}

					};

					makePersistent.schedule();
					try {
						makePersistent.join();
					} catch (InterruptedException e1) {
						Activator.handleWarning(e1);
					}

					okPressed2();

				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// TODO Auto-generated method stub

				}
			});

			save.setEnabled(isEditable);

			return save;
		} else if (id == 1) {
			return super.createButton(parent, id, Messages.MetaDataDialog_cancel, defaultButton);
		}

		return null;
	}

	@Override
	protected void okPressed() {
		// TODO Auto-generated method stub
		// super.okPressed();
	}

	private void okPressed2() {
		super.okPressed();
	}

	@Override
	protected void cancelPressed() {
		super.cancelPressed();
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite area = parent; // (Composite) super.createDialogArea(parent);

		ScrolledPageArea scrolledPageArea = new ScrolledPageArea(area);

		Composite composite = scrolledPageArea.getPageAreaComposite();
		ScrolledForm scrolledForm = scrolledPageArea.getPageAreaScrolledForm();

		GridDataFactory.fillDefaults().grab(true, true).hint(600, 400)
				.applyTo(composite);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(scrolledForm);

		Group metadataGroup = new Group(composite, SWT.SHADOW_ETCHED_IN);
		// GridDataFactory.fillDefaults().grab(true, true).hint(600,
		// 500).applyTo(
		// metadataGroup);
		metadataGroup.setLayout(new GridLayout(1, false));
		// metadataGroup.setText("Metadata");

		metaDataSection = new MetaDataSection(metadataGroup, scrolledForm, null);

		if (tgObj != null) {
			metaDataSection.setNewTGObject(tgObj, true);

			metaDataSection.checkAllFields();

			try {
				if (!tgObj.hasPermissions(ITextGridPermission.UPDATE)) {
					metaDataSection.setEditable(false);
					if (save != null)
						save.setEnabled(false);
					isEditable = false;
				}
			} catch (CoreException e) {
				Activator.handleError(e);
			}
		}

		return composite;
	}

}
