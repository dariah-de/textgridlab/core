/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.core.metadataeditor;

import static org.eclipse.core.runtime.IProgressMonitor.UNKNOWN;
import info.textgrid.lab.core.metadataeditor.MetaDataView.OBJECT_TYPE;
import info.textgrid.lab.core.metadataeditor.elements.IControl;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectFile;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.TgProjectFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import com.logabit.xlayout.XLayout;
import com.logabit.xlayout.XLayoutFactory;
import com.logabit.xlayout.source.XLayoutSource;

/**
 * A class to create a metadata section.
 * 
 * TODO might we extract a common superclass with {@link MetaDataView}?
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class MetaDataSection implements IMetadataPart {

	private XLayoutFactory factory = XLayoutFactory.getFactory();
	private XLayout layout = null;
	private XLayoutSource xmlInputSource = null;

	private TextGridObject tgObj = null;

	private InputStream inputFromOM = null;
	private Composite parent = null;
	private Composite top = null;

	private ScrolledForm metadataForm = null;

	private boolean isProjectFileLoaded = false;

	private OMElement _omElem = null;

	private OBJECT_TYPE objectType = OBJECT_TYPE.ITEM;

	private boolean editable = true;

	/**
	 * The constructor that creates the Metadata section. <br>
	 * <b>The parent and scrolledForm parameters should be created using a
	 * {@link ScrolledPageArea} object.</b>
	 * 
	 * @param parent
	 *            the parent composite
	 * @param scrolledForm
	 *            the scrolled form
	 * @param tgo
	 *            an optional {@link TextGridObject} to set its meta data in the
	 *            section
	 */
	public MetaDataSection(Composite parent, ScrolledForm scrolledForm,
			TextGridObject tgo) {

		this.parent = parent;
		this.metadataForm = scrolledForm;
		rebuild(false, null);
		setNewTGObject(tgo, tgo != null);
	}

	/**
	 * Changes the visibility of the Metadata-section.
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		if (layout != null)
			layout.setVisible(visible);
	}

	/**
	 * Recreates the current Section.
	 * 
	 * @param readProjectFile
	 *            a boolean value which determines whether the project file have
	 *            to be read or not.
	 * 
	 * @param project
	 *            optional TGProject
	 */
	private void rebuild(final boolean readProjectFile, TextGridProject project) {

		// if(layout != null && layout.getForm() != null)
		// layout.dispose();

		layout = factory.createXLayout();
		layout.registerMetadataPart(this);

		if (top != null)
			top.dispose();

		top = new Composite(this.parent, SWT.None);
		top.setBackground(parent.getBackground());
		top.setLayout(parent.getLayout());
		top.setLayoutData(parent.getLayoutData());
		top.setData(parent.getData());

		xmlInputSource = null;
		String type = objectType.toString();

		InputStream input_xul = getClass().getClassLoader()
				.getResourceAsStream("metadata-" + type + ".xl.xml"); //$NON-NLS-1$ //$NON-NLS-2$

		boolean xul_inputSource_created = false;

		if (readProjectFile) {
			InputStream custom_input = getInputStreamFromProjectFile(project);

			if (custom_input != null) {
				try {
					String xul_content = MetaDataView
							.convertStreamToString(input_xul), custom_content = MetaDataView
							.convertStreamToString(custom_input);
					xul_content = xul_content.replaceFirst(
							"<tg:custom-placeholder.*?/>", custom_content); //$NON-NLS-1$

					xmlInputSource = new XLayoutSource("MetaData-XUL" + type //$NON-NLS-1$
							+ "_" + String.valueOf(System.currentTimeMillis()), //$NON-NLS-1$
							new ByteArrayInputStream(
									xul_content.getBytes("UTF-8"))); //$NON-NLS-1$
					xul_inputSource_created = true;
				} catch (Exception e2) {
					Activator.handleError(e2);
					return;
				}

			}
		}

		if (!xul_inputSource_created)
			xmlInputSource = new XLayoutSource("MetaData-XUL" + type //$NON-NLS-1$
			/* + String.valueOf(System.currentTimeMillis()) */, input_xul);

		layout.draw(top, xmlInputSource, type, false);

		editable = true;

		if (metadataForm != null)
			metadataForm.reflow(true);

		parent.layout(true, true);
	}

	/**
	 * Set the metadata of a {@link TextGridObject} in the Metadata section
	 * 
	 * @param tgo
	 *            an optional {@link TextGridObject} to set its meta data in the
	 *            section
	 * @param reloadProjectFile
	 *            should the project-file be reloaded ?
	 */
	public void setNewTGObject(final TextGridObject tgo,
			boolean reloadProjectFile) {

		this.tgObj = tgo;

		// +++ setEnabled(tgo != null);
		// layout.getForm().setEnabled();

		if (tgObj != null) {

			objectType = null;

			checkObjectType(tgObj);

			if (objectType == null) {
				String format = null;

				try {
					format = tgObj.getMetadata().getGeneric().getProvided()
							.getFormat();
				} catch (CrudServiceException e1) {
					Activator.handleError(e1, "Format not found!"); //$NON-NLS-1$
					return;
				}

				if (format != null) {
					if (format.contains("tg.work+")) //$NON-NLS-1$
						objectType = OBJECT_TYPE.WORK;
					else if (format.contains("tg.edition+")) //$NON-NLS-1$
						objectType = OBJECT_TYPE.EDITION;
					else if (format.contains("tg.collection+")) //$NON-NLS-1$
						objectType = OBJECT_TYPE.COLLECTION;
					else
						objectType = OBJECT_TYPE.ITEM;
				}
			}

			if (reloadProjectFile)
				rebuild(true, null);

			Job getJob = new Job(Messages.MetaDataSection_getMetadata) {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					if (monitor == null)
						monitor = new NullProgressMonitor();
					try {
						_omElem = tgo.getMetadataXML();
					} catch (CoreException e) {
						Activator.handleError(e);
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};

			getJob.schedule();

			try {
				getJob.join();
			} catch (InterruptedException e) {
			}

			if (getJob.getResult() != Status.OK_STATUS)
				return;

			layout.setOMElement(_omElem, objectType.toString(), tgo.getURI()
					.toString());
		}
	}

	private boolean validateActive = true;

	public void setMetadataXML(final OMElement elem, final String type,
			final TextGridProject project) {

		validateActive = false;
		internalRebuild(type, project);

		Text format = (Text) layout.getControl("txtFormat"); //$NON-NLS-1$
		if (format != null) {
			format.setEnabled(false);
		}

		if (elem != null) {
			layout.setOMElement(elem, type, null);
		}
	}

	private void internalRebuild(final String type,
			final TextGridProject project) {

		layout = factory.createXLayout();
		layout.registerMetadataPart(this);

		if (top != null)
			top.dispose();

		top = new Composite(this.parent, SWT.None);
		top.setBackground(parent.getBackground());
		top.setLayout(parent.getLayout());
		top.setLayoutData(parent.getLayoutData());
		top.setData(parent.getData());

		xmlInputSource = null;

		InputStream input_xul = getClass().getClassLoader()
				.getResourceAsStream("metadata-" + type + ".xl.xml"); //$NON-NLS-1$ //$NON-NLS-2$

		boolean xul_inputSource_created = false;

		InputStream custom_input = getInputStreamFromProjectFile(project);

		if (custom_input != null) {
			try {
				String xul_content = MetaDataView
						.convertStreamToString(input_xul), custom_content = MetaDataView
						.convertStreamToString(custom_input);
				xul_content = xul_content.replaceFirst(
						"<tg:custom-placeholder.*?/>", custom_content); //$NON-NLS-1$

				xmlInputSource = new XLayoutSource("MetaData-XUL" + type + "_" //$NON-NLS-1$ //$NON-NLS-2$
						+ String.valueOf(System.currentTimeMillis()),
						new ByteArrayInputStream(xul_content.getBytes("UTF-8"))); //$NON-NLS-1$
				xul_inputSource_created = true;
			} catch (Exception e2) {
				Activator.handleError(e2);
				return;
			}

		}

		if (!xul_inputSource_created)
			xmlInputSource = new XLayoutSource("MetaData-XUL" + type //$NON-NLS-1$
			/* + String.valueOf(System.currentTimeMillis()) */, input_xul);

		layout.draw(top, xmlInputSource, type, false);

		editable = true;

		if (metadataForm != null)
			metadataForm.reflow(true);

		parent.layout(true, true);
	}

	/**
	 * Get the object's type
	 * 
	 * @param obj
	 */
	private void checkObjectType(TextGridObject obj) {
		if (obj != null) {
			try {
				ObjectType type = obj.getMetadata();

				if (type.getItem() != null) {
					objectType = OBJECT_TYPE.ITEM;
				} else if (type.getWork() != null) {
					objectType = OBJECT_TYPE.WORK;
				} else if (type.getEdition() != null) {
					objectType = OBJECT_TYPE.EDITION;
				} else if (type.getCollection() != null) {
					objectType = OBJECT_TYPE.COLLECTION;
				} else {
					String contentTypeId = obj.getContentTypeID();
					if (contentTypeId != null && !"".equals(contentTypeId)) { //$NON-NLS-1$
						if (contentTypeId.toLowerCase().contains("tg.work+")) { //$NON-NLS-1$
							objectType = OBJECT_TYPE.WORK;
						} else if (contentTypeId.toLowerCase().contains(
								"tg.edition+")) { //$NON-NLS-1$
							objectType = OBJECT_TYPE.EDITION;
						} else if (contentTypeId.toLowerCase().contains(
								"tg.collection+")) { //$NON-NLS-1$
							objectType = OBJECT_TYPE.COLLECTION;
						} else {
							objectType = OBJECT_TYPE.ITEM;
						}
					}
				}

			} catch (CrudServiceException e) {
				Activator.handleError(e, Messages.MetaDataSection_fetchFailed);
			} catch (CoreException e) {
				Activator.handleError(e, Messages.MetaDataSection_fetchFailed);
			}
		}
	}

	/**
	 * Was the metadata section created ?
	 * 
	 * @return boolean
	 */
	public boolean isMetadataSectionCreated() {
		return (layout != null);
	}

	/**
	 * Was the project file loaded ?
	 * 
	 * @return boolean
	 */
	public boolean isProjectFileLoaded() {
		return isProjectFileLoaded;
	}

	/**
	 * Sets the current metadata section whether editable or read-only.
	 * 
	 * @param editable
	 *            true or false
	 */
	public void setEditable(boolean editable) {

		if (editable == this.editable)
			return;

		List<Control> controls = layout.getAllControls();
		for (Control c : controls) {
			if (c != null
					&& !c.isDisposed()
					&& (!(c instanceof Label
							|| c instanceof Section
							|| (c instanceof Composite && !(c instanceof Combo)) || c instanceof Link))) {
				c.setEnabled(editable);
			}
		}

		this.editable = editable;
	}

	/**
	 * Returns the used {@link XLayout} instance.
	 * 
	 * @return XLayout
	 */
	public XLayout getXLayout() {

		return layout;
	}

	/**
	 * Returns the meta data in the Metadata section.
	 * 
	 * @return OMElement
	 */
	public OMElement getMetadataXML() {

		return layout.getOMElement();
	}

	/**
	 * Writes the meta data in the form to the TextGridObject.
	 */
	public void updateTextGridObject() {

		if (tgObj != null)
			tgObj.setMetadataXML(layout.getOMElement());
	}

	/**
	 * Returns the actual {@link TextGridObject}
	 * 
	 * @return TextGridObject
	 */
	public TextGridObject getTextGridObject() {

		updateTextGridObject();
		return tgObj;
	}

	/**
	 * Validate all required fields.
	 * 
	 */
	public boolean checkAllFields() {

		boolean valid = true;

		if (layout != null) {
			for (IControl c : layout.getAllControlElements()) {
				valid &= c.validate();
			}
		}

		return valid;
	}

	/**
	 * Writes the changes of the {@link TextGridObject} to the grid.
	 */
	public void writeToGrid() {

		OMElement omElem = layout.getOMElement();
		tgObj.setMetadataXML(omElem);

		Job saveJob = new Job(Messages.MetaDataSection_makePersistent) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					tgObj.reloadMetadata(false);
					tgObj.makeMetadataPersistent(monitor);
				} catch (CoreException e) {
					Activator.handleError(e, Messages.MetaDataSection_saveFailed);
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		};

		saveJob.schedule();

		try {
			saveJob.join();
		} catch (InterruptedException e) {
			Activator.handleError(e);
		}

		if (saveJob.getResult() != Status.OK_STATUS)
			return;

		System.out.println(Messages.MetaDataSection_saved);
	}

	/**
	 * Reload the projectfile
	 * 
	 * @param project
	 */
	public void reloadProjectFile(TextGridProject project) {
		rebuild(true, project);
	}

	/**
	 * Get the InputStream of the project file.
	 * 
	 * @param project
	 *            optional: if null, get it from tgObj
	 * 
	 * @return InputStream
	 */
	private InputStream getInputStreamFromProjectFile(TextGridProject p) {

		this.inputFromOM = null;
		TextGridProject project = null;

		if (p == null) {
			if (tgObj == null)
				return null;

			try {
				// String projectId = tgObj.getProject();
				project = tgObj.getProjectInstance();// TextGridProject.getProjectInstance(projectId);
			} catch (CrudServiceException e1) {
				Activator.handleError(e1);
			} catch (CoreException e1) {
				Activator.handleError(e1);
			}
			if (project == null)
				return null;

		} else {
			project = p;
		}

		final TextGridProjectFile projectFile = new TextGridProjectFile(project);

		IRunnableWithProgress fetchJob = new IRunnableWithProgress() {

			@Override
			public void run(IProgressMonitor monitor) {
				try {
					if (monitor == null)
						monitor = new NullProgressMonitor();

					monitor.beginTask(Messages.MetaDataSection_fetchingProjectfile, UNKNOWN);

					TgProjectFile projectFileData = projectFile
							.getProjectFileData(false, true, monitor);
					OMElement[] appDataChildren = TextGridProjectFile.extractAppDataChildren(projectFileData);
					
					OMElement mySection = null;
					if (appDataChildren != null && appDataChildren.length > 0) {
						for (OMElement child : appDataChildren) {
							if (child.getQName().equals(
									TextGridProjectFile.metadataSectionQName)) {
								mySection = child;
								break;
							}
						}
					}

					if (mySection != null) {
						OMElement xul_elem = mySection
								.getFirstChildWithName(new QName("tr")); //$NON-NLS-1$

						if (xul_elem != null) {
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							Writer w = new OutputStreamWriter(baos, "UTF-8"); //$NON-NLS-1$
							xul_elem.serialize(w); // XMLOutputFactory.newInstance().createXMLStreamWriter(baos));
							inputFromOM = new ByteArrayInputStream(
									baos.toByteArray());
							w.flush();
							w.close();
							baos.close();
						}
					}

					// TextGridProjectFile.getElementWithName("xlayout", ,
					// namespace, prefix)
					monitor.done();
				} catch (Exception e) {
					Activator.handleError(e);
				}
			}
		};

		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getWorkbench()
					.getProgressService().busyCursorWhile(fetchJob);
		} catch (InvocationTargetException e) {
			Activator.handleError(e);
		} catch (InterruptedException e) {
			Activator.handleError(e);
		}

		return inputFromOM;
	}

	@Override
	public boolean validate() {
		if (!validateActive)
			return false;

		checkAllFields();
		return true;
	}

	/**
	 * Not implemented: Use updateTextGridObject() instead
	 */
	@Override
	@Deprecated
	public void update() {
		updateTextGridObject();
	}

	@Override
	public boolean isEditable() {
		return editable;
	}

}
