/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.handlers;

import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;

import com.logabit.xlayout.XLayout;

/**
 * This handler creates a {@link List} control and returns it. It fully supports
 * the <code>style</code> attribute.
 * 
 * @author Stephan Niedermeier
 */
public class ListControlHandler extends AbstractControlHandler {

	@Override
	public Control createControl(Composite parent,
			HandlerElement controlElement, final XLayout layout,
			Element dataElement) {

		List list = new List(parent, controlElement.getSWTStyle() | SWT.BORDER);
		list.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				layout.setModified(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		return list;
	}
}
