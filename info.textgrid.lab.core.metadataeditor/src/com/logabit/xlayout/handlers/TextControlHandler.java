/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.handlers;

import org.dom4j.Element;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.logabit.xlayout.XLayout;

/**
 * This handler creates a {@link Text} control and returns it. It fully supports
 * the <code>style</code> attribute. It fully supports the <code>text</code>
 * attribute.
 * 
 * @author Stephan Niedermeier
 */
public class TextControlHandler extends AbstractControlHandler {

	@Override
	public Control createControl(Composite parent,
			HandlerElement controlElement, final XLayout layout,
			Element dataElement) {

		FormToolkit toolkit = new FormToolkit(parent.getDisplay());

		Text t = null;
		if ("true".equals(controlElement.getAttributeValueAsString( //$NON-NLS-1$
				XLayout.ATTR_MULTI_LINES, "false"))) { //$NON-NLS-1$
			t = toolkit.createText(parent, "", SWT.BORDER | SWT.MULTI //$NON-NLS-1$
					| SWT.WRAP | controlElement.getSWTStyle());

			t.addKeyListener(new KeyListener() {

				@Override
				public void keyReleased(KeyEvent e) {
					// TODO Auto-generated method stub
				}

				@Override
				public void keyPressed(KeyEvent e) {
					layout.getForm().reflow(true);
					// if (e.keyCode == SWT.CR || e.keyCode == SWT.BS) {
					// // System.err.println("hallo welt");
					// layout.getForm().reflow(true);
					// }
				}
			});
		} else {
			t = toolkit.createText(parent, "", SWT.BORDER /* | SWT.MULTI */ //$NON-NLS-1$
					| controlElement.getSWTStyle());
		}

		// new Text(parent, SWT.MULTI | controlElement.getSWTStyle());
		t.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				layout.setModified(true);
			}
		});

		return t;
	}
}
