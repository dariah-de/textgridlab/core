/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.handlers;

import org.dom4j.Element;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.logabit.xlayout.XLayout;

/**
 * Interface for control handler. The XLayout parser calls
 * {@link IControlHandler#createControl(Composite, HandlerElement)} every time
 * the according control XML element appears in the XLayout file. After that
 * {@link IControlHandler#configureControl(Control, HandlerElement)} will be
 * called to give the possibility to configure the control in different ways.
 * The best way to start with your own control handler is to inherit from
 * {@link AbstractControlHandler}.
 * 
 * @author Stephan Niedermeier
 * 
 */
public interface IControlHandler {

	/**
	 * Callback method which is responsible to create the according control and
	 * initialize it with given values from the attributes of the XLayout file.
	 * 
	 * @param parent
	 *            The parent composite.
	 * @param handlerElement
	 *            This objects holds all attributes and info about the control
	 *            element of the XLayout file.
	 * @return A new instance of the according control.
	 */
	public Control createControl(Composite parent,
			HandlerElement handlerElement, XLayout layout, Element dataElement);

	/**
	 * Callback method which will be called after createControl. Within this
	 * method additional configuration settings can be done. Each control
	 * handler can overwrite this method in order to set its own configuration.
	 * 
	 * @param control
	 *            - The control created by createControl
	 * @param handlerElement
	 *            - This objects holds all attributes and info about the control
	 *            element of the XLayout file.
	 */
	public void configureControl(Control control, HandlerElement handlerElement);
}
