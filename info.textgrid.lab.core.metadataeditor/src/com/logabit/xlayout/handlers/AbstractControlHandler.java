/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.handlers;

import org.eclipse.swt.widgets.Control;

import com.logabit.xlayout.utils.ControlConfigurator;

/**
 * Abstract base class for all control handlers. Configures the controls by
 * setting its attribute values by reflection on the created control.
 * 
 * @author Stephan Niedermeier
 * 
 */
public abstract class AbstractControlHandler implements IControlHandler {

	@Override
	public void configureControl(Control control, HandlerElement controlElement) {
		ControlConfigurator.setConfigurationsByReflection(control,
				controlElement);
	}
}
