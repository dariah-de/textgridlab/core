/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.handlers;

import java.util.Collections;
import java.util.Map;

import org.eclipse.swt.SWT;

import com.logabit.xlayout.ControlHandlerConfig;
import com.logabit.xlayout.utils.ConvertHelper;

/**
 * This object holds all information about a control element from the XLayout
 * file and provides some methods to easily access attributes and the handler
 * configuration entry.
 * 
 * @author Stephan Niedermeier
 */
public class HandlerElement {

	private Map<String, String> attributes;
	private ControlHandlerConfig config;

	// XXX added
	private static final String ATTR_READ_ONLY = "readonly"; //$NON-NLS-1$
	private int readonly = 0;

	public HandlerElement(ControlHandlerConfig config,
			Map<String, String> attributes) {
		this.config = config;
		this.attributes = attributes;
	}

	/**
	 * Looks for an attribute with given name and returns it as string value.
	 * Uses the rules defined in
	 * {@link ConvertHelper#convertToString(String, String)} for conversion.
	 * 
	 * @param name
	 *            The name of the attribute.
	 * @param defaultValue
	 *            - The default value to return if the conversion to string was
	 *            failed or the attribute value was not found.
	 * @return The attribute value as string.
	 */
	public String getAttributeValueAsString(String name, String defaultValue) {

		String stringValue = this.attributes.get(name);
		return ConvertHelper.convertToString(stringValue, defaultValue);
	}

	/**
	 * Looks for an attribute with given name and returns it as boolean value.
	 * Uses the rules defined in
	 * {@link ConvertHelper#convertToBoolean(String, boolean)} for conversion.
	 * 
	 * @param name
	 *            The name of the attribute.
	 * @param defaultValue
	 *            - The default value to return if the conversion to boolean was
	 *            failed.
	 * @return The attribute value as boolean.
	 */
	public boolean getAttributeValueAsBoolean(String name, boolean defaultValue) {

		String stringValue = this.attributes.get(name);
		return ConvertHelper.convertToBoolean(stringValue, defaultValue);
	}

	/**
	 * Looks for an attribute with given name and returns it as integer value.
	 * Uses the rules defined in
	 * {@link ConvertHelper#convertToInteger(String, int)} for conversion.
	 * 
	 * @param name
	 *            The name of the attribute.
	 * @param defaultValue
	 *            - The default value to return if the conversion to integer was
	 *            failed.
	 * @return The attribute value as integer.
	 */
	public int getAttributeValueAsInteger(String name, int defaultValue) {

		String stringValue = this.attributes.get(name);
		return ConvertHelper.convertToInteger(stringValue, defaultValue);
	}

	/**
	 * Returns all attributes as unmodifiable map.
	 * 
	 * @return The attributes.
	 */
	public Map<String, String> getAttributes() {
		return Collections.unmodifiableMap(this.attributes);
	}

	/**
	 * Returns the configuration object for the according handler.
	 * 
	 * @return The control handler configuration.
	 */
	public ControlHandlerConfig getControlHandlerConfig() {

		return this.config;
	}

	/**
	 * Returns the value of the <code>style</code> attribute already converted
	 * to the SWT like integer value. If no style attribute exists, the value of
	 * the <code>default-style</code> from the handler configuration will be
	 * returned. If also no default style exists, {@link SWT#NONE} will be
	 * returned.
	 * 
	 * @return The configured style for the control.
	 */
	public int getSWTStyle() {

		String styleAttrValue = this.attributes.get("style"); //$NON-NLS-1$
		String rdonly = this.attributes.get(ATTR_READ_ONLY);

		if (rdonly != null && rdonly.equals("true")) { //$NON-NLS-1$
			readonly = SWT.READ_ONLY;
		}

		if (styleAttrValue == null || "".equals(styleAttrValue)) { //$NON-NLS-1$
			return this.config.getDefaultStyle() | readonly;
		}

		String[] tokens = styleAttrValue.split("\\|"); //$NON-NLS-1$

		String token = null;
		int style = 0;
		for (int a = 0; a < tokens.length; a++) {
			token = tokens[a].trim();
			style |= ConvertHelper.convertToSWTStyle(token, SWT.NONE);
		}

		return style | readonly;
	}

	public int getReadOnlyFlag() {

		return readonly;
	}

}