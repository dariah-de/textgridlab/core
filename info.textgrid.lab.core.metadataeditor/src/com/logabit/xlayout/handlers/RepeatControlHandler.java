/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package com.logabit.xlayout.handlers;

import org.dom4j.Element;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import com.logabit.xlayout.XLayout;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class RepeatControlHandler extends AbstractControlHandler {

	private FormToolkit toolkit = null;
	private Section sec = null;

	@Override
	public Section createControl(Composite parent,
			HandlerElement controlElement, final XLayout layout,
			Element dataElement) {

		boolean optional = "true".equals(dataElement //$NON-NLS-1$
				.attributeValue(XLayout.ATTR_OPTIONAL));

		toolkit = new FormToolkit(parent.getParent().getDisplay());
		sec = toolkit.createSection(parent,/*
											 * ExpandableComposite.TITLE_BAR |
											 * ExpandableComposite.TWISTIE |
											 */
		optional ? ExpandableComposite.TWISTIE : ExpandableComposite.EXPANDED);

		sec.setBackground(parent.getBackground());

		sec.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				layout.fireFocusEvent();
			}
		});

		return sec;
	}
}
