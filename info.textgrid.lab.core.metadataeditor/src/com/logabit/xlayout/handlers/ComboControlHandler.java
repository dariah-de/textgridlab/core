/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.handlers;

import org.dom4j.Element;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.logabit.xlayout.XLayout;

/**
 * This handler creates a {@link Combo} control and returns it. It fully
 * supports the <code>style</code> attribute.
 * 
 * @author Stephan Niedermeier
 */
public class ComboControlHandler extends AbstractControlHandler {

	@Override
	public Control createControl(Composite parent,
			HandlerElement controlElement, final XLayout layout,
			Element dataElement) {

		Combo combo = new Combo(parent, controlElement.getSWTStyle());
		combo.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				layout.setModified(true);
			}
		});
		return combo;
	}
}
