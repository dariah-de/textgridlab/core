/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;

import com.logabit.xlayout.handlers.IControlHandler;
import com.logabit.xlayout.utils.ConvertHelper;
import com.logabit.xlayout.utils.InstanceHelper;

/**
 * This class represents the configuration data for exactly one control handler.
 * It defines things like the name of the control element, the according control
 * handler class and more.
 * 
 * @author Stephan Niedermeier
 */
public class ControlHandlerConfig {

	private String name;
	private String className;
	private String defaultStyleAsString = null;
	private int defaultStyle = SWT.NONE;
	private IControlHandler handlerInstance;

	public ControlHandlerConfig(String name, String className,
			String defaultStyleAsString) {
		this(name, className);
		this.defaultStyleAsString = defaultStyleAsString;

		// Convert the default style to SWT constants
		if (defaultStyleAsString != null)
			this.defaultStyle = ConvertHelper.convertToSWTStyle(
					this.defaultStyleAsString, SWT.NONE);
	}

	public ControlHandlerConfig(String name, String className) {
		this.name = name;
		this.className = className;
	}

	public ControlHandlerConfig(String name, IControlHandler handler) {
		this.name = name;
		this.handlerInstance = handler;
		this.className = handler.getClass().getName();
	}

	/**
	 * @return The class which represents the control handler instance.
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Creates an instance of the given control handler class name, stores it as
	 * member and then returns it. The next time this method was called the
	 * already created instance will be returned.
	 * 
	 * @return The instance of the control handler class.
	 */
	protected IControlHandler getControlHandlerInstance(ClassLoader classLoader) {

		if (this.handlerInstance == null) {
			try {
				this.handlerInstance = (IControlHandler) InstanceHelper
						.createInstance(this.className, classLoader);
			} catch (Exception e) {
				throw new ControlHandlerConfigurationException(NLS.bind(
						"Could not create control handler instance '{0}'!", this.className), e); //$NON-NLS-1$
			}
		}

		return this.handlerInstance;
	}

	/**
	 * @return The name of the control element.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the default style as integer like SWT needs it.
	 * 
	 * @return The default style as integer.
	 */
	public int getDefaultStyle() {
		return this.defaultStyle;
	}

	/**
	 * Returns the string representation of the default style as given in the
	 * configuration attribute. If no such attribute was found, null will be
	 * returned.
	 * 
	 * @return The string representation as string.
	 */
	public String getDefaultStyleAsString() {
		return this.defaultStyleAsString;
	}
}
