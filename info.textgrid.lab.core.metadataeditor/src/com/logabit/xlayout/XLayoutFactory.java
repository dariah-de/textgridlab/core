/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.eclipse.osgi.util.NLS;
import org.xml.sax.InputSource;

import com.logabit.xlayout.handlers.IControlHandler;

/**
 * Is reposible to read the default control handler configuration and creates
 * control handler configuration objects out of that. After instanciating and
 * initialization XLayout instances can be created using
 * {@link XLayoutFactory#createXLayout()}. It's also possible to add user
 * defined control handlers either by using one of the <code>register...</code>
 * methods or by placing a control handler XML configuration file
 * <code>handler-config.xml</code> into the classpath root and register all user
 * defined control handlers here. This file will automaticaly picked up at
 * instanciation time of the factory.
 * 
 * @author Stephan Niedermeier, Andreas Schmid
 */
public class XLayoutFactory {

	private static final String DEFAULT_HANDLERS_CONFIG_NAME = "handler-config-default.xml"; //$NON-NLS-1$
	private static final String USER_HANDLERS_CONFIG_NAME = "handler-config.xml"; //$NON-NLS-1$

	private Map<String, ControlHandlerConfig> controlHandlerConfigs = new HashMap<String, ControlHandlerConfig>();
	private static ClassLoader resourceClassLoader;

	private static XLayoutFactory factory = null;

	public static XLayoutFactory getFactory(ClassLoader resourceClassLoader) {
		XLayoutFactory.resourceClassLoader = resourceClassLoader;
		return getFactory();
	}

	public static XLayoutFactory getFactory() {
		if (factory == null)
			factory = new XLayoutFactory();
		return factory;
	}

	private XLayoutFactory() {

		// The last overwrites the preceding one
		this.findAndRegisterDefaultControlHandlers();
		this.findAndRegisterCustomControlHandlers();
	}

	/**
	 * Finds and registers all default control handlers.
	 */
	private void findAndRegisterDefaultControlHandlers() {
		// Register Default handlers.
		URL resource = IControlHandler.class
				.getResource(DEFAULT_HANDLERS_CONFIG_NAME);
		InputStream is = IControlHandler.class
				.getResourceAsStream(DEFAULT_HANDLERS_CONFIG_NAME);
		this.registerControlHandlers(is, resource.getFile(), false);
	}

	/**
	 * Finds and registers all user defined control handlers.
	 */
	private void findAndRegisterCustomControlHandlers() {

		URL resource = this.getResourceClassLoader().getResource(
				USER_HANDLERS_CONFIG_NAME);

		if (resource != null) {
			InputStream is = this.getResourceClassLoader().getResourceAsStream(
					USER_HANDLERS_CONFIG_NAME);
			this.registerControlHandlers(is, resource.getFile(), true); // Overwrite
			// default
			// handlers
		}
	}

	public void destroy() {
		factory = null;
	}

	/**
	 * Creates a new XLayout instance and initializes it with registered
	 * handlers.
	 * 
	 * @return Fresh XLayout instance.
	 */
	public XLayout createXLayout() {
		XLayout layout = new XLayout(this.controlHandlerConfigs,
				this.getResourceClassLoader());
		return layout;
	}

	/**
	 * Returns the resource class loader which will be used to load resources
	 * from the classpath.
	 * 
	 * @return
	 */
	private ClassLoader getResourceClassLoader() {
		if (resourceClassLoader == null)
			return IControlHandler.class.getClassLoader();

		return resourceClassLoader;
	}

	/**
	 * Registers a new control handler. NOTE: The required handlers must be
	 * registered before the createXLayout method is called!
	 * 
	 * @param name
	 *            The name of handler.
	 * @param handler
	 *            The handler instance.
	 * @param overwrite
	 *            If this flag is true, an already existing handler with that
	 *            name will be overwritten. If this flag is false and a handler
	 *            with that name already exists, nothing happens.
	 */
	public void registerControlHandler(String name, IControlHandler handler,
			boolean overwrite) {

		// No overwrite => Return if handler already exists with that name
		if (!overwrite) {
			if (this.controlHandlerConfigs.containsKey(name))
				return;
		}

		ControlHandlerConfig config = new ControlHandlerConfig(name, handler);
		this.registerControlHandler(config, overwrite);
	}

	/**
	 * Removes the handler with the given name from the list of registered
	 * handlers. NOTE: It is possible that the handler is still in use by a
	 * XLayout instance created before!
	 * 
	 * @param name
	 */
	public void removeControlHandler(String name) {
		this.controlHandlerConfigs.remove(name);
	}

	/**
	 * Registers an new handler using a given configuration.
	 * 
	 * @param handlerConfig
	 *            The handler configuration.
	 * @param overwrite
	 *            If true, overwrites an already existing handler with that
	 *            name.
	 */
	public void registerControlHandler(ControlHandlerConfig handlerConfig,
			boolean overwrite) {

		if (!overwrite)
			if (this.controlHandlerConfigs.containsKey(handlerConfig.getName()))
				return;
		try {
			this.controlHandlerConfigs.put(handlerConfig.getName(),
					handlerConfig);
		} catch (Exception e) {
			throw new XLayoutFactoryException(NLS.bind(
					"Could not register control handler '{0}'!", handlerConfig.getName()), e); //$NON-NLS-1$
		}
	}

	/**
	 * Registers new handlers using the given configuration objects.
	 * 
	 * @param handlerConfigs
	 *            The list of configurations.
	 * @param overwrite
	 *            If true, overwrites an already existing handler with that
	 *            name.
	 */
	public void registerControlHandlers(
			List<ControlHandlerConfig> handlerConfigs, boolean overwrite) {

		for (ControlHandlerConfig config : handlerConfigs) {
			this.registerControlHandler(config, overwrite);
		}
	}

	/**
	 * Reads from an xml file and registers all handlers placed in this file.
	 * The general structur of this xml file must be: <control-handlers>
	 * <control-handler name="..." class="..."/> ... </control-handlers>
	 * 
	 * @param file
	 *            The xml configuration file.
	 * @param overwrite
	 *            If true, overwrites an already existing handler with that
	 *            name.
	 */
	public void registerControlHandlers(File file, boolean overwrite) {

		SAXReader reader = new SAXReader();
		Document doc;
		try {
			doc = reader.read(file);
		} catch (DocumentException e) {
			throw new ParseException(NLS.bind("Error parsing control handler config '{0}': {1}", file.getPath(), e.getMessage()), e); //$NON-NLS-1$
		}

		this.parseControlHandlerDocument(doc, overwrite);
	}

	/**
	 * Reads from an xml input stream and registers all handlers placed in this
	 * file. The general structur of this xml file must be: <control-handlers>
	 * <control-handler name="..." class="..."/> ... </control-handlers>
	 * 
	 * @param is
	 *            The xml configuration input stream.
	 * @param hint
	 *            A hint for the input stream which says where the stream comes
	 *            from.
	 * @param overwrite
	 *            If true, overwrites an already existing handler with that
	 *            name.
	 */
	public void registerControlHandlers(InputStream is, String hint,
			boolean overwrite) {

		SAXReader reader = new SAXReader();
		InputSource inputSource = new InputSource(hint);
		inputSource.setByteStream(is);

		Document doc;
		try {
			doc = reader.read(inputSource);
		} catch (DocumentException e) {
			throw new ParseException(NLS.bind(
					"Error parsing control handler config from input stream '{0}': {1}", hint, e.getMessage()), e); //$NON-NLS-1$
		}

		this.parseControlHandlerDocument(doc, overwrite);
	}

	/**
	 * Internal method to parse a given dom4j doc.
	 * 
	 * @param doc
	 * @param overwrite
	 */
	@SuppressWarnings("unchecked")
	private void parseControlHandlerDocument(Document doc, boolean overwrite) {
		Element root = doc.getRootElement();
		List<Element> controlHandlers = root.selectNodes("control-handler"); //$NON-NLS-1$
		List<ControlHandlerConfig> handlerConfigs = new ArrayList<ControlHandlerConfig>();

		String name;
		String className;
		String defaultStyle;
		for (Element elemControlHandler : controlHandlers) {
			name = elemControlHandler.valueOf("@name"); //$NON-NLS-1$
			className = elemControlHandler.valueOf("@class"); //$NON-NLS-1$
			defaultStyle = elemControlHandler.valueOf("@default-style"); //$NON-NLS-1$
			// @TODO Parse the overwrite attribute here
			ControlHandlerConfig config = new ControlHandlerConfig(name,
					className, defaultStyle);
			handlerConfigs.add(config);
		}

		this.registerControlHandlers(handlerConfigs, overwrite);
	}

	/**
	 * Returns the control handler configuration for the given control handler
	 * or null if no such configuration for the given name exists.
	 * 
	 * @param name
	 *            - The name of the control handler.
	 * @return The control handler configuration object or null.
	 */
	public ControlHandlerConfig getControlHandlerConfig(String name) {
		return this.controlHandlerConfigs.get(name);
	}

	/**
	 * Returns whether a control handler with given name is already registered.
	 * 
	 * @param name
	 * @return
	 */
	public boolean containsControlHandler(String name) {
		return this.controlHandlerConfigs.containsKey(name);
	}
}
