/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Modified for the TextGrid-Project by Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
package com.logabit.xlayout;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.metadataeditor.IMetadataPart;
import info.textgrid.lab.core.metadataeditor.elements.AdvancedControlElement;
import info.textgrid.lab.core.metadataeditor.elements.ComplexRepeatControlElement;
import info.textgrid.lab.core.metadataeditor.elements.CustomControlElement;
import info.textgrid.lab.core.metadataeditor.elements.ICheckableElement;
import info.textgrid.lab.core.metadataeditor.elements.IControl;
import info.textgrid.lab.core.metadataeditor.elements.IRepresentableComposite;
import info.textgrid.lab.core.metadataeditor.elements.IRepresentableControl;
import info.textgrid.lab.core.metadataeditor.elements.MoreControlElement;
import info.textgrid.lab.core.metadataeditor.elements.RepeatControlElement;
import info.textgrid.lab.core.metadataeditor.elements.SourceControlElement;
import info.textgrid.lab.core.metadataeditor.elements.SpecificControlElement;
import info.textgrid.lab.core.metadataeditor.elements.StandaloneGroupControlElement;
import info.textgrid.lab.core.metadataeditor.elements.SwitchControlElement;
import info.textgrid.lab.core.metadataeditor.elements.SwitchGroup;
import info.textgrid.lab.core.metadataeditor.elements.TextgridDateControlElement;
import info.textgrid.lab.core.metadataeditor.utils.AddItems;
import info.textgrid.lab.core.metadataeditor.utils.MetadataPath;
import info.textgrid.lab.core.metadataeditor.utils.OMCreator;
import info.textgrid.lab.core.metadataeditor.utils.OMItem;
import info.textgrid.lab.core.metadataeditor.utils.OMReader;
import info.textgrid.lab.core.metadataeditor.utils.OMUtil;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.jaxen.JaxenException;

import com.logabit.xlayout.handlers.ButtonControlHandler;
import com.logabit.xlayout.handlers.ComplexRepeatControlHandler;
import com.logabit.xlayout.handlers.CustomControlHandler;
import com.logabit.xlayout.handlers.HandlerElement;
import com.logabit.xlayout.handlers.IControlHandler;
import com.logabit.xlayout.handlers.MoreControlHandler;
import com.logabit.xlayout.handlers.RepeatControlHandler;
import com.logabit.xlayout.handlers.SourceControlHandler;
import com.logabit.xlayout.handlers.SpecificControlHandler;
import com.logabit.xlayout.handlers.StandaloneGroupControlHandler;
import com.logabit.xlayout.handlers.SwitchControlHandler;
import com.logabit.xlayout.handlers.TextgridDateControlHandler;
import com.logabit.xlayout.source.XLayoutSource;
import com.logabit.xlayout.source.XLayoutSourceResolver;
import com.logabit.xlayout.utils.AttributeHelper;
import com.logabit.xlayout.utils.ConvertHelper;
import com.logabit.xlayout.utils.DurableStack;

/**
 * This is the layout representation of an XLayout definition.
 * 
 * @author Stephan Niedermeier, Andreas Schmid
 */
public class XLayout {

	private static final String NAMESPACE_URI = "http://logabit.com/xlayout/controls/1.0"; //$NON-NLS-1$
	private static final String NAMESPACE_URI_DEPRECATED = "http://logabit.com/xlayout/1.0"; //$NON-NLS-1$
	private static final String TG_NAMESPACE_URI = "http://textgrid.de/xlayout/controls"; //$NON-NLS-1$
	private static final String ELEMENT_TR = "tr"; //$NON-NLS-1$
	private static final String ELEMENT_TD = "td"; //$NON-NLS-1$
	private static final String ELEMENT_TABLE = "table"; //$NON-NLS-1$
	private static final String ATTR_COLSPAN = "colspan"; //$NON-NLS-1$
	private static final String ATTR_ROWSPAN = "rowspan"; //$NON-NLS-1$
	private static final String ATTR_ALIGN = "align"; //$NON-NLS-1$
	private static final String ATTR_VALIGN = "valign"; //$NON-NLS-1$
	private static final String ATTR_ID = "id"; //$NON-NLS-1$
	private static final String ATTR_HEIGHT = "height"; //$NON-NLS-1$
	private static final String ATTR_WIDTH = "width"; //$NON-NLS-1$
	private static final String ELEMENT_INCLUDE = "include"; //$NON-NLS-1$
	private static final String ELEM_ROOT = "xlayout"; //$NON-NLS-1$
	// private static final String XLAYOUT_FILE_SUFFIX = ".xl.xml";

	// XXX added
	public static final String ATTR_ELEMENT_NAME = "element_name"; //$NON-NLS-1$
	public static final String ATTR_RELATED_ATTRIBUTE_NAME = "related_to"; //$NON-NLS-1$
	public static final String ATTR_INPUT_TYPE = "input_type"; //$NON-NLS-1$
	public static final String ATTR_REQUIRED = "required"; //$NON-NLS-1$
	public static final String ATTR_MULTI_LINES = "multi_lines"; //$NON-NLS-1$
	public static final String ATTR_CATEGORY = "category"; //$NON-NLS-1$
	public static final String ATTR_OPTIONAL = "optional"; //$NON-NLS-1$
	public static final String ATTR_ITEMS = "items"; //$NON-NLS-1$
	public static final String ATTR_TEXT = "text"; //$NON-NLS-1$
	public static final String ATTR_AUTO_COMPLETION = "auto_completion"; //$NON-NLS-1$

	private Map<String, ControlHandlerConfig> controlHandlerConfigs = new HashMap<String, ControlHandlerConfig>();
	private boolean throwExceptionIfControlNotFound = false;

	/* Holds all controls defined in the XLayout definition */
	private Map<String, Control> controlsMap = new HashMap<String, Control>();

	/* Holds all "open" composites */
	private DurableStack<Composite> parentsStack = new DurableStack<Composite>();

	/* Holds all "open" xlayout source includes */
	private Stack<XLayoutSource> xLayoutSourceStack = new Stack<XLayoutSource>();

	private XLayoutSourceResolver sourceResolver;
	private ClassLoader resourceClassLoader;

	private FormToolkit toolkit = null;
	private ScrolledForm form = null;

	private RepeatControlElement repElement = null;
	private ComplexRepeatControlElement complexRepElement = null;
	private TextgridDateControlElement tgDateElement = null;
	private SpecificControlElement specificElement = null;
	private CustomControlElement customElement = null;
	private SwitchControlElement switchElement = null;
	private MoreControlElement moreElement = null;
	private StandaloneGroupControlElement standaloneElement = null;
	private SourceControlElement sourceElement = null;

	private StandaloneGroupControlElement editionLinksElement = null;
	private ArrayList<IControl> controlElements = new ArrayList<IControl>();

	private SwitchGroup switchGrp1, switchGrp2, currentSwitchGrp;

	private boolean doRepeat = false;
	private boolean doComplexRepeat = false;
	private boolean doCustom = false;
	private boolean doSpecific = false;
	private boolean doSwitch = false;
	private boolean doMore = false;
	private boolean doStandaloneGroup = false;
	private boolean doSource = false;

	private boolean doCollectSourceElements = false;
	private SourceControlElement collectingSourceElement = null;

	private ArrayList<OMItem> omItems_provided = new ArrayList<OMItem>();
	private ArrayList<OMItem> omItems_specific = new ArrayList<OMItem>();
	private ArrayList<OMItem> omItems_custom = new ArrayList<OMItem>();

	private static OMFactory omFactory = OMAbstractFactory.getOMFactory();

	private OMElement objectOMElem = null;
	private OMElement genericOMElem = null;
	private OMElement providedOMElem = null;
	private OMElement generatedOMElem = null;
	private OMElement specificOMElem = null;
	private OMElement templateEditorOM = null;
	private OMElement customOMElem = null;
	private OMElement relationsOMElem = null;

	private OMCreator omCreator_provided = new OMCreator();
	private OMCreator omCreator_specific = new OMCreator();
	private OMCreator omCreator_custom = new OMCreator();

	private OMReader omReader_provided = new OMReader();
	private OMReader omReader_specific = new OMReader();
	private OMReader omReader_custom = new OMReader();

	// tg-Namespace
	private OMNamespace tgNS = omFactory.createOMNamespace(
			TextGridObject.TEXTGRID_METADATA_NAMESPACE, "ns3"); //$NON-NLS-1$

	// custom-namespace
	private OMNamespace customNS = omFactory.createOMNamespace(
			TextGridObject.CUSTOM_NAMESPACE, "cns"); //$NON-NLS-1$

	private static String templateEditorElementName = "templateEditor"; //$NON-NLS-1$

	// editor modified ??
	private boolean modified = false;

	// The metadata part
	private IMetadataPart metadataPart = null;

	// View with Buttons ??
	private boolean showButtons = true;

	private Map<String, ICheckableElement> pathControlsMap = new HashMap<String, ICheckableElement>();
	private String currentPath = "/object/generic/provided/"; //$NON-NLS-1$

	private Label infoLabel = null;

	public SourceControlElement getCollectingSourceElement() {
		return collectingSourceElement;
	}

	public void setCollectingSourceElement(
			SourceControlElement collectingSourceElement) {
		this.collectingSourceElement = collectingSourceElement;
	}

	public boolean isDoCollectSourceElements() {
		return doCollectSourceElements;
	}

	public void setDoCollectSourceElements(boolean doCollectSourceElements) {
		this.doCollectSourceElements = doCollectSourceElements;
	}

	protected XLayout(Map<String, ControlHandlerConfig> handlerConfigs,
			ClassLoader resourceClassLoader) {

		this.controlHandlerConfigs = handlerConfigs;
		this.resourceClassLoader = resourceClassLoader;
		this.sourceResolver = new XLayoutSourceResolver(resourceClassLoader);
	}

	/**
	 * Return the control instance as given by id in the xLayout file.
	 * 
	 * @param id
	 *            The id of the control within the xLayout file.
	 * @return The control instance with given id or null if no such control
	 *         with the given id was found.
	 */
	public Control getControl(String id) {

		if (this.throwExceptionIfControlNotFound) {
			if (!this.controlsMap.containsKey(id))
				throw new ControlNotFoundException(NLS.bind("No control with id '{0}' found in layout '{1}'!", id, this.xLayoutSourceStack.peek().getHint())); //$NON-NLS-1$
		}

		return this.controlsMap.get(id);
	}

	/**
	 * Sets whether an
	 * 
	 * @see ControlNotFoundException must be thrown if a control with given id
	 *      was requested but was not found.
	 * @param throwException
	 *            True if an exception must be thrown.
	 */
	public void setThrowExceptionIfControlNotFound(boolean throwException) {

		this.throwExceptionIfControlNotFound = throwException;
	}

	/**
	 * Returns whether an
	 * 
	 * @see ControlNotFoundException must be thrown if a control with given id
	 *      was requested but was not found.
	 * @return True if a exception must be thrown
	 */
	public boolean isThrowExceptionIfControlNotFound() {

		return this.throwExceptionIfControlNotFound;
	}

	/**
	 * Renders the editor.
	 * 
	 * @param parent
	 * @param source
	 * @param showButtons
	 */
	public void draw(Composite parent, XLayoutSource source, String type,
			boolean showButtons) {

		this.showButtons = showButtons;

		this.specificElementName = type;

		toolkit = new FormToolkit(parent.getDisplay());
		form = toolkit.createScrolledForm(parent);
		form.getBody().setLayout(new GridLayout());
		form.getBody().setLayoutData(
				new GridData(GridData.FILL, GridData.CENTER, true, false));
		form.setBackground(parent.getBackground());

		this.startParsing(source, form.getBody());
	}

	public ScrolledForm getForm() {
		return form;
	}

	/**
	 * This is the (only) internal entry point to start the overall parsing
	 * process.
	 * 
	 * @param xsource
	 * @param parent
	 */
	private void startParsing(XLayoutSource xsource, Composite parent) {
		// Add current document location to stack
		this.xLayoutSourceStack.push(xsource);

		// Add parent to the parentsStack
		this.parentsStack.push(parent);

		Document doc = CachedDocumentFactory.getDocument(xsource);
		this.parseDocument(doc);

		// Pop stacks
		this.parentsStack.pop();
		this.xLayoutSourceStack.pop();
	}

	/**
	 * Parses the given document.
	 * 
	 * @param doc
	 */
	@SuppressWarnings("unchecked")
	private void parseDocument(Document doc) {

		Element root = doc.getRootElement();

		// Check whether root element ist ELEM_ROOT
		if (!ELEM_ROOT.equals(root.getName()))
			throw new ParseException(NLS.bind("The XLayout file '{0}' must contain the root element '{1}'!", this.xLayoutSourceStack.peek().getHint(), ELEM_ROOT)); //$NON-NLS-1$

		this.controlsMap.clear();

		// Get all child elements
		List<Element> tableElems = root.elements();

		for (Element elem : tableElems) {
			if (ELEMENT_TABLE.equals(elem.getName()))
				this.parseTable(elem, null);
			else if (ELEMENT_INCLUDE.equals(elem.getName()))
				this.parseInclude(elem);
			// Simply ignore all other elements
		}
	}

	/**
	 * Includes a XLayout document and parses it recursively.
	 * 
	 * @param includeElement
	 * @throws ParseException
	 */
	private void parseInclude(Element includeElement) {

		String uriString = includeElement.attributeValue("uri"); //$NON-NLS-1$

		try {
			XLayoutSource xsource = sourceResolver.resolve(uriString);
			this.startParsing(xsource, this.parentsStack.peek());
		} catch (Exception e) {
			throw new ParseException(NLS.bind("Error including '{0}' into '{1}'!", uriString, this.xLayoutSourceStack.peek().getHint()), e); //$NON-NLS-1$
		}
	}

	/**
	 * Parses the given table element. If the argument parent is null, a new
	 * composite will be created. Otherwise the existing parent will be used.
	 * 
	 * @param tableElement
	 *            The table element which contains the layout information.
	 * @param parent
	 *            The parent composite to use or null if a new one should be
	 *            created.
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public Composite parseTable(Element tableElement, Composite parent)
			throws ParseException {

		boolean isSC = false;

		if (parent == null) {
			isSC = true;
			String styleAttr = AttributeHelper.getAsString(tableElement,
					"style", null); //$NON-NLS-1$
			int style = ConvertHelper.convertToSWTStyle(styleAttr, SWT.NONE);

			ScrolledComposite sc = new ScrolledComposite(
					this.parentsStack.peek(), SWT.H_SCROLL | SWT.V_SCROLL
							| style);
			sc.setExpandHorizontal(true);
			sc.setExpandVertical(true);

			parent = new Composite(sc, SWT.NONE);

			GridData data = new GridData();
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			parent.setLayoutData(data);

			sc.setContent(parent);
		}

		// Add new composite that represents the current table to the stack
		this.parentsStack.push(parent);

		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 1;
		gridLayout.marginWidth = 1;

		gridLayout.numColumns = 1;// this.getNumberOfCols(tableElement);
		parent.setLayout(gridLayout);

		List<Element> trList = tableElement.selectNodes(ELEMENT_TR);

		for (Element trElement : trList) {
			this.parseRows(trElement);
		}

		if (isSC && parent.getParent() instanceof ScrolledComposite) {
			ScrolledComposite sc = (ScrolledComposite) parent.getParent();
			sc.setMinSize(parent.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			parent.layout();
		}
		// Complete table parsed, remove composite from stack
		return this.parentsStack.pop();
	}

	private Label elemLabel = null;

	/**
	 * Parses a table row element by iteration over all td elemets within the tr
	 * element.
	 * 
	 * @param trElement
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	private void parseRows(Element trElement) throws ParseException {

		List<Element> tdList = trElement.selectNodes(ELEMENT_TD);

		for (Element tdElement : tdList) {
			this.parseColumn(tdElement);
		}

		elemLabel = null;
	}

	/**
	 * Parses the table column by reading the content of td.
	 * 
	 * @param tdElement
	 * @throws ParseException
	 */
	private void parseColumn(Element tdElement) throws ParseException {

		Composite composite = new Composite(this.parentsStack.peek(), SWT.NONE);

		GridLayout layout = new GridLayout();
		layout.marginBottom = 0;
		layout.marginHeight = 0;
		layout.marginLeft = 0;
		layout.marginRight = 0;
		layout.marginTop = 0;
		layout.marginWidth = 0;
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;

		composite.setLayout(layout);
		setGridData(composite, tdElement);

		// composite.setLayout(layout);

		this.parentsStack.push(composite);

		for (Object ele : tdElement.elements()) {
			if (ele instanceof Element) {
				Element element = (Element) ele;
				this.parseTableData(element);
				layout.numColumns++;
			}
		}

		this.parentsStack.pop();
	}

	/**
	 * Parses the content of td.
	 * 
	 * @param dataElement
	 * @throws ParseException
	 */
	private void parseTableData(Element dataElement) throws ParseException {
		String nameSpaceUri = dataElement.getQName().getNamespaceURI();

		if (NAMESPACE_URI.equals(nameSpaceUri)
				|| TG_NAMESPACE_URI.equals(nameSpaceUri)) {

			Composite parent = this.parentsStack.peek();

			// Generic control element handling
			// A handler must be registered before starting the parse process!
			// This can be done in the constructor or by the user using
			// addControlElementHandler().
			String controlName = dataElement.getQName().getName();
			ControlHandlerConfig handlerConfig = this.controlHandlerConfigs
					.get(controlName);

			if (handlerConfig == null)
				throw new ParseException(NLS.bind(
						"No handler for control element with name '{0}' found!", controlName)); //$NON-NLS-1$

			// Convert from Dom4J Element to XLayout element for abstraction
			// reasons
			HandlerElement handlerElement = this
					.createHandlerElement(dataElement);

			IControlHandler handler = handlerConfig
					.getControlHandlerInstance(this.resourceClassLoader);

			if (!showButtons && handler instanceof ButtonControlHandler
					&& !"true".equals(dataElement.attributeValue("force"))) //$NON-NLS-1$ //$NON-NLS-2$
				return;

			Control control = handler.createControl(parent, handlerElement,
					this, dataElement);

			handler.configureControl(control, handlerElement);

			control.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
					true, false));

			String id = dataElement.attributeValue(ATTR_ID);

			if (control instanceof Label) {
				if (id == null) {
					// new Label(parent, SWT.None);
					return;
				} else {
					if (!"lblInfo".equals(id)) //$NON-NLS-1$
						elemLabel = (Label) control;
					else
						infoLabel = (Label) control;
					
				}
			}

			// XXX you have to modify the code for these elements
			if (control instanceof ProgressBar || control instanceof Sash
					|| control instanceof Scale || control instanceof Table
					|| control instanceof Tree) {
				Activator
						.handleWarning(
								new RuntimeException(),
								NLS.bind(Messages.XLayout_notTestedWarning, dataElement.getName()));
			} else if (control instanceof Text) {
				Text t = (Text) control;
				t.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						modified = true;
					}
				});
				t.addFocusListener(new FocusListener() {
					@Override
					public void focusGained(FocusEvent e) {
					}

					@Override
					public void focusLost(FocusEvent e) {
						fireFocusEvent();
					}
				});
			} else if (control instanceof org.eclipse.swt.widgets.Combo
					|| control instanceof org.eclipse.swt.widgets.List) {

				String t = dataElement.attributeValue(ATTR_ITEMS);
				if (t == null)
					AddItems.addItemsToControl(control, dataElement);

				if (control instanceof Combo) {
					Combo c = (Combo) control;

					if (t != null) {
						if ("agents".equals(t)) //$NON-NLS-1$
							AddItems.addAgentItems(c);
						else if ("scripts".equals(t)) //$NON-NLS-1$
							AddItems.addScriptItems(c);
						// else if ("languages".equals(t))
						// AddItems.addLanguageItems(c);
					}

					c.addModifyListener(new ModifyListener() {
						@Override
						public void modifyText(ModifyEvent e) {
							modified = true;
						}
					});
					c.addFocusListener(new FocusListener() {
						@Override
						public void focusGained(FocusEvent e) {
						}

						@Override
						public void focusLost(FocusEvent e) {
							fireFocusEvent();
						}
					});
				} else if (control instanceof org.eclipse.swt.widgets.List) {
					org.eclipse.swt.widgets.List l = (org.eclipse.swt.widgets.List) control;
					l.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}

						@Override
						public void widgetSelected(SelectionEvent e) {
							modified = true;
							// System.err.println("Listitem selected!!");
						}
					});
					l.addFocusListener(new FocusListener() {
						@Override
						public void focusGained(FocusEvent e) {
						}

						@Override
						public void focusLost(FocusEvent e) {
							fireFocusEvent();
						}
					});
				}
			} else if ("group".equals(dataElement.getName()) && doSwitch) { //$NON-NLS-1$
				currentSwitchGrp = new SwitchGroup(id, dataElement);
				control.setParent(currentSwitchGrp.getComposite());
				if (switchGrp1 == null)
					switchGrp1 = currentSwitchGrp;
				else
					switchGrp2 = currentSwitchGrp;
			} else if ("group".equals(dataElement.getName()) && doSource) { //$NON-NLS-1$
				sourceElement.addElement(dataElement);
				control.dispose();
				return;
			} else if (control instanceof Section
					|| control instanceof Composite) {

				if (handler instanceof StandaloneGroupControlHandler) {
					standaloneElement = new StandaloneGroupControlElement(id,
							control, form, toolkit, this);

					if ("grpEditionLinks_show".equals(id)) { //$NON-NLS-1$
						this.editionLinksElement = standaloneElement;
					} else if (!doCustom && !doSpecific && !doMore
							&& !doComplexRepeat && !doSource
							&& !doCollectSourceElements)
						omItems_provided.add(new OMItem(standaloneElement));

				} else if (handler instanceof RepeatControlHandler) {
					repElement = new RepeatControlElement(id, form, control,
							dataElement, toolkit, this);
					if (!doCustom && !doSpecific && !doMore && !doComplexRepeat
							&& !doSource && !doCollectSourceElements)
						omItems_provided.add(new OMItem(repElement));
				} else if (handler instanceof ComplexRepeatControlHandler) {
					complexRepElement = new ComplexRepeatControlElement(id,
							form, control, dataElement, toolkit, this);
					if (!doCustom && !doSpecific && !doMore && !doSource
							&& !doCollectSourceElements)
						omItems_provided.add(new OMItem(complexRepElement));
				} else if (handler instanceof SwitchControlHandler) {
					switchElement = new SwitchControlElement(id, form, control,
							dataElement, toolkit, this);
					if (!doCustom && !doSpecific && !doMore && !doComplexRepeat
							&& !doSource && !doCollectSourceElements)
						omItems_provided.add(new OMItem(switchElement));
				} else if (handler instanceof TextgridDateControlHandler) {
					tgDateElement = new TextgridDateControlElement(id, form,
							control, dataElement, toolkit, this);
					if (!doCustom && !doSpecific && !doMore && !doComplexRepeat
							&& !doSource && !doCollectSourceElements)
						omItems_provided.add(new OMItem(tgDateElement));

				} else if (handler instanceof SourceControlHandler) {
					sourceElement = new SourceControlElement(id, form, control,
							dataElement, toolkit, this);
					if (!doCustom && !doSpecific && !doMore
							&& !doCollectSourceElements)
						omItems_provided.add(new OMItem(sourceElement));
				} else if (handler instanceof MoreControlHandler) {
					moreElement = new MoreControlElement(id, form, control,
							dataElement, toolkit, this);
					doMore = true;
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							moreElement.getBody());
					doMore = false;

					if (doCollectSourceElements)
						collectingSourceElement.addControl(moreElement);
					else
						controlElements.add(moreElement);

					if (!doCustom && !doSpecific && !doCollectSourceElements)
						omItems_provided.add(new OMItem(moreElement));

				} else if (handler instanceof SpecificControlHandler) {
					specificElement = new SpecificControlElement(id, form,
							control, dataElement);
					omItems_specific.add(new OMItem(specificElement));
					controlElements.add(specificElement);
				} else if (handler instanceof CustomControlHandler) {
					customElement = new CustomControlElement(id, form, control,
							dataElement);
					omItems_custom.add(new OMItem(customElement));
					controlElements.add(customElement);
				}
			}

			if (id == null)
				throw new ParseException(NLS.bind(Messages.XLayout_elementMustContainAttribute, dataElement.getQualifiedName(), ATTR_ID));

			if (doStandaloneGroup) {
				if (standaloneElement != null) {
					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							handlerElement.getReadOnlyFlag(), null);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					standaloneElement.addAdvancedControl(advCon);
					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			} else if (doRepeat) {
				if (repElement != null) {
					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							handlerElement.getReadOnlyFlag(), null);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					repElement.addAdvancedControl(advCon);
					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			} else if (doComplexRepeat) {
				if (complexRepElement != null && !(control instanceof Section)) {
					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							handlerElement.getReadOnlyFlag(), null);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					complexRepElement.addAdvancedControl(advCon);
					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			} else if (doSwitch) {
				if (currentSwitchGrp != null && !(control instanceof Section)) {
					String category = dataElement.attributeValue(ATTR_CATEGORY);
					if ("switcher".equals(category)) { //$NON-NLS-1$
						if (control instanceof Button)
							currentSwitchGrp.addSwitcher((Button) control);
					} else {
						AdvancedControlElement advCon = new AdvancedControlElement(
								control,
								id,
								dataElement.attributeValue(ATTR_ELEMENT_NAME),
								dataElement
										.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
								handlerElement.getReadOnlyFlag(), null);

						String type = dataElement
								.attributeValue(ATTR_INPUT_TYPE);
						if (type != null && !"".equals(type)) //$NON-NLS-1$
							advCon.setInputType(type);

						String required = dataElement
								.attributeValue(ATTR_REQUIRED);
						if (required != null
								&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
							advCon.setRequired(true);

						if (elemLabel != null && !(control instanceof Label))
							advCon.setLabel(elemLabel);

						advCon.setAutoCompletionField(dataElement
								.attributeValue(XLayout.ATTR_AUTO_COMPLETION,
										null));

						currentSwitchGrp.addControl(advCon);
						pathControlsMap
								.put(currentPath
										+ dataElement
												.attributeValue(ATTR_ELEMENT_NAME),
										advCon);
					}
				}
			} else if (doSource) {
				return;
			} else if (doSpecific) {
				if (specificElement != null) {

					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							handlerElement.getReadOnlyFlag(), null);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					specificElement.addControlElement(advCon);
					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			} else if (/* !doRepeat && */doCustom) {
				if (customElement != null) {

					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							handlerElement.getReadOnlyFlag(), null);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					customElement.addControlElement(advCon);
					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			} else if (doCollectSourceElements) {
				if (control instanceof Text
						|| control instanceof org.eclipse.swt.widgets.List
						|| control instanceof Combo) {

					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							0, this);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					collectingSourceElement.addControl(advCon);
					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			} else {
				this.addControl(id, control);

				if (control instanceof Text
						|| control instanceof org.eclipse.swt.widgets.List
						|| control instanceof Combo) {

					AdvancedControlElement advCon = new AdvancedControlElement(
							control,
							id,
							dataElement.attributeValue(ATTR_ELEMENT_NAME),
							dataElement
									.attributeValue(ATTR_RELATED_ATTRIBUTE_NAME),
							0, this);

					String type = dataElement.attributeValue(ATTR_INPUT_TYPE);
					if (type != null && !"".equals(type)) //$NON-NLS-1$
						advCon.setInputType(type);

					String required = dataElement.attributeValue(ATTR_REQUIRED);
					if (required != null
							&& "true".equals(required.toLowerCase())) //$NON-NLS-1$
						advCon.setRequired(true);

					if (elemLabel != null && !(control instanceof Label))
						advCon.setLabel(elemLabel);

					advCon.setAutoCompletionField(dataElement.attributeValue(
							XLayout.ATTR_AUTO_COMPLETION, null));

					if (doMore) {
						if (moreElement != null)
							moreElement.addControlElement(advCon);
					} else {
						omItems_provided.add(new OMItem(advCon));
						controlElements.add(advCon);
					}

					pathControlsMap.put(
							currentPath
									+ dataElement
											.attributeValue(ATTR_ELEMENT_NAME),
							advCon);
				}
			}

			if ((control instanceof Section || (control instanceof Composite && (handler instanceof CustomControlHandler || handler instanceof SpecificControlHandler)))
					&& (dataElement.element(ELEMENT_TABLE) != null)) {

				if (handler instanceof StandaloneGroupControlHandler) {
					doStandaloneGroup = true;
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							standaloneElement.getBody());
					doStandaloneGroup = false;

					if (doComplexRepeat) {
						if (complexRepElement != null)
							complexRepElement.addComposite(standaloneElement);
					} else if (doSpecific) {
						if (specificElement != null)
							specificElement
									.addControlElement(standaloneElement);
					} else if (doCustom) {
						if (customElement != null)
							customElement.addControlElement(standaloneElement);
					} else if (doMore) {
						if (moreElement != null)
							moreElement.addControlElement(standaloneElement);
					} else {
						if (doCollectSourceElements)
							collectingSourceElement
									.addControl(standaloneElement);
						else
							controlElements.add(standaloneElement);
					}

				} else if (handler instanceof RepeatControlHandler) {
					doRepeat = true;
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							repElement.getBody());
					doRepeat = false;
					repElement.endCollecting();

					if (doComplexRepeat) {
						if (complexRepElement != null)
							complexRepElement.addComposite(repElement);
					} else if (doSpecific) {
						if (specificElement != null)
							specificElement.addControlElement(repElement);
					} else if (doCustom) {
						if (customElement != null)
							customElement.addControlElement(repElement);
					} else if (doMore) {
						if (moreElement != null)
							moreElement.addControlElement(repElement);
					} else {
						if (doCollectSourceElements)
							collectingSourceElement.addControl(repElement);
						else
							controlElements.add(repElement);
					}
				} else if (handler instanceof SourceControlHandler) {
					doSource = true;
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							sourceElement.getBody());
					doSource = false;

					if (doComplexRepeat) {
						if (complexRepElement != null) {
							complexRepElement.addComposite(sourceElement);
							complexRepElement.setContainsSourceElement(true);
						}
					} else if (doSpecific) {
						if (specificElement != null)
							specificElement.addControlElement(sourceElement);
					} else if (doCustom) {
						if (customElement != null)
							customElement.addControlElement(sourceElement);
					} else if (doMore) {
						if (moreElement != null)
							moreElement.addControlElement(sourceElement);
					} else {
						controlElements.add(sourceElement);
					}
				} else if (handler instanceof ComplexRepeatControlHandler) {
					doComplexRepeat = true;
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							complexRepElement.getBody());
					doComplexRepeat = false;
					complexRepElement.endCollecting();

					if (doSpecific) {
						if (specificElement != null)
							specificElement
									.addControlElement(complexRepElement);
					} else if (doCustom) {
						if (customElement != null)
							customElement.addControlElement(complexRepElement);
					} else if (doMore) {
						if (moreElement != null)
							moreElement.addControlElement(complexRepElement);
					} else {
						if (doCollectSourceElements)
							collectingSourceElement
									.addControl(complexRepElement);
						else
							controlElements.add(complexRepElement);
					}
				} else if (handler instanceof SwitchControlHandler) {
					doSwitch = true;
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							switchElement.getBody());
					doSwitch = false;

					switchElement.addFirstGroup(switchGrp1);
					switchElement.addSecondGroup(switchGrp2);

					switchGrp1 = null;
					switchGrp2 = null;

					if (doSpecific) {
						if (specificElement != null)
							specificElement.addControlElement(switchElement);
					} else if (doCustom) {
						if (customElement != null)
							customElement.addControlElement(switchElement);
					} else if (doMore) {
						if (moreElement != null)
							moreElement.addControlElement(switchElement);
					} else {
						if (doCollectSourceElements)
							collectingSourceElement.addControl(switchElement);
						else
							controlElements.add(switchElement);
					}
				} else if (handler instanceof SpecificControlHandler) {
					doSpecific = true;
					currentPath = "/object/" + specificElementName + "/"; //$NON-NLS-1$ //$NON-NLS-2$
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							specificElement.getBody());
					doSpecific = false;
					currentPath = "/object/generic/provided/"; //$NON-NLS-1$
				} else if (handler instanceof CustomControlHandler) {
					doCustom = true;
					currentPath = "/object/custom/templateEditor/"; //$NON-NLS-1$
					this.parseTable(dataElement.element(ELEMENT_TABLE),
							customElement.getBody());
					doCustom = false;
					currentPath = "/object/generic/provided/"; //$NON-NLS-1$
				}
				return;
			}

			if (handler instanceof TextgridDateControlHandler) {
				if (doSpecific) {
					if (specificElement != null)
						specificElement.addControlElement(tgDateElement);
				} else if (doCustom) {
					if (customElement != null)
						customElement.addControlElement(tgDateElement);
				} else if (doMore) {
					if (moreElement != null)
						moreElement.addControlElement(tgDateElement);
				} else {
					if (doCollectSourceElements)
						collectingSourceElement.addControl(tgDateElement);
					else
						controlElements.add(tgDateElement);
				}

				pathControlsMap
						.put(currentPath
								+ dataElement.attributeValue(ATTR_ELEMENT_NAME),
								tgDateElement);
			}

			// If it is a composite and contains a table element => read its
			// content
			if (control instanceof Composite
					&& (dataElement.element(ELEMENT_TABLE) != null)) {
				this.parseTable(dataElement.element(ELEMENT_TABLE),
						(Composite) control);
			}

			this.setGridData(control, dataElement.getParent());
			return;
		}

		// Print error if a deprecated namespace is used
		if (NAMESPACE_URI_DEPRECATED.equals(dataElement.getQName()
				.getNamespaceURI())) {
			throw new ParseException(NLS.bind(Messages.XLayout_namepsaceDeprecated, NAMESPACE_URI_DEPRECATED, NAMESPACE_URI));
		}

		// Cover case if dataElement is a table => recursive call
		if (ELEMENT_TABLE.equals(dataElement.getName())) {
			this.parseTable(dataElement, null);
			this.setGridData(this.parentsStack.peek(), dataElement.getParent());
			return;
		}

		// Handle if data element is an include
		if (ELEMENT_INCLUDE.equals(dataElement.getName())) {
			this.parseInclude(dataElement);
			return;
		}

		// Otherwise throw error because another element is not allowed here
		throw new ParseException(NLS.bind(Messages.XLayout_elementNotAllowed, dataElement.getNamespaceURI(), dataElement.getName()));
	}

	private String specificElementName = null;

	/**
	 * Sets the meta data in the Metadata editor.
	 * 
	 * @param root
	 *            the meta data OM-Element
	 */
	public void setOMElement(OMElement root, final String specificElementName,
			final String textGridUri) {

		if (root == null)
			return;

		this.specificElementName = specificElementName;

		// to know which elements the editor contains
		omReader_provided.add(omItems_provided);
		omReader_specific.add(omItems_specific);
		omReader_custom.add(omItems_custom);

		try {
			relationsOMElem = OMUtil.getElementWithName("relations", root, //$NON-NLS-1$
					TextGridObject.TEXTGRID_METADATA_NAMESPACE, "ns3"); //$NON-NLS-1$
			customOMElem = OMUtil.getElementWithName("custom", root, //$NON-NLS-1$
					TextGridObject.TEXTGRID_METADATA_NAMESPACE, "ns3"); //$NON-NLS-1$
			specificOMElem = OMUtil.getElementWithName(
					this.specificElementName, root,
					TextGridObject.TEXTGRID_METADATA_NAMESPACE, "ns3"); //$NON-NLS-1$
			genericOMElem = OMUtil.getElementWithName("generic", root, //$NON-NLS-1$
					TextGridObject.TEXTGRID_METADATA_NAMESPACE, "ns3"); //$NON-NLS-1$
			providedOMElem = OMUtil.getElementWithName("provided", //$NON-NLS-1$
					genericOMElem, TextGridObject.TEXTGRID_METADATA_NAMESPACE,
					"ns3"); //$NON-NLS-1$
			generatedOMElem = OMUtil.getElementWithName("generated", //$NON-NLS-1$
					genericOMElem, TextGridObject.TEXTGRID_METADATA_NAMESPACE,
					"ns3"); //$NON-NLS-1$
		} catch (JaxenException e) {
			Activator.handleError(e,
					Messages.XLayout_parseError);
			return;
		}

		OMElement templateEditorOMElem = null;
		if (customOMElem != null) {
			// FIXME: HACKE - cloneOMElement() s. TG-1580
			templateEditorOMElem = customOMElem.cloneOMElement()
					.getFirstChildWithName(
							new QName(customNS.getNamespaceURI(),
									templateEditorElementName, customNS
											.getPrefix()));
		}

		// System.err.println(descripOMElem);
		omReader_provided.setOMElements(providedOMElem, true);

		// if (specificOMElem != null)
		omReader_specific.setOMElements(specificOMElem, true);

		// if (templateEditorOMElem != null) {
		omReader_custom.setOMElements(templateEditorOMElem, false);
		// }

		if ("item".equals(specificElementName) && editionLinksElement != null //$NON-NLS-1$
				&& textGridUri != null) {

			for (Control c : editionLinksElement.getSWTControls()) {
				c.dispose();
			}

			if (!textGridUri.startsWith("textgrid:")) { //$NON-NLS-1$
				editionLinksElement.setExpanded(false);
				return;
			}

			editionLinksElement.setExpanded(true);

			ArrayList<EntryType> entryTypes = MetadataPath
					.addEditionEntriesToMetadataEditor(textGridUri);
			if (entryTypes == null) {
				Label l = new Label(editionLinksElement.getBody(), SWT.None);
				l.setForeground(l.getShell().getDisplay()
						.getSystemColor(SWT.COLOR_BLUE));
				l.setText(Messages.XLayout_noRelatedEditions_1
						+ Messages.XLayout_noRelatedEditions_2
						+ Messages.XLayout_noRelatedEditions_3);
				editionLinksElement
						.addAdvancedControl(new AdvancedControlElement(l, null,
								null, null, 0, this));
			} else {
				for (EntryType t : entryTypes) {
					final String uri = t.getTextgridUri();
					Link l = new Link(editionLinksElement.getBody(),
							SWT.UNDERLINE_LINK);
					l.setText("<a>" + t.getTitle() + " (" + uri + ")" + "</a>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					l.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {
							try {
								IWorkbench workBench = PlatformUI
										.getWorkbench();

								TGObjectReference object = new TGObjectReference(
										new URI(uri));

								workBench
										.showPerspective(
												"info.textgrid.lab.core.aggregations.ui.AggregationCompositionPerspective", //$NON-NLS-1$
												workBench
														.getActiveWorkbenchWindow());

								IEditorInput input = (IEditorInput) getAdapter(
										object, IEditorInput.class);

								workBench
										.getActiveWorkbenchWindow()
										.getActivePage()
										.openEditor(input,
												"info.textgrid.lab.core.aggregations.ui.editor"); //$NON-NLS-1$
							} catch (WorkbenchException e1) {
								Activator
										.handleError(e1,
												Messages.XLayout_editionInAggregationViewError);
							} catch (URISyntaxException e3) {
								Activator
										.handleError(e3,
												Messages.XLayout_editionInAggregationViewError);
							}
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
							// TODO Auto-generated method stub
						}
					});

					editionLinksElement
							.addAdvancedControl(new AdvancedControlElement(l,
									null, null, null, 0, this));
				}
			}
		}

		if (form != null)
			form.reflow(true);
	}

	/**
	 * Returns an OMElement which represents the meta data in the meta data
	 * editor.
	 * 
	 * @return the OMElement which represents the content in the meta editor
	 */
	public OMElement getOMElement() {

		this.createOMElemens();

		// to know which elements the editor contains
		omCreator_provided.add(omItems_provided);
		omCreator_provided.createOMElements(omFactory, providedOMElem, tgNS);

		omCreator_specific.add(omItems_specific);
		omCreator_specific.createOMElements(omFactory, specificOMElem, tgNS);

		omCreator_custom.add(omItems_custom);
		omCreator_custom
				.createOMElements(omFactory, templateEditorOM, customNS);

		return objectOMElem;
	}

	/**
	 * Creates the OMElement which represents the meta data.
	 */
	private void createOMElemens() {

		if (specificElementName == null || "".equals(specificElementName)) //$NON-NLS-1$
			return;

		objectOMElem = omFactory
				.createOMElement(TextGridObject.METADATA_ROOT_NAME);

		objectOMElem.declareNamespace(tgNS);
		objectOMElem.declareNamespace(
				"http://www.w3.org/1999/02/22-rdf-syntax-ns#", "ns2"); //$NON-NLS-1$ //$NON-NLS-2$

		genericOMElem = omFactory.createOMElement("generic", tgNS); //$NON-NLS-1$

		providedOMElem = omFactory.createOMElement("provided", tgNS); //$NON-NLS-1$
		genericOMElem.addChild(providedOMElem);

		if (generatedOMElem != null)
			genericOMElem.addChild(generatedOMElem);

		objectOMElem.addChild(genericOMElem);

		specificOMElem = omFactory.createOMElement(specificElementName, tgNS);
		objectOMElem.addChild(specificOMElem);

		if (customOMElem == null) {
			customOMElem = omFactory.createOMElement("custom", tgNS); //$NON-NLS-1$
			templateEditorOM = null;
		} else {
			templateEditorOM = customOMElem.getFirstChildWithName(new QName(
					customNS.getNamespaceURI(), templateEditorElementName));
		}

		if (templateEditorOM == null) {
			templateEditorOM = omFactory.createOMElement(
					templateEditorElementName, customNS);
			customOMElem.addChild(templateEditorOM);
		} else {
			// remove all elements under templateEditorOM
			for (Iterator<OMElement> iter = templateEditorOM.getChildElements(); iter
					.hasNext();) {
				OMElement om = iter.next();
				if (om != null)
					om.detach();
			}
		}

		objectOMElem.addChild(customOMElem);

		if (relationsOMElem != null)
			objectOMElem.addChild(relationsOMElem);
	}

	public CustomControlElement getCustomElement() {
		return this.customElement;
	}

	public SpecificControlElement getSpecificElement() {
		return specificElement;
	}

	public List<IControl> getAllControlElements() {
		return controlElements;
	}

	public IControl getControlElementById(String id) {
		for (IControl c : controlElements) {
			if (c.getId().equals(id))
				return c;
		}

		return null;
	}

	/**
	 * Determines whether the content of the editor was modified or not.
	 * 
	 * @return boolean
	 */
	public boolean isModified() {

		return modified;
	}

	/**
	 * Sets the editor as modified or not-modified.
	 * 
	 * @param modified
	 */
	public void setModified(boolean modified) {

		if (modified && !isEditable())
			return;

		this.modified = modified;
		if (metadataPart != null)
			metadataPart.validate();
	}

	/**
	 * Controls the visibility of the Metadata editor.
	 * 
	 * @param visible
	 *            true or false
	 */
	public void setVisible(boolean visible) {
		if (form != null)
			form.setVisible(visible);
	}

	/**
	 * Determines whether the Metadata editor is visible or hidden
	 * 
	 * @return boolean
	 */
	public boolean isVisible() {
		return (form != null) ? form.getVisible() : false;
	}

	/**
	 * is used metadata part editable?
	 * 
	 * @return
	 */
	public boolean isEditable() {
		if (metadataPart != null)
			return metadataPart.isEditable();

		return false;
	}

	/**
	 * Returns a list of all used controls in the Metadata editor.
	 * 
	 * @return list of Controls
	 */
	public List<Control> getAllControls() {

		List<Control> controls = new ArrayList<Control>();

		controls.addAll(this.controlsMap.values());

		for (IControl c : controlElements) {
			if (c instanceof IRepresentableComposite) {
				controls.addAll(((IRepresentableComposite) c).getSWTControls());
			} else if (c instanceof IRepresentableControl) {
				controls.add(((IRepresentableControl) c).getSWTControl());
			}
		}

		return controls;
	}

	/**
	 * Reads the formatting attributes and converts them into SWT layout
	 * information.
	 * 
	 * @param control
	 * @param tdElement
	 * 
	 */
	private void setGridData(Control control, Element tdElement) {

		GridData gridData = new GridData();

		int horizontalSpan = AttributeHelper.getAsInteger(tdElement,
				ATTR_COLSPAN, 1);
		if (horizontalSpan > 1)
			gridData.horizontalSpan = horizontalSpan;

		int verticalSpan = AttributeHelper.getAsInteger(tdElement,
				ATTR_ROWSPAN, 1);
		if (verticalSpan > 1)
			gridData.verticalSpan = verticalSpan;

		String valign = AttributeHelper.getAsString(tdElement, ATTR_VALIGN,
				null);
		if (valign != null)
			gridData.verticalAlignment = ConvertHelper
					.convertToSWTAlignment(valign);

		String align = AttributeHelper.getAsString(tdElement, ATTR_ALIGN, null);
		if (align != null)
			gridData.horizontalAlignment = ConvertHelper
					.convertToSWTAlignment(align);

		// Set height and width (Has only affect if no conflicts occured)
		// See also
		// http://www.eclipse.org/articles/Article-Understanding-Layouts/Understanding-Layouts.htm

		String attrHeight = AttributeHelper.getAsString(tdElement, ATTR_HEIGHT,
				null);
		String attrWidth = AttributeHelper.getAsString(tdElement, ATTR_WIDTH,
				null);

		if (attrHeight != null) {
			if ("100%".equals(attrHeight)) { //$NON-NLS-1$
				gridData.verticalAlignment = GridData.FILL;
				gridData.grabExcessVerticalSpace = true;
				gridData.minimumHeight = control.getParent().getSize().y;
			} else {
				int height = Integer.valueOf(attrHeight);
				gridData.heightHint = height;
				gridData.minimumHeight = height;
			}
		}

		if (attrWidth != null) {
			if ("100%".equals(attrWidth)) { //$NON-NLS-1$
				gridData.horizontalAlignment = GridData.FILL;
				gridData.grabExcessHorizontalSpace = true;
				gridData.minimumWidth = control.getParent().getSize().x;
			} else {
				int width = Integer.valueOf(attrWidth);
				gridData.widthHint = width;
				gridData.minimumWidth = width;
			}
		}

		control.setLayoutData(gridData);
	}

	/**
	 * Counts the maximum number of columns.
	 * 
	 * @param tableElement
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private int getNumberOfCols(Element tableElement) {

		List<Element> trElements = tableElement.elements(ELEMENT_TR);

		int numberOfCols = 0;

		for (Element trElement : trElements) {

			List<Element> tdElements = trElement.elements(ELEMENT_TD);
			int tmpNumberOfCols = 0;

			for (Element tdElement : tdElements) {
				tmpNumberOfCols = tmpNumberOfCols
						+ AttributeHelper.getAsInteger(tdElement, ATTR_COLSPAN,
								1);
			}

			if (tmpNumberOfCols > numberOfCols)
				numberOfCols = tmpNumberOfCols;
		}

		return numberOfCols;
	}

	/**
	 * Adds a control to the map of controls.
	 * 
	 * @param id
	 * @param control
	 * @throws ParseException
	 *             - If a control with the given name already exists.
	 */
	private void addControl(String id, Control control) throws ParseException {

		if (this.controlsMap.containsKey(id)) {
			throw new ParseException(NLS.bind("Could not create control with id '{0}'! A control with this id already exists in '{1}'!",id ,this.xLayoutSourceStack.peek().getHint())); //$NON-NLS-1$
		}

		this.controlsMap.put(id, control);
	}

	/**
	 * Returns true if a control with the given name exists.
	 * 
	 * @param id
	 * 
	 * @return boolean
	 */
	public boolean controlExists(String id) {

		return this.controlsMap.containsKey(id);
	}

	/**
	 * Maps a dom4j Element into a HandlerElement.
	 * 
	 * @param xmlElement
	 *            - The dom4j to map.
	 * @return The HandlerElement.
	 */
	@SuppressWarnings("unchecked")
	private HandlerElement createHandlerElement(Element xmlElement) {

		String name = xmlElement.getName();
		List<Attribute> attributes = xmlElement.attributes();
		Map<String, String> attrMap = new HashMap<String, String>();

		for (Attribute attr : attributes) {
			attrMap.put(attr.getName(), attr.getValue());
		}

		HandlerElement handlerElement = new HandlerElement(
				this.controlHandlerConfigs.get(name), attrMap);

		return handlerElement;
	}

	/**
	 * Registers the meta data's view
	 * 
	 * @param view
	 */
	public void registerMetadataPart(IMetadataPart part) {
		this.metadataPart = part;
	}

	/**
	 * To be called When the controls lost the focus.
	 */
	public void fireFocusEvent() {
		if (metadataPart != null)
			metadataPart.update();
	}

	/**
	 * @see {@link #setDecorationForControls()}
	 */
	public void updateDecorationForAgents() {
		// &&& - auto_com.setDecorationForAgents();
	}

	public Map<String, ICheckableElement> getPathControlsMap() {
		return pathControlsMap;
	}

	public Label getInfoLabel () {
		return infoLabel;
	}
	
	private boolean isDisposed = false;

	public boolean isDisposed() {
		return isDisposed;
	}

	/**
	 * Disposes all registered controls to release their resources and removes
	 * them from the layout.
	 */
	public void dispose() {
		try {
			Collection<Control> controls = this.controlsMap.values();
			for (Control control : controls) {
				control.dispose();
			}
			this.controlsMap.clear();

			Collection<Composite> parents = this.parentsStack
					.getAllPushedValues();
			for (Composite comp : parents) {
				comp.dispose();
			}
			this.parentsStack.clear();

			for (IControl c : controlElements)
				c.dispose();

			controlElements.clear();

			editionLinksElement = null;

		} finally {
			isDisposed = true;
		}
	}
}