package com.logabit.xlayout;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "com.logabit.xlayout.messages"; //$NON-NLS-1$
	public static String CachedDocumentFactory_parseError;
	public static String XLayout_editionInAggregationViewError;
	public static String XLayout_elementMustContainAttribute;
	public static String XLayout_elementNotAllowed;
	public static String XLayout_namepsaceDeprecated;
	public static String XLayout_noRelatedEditions_1;
	public static String XLayout_noRelatedEditions_2;
	public static String XLayout_noRelatedEditions_3;
	public static String XLayout_notTestedWarning;
	public static String XLayout_parseError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
