package com.logabit.xlayout;

import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.eclipse.osgi.util.NLS;

import com.logabit.xlayout.source.XLayoutSource;

public class CachedDocumentFactory {

	private static Map<Object, CachedEntity> documentCache = new HashMap<Object, CachedEntity>();

	public static Document getDocument(XLayoutSource source) {

		Document document = null;
		CachedEntity entity = documentCache.get(source.getCachingKey());

		if (entity == null) {

			// First call
			document = loadDocument(source);
			entity = new CachedEntity(document, source.lastModified());
			documentCache.put(source.getCachingKey(), entity);

		} else if (entity.needsParsing(source)) {

			// Update needed
			document = loadDocument(source);
			entity.setDocument(document, source.lastModified());

		} else {

			// Read from cache => No update needed
			document = entity.getDocument();
		}

		return document;
	}

	/**
	 * Only for testing purposes.
	 */
	static protected Map<Object, CachedEntity> getDocumentCache() {
		return documentCache;
	}

	/**
	 * Creates a dom4j Document from a given input stream.
	 * 
	 * @param is
	 *            The input stream to read from.
	 * @param inputHint
	 *            The hin the input stream comes from. If this input stream
	 *            comes from a file, this hint should usually indicate the
	 *            filename or complete path.
	 * @return A dom4j Document
	 * @throws ParseException
	 */
	private static Document loadDocument(XLayoutSource source)
			throws ParseException {

		SAXReader reader = new SAXReader();
		Document doc;

		try {
			doc = reader.read(source.toSAXInputSource());
		} catch (DocumentException e) {
			throw new ParseException(NLS.bind(Messages.CachedDocumentFactory_parseError, source.getHint(), e.getMessage()), e);
		}

		return doc;
	}

	public static void clearCache() {
		documentCache.clear();
	}

	static class CachedEntity {

		private Document document;
		private long lastModified;

		public CachedEntity(Document doc, long lastModified) {
			this.lastModified = lastModified;
			this.document = doc;
		}

		public void setDocument(Document document, long lastModified) {
			this.document = document;
			this.lastModified = lastModified;
		}

		public long getLastModified() {
			return this.lastModified;
		}

		public void setLastModified(long lastModified) {
			this.lastModified = lastModified;
		}

		public Document getDocument() {
			return this.document;
		}

		public boolean needsParsing(XLayoutSource source) {
			return source.lastModified() != lastModified;
		}
	}
}
