/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.source;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.eclipse.osgi.util.NLS;

import com.logabit.xlayout.ParseException;

/**
 * Resolves the given path and returns a {@link XLayoutSource}.
 * 
 * @author Stephan Niedermeier
 */
public class XLayoutSourceResolver {

	private ClassLoader resourceClassLoader;

	@SuppressWarnings("unchecked")
	private Class relativeClass;

	public XLayoutSourceResolver(ClassLoader resourceClassLoader) {
		this.resourceClassLoader = resourceClassLoader;
	}

	@SuppressWarnings("unchecked")
	public XLayoutSourceResolver(Class relativeClass) {
		this.resourceClassLoader = relativeClass.getClassLoader();
		this.relativeClass = relativeClass;
	}

	public XLayoutSourceResolver(Object relativeObject) {
		this(relativeObject.getClass());
	}

	/**
	 * Inspects the given uri string and tries to resolve the resource.
	 * Currently the following URI types are supported:
	 * <ul>
	 * <li>classpath:/ - This points to a resource within the current classpath</li>
	 * <li>./ - This points to a resource relatively to the current execution
	 * home folder</li>
	 * <li>All absoulte local file paths, like for example D:/myfile.txt</li>
	 * </ul>
	 * Note: Other URIs like URLs for example are currently not supported
	 * because of the lack of need.
	 * 
	 * @param uriString
	 *            - The string to resolve
	 * @return The XLayoutSource that points to the resolved resource.
	 */
	public XLayoutSource resolve(String uriString) {

		String hint;
		InputStream is;

		// Is it a classpath resource?
		if (uriString.startsWith("classpath:/")) { //$NON-NLS-1$

			hint = uriString.substring(11);
			is = this.resourceClassLoader.getResourceAsStream(hint);

			if (is == null)
				throw new ParseException(NLS.bind("Classpath resource not found: '{0}'!", hint)); //$NON-NLS-1$

			// Is it a relative resource?
		} else if (uriString.startsWith("./")) { //$NON-NLS-1$

			hint = uriString.substring(2);
			is = this.relativeClass.getResourceAsStream(hint);

			if (is == null)
				throw new ParseException(NLS.bind("Classpath resource not found: '{0}'!", hint)); //$NON-NLS-1$
			// Its a file path
		} else {

			File file = new File(uriString);
			try {
				is = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				throw new ParseException(NLS.bind("Could not resolve file : '{0}'!", file.getAbsolutePath()), e); //$NON-NLS-1$
			}

			return new XLayoutSource(file);
		}

		// TODO: Handle URIs + URLs

		return new XLayoutSource(hint, is);
	}
}
