/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.source;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

import org.xml.sax.InputSource;

/**
 * This class represents a XLayout definition source.
 * 
 * @author Stephan Niedermeier
 */
public class XLayoutSource {

	private String hint;
	private InputStream is;
	private Object cachingKey;
	private File file;

	public XLayoutSource(String hint, InputStream is) {
		this.hint = hint;
		this.is = is;
		this.setCachingKey(hint);
	}

	public XLayoutSource(URL url) {
		this.hint = url.getFile();
		try {
			this.file = new File(hint);
			this.is = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO: Introduce exception handling
			e.printStackTrace();
		}
		this.setCachingKey(file.getAbsolutePath());
	}

	public XLayoutSource(File file) {
		this.file = file;
		this.hint = file.getAbsolutePath();
		try {
			this.is = new FileInputStream(this.file);
		} catch (FileNotFoundException e) {
			// TODO: Introduce exception handling
			e.printStackTrace();
		}
		this.setCachingKey(file.getAbsolutePath());
	}

	/**
	 * Creates a SAX {@link InputSource} from the xlayout source.
	 * 
	 * @return The SAX InputSource
	 */
	public InputSource toSAXInputSource() {
		InputSource source = new InputSource(this.hint);
		source.setByteStream(is);
		return source;
	}

	/**
	 * Returns the hint for this source. The hint is usually a description of
	 * the underlying source, like the filename or the streams name for example.
	 * 
	 * @return The hint.
	 */
	public String getHint() {
		return hint;
	}

	/**
	 * Returns the input stream to read the xlayout definition.
	 * 
	 * @return
	 */
	public InputStream getInputStream() {
		return is;
	}

	/**
	 * Returns the last modified date of the underlying xlayout definition. This
	 * is important for example for caching strategies. Note: Currently only if
	 * the underlying source is a file, the lastModified date will be returned.
	 * Otherwise allways -1 will be returned.
	 * 
	 * @return The lastModified date of the underlying source or -1 if the
	 *         lastModified date couldn't be resolved.
	 */
	public long lastModified() {
		if (this.file != null)
			return this.file.lastModified();

		return -1;
	}

	/**
	 * Returns a unique Object which describes the underlying source.
	 * 
	 * @return
	 */
	public Object getCachingKey() {
		return this.cachingKey;
	}

	private void setCachingKey(Object cachingKey) {
		this.cachingKey = cachingKey;
	}
}
