/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout;

/**
 * This exception will be thrown if something goes wrong with the configuration
 * of a control handler.
 * 
 * @author Stephan Niedermeier
 * 
 */
public class ControlHandlerConfigurationException extends RuntimeException {

	private static final long serialVersionUID = 640739352651766551L;

	public ControlHandlerConfigurationException() {
		super();
	}

	public ControlHandlerConfigurationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ControlHandlerConfigurationException(String arg0) {
		super(arg0);
	}

	public ControlHandlerConfigurationException(Throwable arg0) {
		super(arg0);
	}
}
