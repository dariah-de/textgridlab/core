/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.utils;

import org.dom4j.Element;

/**
 * This helper class provides some utility methods to extract attribute values
 * from Dom4j objects and convert the into the appropriate datatype.
 * 
 * @author Stephan Niedermeier
 */
public class AttributeHelper {

	/**
	 * Looks for an attribute with given name in the given dom4j element and if
	 * found, converts it to integer. If no such attribute was found or the
	 * conversion to integer was failed, the given default value will be
	 * returned instead. The boolean conversion follows the rules defined by
	 * {@link ConvertHelper#convertToInteger(String, int)}.
	 * 
	 * @param elem
	 *            - The dom4j element wich contains the attributes.
	 * @param attributeName
	 *            - The name of the attribute to extract.
	 * @param defaultValue
	 *            - The default value to return if no attribute with given could
	 *            be found or the conversion to integer was failed.
	 * @return The integer representation of the attribute value.
	 */
	public static int getAsInteger(Element elem, String attributeName,
			int defaultValue) {

		String stringValue = elem.attributeValue(attributeName);
		return ConvertHelper.convertToInteger(stringValue, defaultValue);
	}

	/**
	 * Looks for an attribute with given name in the given dom4j element and if
	 * found, converts it to boolean. If no such attribute was found or the
	 * conversion to boolean was failed, the given default value will be
	 * returned instead. The boolean conversion follows the rules defined by
	 * {@link ConvertHelper#convertToBoolean(String, boolean)}.
	 * 
	 * @param elem
	 *            - The dom4j element wich contains the attributes.
	 * @param attributeName
	 *            - The name of the attribute to extract.
	 * @param defaultValue
	 *            - The default value to return if no attribute with given could
	 *            be found or the conversion to boolean was failed.
	 * @return The boolean representation of the attribute value.
	 */
	public static boolean getAsBoolean(Element elem, String attributeName,
			boolean defaultValue) {

		String stringValue = elem.attributeValue(attributeName);
		return ConvertHelper.convertToBoolean(stringValue, defaultValue);
	}

	/**
	 * Looks for an attribute with given name in the given dom4j element and if
	 * found, converts it to string. If no such attribute was found or the
	 * conversion to string was failed, the given default value will be returned
	 * instead. The string conversion follows the rules defined by
	 * {@link ConvertHelper#convertToString(String, String)}.
	 * 
	 * @param elem
	 *            - The dom4j element wich contains the attributes.
	 * @param attributeName
	 *            - The name of the attribute to extract.
	 * @param defaultValue
	 *            - The default value to return if no attribute with given could
	 *            be found or the conversion to string was failed.
	 * @return The string representation of the attribute value.
	 */
	public static String getAsString(Element elem, String attributeName,
			String defaultValue) {

		String stringValue = elem.attributeValue(attributeName);
		return ConvertHelper.convertToString(stringValue, defaultValue);
	}
}
