/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * This is a stack implementation which remembers all pushed values.
 * 
 * @author Stephan Niedermeier
 * 
 * @param <T>
 */
public class DurableStack<T> extends Stack<T> {

	private static final long serialVersionUID = 7294669325296635026L;

	public List<T> elementList = new ArrayList<T>();

	@Override
	public T push(T element) {
		this.elementList.add(element);
		return super.push(element);
	}

	public List<T> getAllPushedValues() {
		return Collections.unmodifiableList(this.elementList);
	}

	@Override
	public void clear() {
		super.clear();
		this.elementList.clear();
	}
}
