/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * This helper class contains utility methods to create instances and retrieve
 * information about objects and classes.
 * 
 * @author Stephan Niedermeier
 * 
 */
public class InstanceHelper {

	/**
	 * Creates an instance of the given fully qualified class name. Note: The
	 * according class must have a public default constructor!
	 * 
	 * @param className
	 *            The fully qualified class name.
	 * @return The instance of the given class name.
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("unchecked")
	public static Object createInstance(String className,
			ClassLoader classLoader) throws ClassNotFoundException,
			SecurityException, NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException {
		Class clazz = classLoader.loadClass(className);
		Constructor constructor = clazz.getConstructor();
		return constructor.newInstance(new Object[0]);
	}
}
