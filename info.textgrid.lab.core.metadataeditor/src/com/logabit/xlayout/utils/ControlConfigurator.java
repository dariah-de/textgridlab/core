/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.utils;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.widgets.Control;

import com.logabit.xlayout.handlers.HandlerElement;

/**
 * This class can be used to set configurations on controls.
 * 
 * @author Stephan Niedermeier
 */
public class ControlConfigurator {

	private static final String ATTR_ID = "id"; //$NON-NLS-1$
	private static final Object ATTR_STYLE = "style"; //$NON-NLS-1$

	/**
	 * Configures the given control by reflection. Loads all attributes (except
	 * id and style) and calls the method set<AttributeName> on the control. The
	 * argument is the attributes value. Note: Only string arguments are
	 * allowed!
	 * 
	 * @param control
	 * @param controlElement
	 */

	@SuppressWarnings("unchecked")
	public static void setConfigurationsByReflection(Control control,
			HandlerElement controlElement) {

		Iterator attrIt = controlElement.getAttributes().entrySet().iterator();

		while (attrIt.hasNext()) {
			Map.Entry entry = (Entry) attrIt.next();

			String attrName = (String) entry.getKey();

			if ((ATTR_ID.equals(attrName)) || (ATTR_STYLE.equals(attrName)))
				continue;

			invokeMethod(control, attrName, (String) entry.getValue());
		}
	}

	private static void invokeMethod(Control control, String attrName,
			String attrValue) {

		String methodName = attributeNameToMethodName(attrName);
		Method method;
		try {
			method = control.getClass().getMethod(methodName,
					new Class[] { String.class });
			method.invoke(control, new Object[] { attrValue });
		} catch (Exception e) {
			return; // Method doesn't exist or is not accessible
		}
	}

	private static String attributeNameToMethodName(String attrName) {

		StringBuffer buffer = new StringBuffer();
		buffer.append("set"); //$NON-NLS-1$
		buffer.append(attrName.substring(0, 1).toUpperCase());
		buffer.append(attrName.substring(1));
		return buffer.toString();
	}
}
