/**
 * Copyright 2006-2007 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logabit.xlayout.utils;

import org.eclipse.swt.SWT;

/**
 * This helper class provides some utility methods to convert from string into
 * other datatypes and datastructures.
 * 
 * @author Stephan Niedermeier, Andreas Schmid
 */
public class ConvertHelper {

	/**
	 * Looks for the given string. If this string value is null, the default
	 * value will be returned instead.
	 * 
	 * @param value
	 *            - The string value to look for.
	 * @param defaultValue
	 *            The default value to return if the given string is null.
	 * @return The string value if it is not empty. Otherwise the default value.
	 */
	public static String convertToString(String value, String defaultValue) {

		if (value == null)
			return defaultValue;

		return value;
	}

	/**
	 * Tries to convert the given string into a boolean representations. If the
	 * string is "true" or "yes" the returned boolean value will become true. If
	 * the given string is null or empty, the default value will be returned. In
	 * all other cases false will be returned.
	 * 
	 * @param value
	 *            - The string to convert.
	 * @param defaultValue
	 *            - The default value to return if the string is null or empty.
	 * @return - The boolean representation of the string value.
	 */
	public static boolean convertToBoolean(String value, boolean defaultValue) {

		if (value == null || "".equals(value)) //$NON-NLS-1$
			return defaultValue;

		if ("true".equals(value) || "yes".equals(value)) //$NON-NLS-1$ //$NON-NLS-2$
			return true;

		try {
			if (Integer.parseInt(value) > 0)
				return true;
		} finally {
		}

		return false;
	}

	/**
	 * Tries to convert the given string into a integer representation. If the
	 * given string is null or empty, the default value will be returned.
	 * 
	 * @param value
	 *            - The string value to convert to integer.
	 * @param defaultValue
	 *            - The default value to return if the string is null or empty.
	 * @return The integer representation of the string value.
	 */
	public static int convertToInteger(String value, int defaultValue) {

		int intValue = defaultValue;
		if (value != null && (!"".equals(value))) //$NON-NLS-1$
			intValue = Integer.parseInt(value);

		return intValue;
	}

	/**
	 * Converts the given string into a SWT style value. The string value must
	 * have the same name as the SWT style constant. For example "BORDER" would
	 * return the value of the {@link SWT#BORDER} constant. The string value
	 * will be treated case sensitive!
	 * 
	 * @param value
	 *            - The name of the SWT constant.
	 * @param defaultValue
	 *            - The default value to return if the given value couldn't
	 *            extract.
	 * @return The SWT constant value of the given string or the default value
	 *         if the value of the SWT constant couldn't be extracted.
	 */
	public static int convertToSWTStyle(String value, int defaultValue) {

		try {
			return SWT.class.getField(value).getInt(null);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * Converts a given textual alignment representation into its SWT integer
	 * representation. Mappings: left = SWT.BEGINNING right = SWT.RIGHT center =
	 * SWT.CENTER middle = SWT.CENTER top = SWT.BEGINNING bottom = SWT.END
	 * 
	 * If the given alignment string is not supported, SWT.BEGINNING will be
	 * returned.
	 * 
	 * @param align
	 * @return The SWT representation of the align string.
	 */
	public static int convertToSWTAlignment(String align) {

		if ("left".equals(align)) //$NON-NLS-1$
			return SWT.BEGINNING;

		if ("right".equals(align)) //$NON-NLS-1$
			return SWT.RIGHT;

		if ("center".equals(align)) //$NON-NLS-1$
			return SWT.CENTER;

		if ("middle".equals(align)) //$NON-NLS-1$
			return SWT.CENTER;

		if ("top".equals(align)) //$NON-NLS-1$
			return SWT.BEGINNING;

		// Deprecated!
		/*
		 * if("fill".equals(align)) return SWT.FILL;
		 */

		if ("bottom".equals(align)) //$NON-NLS-1$
			return SWT.END;

		return SWT.BEGINNING;
	}
}
