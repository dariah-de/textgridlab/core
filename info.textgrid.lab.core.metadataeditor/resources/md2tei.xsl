<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:md="http://textgrid.info/namespaces/metadata/core/2010" version="1.0">

    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" encoding="UTF-8"/>

    <xsl:key name="author" match="md:agent[@role='author'] | md:author" use="."/>
    <xsl:key name="sponsor" match="md:agent[@role='sponsor']" use="."/>
    <xsl:key name="funder" match="md:agent[@role='funder']" use="."/>
    <xsl:key name="contributor" match="md:agent[@role='contributor']" use="."/>

    <xsl:template match="/">
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <xsl:for-each select=".//md:generic/md:provided/md:title">
                            <title>
                                <xsl:value-of select="."/>
                            </title>
                        </xsl:for-each>
                        
                        <xsl:call-template name="convert-authorlike">
                            <xsl:with-param name="role">author</xsl:with-param>
                            <xsl:with-param name="canonical" select="true()"/>
                            
                        </xsl:call-template>
                        <xsl:call-template name="convert-authorlike">
                            <xsl:with-param name="role">sponsor</xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="convert-authorlike">
                            <xsl:with-param name="role">funder</xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="convert-authorlike">
                            <xsl:with-param name="role">contributor</xsl:with-param>
                            <xsl:with-param name="element">principal</xsl:with-param>
                        </xsl:call-template>
                        
                        <xsl:for-each select=".//md:agent[@role != 'author' and @role != 'sponser' and @role != 'funder' and @role != 'principal']">
                            <respStmt>
                                <resp>
                                    <xsl:value-of select="@role"/>
                                </resp>
                                <xsl:call-template name="generate-name"/>
                            </respStmt>
                        </xsl:for-each>

                        <xsl:for-each
                            select=".//md:object[1]/md:generic/md:generated/md:dataContributor">
                            <respStmt>
                                <resp>Data Contributor</resp>
                                <xsl:call-template name="generate-name"/>
                            </respStmt>
                        </xsl:for-each>
                        <xsl:for-each select=".//md:object[1]/md:item/md:rightsHolder">
                            <respStmt>
                                <resp>Rights Holder</resp>
                                <xsl:call-template name="generate-name"/>
                            </respStmt>
                        </xsl:for-each>
                    </titleStmt>

                    <publicationStmt>
                        <!-- doesn't seem useful: <idno type="pidType">work:generic/generated/pid</idno>  -->
                        <xsl:for-each select=".//md:object[1]/md:generic/md:generated/md:pid">
                        	<idno type="{@pidType}">
                        		<xsl:value-of select="."/>
                        	</idno>
                        </xsl:for-each>
                        <xsl:for-each select=".//md:generic/md:provided/md:identifer">
                            <idno type="{@type}">
                                <xsl:value-of select="."/>
                            </idno>
                        </xsl:for-each>
                        <idno type="textgrid">
                            <xsl:value-of
                                select=".//md:object[1]/md:generic/md:generated/md:textgridUri"/>
                        </idno>                        
                        <!-- not useful for unpublished stuff:
                    <xsl:for-each select=".//md:generated/md:availability">
                        <availability><ab><xsl:value-of select="."/></ab></availability>
                        </xsl:for-each>                    -->
                        <xsl:for-each select=".//md:edition/md:license">
                            <availability>
                                <ab>
                                    <xsl:if test="@licenseUri/text()">
                                        <xsl:attribute name="corresp">
                                            <xsl:value-of select="@licenseUri"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="."/>
                                    </xsl:if>
                                </ab>
                            </availability>
                        </xsl:for-each>
                    </publicationStmt>

                    <xsl:if test=".//md:generic/md:provided/md:notes[text()]">
                        <notesStmt>
                            <xsl:for-each select=".//md:generic/md:provided/md:notes[text()]">
                                <note>
                                    <xsl:value-of select="."/>
                                </note>
                            </xsl:for-each>
                        </notesStmt>
                    </xsl:if>

					<xsl:if test="not(.//md:edition/md:source)">
						<sourceDesc>
							<xsl:comment>To generate sourceDesc content, put your item into an edition
and fill out source information in that edition's metadata.</xsl:comment>
						</sourceDesc>
					</xsl:if>
                    <xsl:for-each select=".//md:edition/md:source">
                        <sourceDesc>
                            <xsl:for-each select="md:bibliographicCitation">

                                <!-- bibliographic-Variante -->
                                <biblFull>
                                    <titleStmt>
                                        <xsl:for-each select="md:editionTitle[text()]">
                                            <title>
                                                <xsl:value-of select="."/>
                                            </title>
                                        </xsl:for-each>
                                        <xsl:for-each select="md:author[text()]">
                                            <author>
                                                <xsl:if test="@id">
                                                  <xsl:attribute name="key">
                                                  <xsl:value-of select="@id"/>
                                                  </xsl:attribute>
                                                </xsl:if>
                                                <xsl:value-of select="."/>
                                            </author>
                                        </xsl:for-each>
                                        <xsl:for-each select="md:editor[text()]">
                                            <respStmt>
                                                <!-- <editor>...</editor>? -->
                                                <resp>editor</resp>
                                                <xsl:call-template name="generate-name"/>
                                            </respStmt>
                                        </xsl:for-each>
                                    </titleStmt>
                                    <xsl:if test="md:editionNo/text()">
                                        <editionStmt>
                                            <edition>
                                                <xsl:value-of select="md:editionNo"/>
                                            </edition>
                                        </editionStmt>
                                    </xsl:if>
                                    <xsl:if test="md:spage | md:epage">
                                        <extent><xsl:value-of select="md:spage"/>-<xsl:value-of
                                                select="md:epage"/></extent>
                                    </xsl:if>
                                    <publicationStmt>
                                        <xsl:if test="md:bibIdentifier/text()">
                                            <idno type="{md:bibidentifier/@type}">
                                                <xsl:value-of select="md:bibIdentifier"/>
                                            </idno>
                                        </xsl:if>
                                        <xsl:if test="md:volume/text()">
                                            <idno type="vol">
                                                <xsl:value-of select="md:volume"/>
                                            </idno>
                                        </xsl:if>
                                        <xsl:if test="md:issue/text()">
                                            <idno type="issue">
                                                <xsl:value-of select="md:issue"/>
                                            </idno>
                                        </xsl:if>
                                        <xsl:if test="md:dateOfPublication">
                                            <xsl:call-template name="generate-date">
                                                <xsl:with-param name="source"
                                                  select="md:dateOfPublication"/>
                                            </xsl:call-template>
                                        </xsl:if>
                                        <xsl:for-each select="md:placeOfPublication">
                                            <pubPlace>
                                                <xsl:value-of select="md:value"/>
                                            </pubPlace>
                                        </xsl:for-each>
                                        <xsl:for-each select="md:publisher">
                                            <publisher>
                                                <xsl:value-of select="."/>
                                            </publisher>
                                        </xsl:for-each>
                                    </publicationStmt>
                                    <xsl:if test="md:series/text()">
                                        <seriesStmt>
                                            <title>
                                                <xsl:value-of select="md:series"/>
                                            </title>
                                        </seriesStmt>
                                    </xsl:if>
                                </biblFull>
                            </xsl:for-each>

                            <xsl:for-each select="md:objectCitation">
                                <!-- objectCitation-Variante -->
                                <biblFull>
                                    <titleStmt>
                                        <xsl:for-each select="md:objectTitle">
                                            <title>
                                                <xsl:value-of select="."/>
                                            </title>
                                        </xsl:for-each>
                                        <xsl:for-each select="md:objectContributor">
                                            <respStmt>
                                                <resp>
                                                  <xsl:value-of select="@role"/>
                                                </resp>
                                                <xsl:call-template name="generate-name"/>
                                            </respStmt>
                                        </xsl:for-each>
                                    </titleStmt>
                                    <xsl:if test="md:objectIdentifier | md:objectDate">
                                        <publicationStmt>
                                            <xsl:for-each select="md:objectIdentifier">
                                                <idno type="{@type}">
                                                  <xsl:value-of select="."/>
                                                </idno>
                                            </xsl:for-each>
                                            <xsl:for-each select="md:objectDate">
                                                <xsl:call-template name="generate-date"/>
                                            </xsl:for-each>
                                        </publicationStmt>
                                    </xsl:if>
                                </biblFull>

                            </xsl:for-each>
                        </sourceDesc>
                    </xsl:for-each>

                </fileDesc>

                <xsl:for-each select=".//md:work">
                    <profileDesc corresp="{../md:generic/md:generated/md:textgridUri}">
                        <xsl:for-each select="md:dateOfCreation">
                            <creation>
                                <xsl:call-template name="generate-date"/>
                            </creation>
                        </xsl:for-each>
                        <textClass>
                            <!-- aus work/genre -->
                            <keywords
                                scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                                <!-- TODO -->
                                <xsl:for-each select="md:genre">
                                    <term>
                                        <xsl:value-of select="."/>
                                    </term>
                                </xsl:for-each>
                            </keywords>
                            <xsl:if test="md:type">
                                <keywords>
                                    <!-- TODO -->
                                    <xsl:for-each select="md:type">
                                        <term>
                                            <xsl:value-of select="."/>
                                        </term>
                                    </xsl:for-each>
                                </keywords>
                            </xsl:if>

                            <!-- TODO was machen wir nun mit den keywords? -->
    
                        </textClass>    
                    </profileDesc>                    
                </xsl:for-each>
                <xsl:for-each select=".//md:edition[md:language]">
                    <profileDesc corresp="{../md:generic/md:generated/md:textgridUri}">
                        <langUsage>
                            <xsl:for-each select="md:language">
                                <language ident="{.}"/>
                            </xsl:for-each>
                        </langUsage>
                    </profileDesc>
                </xsl:for-each>
            </teiHeader>
    </xsl:template>
    <xsl:template name="generate-name">
        <name>
            <xsl:if test="@id">
                <xsl:attribute name="key">
                    <xsl:value-of select="@id"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </name>
    </xsl:template>

    <xsl:template name="generate-date">
        <xsl:param name="source" select="."/>
        <date>
            <xsl:if test="$source/@date">
                <xsl:attribute name="when-iso">
                    <xsl:value-of select="$source/@date"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="$source/@notBefore">
                <xsl:attribute name="notBefore-iso">
                    <xsl:value-of select="$source/@notBefore"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="$source/@notAfter">
                <xsl:attribute name="notAfter-iso">
                    <xsl:value-of select="$source/@notAfter"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="$source"/>
        </date>
    </xsl:template>    


    <xsl:template name="convert-authorlike-old">
        <xsl:param name="role"/>
        <xsl:param name="element" select="$role"/>
        <xsl:param name="canonical" select="false()"/>
            <xsl:for-each select=".//md:agent[@role = $role]">
                <xsl:element name="{$element}">
                    <xsl:copy-of select="@*"/>
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:for-each>        
    </xsl:template>

	<!-- This template converts author-like elements that should not be converted to a respStmt.
	 -->
	<xsl:template name="convert-authorlike">
		<!-- The role from the TG metadata. There must be a key defined for that role. -->
		<xsl:param name="role" />
		<!-- The TEI element  -->
		<xsl:param name="element" select="$role"/>
		<xsl:param name="canonical" select="false()"/>

		<xsl:for-each select=".//md:agent[@role = $role][count(. | key($role, .)[1]) = 1]">
			<xsl:element name="{$element}">
				<xsl:if test="$canonical and (@id != '')">
					<xsl:attribute name="key">
						<xsl:value-of select="@id" />
					</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="." />
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
