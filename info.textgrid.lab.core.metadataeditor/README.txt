The Metadata editor uses the selection service to get the selected TextGrid-Object
and extracts the meta-data from it to display in the view.

The Main class to create the view is MetaDataView.java
in package info.textgrid.lab.core.metadataeditor.