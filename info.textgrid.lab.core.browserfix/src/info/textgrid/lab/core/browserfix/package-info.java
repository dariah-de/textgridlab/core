/**
 * Tries to fix various browser issues in TextGridLab, see TG-188. Use
 * {@link info.textgrid.lab.core.browserfix.TextGridLabBrowser#createBrowser(org.eclipse.swt.widgets.Composite)}
 * if you need a {@link org.eclipse.swt.browser.Browser} control.
 * 
 * At platform startup, this plugin creates a platform-specific configuration
 * directory for the embedded browser, this functionality can be found in the
 * class {@link info.textgrid.lab.core.browserfix.BrowserFixPlugin}.
 */
package info.textgrid.lab.core.browserfix;

