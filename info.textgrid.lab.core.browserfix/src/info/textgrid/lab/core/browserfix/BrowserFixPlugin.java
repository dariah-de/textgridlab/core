package info.textgrid.lab.core.browserfix;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.statushandlers.StatusManager.INotificationListener;
import org.osgi.framework.Bundle;

/**
 * <p>
 * This plugin creates the <code>~/.mozilla/eclipse</code> directory, if it does
 * not already exist. Otherwise, the browser component would not be able to
 * initialize the SSL engine, cf. <a
 * href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=206432">Eclipse Bug
 * #206432</a>. It also initializes the property pointing to the bundled
 * xulrunner, if applicable.
 * </p>
 * <p>
 * To use, simply add this plugin to your plugin's dependencies if you use
 * {@link org.eclipse.swt.browser.Browser}.
 * </p>
 * 
 * 
 * @see #createMozillaEclipseDirectory()
 */
public class BrowserFixPlugin implements IStartup {

	private static final String XULRUNNER_PROPERTY = "org.eclipse.swt.browser.XULRunnerPath";
	private static final String PLUGIN_ID = "info.textgrid.lab.core.browserfix";

	/**
	 * @return true if it has been created, false if not applicable
	 */
	public boolean createMozillaEclipseDirectory() {
		if (Platform.OS_WIN32.equals(Platform.getOS())) {
			String winAppVar = System.getenv("APPDATA");// Win XP,vista
			if (winAppVar != null) {
				File home = new File(winAppVar);
				File mozilla = new File(home, "Mozilla");
				File eclipse = new File(mozilla, "eclipse");
				if (!eclipse.exists())
					return eclipse.mkdirs();
			}
		} else if (Platform.OS_MACOSX.equals(Platform.getOS())) {
			String homeVar = System.getenv("HOME");// macosx
			if (homeVar != null) {
				File home = new File(homeVar);
				File lib = new File(home, "Library");
				File eclipse1 = new File(lib, "eclipse");
				boolean e1 = true;
				boolean e2 = true;
				if (!eclipse1.exists())
					e1 = eclipse1.mkdirs();
				// better twice...
				File mozilla = new File(lib, "Mozilla");
				File eclipse2 = new File(mozilla, "eclipse");
				if (!eclipse2.exists())
					e2 = eclipse2.mkdirs();
				return e1 && e2;
			}
		} else { // every other supported platform is a Unix
			String homeVar = System.getenv("HOME");
			if (homeVar != null) {
				File home = new File(homeVar);
				File mozilla = new File(home, ".mozilla");
				File eclipse = new File(mozilla, "eclipse");
				if (!eclipse.exists())
					return eclipse.mkdirs();
			}
		}
		return false;
	}

	/**
	 * Find the most generic bundle with a symbolic name derived from
	 * <var>baseName</var>.
	 * 
	 * Tries baseName, baseName.WS, basename.WS.OS, baseName.WS.OS.ARCH in this
	 * order until a non-null result has been found
	 * 
	 * @param baseName
	 *            the name to start from, e.g. "org.mozilla.xulrunner"
	 * @return the installed bundle or <code>null</code>, if absolutely none has
	 *         been found
	 */
	private static Bundle findBundle(String baseName) {
		StringBuilder bundleName = new StringBuilder(baseName);
		Bundle bundle = Platform.getBundle(bundleName.toString());

		if (bundle == null) {
			bundleName.append(".").append(Platform.getWS());
			bundle = Platform.getBundle(bundleName.toString());
		}
		if (bundle == null) {
			bundleName.append(".").append(Platform.getOS());
			bundle = Platform.getBundle(bundleName.toString());
		}
		if (bundle == null) {
			bundleName.append(".").append(Platform.getOSArch());
			bundle = Platform.getBundle(bundleName.toString());
		}
		return bundle;
	}

	// FIXME can we refactor this to the browser factory? Welcome screen doesn't
	// seem to be tackled anyway
	public void setXulRunnerPath() {
		Bundle xulRunnerPlugin = findBundle("org.mozilla.xulrunner");

		URL rawXulDirURL = null;
		if (xulRunnerPlugin != null) {
			File xulrunner;
			try {
				rawXulDirURL = FileLocator.find(xulRunnerPlugin, new Path(
						"xulrunner"), null);
				URL resolvedXulURL = FileLocator.resolve(rawXulDirURL);
				// this will of course contain space chars on windows, causing
				// URL.toURI() to fail
				// Java is so stupid.

				if ("file".equals(resolvedXulURL.getProtocol())) {
					xulrunner = new File(resolvedXulURL.getPath());
					String xulRunnerPath = xulrunner.getAbsolutePath();
					System.setProperty(XULRUNNER_PROPERTY, xulRunnerPath);
				} else {
					StatusManager.getManager().handle(new Status(IStatus.ERROR, PLUGIN_ID, "The xulrunner is installed under some strange non-file URL: " + resolvedXulURL.toString()));
				}
				// } catch (URISyntaxException e) {
				// StatusManager.getManager().handle(
				// new Status(IStatus.WARNING, PLUGIN_ID,
				// "Could not extract xulrunner path.", e));
			} catch (IOException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.WARNING, PLUGIN_ID,
								"Could not extract xulrunner path.", e));
			}
		}

		String property = System.getProperty(XULRUNNER_PROPERTY);
		StatusManager
				.getManager()
				.handle(
						new Status(
								IStatus.INFO,
								PLUGIN_ID,
								MessageFormat
										.format(
												"The XULRunnerPath property is now {0} (from XUL dir {1})",
												property, rawXulDirURL)));
//		System.out.println("XULRUNNERPATH:" + property);
	}

	public void earlyStartup() {
		createMozillaEclipseDirectory();
		setXulRunnerPath();
		INotificationListener listener = new BrowserStatusHandler();
		StatusManager.getManager().addListener(listener);

	}

}
