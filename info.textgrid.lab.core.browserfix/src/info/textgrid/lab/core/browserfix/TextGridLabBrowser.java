package info.textgrid.lab.core.browserfix;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;

/**
 * Wrapper for browser creation and management to work around a few browser bugs
 * (see TG-188).
 */
public final class TextGridLabBrowser {
	private static int SWTConstant;
	private static boolean checked = true;
	private static boolean foundOne = true;

	private static Browser checkBrowser(Composite parent) {
		Browser b;
//		IStatus status = new Status(IStatus.INFO,
//				"info.textgrid.lab.core.browserfix",
//				">>>testing SWT.MOZILLA-style Browser...");
//		StatusManager.getManager().handle(status, StatusManager.LOG);
		try {
			b = new Browser(parent, SWT.MOZILLA);
//			status = new Status(IStatus.OK,
//					"info.textgrid.lab.core.browserfix", ">>>succeeded.");
//			StatusManager.getManager().handle(status, StatusManager.LOG);
			SWTConstant = SWT.MOZILLA;
			foundOne = true;
			checked = true;
			return b;
		} catch (Error e) {
			//TODO this pops up instead of writing only to the log. why oh why?
//			StatusManager.getManager().handle(
//					new Status(IStatus.INFO,
//							"info.textgrid.lab.core.browserfix",
//							">>>failed! Trying now non-MOZILLA browser...", e),
//					StatusManager.LOG);
			try {
				b = new Browser(parent, SWT.NONE);
//				status = new Status(IStatus.OK,
//						"info.textgrid.lab.core.browserfix", ">>>succeeded.");
//				StatusManager.getManager().handle(status, StatusManager.LOG);
				SWTConstant = SWT.NONE;
				foundOne = true;
				checked = true;
				return b;
			} catch (Error e2) {
//				status = new Status(
//						IStatus.INFO,
//						"info.textgrid.lab.core.browserfix",
//						">>>failed! Could not find any browser compatible to this Lab!",
//						e2);
//				StatusManager.getManager().handle(status, StatusManager.LOG);
			}
		}
		checked = true;
		return null;
	}

	public static Browser createBrowser(Composite parent) {
		if (!checked) {
			return checkBrowser(parent);
		}

		if (foundOne) {
			return new Browser(parent, SWTConstant);
		} else {
			return null;
		}
	}
}
