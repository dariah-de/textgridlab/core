package info.textgrid.lab.core.browserfix;

import java.io.IOException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

public class BrowserStatusHandler implements StatusManager.INotificationListener {

	private static final String BROWSER_ERROR_1 = "There was an error initializing the Web Browser component which is required by TextGridLab. Basic functionality of the lab will not work. \n\n";
	private static final String BROWSER_ERROR_2 = "The detailed error message can be found in the log file.";
	private static final String TRY_INSTALL_XULRUNNER = "On Linux, the TextGridLab requires the package 'xulrunner-1.9.2' (xulrunner-2.0 will not work!). If you are on Ubuntu or a similar Debian-based distribution, we can try to launch your distribution's package management to install xulrunner-1.9.2. Otherwise you must do this manually before working with the TextGridLab.\n\n";


	public void handle(StatusAdapter statusAdapter) {
		System.out.println("Handled " + statusAdapter);
		IStatus status = statusAdapter.getStatus();
		if (status.getSeverity() == IStatus.ERROR && "org.eclipse.ui.core".equals(status.getPlugin())
				&& status.getException() != null
				&& status.getException() instanceof SWTError) {
			Throwable exception = status.getException();
			StackTraceElement[] stackTrace = exception.getStackTrace();
			for (StackTraceElement element : stackTrace) {
				if ("org.eclipse.swt.browser.Mozilla".equals(element.getClassName())) {
					showBrowserError(statusAdapter, exception);
					break;
				}
			}
		}
	}

	private void showBrowserError(StatusAdapter statusAdapter, Throwable exception) {
		String[] buttons;
		String message;
		if ("gtk".equals(SWT.getPlatform())) {
			message = BROWSER_ERROR_1 + TRY_INSTALL_XULRUNNER + BROWSER_ERROR_2;
			buttons = new String[] { "Exit TextGridLab", "Ignore this problem", "Try to install xulrunner-1.9.2" };
		} else {
			message = BROWSER_ERROR_1 + BROWSER_ERROR_2;
			buttons = new String[] { "Exit TextGridLab", "Ignore this problem" };
		}
		MessageDialog dialog = new MessageDialog(null, "Error configuring browser component", null, message, MessageDialog.ERROR, buttons, 0);
		int action = dialog.open();
		switch (action) {
		case 0:
			PlatformUI.getWorkbench().close();
			break;
		case 1:
			break;
		case 2:
			try {
				Runtime.getRuntime().exec("xdg-open apt://xulrunner-1.9.2");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void statusManagerNotified(int type, StatusAdapter[] adapters) {
		for (StatusAdapter statusAdapter : adapters) {
			handle(statusAdapter);
		}
	}

}
