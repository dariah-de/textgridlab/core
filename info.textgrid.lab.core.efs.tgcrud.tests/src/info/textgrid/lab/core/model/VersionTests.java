package info.textgrid.lab.core.model;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.impl.llom.util.AXIOMUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.junit.Test;

/**
 * This class tests various versioning issues.
 * 
 * @author vitt
 * 
 */
public class VersionTests {

	final static String PROJECT = "TGPR519";
	final static String CONTENT_TYPE = "text/xml";
	final static String METADATA = "<tg:tgObjectMetadata xmlns:tg=\"http://textgrid.info/namespaces/metadata/core/2008-07-24\"><tg:descriptive><tg:agent role=\"author\">Vitt, Thorsten</tg:agent><tg:title>TG-573-Test</tg:title><tg:date><approxDate xmlns=\"http://textgrid.info/namespaces/metadata/dateRange/2008-02-18\" approximateGregorianDate=\"2010\">2010</approxDate></tg:date><tg:type>test</tg:type></tg:descriptive><tg:administrative><tg:client><tg:project id=\""
		+ PROJECT
		+ "\">TG-573</tg:project><tg:format>"
		+ CONTENT_TYPE
		+ "</tg:format><tg:partNo>1</tg:partNo></tg:client></tg:administrative><tg:custom /></tg:tgObjectMetadata>";
	final static String DATA = "<test/>";
	final static URI EXISTING_URI = URI.create("textgrid:TG-573:TG-573-Test:20100510T114151:text%2Fxml:1");

	/**
	 * Tests whether we can open a new file immediately after creation.
	 */
	// @Test
	public void openCreatedImmediately() throws XMLStreamException, UnsupportedEncodingException, CoreException {
		@SuppressWarnings("deprecation")
		final TextGridObject object = TextGridObject.getNewObjectInstance(PROJECT, CONTENT_TYPE);
		object.setMetadataXML(AXIOMUtil.stringToOM(METADATA));
		final IFile file = getAdapter(object, IFile.class);
		file.setContents(new ByteArrayInputStream(DATA.getBytes("UTF-8")), IResource.FORCE, null);
		final IFile file2 = getAdapter(object, IFile.class);
		assertNotNull(file2.getContents());
	}

	/**
	 * Tests creating a new version by code.
	 */
	// @Test
	public void newVersion() throws UnsupportedEncodingException, CoreException {
		final TextGridObject object = TextGridObject.getInstance(EXISTING_URI, true);
		final TextGridObject newVersion = object.prepareNewVersion(null);
		final IFile oldFile = getAdapter(object, IFile.class);
		oldFile.setContents(new ByteArrayInputStream(DATA.getBytes("UTF-8")), IResource.FORCE, null);
		final IFile newFile = getAdapter(newVersion, IFile.class);
		assertNotNull(newFile.getContents());
	}

	@Test
	public void saveAsNewVersionTest() throws PartInitException, CrudServiceException, BadLocationException, InterruptedException {
		final TextGridObject object = TextGridObject.getInstance(EXISTING_URI, false);
		final IEditorInput input = getAdapter(object, IEditorInput.class);
		final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getWorkbenchWindow();
		final ITextEditor editor = (ITextEditor) workbenchWindow.getActivePage().openEditor(input,
		"org.eclipse.ui.DefaultTextEditor");
		assertNotNull(editor);

		final IDocument document = editor.getDocumentProvider().getDocument(input);
		document.replace(0, 0, ""); // Mark as modified

		final TextGridObject newVersion = object.prepareNewVersion(null);

		// SaveAsDialog dialog = new SaveAsDialog(workbenchWindow.getShell());
		// dialog.associateWithEditor(editor);
		// dialog.setBlockOnOpen(false);
		// dialog.open();
		// dialog.okPressed();

		if (editor != null) {
			editor.doSave(null);

			final IEditorInput newInput = getAdapter(newVersion, IEditorInput.class);
			if (editor instanceof IReusableEditor) {
				final IReusableEditor reusableEditor = (IReusableEditor) editor;
				reusableEditor.setInput(newInput);
			} else {
				final IWorkbenchPage page = editor.getEditorSite().getPage();
				page.closeEditor(editor, false);
				page.openEditor(newInput, editor.getSite().getId());
			}
		}

	}

}
