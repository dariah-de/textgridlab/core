package info.textgrid.lab.core.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;

import javax.xml.stream.XMLStreamException;

import junit.framework.Assert;

import org.apache.axiom.om.impl.llom.util.AXIOMUtil;
import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

public class VersionTest {

	final static String METADATA_WITH_VERSION = "<tg:tgObjectMetadata xmlns:tg=\"http://textgrid.info/namespaces/metadata/core/2008-07-24\"><tg:descriptive><tg:agent role=\"author\"></tg:agent><tg:title>Ahnung und Gegenwart</tg:title><tg:date><approxDate xmlns=\"http://textgrid.info/namespaces/metadata/dateRange/2008-02-18\" approximateGregorianDate=\"1234\"></approxDate></tg:date><tg:type>Roman</tg:type></tg:descriptive><tg:administrative><tg:client><tg:project id=\"TGPR516\">Versionierungsprogrammiersprint</tg:project><tg:format>text/xml</tg:format><tg:partNo>1</tg:partNo></tg:client><tg:middleware><tg:created>2010-05-04T11:24:15.030+01:00</tg:created><tg:lastModified>2010-05-04T11:24:15.030+01:00</tg:lastModified><tg:uri>textgrid:Versionierungsprogrammiersprint:Ahnung+und+Gegenwart:20100504T112415:text%2Fxml:1</tg:uri><tg:size>658419</tg:size><tg:owner id=\"stefan.funk@textgrid.de\"></tg:owner><tg:permissions>delegate publish delete read write</tg:permissions></tg:middleware></tg:administrative><tg:relational><tg:isVersionOf>textgrid:Mirjams+Sandk%C3%A4stchen:Ahnung+und+Gegenwart:20100219T161607:text%2Fxml:1</tg:isVersionOf></tg:relational><tg:custom /></tg:tgObjectMetadata>";
	final static String PREVIOUS_VERSION_URI = "textgrid:Mirjams+Sandk%C3%A4stchen:Ahnung+und+Gegenwart:20100219T161607:text%2Fxml:1";
	final static String METADATA_WITHOUT_VERSION = "<tg:tgObjectMetadata xmlns:tg=\"http://textgrid.info/namespaces/metadata/core/2008-07-24\"><tg:descriptive><tg:agent role=\"author\"></tg:agent><tg:title>Ahnung und Gegenwart</tg:title><tg:date><approxDate xmlns=\"http://textgrid.info/namespaces/metadata/dateRange/2008-02-18\" approximateGregorianDate=\"1234\"></approxDate></tg:date><tg:type>Roman</tg:type></tg:descriptive><tg:administrative><tg:client><tg:project id=\"TGPR516\">Versionierungsprogrammiersprint</tg:project><tg:format>text/xml</tg:format><tg:partNo>1</tg:partNo></tg:client><tg:middleware><tg:created>2010-05-04T11:24:15.030+01:00</tg:created><tg:lastModified>2010-05-04T11:24:15.030+01:00</tg:lastModified><tg:uri>textgrid:Versionierungsprogrammiersprint:Ahnung+und+Gegenwart:20100504T112416:text%2Fxml:1</tg:uri><tg:size>658419</tg:size><tg:owner id=\"stefan.funk@textgrid.de\"></tg:owner><tg:permissions>delegate publish delete read write</tg:permissions></tg:middleware></tg:administrative><tg:relational></tg:relational><tg:custom /></tg:tgObjectMetadata>";
	final static String METADATA_WITHOUT_RELATIONAL = "<tg:tgObjectMetadata xmlns:tg=\"http://textgrid.info/namespaces/metadata/core/2008-07-24\"><tg:descriptive><tg:agent role=\"author\"></tg:agent><tg:title>Ahnung und Gegenwart</tg:title><tg:date><approxDate xmlns=\"http://textgrid.info/namespaces/metadata/dateRange/2008-02-18\" approximateGregorianDate=\"1234\"></approxDate></tg:date><tg:type>Roman</tg:type></tg:descriptive><tg:administrative><tg:client><tg:project id=\"TGPR516\">Versionierungsprogrammiersprint</tg:project><tg:format>text/xml</tg:format><tg:partNo>1</tg:partNo></tg:client><tg:middleware><tg:created>2010-05-04T11:24:15.030+01:00</tg:created><tg:lastModified>2010-05-04T11:24:15.030+01:00</tg:lastModified><tg:uri>textgrid:Versionierungsprogrammiersprint:Ahnung+und+Gegenwart:20100504T112417:text%2Fxml:1</tg:uri><tg:size>658419</tg:size><tg:owner id=\"stefan.funk@textgrid.de\"></tg:owner><tg:permissions>delegate publish delete read write</tg:permissions></tg:middleware></tg:administrative><tg:custom /></tg:tgObjectMetadata>";

	@Test
	public void testSetPreviousVersion() throws CoreException, XMLStreamException {
		TextGridObject instance = TextGridObject.getInstance(AXIOMUtil.stringToOM(METADATA_WITHOUT_VERSION), true);
		TextGridObject previousVersion = instance.getPreviousVersion();
		Assert.assertNull(previousVersion);
		instance.setPreviousVersion(URI.create(PREVIOUS_VERSION_URI));
		previousVersion = instance.getPreviousVersion();
		assertEquals(PREVIOUS_VERSION_URI, previousVersion.getURI().toString());
		assertNotNull(instance.getMetadata());


		instance = TextGridObject.getInstance(AXIOMUtil.stringToOM(METADATA_WITHOUT_RELATIONAL), true);
		previousVersion = instance.getPreviousVersion();
		Assert.assertNull(previousVersion);
		instance.setPreviousVersion(URI.create(PREVIOUS_VERSION_URI));
		previousVersion = instance.getPreviousVersion();
		assertEquals(PREVIOUS_VERSION_URI, previousVersion.getURI().toString());
		assertNotNull(instance.getMetadata());

	}

	@Test
	public void testClearPreviousVersion() throws CoreException, XMLStreamException {
		final TextGridObject instance = TextGridObject.getInstance(AXIOMUtil.stringToOM(METADATA_WITH_VERSION), true);
		TextGridObject previousVersion = instance.getPreviousVersion();
		assertEquals(PREVIOUS_VERSION_URI, previousVersion.getURI().toString());

		instance.clearPreviousVersion();
		previousVersion = instance.getPreviousVersion();
		Assert.assertNull(previousVersion);
	}

	// @Test
	// public void testGetPreviousVersion() throws XMLStreamException,
	// CoreException {
	// TextGridObject instance =
	// TextGridObject.getInstance(AXIOMUtil.stringToOM(METADATA_WITH_VERSION),
	// true);
	// TextGridObject previousVersion = instance.getPreviousVersion();
	// assertEquals(PREVIOUS_VERSION_URI, previousVersion.getURI().toString());
	//
	// instance =
	// TextGridObject.getInstance(AXIOMUtil.stringToOM(METADATA_WITHOUT_VERSION),
	// true);
	// previousVersion = instance.getPreviousVersion();
	// Assert.assertNull(previousVersion);
	//
	// instance =
	// TextGridObject.getInstance(AXIOMUtil.stringToOM(METADATA_WITHOUT_RELATIONAL),
	// true);
	// previousVersion = instance.getPreviousVersion();
	// Assert.assertNull(previousVersion);
	//
	// }

}
