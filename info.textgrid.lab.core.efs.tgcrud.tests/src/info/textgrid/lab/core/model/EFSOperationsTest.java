package info.textgrid.lab.core.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.Date;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import junit.framework.TestCase;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMDocument;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.OperationCanceledException;

public class EFSOperationsTest extends TestCase {

	
	// textgridlab.org
	URI existingURI = URI
	.create("textgrid:Thorstens+Spielwiese:CWDS+-+Band+1%2C+aar:20090209T194341:text%2Fxml:1");
//			.create("textgrid:Campe+Band+1%2C+die+zweite:Die+Leiden+des+jungen+Werther+-+Zweyter+Theil:20080922T150606:xml%2Ftei:1");
	
	/** ingrid:8081 */
	// URI existingURI = URI
	// .create(
	// "textgrid:TGPR3:Die+Leiden+des+jungen+Werther+-+Zweyter+Theil:20080523T165844:xml%2Ftei:1"
	// );
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testEfsRead() throws Exception {
		TextGridObject tgo = TextGridObject.getInstance(existingURI, true);
//		assertEquals("Die Leiden des jungen Werther - Zweyter Theil", tgo.getTitle());
		assertEquals("CWDS - Band 1, aar", tgo.getTitle());
	}

	/**
	 * Verifies that we can read
	 * 
	 * @throws CoreException
	 * @throws IOException
	 */
	public void testEfsGetContents() throws CoreException, IOException {
		TextGridObject tgo = TextGridObject.getInstance(existingURI, false);
		IFile file = (IFile) tgo.getAdapter(IFile.class);
		assertTrue(file.exists());
		IProject project = file.getProject();
		assertTrue(project.exists());
		assertTrue(project.isOpen());

		/*
		 * InputStream crudStream = file.getContents(); byte[] inBuf = new
		 * byte[200]; crudStream.read(inBuf);
		 */
	}

	/**
	 * Verifies that we can create an object and read it back.
	 * 
	 * TODO split this in various methods
	 * 
	 * @throws CoreException
	 * @throws XMLStreamException
	 * @throws FactoryConfigurationError
	 * @throws IOException
	 * @throws OperationCanceledException
	 * @throws InterruptedException
	 */
	public void testEfsCreate() throws CoreException, XMLStreamException,
			FactoryConfigurationError, IOException, OperationCanceledException,
			InterruptedException {

		// Create a new, temporary object and the associated file
		TextGridObject tgo = TextGridObject.getNewObjectInstance("TGPR3",
				"xml/tei");
		IFile file = (IFile) tgo.getAdapter(IFile.class);

		// load & parse the metadata file & insert it to the TextGridObject
		InputStream metaInStream = getClass().getResourceAsStream(
				"/resources/cwds_000029_aalpuppe.meta.xml");
		XMLStreamReader reader = XMLInputFactory.newInstance()
				.createXMLStreamReader(metaInStream);
		StAXOMBuilder builder = new StAXOMBuilder(reader);
		OMElement rootElement = builder.getDocument().getOMDocumentElement();
		tgo.setMetadataXML(rootElement);

		// load the content stream and "save" it to the file
		InputStream contentInStream = getClass().getResourceAsStream(
				"/resources/cwds_000029_aalpuppe.xml");

		// FAIL with NPE → TGFileStores need a parent. Cf.
		file.setContents(contentInStream, IFile.FORCE | IFile.KEEP_HISTORY,
				null);

		// now the object should have an URI with the textgrid scheme
		URI uri = tgo.getURI();
		assertEquals("textgrid", uri.getScheme());

		// wait for background processing to be done – maybe the new file has
		// not been created yet (see TextGridObject#move)
		// tgo.joinWorkspaceJobs(null);

		IFile newFile = (IFile) tgo.getAdapter(IFile.class);

		// sing, uh, read it back
		InputStream crudContents = newFile.getContents();
		contentInStream.close();
		contentInStream = getClass().getResourceAsStream(
				"/resources/cwds_000029_aalpuppe.xml");
		int crudnbytes = 2048;
		int contentnbytes = crudnbytes;
		int total = 0;

		while (crudnbytes >= 0 && contentnbytes >= 0) {
			byte[] crudBuffer = new byte[2048];
			byte[] localBuffer = new byte[2048];
			crudnbytes = crudContents.read(crudBuffer);
			contentnbytes = contentInStream.read(localBuffer);
			assertEquals(contentnbytes, crudnbytes);
			assertTrue(Arrays.equals(localBuffer, crudBuffer));
			total += crudnbytes;
		}
		assertTrue("Read 0 bytes, but expected lots more", total > 0);

	}

	public void testEfsSetContents() throws CoreException, IOException,
			XMLStreamException, FactoryConfigurationError {
		TextGridObject tgo = TextGridObject.getInstance(existingURI, false);
		IFile file = (IFile) tgo.getAdapter(IFile.class);
		String marker = new Date().toString();
		InputStream inputStream = file.getContents();
		try {
			XMLStreamReader reader = XMLInputFactory.newInstance()
					.createXMLStreamReader(inputStream);
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMDocument doc = builder.getDocument();
			doc.getOMDocumentElement().addAttribute("modification", marker,
					doc.getOMDocumentElement().getDefaultNamespace());
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			doc.serialize(baos);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos
					.toByteArray());

			// Here is the update trigger. Unfortunately, setContents swallows
			// some exceptions.
			file.setContents(bais, true, true, null);
		} finally {
			inputStream.close();
		}

		InputStream verifyStream = file.getContents();
		try {
			XMLStreamReader reader = XMLInputFactory.newInstance()
					.createXMLStreamReader(verifyStream);
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMDocument doc = builder.getDocument();
			OMElement documentElement = doc.getOMDocumentElement();
			OMNamespace defaultNamespace = documentElement.getDefaultNamespace();
			String namespaceURI = (defaultNamespace != null) ? defaultNamespace.getNamespaceURI() : null;
			QName modQName = new QName(namespaceURI, "modification");
			OMAttribute attribute = documentElement.getAttribute(modQName);
			assertEquals(marker, attribute.getAttributeValue());
		} finally {
			verifyStream.close();
		}
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
