package info.textgrid.lab.core.model;

import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.ObjectFactory;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;

import java.io.StringWriter;
import java.net.URI;

import javax.xml.bind.JAXB;

import junit.framework.TestCase;

import org.apache.axiom.om.OMElement;


/**
 * Attention -- you should set the environment variable TEXTGRIDLAB_SID to a
 * valid session id for testuser@textgrid.de before running these tests.
 * 
 */
public class TextGridObjectTest extends TestCase {

	private OMElement docEl;
	private ObjectType otObject;

	private final URI existingURI = URI.create("textgrid:151f");

	@Override
	public void setUp() throws Exception {
		// create an simple instance of ObjectType
		final ObjectFactory factory = new ObjectFactory();
		otObject = factory.createObjectType();
		final GenericType generic = factory.createGenericType();
		otObject.setGeneric(generic);
		final ProvidedType provided = factory.createProvidedType();
		generic.setProvided(provided);
		provided.setFormat("Testformat");
		otObject.setCustom(factory.createObjectTypeCustom());
		otObject.setRelations(factory.createRelationType());
		otObject.getGeneric().getProvided().getTitle().add("Testtitel");
	}

	public void testGetInstance() {
		final TextGridObject tgo = null;
		try {
			TextGridObject.getInstance(existingURI, true);
			assertNotNull(tgo);
		} catch (final CrudServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// public void testGetInstanceNew() throws CrudServiceException {
	// try {
	// // final URI uri = new URI("textgrid", "test", "2");
	// final URI uri2 = new URI(
	// "textgrid:Thorstens+Spielwiese:CWDS+-+Band+1%2C+aar:20090209T194341:texty1%2Fxml:1");
	// final TextGridObject tgo = TextGridObject.getInstance(uri2, false);
	// tgo.setMetadata(otObject);
	// tgo.setSubmittable(true);
	// tgo.setComplete(true);
	// tgo.setTitleEmpty(false);
	// tgo.setIdnoTypeEmpty(false);
	// tgo.makeMetadataPersistent(null);
	// assertNotNull(tgo);
	// } catch (final URISyntaxException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (final CoreException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	// public void testGetLastModified() throws CoreException {
	// final TextGridObject tgo = TextGridObject.getInstance(otObject, false);
	// final Calendar expectedCal = Calendar.getInstance();
	// expectedCal.clear();
	// expectedCal.set(2007, 8, 27, 17, 42, 27);
	// assertEquals(expectedCal.getTimeInMillis(), tgo.getLastModified()
	// .getTimeInMillis());
	// }
	//
	// public void testGetUri() throws CoreException {
	// final TextGridObject tgo = TextGridObject.getInstance(docEl, false);
	// final URI uri = tgo.getURI();
	// assertNotNull(uri);
	// assertEquals("textgrid", uri.getScheme());
	// }
	//
	// public void testGetSize() throws CoreException {
	// final TextGridObject tgo = TextGridObject.getInstance(docEl, false);
	// assertEquals(136961, tgo.getSize());
	// }
	//
	// URI existingURI = URI
	// .create("textgrid:Thorstens+Spielwiese:CWDS+-+Band+1%2C+aar:20090209T194341:text%2Fxml:1");
	//
	// /**
	// * throws an ResourceException: Resource out of sync with TG-296 (but it
	// * shouldn't).
	// *
	// * @throws CoreException
	// */
	// public void testMakeMetadataPersistentSync() throws CoreException {
	// final TextGridObject object = TextGridObject.getInstance(existingURI,
	// true);
	// final IFile file = AdapterUtils.getAdapter(object, IFile.class);
	// final InputStream contents = file.getContents();
	// assertNotNull(
	// "file contents was null even before making the metadata persistent.",
	// contents);
	// object.makeMetadataPersistent();
	// final InputStream contents2 = file.getContents();
	// assertNotNull(
	// "file contents was ull after making the metadata persistent.",
	// contents2);
	// }
	//
	// public void testSetProject() throws RemoteException,
	// ProjectDoesNotExistException, CoreException {
	// final TextGridProject sourceProject = TextGridProject
	// .getProjectInstance("TGPR3");
	// final TextGridProject targetProject = TextGridProject
	// .getProjectInstance("TGPR14");
	// final TextGridObject object = TextGridObject.getNewObjectInstance(
	// sourceProject,
	// TGContentType.findMatchingContentType("text/plain"));
	// object.setProject(targetProject);
	// assertEquals(targetProject, object.getProjectInstance());
	// }

	public void testDeepCopy() {
		ObjectType otCopy;
		final StringWriter originalXML = new StringWriter();
		final StringWriter copyXML = new StringWriter();

		JAXB.marshal(otObject, originalXML);
		otCopy = TextGridObject.deepCopy(otObject);
		JAXB.marshal(otCopy, copyXML);

		assertEquals(originalXML.toString(), copyXML.toString());
	}
}
