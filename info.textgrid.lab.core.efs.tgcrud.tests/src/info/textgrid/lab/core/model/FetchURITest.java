package info.textgrid.lab.core.model;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.text.MessageFormat;

import junit.framework.Assert;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class FetchURITest {

	@SuppressWarnings("deprecation")	// oh, for mocking it's easier ...
	@Test
	public void testFetchURIs() throws CoreException, UnsupportedEncodingException {
		final TextGridObject o1 = TextGridObject.getNewObjectInstance("TGPR32", "text/xml");
		o1.setTitle("Test (from JUnit");
		final TextGridObject o2 = TextGridObject.getNewObjectInstance("TGPR32", "text/plain");
		o2.setTitle("Test as well (from JUnit");
		TextGridObject.fetchURIs(ImmutableList.of(o1, o2), null);
		final URI u1 = o1.getPreparedURI();
		final URI u2 = o2.getPreparedURI();
		System.out.println(MessageFormat.format("Prepared URIs: {0}, {1}", u1, u2));

		final IFile f1 = (IFile) o1.getAdapter(IFile.class);
		final IFile f2 = (IFile) o2.getAdapter(IFile.class);
		f1.setContents(new ByteArrayInputStream("<test/>".getBytes("UTF-8")), IResource.FORCE, new NullProgressMonitor());
		f2.setContents(new ByteArrayInputStream("test".getBytes("UTF-8")), IResource.FORCE, new NullProgressMonitor());

		Assert.assertEquals(u1, o1.getURI());
		Assert.assertEquals(u2, o2.getURI());
	}

}
