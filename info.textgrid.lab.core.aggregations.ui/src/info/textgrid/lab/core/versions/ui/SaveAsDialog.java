package info.textgrid.lab.core.versions.ui;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.metadataeditor.MetaDataSection;
import info.textgrid.lab.core.metadataeditor.ScrolledPageArea;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.utils.ProjectCombo;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * This is a dialogue to save a new version of the object open in an
 * 
 * @author vitt
 * 
 */
public class SaveAsDialog extends TitleAreaDialog {

	private MetaDataSection metaDataSection;
	private ProgressMonitorPart progressMonitorPart;
	protected IEditorPart editor;
	private ProjectCombo projectCombo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse
	 * .swt.widgets.Composite)
	 */
	protected Control createDialogArea(final Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		setTitle(Messages.SaveAsDialog_SaveAsNewVersion);
		getShell().setText(Messages.SaveAsDialog_SaveAs);
		setMessage(Messages.SaveAsDialog_IM_SaveNewVersion);

		ScrolledPageArea scrolledPageArea = new ScrolledPageArea(area);

		Composite composite = scrolledPageArea.getPageAreaComposite();
		ScrolledForm scrolledForm = scrolledPageArea.getPageAreaScrolledForm();

		GridDataFactory.fillDefaults().grab(true, true).hint(600, 400).applyTo(
				composite);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(scrolledForm);

		Group metadataGroup = new Group(composite, SWT.SHADOW_ETCHED_IN);
		// GridDataFactory.fillDefaults().grab(true, true).hint(600,
		// 500).applyTo(
		// metadataGroup);
		metadataGroup.setLayout(new GridLayout(1, false));
		metadataGroup.setText(Messages.SaveAsDialog_Metadata);

		metaDataSection = new MetaDataSection(metadataGroup, scrolledForm, null);
		metaDataSection.checkAllFields();

		projectCombo = new ProjectCombo(composite, LEVELS.EDITOR);
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL,
				SWT.TOP).applyTo(projectCombo.getControl());
		projectCombo
				.addSelectionChangedListener(new ISelectionChangedListener() {

					public void selectionChanged(SelectionChangedEvent event) {
						IStructuredSelection selection = (IStructuredSelection) event
								.getSelection();
						if (!selection.isEmpty() && newVersion != null) {
							newVersion.setProject((TextGridProject) selection
									.getFirstElement());
						}
					}
				});

		progressMonitorPart = new ProgressMonitorPart(area, null) {

			private ControlEnableState controlEnableState = null;

			public void beginTask(String name, int totalWork) {
				super.beginTask(name, totalWork);
				if (controlEnableState == null)
					controlEnableState = ControlEnableState.disable(parent); // ,
				// ImmutableList.of(getButton(CANCEL),
				// getTray(),
				// this));
			}

			public void done() {
				super.done();
				if (controlEnableState != null) {
					controlEnableState.restore();
					controlEnableState = null;
				}
			}
		};
		progressMonitorPart.setLayoutData(GridDataFactory.swtDefaults().align(
				SWT.FILL, SWT.BOTTOM).create());
		progressMonitorPart.setVisible(false);

		return composite;
	}

	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		getButton(OK).setText(Messages.SaveAsDialog_Save);
		progressMonitorPart.attachToCancelComponent(getButton(CANCEL));
		return contents;
	}

	public SaveAsDialog(Shell parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Runs the given runnable with the dialog's progress monitor, if available,
	 * or with the Workbench {@linkplain IWorkbench#getProgressService()
	 * progress service} otherwise.
	 * 
	 * @param runnable
	 *            the runnable that will be run.
	 * @param fork
	 *            if <code>false</code>, run in the UI thread, otherwise, the
	 *            implementation may fork to a separate thread
	 * @return A status indicating whether the runnable has completed
	 *         successfully. Note that if the runnable encapsulates a
	 *         {@link CoreException} the result will be a MultiStatus containing
	 *         the {@link CoreException}'s status.
	 */
	protected IStatus run(final IRunnableWithProgress runnable,
			final boolean fork) {
		try {
			if (progressMonitorPart != null
					&& !progressMonitorPart.isDisposed()) {
				progressMonitorPart.setVisible(true);
				try {
					ModalContext.run(runnable, fork, progressMonitorPart,
							getShell().getDisplay());
				} finally {
					progressMonitorPart.done();
					progressMonitorPart.setVisible(false);
				}
			} else {
				PlatformUI.getWorkbench().getProgressService().run(fork, true,
						runnable);
			}
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof CoreException) {
				CoreException c = (CoreException) e.getCause();
				return new MultiStatus(c.getStatus().getPlugin(), 0,
						new IStatus[] { c.getStatus() }, c.getMessage(), c);
			} else {
				return new Status(IStatus.ERROR,
						AggregationsUIPlugin.PLUGIN_ID, e.getCause()
								.getLocalizedMessage(), e);
			}
		} catch (InterruptedException e) {
			return Status.CANCEL_STATUS;
		}
		return Status.OK_STATUS;
	}

	private TextGridObject newVersion;
	private TextGridObject currentVersion;

	public void associateWithEditor(final IEditorPart editor) {
		final IStatus status = run(new IRunnableWithProgress() {

			public void run(final IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				final SubMonitor progress = SubMonitor.convert(monitor, 10);
				if (editor != null) {
					SaveAsDialog.this.editor = editor;
					final IEditorInput editorInput = editor.getEditorInput();
					currentVersion = AdapterUtils.getAdapter(
							editorInput, TextGridObject.class);
					progress.worked(1);
					
					if (currentVersion == null) {
						throw new InvocationTargetException(
								new CoreException(
										new Status(
												IStatus.ERROR,
												AggregationsUIPlugin.PLUGIN_ID,
												NLS.bind(Messages.SaveAsDialog_EM_NoTextGridObject, editorInput))));
					}
						

					// TODO the following stuff probably needs to be factored
					// out of here for support of non-editors like the metadata
					// editor etc.
					progress.setTaskName(NLS.bind(Messages.SaveAsDialog_PreparingNewVersion, currentVersion));
					newVersion = currentVersion.prepareNewVersion(progress.newChild(5));
					AdapterUtils.getAdapter(newVersion, IFile.class); // force
					// file
					// creation;
					progress.worked(1);

					if (metaDataSection != null)
						metaDataSection.setNewTGObject(newVersion, true);
					if (projectCombo != null)
						try {
							projectCombo.setProject(newVersion
									.getProjectInstance());
						} catch (CoreException e) {
							StatusManager.getManager().handle(e,
									AggregationsUIPlugin.PLUGIN_ID);
						}

					progress.worked(4);

					// TODO do we need to do more stuff?

				}
				progress.done();
			}
		}, false);
		if (!status.isOK())
			StatusManager.getManager().handle(status); // FIXME display this
		// somehow?
	}

	private static void log(String message, Object... arguments) {
		StatusManager.getManager().handle(
				new Status(IStatus.INFO, AggregationsUIPlugin.PLUGIN_ID,
						MessageFormat.format(message, arguments)));
	}

	/** public for testing purposes */
	public void okPressed() {
		metaDataSection.updateTextGridObject();
		newVersion.setProject(projectCombo.getProject());
		IStatus result = run(new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(
						Messages.SaveAsDialog_Saving, newVersion), 10);
				if (editor != null) {
					getAdapter(newVersion, IFile.class); // work around TG-573
					progress.worked(1);
					if (editor.isDirty())
						editor.doSave(progress.newChild(6));
					else
						// most editors don't save if nothing has been modified
						try {
							IFile currentFile = getAdapter(currentVersion, IFile.class);
							// following will actually save to the new file
							// as described in
							// TextGridObject.prepareNewVersion():
							currentFile.setContents(currentFile.getContents(), true, false, progress.newChild(6));
						} catch (CoreException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					log(Messages.SaveAsDialog_Saved, newVersion);

					IEditorInput newInput = null;
					
					progress.worked(1);
					if (editor instanceof IReusableEditor) {
						final IReusableEditor reusableEditor = (IReusableEditor) editor;
						newInput = getAdapter(newVersion,
								IEditorInput.class);
						reusableEditor.setInput(newInput);
						log("Set resusable editor {0} to open new input {1}", //$NON-NLS-1$
								editor, newInput);
					} else {
						final IWorkbenchPage page = editor.getEditorSite()
								.getPage();
						page.closeEditor(editor, false);
						try {
							newInput = getAdapter(newVersion,
									IEditorInput.class);
							page.openEditor(newInput, editor.getSite().getId());
							log(
									"Set resusable editor {0} to open new input {1}", //$NON-NLS-1$
									editor, newInput);
						} catch (final PartInitException e) {
							throw new InvocationTargetException(e);
						}
					}
				}
			}
		}, false);
		if (result.isOK())
			super.okPressed();
		else
			StatusManager.getManager().handle(result,
					StatusManager.LOG | StatusManager.SHOW);
	}
}
