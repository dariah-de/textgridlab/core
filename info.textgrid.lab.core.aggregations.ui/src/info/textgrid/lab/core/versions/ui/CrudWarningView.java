package info.textgrid.lab.core.versions.ui;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.core.swtutils.PendingLabelProvider;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;

import java.text.MessageFormat;
import java.util.EnumSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;


public class CrudWarningView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.textgrid.lab.crudwarning.views.CrudWarningView"; //$NON-NLS-1$

	private TextGridObjectTableViewer viewer;
	private ComboViewer projectCombo;
	private Button refreshButton;
	private Image refreshImage;
	private Action doubleClickAction;
	private TextGridProject selectedProject = null;
	private boolean comboContainsData = false;


	/**
	 * The constructor.
	 */
	public CrudWarningView() {
	}

	public void createPartControl(Composite parent) {
		GridLayout gridLayout = new GridLayout(1, false);
	    gridLayout.verticalSpacing = 10;
	    gridLayout.horizontalSpacing = 10;
	    parent.setLayout(gridLayout);
	    
	    Composite mdSearchGroup = new Composite(parent, SWT.LEFT);
		GridData gd2 = new GridData(SWT.FILL, GridData.CENTER, false, false);
		mdSearchGroup.setLayoutData(gd2);
		GridLayout gl = new GridLayout(3, false);
		mdSearchGroup.setLayout(gl);
    
	    Label projectLabel = new Label(mdSearchGroup, SWT.NULL);
	    projectLabel.setText(Messages.CrudWarningView_Project);
	    projectLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
	    projectCombo = new ComboViewer(mdSearchGroup, SWT.READ_ONLY);
	    projectCombo.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
	    final DeferredListContentProvider pcContentProvider = new DeferredListContentProvider();
	    projectCombo.setContentProvider(pcContentProvider);
	    projectCombo.setLabelProvider(new PendingLabelProvider());
	    projectCombo.setSorter(new ViewerSorter());	    
	    projectCombo.setInput(TextGridProjectRoot.getInstance(TextGridProjectRoot.LEVELS.EDITOR));
//	    projectCombo.addSelectionChangedListener(new ISelectionChangedListener() {
//			public void selectionChanged(SelectionChangedEvent event) {
//				refreshViewer(null);		
//			}	    	
//	    });
	    
	    pcContentProvider.addDoneListener(new IDoneListener() {
			public void loadDone(Viewer filledViewer) {
				if (!comboContainsData)
					projectCombo.add(Messages.CrudWarningView_AllProjects);
				comboContainsData = true;
				if (selectedProject != null) { 
					refreshViewer(selectedProject);
					selectedProject = null;
				}	
			}
		});
	    
	    
		refreshButton = new Button(mdSearchGroup, SWT.PUSH);
		refreshImage = AbstractUIPlugin.imageDescriptorFromPlugin(
				"info.textgrid.lab.core.aggregations.ui", "/icons/refresh.gif").createImage(); //$NON-NLS-1$ //$NON-NLS-2$
		refreshButton.setImage(refreshImage);
		refreshButton.setToolTipText(Messages.CrudWarningView_RefreshView);
		refreshButton.setLayoutData(new GridData(SWT.LEFT, GridData.CENTER, true, false));
		refreshButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				refreshViewer(((IStructuredSelection) projectCombo.getSelection()).getFirstElement());
			}
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});	

		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;

		viewer = new TextGridObjectTableViewer(parent, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL);
		
		viewer.setVisibleColumns(EnumSet.of(Column.TITLE, Column.PROJECT, Column.WARNING));
		viewer.getTable().setHeaderVisible(true);
		viewer.getControl().setLayoutData(gridData);
		viewer.setSorter(new ViewerSorter());

		makeActions();
		hookDoubleClickAction();
		hookContextMenu(viewer);
	}
	
	private void makeActions() {
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				if (obj instanceof TextGridObject) {
					try {
						TextGridObject object = (TextGridObject) obj;
						showMessage(MessageFormat.format(Messages.CrudWarningView_CrudWarning,
								object.getProject(), object.getTitle()), ((TextGridObject)obj).getCrudWarning());
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}	
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	
	private void hookContextMenu(final TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}
	
	private void showMessage(String title, String message) {
		MessageDialog.openWarning(
			viewer.getControl().getShell(),
			title,
			message);
	}

	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	public void refreshViewer(Object project) {
		
		if (!comboContainsData) {
			if (project instanceof TextGridProject)
				selectedProject = (TextGridProject)project;
			return;
		}
			
		SearchRequest searchRequest = new SearchRequest();
		
		if (project != null) {
			projectCombo.setSelection(new StructuredSelection(project), true);
		}
		
		IStructuredSelection spss = (IStructuredSelection) projectCombo.getSelection();
        TextGridProject comboProject = null;
		if (spss.getFirstElement() instanceof TextGridProject) {
			comboProject = (TextGridProject) spss.getFirstElement();
			if (comboProject != null) {
				searchRequest.setQueryMetadata("warning:\".\" and project:\"" + comboProject.getName() + "\"");
			}
		} else {
			searchRequest.setQueryMetadata("warning:\".\"");
		}
		viewer.setInput(searchRequest);		
	}
	
}
