package info.textgrid.lab.core.versions.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.versions.ui.messages"; //$NON-NLS-1$
	public static String CrudWarningView_AllProjects;
	public static String CrudWarningView_CrudWarning;
	public static String CrudWarningView_Project;
	public static String CrudWarningView_RefreshView;
	public static String DetachVersionHandler_DetachFailed;
	public static String DetachVersionHandler_Detaching;
	public static String DetachVersionHandler_DetachObjectsFromParent;
	public static String OpenVersionViewHandler_CouldNotOpenView;
	public static String SaveAsDialog_EM_NoTextGridObject;
	public static String SaveAsDialog_IM_SaveNewVersion;
	public static String SaveAsDialog_Metadata;
	public static String SaveAsDialog_PreparingNewVersion;
	public static String SaveAsDialog_Save;
	public static String SaveAsDialog_SaveAs;
	public static String SaveAsDialog_SaveAsNewVersion;
	public static String SaveAsDialog_Saved;
	public static String SaveAsDialog_Saving;
	public static String SaveNewVersionHandler_EM_NoOpenEditor;
	public static String VersionView_QueryingForChildren;
	public static String VersionView_RetrieveVersions;
	public static String VersionView_VersionsOfInProject;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
