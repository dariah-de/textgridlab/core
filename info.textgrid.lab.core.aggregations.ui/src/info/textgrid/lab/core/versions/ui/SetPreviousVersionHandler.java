package info.textgrid.lab.core.versions.ui;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapters;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

public class SetPreviousVersionHandler extends AbstractHandler implements IHandler {

	public class SetPreviousVersionDialog extends TitleAreaDialog {

		private TextGridObject[] objects;

		public SetPreviousVersionDialog(final TextGridObject[] objects, final Shell shell) {
			super(shell);
			this.objects = objects;
		}

		protected Control createDialogArea(Composite parent) {
			// TODO Auto-generated method stub
			return super.createDialogArea(parent);
		}

	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) currentSelection;
			TextGridObject[] objects = getAdapters(selection.toArray(), TextGridObject.class, false);
			new SetPreviousVersionDialog(objects, HandlerUtil.getActiveShell(event));

		}

		return null;
	}

}
