package info.textgrid.lab.core.versions.ui;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.search.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Handler for saving the object in the current editor as a new version.
 * Does currently only support editors in which a TextGridObject is edited.
 */
public class SaveNewVersionHandler extends AbstractHandler implements IHandler {

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (editor != null) {
		SaveAsDialog dialog = new SaveAsDialog(HandlerUtil.getActiveShell(event));
		dialog.setBlockOnOpen(false);
		dialog.open();
		dialog.associateWithEditor(editor);
		} else {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, AggregationsUIPlugin.PLUGIN_ID, Messages.SaveNewVersionHandler_EM_NoOpenEditor), StatusManager.SHOW);
		}

		// saveEditor(editor);
		return null;
	}

	/**
	 * @deprecated use {@link SaveAsDialog} instead.
	 */
	@Deprecated
	public static void saveEditor(final IEditorPart editor) {
		if (editor != null) {
			final IEditorInput editorInput = editor.getEditorInput();
			final TextGridObject object = getAdapter(editorInput, TextGridObject.class);
			final TextGridObject newVersion = object.prepareNewVersion(null);
			getAdapter(newVersion, IFile.class);
			editor.doSave(null);
			final IEditorInput newInput = getAdapter(newVersion, IEditorInput.class);
			if (editor instanceof IReusableEditor) {
				final IReusableEditor reusableEditor = (IReusableEditor) editor;
				reusableEditor.setInput(newInput);
			} else {
				final IWorkbenchPage page = editor.getEditorSite().getPage();
				page.closeEditor(editor, false);
				try {
					page.openEditor(newInput, editor.getSite().getId());
				} catch (final PartInitException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				}
			}
		}
	}
}