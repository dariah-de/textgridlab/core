package info.textgrid.lab.core.versions.ui;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapters;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.search.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;

public class DetachVersionHandler extends AbstractHandler implements IHandler {

	public class ClearVersionJob extends Job {

		private TextGridObject[] objects;

		public ClearVersionJob(final TextGridObject... objects) {
			super(NLS.bind(Messages.DetachVersionHandler_DetachObjectsFromParent, objects.length));
			this.objects = objects;
			setUser(true);
		}

		protected IStatus run(IProgressMonitor monitor) {
			SubMonitor progress = SubMonitor.convert(monitor, 10 * objects.length);
			MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, 0,
					Messages.DetachVersionHandler_DetachFailed, null);

			for (TextGridObject object : objects) {
				progress.subTask(NLS.bind(Messages.DetachVersionHandler_Detaching, object));
				if (progress.isCanceled())
					return Status.CANCEL_STATUS;

				object.clearPreviousVersion();
				progress.worked(1);
				try {
					object.makeMetadataPersistent(progress.newChild(9));
				} catch (CoreException e) {
					multiStatus.add(e.getStatus());
				}
			}

			progress.done();
			if (multiStatus.isOK())
				return Status.OK_STATUS;
			else
				return multiStatus;
		}

	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection instanceof IStructuredSelection) {
			TextGridObject[] objects = getAdapters(((IStructuredSelection) currentSelection).toArray(), TextGridObject.class, false);
			new ClearVersionJob(objects).schedule();
		}

		return null;
	}

}