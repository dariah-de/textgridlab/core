package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class AddSelectionHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
//		System.out.println("AddSelectionHandler");
		// get parameter
		String parameterContentType = event
				.getParameter(CommandIDS.COMMAND_PARAMETER_CONTENTTYPE_ID);
		// String parameterExtension =
		// event.getParameter(CommandIDS.COMMAND_PARAMETER_EXTENSION_ID);

		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;
		// TextGridObject tgo = acEditor.getAggregationRoot().getObject();
		ISelection selection = editor.getSite().getWorkbenchWindow()
				.getSelectionService().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();
		Aggregation aggregationRoot = acEditor.getAggregationRoot();
		Aggregation aggregation = aggregationRoot;
		Aggregation viewerRoot = new Aggregation(null,
				"InvisibleRootAggregation", null, acEditor.getTargetProject());
		aggregationRoot = new Aggregation(viewerRoot, "Aggregation", null,
				acEditor.getTargetProject());
		if (obj instanceof Aggregation) {
			aggregation = (Aggregation) obj;
		}

		Aggregation newAggregation = null;
		newAggregation = new Aggregation(aggregation, TGContentType
				.getContentType(parameterContentType).getDescription(), null,
				acEditor.getTargetProject());

		newAggregation.getObject().setContentType(
				TGContentType.getContentType(parameterContentType));
		// if (extension != null)
		// extension.setType(newAggregation);

		acEditor.setIsChanged(true);
		acEditor.getViewer().refresh(aggregation.getParent());
		acEditor.setIsBusy(false);
		acEditor.getViewer().setSelection(
				new StructuredSelection(newAggregation), true);
		acEditor.getViewer().getTree()
				.notifyListeners(SWT.MouseDoubleClick, new Event());

		return null;
	}

}
