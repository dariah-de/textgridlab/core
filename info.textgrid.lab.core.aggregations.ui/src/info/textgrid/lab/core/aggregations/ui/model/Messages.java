package info.textgrid.lab.core.aggregations.ui.model;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.aggregations.ui.model.messages"; //$NON-NLS-1$
	public static String Aggregation_EM_AggregationWithoutName;
	public static String Aggregation_EM_CouldNotConvertToIFile;
	public static String Aggregation_EM_CouldNotRetrieveTitle;
	public static String Aggregation_InvalidMetadata;
	public static String Aggregation_SavingAggregation;
	public static String SectionTreeDropAdapter_IM_NoMoveInSubAggregation;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
