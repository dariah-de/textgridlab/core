package info.textgrid.lab.core.aggregations.ui;



import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class ShowAggregationCompositionPerspectiveHandler extends AbstractHandler implements IHandler{
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			wb.getActiveWorkbenchWindow().getActivePage().setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"info.textgrid.lab.core.aggregations.ui.AggregationCompositionPerspective")); //$NON-NLS-1$

			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.core.aggregations.ui.AggregationCompositionPerspective", //$NON-NLS-1$
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, AggregationsUIPlugin.PLUGIN_ID,
				    Messages.ShowAggregationCompositionPerspectiveHandler_EM_CouldNotOpenPerspective, e);
			AggregationsUIPlugin.getDefault().getLog().log(status);
		}
		return null;
	}
}
