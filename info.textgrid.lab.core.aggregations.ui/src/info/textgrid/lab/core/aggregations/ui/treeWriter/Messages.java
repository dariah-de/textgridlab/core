package info.textgrid.lab.core.aggregations.ui.treeWriter;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.aggregations.ui.treeWriter.messages"; //$NON-NLS-1$
	public static String AggregationWriter_EM_NoURI;
	public static String AggregationWriter_EM_StrangeObject;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
