package info.textgrid.lab.core.aggregations.ui;

/*
 * Command IDs for AggregationComposerEditor
 */
public class CommandIDS {
	// parameterIDs
	public static final String COMMAND_PARAMETER_CONTENTTYPE_ID = "info.textgrid.lab.core.aggregations.ui.EditAggregation.parameter.contenttype";
	public static final String COMMAND_PARAMETER_EXTENSION_ID = "info.textgrid.lab.core.aggregations.ui.EditAggregation.parameter.extension";
	public static final String COMMAND_PARAMETER_REFERTO_ID = "info.textgrid.lab.core.aggregations.ui.ReferTo.parameter.referto";

	// Command IDs
	public static final String COMMAND_ADD_SELECTION_ID = "info.textgrid.lab.core.aggregations.ui.AddSelection";
	public static final String COMMAND_REMOVE_ITEM_ID = "info.textgrid.lab.core.aggregations.ui.RemoveItem";
	public static final String COMMAND_EXPAND_ALL_ID = "info.textgrid.lab.core.aggregations.ui.ExpandAll";
	public static final String COMMAND_COLLAPSE_ALL_ID = "info.textgrid.lab.core.aggregations.ui.CollapseAll";
	public static final String COMMAND_EXPAND_AGGREGATION_ID = "info.textgrid.lab.core.aggregations.ui.ExpandAggregation";
	public static final String COMMAND_COLLAPSE_AGGREGATION_ID = "info.textgrid.lab.core.aggregations.ui.CollapseAggregation";
	public static final String COMMAND_RENAME_ID = "info.textgrid.lab.core.aggregations.ui.RenameItem";
	public static final String COMMAND_REFER_TO_ID = "info.textgrid.lab.core.aggregations.ui.ReferTo";
	public static final String COMMAND_REVISION_REFER_TO_ID = "info.textgrid.lab.core.aggregations.ui.RevisionReferTo";

}