package info.textgrid.lab.core.aggregations.ui.model;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.aggregations.ui.treeWriter.AggregationWriter;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.namespaces.metadata.agent._2010.AgentRoleType;
import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;
import info.textgrid.namespaces.metadata.core._2010.EditionType.License;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

public class Aggregation {

	public static final TGContentType CONTENT_TYPE = TGContentType
			.getContentType("text/tg.aggregation+xml"); //$NON-NLS-1$

	private ArrayList<Object> children;

	private Aggregation parent;

	private String uuid; // FIXME do we need this?

	private TextGridObject object;

	protected ListenerList listeners;

	private boolean latest = true;

	private TextGridObject revision = null;

	/**
	 * Creates a new aggregation.
	 * 
	 * @param parent
	 *            the parent aggregation. If null, no aggregation.
	 * @param name
	 *            the name / title for the aggregation.
	 * @param object
	 *            the TextGridObject for the aggregation. If <code>null</code>,
	 *            create a new one
	 */
	public Aggregation(final Aggregation parent, final String name,
			final TextGridObject object, TextGridProject project) {
		this.children = new ArrayList<Object>();
		this.parent = parent;
		this.listeners = new ListenerList();

		if (object == null) {
			this.object = TextGridObject.getNewObjectInstance(project,
					CONTENT_TYPE, name);
		} else {
			this.object = object;
		}

		TextGridObject.addListener(new ITextGridObjectListener() {
			@Override
			public void textGridObjectChanged(Event event, TextGridObject object) {
				if (object == Aggregation.this.object)
					fireRename();
			}
		});

		if (parent != null)
			parent.addChild(this);

		this.uuid = UUID.randomUUID().toString();
	}

	/**
	 * Creates a new aggregation for (Aggregation)-circle-test only.
	 */
	public Aggregation(final Aggregation parent, final String name,
			final TextGridObject object, TextGridProject project, boolean test) {
		// TG-1750
		if (object == null) {
			this.object = TextGridObject.getNewObjectInstance(project,
					CONTENT_TYPE, name);
		} else {
			this.object = object;
		}
	}

	public void addListener(ISectionListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(ISectionListener listener) {
		this.listeners.remove(listener);
	}

	protected void fireAdd(Object added) {
		for (Object listener : this.listeners.getListeners()) {
			((ISectionListener) listener).add(new SectionEvent(added));
		}
	}

	protected void fireRemove(Object removed) {
		for (Object listener : this.listeners.getListeners()) {
			((ISectionListener) listener).remove(new SectionEvent(removed));
		}
	}

	protected void fireRename() {
		for (Object listener : this.listeners.getListeners()) {
			((ISectionListener) listener).rename(new SectionEvent(this));
		}
	}

	public void addChild(Object child) {
		if (child == null)
			throw new NullPointerException();
		if (!(child instanceof Aggregation) && !(child instanceof TGOentry))
			throw new IllegalArgumentException();
		children.add(child);
		fireAdd(child);
	}

	public Object[] getChildren() {
		return children.toArray(new Object[children.size()]);
	}

	public String getName() {
		try {
			return getObject().getTitle();
		} catch (CoreException e) {
			AggregationsUIPlugin.handleError(e,
					Messages.Aggregation_EM_CouldNotRetrieveTitle, object);
			return Messages.Aggregation_InvalidMetadata;
		}
	}

	public Aggregation getParent() {
		return parent;
	}

	/**
	 * Returns true if this Aggregation has a parent equal to the given
	 * Aggregation, and false otherwise.
	 */
	public boolean hasParent(Aggregation aggregation) {
		if (parent == null)
			return false;
		return parent.equals(aggregation) || parent.hasParent(aggregation);
	}

	public void removeChild(Object child) {
		children.remove(child);
		fireRemove(child);
	}

	public void setParent(Aggregation newParent) {
		if (parent != null)
			parent.removeChild(this);
		this.parent = newParent;
	}

	@Override
	public String toString() {
		return getName();
	}

	public void addBefore(Object before, Object newObject) {
		if (!(newObject instanceof Aggregation)
				&& !(newObject instanceof TGOentry))
			throw new IllegalArgumentException();
		if (before.equals(newObject)) {
			return;
		}
		int index = 0;

		if (newObject instanceof Aggregation) {
			if (((Aggregation) newObject).getParent() != null) {
				Aggregation oldParent = ((Aggregation) newObject).getParent();
				oldParent.removeChild(newObject);

			}
			((Aggregation) newObject).setParent(this);

		} else {
			if (((TGOentry) newObject).getParent() != null) {
				Aggregation oldParent = ((TGOentry) newObject).getParent();
				oldParent.removeChild(newObject);

			}
			((TGOentry) newObject).setParent(this);
		}
		for (int i = 0; i < children.size(); i++) {
			if (children.get(i).equals(before)) {
				index = i;
			}
		}
		children.add(index, newObject);
		fireAdd(newObject);
	}

	public void addAfter(Object after, Object newObject) {
		if (!(newObject instanceof Aggregation)
				&& !(newObject instanceof TGOentry))
			throw new IllegalArgumentException();
		if (after.equals(newObject)) {
			return;
		}
		int index = 0;

		if (newObject instanceof Aggregation) {
			if (((Aggregation) newObject).getParent() != null) {
				Aggregation oldParent = ((Aggregation) newObject).getParent();
				oldParent.removeChild(newObject);

			}
			((Aggregation) newObject).setParent(this);

		} else {
			if (((TGOentry) newObject).getParent() != null) {
				Aggregation oldParent = ((TGOentry) newObject).getParent();
				oldParent.removeChild(newObject);

			}
			((TGOentry) newObject).setParent(this);
		}
		for (int i = 0; i < children.size(); i++) {
			if (children.get(i).equals(after)) {
				index = i + 1;
			}
		}
		children.add(index, newObject);
		fireAdd(newObject);
	}

	public String getUuid() {
		return uuid;
	}

	public void setName(String name) {
		throw new IllegalStateException(
				Messages.Aggregation_EM_AggregationWithoutName);
		// fireRename();
	}

	public void delete() {
		this.parent.removeChild(this);

		this.setParent(null);

	}

	public void setObject(TextGridObject object) {
		this.object = object;
	}

	public TextGridObject getObject() {
		return object;
	}

	public void save(IProgressMonitor monitor, boolean saveAsRevision)
			throws XMLStreamException, CoreException {
		AggregationWriter.saveAggregationTree(this, saveAsRevision, null, monitor);
	}

	public void resetChildren() {
		this.children = new ArrayList<Object>();
		this.listeners = new ListenerList();
	}

	/**
	 * Sets the metadata of the represented TG object to the basic values of an
	 * edition.
	 */
	@Deprecated
	public void setObjectToEdition() {
		getObject().setContentType(
				AggregationComposerEditor.EDITION_CONTENT_TYPE);
		AgentType agent = new AgentType();
		PersonType person = RBACSession.getInstance().getPerson();
		agent.setId(person.getId());
		agent.setRole(AgentRoleType.EDITOR); // FIXME right role?
		agent.setValue(person.getValue());
		License license = new License();
		getObject().setEditionMetadata(null, agent, null, null, license);
		getObject().setTitle("Edition");
	}

	/**
	 * Sets the metadata of the represented TG object to the basic values of an
	 * item.
	 */
	@Deprecated
	public void setObjectToItem() {
		getObject().setContentType(
				AggregationComposerEditor.AGGREGATION_CONTENT_TYPE);
		PersonType rightsHolder = new PersonType();
		rightsHolder.setId(RBACSession.getInstance().getEPPN());
		rightsHolder.setValue(RBACSession.getInstance().getFullName(
				RBACSession.getInstance().getEPPN()));
		getObject().setItemMetadata(rightsHolder);
		getObject().setTitle("Aggregation");
	}

	/**
	 * Sets the metadata of the represented TG object to the basic values of an
	 * collection.
	 */
	@Deprecated
	public void setObjectToCollection() {
		getObject().setContentType(
				AggregationComposerEditor.COLLECTION_CONTENT_TYPE);
		PersonType collector = RBACSession.getInstance().getPerson();
		getObject().setCollectionMetadata(collector, null, null, null, null,
				null);
		getObject().setTitle("Collection");
	}

	public void setLatest(boolean latest) {
		this.latest = latest;
	}

	public boolean getLatest() {
		return latest;
	}

	public TextGridObject getRevision() {
		return revision;
	}

	public void setRevision(TextGridObject revision) {
		this.revision = revision;
	}
	
	/**
	 * Returns the URI that should be written into an aggregation aggregating
	 * this aggregation.
	 */
	public URI getURI() {
		if (getLatest())
			return URI.create(getObject().getLatestURI());
		else if (getRevision() != null)
			return getRevision().getURI();
		else
			return getObject().getURI();
	}
}
