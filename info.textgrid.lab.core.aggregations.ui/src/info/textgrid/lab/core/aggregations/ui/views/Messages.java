package info.textgrid.lab.core.aggregations.ui.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.aggregations.ui.views.messages"; //$NON-NLS-1$
	public static String AggregationComposerEditor__ResetComposer;
	public static String AggregationComposerEditor_AddingMembers;
	public static String AggregationComposerEditor_CollapseAggregation;
	public static String AggregationComposerEditor_CollapseAll;
	public static String AggregationComposerEditor_DisableWidgets;
	public static String AggregationComposerEditor_EM_AccessingAggregation;
	public static String AggregationComposerEditor_EM_AggregationAlreadyPublished;
	public static String AggregationComposerEditor_EM_AggregationContainsNoObject;
	public static String AggregationComposerEditor_EM_AggregationHasSuccessive;
	public static String AggregationComposerEditor_EM_ErrorWhenAccessing;
	public static String AggregationComposerEditor_EM_NoContentType;
	public static String AggregationComposerEditor_EM_NoProjectInstance;
	public static String AggregationComposerEditor_EM_NoValidEditorInput;
	public static String AggregationComposerEditor_EM_SerializeAggregation;
	public static String AggregationComposerEditor_EM_TitleRetrieve;
	public static String AggregationComposerEditor_EM_WhileRetrievingObjects;
	public static String AggregationComposerEditor_ExpandAggregation;
	public static String AggregationComposerEditor_ExpandAll;
	public static String AggregationComposerEditor_FetchAggregation;
	public static String AggregationComposerEditor_FetchingMembers;
	public static String AggregationComposerEditor_IM_Saving;
	public static String AggregationComposerEditor_IM_SelectOnlyOneItem;
	public static String AggregationComposerEditor_LoadAggregation;
	public static String AggregationComposerEditor_Menu_New;
	public static String AggregationComposerEditor_NewKindOfAggregation;
	public static String AggregationComposerEditor_QueryingForChildren;
	public static String AggregationComposerEditor_ReferToLastVersion;
	public static String AggregationComposerEditor_ReferToRevision;
	public static String AggregationComposerEditor_ReferToRevisionPoint;
	public static String AggregationComposerEditor_ReferToThisVersion;
	public static String AggregationComposerEditor_Remove;
	public static String AggregationComposerEditor_RemoveItem;
	public static String AggregationComposerEditor_RemoveItemWithChildren;
	public static String AggregationComposerEditor_RemoveWithChildren;
	public static String AggregationComposerEditor_RenameItem;
	public static String AggregationComposerEditor_SaveAggregation;
	public static String AggregationComposerEditor_Tooltip_NewAggregation;
	public static String AggregationComposerEditor_Tooltip_NewCollection;
	public static String AggregationComposerEditor_Tooltip_NewEdition;
	public static String ResponseDialog_ClickForResult;
	public static String ResponseDialog_PublishedTo;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
