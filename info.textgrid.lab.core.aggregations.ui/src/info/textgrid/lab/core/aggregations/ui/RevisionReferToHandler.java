package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.revisions.ui.RevisionSelectDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public class RevisionReferToHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;

		IStructuredSelection sel = (IStructuredSelection) acEditor.getViewer().getSelection();
		Object obj = sel.getFirstElement();

		if (obj != null) {
			RevisionSelectDialog posDialog = new RevisionSelectDialog(
					PlatformUI.getWorkbench().getDisplay().getActiveShell());
			posDialog.refreshDialog(obj);
			posDialog.open();
			acEditor.getViewer().refresh(obj, true);
			//line added by MH 2016-11-25 Bug #20225
			acEditor.setIsChanged(true);
		}

		return null;
	}

}
