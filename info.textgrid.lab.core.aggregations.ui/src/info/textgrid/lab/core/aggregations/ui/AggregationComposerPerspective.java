package info.textgrid.lab.core.aggregations.ui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

public class AggregationComposerPerspective implements IPerspectiveFactory{
	   
    public void createInitialLayout(IPageLayout layout) {
      	IWorkbench wb = PlatformUI.getWorkbench();
    	wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
    	
    	String editorArea = layout.getEditorArea();

    	IFolderLayout leftLayout = 
    		layout.createFolder("left", IPageLayout.LEFT, (float)0.30, editorArea);//$NON-NLS-1$
       	leftLayout.addView("info.textgrid.lab.core.metadataeditor.view"); //$NON-NLS-1$
 
    	IFolderLayout centerLayout= 
    		layout.createFolder("center", IPageLayout.LEFT, (float)0.40, editorArea);//$NON-NLS-1$
       	centerLayout.addView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
       	
       	layout.addPlaceholder("org.eclipse.ui.views.ProgressView", IPageLayout.RIGHT, 1.0f, layout.getEditorArea()); //$NON-NLS-1$
    	
    	layout.setEditorAreaVisible(true);   
    }
    
}
