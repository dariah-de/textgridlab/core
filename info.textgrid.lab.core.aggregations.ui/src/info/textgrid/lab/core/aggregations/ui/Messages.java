package info.textgrid.lab.core.aggregations.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.aggregations.ui.messages"; //$NON-NLS-1$
	public static String EditAggregationHandler_EM_CouldNotRetrieveContentType;
	public static String SaveNewAggregationRevisionHandler_EM_NoOpenEditor;
	public static String SaveNewAggregationRevisionHandler_IM_NotAllowedToSave;
	public static String SaveNewAggregationRevisionHandler_IM_SuccessiveRevision;
	public static String SaveNewAggregationRevisionHandler_SaveAsNewRevision;
	public static String ShowAggregationCompositionPerspectiveHandler_EM_CouldNotOpenPerspective;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
