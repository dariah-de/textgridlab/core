package info.textgrid.lab.core.aggregations.ui.model;

import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

public class SectionTreeDropAdapter extends ViewerDropAdapter {

	TreeViewer viewer;
	AggregationComposerEditor view;
	private String circleObj = "";
	private String operation = "";
	private ArrayList<Object> childList = new ArrayList<Object>();
	private ArrayList<URI> parentList = new ArrayList<URI>();

	public SectionTreeDropAdapter(TreeViewer viewer,
			AggregationComposerEditor view) {
		super(viewer);
		this.viewer = viewer;
		this.view = view;
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	@Override
	public boolean performDrop(Object data) {
		Boolean insertFlag = null;
		// set the proper drop target. As a TGOentry cannot have children
		// do not allow drops on a TGOentry
		Object target = getCurrentTarget();
		if (target == null) {
			target = view.getAggregationRoot();
		}

		if (target instanceof Aggregation) {
			insertFlag = insert(target, data, getCurrentLocation());
			if (insertFlag)
				view.setIsChanged(true);
			return insertFlag;
		} else if (target instanceof TGOentry) {
			int location = getCurrentLocation();
			if (location == LOCATION_ON) {
				return false;
			}
			insertFlag = insert(target, data, location);
			if (insertFlag)
				view.setIsChanged(true);
			return insertFlag;
		} else {
			return false;
		}
	}

	private boolean insert(Object target, Object data, int location) {

		if ((target == view.getAggregationRoot())
				&& ((location == LOCATION_BEFORE) || (location == LOCATION_AFTER))) {
			return false;
		}

		boolean moveAggregation = dataContainsParent(target, data);
		if (!moveAggregation) {
			MessageDialog
					.openError(
							Display.getCurrent().getActiveShell(),
							"Drag & Drop", //$NON-NLS-1$
							" Object"
									+ Messages.SectionTreeDropAdapter_IM_NoMoveInSubAggregation);
			return false;
		}

		if (data instanceof IStructuredSelection) {

			IStructuredSelection selection = (IStructuredSelection) data;
			TGOentry tgoEntry = null;

			view.setIsBusy(true);
			for (Iterator<?> it = selection.iterator(); it.hasNext();) {
				Object temp = it.next();

				if (temp instanceof TGOentry) {
					tgoEntry = (TGOentry) temp;
					if (((TGOentry) temp).isRestrictedTextGridObject())
						temp = ((TGOentry) temp).getRestrictedTgo();
					else
						temp = ((TGOentry) temp).getTgo();
				} else if (temp instanceof TGObjectReference) {
					temp = AdapterUtils.getAdapter(temp, TextGridObject.class);
				}

				if (temp instanceof TextGridObject
						|| temp instanceof RestrictedTextGridObject) {
					TextGridObject tgo = null;

					if (temp instanceof TextGridObject) {
						temp = new TGOentry(((TextGridObject) temp).getURI()
								.toString());
						tgo = ((TGOentry) temp).getTgo();
					} else {
						temp = new TGOentry(((RestrictedTextGridObject) temp));
					}

					if (target instanceof Aggregation) {
						Aggregation targetSection = (Aggregation) target;
						if (targetSection == view.getViewerRoot())
							targetSection = view.getAggregationRoot();
						try {
							if (tgo.getContentTypeID().contains("aggregation")) {
								Aggregation newAggregation = new Aggregation(
										targetSection, tgo.getTitle(), tgo,
										view.getTargetProject());
								view.importAggregation(newAggregation, tgo);
							} else if (temp instanceof TGOentry) {

								TGOentry entry = (TGOentry) temp;
								switch (location) {
								case LOCATION_BEFORE:
									targetSection.getParent().addBefore(
											targetSection, entry);
									break;

								case LOCATION_AFTER:
									targetSection.getParent().addAfter(
											targetSection, entry);
									break;
								default:
									entry.setParent(targetSection);
									targetSection.addChild(entry);
								}

							}
						} catch (CoreException e) {
							AggregationsUIPlugin.handleError(e,
									"SectionTreeDropAdapter Error");
						}
					} else if (target instanceof TGOentry) {
						TGOentry targetEntry = (TGOentry) target;
						if (temp instanceof Aggregation) {
							Aggregation aggregation = (Aggregation) temp;
							// cannot drop a Aggregation onto itself or a child
							if (aggregation.equals(targetEntry.getParent())
									|| targetEntry.getParent().hasParent(
											aggregation))
								return false;
							switch (location) {
							case LOCATION_BEFORE:
								targetEntry.getParent().addBefore(targetEntry,
										aggregation);
								break;

							case LOCATION_AFTER:
								targetEntry.getParent().addAfter(targetEntry,
										aggregation);
								break;

							default:
								return false;
							}
						} else if (temp instanceof TGOentry) {

							TGOentry entry = (TGOentry) temp;
							switch (location) {
							case LOCATION_BEFORE:
								targetEntry.getParent().addBefore(targetEntry,
										entry);
								break;

							case LOCATION_AFTER:
								targetEntry.getParent().addAfter(targetEntry,
										entry);
								break;

							default:
								return false;
							}

						}
					}
				}
				if (temp instanceof Aggregation) {
					// local aggregation movement
					if (target instanceof Aggregation) {
						if (location == LOCATION_BEFORE) {
							((Aggregation) temp)
									.setParent(((Aggregation) target)
											.getParent());
							((Aggregation) target).getParent().addBefore(
									target, temp);
						} else if (location == LOCATION_AFTER) {
							((Aggregation) temp)
									.setParent((Aggregation) target);
							((Aggregation) target).addAfter(target, temp);
						} else if (location == LOCATION_ON) { // TG-1847
							((Aggregation) temp)
									.setParent((Aggregation) target);
							((Aggregation) target).addChild(temp);
						}
					} else if (target instanceof TGOentry) {
						TGOentry targetEntry = (TGOentry) target;
						switch (location) {
						case LOCATION_BEFORE:
							targetEntry.getParent()
									.addBefore(targetEntry, temp);
							break;

						case LOCATION_AFTER:
							targetEntry.getParent().addAfter(targetEntry, temp);
							break;

						default:
							return false;
						}
					}
				}

				if (tgoEntry != null) {
					tgoEntry.delete();
					tgoEntry = null;
				}
			}
			view.getViewer().refresh();
			view.setIsBusy(false);
			return true;
		}
		return false;
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	@Override
	public boolean validateDrop(Object target, int op, TransferData type) {
		if (!LocalSelectionTransfer.getTransfer().isSupportedType(type))
			return false;
		
		Aggregation targetAggregation = null;
		
		if (target instanceof Aggregation)
			targetAggregation = (Aggregation) target;
		else if (target instanceof TGOentry)
			targetAggregation = ((TGOentry) target).getParent();
		
		if (targetAggregation == null)
			return false;
		
		Builder<URI> forbiddenURIBuilder = ImmutableSet.builder();
		while (targetAggregation != null) {
			forbiddenURIBuilder.add(targetAggregation.getURI());
			targetAggregation = targetAggregation.getParent();
		}
		ImmutableSet<URI> forbiddenURIs = forbiddenURIBuilder.build();
			
		
		ISelection selection = LocalSelectionTransfer.getTransfer().getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) selection;
			for (Object item : sel.toList()) {
				if (item instanceof Aggregation)
					if (forbiddenURIs.contains(((Aggregation) item).getURI()))
						return false;
			}
		}
		

		return true;
	}

	@Override
	public void dragOver(DropTargetEvent event) {

		// set the proper feedback...
		super.dragOver(event);

		// detecting the operating type (move/copy)
		getDropTarget(event);

		event.feedback = DND.FEEDBACK_EXPAND | DND.FEEDBACK_SCROLL;

		if (getCurrentLocation() == LOCATION_BEFORE) {
			event.feedback |= DND.FEEDBACK_INSERT_BEFORE;
		} else if (getCurrentLocation() == LOCATION_AFTER) {
			event.feedback |= DND.FEEDBACK_INSERT_AFTER;
		} else if (getCurrentLocation() == LOCATION_ON) {
			event.feedback |= DND.FEEDBACK_SELECT;
		}
	}

	/**
	 * A method for detecting the operating type
	 */
	public void getDropTarget(DropTargetEvent event) {

		switch (event.detail) {
		case DND.DROP_MOVE:
			operation = "moved";
			break;
		case DND.DROP_COPY:
			operation = "copied";
			break;
		// case DND.DROP_LINK:
		// operation = "linked";
		// break;
		// case DND.DROP_NONE:
		// operation = "disallowed";
		// break;
		default:
			operation = "unknown";
			break;
		}
	}

	/**
	 * It is checked, whether an aggregation in data (which should be moved) is
	 * a parent/child object of target (No circle).
	 * 
	 * @param target
	 * @param data
	 * @return Info of circle
	 */
	private boolean dataContainsParent(Object target, Object data) {
		// changed for TG-1750

		
		if (!(target instanceof Aggregation))
			return true;

		if (data instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) data;

			// retrieve parents of target.
			// ArrayList<URI> parentList = new ArrayList<URI>();
			this.parentList.clear();
			Aggregation parent = ((Aggregation) target).getParent();
			while (parent != null) {
				parentList.add(parent.getObject().getURI());
				parent = parent.getParent();
			}
			parentList.add(view.getAggregationRoot().getObject().getURI());

			for (Iterator<?> it = selection.iterator(); it.hasNext();) {
				Object temp = it.next();

				if (temp instanceof TGOentry)
					return true;

				URI tgUri;

				if (temp instanceof TGObjectReference) {
					tgUri = ((TGObjectReference) temp).getTgo().getURI();
				} else
					tgUri = ((Aggregation) temp).getObject().getURI();

				String tgObjName = "";
				TextGridObject tgo = null;
				try {
					if (temp instanceof TGObjectReference) {
						tgObjName = ((TGObjectReference) temp).getTgo()
								.getNameCandidate();
						tgo = ((TGObjectReference) temp).getTgo();
					} else
						tgObjName = ((Aggregation) temp).getObject()
								.getNameCandidate();
				} catch (CoreException e) {
					AggregationsUIPlugin.handleError(e,
							"SectionTreeDropAdapter Error");
				}

				// ###############################################################
				// proof child's
				if (target instanceof Aggregation) {
					Aggregation targetSection = (Aggregation) target;
					if (targetSection == view.getViewerRoot())
						targetSection = view.getAggregationRoot();
					try {
						if (tgo.getContentTypeID().contains("aggregation")) {
							Aggregation newAggregation = new Aggregation(
									targetSection, tgo.getTitle(), tgo,
									view.getTargetProject(), true);
							// ################
							view.resetChildList();
							// childList =
							// view.proofImportAggregation(newAggregation, tgo);
							view.proofImportAggregation(newAggregation, tgo);
							childList = view.getChildList();
							childList.add(tgo.getURI());
							// System.out.println("childList :  "+childList.toString());
							// System.out.println("parentList:  "+this.parentList.toString());
							for (Iterator<URI> iterator = parentList.iterator(); iterator
									.hasNext();) {
								Object object = iterator.next();

								if (childList.contains(object)) {
									// Zyklus identified
									return false;
								}
							}
						}
					} catch (CoreException e) {
						AggregationsUIPlugin.handleError(e,
								"SectionTreeDropAdapter Error");
					} catch (Exception e) {
						AggregationsUIPlugin.handleError(e,
								"SectionTreeDropAdapter Error");
					}
				}

				// ###############################################################

				// if (parentList.contains(temp))
				// return (Aggregation) temp;
				for (Iterator<URI> iterator = parentList.iterator(); iterator
						.hasNext();) {
					URI uri = iterator.next();

					if (uri.equals(tgUri)
							|| view.getAggregationRoot().getObject().getURI()
									.equals(tgUri)
							|| ((Aggregation) target).getObject().getURI()
									.equals(tgUri)) {
						//
						this.circleObj = tgObjName + " (" + tgUri + ")";
						return false;
					}
				}
			}
		}
		return true;
	}
}
