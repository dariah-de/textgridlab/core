package info.textgrid.lab.core.aggregations.ui.treeWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.Lists;

import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.newsearch.SearchRequest.EndPoint;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;

public class AggregationWriter {

	private static final String ORE_NS = "http://www.openarchives.org/ore/terms/"; //$NON-NLS-1$
	public static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"; //$NON-NLS-1$

	/**
	 * Recursively saves the aggregations starting at the given tree.
	 * 
	 * 
	 * @param root The aggregation at which to start saving.
	 * @param asRevision if true, save as new revision
	 * @param knownParents a list of known ancestors of the given root, or <code>null</code> to ask TG-search
	 * @param monitor progress monitor
	 * @throws CoreException when TGO handling fails
	 * @throws XMLStreamException 
	 */
	public static void saveAggregationTree(final Aggregation root, final boolean asRevision, List<URI> knownParents, final IProgressMonitor monitor) throws CoreException, XMLStreamException {
		/*
		 *    TG-search ancestor query (optional)
		 * +  potential time for subaggregations (* children)
		 * +  saving this
		 */
		final SubMonitor progress = SubMonitor.convert(monitor, 100 + 100 * root.getChildren().length + 100);

		if (knownParents == null) {
			// determine potential parents if they haven't been passed to us.
			if (root.getObject().isNew())
				knownParents = Lists.newArrayList();
			else {
				TextgridUris parents = new SearchRequest(EndPoint.STANDARD).getSearchClient().getParents(root.getObject().getLatestURI());
				List<String> textgridUri = parents.getTextgridUri();
				knownParents = Lists.newArrayListWithExpectedSize(textgridUri.size() + 4);
				for (String uri : textgridUri)
					knownParents.add(URI.create(uri.split("\\.")[0]));
			}
		}

		// for our child aggregations, we ourselves are potential parents.
		knownParents.add(URI.create(root.getObject().getLatestURI()));
		progress.worked(100); if (progress.isCanceled()) return;

		List<URI> aggregationURIs = Lists.newLinkedList();

		for (Object child : root.getChildren()) {
			if (child instanceof Aggregation) {
				Aggregation childAggregation = (Aggregation) child;
				TextGridObject childObject = childAggregation.getObject();
				// If adding this child would introduce a cycle, skip it with a warning.
				if (knownParents.contains(childAggregation.getObject().getLatestURI())) {
					StatusManager
					.getManager().handle(
							new Status(IStatus.WARNING, Activator.PLUGIN_ID,
									NLS.bind(
											"{0} cannot be included in {1} since it is already one of its ancestors. Skipping.",
											childObject, root.getObject())),
							StatusManager.SHOW);
					progress.worked(10);
					continue;
				}
				// do we need to save the child aggregation?
				// currently, we save everything that we _can_ save
				if (!childObject.isPublic() || childObject.hasPermissions(ITextGridPermission.UPDATE) || asRevision && childObject.getProjectInstance().hasPermissions(ITextGridPermission.CREATE)) {
					saveAggregationTree(childAggregation, asRevision, knownParents, progress.newChild(100));
				} else
					progress.worked(100);
				if (progress.isCanceled()) return;

				// if the child aggregation is still new, we need to wait until its save process is finished
				if (childObject.isNew()) {
					long timeout = System.currentTimeMillis() + 10000;
					while (childObject.isNew()) {
						if (System.currentTimeMillis() > timeout)
							throw new CoreException(new Status(IStatus.ERROR, AggregationsUIPlugin.PLUGIN_ID, NLS.bind(
									Messages.AggregationWriter_EM_NoURI, childAggregation.getObject().getTitle())));
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) { /* ignore */ }
						if (progress.isCanceled()) return;
					}
				}

				aggregationURIs.add(childAggregation.getURI());

			} else if (child instanceof TGOentry) {
				aggregationURIs.add(((TGOentry) child).getURI());
				progress.worked(100);
			} else if (child instanceof TextGridObject) {
				aggregationURIs.add(((TextGridObject) child).getURI());
				progress.worked(100);
			}
			else
				Activator.handleError(null, Messages.AggregationWriter_EM_StrangeObject, root, child);
		}
		
		// now that we've handled all our children, finally write the URI list to the aggregation.
		TextGridObject rootObject = root.getObject();
		
		progress.setTaskName(NLS.bind("Saving {0}", rootObject));
		
		if (asRevision)
			rootObject.prepareNewRevision(progress.newChild(10));
		else
			progress.worked(10);
		IFile rootFile = AdapterUtils.getAdapter(rootObject, IFile.class);
		rootFile.setContents(getAggregationStream(aggregationURIs), true, false, progress.newChild(90));

		knownParents.remove(knownParents.size()-1);
	}

	public static InputStream getAggregationStream(List<URI> aggregationURIs) throws XMLStreamException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writeAggregation(aggregationURIs, baos);
		return new ByteArrayInputStream(baos.toByteArray());
	}

	/**
	 * Creates an aggregation document from the given list of URIs.
	 * 
	 * @param uris
	 *            A list of TextGrid URIs
	 * @param output
	 *            The stream to write to
	 * @throws XMLStreamException
	 *             when something goes wrong writing the output
	 */
	public static void writeAggregation(final List<URI> uris, OutputStream output) throws XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = outputFactory.createXMLStreamWriter(output);
		writer.writeStartDocument();

		writer.setPrefix("rdf", RDF_NS); //$NON-NLS-1$
		writer.writeStartElement(RDF_NS, "RDF");//$NON-NLS-1$
		writer.writeNamespace("rdf", RDF_NS);//$NON-NLS-1$
		writer.writeNamespace("ore", ORE_NS);//$NON-NLS-1$
		writer.writeStartElement(RDF_NS, "Description");//$NON-NLS-1$

		for (URI uri : uris) {
			writer.writeEmptyElement(ORE_NS, "aggregates");//$NON-NLS-1$
			writer.writeAttribute("rdf:resource", uri.toString());//$NON-NLS-1$
		}

		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndDocument();
		writer.flush();
		writer.close();
	}

}
