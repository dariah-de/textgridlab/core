package info.textgrid.lab.core.aggregations.ui.model;

public class SectionEvent {
	protected Object actedUpon;

	public SectionEvent(Object receiver) {
		actedUpon = receiver;
	}

	public Object receiver() {
		return actedUpon;
	}
}
