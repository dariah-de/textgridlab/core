package info.textgrid.lab.core.aggregations.ui.model.adapters;

import info.textgrid.lab.core.model.TGContentType;

public class AbstractAggregationTypeAdapter {

	private TGContentType contentType;

	public AbstractAggregationTypeAdapter() {
		super();
	}

	public TGContentType getContentType() {
		return contentType;
	}

	public void init(final TGContentType contentType) {
		this.contentType = contentType;
	}

}