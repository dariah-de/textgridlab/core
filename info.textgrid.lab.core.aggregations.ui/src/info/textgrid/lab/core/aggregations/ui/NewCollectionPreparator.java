package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;

public class NewCollectionPreparator implements INewObjectPreparator {

	private ITextGridWizard wizard;

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;
	}

	public void initializeObject(TextGridObject textGridObject) {
		textGridObject.setTitle("Collection");
	}

	public boolean performFinish(TextGridObject textGridObject) {
		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard).defaultPerformFinish("info.textgrid.lab.core.aggregations.ui.AggregationCompositionPerspective");
		else
			return false;
	}

}
