package info.textgrid.lab.core.aggregations.ui.model;

import info.textgrid.lab.core.model.TGContentType;

public interface IAggregationTypeAdapter {
	
	void init(final TGContentType contentType);
	public void setType(final Aggregation aggregation);

}
