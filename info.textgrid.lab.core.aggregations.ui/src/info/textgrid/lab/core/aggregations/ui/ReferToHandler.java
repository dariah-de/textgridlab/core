package info.textgrid.lab.core.aggregations.ui;

import java.util.Iterator;

import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class ReferToHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
//		System.out.println("ReferToHandler");
		// get parameter
		String parameterReferTo = event
				.getParameter(CommandIDS.COMMAND_PARAMETER_REFERTO_ID);

		boolean booleValue = Boolean.parseBoolean(parameterReferTo);

		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;

		IStructuredSelection sel = (IStructuredSelection) acEditor.getViewer().getSelection();

		if (!sel.isEmpty()) {
			for (Iterator<?> iterator = sel.iterator(); iterator.hasNext();) {
				Object obj = iterator.next();
				if (obj instanceof Aggregation) {
					((Aggregation) obj).setLatest(booleValue);
					((Aggregation) obj).setRevision(null);
					acEditor.getViewer().refresh(obj, true);
					//line added by MH 2016-11-25 Bug #20225
					acEditor.setIsChanged(true);
				} else if (obj instanceof TGOentry) {
					((TGOentry) obj).setLatest(booleValue);
					((TGOentry) obj).setRevision(null);
					acEditor.getViewer().refresh(obj, true);
					//line added by MH 2016-11-25 Bug #20225
					acEditor.setIsChanged(true);
				}
			}
		}

		return null;
	}

}
