package info.textgrid.lab.core.aggregations.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class AggregationsUIPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.core.aggregations.ui";
	
	public static final String ADD_IMAGE_ID = "add"; //$NON-NLS-1$
	public static final String AGGREGATION_IMAGE_ID = "aggregation"; //$NON-NLS-1$
	public static final String COLLAPSEALL_IMAGE_ID = "collapseall"; //$NON-NLS-1$
	public static final String CHANGE_IMAGE_ID = "change"; //$NON-NLS-1$
	public static final String COLLECTION_IMAGE_ID = "collection"; //$NON-NLS-1$
	public static final String DELETE_IMAGE_ID = "delete"; //$NON-NLS-1$
	public static final String EDITION_IMAGE_ID = "edition"; //$NON-NLS-1$
	public static final String EXPANDALL_IMAGE_ID = "expandall"; //$NON-NLS-1$
	public static final String REFERTO_IMAGE_ID = "referto"; //$NON-NLS-1$
	public static final String REFERTOLATEST_IMAGE_ID = "refertolatest"; //$NON-NLS-1$
	public static final String REFERTOREVISION_IMAGE_ID = "refertoversion"; //$NON-NLS-1$
	public static final String RESET_IMAGE_ID = "reset"; //$NON-NLS-1$
	public static final String SAVE_IMAGE_ID = "save"; //$NON-NLS-1$

	// The shared instance
	private static AggregationsUIPlugin plugin;

	/**
	 * The constructor
	 */
	public AggregationsUIPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static AggregationsUIPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static IStatus handleError(Throwable e, String message, Object... args) {
		Status status = new Status(IStatus.ERROR, PLUGIN_ID, NLS.bind(message, args));
		StatusManager.getManager().handle(status);
		return status;
	}
	
	protected void initializeImageRegistry(ImageRegistry registry) {
		registerImage(registry, ADD_IMAGE_ID, "icons/add.gif");
		registerImage(registry, AGGREGATION_IMAGE_ID, "icons/107-ist-Aggregation.gif");
		registerImage(registry, COLLAPSEALL_IMAGE_ID, "icons/collapseall.gif");
		registerImage(registry, CHANGE_IMAGE_ID, "icons/change.gif");
		registerImage(registry, COLLECTION_IMAGE_ID, "icons/109-ist-Collection.gif");
		registerImage(registry, DELETE_IMAGE_ID, "icons/delete.gif");
		registerImage(registry, EDITION_IMAGE_ID, "icons/108-ist-Edition.gif");
		registerImage(registry, EXPANDALL_IMAGE_ID, "icons/expandall.gif");
		registerImage(registry, REFERTO_IMAGE_ID, "icons/refertothis.png");
		registerImage(registry, REFERTOLATEST_IMAGE_ID, "icons/refertolatest.png");
		registerImage(registry, REFERTOREVISION_IMAGE_ID, "icons/refertorevision.png");
		registerImage(registry, RESET_IMAGE_ID, "icons/reset.gif");
		registerImage(registry, SAVE_IMAGE_ID, "icons/save.gif");
    }
	
	/**
	 * Registers the image in the image registry.
	 * 
	 * @param registry
	 *            the image registry to use
	 * @param key
	 *            the key by which the image will be adressable
	 * @param fullPath
	 *            the full path to the image, relative to the plugin directory.
	 */
	private void registerImage(ImageRegistry registry, String key, String fullPath) {
		URL imageURL = FileLocator.find(getBundle(), new Path(fullPath), null);
		registry.put(key, ImageDescriptor.createFromURL(imageURL));
	}
}
