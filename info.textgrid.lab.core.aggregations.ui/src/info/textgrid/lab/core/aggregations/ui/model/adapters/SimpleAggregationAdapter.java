package info.textgrid.lab.core.aggregations.ui.model.adapters;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.IAggregationTypeAdapter;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;

public class SimpleAggregationAdapter extends AbstractAggregationTypeAdapter implements IAggregationTypeAdapter {

	public SimpleAggregationAdapter() {
	}

	public void setType(final Aggregation aggregation) {
		final TextGridObject object = aggregation.getObject();
		object.setContentType(getContentType());
		PersonType rightsHolder = new PersonType();
		rightsHolder.setId(RBACSession.getInstance().getEPPN());
		rightsHolder.setValue(RBACSession.getInstance().getFullName(RBACSession.getInstance().getEPPN()));
		object.setItemMetadata(rightsHolder);
		object.setTitle("Aggregation");
	}

}
