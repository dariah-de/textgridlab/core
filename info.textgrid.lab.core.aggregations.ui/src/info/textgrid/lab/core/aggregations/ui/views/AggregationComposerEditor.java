package info.textgrid.lab.core.aggregations.ui.views;

import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.aggregations.ui.CommandIDS;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.IAggregationTypeAdapter;
import info.textgrid.lab.core.aggregations.ui.model.SectionDragListener;
import info.textgrid.lab.core.aggregations.ui.model.SectionTreeDropAdapter;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;
import info.textgrid.lab.core.metadataeditor.MetaDataView;
import info.textgrid.lab.core.model.ITextGridProjectListener;
import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.revisions.ui.RevisionSelectDialog;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Predicate;

public class AggregationComposerEditor extends EditorPart {
	private Action addAction;
	private Action revisionAction;
	private Action removeAction;
	private Action renameAction;
	private Action expandAllAction;
	private Action collapseAllAction;
	private Action expandAction;
	private Action collapseAction;
	private Action referToAction;
	private Action referToLatestAction;
	private Action referToRevisionAction;
	private Clipboard clipboard;
	private TreeViewer viewer;
	private SectionTreeDropAdapter dropAdapter;
	private Aggregation viewerRoot;
	private Aggregation aggregationRoot;
	private boolean isChanged;
	private TextGridProject targetProject;
	private TextGridObject loadObject = null;
	private boolean isBusy = false;
	private boolean contentTypeChanged = false;
	private ToolBar toolBar;

	private MetaDataView metaDataView = null;

	public static final TGContentType AGGREGATION_CONTENT_TYPE = TGContentType
			.getContentType("text/tg.aggregation+xml"); //$NON-NLS-1$
	public static final TGContentType EDITION_CONTENT_TYPE = TGContentType
			.getContentType("text/tg.edition+tg.aggregation+xml"); //$NON-NLS-1$
	public static final TGContentType COLLECTION_CONTENT_TYPE = TGContentType
			.getContentType("text/tg.collection+tg.aggregation+xml"); //$NON-NLS-1$

	private enum AggregationType {
		AGGREGATION, COLLECTION, EDITION
	};

	public enum SaveType {
		STANDARD, REVISION
	};

	public ArrayList<Object> childList = new ArrayList<Object>();

	public synchronized ArrayList<Object> getChildList() {
		return childList;
	}

	public synchronized void resetChildList() {
		this.childList.clear();
	}

	class AggregationLabelProvider extends LabelProvider implements
			IStyledLabelProvider {

		@Override
		public StyledString getStyledText(Object obj) {
			StyledString styledString = new StyledString();
			String decoration = "";
			if (obj instanceof TGOentry) {
				try {
					styledString.append(((TGOentry) obj).getTitle());
					if (!((TGOentry) obj).getLatest()
							&& (((TGOentry) obj).getRevision() == null)) {
						decoration = MessageFormat
								.format(" (Revision {0})", ((TGOentry) obj).getTgo().getRevisionNumber()); //$NON-NLS-1$
						styledString.append(decoration,
								StyledString.DECORATIONS_STYLER);
					} else if (((TGOentry) obj).getRevision() != null) {
						decoration = MessageFormat
								.format(" (Revision {0})", ((TGOentry) obj).getRevision().getRevisionNumber()); //$NON-NLS-1$
						styledString.append(decoration,
								StyledString.DECORATIONS_STYLER);
					}
				} catch (CoreException e) {
					AggregationsUIPlugin
							.handleError(
									e,
									Messages.AggregationComposerEditor_EM_TitleRetrieve,
									obj);
				}
			} else if (obj instanceof Aggregation) {
				styledString.append(((Aggregation) obj).toString());
				if (!((Aggregation) obj).getLatest()) {
					decoration = MessageFormat
							.format(" (Revision {0})", ((Aggregation) obj).getObject().getRevisionNumber()); //$NON-NLS-1$
					styledString.append(decoration,
							StyledString.DECORATIONS_STYLER);
				} else if (((Aggregation) obj).getRevision() != null) {
					decoration = MessageFormat
							.format(" (Revision {0})", ((Aggregation) obj).getRevision().getRevisionNumber()); //$NON-NLS-1$
					styledString.append(decoration,
							StyledString.DECORATIONS_STYLER);
				}
			} else {
				styledString.append(obj.toString());
			}
			return styledString;
		}

		@Override
		public String getText(Object element) {
			return getStyledText(element).toString();
		}

		@Override
		public Image getImage(Object obj) {
			if (obj instanceof TGOentry) {
				TGODefaultLabelProvider provider = new TGODefaultLabelProvider();
				if (!((TGOentry) obj).isRestrictedTextGridObject())
					return provider.getImage(((TGOentry) obj).getTgo());
				else
					return provider.getImage(((TGOentry) obj)
							.getRestrictedTgo());
			} else if (obj instanceof Aggregation) {
				try {
					String ContentType = ((Aggregation) obj).getObject()
							.getContentTypeID();
					return TGContentType.getContentType(ContentType).getImage(
							true);
				} catch (CoreException e) {
					String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
					return PlatformUI.getWorkbench().getSharedImages()
							.getImage(imageKey);
				}
			} else {
				String imageKey = ISharedImages.IMG_OBJ_FOLDER;
				return PlatformUI.getWorkbench().getSharedImages()
						.getImage(imageKey);
			}
		}
	}

	final DelegatingStyledCellLabelProvider styledLabelProvider = new DelegatingStyledCellLabelProvider(
			new AggregationLabelProvider());
	private AddAggregationAction[] aggregationActions;

	/**
	 * sets the viewerRoot and aggregationRoot of the current composer window.
	 */
	public void setViewerAndAggregationRoot() {
		if (targetProject != null) {
			if (aggregationRoot == null) {
				viewerRoot = new Aggregation(null, "InvisibleRootAggregation",
						null, targetProject);
				aggregationRoot = new Aggregation(viewerRoot, "Aggregation",
						null, targetProject);
			}
		}
	}

	public Aggregation getViewerRoot() {
		return viewerRoot;
	}

	private void contributeToActionBars() {
		fillLocalToolBar();
	}

	private void fillLocalToolBar() {
		final ToolBarManager manager = new ToolBarManager(toolBar);
		manager.add(addAction);
		manager.add(removeAction);
		manager.add(new Separator());
		manager.add(expandAllAction);
		manager.add(collapseAllAction);
		manager.update(true);
	}

	/**
	 * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		final Composite mainComposite = new Composite(parent, SWT.BORDER);

		GridLayout glMain = new GridLayout(2, false);
		glMain.verticalSpacing = 0;
		glMain.marginTop = 2;
		mainComposite.setLayout(glMain);

		createUpperControls(mainComposite);

		createViewer(mainComposite);
		loadAggregation(getLoadObject());
		setLoadObject(null);

		makeActions();
		hookContextMenu();
		makeToolbarPulldownMenuItems();
		contributeToActionBars();

		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(parent,
						"info.textgrid.lab.core.aggregations.ui.AggregationComposerEditor"); //$NON-NLS-1$
	}

	private void createUpperControls(Composite mainComposite) {
		GridData gd = new GridData(SWT.FILL, SWT.TOP, true, true);
		mainComposite.setLayoutData(gd);

		Composite toolBarComposite = new Composite(mainComposite, SWT.NONE);
		GridData gdToolbar = new GridData(SWT.FILL, SWT.FILL, true, false);
		gdToolbar.horizontalSpan = 2;
		toolBarComposite.setLayoutData(gdToolbar);
		toolBar = new ToolBar(toolBarComposite, SWT.HORIZONTAL | SWT.NONE);

		Label seperator = new Label(mainComposite, SWT.SEPARATOR
				| SWT.HORIZONTAL | SWT.SHADOW_OUT);
		seperator.setLayoutData(gdToolbar);
	}

	private void createViewer(Composite mainComposite) {
		Group viewerGroup = new Group(mainComposite, SWT.NONE);
		viewerGroup.setLayout(new GridLayout());
		GridData gdViewer = new GridData(SWT.FILL, SWT.FILL, true, true);
		gdViewer.horizontalSpan = 2;
		viewerGroup.setLayoutData(gdViewer);

		viewer = new TreeViewer(viewerGroup, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL);
		viewer.setContentProvider(new SectionContentProvider());
		// viewer.setLabelProvider(new AggregationLabelProvider());
		viewer.setLabelProvider(styledLabelProvider);
		viewer.setInput(viewerRoot);
		viewer.getControl().setLayoutData(gdViewer);
		getSite().setSelectionProvider(viewer);
		mainComposite.layout();

		// add drag and drop support
		int ops = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer
				.getTransfer() };
		viewer.addDragSupport(ops, transfers, new SectionDragListener(viewer));
		transfers = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		dropAdapter = new SectionTreeDropAdapter(viewer, this);

		viewer.addDropSupport(ops, transfers, dropAdapter);

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if (isBusy())
					return;
				IStructuredSelection sel = (IStructuredSelection) event
						.getSelection();

				metaDataView = (MetaDataView) PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.findView(MetaDataView.ID);

				if (metaDataView == null)
					return;

				if (sel.isEmpty()) {
					// metaDataView.setMetadataInView(getAggregationRoot().getObject(),
					// true);
				} else {
					if (sel.getFirstElement() instanceof Aggregation) {
						if (sel.getFirstElement() == getAggregationRoot()) {
							removeAction.setEnabled(false);
						} else {
							removeAction.setEnabled(true);
							addAction.setEnabled(true);
						}
						Aggregation selectedAggregation = (Aggregation) sel
								.getFirstElement();
						if (metaDataView.getTextGridObject() != selectedAggregation
								.getObject() || contentTypeChanged) {
							metaDataView.setMetadataInView(
									selectedAggregation.getObject(), true);
							contentTypeChanged = false;
						}
					} else {
						removeAction.setEnabled(true);
						addAction.setEnabled(true);
					}
				}
			}
		});

		// Create the editor and set its attributes
		final TreeEditor editor = new TreeEditor(viewer.getTree());
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;

		viewer.getTree().addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				try {
					// get the item
					final TreeItem item = viewer.getTree().getSelection()[0];
					if (!(item.getData() instanceof Aggregation))
						return;
					viewer.setSelection(null);
					final Aggregation aggregation = (Aggregation) item
							.getData();

					// create a text field to do the editing
					final Text text = new Text(viewer.getTree(), SWT.NONE);
					text.setText(item.getText());
					text.selectAll();
					text.setFocus();

					// focus lost
					text.addFocusListener(new FocusAdapter() {
						@Override
						public void focusLost(FocusEvent event) {
							item.setText(text.getText());
							//TG-1887	
							aggregation.getObject()
								.setTitle(text.getText());
							aggregation.getObject().notifyMetadataEditor();
							
							//Leuk 06/2013
							setIsChanged(true);
							viewer.refresh(aggregation);
							text.dispose();
						}
						
						@Override
						public void focusGained(FocusEvent arg0) {
							// TODO Auto-generated method stub
							System.out.println("focusGained");
						}
					});

					text.addKeyListener(new KeyAdapter() {
						@Override
						public void keyPressed(KeyEvent event) {
							switch (event.keyCode) {
							case SWT.CR:
								aggregation.getObject()
										.setTitle(text.getText());
								aggregation.getObject().notifyMetadataEditor();
								setIsChanged(true);
								viewer.refresh(aggregation);
							case SWT.ESC:
								text.dispose();
								viewer.refresh(aggregation);
								break;
							}
						}
					});

					// set the text field into the editor
					editor.setEditor(text, item);
				} catch (ArrayIndexOutOfBoundsException exception) {
					// no double click support at this position
				}
			}
		});
	}

	public void hookContextMenu() {
		MenuManager menuManager = new MenuManager();
		menuManager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		viewer.getTree().setMenu(
				menuManager.createContextMenu(viewer.getTree()));
		getSite().setSelectionProvider(viewer);

		MenuManager newSubMenu = new MenuManager(
				Messages.AggregationComposerEditor_Menu_New);
		MenuManager revisionSubMenu = new MenuManager("Revision");

		for (AddAggregationAction action : getAggregationTypeActions())
			newSubMenu.add(action);

		menuManager.add(newSubMenu);
		menuManager.add(renameAction);
		menuManager.add(removeAction);
		revisionSubMenu.add(referToAction);
		revisionSubMenu.add(referToLatestAction);
		revisionSubMenu.add(referToRevisionAction);
		menuManager.add(revisionSubMenu);
		menuManager.add(new Separator());
		menuManager.add(expandAction);
		menuManager.add(collapseAction);

		menuManager.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager manager) {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				if (obj instanceof TGOentry) {
					if (((TGOentry) obj).isRestrictedTextGridObject()) {
						referToAction.setEnabled(false);
						referToLatestAction.setEnabled(false);
						referToRevisionAction.setEnabled(false);
					} else {
						referToAction.setEnabled(true);
						referToLatestAction.setEnabled(true);
						referToRevisionAction.setEnabled(true);
					}
				}

				if (obj instanceof Aggregation) {
					expandAction.setEnabled(true);
					collapseAction.setEnabled(true);
				} else {
					expandAction.setEnabled(false);
					collapseAction.setEnabled(false);
				}
			}
		});

	}

	@Override
	public void dispose() {
		super.dispose();
		if (clipboard != null)
			clipboard.dispose();
		clipboard = null;
	}

	private AddAggregationAction[] getAggregationTypeActions() {
		if (this.aggregationActions == null) {
			final TGContentType[] aggregationTypes = TGContentType
					.getContentTypes(new Predicate<TGContentType>() {

						@Override
						public boolean apply(TGContentType input) {
							return input.getId().contains("aggregation"); //$NON-NLS-1$
						}
					});
			java.util.Arrays.sort(aggregationTypes);
			aggregationActions = new AddAggregationAction[aggregationTypes.length];
			IConfigurationElement[] elements = Platform
					.getExtensionRegistry()
					.getConfigurationElementsFor(
							"info.textgrid.lab.core.aggregations.ui.aggregationTypeAdapter");
			for (int i = 0; i < aggregationTypes.length; i++) {
				aggregationActions[i] = new AddAggregationAction(
						aggregationTypes[i], elements, SWT.NONE);

			}

		}
		return aggregationActions;
	}

	protected class AddAggregationAction extends Action {
		
		private final TGContentType contentType;
		private IAggregationTypeAdapter extension;

		protected AddAggregationAction(final TGContentType contentType,
				final IConfigurationElement[] elements, int style) {
			super(contentType.getDescription(), style);
			this.setImageDescriptor(contentType.getImageDescriptor());
			this.contentType = contentType;
			setToolTipText(NLS.bind("Add a new {0}.",
					contentType.getDescription()));

			for (IConfigurationElement element : elements) {
				if (element.getAttribute("contentType").equals(
						contentType.getId())) {
					try {
						this.extension = (IAggregationTypeAdapter) element
								.createExecutableExtension("class");
						extension.init(contentType);
						break;
					} catch (CoreException e) {
						StatusManager.getManager().handle(e,
								AggregationsUIPlugin.PLUGIN_ID);
					}
				}
			}
		}

		/**
		 * Creates a menu item equivalent to this action
		 */
		public MenuItem createMenuItem(final Menu menu, final int style) {
			final MenuItem item = new MenuItem(menu, SWT.PUSH);
			item.setImage(contentType.getImage(true));
			item.setText(contentType.getDescription());
			item.addListener(SWT.Selection, new Listener() {
				@Override
				public void handleEvent(Event event) {
					run();
				}
			});
			return item;
		}

		@Override
		public void run() {
			//TODO
			callAddSelectionHandler(contentType, extension);
		}
	}
	
	private void callAddSelectionHandler(TGContentType contentType, IAggregationTypeAdapter extension) {
		Command command = getCommand(CommandIDS.COMMAND_ADD_SELECTION_ID);
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put(CommandIDS.COMMAND_PARAMETER_CONTENTTYPE_ID, contentType.getId());
		params.put(CommandIDS.COMMAND_PARAMETER_EXTENSION_ID, extension.toString());
		ParameterizedCommand paramCommand = ParameterizedCommand.generateCommand(command, params);
		
		executeCommand(paramCommand);
	}
	
	private void callReferToHandler(String referTo) {
		Command command = getCommand(CommandIDS.COMMAND_REFER_TO_ID);

		Map<String,Object> params = new HashMap<String,Object>();
		params.put(CommandIDS.COMMAND_PARAMETER_REFERTO_ID, referTo);
		ParameterizedCommand paramCommand = ParameterizedCommand.generateCommand(command, params);
		
		executeCommand(paramCommand);
	}
	
	private void callRevisionReferToHandler() {
		executeCommand(CommandIDS.COMMAND_REVISION_REFER_TO_ID);
	}

	private void callRemoveItemHandler() {
		executeCommand(CommandIDS.COMMAND_REMOVE_ITEM_ID);
	}

	private void callRenameItemHandler() {
		executeCommand(CommandIDS.COMMAND_RENAME_ID);
	}
	
	private void callExpandAllHandler() {
		executeCommand(CommandIDS.COMMAND_EXPAND_ALL_ID);
	}
	
	private void callExpandAggregationHandler() {
		executeCommand(CommandIDS.COMMAND_EXPAND_AGGREGATION_ID);
	}
	
	private void callCollapseAllHandler() {
		executeCommand(CommandIDS.COMMAND_COLLAPSE_ALL_ID);
	}
	
	private void callcollapseAggregationHandler() {
		executeCommand(CommandIDS.COMMAND_COLLAPSE_AGGREGATION_ID);
	}
	
	private void executeCommand(Object command) {
		IHandlerService handlerService = getIHandlerService();
		
		try {
			if (command instanceof ParameterizedCommand) {
				handlerService.executeCommand((ParameterizedCommand) command, null);
			} else {
				handlerService.executeCommand((String) command, null);
			}
				
		} catch (ExecutionException e) {
			AggregationsUIPlugin.handleError(e, e.getMessage(),
					viewerRoot);
		} catch (NotDefinedException e) {
			AggregationsUIPlugin.handleError(e, e.getMessage(),
					viewerRoot);
		} catch (NotEnabledException e) {
			AggregationsUIPlugin.handleError(e, e.getMessage(),
					viewerRoot);
		} catch (NotHandledException e) {
			AggregationsUIPlugin.handleError(e, e.getMessage(),
					viewerRoot);
		}
	}
	
	/*
	 * deliver IHandlerService
	 */
	private IHandlerService getIHandlerService() {
		IHandlerService hS = (IHandlerService)getSite().getService(IHandlerService.class);
		return hS;
	}

	/*
	 * deliver Command for given ID
	 *
	 * @id command ID
	 */
	private Command getCommand(String id) {
		Command command = ((ICommandService)getSite().getService(ICommandService.class)).getCommand(id);
		return command;
	}
	

	private void makeActions() {
		// NEW MENU
		addAction = new Action(
				Messages.AggregationComposerEditor_NewKindOfAggregation,
				IAction.AS_DROP_DOWN_MENU) {
			@Override
			public void run() {
				// TODO
				addSection(AggregationType.AGGREGATION);
			}
		};

		addAction.setMenuCreator(new IMenuCreator() {
			private Menu listMenu;

			@Override
			public Menu getMenu(Menu parent) {
				return null;
			}

			@Override
			public Menu getMenu(Control parent) {
				if (listMenu == null)
					listMenu = new Menu(parent);
				return listMenu;
			}

			@Override
			public void dispose() {
				if (listMenu != null)
					listMenu.dispose();
			}
		});

		addAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.ADD_IMAGE_ID));
		addAction
				.setText(Messages.AggregationComposerEditor_NewKindOfAggregation);
		addAction
				.setToolTipText(Messages.AggregationComposerEditor_NewKindOfAggregation);

		// REMOVE
		removeAction = new Action() {
			@Override
			public void run() {
				//removeItem();
				callRemoveItemHandler();
			}
		};
		removeAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.DELETE_IMAGE_ID));
		removeAction.setText(Messages.AggregationComposerEditor_RemoveItem);
		removeAction
				.setToolTipText(Messages.AggregationComposerEditor_RemoveItem);

		// RENAME
		renameAction = new Action() {
			@Override
			public void run() {
//				renameItem();
				callRenameItemHandler();
			}
		};
		// renameAction.setImageDescriptor(AggregationsUIPlugin.getDefault().
		// getImageRegistry().getDescriptor(AggregationsUIPlugin.DELETE_IMAGE_ID));
		renameAction.setText(Messages.AggregationComposerEditor_RenameItem);
		renameAction
				.setToolTipText(Messages.AggregationComposerEditor_RenameItem);

		// REVISION MENU
		revisionAction = new Action("Revision", IAction.AS_DROP_DOWN_MENU) {
			@Override
			public void run() {
			}
		};

		revisionAction.setMenuCreator(new IMenuCreator() {
			private Menu listMenu;

			@Override
			public Menu getMenu(Menu parent) {
				return null;
			}

			@Override
			public Menu getMenu(Control parent) {
				if (listMenu == null)
					listMenu = new Menu(parent);
				return listMenu;
			}

			@Override
			public void dispose() {
				if (listMenu != null)
					listMenu.dispose();
			}
		});

		revisionAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.ADD_IMAGE_ID));
		revisionAction.setText("Revision");
		revisionAction.setToolTipText("Revision");

		// REFER TO
		referToAction = new Action() {
			@Override
			public void run() {
//				setLatest(false);
				callReferToHandler("false");
			}
		};
		referToAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.REFERTO_IMAGE_ID));
		referToAction
				.setText(Messages.AggregationComposerEditor_ReferToThisVersion);
		referToAction
				.setToolTipText(Messages.AggregationComposerEditor_ReferToThisVersion);

		// REFER TO LATEST
		referToLatestAction = new Action() {
			@Override
			public void run() {
//				setLatest(true);
				callReferToHandler("true");
			}
		};
		referToLatestAction.setImageDescriptor(AggregationsUIPlugin
				.getDefault().getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.REFERTOLATEST_IMAGE_ID));
		referToLatestAction
				.setText(Messages.AggregationComposerEditor_ReferToLastVersion);
		referToLatestAction
				.setToolTipText(Messages.AggregationComposerEditor_ReferToLastVersion);

		// REFER TO REVISON
		referToRevisionAction = new Action() {
			@Override
			public void run() {
//				selectRevision();
				callRevisionReferToHandler();
			}
		};
		referToRevisionAction.setImageDescriptor(AggregationsUIPlugin
				.getDefault().getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.REFERTOREVISION_IMAGE_ID));
		referToRevisionAction
				.setText(Messages.AggregationComposerEditor_ReferToRevisionPoint);
		referToRevisionAction
				.setToolTipText(Messages.AggregationComposerEditor_ReferToRevision);

		// EXPAND
		expandAllAction = new Action() {
			@Override
			public void run() {
//				viewer.expandAll();
				callExpandAllHandler();
			}
		};
		expandAllAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.EXPANDALL_IMAGE_ID));
		expandAllAction.setText(Messages.AggregationComposerEditor_ExpandAll);
		expandAllAction
				.setToolTipText(Messages.AggregationComposerEditor_ExpandAll);

		expandAction = new Action() {
			@Override
			public void run() {
				System.out.println("expandAction");
//				ISelection selection = viewer.getSelection();
//				Object obj = ((IStructuredSelection) selection)
//						.getFirstElement();
//				viewer.expandToLevel(obj, AbstractTreeViewer.ALL_LEVELS);
				callExpandAggregationHandler();
			}
		};
		expandAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.EXPANDALL_IMAGE_ID));
		expandAction
				.setText(Messages.AggregationComposerEditor_ExpandAggregation);
		expandAction
				.setToolTipText(Messages.AggregationComposerEditor_ExpandAggregation);

		// COLLAPSE
		collapseAllAction = new Action() {
			@Override
			public void run() {
				//viewer.collapseAll();
				callCollapseAllHandler();
			}
		};
		collapseAllAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.COLLAPSEALL_IMAGE_ID));
		collapseAllAction
				.setText(Messages.AggregationComposerEditor_CollapseAll);
		collapseAllAction
				.setToolTipText(Messages.AggregationComposerEditor_CollapseAll);

		collapseAction = new Action() {
			@Override
			public void run() {
//				ISelection selection = viewer.getSelection();
//				Object obj = ((IStructuredSelection) selection)
//						.getFirstElement();
//				viewer.collapseToLevel(obj, AbstractTreeViewer.ALL_LEVELS);
				callcollapseAggregationHandler();
			}
		};
		collapseAction.setImageDescriptor(AggregationsUIPlugin.getDefault()
				.getImageRegistry()
				.getDescriptor(AggregationsUIPlugin.COLLAPSEALL_IMAGE_ID));
		collapseAction
				.setText(Messages.AggregationComposerEditor_CollapseAggregation);
		collapseAction
				.setToolTipText(Messages.AggregationComposerEditor_CollapseAggregation);
	}

	private void makeToolbarPulldownMenuItems() {
		IMenuCreator mc = addAction.getMenuCreator();
		Menu menu = mc.getMenu(toolBar);

		for (AddAggregationAction action : getAggregationTypeActions())
			action.createMenuItem(menu, SWT.PUSH);
	}

	@Override
	public void setFocus() {
		if (viewer != null)
			viewer.getControl().setFocus();
	}

	/**
	 * Adds a new section at the current position
	 * 
	 * @param contentType
	 *            the content type of the aggregation to add. Must, of course,
	 *            be an aggregation content type.
	 * @param extension
	 *            an extension adapter that will configure the aggregation. May
	 *            be <code>null</code>, meaning no special configuration will
	 *            happen.
	 */
	protected void addSection(final TGContentType contentType,
			final IAggregationTypeAdapter extension) {
		setIsBusy(true);
		ISelection selection = viewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();
		Aggregation aggregation = aggregationRoot;
		if (obj instanceof Aggregation) {
			aggregation = (Aggregation) obj;
		}

		Aggregation newAggregation = new Aggregation(aggregation,
				contentType.getDescription(), null, getTargetProject());

		newAggregation.getObject().setContentType(contentType);
		if (extension != null)
			extension.setType(newAggregation);

		setIsChanged(true);
		viewer.refresh(aggregation.getParent());
		setIsBusy(false);
		viewer.setSelection(new StructuredSelection(newAggregation), true);
		viewer.getTree().notifyListeners(SWT.MouseDoubleClick, new Event());

	}

	@Deprecated
	private void addSection(AggregationType type) {
		setIsBusy(true);
		ISelection selection = viewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();
		Aggregation aggregation = aggregationRoot;
		if (obj instanceof Aggregation) {
			aggregation = (Aggregation) obj;
		}

		String name = type.toString().charAt(0)
				+ type.toString().substring(1, type.toString().length())
						.toLowerCase();

		Aggregation newAggregation = new Aggregation(aggregation, name, null,
				getTargetProject());
		switch (type) {
		case AGGREGATION:
			newAggregation.setObjectToItem();
			break;
		case COLLECTION:
			newAggregation.setObjectToCollection();
			break;
		case EDITION:
			newAggregation.setObjectToEdition();
		}
		setIsChanged(true);
		viewer.refresh(aggregation.getParent());
		setIsBusy(false);
		viewer.setSelection(new StructuredSelection(newAggregation), true);
		viewer.getTree().notifyListeners(SWT.MouseDoubleClick, new Event());
	}

	public void save(final SaveType saveType) {
		if (saveType.equals(SaveType.REVISION)) {
			if (!SearchRequest.isTgoLatestRevision(getAggregationRoot()
					.getObject().getURI().toString())) {
				MessageDialog
						.openError(
								Display.getCurrent().getActiveShell(),
								Messages.AggregationComposerEditor_SaveAggregation,
								Messages.AggregationComposerEditor_EM_AggregationHasSuccessive);
				return;
			}
		}

		if (saveType.equals(SaveType.STANDARD)) {
			try {
				if (getAggregationRoot().getObject().isPublic()) {
					MessageDialog
							.openWarning(
									Display.getCurrent().getActiveShell(),
									Messages.AggregationComposerEditor_SaveAggregation,
									Messages.AggregationComposerEditor_EM_AggregationAlreadyPublished);
					return;
				}
			} catch (CoreException e) {
				AggregationsUIPlugin
						.handleError(
								e,
								Messages.AggregationComposerEditor_EM_ErrorWhenAccessing,
								getAggregationRoot().getObject());
				return;
			}
		}

		try {
			if (!getAggregationRoot().getObject().getProjectInstance()
					.equals(getTargetProject()))
				setAggregationsToTargetProject(getAggregationRoot());
		} catch (CoreException e) {
			AggregationsUIPlugin.handleError(e, "Aggregation Composer Error.");
		}

		Job saveJob = new Job(NLS.bind(
				Messages.AggregationComposerEditor_IM_Saving,
				getAggregationRoot())) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				setIsBusy(true);
				try {
					getAggregationRoot().save(monitor,
							((saveType == SaveType.REVISION) ? true : false));
				} catch (XMLStreamException e) {
					return AggregationsUIPlugin
							.handleError(
									e,
									Messages.AggregationComposerEditor_EM_SerializeAggregation,
									viewerRoot);
				} catch (CoreException e) {
					return AggregationsUIPlugin.handleError(e, e.getMessage(),
							viewerRoot);
				}
				return Status.OK_STATUS;
			}
		};
		saveJob.setUser(true);
		saveJob.addJobChangeListener(new JobChangeAdapter() {

			@Override
			public void aboutToRun(IJobChangeEvent event) {
				super.aboutToRun(event);
				UIJob job = new UIJob(
						Messages.AggregationComposerEditor_DisableWidgets) {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						setWidgetsEnabled(false);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}

			@Override
			public void done(IJobChangeEvent event) {
				super.done(event);
				UIJob job = new UIJob(
						Messages.AggregationComposerEditor__ResetComposer) {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						setIsChanged(false);
						setIsBusy(false);
						setWidgetsEnabled(true);
						TextGridProject.notifyListeners(
								ITextGridProjectListener.Event.CONTENT_CHANGED,
								targetProject);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		});
		saveJob.schedule(1000);
	}

	private void setWidgetsEnabled(final boolean enabled) {
		for (AddAggregationAction action : getAggregationTypeActions())
			action.setEnabled(enabled);
		addAction.setEnabled(enabled);
		revisionAction.setEnabled(enabled);
		removeAction.setEnabled(enabled);
		expandAllAction.setEnabled(enabled);
		collapseAllAction.setEnabled(enabled);
		if (!viewer.getTree().isDisposed())
			viewer.getTree().setEnabled(enabled);
	}

//	private void removeItem() {
//		String message = null;
//		ISelection selection = viewer.getSelection();
//
//		if (((IStructuredSelection) selection).size() == 1) {
//			Object obj = ((IStructuredSelection) selection).getFirstElement();
//			if (obj instanceof Aggregation) {
//				Aggregation aggregation = (Aggregation) obj;
//				if (aggregation.getChildren().length > 0) {
//					message = NLS
//							.bind(Messages.AggregationComposerEditor_RemoveWithChildren,
//									aggregation.getName());
//				} else {
//					message = NLS.bind(
//							Messages.AggregationComposerEditor_Remove,
//							aggregation.getName());
//				}
//			} else if (obj instanceof TGOentry) {
//				TGOentry entry = (TGOentry) obj;
//				try {
//					message = NLS.bind(
//							Messages.AggregationComposerEditor_Remove,
//							entry.getTitle());
//				} catch (CoreException e) {
//					AggregationsUIPlugin.handleError(e,
//							"Aggregation Composer Error.");
//				}
//			}
//		} else {
//			message = NLS.bind(
//					Messages.AggregationComposerEditor_RemoveItemWithChildren,
//					((IStructuredSelection) selection).size());
//		}
//
//		if (MessageDialog.openConfirm(Display.getCurrent().getActiveShell(),
//				"Aggregation Composer", message)) {
//			for (Object item : ((IStructuredSelection) selection).toArray()) {
//				if (item instanceof Aggregation) {
//					((Aggregation) item).delete();
//					setIsChanged(true);
//				} else if (item instanceof TGOentry) {
//					((TGOentry) item).delete();
//					setIsChanged(true);
//				}
//			}
//			viewer.refresh(aggregationRoot);
//			viewer.setSelection(new StructuredSelection(aggregationRoot), true);
//		}
//	}

//	private void renameItem() {
//		ISelection selection = viewer.getSelection();
//		if (((IStructuredSelection) selection).size() == 1) {
//			viewer.getTree().notifyListeners(SWT.MouseDoubleClick, new Event());
//		} else {
//			MessageDialog.openInformation(
//					Display.getCurrent().getActiveShell(),
//					"Aggregation Composer",
//					Messages.AggregationComposerEditor_IM_SelectOnlyOneItem);
//		}
//	}

//	private void setLatest(boolean value) {
//		IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
//
//		if (!sel.isEmpty()) {
//			for (Iterator<?> iterator = sel.iterator(); iterator.hasNext();) {
//				Object obj = iterator.next();
//				if (obj instanceof Aggregation) {
//					((Aggregation) obj).setLatest(value);
//					((Aggregation) obj).setRevision(null);
//					viewer.refresh(obj, true);
//				} else if (obj instanceof TGOentry) {
//					((TGOentry) obj).setLatest(value);
//					((TGOentry) obj).setRevision(null);
//					viewer.refresh(obj, true);
//				}
//			}
//		}
//	}

//	private void selectRevision() {
//		IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
//		Object obj = sel.getFirstElement();
//
//		if (obj != null) {
//			RevisionSelectDialog posDialog = new RevisionSelectDialog(
//					PlatformUI.getWorkbench().getDisplay().getActiveShell());
//			posDialog.refreshDialog(obj);
//			posDialog.open();
//			viewer.refresh(obj, true);
//		}
//	}

	public TextGridProject getTargetProject() {
		return this.targetProject;
	}

	public void setTargetProject(TextGridProject targetProject) {
		this.targetProject = targetProject;
	}

	public Aggregation getAggregationRoot() {
		return aggregationRoot;
	}

	public void loadAggregation(final Object object) {

		if (aggregationRoot == null || object == null)
			return;

		UIJob job = new UIJob(
				Messages.AggregationComposerEditor_LoadAggregation) {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				if (object instanceof TextGridObject) {
					getAggregationRoot().setObject((TextGridObject) object);
					adaptPartInformation();
					SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(
							Messages.AggregationComposerEditor_FetchingMembers,
							object), 100);
					try {
						if (((TextGridObject) object).getCreated() != null) {
							setIsChanged(false);
							return fetchAggregation((TextGridObject) object,
									progress, getAggregationRoot());
						} else {
							setIsChanged(true);
						}
					} catch (CoreException e) {
						IStatus status = new Status(
								IStatus.ERROR,
								Activator.PLUGIN_ID,
								NLS.bind(
										Messages.AggregationComposerEditor_EM_AccessingAggregation,
										object.toString()), e);
						Activator.getDefault().getLog().log(status);
					}
				}
				return Status.OK_STATUS;
			}
		};

		job.addJobChangeListener(new JobChangeAdapter() {
			@Override
			public void done(IJobChangeEvent event) {
				viewer.refresh();
				viewer.expandAll();
				viewer.setSelection(new StructuredSelection(aggregationRoot),
						true);
			}
		});

		job.setUser(true);
		job.schedule();

	}

	public void importAggregation(final Aggregation target, final Object object) {
		UIJob job = new UIJob(
				Messages.AggregationComposerEditor_FetchAggregation) {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(
						Messages.AggregationComposerEditor_FetchingMembers,
						object), 100);
				return fetchAggregation((TextGridObject) object, progress,
						target);
			}
		};

		job.addJobChangeListener(new IJobChangeListener() {

			@Override
			public void sleeping(IJobChangeEvent event) {
			}

			@Override
			public void scheduled(IJobChangeEvent event) {
			}

			@Override
			public void running(IJobChangeEvent event) {
			}

			@Override
			public void done(IJobChangeEvent event) {
				viewer.refresh();
			}

			@Override
			public void awake(IJobChangeEvent event) {
			}

			@Override
			public void aboutToRun(IJobChangeEvent event) {

			}
		});

		job.setUser(true);
		job.schedule();
	}

	public IStatus fetchAggregation(final TextGridObject aggregation,
			SubMonitor progress, Aggregation actualRoot) {
		setIsBusy(true);
		progress.beginTask(NLS.bind(
				Messages.AggregationComposerEditor_QueryingForChildren,
				aggregation), 100);
		SearchRequest searchRequest = new SearchRequest();
		Response response = searchRequest.listAggregation(aggregation.getURI()
				.toString());

		if (progress.isCanceled())
			return Status.CANCEL_STATUS;

		int hits = response.getResult().size();
		progress.beginTask(NLS.bind(
				Messages.AggregationComposerEditor_AddingMembers, hits,
				aggregation), hits);

		Iterator<?> iter = response.getResult().iterator();
		while (iter.hasNext()) {
			if (progress.isCanceled())
				return Status.CANCEL_STATUS;

			Object obj = iter.next();
			if (obj instanceof ResultType) {
				ObjectType oType = ((ResultType) obj).getObject();
				try {
					if (oType.getGeneric().getProvided() != null) {
						TextGridObject tgo = TextGridObject.getInstance(oType,
								true, true);
						if (tgo.getContentTypeID().contains("aggregation")) {
							Aggregation newAggregation = new Aggregation(
									actualRoot, tgo.getTitle(), tgo,
									getTargetProject());
							fetchAggregation(tgo, progress, newAggregation);
						} else {
							TGOentry tgoEntry = new TGOentry(tgo.getURI()
									.toString());
							if (((ResultType) obj).getTextgridUri().contains(
									"."))
								tgoEntry.setLatest(false);
							actualRoot.addChild(tgoEntry);
							tgoEntry.setParent(actualRoot);
						}
					} else {
						// Restricted object
						TGOentry tgoRestricted = new TGOentry(
								new RestrictedTextGridObject(oType.getGeneric()
										.getGenerated().getTextgridUri()
										.getValue()));
						actualRoot.addChild(tgoRestricted);
						tgoRestricted.setParent(actualRoot);
					}
				} catch (CoreException e) {
					IStatus status = new Status(
							IStatus.ERROR,
							Activator.PLUGIN_ID,
							NLS.bind(
									Messages.AggregationComposerEditor_EM_WhileRetrievingObjects,
									aggregation.toString()), e);
					Activator.getDefault().getLog().log(status);
				}
			}
			progress.worked(1);
		}
		setIsBusy(false);
		return Status.OK_STATUS;
	}

	public ArrayList<Object> proofImportAggregation(final Aggregation target,
			final Object object) {
		return fetchAggregation4ZyklusProofing((TextGridObject) object, target);
	}

	public ArrayList<Object> fetchAggregation4ZyklusProofing(
			final TextGridObject aggregation, Aggregation actualRoot) {

		SearchRequest searchRequest = new SearchRequest();
		Response response = searchRequest.listAggregation(aggregation.getURI()
				.toString());

		Iterator<?> iter = response.getResult().iterator();
		while (iter.hasNext()) {

			Object obj = iter.next();
			if (obj instanceof ResultType) {
				ObjectType oType = ((ResultType) obj).getObject();
				try {
					if (oType.getGeneric().getProvided() != null) {
						TextGridObject tgo = TextGridObject.getInstance(oType,
								true, true);
						if (tgo.getContentTypeID().contains("aggregation")) {
							Aggregation newAggregation = new Aggregation(
									actualRoot, tgo.getTitle(), tgo,
									getTargetProject(), false);
							childList.add(newAggregation.getObject().getURI());
							fetchAggregation4ZyklusProofing(tgo, newAggregation);
						}
					}
				} catch (CoreException e) {
					IStatus status = new Status(
							IStatus.ERROR,
							Activator.PLUGIN_ID,
							NLS.bind(
									Messages.AggregationComposerEditor_EM_WhileRetrievingObjects,
									aggregation.toString()), e);
					Activator.getDefault().getLog().log(status);
				}
			}
		}

		return childList;
	}

	public boolean getIsChanged() {
		return isChanged;
	}

	public void setIsChanged(boolean isChanged) {
		this.isChanged = isChanged;
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	private void setAggregationsToTargetProject(Aggregation actualRoot) {
		Object[] children = actualRoot.getChildren();
		for (int i = 0; i < children.length; i++) {
			if (children[i] instanceof Aggregation) {
				try {
					if (((Aggregation) children[i]).getObject().getCreated() == null)
						setAggregationsToTargetProject((Aggregation) children[i]);
				} catch (CoreException e) {
					AggregationsUIPlugin
							.handleError(
									e,
									Messages.AggregationComposerEditor_EM_AggregationContainsNoObject);
				}
			}
		}
		actualRoot.getObject().setProject(getTargetProject());
	}

	public TextGridObject getLoadObject() {
		return loadObject;
	}

	public void setLoadObject(TextGridObject loadObject) {
		this.loadObject = loadObject;
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void setIsBusy(boolean value) {
		isBusy = value;
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		save(SaveType.STANDARD);
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		TextGridObject tgo = (TextGridObject) input
				.getAdapter(TextGridObject.class);
		if (tgo != null) {
			AggregationComposerEditorInput aggregationEditorInput = new AggregationComposerEditorInput(
					tgo);
			setSite(site);
			setInput(aggregationEditorInput);
			try {
				targetProject = tgo.getProjectInstance();
				setViewerAndAggregationRoot();
				setLoadObject(tgo);
			} catch (CoreException e) {
				AggregationsUIPlugin
						.handleError(
								e,
								NLS.bind(
										Messages.AggregationComposerEditor_EM_NoProjectInstance,
										aggregationRoot.getObject().toString()));
			}
		} else {
			AggregationsUIPlugin.handleError(new Exception(),
					Messages.AggregationComposerEditor_EM_NoValidEditorInput);
		}
	}

	@Override
	public boolean isDirty() {
		return isChanged;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * deliver information of the root aggregation to the editor
	 */
	private void adaptPartInformation() {
		try {
			setPartName(aggregationRoot.getName());
			setTitleToolTip(aggregationRoot.getName()
					+ " "
					+ aggregationRoot.getObject().getContentType(true)
							.getDescription());
			setTitleImage(aggregationRoot.getObject().getContentType(true)
					.getImage(false));
		} catch (CoreException e) {
			AggregationsUIPlugin.handleError(e, NLS.bind(
					Messages.AggregationComposerEditor_EM_NoContentType,
					aggregationRoot.getObject().toString()));
		}
	}
	
}
