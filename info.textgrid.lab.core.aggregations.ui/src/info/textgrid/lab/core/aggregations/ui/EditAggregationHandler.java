package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.menus.OpenObjectService;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

public class EditAggregationHandler extends AbstractHandler {

	private static final String PARAMETER_URI_ID = "info.textgrid.lab.core.aggregations.ui.EditAggregation.parameter.uri";
	private static final String PARAMETER_READONLY_ID = "info.textgrid.lab.core.aggregations.ui.EditAggregation.parameter.readonly";
	final String AGGREGATION = "aggregation";
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String parameterURI = null;
		String parameterReadOnly = null;
		TextGridObject tgo = null; 

		//get parameter
		parameterURI = event.getParameter(PARAMETER_URI_ID);
		parameterReadOnly = event.getParameter(PARAMETER_READONLY_ID);
		
		//get TextGrid object
		if (parameterURI != null) {
			try {
				tgo = TextGridObject.getInstance(new URI(parameterURI), false);
			} catch (CrudServiceException e) {
				AggregationsUIPlugin.handleError(e, e.getMessage(), parameterURI);
			} catch (URISyntaxException e) {
				AggregationsUIPlugin.handleError(e, e.getMessage(), parameterURI);
			}
		} else {
			ISelection selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				tgo = AdapterUtils.getAdapter(obj, TextGridObject.class);
			}	
		}
 
		
		if (tgo != null) {
			try {
				if (tgo.getContentTypeID().contains(AGGREGATION)) {
					ShowAggregationCompositionPerspectiveHandler PerspectiveHandler = new ShowAggregationCompositionPerspectiveHandler();
					PerspectiveHandler.execute(event);	
					//TODO: Locking for nested Aggregations
					OpenObjectService.getInstance().openObject(tgo, 2, Boolean.parseBoolean(parameterReadOnly));
			    }	
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, AggregationsUIPlugin.PLUGIN_ID,
						Messages.EditAggregationHandler_EM_CouldNotRetrieveContentType 
								+ tgo.toString(), e);
				AggregationsUIPlugin.getDefault().getLog().log(status);
			}
		}

		return null;
	}
}
