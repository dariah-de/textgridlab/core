package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.revisions.ui.SaveAsRevisionAggregationDialog;
import info.textgrid.lab.newsearch.SearchRequest;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Handler for saving the aggregation in the current editor as a new revision.
 */
public class SaveNewAggregationRevisionHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (editor != null && 
			editor instanceof AggregationComposerEditor) {
			TextGridObject tgo = ((AggregationComposerEditor)editor).getAggregationRoot().getObject();
			try {
				if (!tgo.getProjectInstance().hasPermissions(ITextGridPermission.CREATE)) {
					MessageDialog.openError(Display.getCurrent().getActiveShell(), Messages.SaveNewAggregationRevisionHandler_SaveAsNewRevision, 
					NLS.bind(Messages.SaveNewAggregationRevisionHandler_IM_NotAllowedToSave, tgo.getProjectInstance().getName()));
					return null;
				}
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, AggregationsUIPlugin.PLUGIN_ID);
			}
			
			if (!SearchRequest.isTgoLatestRevision(tgo.getURI().toString())) {
				MessageDialog.openError(Display.getCurrent().getActiveShell(), Messages.SaveNewAggregationRevisionHandler_SaveAsNewRevision, 
				Messages.SaveNewAggregationRevisionHandler_IM_SuccessiveRevision);
				return null;
			}
			SaveAsRevisionAggregationDialog dialog = new SaveAsRevisionAggregationDialog(HandlerUtil.getActiveShell(event));
			dialog.setBlockOnOpen(false);
			dialog.open();
			dialog.init((AggregationComposerEditor)editor);
		} else {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, AggregationsUIPlugin.PLUGIN_ID, Messages.SaveNewAggregationRevisionHandler_EM_NoOpenEditor), StatusManager.SHOW);
		}
		return null;
	}

}
