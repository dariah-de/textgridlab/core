package info.textgrid.lab.core.aggregations.ui.model.adapters;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.IAggregationTypeAdapter;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.namespaces.metadata.agent._2010.AgentRoleType;
import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;
import info.textgrid.namespaces.metadata.core._2010.EditionType.License;

public class EditionTypeAdapter extends AbstractAggregationTypeAdapter
		implements IAggregationTypeAdapter {

	public EditionTypeAdapter() {
		// TODO Auto-generated constructor stub
	}

	public void setType(Aggregation aggregation) {
		TextGridObject object = aggregation.getObject();
		object.setContentType(AggregationComposerEditor.EDITION_CONTENT_TYPE);
		AgentType agent = new AgentType();
		PersonType person = RBACSession.getInstance().getPerson();
		agent.setId(person.getId());
		agent.setRole(AgentRoleType.EDITOR); // FIXME right role?
		agent.setValue(person.getValue());
		License license = new License();
		object.setEditionMetadata(null, agent, null, null, license);
		object.setTitle("Edition");

	}

}
