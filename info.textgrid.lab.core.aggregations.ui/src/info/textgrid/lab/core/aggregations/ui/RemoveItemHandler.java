package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.aggregations.ui.views.Messages;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class RemoveItemHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
//		System.out.println("RemoveItemHandler");

		String message = null;
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;

		Aggregation aggregationRoot = acEditor.getAggregationRoot();
		ISelection selection = editor.getSite().getWorkbenchWindow()
				.getSelectionService().getSelection();

		if (((IStructuredSelection) selection).size() == 1) {
			Object obj = ((IStructuredSelection) selection).getFirstElement();
			if (obj instanceof Aggregation) {
				Aggregation aggregation = (Aggregation) obj;
				if (aggregation.getChildren().length > 0) {
					message = NLS
							.bind(Messages.AggregationComposerEditor_RemoveWithChildren,
									aggregation.getName());
				} else {
					message = NLS.bind(
							Messages.AggregationComposerEditor_Remove,
							aggregation.getName());
				}
			} else if (obj instanceof TGOentry) {
				TGOentry entry = (TGOentry) obj;
				try {
					message = NLS.bind(
							Messages.AggregationComposerEditor_Remove,
							entry.getTitle());
				} catch (CoreException e) {
					AggregationsUIPlugin.handleError(e,
							"Aggregation Composer Error.");
				}
			}
		} else {
			message = NLS.bind(
					Messages.AggregationComposerEditor_RemoveItemWithChildren,
					((IStructuredSelection) selection).size());
		}

		if (MessageDialog.openConfirm(Display.getCurrent().getActiveShell(),
				"Aggregation Composer", message)) {
			for (Object item : ((IStructuredSelection) selection).toArray()) {
				if (item instanceof Aggregation) {
					((Aggregation) item).delete();
					acEditor.setIsChanged(true);
				} else if (item instanceof TGOentry) {
					((TGOentry) item).delete();
				}
			}
			acEditor.getViewer().refresh(aggregationRoot);
			acEditor.getViewer().setSelection(
					new StructuredSelection(aggregationRoot), true);
		}

		return null;
	}

}
