package info.textgrid.lab.core.aggregations.ui.model.adapters;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.IAggregationTypeAdapter;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;


public class CollectionTypeAdapter extends AbstractAggregationTypeAdapter
		implements IAggregationTypeAdapter {

	public CollectionTypeAdapter() {
		// TODO Auto-generated constructor stub
	}

	public void setType(Aggregation aggregation) {
		TextGridObject object = aggregation.getObject();
		object.setContentType(AggregationComposerEditor.COLLECTION_CONTENT_TYPE);
		PersonType collector = RBACSession.getInstance().getPerson();
		object.setCollectionMetadata(collector, null, null, null, null, null);
		object.setTitle("Collection");
	}

}
