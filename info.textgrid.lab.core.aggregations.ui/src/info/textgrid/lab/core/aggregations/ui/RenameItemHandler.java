package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.aggregations.ui.views.Messages;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class RenameItemHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
//		System.out.println("RenameItemHandler");

		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;

		Aggregation aggregationRoot = acEditor.getAggregationRoot();
		ISelection selection = editor.getSite().getWorkbenchWindow()
				.getSelectionService().getSelection();

		if (((IStructuredSelection) selection).size() == 1) {
			acEditor.getViewer().getTree()
					.notifyListeners(SWT.MouseDoubleClick, new Event());
			acEditor.setIsChanged(true);
		} else {
			MessageDialog.openInformation(
					Display.getCurrent().getActiveShell(),
					"Aggregation Composer",
					Messages.AggregationComposerEditor_IM_SelectOnlyOneItem);
		}

		acEditor.getViewer().refresh(aggregationRoot);

		return null;
	}

}
