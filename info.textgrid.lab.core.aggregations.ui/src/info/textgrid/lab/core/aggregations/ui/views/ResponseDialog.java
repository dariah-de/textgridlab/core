package info.textgrid.lab.core.aggregations.ui.views;

import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;

/**
 * @author hausner
 *
 *A Dialog with a scrollable Text to show the output of the Web Publisher.
 */
public class ResponseDialog extends Dialog {

	private String textMessage = "";
	private String publishURL = "";

	protected ResponseDialog(Shell parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}


	protected Control createDialogArea(Composite parent) {


		Composite composite = (Composite) super.createDialogArea(parent);

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 330;
		parentGD.widthHint = 660;
		parentGD.verticalAlignment = GridData.FILL;       

		composite.setLayoutData(parentGD);
		
		Link link = new Link(composite, SWT.NONE);
		link.setText(NLS.bind(Messages.ResponseDialog_ClickForResult,
				publishURL));
		link.addSelectionListener (new SelectionListener () {

			public void widgetDefaultSelected(SelectionEvent event) {
				try {
					PlatformUI.getWorkbench().getBrowserSupport().createBrowser(IWorkbenchBrowserSupport.AS_EXTERNAL, "aCustomId", "url", "url").openURL(new URL(publishURL));
				} catch (PartInitException e) {
					AggregationsUIPlugin.handleError(e, "Web Publisher Error."); //$NON-NLS-1$
				} catch (MalformedURLException e) {
					AggregationsUIPlugin.handleError(e, "Web Publisher Error."); //$NON-NLS-1$
				}
			}

			public void widgetSelected(SelectionEvent event) {
				try {
					PlatformUI.getWorkbench().getBrowserSupport().createBrowser(IWorkbenchBrowserSupport.AS_EXTERNAL, "aCustomId", "url", "url").openURL(new URL(publishURL));
				} catch (PartInitException e) {
					AggregationsUIPlugin.handleError(e, "Web Publisher Error."); //$NON-NLS-1$
				} catch (MalformedURLException e) {
					AggregationsUIPlugin.handleError(e, "Web Publisher Error."); //$NON-NLS-1$
				}
			}
		});

		Text text = new Text(composite, SWT.MULTI | SWT.V_SCROLL|SWT.H_SCROLL| SWT.BORDER);
		text.setEditable(false);
		text.setText(NLS.bind(Messages.ResponseDialog_PublishedTo, publishURL, textMessage));

		text.setLayoutData(new GridData(GridData.FILL_BOTH));
		getShell().setText("Web Publisher"); //$NON-NLS-1$

		return composite;


	}
	public void setText(String message, String url){
		textMessage = message;
		publishURL = url;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.OK_ID)
			this.close();
	}



}
