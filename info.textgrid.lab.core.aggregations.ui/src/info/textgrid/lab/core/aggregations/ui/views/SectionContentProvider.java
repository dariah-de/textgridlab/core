package info.textgrid.lab.core.aggregations.ui.views;

import info.textgrid.lab.core.aggregations.ui.model.ISectionListener;
import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.SectionEvent;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.TreeItem;

public class SectionContentProvider implements ITreeContentProvider, ISectionListener {

	private TreeViewer viewer;
	
	public void dispose() {
	}
	
	public Object[] getChildren(Object parentElement) {
		//System.out.println("getChildren: " + parentElement.toString());
		if (parentElement == null)
			return new Aggregation[0];
		if (parentElement instanceof Aggregation)
			return ((Aggregation)parentElement).getChildren();
		else
			return null;
	}
	
	public Object[] getElements(Object input) {
		return getChildren(input);
	}
	
	public Object getParent(Object element) {
		if(element instanceof Aggregation){
			return ((Aggregation)element).getParent();
		} else if(element instanceof TGOentry){
			return ((TGOentry)element).getParent();
		}else return null;

	}
	
	public boolean hasChildren(Object element) {
		//System.out.println("hasChildren: " + element.toString());
		if(element instanceof Aggregation){
			return getChildren(element).length > 0;
		}
		else return false;
	}
	
	public void inputChanged(Viewer v, Object old, Object newt) {
		this.viewer = (TreeViewer)v;
		if(old != null) {
			removeListenerFrom((Aggregation)old);
		}
		if(newt != null) {
			addListenerTo((Aggregation)newt);
		}
	}

	protected void addListenerTo(Aggregation aggregation){
		aggregation.addListener(this);
		Object[] children = aggregation.getChildren();
		for(int i = 0; i < children.length; i++){
			if(children[i] instanceof Aggregation){
				addListenerTo((Aggregation)children[i]);
			}
		}
	}

	protected void removeListenerFrom(Aggregation aggregation){
		aggregation.removeListener(this);
		Object[] children = aggregation.getChildren();
		for(int i = 0; i < children.length; i++){
			if(children[i] instanceof Aggregation){
				removeListenerFrom((Aggregation)children[i]);
			}
		}
	}

	public void add(SectionEvent event) {
		Object receiver = event.receiver();
		if(receiver instanceof Aggregation){
			Aggregation aggregation = (Aggregation) receiver;
			addListenerTo(aggregation);
			viewer.setSelection(new StructuredSelection(receiver), true);
		}else if(receiver instanceof TGOentry){
			TGOentry entry = (TGOentry) receiver;
			viewer.setSelection(new StructuredSelection(receiver), true);
		}
	}
	
	public void remove(SectionEvent event) {
		Object receiver = event.receiver();
		if(receiver instanceof Aggregation){
			Aggregation aggregation = (Aggregation) receiver;
			removeListenerFrom(aggregation);
		}else if(receiver instanceof TGOentry){
			TGOentry entry = (TGOentry) receiver;
		}

	}
	
	public void rename(SectionEvent event) {
		Object receiver = event.receiver();
		viewer.update(receiver, null);
	}

}
