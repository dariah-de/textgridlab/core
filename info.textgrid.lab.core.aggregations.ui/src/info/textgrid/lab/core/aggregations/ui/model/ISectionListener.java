package info.textgrid.lab.core.aggregations.ui.model;



public interface ISectionListener {
	public void add(SectionEvent event);
	public void remove(SectionEvent event);
	public void rename(SectionEvent event);
}
