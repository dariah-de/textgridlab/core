package info.textgrid.lab.core.aggregations.ui.model;

import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditorInput;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.IAdapterFactory;

/**
 * Adapts TextGridObjects to AggregationComposerEditorInput. 
 */
public class ACEditorInputFactory implements IAdapterFactory {

	public static final Class<?>[] ADAPTER_LIST = {AggregationComposerEditorInput.class};

	public Object getAdapter(Object adaptableObject,
			@SuppressWarnings("unchecked") Class adapterType) {
		if (adaptableObject instanceof TGObjectReference) {
			return new AggregationComposerEditorInput(((TGObjectReference)adaptableObject).getTgo());
		}

		return null;
	}

	public Class<?>[] getAdapterList() {
		return ADAPTER_LIST;
	}

}
