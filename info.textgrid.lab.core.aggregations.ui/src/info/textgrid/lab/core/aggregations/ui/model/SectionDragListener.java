package info.textgrid.lab.core.aggregations.ui.model;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;

public class SectionDragListener extends DragSourceAdapter {

	private StructuredViewer viewer;
	public SectionDragListener(StructuredViewer viewer) {
		this.viewer = viewer;
	}
	/**
	 * Method declared on DragSourceListener
	 */
	public void dragFinished(DragSourceEvent event) {
		
		if (!event.doit)
			return;

	}
	/**
	 * Method declared on DragSourceListener
	 */
	public void dragSetData(DragSourceEvent event) {
		Object selectionObject = viewer.getSelection();
		//System.out.println(((IStructuredSelection)viewer.getSelection()).getFirstElement().toString());


		if (event.dataType == null || LocalSelectionTransfer.getTransfer().isSupportedType(event.dataType)) {
			event.data = selectionObject;
			LocalSelectionTransfer.getTransfer().setSelection(viewer.getSelection());
		}

	}
	/**
	 * Method declared on DragSourceListener
	 */
	public void dragStart(DragSourceEvent event) {
		event.doit = !viewer.getSelection().isEmpty();
		if (event.doit)
			dragSetData(event);
	}

}
