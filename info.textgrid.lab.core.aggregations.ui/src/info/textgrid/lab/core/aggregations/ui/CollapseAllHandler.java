package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.IAggregationTypeAdapter;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.aggregations.ui.views.Messages;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.ParameterValueConversionException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.common.base.Predicate;

public class CollapseAllHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
//		System.out.println("CollapseAllHandler");
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;

		Aggregation aggregationRoot = acEditor.getAggregationRoot();

		acEditor.getViewer().collapseAll();
		// acEditor.getViewer().refresh(aggregationRoot);

		return null;
	}

}
