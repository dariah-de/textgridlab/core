package info.textgrid.lab.core.aggregations.ui;

import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class ExpandAggregationHandler extends AbstractHandler implements
		IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
//		System.out.println("ExpandAggregationHandler");
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		AggregationComposerEditor acEditor = (AggregationComposerEditor) editor;
		ISelection selection = editor.getSite().getWorkbenchWindow()
				.getSelectionService().getSelection();

		Object obj = ((IStructuredSelection) selection).getFirstElement();

		acEditor.getViewer().expandToLevel(obj, AbstractTreeViewer.ALL_LEVELS);

		return null;
	}

}
