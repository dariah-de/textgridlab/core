package info.textgrid.lab.core.aggregations.ui.model;

import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TextGridObject;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;

/**
 * @author hausner
 * 
 *         Provisional class to represent TGOs in the Web Publish View. In
 *         contrast to a TGO a TGOentry knows about its parent Aggregation.
 *         Maybe this should be replaced with a TGO-adaptable class in order to
 *         allow TGO-operations like opening on TGOentries.
 * 
 */
public class TGOentry implements IAdaptable {

	private Aggregation parent;
	private TextGridObject tgo = null;
	private boolean latest = true;
	private TextGridObject revision = null;
	private RestrictedTextGridObject rtgo = null;

	public TGOentry(String uri) {
		URI tgouri = null;
		try {
			tgouri = new URI(uri);
		} catch (URISyntaxException e) {
			AggregationsUIPlugin.handleError(e, "Aggregation Composer Error.");
		}
		this.tgo = TextGridObject.getInstanceOffline(tgouri);
	}

	public TGOentry(RestrictedTextGridObject rtgo) {
		this.rtgo = rtgo;
	}

	public Aggregation getParent() {
		return parent;
	}

	public void setParent(Aggregation newParent) {
		if (parent != null)
			parent.removeChild(this);
		this.parent = newParent;

	}

	public String getTitle() throws CoreException {
		if (tgo != null)
			return tgo.getTitle();
		else
			return RestrictedTextGridObject.TITLE + " (" + rtgo.getURI() + ")";
	}

	public TextGridObject getTgo() {
		return tgo;
	}

	public void delete() {
		this.parent.removeChild(this);

		this.setParent(null);

	}

	public void setLatest(boolean latest) {
		this.latest = latest;
	}

	public boolean getLatest() {
		return latest;
	}

	public TextGridObject getRevision() {
		return revision;
	}

	public void setRevision(TextGridObject revision) {
		this.revision = revision;
	}

	public boolean isRestrictedTextGridObject() {
		return this.rtgo != null;
	}

	public RestrictedTextGridObject getRestrictedTgo() {
		return this.rtgo;
	}

	public Object getAdapter(Class adapter) {
		if (adapter == TextGridObject.class) {
			return tgo;
		}

		return null;
	}
	
	/**
	 * Returns the URI to use for this entry.
	 */
	public URI getURI() {
		if (getLatest()) {
			if (isRestrictedTextGridObject())
				return URI.create(getRestrictedTgo().getURI());
			else
				return URI.create(getTgo().getLatestURI());
		} else if (getRevision() != null)
			return getRevision().getURI();
		else
			return getTgo().getURI();
	}

	@Override
	public String toString() {
		return tgo.toString();
	}
}
