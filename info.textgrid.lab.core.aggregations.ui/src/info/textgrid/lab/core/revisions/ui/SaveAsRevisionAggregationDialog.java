package info.textgrid.lab.core.revisions.ui;

import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor.SaveType;
import info.textgrid.lab.core.metadataeditor.MetaDataSection;
import info.textgrid.lab.core.metadataeditor.ScrolledPageArea;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * This is a dialogue to save a new revision of the object open 
 * in an Aggregation Composer editor.
 * 
 * @author queens
 * 
 */
public class SaveAsRevisionAggregationDialog extends TitleAreaDialog {

	private MetaDataSection metaDataSection;
	private ProgressMonitorPart progressMonitorPart;
	AggregationComposerEditor editor;
	private TextGridObject tgo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse
	 * .swt.widgets.Composite)
	 */
	protected Control createDialogArea(final Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		setTitle(Messages.SaveAsRevisionAggregationDialog_SaveAsNewRevision);
		getShell().setText(Messages.SaveAsRevisionAggregationDialog_SaveAs);
		setMessage(Messages.SaveAsRevisionAggregationDialog_IM_SaveAsNewRevision);

		ScrolledPageArea scrolledPageArea = new ScrolledPageArea(area);

		Composite composite = scrolledPageArea.getPageAreaComposite();
		ScrolledForm scrolledForm = scrolledPageArea.getPageAreaScrolledForm();

		GridDataFactory.fillDefaults().grab(true, true).hint(600, 400).applyTo(
				composite);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(scrolledForm);

		Group metadataGroup = new Group(composite, SWT.SHADOW_ETCHED_IN);
		metadataGroup.setLayout(new GridLayout(1, false));
		metadataGroup.setText(Messages.SaveAsRevisionAggregationDialog_Metadata);

		metaDataSection = new MetaDataSection(metadataGroup, scrolledForm, null);
		metaDataSection.checkAllFields();

		progressMonitorPart = new ProgressMonitorPart(area, null) {

			private ControlEnableState controlEnableState = null;

			public void beginTask(String name, int totalWork) {
				super.beginTask(name, totalWork);
				if (controlEnableState == null)
					controlEnableState = ControlEnableState.disable(parent);
			}

			public void done() {
				super.done();
				if (controlEnableState != null) {
					controlEnableState.restore();
					controlEnableState = null;
				}
			}
		};
		progressMonitorPart.setLayoutData(GridDataFactory.swtDefaults().align(
				SWT.FILL, SWT.BOTTOM).create());
		progressMonitorPart.setVisible(false);

		return composite;
	}

	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		getButton(OK).setText(Messages.SaveAsRevisionAggregationDialog_Save);
		progressMonitorPart.attachToCancelComponent(getButton(CANCEL));
		return contents;
	}

	public SaveAsRevisionAggregationDialog(Shell parentShell) {
		super(parentShell);
	}
	
	public void init(AggregationComposerEditor editor) {
		this.editor = editor;
		tgo = editor.getAggregationRoot().getObject();
		metaDataSection.setNewTGObject(tgo, true);
	}

	public void okPressed() {
		metaDataSection.updateTextGridObject();
		editor.save(SaveType.REVISION);
		super.okPressed();
	}
}
