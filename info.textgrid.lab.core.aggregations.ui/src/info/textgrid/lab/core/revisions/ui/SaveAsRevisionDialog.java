package info.textgrid.lab.core.revisions.ui;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.metadataeditor.MetaDataSection;
import info.textgrid.lab.core.metadataeditor.ScrolledPageArea;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.locking.LockingService;
import info.textgrid.lab.ui.core.menus.OpenObjectService;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IReusableEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.internal.PartService;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.service.log.LogService;

/**
 * This is a dialogue to save a new revision of the object open in an
 * 
 * @author queens
 * 
 */
public class SaveAsRevisionDialog extends TitleAreaDialog {

	private MetaDataSection metaDataSection;
	private ProgressMonitorPart progressMonitorPart;
	protected IEditorPart editor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse
	 * .swt.widgets.Composite)
	 */
	protected Control createDialogArea(final Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		setTitle(Messages.SaveAsRevisionDialog_SaveAsNewRevision);
		getShell().setText(Messages.SaveAsRevisionDialog_SaveAs);
		setMessage(Messages.SaveAsRevisionDialog_IM_SaveNewRevision);

		ScrolledPageArea scrolledPageArea = new ScrolledPageArea(area);

		Composite composite = scrolledPageArea.getPageAreaComposite();
		ScrolledForm scrolledForm = scrolledPageArea.getPageAreaScrolledForm();

		GridDataFactory.fillDefaults().grab(true, true).hint(600, 400).applyTo(
				composite);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(scrolledForm);

		Group metadataGroup = new Group(composite, SWT.SHADOW_ETCHED_IN);
		// GridDataFactory.fillDefaults().grab(true, true).hint(600,
		// 500).applyTo(
		// metadataGroup);
		metadataGroup.setLayout(new GridLayout(1, false));
		metadataGroup.setText(Messages.SaveAsRevisionDialog_Metadata);

		metaDataSection = new MetaDataSection(metadataGroup, scrolledForm, null);
		metaDataSection.checkAllFields();

		progressMonitorPart = new ProgressMonitorPart(area, null) {

			private ControlEnableState controlEnableState = null;

			public void beginTask(String name, int totalWork) {
				super.beginTask(name, totalWork);
				if (controlEnableState == null)
					controlEnableState = ControlEnableState.disable(parent); // ,
				// ImmutableList.of(getButton(CANCEL),
				// getTray(),
				// this));
			}

			public void done() {
				super.done();
				if (controlEnableState != null) {
					controlEnableState.restore();
					controlEnableState = null;
				}
			}
		};
		progressMonitorPart.setLayoutData(GridDataFactory.swtDefaults().align(
				SWT.FILL, SWT.BOTTOM).create());
		progressMonitorPart.setVisible(false);

		return composite;
	}

	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		getButton(OK).setText(Messages.SaveAsRevisionDialog_Save);
		progressMonitorPart.attachToCancelComponent(getButton(CANCEL));
		return contents;
	}

	public SaveAsRevisionDialog(Shell parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Runs the given runnable with the dialog's progress monitor, if available,
	 * or with the Workbench {@linkplain IWorkbench#getProgressService()
	 * progress service} otherwise.
	 * 
	 * @param runnable
	 *            the runnable that will be run.
	 * @param fork
	 *            if <code>false</code>, run in the UI thread, otherwise, the
	 *            implementation may fork to a separate thread
	 * @return A status indicating whether the runnable has completed
	 *         successfully. Note that if the runnable encapsulates a
	 *         {@link CoreException} the result will be a MultiStatus containing
	 *         the {@link CoreException}'s status.
	 */
	protected IStatus run(final IRunnableWithProgress runnable,
			final boolean fork) {
		try {
			if (progressMonitorPart != null
					&& !progressMonitorPart.isDisposed()) {
				progressMonitorPart.setVisible(true);
				try {
					ModalContext.run(runnable, fork, progressMonitorPart,
							getShell().getDisplay());
				} finally {
					progressMonitorPart.done();
					progressMonitorPart.setVisible(false);
				}
			} else {
				PlatformUI.getWorkbench().getProgressService().run(fork, true,
						runnable);
			}
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof CoreException) {
				CoreException c = (CoreException) e.getCause();
				return new MultiStatus(c.getStatus().getPlugin(), 0,
						new IStatus[] { c.getStatus() }, c.getMessage(), c);
			} else {
				return new Status(IStatus.ERROR,
						AggregationsUIPlugin.PLUGIN_ID, e.getCause()
								.getLocalizedMessage(), e);
			}
		} catch (InterruptedException e) {
			return Status.CANCEL_STATUS;
		}
		return Status.OK_STATUS;
	}

	private TextGridObject newRevision;
	private TextGridObject currentRevision;

	public void associateWithEditor(final IEditorPart editor) {
		final IStatus status = run(new IRunnableWithProgress() {

			public void run(final IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				final SubMonitor progress = SubMonitor.convert(monitor, 10);
				if (editor != null) {
					SaveAsRevisionDialog.this.editor = editor;
					final IEditorInput editorInput = editor.getEditorInput();
					currentRevision = AdapterUtils.getAdapter(
							editorInput, TextGridObject.class);
					progress.worked(1);
					
					if (currentRevision == null) {
						throw new InvocationTargetException(
								new CoreException(
										new Status(
												IStatus.ERROR,
												AggregationsUIPlugin.PLUGIN_ID,
												NLS.bind(Messages.SaveAsRevisionDialog_EM_NoTextGridObject, editorInput))));
					}
						

					// TODO the following stuff probably needs to be factored
					// out of here for support of non-editors like the metadata
					// editor etc.
					progress.setTaskName(NLS.bind(Messages.SaveAsRevisionDialog_PreparingNewRevisionOf, currentRevision));
					newRevision = currentRevision.prepareNewRevision(progress.newChild(5));
					AdapterUtils.getAdapter(newRevision, IFile.class); // force
					// file
					// creation;
					progress.worked(1);

					if (metaDataSection != null)
						metaDataSection.setNewTGObject(newRevision, true);
	
					progress.worked(4);

					// TODO do we need to do more stuff?

				}
				progress.done();
			}
		}, false);
		if (!status.isOK())
			StatusManager.getManager().handle(status); // FIXME display this
		// somehow?
	}

	private static void log(String message, Object... arguments) {
		StatusManager.getManager().handle(
				new Status(IStatus.INFO, AggregationsUIPlugin.PLUGIN_ID,
						MessageFormat.format(message, arguments)));
	}

	/** public for testing purposes */
	public void okPressed() {
		metaDataSection.updateTextGridObject();
		try {
			newRevision.setProject(currentRevision.getProjectInstance());
		} catch (IllegalStateException e) {
			AggregationsUIPlugin.handleError(e, NLS.bind(Messages.SaveAsRevisionDialog_EM_RetvievingProject, currentRevision.toString()));
		} catch (CoreException e) {
			AggregationsUIPlugin.handleError(e, NLS.bind(Messages.SaveAsRevisionDialog_EM_RetvievingProject, currentRevision.toString()));
		}
		
		IStatus result = run(new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(
						Messages.SaveAsRevisionDialog_Saving, newRevision), 10);
				if (editor != null) {
					progress.worked(1);
					if (editor.isDirty())
						editor.doSave(progress.newChild(6));
					else
						// most editors don't save if nothing has been modified
						try {
							IFile currentFile = getAdapter(currentRevision, IFile.class);
							// following will actually save to the new file
							// as described in
							// TextGridObject.prepareNewRevision():
							currentFile.setContents(currentFile.getContents(), true, false, progress.newChild(6));
						} catch (CoreException e1) {
							StatusManager.getManager().handle(e1, AggregationsUIPlugin.PLUGIN_ID);
						}

					log(Messages.SaveAsRevisionDialog_Saved, newRevision);

					IEditorInput newInput = null;
					
					progress.worked(1);
					if (editor instanceof IReusableEditor) {
						final IReusableEditor reusableEditor = (IReusableEditor) editor;
						if (LockingService.getInstance().lockObject(newRevision, true)) {
							newInput = getAdapter(newRevision, IEditorInput.class);
							reusableEditor.setInput(newInput);
							log("Set reusable editor {0} to open new input {1}", //$NON-NLS-1$
									editor, newInput);
						} else {
							reusableEditor.getEditorSite().getPage().closeEditor(reusableEditor, true);
						}
							
					} else {
						final IWorkbenchPage page = editor.getEditorSite()
								.getPage();
						page.closeEditor(editor, false);
						if (LockingService.getInstance().lockObject(newRevision, true)) {
							try {
								newInput = getAdapter(newRevision,
										IEditorInput.class);
								page.openEditor(newInput, editor.getSite().getId());
								log("Opened new editor {0} for input {1}", //$NON-NLS-1$
										editor, newInput);
							} catch (final PartInitException e) {
								throw new InvocationTargetException(e);
							}
						}
					}
				}
			}
		}, false);
		if (result.isOK())
			super.okPressed();
		else
			StatusManager.getManager().handle(result,
					StatusManager.LOG | StatusManager.SHOW);
	}

	@Override
	protected void cancelPressed() {
		currentRevision.cancelNewRevision();
		super.cancelPressed();
	}
}
