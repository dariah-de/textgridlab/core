package info.textgrid.lab.core.revisions.ui;

import info.textgrid.lab.core.aggregations.ui.AggregationsUIPlugin;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;

import java.math.BigInteger;
import java.util.Collections;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Handler for saving the object in the current editor as a new revision.
 * Does currently only support editors in which a TextGridObject is edited.
 */
public class SaveNewRevisionHandler extends AbstractHandler implements IHandler {

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (editor != null) {
			TextGridObject tgo = AdapterUtils.getAdapter(
					editor.getEditorInput(), TextGridObject.class);
			if (tgo != null) {
				try {
					if (!tgo.getProjectInstance().hasPermissions(ITextGridPermission.CREATE)) {
						MessageDialog.openError(Display.getCurrent().getActiveShell(), Messages.SaveNewRevisionHandler_SaveAsNewRevision, 
						NLS.bind(Messages.SaveNewRevisionHandler_EM_NoPermission, tgo.getProjectInstance().getName()));
						return null;
					}
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, AggregationsUIPlugin.PLUGIN_ID);
				}
				
				Revisions revisions = new SearchRequest().getSearchClient().listRevisions(tgo.getURI().toString());
				if (!revisions.getRevision().isEmpty()) {
					BigInteger currentRevision = new BigInteger(tgo.getRevisionNumber());
					BigInteger lastRevision = Collections.max(revisions.getRevision());
					if (!currentRevision.equals(lastRevision)) {
						if (!MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), Messages.SaveAsRevisionDialog_SaveAsNewRevision, NLS.bind(Messages.SaveNewRevisionHandler_LaterRevisionExists, currentRevision, lastRevision)))
							return null;
					}
				}
				
				SaveAsRevisionDialog dialog = new SaveAsRevisionDialog(HandlerUtil.getActiveShell(event));
				dialog.setBlockOnOpen(false);
				dialog.open();
				dialog.associateWithEditor(editor);
			}	
		} else {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, AggregationsUIPlugin.PLUGIN_ID, Messages.SaveNewRevisionHandler_EM_NoOpenEditor), StatusManager.SHOW);
		}
		return null;
	}
}
