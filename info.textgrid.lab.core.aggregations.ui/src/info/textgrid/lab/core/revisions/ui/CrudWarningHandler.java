package info.textgrid.lab.core.revisions.ui;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.versions.ui.CrudWarningView;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Handler opens the CRUD-Warning view.
 */
public class CrudWarningHandler extends AbstractHandler implements IHandler {

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {

			Object obj = ((IStructuredSelection) selection).getFirstElement();
			TextGridProject tgp = null;
			
			if (obj instanceof TGObjectReference)
				try {
					tgp = ((TGObjectReference)obj).getTgo().getProjectInstance();
				} catch (CoreException e) {
					tgp = null;
				}
			else if (obj instanceof TextGridProject)
				tgp = (TextGridProject) obj;
			
			try {
				IViewPart viewPart = PlatformUI
						.getWorkbench()
						.getActiveWorkbenchWindow()
						.getActivePage()
						.showView(
								"info.textgrid.lab.core.aggregations.ui.crudwarningview", //$NON-NLS-1$
								null, IWorkbenchPage.VIEW_ACTIVATE);
				((CrudWarningView)viewPart).refreshViewer(tgp);
			} catch (PartInitException e) {
				Activator.handleError(e, Messages.CrudWarningHandler_EM_CouldNotOpenView);
			}
		}

		return null;

	}
}
