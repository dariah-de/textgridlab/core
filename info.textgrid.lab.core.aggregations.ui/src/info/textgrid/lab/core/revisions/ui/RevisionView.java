package info.textgrid.lab.core.revisions.ui;

import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;

import java.util.EnumSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;


public class RevisionView extends ViewPart {
	private TextGridObjectTableViewer viewer;
	private TextGridObject tgo;
	private Composite composite;
	private Label tgoLabel;
	private final ISelectionListener tgoSelectedListener;
	private ITextGridObjectListener objectListener;


	/**
	 * The constructor.
	 */
	public RevisionView() {
		tgoSelectedListener = new ISelectionListener() {
			public void selectionChanged(IWorkbenchPart sourcepart,
					ISelection selection) {
					if (sourcepart.getSite().getId().equals("info.textgrid.lab.navigator.view") //$NON-NLS-1$
						&& selection instanceof IStructuredSelection) {
						final IStructuredSelection iss = (IStructuredSelection) selection;
						// take first selected element
						if (!iss.isEmpty()) {
							Object firstelem = iss.toArray()[0];
							if (firstelem instanceof TGObjectReference) {
								refreshViewer(((TGObjectReference) firstelem).getTgo());
							} else {
								refreshViewer(null);
							}
						} else {
							refreshViewer(null);
						}
				}
			}
		};
		
		objectListener = new ITextGridObjectListener(){
			public void textGridObjectChanged(
					info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event event,
					TextGridObject object) {
				
				if (event == Event.DELETED)
					viewer.remove(object);
			}	
		};
		
		TextGridObject.addListener(objectListener);
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		composite = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		composite.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		composite.setLayout(glp);
		
		tgoLabel = new Label(composite, SWT.LEFT);
		
		viewer = new TextGridObjectTableViewer(composite, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL);
		
		viewer.setVisibleColumns(EnumSet.of(Column.REVISION, Column.TITLE, Column.PROJECT, Column.DATE, Column.OWNER));
		viewer.getTable().setHeaderVisible(true);
		
		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		viewer.getControl().setLayoutData(gd1);
		getSite().setSelectionProvider(viewer);
		getSite().getPage().addSelectionListener(tgoSelectedListener);

		hookContextMenu(viewer);
	}
	
	public void refreshViewer(final TextGridObject tgo) {
		if (this.tgo == tgo)
			return;
		
		setTgo(tgo);
		
		if (tgo == null) {
			tgoLabel.setText("");
			viewer.setInput(null);
			return;
		}
		
		try {
			tgoLabel.setText(NLS
					.bind(Messages.RevisionView_RevisionsOfInProject,
							getTgo().getTitle().replaceAll("&", "&&"), getTgo().getProjectInstance().getName().replaceAll("&", "&&"))
							);
		} catch (CoreException e) {
			Activator.handleError(e);
		}
		composite.layout();
			
		final SearchRequest searchRequest = new SearchRequest();
		
		UIJob job = new UIJob(Messages.RevisionView_RetrieveRevisions) {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					SubMonitor progress = SubMonitor.convert(monitor);
					progress.beginTask(NLS.bind(Messages.RevisionView_QueryForChildren, tgo.getTitle()), 100);
					searchRequest.setQueryRevision(getTgo().getURI().toString());
					viewer.setInput(searchRequest);
				return Status.OK_STATUS;
				} catch (CoreException e) {
					Activator.handleError(e);
					return Status.CANCEL_STATUS;
				} 
			}
		};
		job.setUser(true);
		job.schedule();		
	}

	private void hookContextMenu(final TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public TextGridObject getTgo() {
		return tgo;
	}

	public void setTgo(TextGridObject tgo) {
		this.tgo = tgo;
	}
	
	@Override
	public void dispose() {
		if (tgoSelectedListener != null) {
			getSite().getPage().removeSelectionListener(tgoSelectedListener);
		}
		
		if (objectListener != null) {
			TextGridObject.removeListener(objectListener);
		}
		
		super.dispose();
	}

}
