package info.textgrid.lab.core.revisions.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.revisions.ui.messages"; //$NON-NLS-1$
	public static String CrudWarningHandler_EM_CouldNotOpenView;
	public static String OpenRevisionViewHandler_EM_CouldNotOpenView;
	public static String RevisionSelectDialog_IM_SelectRevision;
	public static String RevisionSelectDialog_QueryingRevisions;
	public static String RevisionSelectDialog_ReferToRevision;
	public static String RevisionSelectDialog_RetrieveRevisions;
	public static String RevisionView_QueryForChildren;
	public static String RevisionView_RetrieveRevisions;
	public static String RevisionView_RevisionsOfInProject;
	public static String SaveAsRevisionAggregationDialog_IM_SaveAsNewRevision;
	public static String SaveAsRevisionAggregationDialog_Metadata;
	public static String SaveAsRevisionAggregationDialog_Save;
	public static String SaveAsRevisionAggregationDialog_SaveAs;
	public static String SaveAsRevisionAggregationDialog_SaveAsNewRevision;
	public static String SaveAsRevisionDialog_EM_NoTextGridObject;
	public static String SaveAsRevisionDialog_EM_RetvievingProject;
	public static String SaveAsRevisionDialog_IM_SaveNewRevision;
	public static String SaveAsRevisionDialog_Metadata;
	public static String SaveAsRevisionDialog_PreparingNewRevisionOf;
	public static String SaveAsRevisionDialog_Save;
	public static String SaveAsRevisionDialog_SaveAs;
	public static String SaveAsRevisionDialog_SaveAsNewRevision;
	public static String SaveAsRevisionDialog_Saved;
	public static String SaveAsRevisionDialog_Saving;
	public static String SaveNewRevisionHandler_EM_NoOpenEditor;
	public static String SaveNewRevisionHandler_EM_NoPermission;
	public static String SaveNewRevisionHandler_EM_SuccessiveRevision;
	public static String SaveNewRevisionHandler_LaterRevisionExists;
	public static String SaveNewRevisionHandler_SaveAsNewRevision;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
