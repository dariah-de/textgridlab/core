package info.textgrid.lab.core.revisions.ui;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;


public class OpenRevisionViewHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {

			Object obj = ((IStructuredSelection) selection).getFirstElement();

			if (obj instanceof IAdaptable) {
				TextGridObject tgo = (TextGridObject) ((IAdaptable) obj)
						.getAdapter(TextGridObject.class);
				try {
					((RevisionView) PlatformUI
							.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage()
							.showView(
									"info.textgrid.lab.core.aggregations.ui.revisionview", //$NON-NLS-1$
									null, IWorkbenchPage.VIEW_ACTIVATE))
							.refreshViewer(tgo);

				} catch (PartInitException e) {
					Activator.handleError(e, Messages.OpenRevisionViewHandler_EM_CouldNotOpenView);
				}
			}
		}

		return null;

	}
}
