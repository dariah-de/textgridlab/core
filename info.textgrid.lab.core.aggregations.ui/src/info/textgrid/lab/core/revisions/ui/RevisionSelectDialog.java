package info.textgrid.lab.core.revisions.ui;

import info.textgrid.lab.core.aggregations.ui.model.Aggregation;
import info.textgrid.lab.core.aggregations.ui.model.TGOentry;
import info.textgrid.lab.core.metadataeditor.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;

import java.util.EnumSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.progress.UIJob;


public class RevisionSelectDialog extends TitleAreaDialog {	
	private TextGridObjectTableViewer viewer;
	private Button buttonOK;
	private Object objTest;
	private TextGridObject tgo;
	private Object selObj;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public RevisionSelectDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle(Messages.RevisionSelectDialog_ReferToRevision);
		setMessage(Messages.RevisionSelectDialog_IM_SelectRevision, IMessageProvider.INFORMATION);
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		GridData gdMain = new GridData(SWT.FILL, SWT.FILL, true, true);
		container.setLayoutData(gdMain);
		
		viewer = new TextGridObjectTableViewer(container, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL);
		
		viewer.setVisibleColumns(EnumSet.of(Column.REVISION, Column.TITLE, Column.PROJECT, Column.DATE, Column.OWNER));
		viewer.getTable().setHeaderVisible(true);
		
		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		viewer.getControl().setLayoutData(gd1);
		viewer.setSorter(new ViewerSorter());
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
				selObj = sel.getFirstElement();
				buttonOK.setEnabled(true);
			}
		});
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			
			public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
				selObj = sel.getFirstElement();
				okPressed();
			}
		});
	
		return area;
	}
	
	public void refreshDialog(final Object obj) {
		this.objTest = obj;
		if (obj instanceof Aggregation) {
			setTgo(((Aggregation)obj).getObject());
		} else if (obj instanceof TGOentry) {
			setTgo(((TGOentry)obj).getTgo());
		}
		
		final SearchRequest searchRequest = new SearchRequest();
		
		UIJob job = new UIJob(Messages.RevisionSelectDialog_RetrieveRevisions) {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					SubMonitor progress = SubMonitor.convert(monitor);
					progress.beginTask(NLS.bind(Messages.RevisionSelectDialog_QueryingRevisions, tgo.getTitle()), 100);
					searchRequest.setQueryRevision(getTgo().getURI().toString());
					viewer.setInput(searchRequest);
				return Status.OK_STATUS;
				} catch (CoreException e) {
					Activator.handleError(e);
					return Status.CANCEL_STATUS;
				} 
			}
		};
		job.setUser(true);
		job.schedule();		
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		buttonOK = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, false);
		buttonOK.setEnabled(false);

		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	@Override
	protected void okPressed() {
		super.okPressed();
		if (selObj instanceof TextGridObject) {
			if (objTest instanceof Aggregation) {
				((Aggregation)objTest).setRevision((TextGridObject) selObj);
				((Aggregation)objTest).setLatest(false);
			} else if (objTest instanceof TGOentry) {
				((TGOentry)objTest).setRevision((TextGridObject) selObj);
				((TGOentry)objTest).setLatest(false);
			}
		}
	};

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800, 400); 
	}
	
	public TextGridObject getTgo() {
		return tgo;
	}

	public void setTgo(TextGridObject tgo) {
		this.tgo = tgo;
	}
}
