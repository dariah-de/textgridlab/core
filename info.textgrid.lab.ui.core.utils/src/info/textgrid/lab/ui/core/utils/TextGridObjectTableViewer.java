package info.textgrid.lab.ui.core.utils;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static info.textgrid.lab.ui.core.utils.Activator.PLUGIN_ID;

import java.text.SimpleDateFormat;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.JFaceColors;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.progress.IDeferredWorkbenchAdapter;
import org.eclipse.ui.progress.PendingUpdateAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.newsearch.SearchRequest;

/**
 * A {@link TableViewer} that can display {@link TextGridObject}s and is
 * prepared for fetching them in the background.
 */
public class TextGridObjectTableViewer extends TableViewer {

	protected class ColumnComparator extends ViewerComparator {
		private EnumMap<Column, SortColumn> sortColumns = Maps.newEnumMap(Column.class);
		private SortColumn currentColumn = null; 
		

		private class SortColumn {

			public final TableColumn tableColumn;
			public final Table table;
			public final Column columnType;
			private int direction = 1;
			public final ColumnViewer viewer;

			public SortColumn(TableViewerColumn column, Column columnType) {
				this.columnType = columnType;
				this.tableColumn = column.getColumn();
				this.table = tableColumn.getParent();
				viewer = column.getViewer();

				tableColumn.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						if (table.getSortColumn() == tableColumn)
							direction *= -1;
						activate();
						viewer.refresh();
					}
				});
			}

			public SortColumn activate() {
				table.setSortColumn(tableColumn);
				table.setSortDirection(direction < 0? SWT.UP : SWT.DOWN);
				currentColumn = this;
				return this;
			}

			@SuppressWarnings("rawtypes")
			protected Comparable getProperty(Object object) {

				TextGridObject textGridObject = AdapterUtils.getAdapter(object, TextGridObject.class);
				if (textGridObject == null) {
					if (object != null)
						return object.toString();
					else
						return "";
				}
				try {
					switch(columnType) {
					case TITLE: 
						return textGridObject.getTitle();
					case AUTHORS: return Joiner.on(", ").join(textGridObject.getAuthors());
					case DATE: return textGridObject.getLastModified();
					case OWNER: return textGridObject.getMetadataForReading().getGeneric().getGenerated().getDataContributor();
					case PROJECT: return textGridObject.getProjectInstance().getName();
					case REVISION: return Integer.valueOf(textGridObject.getRevisionNumber());
					case WARNING: return textGridObject.getCrudWarning();
					}
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				}
				return object.toString();
			}
		}

		
		public ColumnComparator registerColumn(Column columnType, TableViewerColumn viewerColumn) {
			sortColumns.put(columnType, new SortColumn(viewerColumn, columnType));
			return this;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public int compare(Viewer aViewer, Object e1, Object e2) {
			if (e1 instanceof PendingUpdateAdapter)
				return -1;
			else if (e2 instanceof PendingUpdateAdapter)
				return 1;
			if (currentColumn != null)
				return currentColumn.direction * currentColumn.getProperty(e1).compareTo(currentColumn.getProperty(e2));
			else
				return e1.toString().compareTo(e2.toString());
		}

		public void activate(Column column) {
			sortColumns.get(column).activate();
		}
	}

	public enum Column {
		REVISION, TITLE, PROJECT, AUTHORS, OWNER, DATE, WARNING
	}

	/** cache for pre-setting the visible columns */
	private EnumSet<Column> visibleColumns;

	private final EnumMap<Column, TableViewerColumn> columns = Maps
			.newEnumMap(Column.class);

	public TableViewerColumn getColumn(final Column column) {
		return columns.get(column);
	}

	public void setVisibleColumns(final EnumSet<Column> columnsToShow) {
		visibleColumns = columnsToShow;
		for (Column column : Column.values()) {
			TableViewerColumn tableViewerColumn = columns.get(column);
			if (tableViewerColumn != null) {
				TableColumn tableColumn = tableViewerColumn.getColumn();
				if (columnsToShow.contains(column)) {
					Object data = tableColumn.getData("defaultwidth");
					int width;
					if (data != null && data instanceof Integer)
						width = (Integer) data;
					else if (column.equals(Column.WARNING))
						width = 800;
					else 	
						width = 200;
					tableColumn.setWidth(width);
					tableColumn.setResizable(true);
				} else {
					int width = tableColumn.getWidth();
					if (width > 0)
						tableColumn.setData("defaultwidth", width);
					tableColumn.setWidth(0);
					tableColumn.setResizable(false);
				}
			}
		}
		comparator.activate(visibleColumns.iterator().next());
	}

	/**
	 * A {@link IStyledLabelProvider} that labels the project field of
	 * TextGridObjects.
	 * 
	 * @author vitt
	 */
	public class TGOProjectLabelProvider extends BaseLabelProvider implements IStyledLabelProvider {

		public Image getImage(Object element) {
			return null; // no image
		}

		public StyledString getStyledText(Object element) {
			if (element instanceof PendingUpdateAdapter)
				return new StyledString(); // one label per table is enough

			TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
			if (textGridObject != null) {
				TextGridProject project;
				try {
					project = textGridObject.getProjectInstance();
					StyledString label = new StyledString(project.getName());
					label.append(' ').append(project.getId(), StyledString.DECORATIONS_STYLER);
					return label;
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
					return new StyledString(e.getLocalizedMessage(), ERROR_STYLER);
				}
			}
			return new StyledString(element.toString()).append(" ?", ERROR_STYLER);
		}

	}

	private UpdatingDeferredListContentProvider defaultContentProvider;
	private TextGridObject[] defaultSelection = null;
	/**
	 * A styler used for error messages
	 */
	public static final Styler ERROR_STYLER = new Styler() {

		public void applyStyles(TextStyle textStyle) {
			textStyle.foreground = JFaceColors.getErrorText(Display.getCurrent());
			textStyle.background = JFaceColors.getErrorBackground(Display.getCurrent());
		}

	};

	private ColumnComparator comparator;

	/**
	 * Creates a new {@link TextGridObject} {@link TableViewer} with a default
	 * label provider and an {@link UpdatingDeferredListContentProvider}. You
	 * must either call {@link #setInput(Object)} with some input that has a
	 * {@link IDeferredWorkbenchAdapter}, or set a different content provider
	 * before you assign any input.
	 * 
	 * <dl>
	 * <dt><b>Styles:</b></dt>
	 * <dd>SINGLE, MULTI, CHECK, FULL_SELECTION, HIDE_SELECTION, VIRTUAL</dd>
	 * <dt><b>Events:</b></dt>
	 * <dd>Selection, DefaultSelection, SetData, MeasureItem, EraseItem,
	 * PaintItem</dd>
	 * </dl>
	 * </p>
	 * <p>
	 * Note: Only one of the styles SINGLE, and MULTI may be specified.
	 * 
	 * 
	 * @param parent
	 *            the parent on which to create the {@link Table} control.
	 * @param style
	 *            style bits for the {@link Table} control
	 */
	public TextGridObjectTableViewer(Composite parent, int style) {
		super(parent, style);
		init();
	}

	/**
	 * Returns the default content provider. If you don't set a content provider
	 * yourself, this one will be used.
	 */
	public UpdatingDeferredListContentProvider getDefaultContentProvider() {
		return defaultContentProvider;
	}

	/**
	 * Registers a default selection for this content provider. This method may
	 * be called before {@link #setInput(Object)}.
	 * 
	 * <p>
	 * If you use the default content provider, you cannot call
	 * {@link #setSelection(org.eclipse.jface.viewers.ISelection, boolean)}
	 * immediately after {@link #setInput(Object)} because the viewer will only
	 * contain a {@link PendingUpdateAdapter} by then. When you set a default
	 * selection instead using this method, these objects will be selected after
	 * the content provider has finished loading (unless the user has already
	 * selected something else).
	 * </p>
	 * 
	 * @param defaultSelection
	 *            One or more {@link TextGridObject}s to select after loading.
	 */
	public void setDefaultSelection(TextGridObject... defaultSelection) {
		this.defaultSelection = defaultSelection;
	}

	/**
	 * Initializes the viewer. Override this to change the whole default
	 * behaviour, including the content and label providers.
	 */
	protected void init() {
		initColumns();

		defaultContentProvider = new UpdatingDeferredListContentProvider(TextGridObject.class);
		defaultContentProvider.addDoneListener(new IDoneListener() {

			public void loadDone(Viewer viewer) {
				if (viewer == TextGridObjectTableViewer.this && getContentProvider() == defaultContentProvider
						&& defaultSelection != null) {
					if (getSelection().isEmpty())
						setSelection(new StructuredSelection(defaultSelection), true);
				}
			}
		});
		setContentProvider(defaultContentProvider);
		// defaultLabelProvider = new TextGridObjectTableLabelProvider();
		// setLabelProvider(defaultLabelProvider);
		ColumnViewerToolTipSupport.enableFor(this);
	}

	/**
	 * Initializes the {@linkplain TableColumn Table's columns}. Called from
	 * {@link #init()}. If you override this, you will probably also need to
	 * {@linkplain #setLabelProvider(org.eclipse.jface.viewers.IBaseLabelProvider)
	 * set an appropriate label provider}.
	 */
	protected void initColumns() {
		final TableViewerColumn revisionColumn = new TableViewerColumn(this,
				SWT.NONE);
		revisionColumn.getColumn().setText("Revision");
		revisionColumn.getColumn().setWidth(50);
		revisionColumn.setLabelProvider(new ColumnLabelProvider() {
			public String getText(Object element) {
				if (element instanceof PendingUpdateAdapter)
					return null;
				TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
				if (textGridObject != null)
					return textGridObject.getRevisionNumber();
				return null;
			}
		});
		columns.put(Column.REVISION, revisionColumn);
		
		final TableViewerColumn titleColumn = new TableViewerColumn(this,
				SWT.NONE);
		titleColumn.getColumn().setText(Messages.TextGridObjectTableViewer_Title);
		titleColumn.getColumn().setWidth(200);
		titleColumn.setLabelProvider(new TGODefaultLabelProvider(true));
		columns.put(Column.TITLE, titleColumn);

		final TableViewerColumn projectColumn = new TableViewerColumn(this,
				SWT.NONE);
		projectColumn.getColumn().setText(Messages.TextGridObjectTableViewer_Project);
		projectColumn.getColumn().setWidth(125);
		projectColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new TGOProjectLabelProvider()));
		columns.put(Column.PROJECT, projectColumn);

		final TableViewerColumn authorsColumn = new TableViewerColumn(this,
				SWT.NONE);
		authorsColumn.getColumn().setText(Messages.TextGridObjectTableViewer_Author);
		authorsColumn.getColumn().setWidth(150);
		authorsColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				if (element instanceof PendingUpdateAdapter)
					return null;
				TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
				if (textGridObject != null)
					try {
						return Joiner.on(", ").join(textGridObject.getAuthors());
					} catch (CoreException e) {
						StatusManager.getManager().handle(e, PLUGIN_ID);
					}
				return null;
			}
		});
		columns.put(Column.AUTHORS, authorsColumn);
		
		final TableViewerColumn ownerColumn = new TableViewerColumn(this,
				SWT.NONE);
		ownerColumn.getColumn().setText(Messages.TextGridObjectTableViewer_DataContributor);
		ownerColumn.getColumn().setWidth(125);
		ownerColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				if (element instanceof PendingUpdateAdapter)
					return null;
				TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
				if (textGridObject != null)
					try {
						String owner = textGridObject.getDataContributor();
						if (owner.equals("")) {
							// reload metadata only if ownerColumn is visible
							if (visibleColumns != null)
								if (visibleColumns.contains(Column.OWNER)) {
									textGridObject.reloadMetadata(true);
									owner = textGridObject.getDataContributor();
								}	
						}
						return owner;
					} catch (CoreException e) {
						StatusManager.getManager().handle(e, PLUGIN_ID);
					}
				return null;
			}
		});
		columns.put(Column.OWNER, ownerColumn);
		
		final TableViewerColumn dateColumn = new TableViewerColumn(this,
				SWT.NONE);
		dateColumn.getColumn().setText(Messages.TextGridObjectTableViewer_CreationDate);
		dateColumn.getColumn().setWidth(125);
		final SimpleDateFormat sdFormatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss"); //$NON-NLS-1$
		dateColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				if (element instanceof PendingUpdateAdapter)
					return null;
				TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
				if (textGridObject != null)
					try {
						return sdFormatter.format(textGridObject.getCreated().getTime());
					} catch (CoreException e) {
						StatusManager.getManager().handle(e, PLUGIN_ID);
					}
				return null;
			}
		});
		columns.put(Column.DATE, dateColumn);
		
		
		
		final TableViewerColumn warningColumn = new TableViewerColumn(this,
				SWT.NONE);
		warningColumn.getColumn().setText(Messages.TextGridObjectTableViewer_Warning);
		warningColumn.getColumn().setWidth(800);
		warningColumn.setLabelProvider(new ColumnLabelProvider() {

			public String getText(Object element) {
				// retrieve warning only if warningColumn is visible
				if (visibleColumns != null)
					if (visibleColumns.contains(Column.WARNING)) {
						if (element instanceof PendingUpdateAdapter)
							return null;
						TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
						if (textGridObject != null)
							try {
								String warning = textGridObject.getCrudWarning();
								return warning;
							} catch (CoreException e) {
								StatusManager.getManager().handle(e, PLUGIN_ID);
							}
					}		
				return null;
			}
		});
		columns.put(Column.WARNING, warningColumn);
		
		comparator = new ColumnComparator();
		for (Column columnType : columns.keySet()) {
			comparator.registerColumn(columnType, columns.get(columnType));
		}
		setComparator(comparator);

		getTable().setLinesVisible(true);
		getTable().setHeaderVisible(true);

	}

	/**
	 * Convenience method that creates a metadata query to TG-Search and fills
	 * this viewer correspondingly. This is expects the default content provider
	 * or something similar.
	 * 
	 * @param queryFields
	 *            the metadata fields to query.
	 * @see SearchRequest#setQueryMetadata(Map)
	 * 
	 */
	public void queryMetadata(String query) {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setQueryMetadata(query);
//		searchRequest.setAllProjects(true);
		setInput(searchRequest);
	}

}
