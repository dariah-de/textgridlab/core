package info.textgrid.lab.ui.core.utils;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.net.URI;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.util.Policy;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

public class TextGridObjectTransfer extends ByteArrayTransfer {

	private static final String TYPE_NAME = "textgrid-object-transfer-format" + (new Long(System.currentTimeMillis())).toString(); //$NON-NLS-1$;

	private static final int TYPEID = registerType(TYPE_NAME);

	private static final TextGridObjectTransfer INSTANCE = new TextGridObjectTransfer();

	private ISelection copiedSelection;

	private ArrayList<URI> selection;

	private long selectionSetTime;

	/**
	 * Only the singleton instance of this class may be used.
	 */
	protected TextGridObjectTransfer() {
		// do nothing
	}

	/**
	 * Returns the singleton.
	 * 
	 * @return the singleton
	 */
	public static TextGridObjectTransfer getTransfer() {
		return INSTANCE;
	}

	/**
	 * Tests whether native drop data matches this transfer type.
	 * 
	 * @param result
	 *            result of converting the native drop data to Java
	 * @return true if the native drop data does not match this transfer type.
	 *         false otherwise.
	 */
	private boolean isInvalidNativeType(Object result) {
		return !(result instanceof byte[])
				|| !TYPE_NAME.equals(new String((byte[]) result));
	}

	/**
	 * Returns the type id used to identify this transfer.
	 * 
	 * @return the type id used to identify this transfer.
	 */
	protected int[] getTypeIds() {
		return new int[] { TYPEID };
	}

	/**
	 * Returns the type name used to identify this transfer.
	 * 
	 * @return the type name used to identify this transfer.
	 */
	protected String[] getTypeNames() {
		return new String[] { TYPE_NAME };
	}

	/**
	 * Overrides org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(Object,
	 * TransferData). Only encode the transfer type name since the selection is
	 * read and written in the same process.
	 * 
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(java.lang.Object,
	 *      org.eclipse.swt.dnd.TransferData)
	 */
	public void javaToNative(Object object, TransferData transferData) {
		byte[] check = TYPE_NAME.getBytes();
		super.javaToNative(check, transferData);
	}

	public ISelection getCopiedSelection() {
		return copiedSelection;
	}

	public void setCopiedSelection(ISelection copiedSelection) {
		this.copiedSelection = copiedSelection;
	}

	public ArrayList<URI> getCopiedSelectionAsUriList() {
		ArrayList<URI> copiedTGObjects = new ArrayList<URI>();
		if (copiedSelection instanceof IStructuredSelection) {
			IStructuredSelection structSel = (IStructuredSelection) copiedSelection;
			final Object[] objects = structSel.toArray();
			final TextGridObject[] textGridObjects = AdapterUtils.getAdapters(
					objects, TextGridObject.class, false);
			for (TextGridObject tgo : textGridObjects) {
				copiedTGObjects.add(tgo.getURI());
			}
			return copiedTGObjects;
		}
		return null;
	}
	
	public ArrayList<String> getCopiedSelectionAsStringList() {
		ArrayList<String> copiedTGObjects = new ArrayList<String>();
		if (copiedSelection instanceof IStructuredSelection) {
			IStructuredSelection structSel = (IStructuredSelection) copiedSelection;
			final Object[] objects = structSel.toArray();
			final TextGridObject[] textGridObjects = AdapterUtils.getAdapters(
					objects, TextGridObject.class, false);
			for (TextGridObject tgo : textGridObjects) {
				copiedTGObjects.add(tgo.getURI().toString());
			}
			return copiedTGObjects;
		}
		return null;
	}

	public ArrayList<URI> getSelection() {
		return selection;
	}

	public void setSelection(ArrayList<URI> selection) {
		this.selection = selection;
	}

	/**
	 * Overrides
	 * org.eclipse.swt.dnd.ByteArrayTransfer#nativeToJava(TransferData). Test if
	 * the native drop data matches this transfer type.
	 * 
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#nativeToJava(TransferData)
	 */
	public Object nativeToJava(TransferData transferData) {
		Object result = super.nativeToJava(transferData);
		if (isInvalidNativeType(result)) {
			Policy.getLog().log(
					new Status(IStatus.ERROR, Policy.JFACE, IStatus.ERROR,
							"TextGridObjectTransfer", null)); //$NON-NLS-1$
		}
		return selection;
	}

	/**
	 * Returns the time when the selection operation this transfer is associated
	 * with was started.
	 * 
	 * @return the time when the selection operation has started
	 * 
	 * @see org.eclipse.swt.events.TypedEvent#time
	 */
	public long getSelectionSetTime() {
		return selectionSetTime;
	}

	/**
	 * Sets the time when the selection operation this transfer is associated
	 * with was started. If assigning this from an SWT event, be sure to use
	 * <code>setSelectionTime(event.time & 0xFFFF)</code>
	 * 
	 * @param time
	 *            the time when the selection operation was started
	 * 
	 * @see org.eclipse.swt.events.TypedEvent#time
	 */
	public void setSelectionSetTime(long time) {
		selectionSetTime = time;
	}

}
