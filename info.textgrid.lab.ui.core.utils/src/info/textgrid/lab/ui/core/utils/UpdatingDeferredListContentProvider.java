package info.textgrid.lab.ui.core.utils;

import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * A {@link DeferredListContentProvider} that notifies its viewer when its
 * input's child list has changed, provided the input is a
 * {@link IChildListParent}.
 * 
 * @author tv
 */
public class UpdatingDeferredListContentProvider extends
		DeferredListContentProvider implements IChildListChangedListener,
		IContentProvider {
	
	public UpdatingDeferredListContentProvider() {
		super();
	}

	/**
	 * @see DeferredListContentProvider#DeferredListContentProvider(Class)
	 */
	public UpdatingDeferredListContentProvider(Class<?> targetClass) {
		super(targetClass);
	}

	IChildListParent input = null;
	Viewer viewer = null;

	@Override
	public void dispose() {
		if (input != null)
			input.removeChildListChangedListener(this);
		super.dispose();
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		super.inputChanged(viewer, oldInput, newInput);
		this.viewer = viewer;
		if (newInput != input) {
			if (input != null)
				input.removeChildListChangedListener(this);
			if (newInput instanceof IChildListParent)
				input = (IChildListParent) newInput;
			if (input != null)
				input.addChildListChangedListener(this);
		}
	}

	public void childListChanged(IChildListParent parent) {
		viewer.setInput(input);
	}
}