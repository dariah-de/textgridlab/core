package info.textgrid.lab.ui.core.utils;

import info.textgrid.lab.core.model.ITextGridProjectListener;
import info.textgrid.lab.core.model.ITextGridProjectListener.Event;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;

import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.widgets.Composite;

/**
 * This is a pre-configured {@link ComboViewer} that displays a list of
 * projects. Use {@link #getProject()} to get the selected project. An initial
 * value can be initialized using {@link #setProject(TextGridProject)}.
 * Alternatively you can call {@link #setNeedsValue(boolean)} if the class
 * should select an initial project after loading the list of projects.
 * 
 * @author vitt
 * 
 */
public class ProjectCombo extends ComboViewer implements IDoneListener, ITextGridProjectListener {

	protected void updateSelection(ISelection selection) {
		if (!selection.isEmpty()) {
			TextGridProject project = AdapterUtils.getAdapter(((IStructuredSelection) selection).getFirstElement(), TextGridProject.class);
			if (project != null)
				selectedProject = project;
		}
		super.updateSelection(selection);
	}

	private final UpdatingDeferredListContentProvider contentProvider;
	private volatile boolean loadDone = false;
	protected TextGridProject selectedProject = null;
	private boolean needsValue = false;

	/**
	 * If <code>true</code> and no project has been
	 * {@linkplain #setProject(TextGridProject) initially selected}, the first
	 * project will be selected after the list of project has been initially
	 * loaded.
	 */
	public boolean needsValue() {
		return needsValue;
	}

	public void setNeedsValue(boolean needsValue) {
		this.needsValue = needsValue;
	}

	public ProjectCombo(final Composite parent) {
		this(parent, LEVELS.EDITOR);
	}

	public ProjectCombo(final Composite parent, LEVELS level) {
		super(parent, SWT.SINGLE | SWT.READ_ONLY | SWT.SIMPLE | SWT.VIRTUAL);
		setLabelProvider(new TGODefaultLabelProvider());
		contentProvider = new UpdatingDeferredListContentProvider();
		setContentProvider(contentProvider);
		setSorter(new ViewerSorter());
		contentProvider.addDoneListener(this);
		init(level);
	}

	public void loadDone(final Viewer viewer) {
		setLoadDone(true);
		if (selectedProject == null && needsValue)
			selectedProject = AdapterUtils.getAdapter(getElementAt(0), TextGridProject.class);
		if (selectedProject != null)
			setProject(selectedProject);
	}

	public void setProject(final TextGridProject project) {
		selectedProject = project;
		if (isLoadDone())
			setSelection(new StructuredSelection(project), true);
	}

	public TextGridProject getProject() {
		if (!getControl().isDisposed()) {
			IStructuredSelection selection = (IStructuredSelection) getSelection();
			if (selection.isEmpty())
				return null;
			return AdapterUtils.getAdapter(selection.getFirstElement(), TextGridProject.class);
		}
		return selectedProject;
	}

	public void init(final TextGridProjectRoot.LEVELS level) {
		setLoadDone(false);
		setInput(TextGridProjectRoot.getInstance(level));
		TextGridProject.addListener(this);
	}

	@Override
	public void textGridProjectChanged(Event event, TextGridProject project) {
		if (isLoadDone()) {
			switch (event) {
				case CREATED:
					add(project);
					break;
				case DELETED:
					remove(project);
					break;
			}
		}
	}

	@Override
	protected void handleDispose(DisposeEvent event) {
		super.handleDispose(event);
		TextGridProject.removeListener(this);
	}

	public void setLoadDone(boolean loadDone) {
		this.loadDone = loadDone;
	}

	public boolean isLoadDone() {
		return loadDone;
	}
}
