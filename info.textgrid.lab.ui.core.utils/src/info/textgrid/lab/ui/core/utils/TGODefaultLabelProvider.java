package info.textgrid.lab.ui.core.utils;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.RestrictedTextGridObject;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.search.TextGridRepositoryItem;
import info.textgrid.lab.search.TextGridRepositoryItem_SandBox;
import info.textgrid.lab.ui.core.utils.Activator;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.DecoratingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE.SharedImages;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.progress.PendingUpdateAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Joiner;

/**
 * This is a generic label provider that can label common TextGrid stuff. It can
 * be used for first or single columns. It will typically display the title and
 * icon of the object to label, and it will use the platform decorator to add
 * further decoration.
 * 
 * <h3>Supported Object Types</h3> Currently the following stuff will be
 * supported:
 * 
 * <dl>
 * <dt>{@link TextGridObject}s</dt>
 * <dd>displays the object's title and an icon indicating the content type
 * (unstyled)</dd>
 * <dt>{@link TextGridProject}s</dt>
 * <dd>displays the project's title and the platform's default project icon</dd>
 * <dt>{@link PendingUpdateAdapter} (used with deferred stuff)</dt>
 * <dd>displays the string <em>Pending ...</em> and a clock icon</dd>
 * </dl>
 * 
 * @author vitt
 * 
 */
public class TGODefaultLabelProvider extends DecoratingStyledCellLabelProvider implements ILabelProvider {

	/**
	 * A styled label provider that displays the icon and title of a
	 * TextGridObject
	 * 
	 * @author vitt
	 */
	protected static class TGOTitleLabelProvider extends BaseLabelProvider implements IStyledLabelProvider {

		private static final Styler PENDING_STYLER = new Styler() {
		
							public void applyStyles(TextStyle textStyle) {
								textStyle.font = JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT);
							}
		
						};
						
						
		private final boolean usingIcons;

		public TGOTitleLabelProvider(final boolean usingIcons) {
			this.usingIcons = usingIcons;
		}

		public Image getImage(Object element) {

			if (!usingIcons)
				return null;

			if (element instanceof PendingUpdateAdapter)
				return Activator.getDefault().getImageRegistry().get(Activator.LOADING_IMAGE_ID);

			if (element instanceof RestrictedTextGridObject) 
				return Activator.getDefault().getImageRegistry().get(Activator.RESTRICTED_IMAGE_ID);
			
			if (element instanceof TextGridRepositoryItem) 
				return Activator.getDefault().getImageRegistry().get(Activator.REPOSITORY_IMAGE_ID);
			
			if (element instanceof TextGridRepositoryItem_SandBox) 
				return Activator.getDefault().getImageRegistry().get(Activator.REPOSITORY_SANDBOX_IMAGE_ID);
			
			try {
				TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
				if (textGridObject != null)
					return textGridObject.getContentType(false).getImage(true);
				
				TextGridProject project = getAdapter(element, TextGridProject.class);
				if (project != null) {
					return PlatformUI.getWorkbench().getSharedImages().getImage(SharedImages.IMG_OBJ_PROJECT);
				}
				
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}

			return null;
		}

		public StyledString getStyledText(Object element) {
			if (element instanceof PendingUpdateAdapter)
				return new StyledString(Messages.TGODefaultLabelProvider_Pending, PENDING_STYLER);
			
			try {
				final TextGridObject textGridObject = getAdapter(element, TextGridObject.class);
				if (textGridObject != null)
					return new StyledString(textGridObject.getTitle());

				final TextGridProject project = getAdapter(element, TextGridProject.class);
				if (project != null)
					return new StyledString(project.getName());

			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				return new StyledString(e.getLocalizedMessage(), TextGridObjectTableViewer.ERROR_STYLER);
			}

			IWorkbenchAdapter workbenchAdapter = getAdapter(element, IWorkbenchAdapter.class);
			if (workbenchAdapter != null)
				return new StyledString(workbenchAdapter.getLabel(element));
			else if (element != null)
				return new StyledString(element.toString());
			else
				return new StyledString();
		}
	}

	public TGODefaultLabelProvider(boolean usingIcons) {
		super(new TGOTitleLabelProvider(usingIcons), PlatformUI.getWorkbench().getDecoratorManager(), null);
	}

	public TGODefaultLabelProvider() {
		this(true);
	}

	@Override
	public String getToolTipText(Object element) {
		TextGridObject object = getAdapter(element, TextGridObject.class);
		if (object != null) {
			try {
				return MessageFormat
						.format(
								Messages.TGODefaultLabelProvider_ToolTipText,
								object.getTitle(), Joiner.on("; ").join(
										object.getAuthors()), object
										.getProjectInstance().getName(),
								object.getContentType(false).getDescription(),
								object.getLastModified().getTime(),
								object.getURI()
								);
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}

		return null;
	}

	public String getText(Object element) {
		if (element instanceof PendingUpdateAdapter) {
			return getStyledText(element).toString();	
		} else if (element instanceof TextGridProject) {
			return ((TextGridProject)element).getName();
		} else if (element instanceof RestrictedTextGridObject) {
			return NLS.bind("{0} ({1})", RestrictedTextGridObject.TITLE, ((RestrictedTextGridObject) element).getURI().toString());
		} else if (element instanceof TextGridRepositoryItem) {
			return ((TextGridRepositoryItem)element).toString();
		} else if (element instanceof TextGridRepositoryItem_SandBox) {
			return ((TextGridRepositoryItem_SandBox)element).toString();
		} else {
			TextGridObject tgo = AdapterUtils.getAdapter(element, TextGridObject.class);
			if (tgo != null) {
				try {
					return tgo.getTitle();
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				}
			}
		}
		return null;	
	}

}
