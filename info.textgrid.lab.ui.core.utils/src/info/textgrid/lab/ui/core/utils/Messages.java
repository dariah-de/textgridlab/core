package info.textgrid.lab.ui.core.utils;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.ui.core.utils.messages"; //$NON-NLS-1$
	public static String ProjectNavigator_WM_CouldNotRetrieveContentType;
	public static String TextGridObjectTableViewer_Author;
	public static String TextGridObjectTableViewer_CreationDate;
	public static String TextGridObjectTableViewer_DataContributor;
	public static String TextGridObjectTableViewer_Project;
	public static String TextGridObjectTableViewer_Title;
	public static String TextGridObjectTableViewer_Warning;
	public static String TGODefaultLabelProvider_Pending;
	public static String TGODefaultLabelProvider_ToolTipText;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
