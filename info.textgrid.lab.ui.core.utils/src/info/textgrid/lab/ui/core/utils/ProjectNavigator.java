package info.textgrid.lab.ui.core.utils;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.search.DeferredTreeContentProvider;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

/**
 * This is a pre-configured {@link TreeViewer} that displays all projects and
 * allows to browse the associated Tg-object.
 * 
 * @author Frank Queens for TextGrid
 * 
 */
public class ProjectNavigator extends TreeViewer {

	class ProjectNavigatorFilter extends ViewerFilter {
		private String contentTypeID;

		ProjectNavigatorFilter(String contentTypeID) {
			this.contentTypeID = contentTypeID;
		}

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {
			if (element instanceof TGObjectReference) {
				TextGridObject tgo = AdapterUtils.getAdapter(element,
						TextGridObject.class);
				if (tgo != null) {
					try {
						if (tgo.getContentTypeID().equals(contentTypeID))
							return true;
						else
							return false;
					} catch (CoreException e) {
						Activator
								.handleProblem(
										IStatus.WARNING,
										Messages.ProjectNavigator_WM_CouldNotRetrieveContentType,
										e);
					}
				}
				return false;
			} else {
				return true;
			}
		}

	}

	class ProjectNavigatorWorkFilter extends ViewerFilter {

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {
			if (element instanceof TGObjectReference) {
				TextGridObject tgo = AdapterUtils.getAdapter(element,
						TextGridObject.class);
				if (tgo != null) {
					try {
						if (tgo.getContentTypeID().equals("text/tg.work+xml") //$NON-NLS-1$
								|| tgo.getContentTypeID().contains(
										"tg.aggregation+xml")) //$NON-NLS-1$
							return true;
						else
							return false;
					} catch (CoreException e) {
						Activator
								.handleProblem(
										IStatus.WARNING,
										Messages.ProjectNavigator_WM_CouldNotRetrieveContentType,
										e);
					}
				}
				return false;
			} else {
				return true;
			}
		}

	}

	class ProjectNavigatorXmlImageFilter extends ViewerFilter {

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {
			if (element instanceof TGObjectReference) {
				TextGridObject tgo = AdapterUtils.getAdapter(element,
						TextGridObject.class);
				if (tgo != null) {
					try {
						String id = tgo.getContentTypeID();
						if (id.equals("text/xml") || id.contains("image/") //$NON-NLS-1$ //$NON-NLS-2$
								|| id.contains("tg.aggregation+xml")) //$NON-NLS-1$
							return true;
						else
							return false;
					} catch (CoreException e) {
						Activator
								.handleProblem(
										IStatus.WARNING,
										Messages.ProjectNavigator_WM_CouldNotRetrieveContentType,
										e);
					}
				}
				return false;
			} else {
				return true;
			}
		}

	}

	public ProjectNavigator(final Composite parent) {
		this(parent, LEVELS.EDITOR, SWT.NONE);
	}

	public ProjectNavigator(final Composite parent, LEVELS level, int style) {
		super(parent, style);

		setContentProvider(new DeferredTreeContentProvider());
		setLabelProvider(new TGODefaultLabelProvider(true));
		setSorter(new ViewerSorter());
		setInput(TextGridProjectRoot.getInstance(level));
	}

	/**
	 * Sets filter to limit the navigator content to particular content types.
	 * The filter only cares about TextGrid object, projects aren't hit by the
	 * filter. Viewer filters are additive, which means that the output of one
	 * filter is passed in as the input of the next filter. The final result has
	 * been filtered through each view filter.
	 * 
	 * @param contentTypeID
	 * @return the new filter
	 */
	public ViewerFilter setContentTypeFilter(String contentTypeID) {
		ProjectNavigatorFilter filter = new ProjectNavigatorFilter(
				contentTypeID);
		this.addFilter(filter);
		return filter;
	}

	/**
	 * Sets a filter so that only works and container objects like aggregations,
	 * editions and collections are shown. Viewer filters are additive, which
	 * means that the output of one filter is passed in as the input of the next
	 * filter. The final result has been filtered through each view filter.
	 * 
	 * @return the new filter
	 */
	public ViewerFilter setWorkFilter() {
		ProjectNavigatorWorkFilter filter = new ProjectNavigatorWorkFilter();
		this.addFilter(filter);
		return filter;
	}

	/**
	 * Sets a filter so that only xml and image objects and container objects
	 * like aggregations, editions and collections are shown. Viewer filters are
	 * additive, which means that the output of one filter is passed in as the
	 * input of the next filter. The final result has been filtered through each
	 * view filter.
	 * 
	 * @return the new filter
	 */
	public ViewerFilter setXmlImageFilter() {
		ProjectNavigatorXmlImageFilter filter = new ProjectNavigatorXmlImageFilter();
		this.addFilter(filter);
		return filter;
	}

}
