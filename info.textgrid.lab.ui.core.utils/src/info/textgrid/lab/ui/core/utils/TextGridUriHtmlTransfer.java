package info.textgrid.lab.ui.core.utils;

import info.textgrid.lab.core.model.TGObjectReference;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TransferData;

/**
 * Transfer for Drag and Drop from Navigator to Browserwidget,
 * writes TextGridUri, title and content type to <li> item
 * 
 * @author ubbo
 *
 */
public class TextGridUriHtmlTransfer extends ByteArrayTransfer {

	private static final String TEXT_HTML = "text/html"; //$NON-NLS-1$
	private static final int TEXT_HTML_ID = registerType(TEXT_HTML);
	private static final String TEXT_HTML2 = "TEXT/HTML"; //$NON-NLS-1$
	private static final int TEXT_HTML2_ID = registerType(TEXT_HTML2);

	private static final TextGridUriHtmlTransfer INSTANCE = new TextGridUriHtmlTransfer();

	/**
	 * Only the singleton instance of this class may be used.
	 */
	protected TextGridUriHtmlTransfer() {
		// do nothing
	}

	/**
	 * Returns the singleton.
	 * 
	 * @return the singleton
	 */
	public static TextGridUriHtmlTransfer getTransfer() {
		return INSTANCE;
	}

	/**
	 * Overrides org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(Object,
	 * TransferData).
	 * 
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(java.lang.Object,
	 *      org.eclipse.swt.dnd.TransferData)
	 */
	public void javaToNative(Object object, TransferData transferData) {

		if (!checkTGO(object) || !isSupportedType(transferData)) {
			DND.error(DND.ERROR_INVALID_DATA);
		}
		
		TGObjectReference tgor = (TGObjectReference) object;

		String title = "";
		String type = "";
		try {
			title = tgor.getTgo().getTitle();
			type = tgor.getTgo().getContentTypeID();
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String uri = tgor.getRefUri() + "." + tgor.getRevisionNumber();
		
		String string = "<li class='tgo' uri='"+uri+"' type='"+type+"'> "+ title +" ("+uri+") </li>";

		
		byte[] buff = string.getBytes();
		
		String os = System.getProperty("os.name").toLowerCase();
		
		// on windows the browser thinks the transferdata is utf-16, so fill some 0 to align the string
		// for javascript access
		if (os.indexOf("win") >= 0 || os.indexOf("mac") > 0) {
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
	        DataOutputStream writeOut = new DataOutputStream(out);
			for (int i = 0; i < buff.length; i++ ) {
				try {
					writeOut.write(buff[i]);
					writeOut.write(new byte[1]);
					
					//writeOut.writeInt(buff.length);
					//writeOut.write(buff);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			buff = out.toByteArray();
		} 

		super.javaToNative(buff, transferData);
		

	}

	
	private boolean checkTGO(Object object) {
		return (object != null && object instanceof TGObjectReference);
	}

	protected int[] getTypeIds() {
		return new int[] {TEXT_HTML_ID, TEXT_HTML2_ID};
	}

	protected String[] getTypeNames() {
		return new String[] {TEXT_HTML, TEXT_HTML2};
	}

}
