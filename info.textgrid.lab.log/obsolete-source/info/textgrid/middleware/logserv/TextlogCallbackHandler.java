
/**
 * TextlogCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4  Built on : Apr 26, 2008 (06:24:30 EDT)
 */

    package info.textgrid.middleware.logserv;

    /**
     *  TextlogCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class TextlogCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public TextlogCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public TextlogCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for initialize method
            * override this method for handling normal response from initialize operation
            */
           public void receiveResultinitialize(
                    info.textgrid.middleware.logserv.TextlogStub.InitializeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from initialize operation
           */
            public void receiveErrorinitialize(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for log method
            * override this method for handling normal response from log operation
            */
           public void receiveResultlog(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from log operation
           */
            public void receiveErrorlog(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLog method
            * override this method for handling normal response from getLog operation
            */
           public void receiveResultgetLog(
                    info.textgrid.middleware.logserv.TextlogStub.GetLogResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLog operation
           */
            public void receiveErrorgetLog(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLogFragment method
            * override this method for handling normal response from getLogFragment operation
            */
           public void receiveResultgetLogFragment(
                    info.textgrid.middleware.logserv.TextlogStub.GetLogFragmentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLogFragment operation
           */
            public void receiveErrorgetLogFragment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for endSession method
            * override this method for handling normal response from endSession operation
            */
           public void receiveResultendSession(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from endSession operation
           */
            public void receiveErrorendSession(java.lang.Exception e) {
            }
                


    }
    