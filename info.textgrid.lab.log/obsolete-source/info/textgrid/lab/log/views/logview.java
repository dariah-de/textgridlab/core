package info.textgrid.lab.log.views;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.OnlineStatus.IStatusChangeListener;
import info.textgrid.lab.log.Activator;
import info.textgrid.lab.log.LogDaemon;
import info.textgrid.lab.log.logsession;
import info.textgrid.middleware.logserv.TextlogStub;
import info.textgrid.middleware.logserv.TextlogStub.EndSession;
import info.textgrid.middleware.logserv.TextlogStub.InitializeResponse;
import java.rmi.RemoteException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

/**
 * Starts a {@link logsession} and the {@link LogDaemon}.
 * Shows the logview, with the possibility to set a loglevel and view logs. 
 */
public class logview extends ViewPart {

	private Text logoutput;
	private Text anzeige;
	private logsession session;
	private String logEndpoint;
	private TextlogStub stub;
	private Button c, d, e, f, g;
	private Boolean enabled = true;
	private IStatusChangeListener listener;

	private String logsessionid;
	private LogDaemon logdaemon;

	private void setloglevel(int level) {
		this.session.setLogLevel(level, logsessionid);
	}

	/**
	 * The constructor.
	 */
	public logview() {
	}

	/**
	 * Create the view, add "choose-loglevel"-radiobuttons and textoutput.
	 * Start a {@link logsession} and the {@link LogDaemon}
	 */
	@Override
	public void createPartControl(Composite parent) {

		/**
		 * Build the ui
		 */
		Composite hauptcom = new Composite(parent, SWT.NONE);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		hauptcom.setLayout(gridLayout);
		GridData gridData = new GridData(GridData.FILL_VERTICAL);
		hauptcom.setLayoutData(gridData);

		Group top1 = new Group(hauptcom, SWT.NONE);

		RowLayout rowl2 = new RowLayout();
		rowl2.wrap = false;
		top1.setLayout(rowl2);
		Label label = new Label(top1, SWT.NULL);
		label.setText("Set Log Level: ");
		c = new Button(top1, SWT.RADIO);
		c.setText("0");
		c.setSelection(true);
		c.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				setloglevel(0);
				anzeige.setText("0 - Disabled");
			}
		});
		d = new Button(top1, SWT.RADIO);
		d.setText("1");
		d.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				setloglevel(1);
				anzeige.setText("1 - Error");
			}
		});
		e = new Button(top1, SWT.RADIO);
		e.setText("2");
		e.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				setloglevel(2);
				anzeige.setText("2 - Warning");
			}
		});
		f = new Button(top1, SWT.RADIO);
		f.setText("3");
		f.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				setloglevel(3);
				anzeige.setText("3 - Info");
			}
		});
		g = new Button(top1, SWT.RADIO);
		g.setText("4");
		g.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				setloglevel(4);
				anzeige.setText("4 - Debug");
			}
		});

		Group top3 = new Group(hauptcom, SWT.NONE);
		RowLayout rowl3 = new RowLayout();
		rowl3.wrap = false;
		top3.setLayout(rowl3);
		anzeige = new Text(top3, SWT.READ_ONLY | SWT.SINGLE);
		anzeige.setVisible(true);
		anzeige.setText("0 - Disabled");

		Group top2 = new Group(hauptcom, SWT.NONE);
		GridLayout rowLayout = new GridLayout();
		rowLayout.numColumns = 1;
		top2.setLayout(rowLayout);

		GridData gridData2 = new GridData();
		gridData2.horizontalAlignment = GridData.FILL;
		gridData2.verticalAlignment = GridData.FILL;
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.grabExcessVerticalSpace = true;
		GridData gridData3 = new GridData();
		gridData3.horizontalAlignment = GridData.FILL;
		gridData3.horizontalSpan = 2;
		gridData3.verticalAlignment = GridData.FILL;
		gridData3.grabExcessHorizontalSpace = true;
		gridData3.grabExcessVerticalSpace = true;
		top2.setLayoutData(gridData3);

		top2.setText("Log Monitor");

		logoutput = new Text(top2, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER
				| SWT.MULTI);
		logoutput.setLayoutData(gridData2);
		logoutput.setEditable(false);
		
		/**
		 * connect logdaemon, logsession etc
		 */	
		session = logsession.getInstance();
		try {
			logEndpoint = session.getLogEndpoint();
		} catch (OfflineException e1) {
			OnlineStatus.netAccessFailed("logsession: Could not fetch Log-Service-Endpoint from Confserver!", e1);
			setEnabled(false, "no logendpoint set");
			return;
		}
		
		
		try {
			this.stub = new TextlogStub(this.logEndpoint);
			InitializeResponse initResp = stub.initialize();
			logsessionid = initResp.get_return();
		} catch (RemoteException te) {
			setEnabled(false, "LogService does not respond on init");
		}

		this.setloglevel(0);

		logdaemon = new LogDaemon(this, logsessionid, stub);
		logdaemon.start();
		
		/**
		 * set listener on onlinestatus
		 */
		listener = new IStatusChangeListener(){

			public void statusChanged(final boolean state) {

				if(Display.getCurrent() == null){
					UIJob uij = new UIJob ("LogView") {
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							logdaemon.setSuspended(!state);
							enabled = state;
							c.setEnabled(state);
							d.setEnabled(state);
							e.setEnabled(state);
							f.setEnabled(state);
							g.setEnabled(state);
							return new Status(IStatus.OK,Activator.PLUGIN_ID,
									"changed button status");
						}		
					};
					uij.schedule();
					try {
						uij.join();
					} catch (InterruptedException e) {
						IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
								"button status could not be changed", e);
						Activator.getDefault().getLog().log(status);
					}
					
				}else{
					logdaemon.setSuspended(!state);
					enabled = state;
					c.setEnabled(state);
					d.setEnabled(state);
					e.setEnabled(state);
					f.setEnabled(state);
					g.setEnabled(state);
				}
			}
		};	
		OnlineStatus.addOnlineStatusChangeListener(listener);

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {

	}

	@Override
	public void dispose() {
		this.setloglevel(0);
		logdaemon.stopPolite();
		OnlineStatus.removeOnlineStatusChangeListener(listener);
		try {
			EndSession endSession = new EndSession();
			endSession.setParam0(logsessionid);
			this.stub.endSession(endSession);

		} catch (RemoteException e) {
			Activator
			.handleError(e, "error reaching logservice to end logsession");
		}
	}

	/**
	 * set / unset enabled state for logview, used e.g. when lab is offline.
	 * disables radiobuttons for loglevel-selection.
	 * 
	 * @param state boolean, true for enabled / false for disabled
	 * @param message message to show in logview
	 */
	public void setEnabled(Boolean state, String message){
		enableButtons(state);
		if(message != null){
			this.puttext(message);
		}	
	}

	/**
	 * check if logview is enabled
	 * @return
	 */
	public Boolean getEnabled(){
		return enabled;
	}

	/**
	 * enable / disable loglevel buttons
	 * @param state
	 */
	private void enableButtons(final Boolean state){
		if(Display.getCurrent() == null){
			UIJob uij = new UIJob ("LogView") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					enabled = state;
					c.setEnabled(state);
					d.setEnabled(state);
					e.setEnabled(state);
					f.setEnabled(state);
					g.setEnabled(state);
					return new Status(IStatus.OK,Activator.PLUGIN_ID,
					"changed button status");
				}		
			};
			uij.schedule();
			try {
				uij.join();


			} catch (InterruptedException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"button status could not be changed", e);
				Activator.getDefault().getLog().log(status);
			}			
		}else{
			enabled = state;
			c.setEnabled(state);
			d.setEnabled(state);
			e.setEnabled(state);
			f.setEnabled(state);
			g.setEnabled(state);
		}
	}

	/**
	 * output text in logview
	 * @param textout
	 */
	public void puttext(final String textout){
		if(Display.getCurrent() == null){
			UIJob uij = new UIJob ("LogView textoutput") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					logoutput.append(textout);
					logoutput.append("\n");
					
					return new Status(IStatus.OK,Activator.PLUGIN_ID,
					"textoutput done");
				}		
			};
			uij.schedule();
			try {
				uij.join();

			} catch (InterruptedException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"could not write text to output", e);
				Activator.getDefault().getLog().log(status);
			}			
		}else{
			logoutput.append(textout);
			logoutput.append("\n");
		}
	}

}
