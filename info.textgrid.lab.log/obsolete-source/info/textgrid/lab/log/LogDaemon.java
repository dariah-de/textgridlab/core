package info.textgrid.lab.log;

import info.textgrid.lab.log.views.logview;
import info.textgrid.middleware.logserv.TextlogStub;
import info.textgrid.middleware.logserv.TextlogStub.GetLogFragment;
import info.textgrid.middleware.logserv.TextlogStub.GetLogFragmentResponse;

import java.rmi.RemoteException;

/**
 * A thread that queries the logserver for new LogFragments
 * every 2000ms. Puts new LogFragments in the textoutput of {@link logview}.
 */
public class LogDaemon extends Thread {

	logview log;
	String sessionid;
	TextlogStub stub;
	Boolean stopped = false;
	Boolean suspended = false;

	int counter = 0;

	/**
	 * Constructor
	 * 
	 * @param log 
	 * 		the logview for textoutput
	 * @param sessionid 
	 * 		the logsessionid from logserver
	 * @param stub 
	 * 		the stub to use for logservice-querying 
	 */
	public LogDaemon(logview log, String sessionid, TextlogStub stub) {
		setDaemon(true);
		this.log = log;
		this.sessionid = sessionid;
		this.stub = stub;
	}
	
	/**
	 * Suspend the logdaemon, for example if lab offline, so it won't
	 * try to access the net
	 * @param state
	 */
	public void setSuspended(Boolean state){
		suspended = state;
	}

	/**
	 * stop log-daemon, after thread wakes up
	 */
	public void stopPolite(){
		stopped = true;
	}

	/**
	 * look for new logs at LOG_ENDPOINT,
	 * output new entries on logview - sleep 2000ms
	 */
	public void run() {

		while (!stopped) {
			if(!suspended){

				try {
					GetLogFragment getLogFragment = new GetLogFragment();
					getLogFragment.setParam0(sessionid);
					getLogFragment.setParam1(counter);
					GetLogFragmentResponse resp = stub.getLogFragment(getLogFragment);
					String[] output = resp.get_return();

					if (output.length != 1 || !(output[0].equalsIgnoreCase(""))) {
						for (int i = 0; i < output.length; i++) {
							log.puttext(output[i]);
						}
						counter = counter + output.length;
					}
					
					if(!log.getEnabled()){
						log.setEnabled(true, "Log-Service responds again");
					}	
				} catch (RemoteException e) {
					if(log.getEnabled()){
						log.setEnabled(false, "Log-Service does not respond");
					}
				}
			}

			try	{
				Thread.sleep(2000);
			} catch (InterruptedException e1) {

			}

		}

	}
}
